﻿using System.Collections;
using TerrainEngine;
using UnityEngine;

public class FlammableScript : MonoBehaviour 
{
    public TerrainObject TObject;

    public GameObject[] RandomEffects;
    private GameObject EffectObject;
    public float PropagationTime = 3;
    public float DestroyTime = 6f;
    public int MaxPropagation =3;
    public int MaxChild = 50;
    private int PropagationCount = 0;
    private ActionProgressCounter Progress;
    private bool HasFlame = false;

	// Use this for initialization
	void Start () 
    {
        if (MaxChild <= 0)
        {
            GameObject.Destroy(gameObject);
            return;
        }

        if (TObject == null)
            TObject = gameObject.AddComponent<TerrainObject>();

        TObject.UpdateCurrentVoxel = true;
        EffectObject = RandomEffects[Random.Range(0, RandomEffects.Length)];
        EffectObject = GameObject.Instantiate(EffectObject,transform.position,Quaternion.Euler(0,Random.Range(0,360),0)) as GameObject;
        EffectObject.SetActive(true);

        StartCoroutine(FlameRoutine());
	}

    public IEnumerator FlameRoutine()
    {
        int Count = 0;
        while (!TObject.HasBlock || !TObject.HasChunk)
        {
            if (Count >= 20)
            {
                GameObject.Destroy(gameObject);
                yield return null;
            }
            ++Count;
            yield return new WaitForSeconds(1f);
        }

        TObject.UpdatePosition();
        yield return new WaitForSeconds(PropagationTime + Random.Range(0,PropagationTime*0.3f));
        Flame();

        if(HasFlame)
        {
            //yield return new WaitForSeconds(PropagationTime);
            Propagate();
        }

        while (Progress != null && Progress.DoneProgress < 100f)
        {
            yield return new WaitForSeconds(1f);
        }

        yield return new WaitForSeconds(DestroyTime - PropagationTime + Random.Range(0, PropagationTime * 0.3f));
        GameObject.Destroy(gameObject);
        yield return null;
    }

    public void Flame()
    {
        if (TObject == null || TObject.BlockObject == null || !TObject.HasBlock)
            return;

        VoxelsOperator Operator = null;

        for (int i = TObject.BlockObject.Voxels.Customs.Count - 1; i >= 0; --i)
        {
            if (TObject.BlockObject.Voxels.Customs.array[i] is SimpleTypeDatas)
            {
                if (TObject.BlockObject.Voxels.Customs.array[i].GetByte(TObject.IntCurrentVoxel.x, TObject.IntCurrentVoxel.y, TObject.IntCurrentVoxel.z) != 0)
                {
                    if (Operator == null)
                        Operator = new VoxelsOperator();

                    Operator.Voxels.Add(new VoxelPoint(TObject.BlockObject.BlockId, TObject.IntCurrentVoxel.x, TObject.IntCurrentVoxel.y, TObject.IntCurrentVoxel.z, 0, 0, VoxelModificationType.SEND_EVENTS));
                    HasFlame = true;
                    break;
                }

            }
        }

        if (Operator != null)
        {
            TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
            TerrainManager.Instance.ModificationInterface.Flush();
        }
    }

    public void Propagate()
    {
        VectorI3 Dir;
        if (TObject == null || TObject.BlockObject == null || !TObject.HasBlock)
            return;

        for (int j = TObject.BlockObject.Voxels.Customs.Count - 1; j >= 0;--j)
        {
            if (TObject.BlockObject.Voxels.Customs.array[j] is SimpleTypeDatas)
            {
                for (int i = 0; i < 26; ++i)
                {
                    if (Random.Range(0, 100) < 50)
                        continue;

                    Dir = BlockHelper.direction[i];
                    if (Dir.x == 0 && Dir.y == 0 && Dir.z == 0)
                        continue;

                    if (Dir.x == 0 && Dir.y == -1 && Dir.z == 0)
                        continue;

                    if (CheckElement(TObject.BlockObject, TObject.IntCurrentVoxel.x + Dir.x, TObject.IntCurrentVoxel.y + Dir.y, TObject.IntCurrentVoxel.z + Dir.z))
                    {
                        if (Progress == null)
                            Progress = new ActionProgressCounter();

                        //Debug.Log("-->Flamme dispatch to : " + (TObject.IntCurrentVoxel + Dir));

                        CreateGameObjectAction Action = PoolManager.UPool.GameObjectCreateActionPool.GetData<CreateGameObjectAction>();
                        Action.Init(gameObject, TObject.BlockObject.GetWorldPosition(TObject.IntCurrentVoxel + Dir, false) + TerrainManager.Instance.Informations.DemiScale, Quaternion.Euler(0, Random.Range(0, 360), 0), Vector3.one);
                        Progress.Add(ActionProcessor.AddToUnity(Action));
                        ++PropagationCount;

                        --MaxChild;
                        if (MaxChild <= 0)
                            return;
                    }

                    if (PropagationCount >= MaxPropagation)
                        return;
                }
            }
        }
    }

    public bool CheckElement(TerrainBlock Block, int x,int y,int z)
    {
        Block = Block.GetAroundBlock(ref x,ref y,ref z) as TerrainBlock;
        if(Block == null)
            return false;

        for (int j = Block.Voxels.Customs.Count - 1; j >= 0; --j)
        {
            if (Block.Voxels.Customs.array[j] is SimpleTypeDatas)
            {
                if (Block.Voxels.Customs.array[j].GetByte(x, y, z) != 0)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
