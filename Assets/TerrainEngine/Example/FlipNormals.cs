﻿using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class FlipNormals : MonoBehaviour
{
    void Start()
	{
        Debug.Log("Flip");

        MeshFilter CFilter = GetComponent<MeshFilter>();

            Mesh M = CFilter.sharedMesh;
            Vector3[] Normals = M.normals;

                for (int i = 0; i < Normals.Length; ++i)
                    Normals[i] = -Normals[i];
        M.normals = Normals;
        CFilter.sharedMesh = M;
    }
}