using UnityEngine;

public class InstanciateEventScript : MonoBehaviour 
{
    public GameObject ToInstanciate;
    public bool UseCurrentPosition = true;
    public bool UseCurrentRotation = true;
    public float ForwardPower = 0;
    public bool UseAsParent=false;
    public Vector3 PositionOffset;
    public Vector3 RotationOffset;
    public GameObject CreatedObject;
    public bool AutoDisable = false;

    void OnEnable()
    {
        if (ToInstanciate != null)
        {
            CreatedObject = GameObject.Instantiate(ToInstanciate) as GameObject;
            if (UseCurrentPosition)
                CreatedObject.transform.position = transform.position + PositionOffset + transform.forward * ForwardPower;

            if (UseCurrentRotation)
                CreatedObject.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + RotationOffset);

            if (UseAsParent)
                CreatedObject.transform.parent = transform;
        }

        if (AutoDisable)
            enabled = false;
    }
}
