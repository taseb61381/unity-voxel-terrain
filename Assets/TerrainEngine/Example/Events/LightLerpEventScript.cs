﻿using TerrainEngine;
using UnityEngine;

public class LightLerpEventScript : MonoBehaviour 
{
    public string Name = "";
    public Light TargetLight;
    public float LerpTime;
    public float Targetintensity;
    public Color TargetColor;
    public bool ModifyIntensity = true;
    public bool ModifyColor = true;
    public bool ModifyAmbientColor = true;
    public bool CopyValuesOnAwake = false;
    public bool UseDistance = false;
    public Transform TargetTransform;

    public bool DisableWhenDone = true;

    public bool Fog = true;
    public Color TargetFogColor;
    public Color TargetAmbientColor;
    public float t;
    public ActiveDisableContainer OnDone;

    private float StartTime;
    private float Startintensity;
    private Color StartColor;
    private Color StartFogColor;
    private Color StartAmbientColor;
    private Vector3 StartPosition;

    void Awake()
    {
        if (CopyValuesOnAwake)
        {
            Targetintensity = TargetLight.intensity;
            TargetColor = TargetLight.color;
            TargetFogColor = RenderSettings.fogColor;
            TargetAmbientColor = RenderSettings.ambientLight;
            Fog = RenderSettings.fog;
            TargetFogColor = RenderSettings.fogColor;
            enabled = false;
        }

        if(TargetTransform ==null)
            TargetTransform = transform;
    }

    void OnEnable()
    {
        StartTime = Time.realtimeSinceStartup;
        Startintensity = TargetLight.intensity;
        StartColor = TargetLight.color;
        StartFogColor = RenderSettings.fogColor;
        StartAmbientColor = RenderSettings.ambientLight;
        StartPosition = TargetTransform.position;
    }

    void Update()
    {
        if (!UseDistance)
        {
            t = (Time.realtimeSinceStartup - StartTime) / LerpTime;
        }
        else
        {
            t = Mathf.Abs(Vector3.Distance(StartPosition, TargetTransform.position)) / LerpTime;
        }
        if (t < 0)
            t = 0;
        else if (t > 1)
            t = 1;

        if (ModifyColor)
            TargetLight.color = Color.Lerp(StartColor, TargetColor, t);
        if (ModifyIntensity)
            TargetLight.intensity = Mathf.Lerp(Startintensity, Targetintensity, t);

        RenderSettings.fog = Fog;

        if (Fog)
        {
            RenderSettings.fogColor = Color.Lerp(StartFogColor, TargetFogColor, t);
        }

        if (ModifyAmbientColor)
        {
            RenderSettings.ambientLight = Color.Lerp(StartAmbientColor, TargetAmbientColor, t);
        }

        if (DisableWhenDone && t >= 1f)
        {
            if (OnDone != null)
                OnDone.Call();
            enabled = false;
        }
    }
}
