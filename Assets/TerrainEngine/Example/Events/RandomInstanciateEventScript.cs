using UnityEngine;

public class RandomInstanciateEventScript : MonoBehaviour
{
    public GameObject[] ToInstanciates;
    public bool UseCurrentPosition = true;
    public bool UseCurrentRotation = true;
    public float ForwardPower = 0;
    public bool UseAsParent = false;
    public Vector3 PositionOffset;
    public Vector3 RotationOffset;
    public GameObject CreatedObject;

    private bool MustInstanciate = false;
    void OnEnable()
    {
        if (ToInstanciates != null && ToInstanciates.Length > 0)
        {
            MustInstanciate = true;
        }
    }

    void Update()
    {
        if (!MustInstanciate)
            return;

        if (MustInstanciate)
        {
            MustInstanciate = false;
            Transform tr = transform;
            SFastRandom FRandom = new SFastRandom(((int)tr.position.x ^ (int)tr.position.y ^ (int)tr.position.z) + 1);
            GameObject ToInstanciate = ToInstanciates[FRandom.randomIntAbs(ToInstanciates.Length)];

            if (ToInstanciate != null)
            {
                CreatedObject = GameObject.Instantiate(ToInstanciate) as GameObject;
                if (UseAsParent)
                    CreatedObject.transform.parent = tr;

                if (UseCurrentPosition)
                    CreatedObject.transform.position = tr.position + PositionOffset + tr.forward * ForwardPower;
                else
                    CreatedObject.transform.localPosition = new Vector3();

                if (UseCurrentRotation)
                    CreatedObject.transform.localRotation = Quaternion.Euler(tr.rotation.eulerAngles + RotationOffset);

                CreatedObject.transform.localScale = new Vector3(1, 1, 1);

            }
        }

        enabled = false;
    }
}
