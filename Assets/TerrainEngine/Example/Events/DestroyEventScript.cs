using TerrainEngine;
using UnityEngine;

public class DestroyEventScript : MonoBehaviour 
{
    public float DestroyTime = 5f;
    public bool DestroyParent = false;
    public ActiveDisableContainer OnDestroy;

    private float NextDestroy;

    void OnEnable()
    {
        NextDestroy = Time.realtimeSinceStartup + DestroyTime;
    }

    void Update()
    {
        if (NextDestroy != 0 && NextDestroy < Time.realtimeSinceStartup)
        {
            if (OnDestroy != null)
                OnDestroy.Call();

            if (DestroyParent && transform.parent != null)
                Destroy(transform.parent.gameObject);

            Destroy(gameObject);
        }
    }
}
