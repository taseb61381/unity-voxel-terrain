using UnityEngine;


public class RandomPlaySoundEventScript : MonoBehaviour
{
    public AudioClip[] Audios;
    public float Volume;
    public bool AutoDisable = false;

    void OnEnable()
    {
        if (Audios != null && Audios.Length > 0)
        {
            AudioClip Clip = Audios[Random.Range(0, Audios.Length)];
            AudioSource.PlayClipAtPoint(Clip, transform.position, Volume);
        }

        if(AutoDisable)
            enabled = false;
    }
}
