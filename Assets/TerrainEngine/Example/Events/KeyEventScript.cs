using TerrainEngine;
using UnityEngine;

public class KeyEventScript : MonoBehaviour 
{
    public string Name;
    public KeyCode Key;
    public ActiveDisableContainer OnKey;
    public ActiveDisableContainer OnKeyDown;
    public ActiveDisableContainer OnKeyUp;

    public MonoBehaviour[] DisableIfActive;
    public bool[] LastStates;

	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(Key))
        {
            if (LastStates == null || LastStates.Length != DisableIfActive.Length)
                LastStates = new bool[DisableIfActive.Length];

            for (int i = 0; i < LastStates.Length; ++i)
            {
                if (LastStates[i] != DisableIfActive[i].enabled || DisableIfActive[i].enabled)
                {
                    LastStates[i] = DisableIfActive[i].enabled;
                    DisableIfActive[i].enabled = !DisableIfActive[i].enabled;
                }
            }

            OnKeyDown.Call();
            OnKeyDown.InverseStates();
        }
        else if (Input.GetKeyUp(Key))
        {
            OnKeyUp.Call();
            OnKeyUp.InverseStates();
        }

        if (Input.GetKey(Key))
            OnKey.Call();

        OnKey.Update();
        OnKeyDown.Update();
        OnKeyUp.Update();
	}
}
