﻿using System.Collections.Generic;
using UnityEngine;

// This Script will disable this gameobject if too many objects are close to him.
// Usefull for audio, particles, or any objects that can"t be too much in scene and too close
public class DisableTooCloseObjects : MonoBehaviour 
{
    static public Dictionary<string, List<DisableTooCloseObjects>> Objects = new Dictionary<string, List<DisableTooCloseObjects>>();

    static public bool AddObject(DisableTooCloseObjects Object)
    {
        List<DisableTooCloseObjects> L = null;
        if (!Objects.TryGetValue(Object.ContainerName, out L))
        {
            L = new List<DisableTooCloseObjects>();
            Objects.Add(Object.ContainerName, L);
        }

        for (int i = 0; i < L.Count; ++i)
        {
            if (Vector3.Distance(L[i].CTransform.position, Object.CTransform.position) < Object.MinDistanceFromOtherObject)
            {
                return false;
            }
        }

        L.Add(Object);
        return true;
    }

    static public bool RemoveObject(DisableTooCloseObjects Object)
    {
        List<DisableTooCloseObjects> L = null;
        if (Objects.TryGetValue(Object.ContainerName, out L))
        {
            if (L.Remove(Object))
            {
                if (L.Count == 0)
                    Objects.Remove(Object.ContainerName);

                return true;
            }
        }

        return false;
    }

    // Cached transform
    public Transform CTransform;
    public string ContainerName = ""; // Unique name to store this object with other objects with same name
    public float MinDistanceFromOtherObject = 5; // If any other same object is too close , this one is disable
    public AudioSource AudioToChange;
    public MonoBehaviour MonoToChange;
    public GameObject ObjToChange;
    private bool Added;

    void Awake()
    {
        Added = false;
        if(CTransform == null)
            CTransform = transform;
    }

    void OnEnable()
    {
        if (!Added)
            Added = AddObject(this);

        if (AudioToChange != null)
            AudioToChange.enabled = Added;
        if (MonoToChange != null)
            MonoToChange.enabled = Added;
        if (ObjToChange != null)
            ObjToChange.SetActive(Added);
    }

    void OnDisable()
    {
        if (Added)
        {
            RemoveObject(this);
        }

        Added = false;
    }
}
