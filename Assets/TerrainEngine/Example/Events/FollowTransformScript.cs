using UnityEngine;

public class FollowTransformScript : MonoBehaviour 
{
    public Transform ToFollow;
    public float UpdateDistance = 0;
    public bool FollowPosition = true;
    public bool FollowRotation = true;
    public bool FollowRotationX = true;
    public bool FollowRotationY = true;
    public bool FollowRotationZ = true;

    public bool FollowPositionX = true;
    public bool FollowPositionY = true;
    public bool FollowPositionZ = true;


    public bool FollowDistance = false;
    public bool LookAt = true;
    public Vector3 PositionOffset;
    public Vector3 RotationOffset;
    public float Distance = 0;
    private Transform CTransform;
    private Vector3 LastPosition;

	// Use this for initialization
	void Start () {
        CTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {

        if (ToFollow != null)
        {
            if (Vector3.Distance(LastPosition, ToFollow.transform.position) < UpdateDistance)
                return;

            LastPosition = ToFollow.transform.position;
            if (LookAt)
                CTransform.LookAt(ToFollow);

            if (FollowPosition)
            {
                Vector3 P = CTransform.position;
                Vector3 FollowP = ToFollow.position + PositionOffset;
                if (FollowPositionX) P.x = FollowP.x;
                if (FollowPositionY) P.y = FollowP.y;
                if (FollowPositionZ) P.z = FollowP.z;
                CTransform.position = P;
            }

            if (FollowRotation)
            {
                CTransform.rotation = Quaternion.Euler(
                    FollowRotationX ? ToFollow.rotation.eulerAngles.x + RotationOffset.x : CTransform.eulerAngles.x,
                    FollowRotationY ? ToFollow.rotation.eulerAngles.y + RotationOffset.y : CTransform.eulerAngles.y,
                    FollowRotationZ ? ToFollow.rotation.eulerAngles.z + RotationOffset.z : CTransform.eulerAngles.z);
            }

            if(FollowDistance)
                CTransform.position = ToFollow.transform.position - Distance * CTransform.forward;
        }
	}
}
