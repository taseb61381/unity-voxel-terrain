using UnityEngine;


public class PlaySoundEventScript : MonoBehaviour 
{
    public AudioSource Source;
    public AudioClip Audio;
    public float Volume;
    public bool AutoDisable = true;

    void OnEnable()
    {
        if (Source != null)
        {
            Source.enabled = false;
            Source.enabled = true;
            Source.clip = Source.clip;
            Source.priority = 0;
            Source.time = 0;
            Source.Play();
        }
        else
            AudioSource.PlayClipAtPoint(Audio, transform.position, Volume);

        if(AutoDisable)
            enabled = false;
    }

    void OnDisable()
    {
        if (Source != null)
            Source.enabled = false;
    }
}
