using System;
using TerrainEngine;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class DetailSelecterGUI : MonoBehaviour 
{
    static public string[] Qualities = new string[] { "Low", "Medium", "Normal", "High", "Ultra" };

    public FileMgr FMgr;
    public TerrainManager Manager;
    public GrassProcessor[] Grass;
    public bool ModifyGrassRange = true;
    public DetailProcessor[] Details;
    public bool ModifyDetailsRange = true;

    public GameObjectProcessor[] Trees;
    public FluidProcessor Fluids;
    public GameObject Camera;
    public bool IsForUsers = false;

    private Rect Box;
    private Vector2 Scroll;

    public Camera TCamera;
    int QualityID = -1;
    bool Reflection = false;

    public string IP = "0.0.0.0";
    public int Port = 8000;
    NetworkTerrainModificationInterface Network = null;

    public bool UseTerrainLimits = false;
    public Vector3 MinLimits = new Vector3();
    public Vector3 MaxLimits = new Vector3();

    public MonoBehaviour[] Systems;

    void Start()
    {
        Manager.Informations.Init();

        if (UseTerrainLimits)
        {
            MinLimits = Manager.Informations.MinRange;
            MaxLimits = Manager.Informations.MaxRange;
        }

        Array.Sort(Grass, delegate(IBlockProcessor a, IBlockProcessor b)
        {
            return a.DataName.CompareTo(b.DataName);
        });
        Array.Sort(Details, delegate(IBlockProcessor a, IBlockProcessor b)
        {
            return a.DataName.CompareTo(b.DataName);
        });
        Array.Sort(Trees, delegate(IBlockProcessor a, IBlockProcessor b)
        {
            return a.DataName.CompareTo(b.DataName);
        });
    }

    void OnGUI()
    {
        DrawMenu();
    }

    public void DrawGUI(bool DrawLOD=true)
    {
        float Width = Screen.width / 4;
        float Height = Screen.height - 80;

        GUILayout.BeginArea(new Rect(0, 0, Width, Height / 2), "Terrain", GUI.skin.box);
        {
            GUILayout.Space(20);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Terrain Seed", GUILayout.Width(Width / 3));
            Manager.Informations.Seed = GUILayout.TextField(Manager.Informations.Seed, 255);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Terrain Width: " + (int)Manager.Informations.MaxRange.x, GUILayout.Width(Width / 3));
            Manager.Informations.MaxRange.x = (int)GUILayout.HorizontalSlider((int)Manager.Informations.MaxRange.x, 0, 10);
            GUILayout.EndHorizontal();

            Manager.Informations.MinRange.x = -Manager.Informations.MaxRange.x;
            Manager.Informations.MaxRange.z = Manager.Informations.MaxRange.x;
            Manager.Informations.MinRange.z = Manager.Informations.MinRange.x;

            GUILayout.BeginHorizontal();
            GUILayout.Label("Terrain Height: " + (int)Manager.Informations.MaxRange.y, GUILayout.Width(Width / 3));
            Manager.Informations.MaxRange.y = (int)GUILayout.HorizontalSlider((int)Manager.Informations.MaxRange.y, 0, 10);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Terrain Scale: " + Manager.Informations.Scale, GUILayout.Width(Width / 3));
            Manager.Informations.Scale = GUILayout.HorizontalSlider(Manager.Informations.Scale, 0.1f, 5f);
            Manager.Informations.Scale = (int)(Manager.Informations.Scale / 0.01f) * 0.01f;
            GUILayout.EndHorizontal();

            if (DrawLOD)
            {
                if (Manager is TransvoxelManager)
                {
                    TransvoxelManager TManager = Manager as TransvoxelManager;

                    int NodeSize = (int)Manager.Informations.VoxelPerChunkXYZ;
                    int TreeSize = (int)Manager.Informations.VoxelPerChunkXYZ * (int)Manager.Informations.ChunkPerBlockXYZ;
                    TManager.TransvoxelInfo.NodeLevels.SetMaxLevel(TreeSize, NodeSize);

                    TManager.TransvoxelInfo.NodeLevels.GetDistances(TreeSize, NodeSize, (int Level, float Distance, int Power) =>
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Lod:" + Level, GUILayout.Width(50));
                        int NewPower = (int)GUILayout.HorizontalSlider(Power, 0, 10);

                        GUILayout.Label("Meters:" + Distance * Manager.Informations.Scale + " (" + Power + ")");
                        GUILayout.EndHorizontal();

                        return NewPower;
                    });
                }
            }

            if(UseTerrainLimits)
            {
                Vector3 MinRange = Manager.Informations.MinRange;
                Vector3 MaxRange = Manager.Informations.MaxRange;

                if (MinRange.x < MinLimits.x) MinRange.x = MinLimits.x;
                if (MinRange.y < MinLimits.y) MinRange.y = MinLimits.y;
                if (MinRange.z < MinLimits.z) MinRange.z = MinLimits.z;

                if (MaxRange.x > MaxLimits.x) MaxRange.x = MaxLimits.x;
                if (MaxRange.y > MaxLimits.y) MaxRange.y = MaxLimits.y;
                if (MaxRange.z > MaxLimits.z) MaxRange.z = MaxLimits.z;

                Manager.Informations.MinRange = MinRange;
                Manager.Informations.MaxRange = MaxRange;

            }
        }
        GUILayout.EndArea();

        GUILayout.BeginArea(new Rect(0, Height / 2, Width, Height / 2), "Graphics", GUI.skin.box);
        {
            GUILayout.Space(20);

            GUILayout.BeginVertical("Systems",GUI.skin.box);
            {
                if (Systems != null)
                {
                    for (int i = 0; i < Systems.Length; ++i)
                    {
                        MonoBehaviour Obj = Systems[i];
                        if (Obj != null)
                        Systems[i].enabled = GUILayout.Toggle(Systems[i].enabled,Obj.GetType().Name);
                    }
                }
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndArea();

        GUILayout.BeginArea(new Rect(Width, 0, Width, Height), "Grass", GUI.skin.box);
        {
            GUILayout.Space(20);

            byte State = 2;
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Enable")) State = 1;
            if (GUILayout.Button("Disable")) State = 0;
            GUILayout.EndHorizontal();

            for (int i = 0; i < Grass.Length; ++i)
            {
                if (Grass[i] == null)
                    continue;

                if (State == 0) Grass[i].enabled = false;
                else if (State == 1) Grass[i].enabled = true;

                GUILayout.BeginVertical(Grass[i].DataName, GUI.skin.box);
                {
                    GUILayout.Space(20);
                    Grass[i].enabled = GUILayout.Toggle(Grass[i].enabled, "Active");

                    if (Grass[i].enabled)
                    {
                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label("Density:" + Grass[i].Density.x, GUILayout.Width(Width / 3));
                            Grass[i].Density.x = GUILayout.HorizontalSlider(Grass[i].Density.x, 0, 100);
                        }
                        GUILayout.EndHorizontal();

                        Grass[i].CastShadow = GUILayout.Toggle(Grass[i].CastShadow, "Cast Shadow");

                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label("View Distance:" + Grass[i].ViewDistance, GUILayout.Width(Width / 3));
                            Grass[i].ViewDistance = GUILayout.HorizontalSlider(Grass[i].ViewDistance, 0f, 1000f);
                        }
                        GUILayout.EndHorizontal();
                    }
                }
                GUILayout.EndVertical();
            }
        }
        GUILayout.EndArea();

        GUILayout.BeginArea(new Rect(Width*2, 0, Width, Height), "Details", GUI.skin.box);
        {
            GUILayout.Space(20);

            byte State = 2;
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Enable")) State = 1;
            if (GUILayout.Button("Disable")) State = 0;
            GUILayout.EndHorizontal();

            for (int i = 0; i < Details.Length; ++i)
            {
                if (Details[i] == null)
                    continue;

                if (State == 0) Details[i].enabled = false;
                else if (State == 1) Details[i].enabled = true;

                GUILayout.BeginVertical(Details[i].DataName, GUI.skin.box);
                {
                    GUILayout.Space(20);
                    Details[i].enabled = GUILayout.Toggle(Details[i].enabled, "Active");
                    if (Details[i].enabled)
                    {
                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label("Density:" + Details[i].Density.x, GUILayout.Width(Width / 3));
                            Details[i].Density.x = GUILayout.HorizontalSlider(Details[i].Density.x, 0, 100);
                        }
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        {
                            Details[i].CastShadow = GUILayout.Toggle(Details[i].CastShadow, "Cast Shadow");
                            Details[i].ReceiveShadow = GUILayout.Toggle(Details[i].ReceiveShadow, "Receive Shadow");
                        }
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label("View Distance:" + Details[i].ViewDistance, GUILayout.Width(Width / 3));
                            Details[i].ViewDistance = GUILayout.HorizontalSlider(Details[i].ViewDistance, 0, 1000);
                        }
                        GUILayout.EndHorizontal();
                    }
                }
                GUILayout.EndVertical();
            }
        }
        GUILayout.EndArea();

        GUILayout.BeginArea(new Rect(Width * 3, 0, Width, Height), "Trees", GUI.skin.box);
        {
            GUILayout.Space(20);

            byte State = 2;
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Enable")) State = 1;
            if (GUILayout.Button("Disable")) State = 0;
            GUILayout.EndHorizontal();

            for (int i = 0; i < Trees.Length; ++i)
            {
                if (Trees[i] == null)
                    continue;

                if (State == 0) Trees[i].enabled = false;
                else if (State == 1) Trees[i].enabled = true;

                GUILayout.BeginVertical(Trees[i].DataName, GUI.skin.box);
                {
                    GUILayout.Space(20);
                    Trees[i].enabled = GUILayout.Toggle(Trees[i].enabled, "Active");

                    if (Trees[i].enabled)
                    {
                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label("Density:" + Trees[i].Density.x, GUILayout.Width(Width / 3));
                            Trees[i].Density.x = GUILayout.HorizontalSlider(Trees[i].Density.x, 0, 100);
                        }
                        GUILayout.EndHorizontal();
                    }
                }
                GUILayout.EndVertical();
            }
        }
        GUILayout.EndArea();
    }

    public void SetQuality(int Level)
    {
        QualityID = Level;
        if (Level > 3)
            Level = 3;

        SetReflection(false);

        if (TerrainManager.Instance == null)
            TerrainManager.Instance = FindObjectOfType<TerrainManager>();

        if(TerrainManager.Instance != null)
            TerrainManager.Instance.Informations.MaxRange.x = Level + 1;


        for (int i = 0; i < Trees.Length; ++i)
            if (Trees[i] != null)
                Trees[i].Density.x = (QualityID + 1) * 2.6f - (QualityID > 2 ? 1f : 0);


        for (int i = 0; i < Grass.Length; ++i)
        {
            if (Grass[i] == null)
                continue;

            if(ModifyGrassRange)
                Grass[i].ViewDistance = (QualityID+1) * 35f;
            Grass[i].Density.x = 25f * QualityID;
            if (Grass[i].Density.x < 40f)
                Grass[i].Density.x = 40f;

            if (QualityID == 4) // Ultra
            {
                Grass[i].CastShadow = true;
            }
            else
                Grass[i].CastShadow = false;
        }

        for (int i = 0; i < Details.Length; ++i)
        {
            if (Details[i] == null)
                continue;

            Details[i].Density.x = (QualityID + 2) * 3;

            if(ModifyDetailsRange)
                Details[i].ViewDistance = (QualityID+1) * 35f;
            if (QualityID == 4)
                Details[i].Density.x += 3f;

            if (QualityID > 1) // Normal
            {
                Details[i].ReceiveShadow = true;
                if (QualityID > 2) // High
                    Details[i].CastShadow = true;
            }
            else
            {
                Details[i].ReceiveShadow = false;
                Details[i].CastShadow = false;
            }
        }
    }

    private UnityStandardAssets.Water.Water W;
    private UnityStandardAssets.Water.WaterBase WBase;
    public void SetReflection(bool Value)
    {
        Reflection = Value;

        if(W == null)
            W = FindObjectOfType<UnityStandardAssets.Water.Water>();

        if(WBase == null)
            WBase = FindObjectOfType<UnityStandardAssets.Water.WaterBase>();
        MarchingPalette Palette = FindObjectOfType<MarchingPalette>();

        if (Palette == null || W == null || WBase == null)
            return;

        W.gameObject.SetActive(Value);
        WBase.gameObject.SetActive(!Value);

        foreach (MarchingInformation Info in Palette.VoxelBlocks)
        {
            if (Info.Name == "Water")
            {
                Info.Material = Value ? W.GetComponent<MeshRenderer>().sharedMaterial : WBase.sharedMaterial;
            }
        }
    }

    public int MenuId = 0;

    public void DrawMenu()
    {
        Box = new Rect(Screen.width / 4, Screen.height - 80, Screen.width / 2, 80);
        Rect LeftButton = new Rect(Box.x + 5, Box.y, Box.width / 2 - 5, 60f);
        Rect RightButton = new Rect(Box.x + 5 + Box.width / 2, Box.y, Box.width / 2 - 5, 60f);

        if (MenuId == 0)
        {
            float Left = (Screen.width / 3);
            float Top = (Screen.height / 8);
            if (GUI.Button(new Rect(Left, Top, Left, Top), "SinglePlayer"))
                MenuId = 1;

            if (GUI.Button(new Rect(Left, Top * 2, Left, Top), "MultiPlayer"))
                MenuId = 2;

            if (GUI.Button(new Rect(Left, Top * 3, Left, Top), "Dedicated Server : Console"))
                MenuId = 3;

            if (GUI.Button(new Rect(Left, Top * 4, Left, Top), "Client as Server : Colliders"))
                MenuId = 4;
        }
        else if(MenuId == 1)
        {
            DrawGUI();

            GUI.Box(Box, "");
            {
                int Selected = GUI.SelectionGrid(new Rect(Box.x + 5, Box.y-20, Box.width - 10, 20), QualityID < 0 ? 1 : QualityID, Qualities, 5);

                if (Selected != QualityID)
                    SetQuality(Selected);

                Box.y += 20;

                if (GUI.Button(LeftButton, "Back"))
                {
                    MenuId = 0;
                    return;
                }

                if (GUI.Button(RightButton, "Explore"))
                {
                    enabled = false;
                    FMgr.gameObject.SetActive(true);
                }
            }
        }
        else if(MenuId == 2)
        {
            DrawGUI();

            GUI.Box(Box, "");
            {
                float MidWidth = Box.width / 2f;
                this.IP = GUI.TextField(new Rect(Box.x + 5 + MidWidth - 5, Box.y, MidWidth / 2 - 5, 30f), this.IP);
                string Port = GUI.TextField(new Rect(Box.x + 5 + MidWidth - 5 + MidWidth / 2, Box.y, MidWidth / 2 - 5, 30f), this.Port.ToString());
                int.TryParse(Port, out this.Port);

                if (GUI.Button(LeftButton, "Back"))
                {
                    MenuId = 0;
                    return;
                }

                if (GUI.Button(new Rect(Box.x + 5 + Box.width / 2 - 5, Box.y + 30f, Box.width / 4 - 5, 30f), "Connect"))
                {
                    LocalTerrainObjectServerScript LocalObject = FindObjectOfType<LocalTerrainObjectServerScript>();
                    Network = new GameObject().AddComponent<NetworkTerrainModificationInterface>();
                    Network.CTransform = LocalObject.CTransform;
                    Network.FixedPositions = LocalObject.FixedPositions;
                    Network.IP = this.IP;
                    Network.Port = this.Port;

                    TerrainManager Manager = FindObjectOfType<TerrainManager>();
                    Manager.ModificationInterface = Network;

                    enabled = false;
                    FMgr.gameObject.SetActive(true);
                    GameObject.Destroy(FindObjectOfType<LocalTerrainObjectServerScript>());
                }

                if (GUI.Button(new Rect(Box.x + 5 + Box.width / 2 - 5 + Box.width / 4, Box.y + 30f, Box.width / 4 - 5, 30f), "Host"))
                {
                    TerrainManager Manager = FindObjectOfType<TerrainManager>();

                    HostTerrainModificationInterface Host = Manager.gameObject.AddComponent<HostTerrainModificationInterface>();
                    Host.LocalObject = FindObjectOfType<LocalTerrainObjectServerScript>();
                    Host.IP = this.IP;
                    Host.Port = this.Port;

                    Manager.ModificationInterface = Host;

                    enabled = false;
                    FMgr.gameObject.SetActive(true);
                }
            }
        }
        else if(MenuId == 3)
        {
            TerrainServerGUI ServerGUI = FindObjectOfType<TerrainServerGUI>();
            if (ServerGUI == null)
            {
                ServerGUI = gameObject.AddComponent<TerrainServerGUI>();
                TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
                if (Manager != null)
                {
                    WorldGenerator Gen = Manager.InitGenerators(true); // Initing Generator class before serialization

                    Debug.Log("Exporting Biomes : " + Manager + ",Generators=" + Gen.Generators.Count + ",Files=" + Gen.GeneratorsFiles.Count);
                    ServerGUI.BiomesFolder = Gen.ExportToXML(); // Open folder
                }
                else
                    Debug.Log("TerrainManager not found. Create or Enable a Terrain");
            }

            if (ServerGUI.Server == null && GUI.Button(LeftButton, "Back"))
            {
                GameObject.Destroy(ServerGUI);
                MenuId = 0;
                return;
            }
        }
        else if(MenuId == 4)
        {
            DrawGUI(false);
            GUI.Box(Box, "");
            {
                float MidWidth = Box.width / 2f;
                this.IP = GUI.TextField(new Rect(Box.x + 5 + MidWidth - 5, Box.y, MidWidth / 2 - 5, 30f), this.IP);
                string Port = GUI.TextField(new Rect(Box.x + 5 + MidWidth - 5 + MidWidth / 2, Box.y, MidWidth / 2 - 5, 30f), this.Port.ToString());
                int.TryParse(Port, out this.Port);

                if (GUI.Button(LeftButton, "Back"))
                {
                    MenuId = 0;
                    return;
                }

                if (GUI.Button(new Rect(Box.x + 5 + Box.width / 2 - 5, Box.y + 30f, Box.width / 2 - 5, 30f), "Host"))
                {
                    TerrainManager Manager = FindObjectOfType<TerrainManager>();

                    GameObject Obj = new GameObject();
                    Obj.transform.SetParent(Manager.transform);
                    ClientAsServerScript ClientAsServer = Obj.AddComponent<ClientAsServerScript>();
                    ClientAsServer.Init(Manager, this.IP, this.Port);

                    enabled = false;
                    FMgr.gameObject.SetActive(true);
                }
            }
        }
    }
}
