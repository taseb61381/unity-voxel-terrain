﻿using UnityEngine;

public class FpsGUI : MonoBehaviour
{
    static public float fps;

    public float updateInterval = 0.5F;
    public float updateLagInterval = 0.1f;

    private float lastInterval;
    private float laglastInterval;

    private int frames = 0;

    // Use this for initialization
    void Start()
    {
        lastInterval = Time.realtimeSinceStartup;
        frames = 0;
    }

    void OnGUI()
    {
        if (!DebugGUIProcessor.DrawGUI)
            return;

        Rect rect = new Rect(Screen.width - 100, Screen.height - 20, 100, 20);
        GUI.Box(rect, "");

        if (fps < 30)
            GUI.contentColor = Color.yellow;
        else
            if (fps < 10)
                GUI.contentColor = Color.red;
            else
                GUI.contentColor = Color.green;

        GUI.Label(rect, fps.ToString("f2") + " FPS");
    }

    // Update is called once per frame
    void Update()
    {
        ++frames;

        float timeNow = Time.realtimeSinceStartup;
        if (timeNow > lastInterval + updateInterval)
        {
            fps = (frames / (timeNow - lastInterval));
            frames = 0;
            lastInterval = timeNow;
        }
    }
}
