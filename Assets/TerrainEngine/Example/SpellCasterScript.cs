using UnityEngine;

namespace TerrainEngine
{
    public class SpellCasterScript : MonoBehaviour
    {
        public GameObject[] Spells;
        public int CurrentSpell = 0;
        public Transform Target;

        void Start()
        {
            if (Target == null)
                Target = transform;
        }

        void Update()
        {
            float Wheel = Input.GetAxis("Mouse ScrollWheel");
            if (Wheel > 0)
                NextSpell();
            else if (Wheel < 0)
                PreviousSpell();

            if (InputManager.GetMouseButtonDown(1))
            {
                GameObject Obj = GetSpell();
                if (Obj == null)
                    return;

                Obj = GameObject.Instantiate(Obj, transform.position, Target.rotation) as GameObject;
                SphereModificator SM = Obj.AddComponent<SphereModificator>();
                SM.Volume = 0.2f;
                SM.Type = 0;
                SM.ModType = VoxelModificationType.REMOVE_VOLUME;
                SM.Size = 2;
                SM.StartDelay = 0.2f;
            }
        }

        public void Cast()
        {

        }

        public GameObject GetSpell()
        {
            if (CurrentSpell >= Spells.Length)
                return null;

            return Spells[CurrentSpell];
        }

        public void NextSpell()
        {
            ++CurrentSpell;
            if (CurrentSpell >= Spells.Length)
                CurrentSpell = 0;
        }

        public void PreviousSpell()
        {
            --CurrentSpell;
            if (CurrentSpell < 0)
                CurrentSpell = Spells.Length - 1;
        }
    }

}