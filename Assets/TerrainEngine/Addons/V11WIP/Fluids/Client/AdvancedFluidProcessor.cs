﻿using UnityEngine;
using System;
using System.Collections;

using TerrainEngine;

public class AdvancedFluidProcessor : IBlockProcessor
{
    public bool GenerateOcean = true;
    public float OceanLevel = 0;

    public string BiomeName = "River";
    public string FluidName = "Water";
    public AdvancedFluidGenerator.CustomBox[] Boxes;
    public int ScriptHashCode = 0;
    public GameObject DefaultMeshObject;
    public float UVScale = 1f;

    private int InternalIndex = 0;

    public override void OnInitManager(TerrainManager Manager)
    {
        base.OnInitManager(Manager);
        InternalDataName = "Fluid-" + DataName;
    }

    public PoolInfoRef RefInfo;

    public override void OnTerrainLODUpdate(TerrainChunkGenerateAndBuildGroup Group, int Level)
    {
        //Debug.Log("OnTerrainLODUpdate : " + Group.Chunks.Length + ",Level:" + Level);

        if (RefInfo.Index == 0)
            RefInfo = PoolManager.Pool.GetPool("FluidGenerateAndBuildEvent");

        FluidGenerateAndBuildEvent Event = RefInfo.GetData<FluidGenerateAndBuildEvent>();
        Event.Processor = this;
        Event.InternalIndex = InternalIndex;
        Group.Event = Event;
    }

    public override void OnBlockDataCreate(TerrainDatas Data)
    {
        AdvancedFluidData FData = Data.GetCustomData<AdvancedFluidData>(InternalDataName);
        if (FData == null)
        {
            FData = new AdvancedFluidData();
            Data.RegisterCustomData(InternalDataName, FData);
            InternalIndex = Data.GetCustomDataIndex(InternalDataName);
            ScriptHashCode = InternalDataName.GetHashCode();
        }
        InternalIndex = Data.GetCustomDataIndex(InternalDataName);


        if (TerrainManager.Instance.Builder.BlockBuilder.Event == null)
        {
            FluidGenerateAndBuildEvent Event = new FluidGenerateAndBuildEvent();
            Event.Processor = this;
            Event.InternalIndex = InternalIndex;
            TerrainManager.Instance.Builder.BlockBuilder.Event = Event;
        }
    }

    public override IWorldGenerator.GeneratorTypes GetGeneratorType()
    {
        return IWorldGenerator.GeneratorTypes.TERRAIN;
    }

    public override IWorldGenerator GetGenerator()
    {
        InternalDataName = "Fluid-" + DataName;

        AdvancedFluidGenerator Gen = new AdvancedFluidGenerator();
        Gen.GenerateOcean = GenerateOcean;
        Gen.OceanLevel = OceanLevel;
        Gen.FluidName = FluidName;
        Gen.Boxes = Boxes;
        Gen.ScriptHashCode = ScriptHashCode;
        Gen.Name = InternalName;
        Gen.DataName = InternalDataName;

        return Gen;

    }
}
