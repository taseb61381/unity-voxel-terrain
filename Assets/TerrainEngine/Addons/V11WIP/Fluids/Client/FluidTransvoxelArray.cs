﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TerrainEngine
{
    public class FluidTransvoxelArray : ITransvoxelArray
    {
        public int InternalIndex;

        public override void Clear()
        {
            base.Clear();
        }

        public override void LoadVoxels(TerrainBlock Block, int m_lod, int StartX, int StartY, int StartZ)
        {
            int m_voxelSkip = (int)Math.Pow(2.0, (double)m_lod);
            int DataCount = TerrainManager.Instance.Palette.DataCount;

            if (ActiveSubMesh == null || ActiveSubMesh.Length != DataCount)
                ActiveSubMesh = new bool[DataCount];

            for (DataCount = DataCount - 1; DataCount >= 0; --DataCount)
                ActiveSubMesh[DataCount] = false;

            IsValidChunk = false;
            bool LocalIsValidChunk = false;
            int LocalCount = 0;

            Count = 0;
            MaterialCount = 0;
            int indX, indY, indZ, Current = 0,
                px, py, pz, x, y, z;
            bool sx, sy, vx = false, vy = false, IsInChunk = false;
            byte LastType = 0;
            float Isolevel = TerrainInformation.Isolevel;

            MarchingInformation LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(0);

            int Index = 0,
                End = TransvoxelInfo.voxelLength + 2,
                InsideEnd = TransvoxelInfo.voxelLength + 1;
                //ChunkX, ChunkY, IndexX, IndexY;

            TerrainInformation Information = Block.Information;
            TerrainBlock AroundBlock = null;
            byte VType = 0;
            float Volume = 0;

            int FluidCustomIndex = InternalIndex;
            AdvancedFluidData FData = Block.Voxels.Customs.array[FluidCustomIndex] as AdvancedFluidData;
            AdvancedFluidData AData = null;

            int VoxelPerChunk = Information.VoxelPerChunk;
            int ChunkPerBlock = Information.ChunkPerBlock;
            int VoxelsPerAxis = Information.VoxelsPerAxis;
            int vcountlod = TransvoxelInfo.vcountlod;
            int vlenghtlod = TransvoxelInfo.vlenghtlod;
            byte LastRegisteredType = 0;

            for (indX = -1; indX < End; ++indX)
            {
                px = (StartX + indX * m_voxelSkip);
                sx = px >= 0 && px < VoxelsPerAxis; // Position is inside Block
                vx = indX >= 0 && indX < InsideEnd; // Position is inside current chunk
                //ChunkX = (px / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock;
                //IndexX = (px % VoxelPerChunk) * VoxelPerChunk * VoxelPerChunk;

                for (indY = -1; indY < End; ++indY)
                {
                    py = (StartY + indY * m_voxelSkip);
                    sy = py >= 0 && py < VoxelsPerAxis; // Position is inside Block
                    vy = indY >= 0 && indY < InsideEnd; // Position is inside current chunk
                    //ChunkY = (py / VoxelPerChunk) * ChunkPerBlock;
                    //IndexY = (py % VoxelPerChunk) * VoxelPerChunk;

                    for (indZ = -1; indZ < End; ++indZ, ++Index)
                    {
                        pz = (StartZ + indZ * m_voxelSkip);

                        IsInChunk = vx && vy && indZ >= 0 && indZ < InsideEnd;  // Position is inside current chunk

                        //Block.GetByteAndFloat(px, py, pz, ref VType, ref Volume);

                        if ((sx && sy && (pz >= 0 && pz < VoxelsPerAxis)))
                        {
                            FData.GetTypeAndVolume(px, py, pz, ref VType, ref Volume);
                        }
                        else
                        {
                            x = px;
                            y = py;
                            z = pz;
                            AroundBlock = Block.GetAroundBlock(ref x, ref y, ref z) as TerrainBlock;
                            if (AroundBlock == null)
                            {
                                VType = 0;
                                Volume = 0;
                            }
                            else
                            {
                                AData = AroundBlock.Voxels.Customs.array[FluidCustomIndex] as AdvancedFluidData;
                                AData.GetTypeAndVolume(x, y, z, ref VType, ref Volume);
                            }
                        }

                        if (VType != 0)
                        {
                            if (VType != LastType)
                            {
                                LastType = VType;
                                LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(VType);
                            }

                            if (LastVoxelType.UseSplatMap && LastVoxelType.MainVoxelId != 0)
                            {
                                VType = LastVoxelType.MainVoxelId;
                            }

                            if (LastVoxelType.VoxelType != VoxelTypes.FLUID)
                            {
                                VType = 0;
                                Volume = 0;
                                voxels[Index].VType = 0;
                            }
                            else
                            {
                                if (IsInChunk && !LocalIsValidChunk)
                                {
                                    if (Volume >= Isolevel)
                                    {
                                        LocalIsValidChunk = LastVoxelType.VoxelType == VoxelTypes.TRANSLUCENT;
                                        ++LocalCount;
                                    }
                                }

                                if (IsInChunk && LastRegisteredType != VType && ActiveSubMesh[VType] == false)
                                {
                                    LastRegisteredType = VType;
                                    ActiveSubMesh[VType] = true;
                                    ++MaterialCount;
                                }

                                voxels[Index].VType = VType;
                                voxels[Index].Volume = Volume;
                            }
                        }
                        else
                        {
                            Volume = 0;
                            voxels[Index].VType = 0;
                        }


                        if (IsInChunk && !LocalIsValidChunk)
                        {
                            ++Current;

                            if (LocalCount != 0 && LocalCount != Current)
                                LocalIsValidChunk = true;
                        }
                    }
                }
            }

            IsValidChunk = LocalIsValidChunk;
            Count = LocalCount;

            if (IsValidChunk && m_lod > 0)
            {
                int Skip = (m_voxelSkip / 2);
                VectorI3 start, end;

                for (int lod = 0; lod < 6; ++lod)
                {
                    start = TransvoxelInfo.toIterate[lod][0];
                    end = TransvoxelInfo.toIterate[lod][1];
                    LastType = 0;
                    LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(0);

                    for (indX = start.x; indX < end.x; ++indX)
                    {
                        px = StartX + indX * Skip;
                        sx = px >= 0 && px < VoxelsPerAxis; // Position is inside Block
                        vx = indX >= 0 && indX < InsideEnd; // Position is inside current chunk
                        //ChunkX = (px / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock;
                        //IndexX = (px % VoxelPerChunk) * VoxelPerChunk * VoxelPerChunk;

                        for (indY = start.y; indY < end.y; ++indY)
                        {
                            py = StartY + indY * Skip;
                            sy = py >= 0 && py < VoxelsPerAxis; // Position is inside Block
                            vy = indY >= 0 && indY < InsideEnd; // Position is inside current chunk
                            //ChunkY = (py / VoxelPerChunk) * ChunkPerBlock;
                            //IndexY = (py % VoxelPerChunk) * VoxelPerChunk;

                            for (indZ = start.z; indZ < end.z; ++indZ)
                            {
                                pz = StartZ + indZ * Skip;
                                IsInChunk = vx && vy && indZ >= 0 && indZ < InsideEnd;  // Position is inside current chunk

                                Index = GetLodInterpolationIndex(indX - start.x, indY - start.y, indZ - start.z, lod, vcountlod, vlenghtlod);

                                if ((sx && sy && (pz >= 0 && pz < VoxelsPerAxis)))
                                {
                                    FData.GetTypeAndVolume(px, py, pz, ref VType, ref Volume);
                                }
                                else
                                {
                                    x = px;
                                    y = py;
                                    z = pz;
                                    AroundBlock = Block.GetAroundBlock(ref x, ref y, ref z) as TerrainBlock;
                                    if (AroundBlock == null)
                                    {
                                        VType = 0;
                                        Volume = 0;
                                    }
                                    else
                                    {
                                        AData = AroundBlock.Voxels.Customs.array[FluidCustomIndex] as AdvancedFluidData;
                                        AData.GetTypeAndVolume(x, y, z, ref VType, ref Volume);
                                    }
                                }

                                if (VType != 0)
                                {
                                    if (VType != LastType)
                                    {
                                        LastType = VType;
                                        LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(VType);
                                    }

                                    if (LastVoxelType.UseSplatMap && LastVoxelType.MainVoxelId != 0)
                                    {
                                        voxelslod[Index].VOriginalType = VType;
                                        VType = LastVoxelType.MainVoxelId;
                                    }
                                    else
                                        voxelslod[Index].VOriginalType = VType;

                                    if (LastVoxelType.VoxelType != VoxelTypes.FLUID)
                                    {
                                        Volume = 0;
                                        VType = 0;
                                        voxelslod[Index].VType = 0;
                                    }
                                    else
                                    {
                                        if (IsInChunk && LastRegisteredType != VType && ActiveSubMesh[VType] == false)
                                        {
                                            LastRegisteredType = VType;
                                            ActiveSubMesh[VType] = true;
                                            ++MaterialCount;
                                        }

                                        voxelslod[Index].VType = VType;
                                        voxelslod[Index].Volume = Volume;
                                    }
                                }
                                else
                                {
                                    Volume = 0;
                                    voxelslod[Index].VType = 0;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
