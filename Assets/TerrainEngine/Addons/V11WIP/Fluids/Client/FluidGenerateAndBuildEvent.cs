﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TerrainEngine
{
    public class FluidGenerateAndBuildEvent : GenerateAndBuildGroupEvent
    {
        static public PoolInfoRef PoolRef;

        public AdvancedFluidProcessor Processor;
        public SList<ChunkGenerateInfo> Infos = new SList<ChunkGenerateInfo>(10);
        public int InternalIndex;

        public int GetInfoIndex(ushort BlockId, int ChunkId)
        {
            for (int i = Infos.Count - 1; i >= 0; --i)
                if (Infos.array[i].ChunkRef.ChunkId == ChunkId && Infos.array[i].ChunkRef.BlockId == BlockId)
                    return i;

            return -1;
        }


        public override void OnCreateGenerateAction(TerrainChunkGenerateAndBuildGroup Current, TerrainChunkGenerateAction Action)
        {
            //Debug.Log("OnCreateGenerateAction");
            Action.Event = this;
        }

        public override void OnCreateBuildGroupAction(TerrainChunkGenerateAndBuildGroup Current, TerrainChunkBuildGroupAction Action)
        {
            //Debug.Log("OnCreateBuildGroupAction");
            Action.Event = this;

        }

        public override void OnCloseActions(TerrainChunkGenerateAndBuildGroup Current, TerrainChunkGenerateAndBuildGroup Action)
        {

        }

        public override void OnGenerateChunk(ActionThread Thread,TerrainBlock Block,int ChunkId)
        {
            if(PoolRef.Index == 0)
            {
                PoolRef = Thread.Pool.GetPool("FluidTransvoxelArray");
            }

            TransvoxelBlock TBlock = (Block as TransvoxelBlock);
            int Parent = TBlock.NodesArray.Array[TBlock.ChunkNodes[ChunkId]].GetOpenParent(TBlock.NodesArray);

            if (TBlock.NodesArray.Array[Parent].Data == ChunkId)
            {
                TempMeshData Info = null;
                IMeshData Data = null;

                GenerateNode(Thread, TBlock, ref Info, ref Data, Parent);

                if (Info != null)
                    Info.Close();

                if (Data != null && !Data.IsValid())
                {
                    Data.Close();
                    Data = null;
                }

                if (Data != null)
                {
                    ChunkGenerateInfo CInfo = new ChunkGenerateInfo();
                    CInfo.ChunkRef = new TerrainChunkRef(Block, ChunkId);
                    CInfo.Data = Data;
                    Infos.Add(CInfo);

                    if (Data.IsValid())
                    {
                        Vector3 WorldPosition = TBlock.Chunks[ChunkId].GetWorldPosition(TBlock);

                        Vector2[] UV = new Vector2[Data.Vertices.Length];
                        Vector2 uv = new Vector2();
                        Vector3 v = new Vector3();

                        for (int i = Data.Vertices.Length - 1; i >= 0; --i)
                        {
                            v = Data.Vertices[i];
                            v.x += WorldPosition.x;
                            v.y += WorldPosition.y;
                            v.z += WorldPosition.z;

                            uv.x = v.x + v.y;
                            uv.y = v.y + v.z;
                            UV[i] = uv * Processor.UVScale;
                        }

                        Data.GenerateTangents = true;
                        Data.SetUVs(UV);
                    }
                }
            }
        }

        public void GenerateNode(ActionThread Thread, TransvoxelBlock Block, ref TempMeshData Info, ref IMeshData MData, int NodeIndex)
        {
            int ChunkId = Block.NodesArray.Array[NodeIndex].Data;
            int m_lod = Block.NodesArray.MaxLevel - Block.NodesArray.Array[NodeIndex].m_level;

            int LocalX = Block.Chunks[ChunkId].LocalX;
            int LocalY = Block.Chunks[ChunkId].LocalY;
            int LocalZ = Block.Chunks[ChunkId].LocalZ;

            if (ThreadManager.EnableStats)
                Thread.RecordTime();

            FluidTransvoxelArray VoxelArray = PoolRef.GetData<FluidTransvoxelArray>();
            
            TransvoxelManager Data = TransvoxelManager.Instance as TransvoxelManager;
            VoxelArray.InternalIndex = InternalIndex;
            VoxelArray.TransvoxelInfo = Data.TransvoxelInfo;
            VoxelArray.GenerateUVs = true;
            VoxelArray.Init(Block as TransvoxelBlock, m_lod, LocalX * Data.Informations.VoxelPerChunk, LocalY * Data.Informations.VoxelPerChunk, LocalZ * Data.Informations.VoxelPerChunk, Data.TransvoxelInfo.voxelLength);

            if (ThreadManager.EnableStats)
                Thread.AddStats("FluidNodeInit", Thread.Time(), false);

            Block.GenerateNode(Thread, ref Info, ref MData, m_lod, ChunkId, VoxelArray);
            VoxelArray.Close();
        }

        public override void OnBuildCheckGameObject(ActionThread Thread, TerrainBlock Block, int ChunkId)
        {

        }

        public override bool OnTerrainBuilded(ActionThread Thread, TerrainBlock Block)
        {
            if (Infos.Count == 0)
                return true;

            TerrainBlock AroundBlock = null;
            AdvancedFluidData Data;


            for (int CurrentIndex = Infos.Count - 1; CurrentIndex >= 0; --CurrentIndex)
            {
                if (Infos.array[CurrentIndex].ChunkRef.IsValid(ref AroundBlock))
                {
                    Data = AroundBlock.Voxels.Customs.array[InternalIndex] as AdvancedFluidData;
                    Data.Build(Processor, AroundBlock as TransvoxelBlock, Infos.array[CurrentIndex].ChunkRef.ChunkId, Infos.array[CurrentIndex].Data, false);
                }
            }

           Clear();
           return true;
        }

        public override void OnBuildChunk(ActionThread Thread, TerrainBlock Block,int ChunkId)
        {
            AdvancedFluidData Data = Block.Voxels.Customs.array[InternalIndex] as AdvancedFluidData;
            int Index = GetInfoIndex(Block.BlockId, ChunkId);

            Data.Build(Processor, Block as TransvoxelBlock, ChunkId, Index == -1 ? null : Infos.array[Index].Data);
        }

        public override bool CanStartToBuild()
        {
            return base.CanStartToBuild();
        }

        public override bool CanFinish()
        {
            return base.CanFinish();
        }

        public override void Clear()
        {
            Infos.Clear();
        }
    }
}
