﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace TerrainEngine
{
    /*public class FluidContainer
    {
        public int ChunkId;
        public GameObject Obj;
        public Transform Tr;
        public MeshFilter CFilter;
        public MeshRenderer CRenderer;
    }*/

    public class AdvancedFluidData : ICustomTerrainData
    {
        public byte[] Types; // Type of fluid at given X/Z position
        public ushort[] Sizes; // World Level of the fluid
        public ushort[] Heights; // Height of the fluid

        public double RiverHeight = 0;
        public double RiverCount = 0;

        public override void Init(TerrainDatas Terrain)
        {
            base.Init(Terrain);

            if (Types == null)
            {
                Types = new byte[Terrain.Information.VoxelsPerAxis * Terrain.Information.VoxelsPerAxis];
                Sizes = new ushort[Terrain.Information.VoxelsPerAxis * Terrain.Information.VoxelsPerAxis];
                Heights = new ushort[Terrain.Information.VoxelsPerAxis * Terrain.Information.VoxelsPerAxis];
            }
        }

        public void SetFluid(int x,int z, byte Type,float Height,float Size)
        {
            Height -= Terrain.WorldY;
            if (Size<=0)
            {
                return;
            }

            x = x * Information.VoxelsPerAxis + z;
            Heights[x] = (ushort)(Height / TerrainDatas.HeightScale);
            Sizes[x] = (ushort)(Size / TerrainDatas.HeightScale);
            Types[x] = Type;
            Dirty = true;
        }

        public void SetFluid(int Index, byte Type, float Height, float Size)
        {
            Height -= Terrain.WorldY;
            if (Size <= 0)
            {
                return;
            }

            Heights[Index] = (ushort)(Height / TerrainDatas.HeightScale);
            Sizes[Index] = (ushort)(Size / TerrainDatas.HeightScale);
            Types[Index] = Type;
            Dirty = true;
        }

        public byte GetTypeAndVolume(int x,int y,int z,ref byte Type,ref float Volume)
        {
            x = x * Information.VoxelsPerAxis + z;
            z = Sizes[x];
            if (z == 0)
            {
                Type = 0;
                Volume = 0;
                return 0;
            }

            Volume = (float)Heights[x] * TerrainDatas.HeightScale;

            if (y < Volume)
            {
                Type = 0;
                Volume = 0;
                return 0;
            }

            Type = Types[x];
            Volume = (float)(Volume + ((float)z * TerrainDatas.HeightScale)) - y;

            if (Volume < TerrainDatas.MinVolume)
            {
                Volume = TerrainDatas.MinVolume;
                Type = 0;
                return 0;
            }
            if (Volume > TerrainDatas.MaxVolume) Volume = TerrainDatas.MaxVolume;

            Volume = (((ushort)((Volume - TerrainDatas.MinVolume) * TerrainDatas.UshortPerValue)) + TerrainDatas.MinUshort) * TerrainDatas.InvertUshortPerValue;
            
            return Type;
        }

        public override void GenerateGroundPoints(BiomeData Data, int ChunkX, int ChunkZ)
        {
            int StartX = ChunkX * Information.VoxelPerChunk;
            int StartZ = ChunkZ * Information.VoxelPerChunk;

            int px, pz;
            int Index = 0;
            float Height = 0;
            byte Type = 0;
            GroundPoint Point = new GroundPoint();
            for (px = StartX; px < StartX+Information.VoxelPerChunk; ++px)
            {
                for (pz = StartZ; pz < StartZ + Information.VoxelPerChunk; ++pz)
                {
                    Index = px * Information.VoxelPerChunk + pz;
                    Type = Types[Index];
                    if (Type == 0) continue;

                    Height = (float)Heights[Index] * TerrainDatas.HeightScale;
                    if (Height <= 0) continue;

                    Point.x = (byte)(px - StartX);
                    Point.y = (ushort)Height;
                    Point.z = (byte)(pz - StartZ);
                    Point.Type = Type;
                    Point.Side = 2; // Fluid
                    Data.GroundPoints.Add(Point);
                }
            }
        }

        public override void Clear()
        {
            base.Clear();

            int Len = Types.Length;
            Array.Clear(Types, 0, Len);
            Array.Clear(Heights, 0, Len);
            Array.Clear(Sizes, 0, Len);
        }

        #region SaveLoad

        /// <summary>
        /// Save current terrain block to a file
        /// </summary>
        public override void Save(BinaryWriter Stream)
        {
            base.Save(Stream);

            /// TYPES 
            ushort LastCount = 0;
            ushort LastValue = 0;
            ushort CurrentValue = 0;
            for(int i=Types.Length-1;i>=0;--i)
            {
                CurrentValue = Types[i];
                if (CurrentValue == LastValue && LastCount < ushort.MaxValue) ++LastCount;
                else
                {
                    Stream.Write(LastCount);
                    Stream.Write((byte)LastValue);
                    LastCount = 1; LastValue = CurrentValue;
                }
            }

            if (LastCount != 0)
            {
                Stream.Write(LastCount);
                Stream.Write((byte)LastValue);
            }

            /// Height // Size
            for (int i = Heights.Length - 1; i >= 0; --i)
            {
                CurrentValue = Sizes[i];
                Stream.Write(Sizes[i]);

                if (CurrentValue != 0)
                    Stream.Write(Heights[i]);
            }
        }

        /// <summary>
        /// Read current terrain block data from file
        /// </summary>
        public override void Read(BinaryReader Stream)
        {
            base.Read(Stream);

            ushort LastCount = 0;
            ushort LastValue = 0;
            byte CurrentValue = 0;
            for (int i = Types.Length - 1; i >= 0; )
            {
                LastCount = Stream.ReadUInt16();
                CurrentValue = Stream.ReadByte();
                for (LastValue = 0; LastValue < LastCount; ++LastValue)
                {
                    Types[i - LastValue] = CurrentValue;
                }
                i -= LastCount;
            }

            for (int i = Heights.Length - 1; i >= 0; --i)
            {
                LastValue = Sizes[i] = Stream.ReadUInt16();
                if (LastValue != 0)
                    Heights[i] = Stream.ReadUInt16();
            }

        }

        #endregion


        #region Meshes

        static public int DefaultPoolId = -1;
        private int LastFrameCount = 0;
        private SList<int> BuildedNodes = new SList<int>(4);

        private void BuildData(AdvancedFluidProcessor Processor, TransvoxelBlock Block, int ChunkId, IMeshData Data)
        {
            if (DefaultPoolId == -1)
            {
                GameObject DefaultPoolObject = null;

                if (Processor.DefaultMeshObject == null)
                {
                    Processor.DefaultMeshObject = new GameObject();
                    DefaultPoolObject = Processor.DefaultMeshObject;
                    DefaultPoolObject.AddComponent<TerrainSubScript>();
                }
                else
                    DefaultPoolObject = Processor.DefaultMeshObject;


                DefaultPoolObject.name = Processor.DataName;
                DefaultPoolObject = Processor.DefaultMeshObject;
                DefaultPoolObject.SetActive(false);
                DefaultPoolObject.transform.parent = Processor.transform;

                DefaultPoolId = GameObjectPool.GetPoolId(Processor.InternalDataName + DefaultPoolObject.GetInstanceID());
                GameObjectPool.SetDefaultObject(DefaultPoolId, DefaultPoolObject);
            }

            ITerrainScript IScript = null;

            if (Data != null && Data.IsValid())
            {
                Block.CheckGameObject();
                IScript = Block.Script as ITerrainScript;

                TerrainSubScript Script = IScript.GetSubScript<TerrainSubScript>(Processor.ScriptHashCode,ChunkId);
                if (Script == null)
                {
                    GameObject Obj = GameObjectPool.Pools.array[DefaultPoolId].GetDefaultObject();
                    Transform tr = Obj.transform;

                    Obj.layer = LayerMask.NameToLayer("Water");
                    Obj.name = Processor.DataName;

                    Script = Obj.GetComponent<TerrainSubScript>();
                    Script.Init(Processor.ScriptHashCode,ChunkId, Processor, IScript);
                    tr.parent = IScript.transform;

                    if (ChunkId != int.MaxValue)
                        tr.position = Block.GetChunkWorldPosition(ChunkId);
                    else
                        tr.localPosition = new Vector3();
                }

                Data.CastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                Data.ReceiveShadows = true;
                Script.Apply(Data);
            }
            else
            {
                DisableData(Processor, Block, ChunkId);
            }

            if (Data != null)
            {
                Data.Close();
            }
        }

        private void DisableData(AdvancedFluidProcessor Processor, TransvoxelBlock Block, int ChunkId)
        {
            ITerrainScript IScript = Block.Script as ITerrainScript;

            if (IScript != null)
            {
                TerrainSubScript Script = IScript.GetSubScript<TerrainSubScript>(Processor.ScriptHashCode, ChunkId);
                if (Script != null)
                {
                    Script.Close(true);
                    GameObjectPool.Pools.array[DefaultPoolId].CloseObject(Script.gameObject);
                }
            }
        }

        public void Build(AdvancedFluidProcessor Processor, TransvoxelBlock Block, int ChunkId, IMeshData Data,bool CanDisableChilds = true)
        {
            int Parent = Block.NodesArray.Array[Block.ChunkNodes[ChunkId]].GetOpenParent(Block.NodesArray);

            BuildData(Processor,Block,ChunkId,Data);

            if (CanDisableChilds)
            {
                if (LastFrameCount != Time.frameCount)
                {
                    LastFrameCount = Time.frameCount;
                    BuildedNodes.ClearFast();
                }

                for (int i = BuildedNodes.Count - 1; i >= 0; --i)
                {
                    if (BuildedNodes.array[i] == Parent)
                        return;
                }

                BuildedNodes.Add(Parent);
                DisableChilds(Processor, Block, ChunkId, Parent);
            }
        }

        public void DisableChilds(AdvancedFluidProcessor Processor, TransvoxelBlock Block, int IgnoreChunk, int Index)
        {
            ITerrainScript IScript = Block.Script as ITerrainScript;
            if (IScript == null)
                return;

            TerrainSubScript Script;
            int ScriptHashCode = Processor.ScriptHashCode;

            int ChunkId = Block.NodesArray.Array[Index].Data;
            if (ChunkId != IgnoreChunk)
            {
                Script = IScript.GetSubScript<TerrainSubScript>(ScriptHashCode, ChunkId);
                if ((object)Script != null)
                {
                    Script.Close(true);
                    GameObjectPool.Pools.array[DefaultPoolId].CloseObject(Script.gameObject);
                }
            }

            int Start = Block.NodesArray.Array[Index].StartIndex;
            int End = Block.NodesArray.Array[Index].EndIndex;

            if (Start == -1 || End == -1)
                return;

            for (Index = Start; Index < End; ++Index)
            {
                if (!Block.NodesArray.ClosedNodes[Index])
                {
                    ChunkId = Block.NodesArray.Array[Index].Data;
                    if (ChunkId != IgnoreChunk)
                    {
                        Script = IScript.GetSubScript<TerrainSubScript>(ScriptHashCode, ChunkId);
                        if ((object)Script != null)
                        {
                            Script.Close(true);
                            GameObjectPool.Pools.array[DefaultPoolId].CloseObject(Script.gameObject);
                        }
                    }
                }
            }

        }


        #endregion
    }
}
