﻿using LibNoise;
using System;
using System.Xml.Serialization;
using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class AdvancedFluidGenerator : IWorldGenerator
    {
        [Serializable]
        public struct CustomBox
        {
            public Transform TR;
            public VectorI3 Scale;
            public VectorI3 StartPosition;
            public VectorI3 EndPosition;
            public VectorI3 Size
            {
                get
                {
                    return EndPosition - StartPosition;
                }
            }
            public float Height;
            public bool Ocean;

            public bool IsInside(int x, int z)
            {
                if (x >= StartPosition.x && z >= StartPosition.z && x < EndPosition.x && z < EndPosition.z)
                    return true;

                return false;
            }

            public bool Intersect(VectorI3 Start, VectorI3 End)
            {
                Rect A = new Rect(new Vector2(StartPosition.x, StartPosition.z), new Vector2(Size.x, Size.z));
                Rect B = new Rect(new Vector2(Start.x, Start.z), new Vector2(End.x - Start.x, End.z - Start.z));
                return A.Overlaps(B);
            }

            public void Convert(Vector3 Start, Vector3 End, float Scale)
            {
                StartPosition.x = (int)(Start.x / Scale);
                StartPosition.y = (int)(Start.y / Scale);
                StartPosition.z = (int)(Start.z / Scale);

                EndPosition.x = (int)(End.x / Scale);
                EndPosition.y = (int)(End.y / Scale);
                EndPosition.z = (int)(End.z / Scale);
            }
        }

        [Serializable]
        public struct BiomeFluidHeight
        {
            public string BiomeName;
            public int Height;

            [HideInInspector]
            public int BiomeHashCode;

            public string VoxelName;

            [HideInInspector]
            public int FluidType;
        }

        public string BiomeName = "River";
        public bool GenerateOcean = true;
        public float OceanLevel = 0;

        public string FluidName = "Water";
        public CustomBox[] Boxes;
        public int ScriptHashCode = 0;
        public float UVScale = 1f;

        private int VoxelId = 0;
        private int InternalIndex = 0;

        public BiomeFluidHeight[] Biomes;

        public override void OnServerStart(TerrainWorldServer Server)
        {
            VoxelId = GetVoxelID(FluidName);

            Biomes = new BiomeFluidHeight[1];
            Biomes[0].BiomeName = "Geyser";
            Biomes[0].BiomeHashCode = Biomes[0].BiomeName.GetHashCode();
            Biomes[0].Height = 3;
            Biomes[0].VoxelName = "Water";
            Biomes[0].FluidType = Server.Generator.GetVoxelID(Biomes[0].VoxelName);

            for (int i = 0; i < Boxes.Length; ++i)
            {
                Vector3 Position = Boxes[i].TR.position;
                Vector3 Scale = Boxes[i].TR.localScale;

                Boxes[i].Convert(Position - (Scale * 0.5f),
                    Position + (Scale * 0.5f), Server.Information.Scale);
            }
        }


        public override void OnTerrainDataCreate(TerrainDatas Data)
        {
            AdvancedFluidData FData = Data.GetCustomData<AdvancedFluidData>(DataName);
            if (FData == null)
            {
                FData = new AdvancedFluidData();
                Data.RegisterCustomData(DataName, FData);
                InternalIndex = Data.GetCustomDataIndex(DataName);
                ScriptHashCode = DataName.GetHashCode();
            }
            InternalIndex = Data.GetCustomDataIndex(DataName);
            FData.UID = UID;
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            int VPC = Block.Information.VoxelPerChunk;
            int StartX = ChunkX * VPC, StartZ = ChunkZ * VPC;
            int WorldX, WorldZ;
            AdvancedFluidData Fluid = Block.Customs.array[InternalIndex] as AdvancedFluidData;
            int x, z, i, Lenght;
            int Level = (int)(OceanLevel / Block.Information.Scale);
            byte VoxelType = (byte)VoxelId;
            VectorI3 Start = new VectorI3(Block.WorldX + ChunkX * VPC, 0, Block.WorldZ + ChunkZ * VPC);
            VectorI3 End = new VectorI3(Start.x + VPC, 0, Start.z * VPC);

            FList<CustomBox> ChunkBoxes = Data.GetCustomData("ChunkBoxes") as FList<CustomBox>;
            if (ChunkBoxes == null)
            {
                ChunkBoxes = new FList<CustomBox>(2);
                Data.SetCustomData("ChunkBoxes", ChunkBoxes);
            }
            else
                ChunkBoxes.Clear();

            for (i = Boxes.Length - 1; i >= 0; --i)
            {
                if (Boxes[i].Intersect(Start, End))
                    ChunkBoxes.Add(Boxes[i]);
            }

            Lenght = ChunkBoxes.Length - 1;


            float Height = 0;
            int RiverHashCode = BiomeName.GetHashCode();

            int BiomesCount = Biomes.Length - 1;


            int px, pz;
            float FluidHeigt = 0;
            for (x = VPC - 1; x >= 0; --x)
            {
                WorldX = StartX + x + Block.WorldX;
                px = StartX + x;
                for (z = VPC - 1; z >= 0; --z)
                {
                    WorldZ = StartZ + z + Block.WorldZ;
                    pz = StartZ + z;

                    Height = Block.GetHeight(StartX + x, StartZ + z);

                    #region Old
                    /*for (i = Lenght; i >= 0; --i)
                    {
                        if (ChunkBoxes.array[i].IsInside(WorldX, WorldZ))
                        {
                            if (ChunkBoxes.array[i].Ocean)
                            {
                                FluidHeight = ChunkBoxes.array[i].EndPosition.y;

                                if (FluidHeight - (Height - 1) > 0)
                                {
                                    Fluid.SetFluid(StartX + x, StartZ + z, VoxelType, Height - 1, FluidHeight - (Height - 1));
                                }
                            }
                            else
                            {
                                FluidHeight = ChunkBoxes.array[i].Height;
                                Block.SetHeightFast(px, pz, Height - FluidHeight);
                                Fluid.SetFluid(px, pz, VoxelType, Height - FluidHeight - 1, FluidHeight);
                            }
                            break;
                        }
                    }*/
                    #endregion

                    if (GenerateOcean && Level - (Height - 1) > 0)
                    {
                        Fluid.SetFluid(px, pz, VoxelType, Height - 1, Level - (Height - 1));
                    }
                    else
                    {
                        Data.LoadBiomeReso(x, z);
                        for (i = BiomesCount; i >= 0; --i)
                        {
                            if (Data.Contains(Biomes[i].BiomeHashCode))
                            {
                                FluidHeigt = (float)Data.GetCustomNoiseData(Biomes[i].BiomeHashCode);
                                Fluid.SetFluid(px, pz, (byte)Biomes[i].FluidType, Height - 1, FluidHeigt - (Height-1));
                                break;
                            }
                        }
                    }
                }
            }


            return true;
        }

       /* public override void OnBlockGenerated(ActionThread Thread, TerrainBlockServer Block)
        {
            AdvancedFluidData Fluid = Block.Voxels.Customs.array[InternalIndex] as AdvancedFluidData;
            if (Fluid.RiverCount == 0)
                return;

            byte VoxelType = (byte)VoxelId;

            double Height = (Fluid.RiverHeight / Fluid.RiverCount) + 5.0;
            Debug.Log("OnBlockGenerated " + Fluid.RiverCount + "," + Fluid.RiverHeight+",Height:"+Height);
            for(int i=Fluid.Sizes.Length-1;i>=0;--i)
            {
                if(Fluid.Sizes[i] == 1)
                {
                    Fluid.SetFluid(i, VoxelType, 1, (float)Height);   
                }
            }
        }*/
    }
}
