﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace TerrainEngine
{
    public class HandPlacedDatas : ICustomTerrainData
    {
        public class HandPlacedObject
        {
            public string Name;
            public Vector3 Position;
            public Vector3 Scale;
            public Vector3 Rotation;
            public GameObject Instance;
            public TerrainObject TObject;

            public HandPlacedObject(string Name, Vector3 Position,Vector3 Rotation, Vector3 Scale)
            {
                this.Name = Name;
                this.Position = Position;
                this.Rotation = Rotation;
                this.Scale = Scale;
                this.Instance = null;
            }

            // Handle the callback when object is instancied and store instance ref
            public void OnObjectInstancied(CreateGameObjectAction CreateObj)
            {
                Instance = CreateObj.CreatedObject;

                /* Here, if you want update position/scale/rotation from the object when terrain is saved (on other thread)
                 * You need a reference to TerrainObject script on this object for be able to access to values on non unity thread
                 * 
                    TObject = Instance.GetComponent<TerrainObject>();
                    if(TObject == null)
                        TObject = Instance.AddComponent<TerrainObject>();
                 * 
                 * */
            }

            public void OnObjectDeleted(DeleteGameObjectAction DeleteObj)
            {
                Instance = null;
                TObject = null;
            }

            // Update values from the current object instance
            public void UpdateValues()
            {
                Position = TObject.Position;
                Rotation = TObject.Rotation;
                //Scale = TObject.Scale; For the scale , add value on TerrainObject Script
            }
        }

        public List<HandPlacedObject> Objects = new List<HandPlacedObject>();
        public float NextUpdate = 0;
        public float UpdateInterval = 4f;

        // Use this function for add an new object. SetDirty will allow this Block to be saved
        public void AddObject(HandPlacedObject Obj)
        {
            Objects.Add(Obj);
            Dirty = true;
        }

        public void RemoveObject(HandPlacedObject Obj)
        {
            if (Objects.Remove(Obj))
                Dirty = true;
        }

        public override void Save(BinaryWriter Stream)
        {
            Stream.Write(Objects.Count);
            HandPlacedObject Obj;
            for (int i = 0; i < Objects.Count; ++i) // Using loop for avoid Garbage generated by unity when using Foreach.
            {
                Obj = Objects[i];
                Stream.Write(Obj.Name);

                // If you want update values from current instance and store for example, last NPC position / rotation
                // Obj.Update(); // Uncomment line 37 that add TerrainObject script to your prefab

                // Here we store data from the list, but not the position of the instance. You must update this values before if you want current instance values
                WriteVector(Stream, Obj.Position);
                WriteVector(Stream, Obj.Rotation);
                WriteVector(Stream, Obj.Scale);
            }
            base.Save(Stream);
        }

        public override void Read(BinaryReader Stream)
        {
            int Count = Stream.ReadInt32();
            while (Count > 0)
            {
                --Count;
                Objects.Add(new HandPlacedObject(Stream.ReadString(), ReadVector(Stream), ReadVector(Stream), ReadVector(Stream)));
            }
        }

        public override void Clear()
        {
            Objects.Clear();
            base.Clear();
        }

        static public void WriteVector(BinaryWriter Writer, Vector3 V)
        {
            Writer.Write(V.x);
            Writer.Write(V.y);
            Writer.Write(V.z);
        }

        static public Vector3 ReadVector(BinaryReader Stream)
        {
            return new Vector3(Stream.ReadSingle(), Stream.ReadSingle(), Stream.ReadSingle());
        }
    }
}