﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class HandPlacedObjectsMenu
{
    [MenuItem("TerrainEngine/Processors/6-HandPlacedProcessor")]
    static public void CreateHandPlacedProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        HandPlacedProcessor Obj = GameObject.FindObjectOfType(typeof(HandPlacedProcessor)) as HandPlacedProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<HandPlacedProcessor>();

        Obj.name = "6-HandPlacedProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 6;
    }
}
