﻿using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class HandPlacedProcessor : IBlockProcessor
{
    public bool DrawGUI = true;
    public List<GameObject> Prefabs = new List<GameObject>(); // List of prefabs instanciable
    public Dictionary<string, GameObject> OrderedPrefabs = new Dictionary<string, GameObject>();

    public PoolInfoRef CreatePool;
    public PoolInfoRef ClosePool;

    // We generate an Unique Name used only for this Processor
    public override void OnInitManager(TerrainManager Manager)
    {
        base.OnInitManager(Manager);
        InternalDataName = "HandPlaced-" + DataName;
        CreatePool = Pool.GetPool("CreateGameObjectAction");
        ClosePool = Pool.GetPool("DeleteGameObjectAction");

        foreach (GameObject Obj in Prefabs)
        {
            if (Obj == null)
            {
                Debug.LogError(name + " : Missing Prefab");
                continue;
            }

            if (!OrderedPrefabs.ContainsKey(Obj.name))
                OrderedPrefabs.Add(Obj.name, Obj);
            else
                Debug.LogError(name + " : Duplicate Prefab Name :" + Obj.name);
        }

        EnableUnityUpdateFunction = true;
    }

    // We add a custom class that contains all objects on each TerrainBlock
    public override void OnBlockDataCreate(TerrainDatas Data)
    {
        HandPlacedDatas HData = Data.GetCustomData<HandPlacedDatas>(InternalDataName);
        if (HData == null)
        {
            HData = new HandPlacedDatas();
            Data.RegisterCustomData(InternalDataName, HData);
        }
    }

    public override void OnTerrainLoadingDone(TerrainBlock Terrain)
    {
        HandPlacedDatas HData = Terrain.Voxels.GetCustomData<HandPlacedDatas>(InternalDataName);

        foreach (HandPlacedDatas.HandPlacedObject Obj in HData.Objects)
        {
            InstanciateObject(Obj);
        }
    }

    // Remove all objects instance from the world.
    public override void OnTerrainThreadClose(TerrainBlock Terrain)
    {
         HandPlacedDatas HData = Terrain.Voxels.GetCustomData<HandPlacedDatas>(InternalDataName);
         foreach (HandPlacedDatas.HandPlacedObject Obj in HData.Objects)
         {
             DeleteGameObjectAction Action = ClosePool.GetData<DeleteGameObjectAction>();
             Action.ToRemove = Obj.Instance;
             Action.OnEnd += Obj.OnObjectDeleted;
             ActionProcessor.AddToUnity(Action);
         }
    }

    public override bool OnTerrainUnityUpdate(ActionThread Thread, TerrainBlock Terrain)
    {
        HandPlacedDatas HData = Terrain.Voxels.GetCustomData<HandPlacedDatas>(InternalDataName);
        if (HData.NextUpdate <= Time.time)
        {
            HData.NextUpdate = Time.time + HData.UpdateInterval;

            for (int i = HData.Objects.Count-1; i >= 0; --i)
            {
                if (HData.Objects[i].Instance == null)
                {
                    HData.Objects.RemoveAt(i);
                }
            }
        }

        return false;
    }

    public void InstanciateObject(HandPlacedDatas.HandPlacedObject Obj)
    {
        GameObject Prefab = null;
        if (OrderedPrefabs.TryGetValue(Obj.Name, out Prefab))
        {
            CreateGameObjectAction Action = CreatePool.GetData<CreateGameObjectAction>();
            Action.Init(Prefab, Obj.Position, Quaternion.Euler(Obj.Rotation), Obj.Scale);
            Action.OnEnd += Obj.OnObjectInstancied;

            ActionProcessor.AddToUnity(Action);
        }
    }


    #region GUI

    void OnGUI()
    {
        if (!DrawGUI)
            return;

        if (!ITerrainCameraHandler.Instance.HasCamera)
        {
            GUI.Label(new Rect(10, 30, 300, 20), "No Camera");
            return;
        }

        if (ITerrainCameraHandler.Instance.GetMainCameraTObject().BlockObject == null)
        {
            GUI.Label(new Rect(10, 30, 300, 20), "Camera not on terrain");
            return;
        }

        TerrainObject Camera = ITerrainCameraHandler.Instance.GetMainCameraTObject();


        float y = 30;

        GUI.Box(new Rect(0, y, 610, 22 * Prefabs.Count + 80), "");
        y += 5;
        GUI.Label(new Rect(5, y, 600, 20), "Example for place object manually. They are automatically saved and loaded with the terrain");
        y += 20;
        if (GUI.Button(new Rect(5, y, 200, 20), "Save terrain"))
        {
            if (TerrainManager.Instance.Server != null)
                TerrainManager.Instance.Server.SaveAll();
        }
        y += 22;
        foreach (GameObject Prefab in Prefabs)
        {
            if (GUI.Button(new Rect(10, y, 300, 20), "Instanciate : " + Prefab.name))
            {
                HandPlacedDatas HData = ITerrainCameraHandler.Instance.GetMainCameraTObject().BlockObject.Voxels.GetCustomData<HandPlacedDatas>(InternalDataName);
                HandPlacedDatas.HandPlacedObject Obj = new HandPlacedDatas.HandPlacedObject(Prefab.name, Camera.Position, Camera.Rotation, new Vector3(1,1,1));
                HData.AddObject(Obj);
                InstanciateObject(Obj);
            }

            y += 22;
        }
    }

    #endregion
}
