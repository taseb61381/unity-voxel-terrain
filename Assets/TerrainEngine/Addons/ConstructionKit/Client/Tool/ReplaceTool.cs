﻿

public class ReplaceTool : ToolSystem
{
    public bool IsEnabled = false;
    public int TextureA;
    public int TextureB;
    public string Min="0", Max="999";
    public bool RandomOrientation = true;
    public int Selected = -1;

    public override bool IsActiveTool()
    {
        return IsEnabled || base.IsActiveTool();
    }

    public override void OnSelectTexture(int TypeId)
    {
        if (Selected != -1)
        {
            if (Selected == 0)
                TextureA = TypeId;
            else
                TextureB = TypeId;

            Selected = -1;
            return;
        }
    }

    public override void EnableTool()
    {
        IsEnabled = true;
    }
}