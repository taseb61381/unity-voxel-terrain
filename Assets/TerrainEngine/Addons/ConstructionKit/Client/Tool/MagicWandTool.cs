using UnityEngine;

public class MagicWandTool : ToolSystem
{
	public ObjectTool ObjectTool;
	
	public override void EnableTool()
	{
		ReplaceMode = true;
		Interface.PreviewObject.GetComponent<MeshFilter>().sharedMesh.Clear();		
	}
	
	public override void DisableTool() {
		ReplaceMode = false;
	}

	public override void InterfaceUpdate()
	{		
		/*Preview(true);

		if (Input.GetMouseButtonDown(0))
		{

			ObjectTool.ReplaceMode = false;

			VectorI3 voxelpos = Interface.GetVoxelFromWorldPosition(Interface.point, Interface.Script);

			Vector3 absolute = Interface.point;

			byte currentType = 0;
			byte currentTex = 0;
			byte currentRot = 0;

			Interface.Script.GetValues(voxelpos.x, voxelpos.y, voxelpos.z, ref currentType, ref currentTex,
			                                          ref currentRot);

			Recorder recorder = new Recorder(Interface, absolute);


			if (Modifier )
				Debug.Log("Recording all textures:" + currentTex);
			else
				Debug.Log("Recording texture:" + currentTex);
			var data = recorder.Record(currentTex,Modifier);
			if (data != null)
			{
				
				data.Optimize();
				 //if you want to replace by the object, you just press ctrl
				ObjectTool.ReplaceMode = false;
				ObjectTool.ApplyObject(data);
				Interface.LastGUISelected = ObjectTool;
				
			}
			
		}*/
	}

	public override void Preview(bool Now = false)
	{
		base.Preview(Now);
	}
}