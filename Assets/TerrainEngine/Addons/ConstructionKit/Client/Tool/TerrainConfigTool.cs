using TerrainEngine;
using UnityEngine;

public class TerrainConfigTool : ToolSystem 
{
    public bool IsEnabled = false;
    public string TerrainSizeX = "";
    public string TerrainSizeY = "";
    public string TerrainSizeZ = "";
    public string Message = "";
    public bool RandomOrientation = false;
    public int Seed;
    public int Resolution = 10;
    public int Frequency = 5;

    public override bool IsActiveTool()
    {
        return IsEnabled || base.IsActiveTool();
    }

    public override void EnableTool()
    {
        IsEnabled = true;
        SimplexNoise.Initialize();

        int X=0,Y=0,Z=0;
        foreach (Vector3 Chunk in Interface.Manager.ChunksSet)
        {
            if (Chunk.x > X) X = (int)Chunk.x;
            if (Chunk.y > Y) Y = (int)Chunk.y;
            if (Chunk.z > Z) Z = (int)Chunk.z;
        }

        TerrainSizeX = X.ToString();
        TerrainSizeY = Y.ToString();
        TerrainSizeZ = Z.ToString();
    }

    /*public void GenerateRandomFlore(ObjectChunk Chunk)
    {
        int x, y, z;
        SFastRandom FRandom = new SFastRandom(Seed);
        byte Type = 0;
        for (x = 0; x < ObjectChunk.ChunkSizeX; ++x)
        {
            for (y = 0; y < ObjectChunk.ChunkSizeY; ++y)
            {
                for (z = 0; z < ObjectChunk.ChunkSizeZ; ++z)
                {
                    if (Chunk.GetType(x, y, z) == 0 && FRandom.randomIntAbs(1000) <= Frequency)
                    {
                        Type = Chunk.GetType(x, y - 1, z);
                        if (Type != 0 && Type != Interface.TypeId)
                        {
                            Chunk.SetType(x, y, z, (byte)Interface.TypeId);
                            Chunk.SetTexture(x, y, z,(byte)Interface.TextureId);
                            Chunk.Dirty = true;
                        }
                    }
                }
            }
        }

        if (Chunk.Dirty)
            Interface.Manager.AddChunkToGenerate(Chunk);
    }*/

    /*public void GenerateTerracedTerrain(ObjectChunk Chunk)
    {
        int x, y, z;
        float px, py, pz;
        float noise = 0f,s,ppx,ppy,ppz;
        SFastRandom FRandom = new SFastRandom(Seed);
        float Res = 1f / (float)Resolution;
        for (x = 0; x < ObjectChunk.ChunkSizeX; ++x)
        {
            for (y = 0; y < ObjectChunk.ChunkSizeY; ++y)
            {
                for (z = 0; z < ObjectChunk.ChunkSizeZ; ++z)
                {
                    px = x + ObjectChunk.ChunkSizeX * (int)Chunk.Position.x;
                    py = y + ObjectChunk.ChunkSizeY * (int)Chunk.Position.y;
                    pz = z + ObjectChunk.ChunkSizeZ * (int)Chunk.Position.z;

                    if (py <= 1)
                    {
                        Chunk.SetType(x, y, z, 1);
                        Chunk.SetTexture(x, y, z, (byte)Interface.TextureId);
                        Chunk.SetOrientation(x, y, z, (byte)(RandomOrientation ? FRandom.randomIntAbs(4) : 0));
                        continue;
                    }

                    px *= Res;
                    pz *= Res;
                    py *= Res;
                    noise = 0f;

                    for (s = 1f; s < 8f; s *= 2f)
                    {
                        ppx = px + Mathf.Sin((py * 4f) - Mathf.Ceil(py * 4f)) * .1f;
                        ppy = Mathf.Ceil((py) * 4f);
                        ppz = pz - Mathf.Cos(Mathf.Ceil(py * 8f)) * .1f;

                        noise += s * SimplexNoise.Noise(ppx, ppy, ppz);
                        px *= .5f;
                        py *= .5f;
                        pz *= .5f;

                    }

                    noise = Mathf.Pow((noise / 15f) - 1f, 5f) + 1f;
                    Chunk.SetType(x, y, z,(byte)(noise > 0f ? 1 : 0));
                    Chunk.SetTexture(x, y, z,(byte)Interface.TextureId);
                    Chunk.SetOrientation(x, y, z,(byte)(RandomOrientation ? FRandom.randomIntAbs(4) : 0));
                }
            }
        }

        Chunk.Dirty = true;
        Interface.Manager.AddChunkToGenerate(Chunk);
    }*/
}
