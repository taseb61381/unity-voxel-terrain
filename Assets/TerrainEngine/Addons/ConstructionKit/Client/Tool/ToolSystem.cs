﻿using TerrainEngine.ConstructionKit;
using UnityEngine;

public class ToolSystem : MonoBehaviour
{
	public ToolInterface Interface;

	public KeyCode[] Keys;
	public KeyCode[] NonKeys;
	public int[] MouseButtons;
	public int[] MouseButtonsDown;
	public TempVoxel[][][] Temp;

	public bool ReplaceMode;

	public bool Modifier;//an aternate tool mode. magic wand in oneTexture / allTexture mode, can be used for sphere / cube or anything else

	public virtual void Init()
	{
	}

	public virtual bool IsActiveTool()
	{
		foreach (int Mouse in MouseButtons)
			if (!Input.GetMouseButton(Mouse))
				return false;

		foreach (KeyCode Key in Keys)
			if (!Input.GetKey(Key))
				return false;

		foreach (KeyCode Key in NonKeys)
			if (Input.GetKey(Key))
				return false;

		foreach (int Mouse in MouseButtonsDown)
			if (!Input.GetMouseButtonDown(Mouse))
				return false;

		return true;
	}

	public virtual void DisableTool()
	{
	}

	public virtual void EnableTool()
	{
	}

	public virtual void InterfaceUpdate()
	{
	}

	public virtual void EnableRaycast()
	{
	}

	public bool IsCurrentTool()
	{
		return Interface.CurrentSystem == this;
	}

	public virtual bool IsReplaceMode()
	{
		
		if (ReplaceMode || Interface.TypeId == 0 || Input.GetKey(KeyCode.LeftControl))
			return true;

		return false;
	}

	public virtual void OnSelectType(int TypeId)
	{
	}

	public virtual void OnSelectTexture(int TextureId)
	{
	}

	public virtual void OnRotateObject(int Rotation)
	{
	}

	public virtual void OnChangeSize(int SizeX, int SizeY, int SizeZ)
	{
	}

	public virtual void OnObjectLoaded(ObjectDatas Data)
	{
	}

	public bool ResizeTool(int SizeX, int SizeY, int SizeZ)
	{
		int x, y, z;
		if (Temp == null || Temp.GetLength(0) != SizeX + 2 || Temp[0].GetLength(0) != SizeY + 2 ||
		    Temp[0][0].GetLength(0) != SizeZ + 2)
		{
			Temp = new TempVoxel[SizeX + 2][][];

			for (x = 0; x < SizeX + 2; ++x)
			{
				Temp[x] = new TempVoxel[SizeY + 2][];

				for (y = 0; y < SizeY + 2; ++y)
				{
					Temp[x][y] = new TempVoxel[SizeZ + 2];

					for (z = 0; z < SizeZ + 2; ++z)
					{
						Temp[x][y][z] = new TempVoxel();
					}
				}
			}

			return true;
		}

		return false;
	}

	public void Fill(int SizeX, int SizeY, int SizeZ)
	{
		Fill(SizeX, SizeY, SizeZ, (byte) Interface.TypeId, (byte) Interface.TextureId, (byte) Interface.Rotation);
	}

	public void Fill(int SizeX, int SizeY, int SizeZ, byte Type, byte Texture, byte Rotation)
	{
        CKVoxelInformation Info = Interface.Manager.Voxels[Type];
		TextureInformation TInfo = Interface.Manager.Textures[Texture];

		int x, y, z;
		for (x = 1; x < SizeX + 1; ++x)
			for (y = 1; y < SizeY + 1; ++y)
				for (z = 1; z < SizeZ + 1; ++z)
					Temp[x][y][z].Init(Type, Texture, Rotation, Info, TInfo);
	}

	public virtual void Preview(bool Now = false)
	{
		Interface.Temp = this.Temp;
		if (!Now)
			Interface.MustApplyMesh = true;
		else
			Interface.ApplyMesh();
	}

	public virtual void OnStartDrag()
	{
	}

	public virtual void OnEndDrag()
	{
	}
}