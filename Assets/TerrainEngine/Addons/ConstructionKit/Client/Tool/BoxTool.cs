﻿using TerrainEngine;
using UnityEngine;

public class BoxTool : ToolSystem
{
    public GameObject PlaneA, PlaneB, PlaneC, PlaneD;
    public TextMesh SideText;
    public Vector3 StartPosition;
    public Color GUIColor = Color.black;

    public override void EnableTool()
    {
        Interface.PreviewObject.GetComponent<MeshFilter>().sharedMesh.Clear();
    }

    public override void OnStartDrag()
    {
        PlaneB.SetActive(true);
        PlaneC.SetActive(true);
        PlaneD.SetActive(true);
        SideText.gameObject.SetActive(true);

        StartPosition = Interface.GetWorldPosition(Interface.point);
        if (SideText != null)
            SideText.GetComponent<Renderer>().material.color = GUIColor;
    }

    /// <summary>
    /// Apply Voxel change
    /// </summary>
    public override void OnEndDrag()
    {
        PlaneA.SetActive(false);
        PlaneB.SetActive(false);
        PlaneC.SetActive(false);
        PlaneD.SetActive(false);

        if (!Interface.ChunkRef.IsValid())
            return;

        Vector3 S = Interface.Block.GetVoxelFromWorldPosition(StartPosition, false);
        Vector3 P = Interface.Block.GetVoxelFromWorldPosition(Interface.GetWorldPosition(Interface.point), false);

        int ex = (int)Mathf.Max(S.x, P.x);
        int ey = (int)Mathf.Max(S.y, P.y);
        int ez = (int)Mathf.Max(S.z, P.z);

        int sx = (int)Mathf.Min(S.x, P.x);
        int sy = (int)Mathf.Min(S.y, P.y);
        int sz = (int)Mathf.Min(S.z, P.z);

        int x, y, z;
        CKVoxelModification Vm = new CKVoxelModification();
        VectorI3 Start = TerrainBlock.GetChunkStart(Interface.ChunkRef.ChunkId);
        for (x = sx; x <= ex; ++x)
            for (y = sy; y <= ey; ++y)
                for (z = sz; z <= ez; ++z)
                {
                    Vm.Chunk = Interface.ChunkRef;
                    Vm.x = x - Start.x;
                    Vm.y = y - Start.y;
                    Vm.z = z - Start.z;
                    Vm.Type = (byte)Interface.TypeId;
                    Vm.Texture = (byte)Interface.TextureId;
                    Vm.Rotation = (byte)Interface.Rotation;
                    Interface.Manager._Modifications.Enqueue(Vm);
                }

        Temp = null;
        Interface.PreviewObject.GetComponent<MeshFilter>().sharedMesh.Clear();
    }

    /// <summary>
    /// Update the Preview Box
    /// </summary>
    public override void InterfaceUpdate()
    {
        if (!Interface.IsDragging)
            return;

        if (SideText != null)
        {
            SideText.GetComponent<Renderer>().material.color = GUIColor;
            SideText.transform.position = Interface.point + new Vector3(0, 3, 0);
            SideText.transform.LookAt(Interface.CurrentCam.transform);
            SideText.transform.Rotate(0, 180, 0);
            SideText.transform.position -= SideText.transform.forward * 1;
        }

        Vector3 Point = Interface.GetWorldPosition(Interface.point);
        Vector3 S = Interface.Block.GetVoxelFromWorldPosition(StartPosition, false);
        Vector3 P = Interface.Block.GetVoxelFromWorldPosition(Interface.GetWorldPosition(Interface.point), false);

        int ex = (int)Mathf.Max(S.x, P.x);
        int ey = (int)Mathf.Max(S.y, P.y);
        int ez = (int)Mathf.Max(S.z, P.z);

        int sx = (int)Mathf.Min(S.x, P.x);
        int sy = (int)Mathf.Min(S.y, P.y);
        int sz = (int)Mathf.Min(S.z, P.z);

        float X = Mathf.Abs(ex - sx);
        float Y = Mathf.Abs(ey - sy);
        float Z = Mathf.Abs(ez - sz);

        if (SideText != null)
        {
            SideText.text = (X + 1) + "x" + (Y + 1) + "x" + (Z + 1);
        }

        ResizeTool((int)X + 1, (int)Y + 1, (int)Z + 1);
        Fill((int)X + 1, (int)Y + 1, (int)Z + 1);
        Preview(true);

        Interface.PreviewObject.transform.position = new Vector3((int)P.x < S.x ? (int)Point.x : StartPosition.x,
            (int)P.y < S.y ? (int)Point.y : StartPosition.y,
            (int)P.z < S.z ? (int)Point.z : StartPosition.z) - Interface.ScaleOffset;

        X *= Interface.Scale;
        Y *= Interface.Scale;
        Z *= Interface.Scale;
        PlaneA.transform.position = PlaneB.transform.position = (Interface.PreviewObject.transform.position - new Vector3(0.05f, 0.05f, 0.05f)) + new Vector3(0, (Y + Interface.Scale) * 0.5f, 0) - new Vector3(0.5f, 0.5f, 0.5f);
        PlaneC.transform.position = PlaneD.transform.position = (Interface.PreviewObject.transform.position + new Vector3((X + Interface.Scale) + 0.05f, (Y + Interface.Scale) + 0.05f, (Z + Interface.Scale) + 0.05f)) - new Vector3(0, (Y + Interface.Scale) * 0.5f, 0) - new Vector3(0.5f, 0.5f, 0.5f);
        PlaneA.transform.localScale = PlaneB.transform.localScale = PlaneC.transform.localScale = PlaneD.transform.localScale = new Vector3(100, Interface.Scale, (Y + Interface.Scale) * 0.1f);
    }

    public override bool IsReplaceMode()
    {
        if (ReplaceMode || Input.GetKey(KeyCode.LeftControl))
            return true;

        return false;
    }

    public override void Preview(bool Now = false)
    {
        if (!Interface.IsDragging)
            return;

        base.Preview(Now);
    }
}
