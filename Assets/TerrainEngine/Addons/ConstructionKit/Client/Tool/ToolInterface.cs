using System.Collections.Generic;
using TerrainEngine;
using TerrainEngine.ConstructionKit;
using UnityEngine;

public class ToolInterface : MonoBehaviour
{
    public CKChunkProcessor Manager;
    public int TypeId = 0;
    public int TextureId = 0;
    public int Rotation = 0;

    [HideInInspector]
    public GameObject PreviewObject;
    public Material TransparentMaterial;
    public LayerMask Layer;
    public ActiveDisableContainer OnEnableEvent;
    public ActiveDisableContainer OnDisableEvent;
    public Camera CurrentCam;
    public float Scale
    {
        get
        {
            return TerrainManager.Instance.Informations.Scale;
        }
    }

    public List<ToolSystem> Systems;
  // Tools, Be carefull , order is very important. Last tool is the default tool , first in list , first to be checked , first to be used if requirements are ok


    public ToolSystem DefaultTool;
    public ToolSystem CurrentSystem; // Current Tool
    public ToolSystem LastSystem;
    public ToolSystem LastGUISelected; // GUI Button SelectedTool
    public TempVoxel[][][] Temp; // Temp Voxel Storage for build preview.
    private TempMesh TMesh = new TempMesh();
    public bool MustApplyMesh; // True is PreviewObject Must be rebuilded

    // GUI
    public GameObject HelpGUI;
    public bool MouseIsOnGUI = false;
    public string SaveName = "NewMap";
    private List<string> Files = new List<string>();
    private int Selected = -1;

    private string[] _VoxelNames;
    private GUIContent[] _VoxelTextures;
    private string[] VoxelNames
    {
        get
        {
            if (_VoxelNames == null || _VoxelNames.Length != Manager.Voxels.Length)
            {
                _VoxelNames = new string[Manager.Voxels.Length];
                for (int i = 0; i < _VoxelNames.Length; ++i)
                    _VoxelNames[i] = Manager.Voxels[i].Name;
            }

            return _VoxelNames;

        }
    }
    private GUIContent[] VoxelTextures
    {
        get
        {
            if (_VoxelTextures == null || _VoxelTextures.Length != Manager.Textures.Length)
            {
                _VoxelTextures = new GUIContent[Manager.Textures.Length];
                for (int i = 0; i < _VoxelTextures.Length; ++i)
                    _VoxelTextures[i] = new GUIContent(Manager.Textures[i].Top);
            }

            return _VoxelTextures;
        }
    }
    private bool LoadWindow = false;
    private string[] StrFiles;

    public Vector3 point; // Current Cursor World Position
    public Vector3 pointOffset;
    public Vector3 ScaleOffset
    {
        get
        {
            return new Vector3(Scale, Scale, Scale) * 0.5f;
        }
    }
    public TerrainChunkRef ChunkRef;
    public TerrainBlock Block
    {
        get
        {
            TerrainBlock Block = null;
            ChunkRef.IsValid(ref Block);
            return Block;
        }
    }
    public Vector3 WorldPosition
    {
        get
        {
            TerrainBlock Block = this.Block;
            return Block.Chunks[ChunkRef.ChunkId].GetWorldPosition(Block);
        }
    }
    public CKDatas Datas;

    private void Start()
    {
        CurrentCam = Camera.main;

        PreviewObject = new GameObject();
        PreviewObject.AddComponent<MeshFilter>().sharedMesh = new Mesh();
        PreviewObject.AddComponent<MeshRenderer>();

        if (Manager == null)
            Manager = FindObjectOfType(typeof(CKChunkProcessor)) as CKChunkProcessor;

        if (Systems == null)
            Systems = new List<ToolSystem>();

        Systems.Add(DefaultTool);

        if (Systems.Count == 0)
        {
            Systems.AddRange(gameObject.GetComponentsInChildren<ToolSystem>());
        }

        foreach (ToolSystem System in Systems)
        {
            System.Interface = this;
            System.Init();
        }

        LastGUISelected = DefaultTool;
    }

    void OnEnable()
    {
        if (OnEnableEvent != null)
            OnEnableEvent.Call();
    }

    void OnDisable()
    {
        if (OnDisableEvent != null)
            OnDisableEvent.Call();
    }

    public Vector3 GetWorldPosition(Vector3 Position)
    {
        Vector3 P = Block.GetVoxelFromWorldPosition(Position, false);
        P = Block.GetWorldVoxel(P) * Scale;
        return P;
    }

    public void CheckMouse(params Rect[] Values)
    {
        Vector2 M = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        foreach (Rect R in Values)
            if (R.Contains(M))
            {
                MouseIsOnGUI = true;
                return;
            }
    }

    private void OnGUI()
    {
        if (Manager == null)
            return;

        if(!DebugGUIProcessor.DrawGUI)
            return;

        CurrentCam = Camera.main;

        if (CurrentCam == null)
            return;

        MouseIsOnGUI = false;
        
        
        Rect LoadRect = new Rect(Screen.width - 200, 30, 100, 20);
        Rect LoadMenuRect = new Rect(Screen.width/2 - 200, Screen.height/2 - 250, 400, 500);
        Rect SaveNameRect = new Rect(Screen.width - 400, 30, 200, 20);
        Rect SaveRect = new Rect(Screen.width - 100, 30, 100, 20);
        Rect VoxelsRect = new Rect(0, Screen.height - 80, Screen.width, 80);
        Rect TexturesRect = new Rect(Screen.width - 80, 50, 80, Screen.height - 20);
        Rect ToolsRect = new Rect(0,30, 60, Screen.height);

        Rect upRect = new Rect(0, 30, Screen.width, 60); //TODO the whole CheckMouse rect thing became stupid, just pick in center rect instead of ignoring borders !
        CheckMouse(upRect, VoxelsRect, TexturesRect, ToolsRect);
        
        GUI.backgroundColor = Color.white;
        GUI.color = Color.white;
        SetTexture(GUI.SelectionGrid(TexturesRect, TextureId, VoxelTextures, 2));
        SetType(GUI.SelectionGrid(VoxelsRect, TypeId, VoxelNames, VoxelNames.Length / 2));
        
        

        if (GUI.Button(LoadRect, "Load"))
        {
            StrFiles = Files.ToArray();
            LoadWindow = !LoadWindow;
        }
        else if (LoadWindow)
        {
            GUI.Box(LoadMenuRect, "Load Menu");
            CheckMouse(LoadMenuRect);

            if (Selected == -1)
                Selected = GUI.SelectionGrid(new Rect(Screen.width/2 - 190, Screen.height/2 - 230, 380, StrFiles.Length*20 + 20),
                                             Selected, StrFiles, 1);
            else
            {
                if (GUI.Button(new Rect(Screen.width/2 - 190, Screen.height/2 - 200, 380, 20), "Load as Object"))
                {
                    Selected = -1;
                    LoadWindow = false;
                }
            }
        }

        SaveName = GUI.TextField(SaveNameRect, SaveName);

        if (GUI.Button(SaveRect, "Save"))
        {
            if (TerrainManager.Instance.Server != null)
                TerrainManager.Instance.Server.SaveAll();
        }
    }

    public int GetVoxelPoint(float P)
    {
        return (int)((P-Scale*0.25f) / Scale);
    }

    public float GetWorldPoint(int P)
    {
        return (float)P * Scale;
    }

    private void FixedUpdate()
    {
        if (MouseIsOnGUI)
            return;

        CurrentCam = Camera.main;
        if (CurrentCam == null)
            return;

        Ray R = CurrentCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit Hit;

        Vector3 normal = new Vector3();
        point = new Vector3();

        if (Physics.Raycast(R, out Hit, 500, Layer))
        {
            normal = Hit.normal;
            point = Hit.point;

            point.x = GetVoxelPoint(point.x);
            point.y = GetVoxelPoint(point.y);
            point.z = GetVoxelPoint(point.z);

            if (CurrentSystem != null && CurrentSystem.IsReplaceMode())
            {

                /*if (normal.y > 0.1f)
                    point.y -= Scale*0.5f + 0.1f;
                else if (normal.y < -0.1f)
                    point.y += Scale*0.5f + 0.1f;

                    if (normal.x < -0.1f)
                        point.x += Scale + 0.1f;
                    else if (normal.x > 0.1f)
                        point.x -= Scale + 0.1f;

                    if (normal.z < -0.1f)
                        point.z += Scale + 0.1f;
                    else if (normal.z > 0.1f)
                        point.z -= Scale + 0.1f;
                */
                PreviewObject.transform.localScale = new Vector3(1.01f, 1.01f, 1.01f);
            }
            else
            {
                if (normal.x > 0)
                    point.x += 1;
                if (normal.y > 0)
                    point.y += 1;
                if (normal.z > 0)
                    point.z += 1;

                PreviewObject.transform.localScale = new Vector3(1, 1, 1);
            }

            point.x = GetWorldPoint((int)point.x);
            point.y = GetWorldPoint((int)point.y);
            point.z = GetWorldPoint((int)point.z);

            VectorI3 NewChunk = TerrainManager.Instance.Informations.GetChunkGlobalPosition(point);
            VectorI3 NewBlock = TerrainManager.Instance.Informations.GetBlockFromWorldPosition(point);

            TerrainBlock Block = TerrainManager.Instance.Container.GetBlock(NewBlock) as TerrainBlock;
            TerrainChunkRef ChunkRef = new TerrainChunkRef();
            if (Block != null)
            {
                Block.GetChunkFromGlobalPosition(NewChunk, ref ChunkRef);
            }

            this.ChunkRef = ChunkRef;

            if (ChunkRef.IsValid())
            {
                Datas = Block.Voxels.GetCustomData<CKDatas>(Manager.InternalDataName);
            }
            else
                Datas = null;

            if (LastSystem != CurrentSystem && CurrentSystem != null)
            {
                LastSystem = CurrentSystem;
                LastSystem.EnableRaycast();
            }
        }
        else
            this.ChunkRef = TerrainChunkRef.Null;
    }

    private void Update()
    {
        if (Manager == null || !Manager.IsLoaded)
            return;
        
        if (MustApplyMesh)
        {
            MustApplyMesh = false;
            ApplyMesh();
        }

        if (!Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
                SetType(TypeId + 1);
            else if (Input.GetAxis("Mouse ScrollWheel") < 0)
                SetType(TypeId - 1);
        }

        if (MouseIsOnGUI)
            return;

        ToolSystem NewSystem = LastGUISelected;
        bool Change = false;


        foreach (ToolSystem System in Systems)
        {
            if (System.IsActiveTool())
            {
                NewSystem = System;
                break;
            }
        }

        if (Input.GetKey(KeyCode.Escape) || NewSystem==null) //TODO newsystem being null may be a bug with magicwand / objectTool
        {
            LastGUISelected = null;
            NewSystem = DefaultTool;
        }


        if (CurrentSystem != null && CurrentSystem != NewSystem)
            CurrentSystem.DisableTool();

        if (CurrentSystem != NewSystem)
        {
            UpdateDrag(true);			
            CurrentSystem = NewSystem;

            NewSystem.EnableTool();
            Change = true;
        }
        
        if (!Change && LastSystem == CurrentSystem)
            UpdateDrag(false);

        if (PreviewObject.activeInHierarchy != ChunkRef.IsValid())
            PreviewObject.SetActive(ChunkRef.IsValid());

        if (Manager._Modifications.Count != 0)
            Manager.Flush(Manager._Modifications, true, true);

        if (!ChunkRef.IsValid())
            return;

        if (Input.GetKeyDown(KeyCode.R))
            SetRotation(Rotation + 1);

        if (CurrentSystem != null)
            CurrentSystem.InterfaceUpdate();

        if (Change)
            CurrentSystem.Preview(true);
    }

    public VectorI3 GetVoxelFromWorldPosition(Vector3 WorldPosition,TerrainBlock Block, int ChunkId)
    {
        return (WorldPosition - Block.GetChunkWorldPosition(ChunkId)) / TerrainManager.Instance.Informations.Scale;
    }

    public void SetType(int Value)
    {
        if (TypeId == Value)
            return;

        TypeId = Value;

        if (TypeId < 0) TypeId = 0;
        else if (TypeId >= Manager.Voxels.Length) TypeId = Manager.Voxels.Length - 1;

        foreach (ToolSystem System in Systems)
            System.OnSelectType(Value);
    }

    public void SetTexture(int Value)
    {
        if (TextureId == Value)
            return;

        TextureId = Value;

        foreach (ToolSystem System in Systems)
            System.OnSelectTexture(Value);
    }

    public void SetRotation(int Value)
    {
        if (Rotation == Value)
            return;

        Rotation = Value;

        if (Rotation > 3) Rotation = 0;
        else if (Rotation < 0) Rotation = 3;

        foreach (ToolSystem System in Systems)
            System.OnRotateObject(Value);
    }

    public void ApplyMesh()
    {
        if (Temp == null)
            return;

        TMesh.Clear();
        TempMeshArray Array = new TempMeshArray();

        CKChunkProcessor.GenerateMesh(Manager, Temp, TMesh, ref Array, Temp.GetLength(0) - 2, Temp[0].GetLength(0) - 2,
                             Temp[0][0].GetLength(0) - 2, 0, 0, 0, TerrainManager.Instance.Informations.Scale);
        TMesh.RecalculateTangents();

        MeshRenderer Renderer = PreviewObject.GetComponent<MeshRenderer>();
        MeshFilter Filter = PreviewObject.GetComponent<MeshFilter>();
        if (Filter.sharedMesh == null)
            Filter.sharedMesh = new Mesh();

        Filter.sharedMesh.Clear();
        Filter.sharedMesh.vertices = TMesh.Vertices.ToArray();
        Filter.sharedMesh.subMeshCount = Array.Materials.Length;
        for (int i = 0; i < Array.Materials.Length; ++i)
            Filter.sharedMesh.SetIndices(Array.Indices[i], MeshTopology.Triangles, i);
        Filter.sharedMesh.uv = TMesh.Uvs.ToArray();
        Filter.sharedMesh.normals = TMesh.Normals.ToArray();
        Filter.sharedMesh.tangents = TMesh.Tangents.ToArray();

        Renderer.sharedMaterials = Array.Materials;
    }

    public void ApplyObjectData(ObjectDatas Data)
    {
        foreach (ToolSystem System in Systems)
            System.OnObjectLoaded(Data);
    }

    public bool IsDragging;
    public Vector3 StartDragPosition;

    public void UpdateDrag(bool ForceEnd)
    {
        if (Input.GetMouseButton(0) && !ForceEnd)
        {
            if (!IsDragging)
            {
                IsDragging = true;
                StartDragPosition = point;
                if (CurrentSystem != null && ChunkRef.IsValid())
                    //script null check to avoid bugs when going from linetool to simpletool
                    CurrentSystem.OnStartDrag();
            }
        }
        else
        {
            if (IsDragging)
            {
                IsDragging = false;
                if (CurrentSystem != null)
                    CurrentSystem.OnEndDrag();
                StartDragPosition = new Vector3();
            }
        }
    }

    public void ClearAll()
    {
        /*foreach (ObjectChunk Chunk in Manager.Chunks.Values)
        {
            Chunk.ClearVoxels(true);
            Chunk.Dirty = true;
            Manager.AddChunkToGenerate(Chunk);
        }*/
    }
}