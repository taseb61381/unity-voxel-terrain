﻿using TerrainEngine.ConstructionKit;
using UnityEngine;

public class ObjectTool : ToolSystem
{
	public ObjectDatasRotation RData;
	public Vector3 Offset;

	public bool DoubleSize = false;

	public bool NoBlank = true; // do not replace anything with empty blocks

	public GameObject SaveObjectPopup;

	public override bool IsActiveTool()
	{
		return RData.Data != null;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			RData.Data = null;
	}

	public override void DisableTool()
	{
		Offset = new Vector3();
		RData.Data = null;
	}

	public override void EnableTool()
	{
	}

	public override void InterfaceUpdate()
	{
		if (DoubleSize)
		{
			InterfaceUpdateX2();
			return;
		}

        Interface.PreviewObject.transform.position = Interface.point + Offset - new Vector3(0.5f,0.5f,0.5f);

		if (Input.GetKeyDown(KeyCode.Return))
		{
			if (RData.Data != null)
			{

				SaveObjectPopup.SetActive(true); 
				/*RData.Data.Name = "I_LOVE_THE_CK";
				RData.Data.Author = "Enomi";
				RData.Data.CreationDate = DateTime.UtcNow.ToShortDateString();
				SaveManager.AddObjectToWrite(RData.Data);	*/
			}
			
		}
		else if (Input.GetMouseButtonDown(0))
		{
			Modify(false);
		}
		else if (Input.GetKeyDown(KeyCode.Delete))
		{
			Modify(true);
		}
	}
	
	private void Modify(bool clear){
        Interface.point = Interface.GetVoxelFromWorldPosition(Interface.point + Offset, Interface.Block, Interface.ChunkRef.ChunkId);
			int x, y, z;
			CKVoxelModification Vm = new CKVoxelModification();

			for (x = 0; x < Temp.GetLength(0); ++x)
				for (y = 0; y < Temp[0].GetLength(0); ++y)
					for (z = 0; z < Temp[0][0].GetLength(0); ++z)
					{
						byte type = Temp[x][y][z].Type;
						if ((clear || NoBlank) && type == 0) continue;
						
						Vm.Chunk = Interface.ChunkRef;
						Vm.x = (int) Interface.point.x + x - 1;
						Vm.y = (int) Interface.point.y + y - 1;
						Vm.z = (int) Interface.point.z + z - 1;
						if (!clear)
						{ 
							Vm.Type = type;
							Vm.Texture = Temp[x][y][z].Texture;
							Vm.Rotation = Temp[x][y][z].Rotation;
						}
                        Interface.Manager._Modifications.Enqueue(Vm);
					}
	}

	//quick and dirty object size doubler - slopes will be stupidly doubled and wrong, but it can be fixed by hand 
	private void InterfaceUpdateX2()
	{
        Interface.PreviewObject.transform.position = Interface.point + Offset - new Vector3(0.5f, 0.5f, 0.5f);
		if (Input.GetKeyDown(KeyCode.Return))
		{
            Interface.point = Interface.GetVoxelFromWorldPosition(Interface.point + Offset, Interface.Block, Interface.ChunkRef.ChunkId);
			int x, y, z;
			int dx, dy, dz;

			for (x = 0; x < Temp.GetLength(0); ++x)
				for (y = 0; y < Temp[0].GetLength(0); ++y)
					for (z = 0; z < Temp[0][0].GetLength(0); ++z)
					{
						dx = (int) Interface.point.x + 2*x - 1;
						dy = (int) Interface.point.y + 2*y - 1;
						dz = (int) Interface.point.z + 2*z - 1;

						EnqueueMod(x, y, z, dx, dy, dz);

						EnqueueMod(x, y, z, dx + 1, dy, dz);
						EnqueueMod(x, y, z, dx, dy + 1, dz);
						EnqueueMod(x, y, z, dx, dy, dz + 1);

						EnqueueMod(x, y, z, dx + 1, dy + 1, dz);
						EnqueueMod(x, y, z, dx, dy + 1, dz + 1);
						EnqueueMod(x, y, z, dx + 1, dy, dz + 1);

						EnqueueMod(x, y, z, dx + 1, dy + 1, dz + 1);
					}
		}
	}

	private void EnqueueMod(int x, int y, int z, int dx, int dy, int dz)
	{
		CKVoxelModification Vm = new CKVoxelModification();

		Vm.x = dx;
		Vm.y = dy;
		Vm.z = dz;

		Vm.Chunk = Interface.ChunkRef;
		Vm.Type = Temp[x][y][z].Type;
		Vm.Texture = Temp[x][y][z].Texture;
		Vm.Rotation = Temp[x][y][z].Rotation;

        Interface.Manager._Modifications.Enqueue(Vm);
	}

	public override void OnSelectType(int TypeId)
	{
		RData.Data = null;
	}

	public override void OnRotateObject(int Rotation)
	{
		ApplyObject(RData.Data);
	}

	public override void OnObjectLoaded(ObjectDatas Data)
	{
		ApplyObject(Data);
		Interface.LastGUISelected = this;

	}

	public void ApplyObject(ObjectDatas Data)
	{
		if (Data == null)
			return;

		RData = new ObjectDatasRotation();
		RData.Init(Data, (byte) Interface.Rotation);
		ResizeTool(RData.SizeX, RData.SizeY, RData.SizeZ);
		int x, y, z;
		int Rotation = 0;

		for (x = 0; x < RData.SizeX; ++x)
			for (y = 0; y < RData.SizeY; ++y)
				for (z = 0; z < RData.SizeZ; ++z)
				{
					RData.GetValues(x, y, z, ref Temp[x + 1][y + 1][z + 1].Type, ref Temp[x + 1][y + 1][z + 1].Texture,
					                ref Temp[x + 1][y + 1][z + 1].Rotation);
					Rotation = Temp[x + 1][y + 1][z + 1].Rotation + Interface.Rotation;
					if (Rotation > 3)
						Rotation -= 4;

					Temp[x + 1][y + 1][z + 1].Rotation = (byte) Rotation;
					Temp[x + 1][y + 1][z + 1].Info = Interface.Manager.Voxels[Temp[x + 1][y + 1][z + 1].Type];
					Temp[x + 1][y + 1][z + 1].TInfo = Interface.Manager.Textures[Temp[x + 1][y + 1][z + 1].Texture];
				}

		Offset = new Vector3(-RData.SizeX/2, 0, -RData.SizeZ/2);
		Preview();
	}

	public override void OnSelectTexture(int TextureId)
	{
		Debug.Log("Select Texture :" + TextureId);

		if (RData.Data == null)
			return;

		int x, y, z;
		for (x = 0; x < RData.SizeX; ++x)
			for (y = 0; y < RData.SizeY; ++y)
				for (z = 0; z < RData.SizeZ; ++z)
				{
					Temp[x + 1][y + 1][z + 1].Texture = (byte) TextureId;
					Temp[x + 1][y + 1][z + 1].TInfo = Interface.Manager.Textures[TextureId];
				}
		Preview(true);
	}


	public override bool IsReplaceMode()
	{
		return ReplaceMode || Input.GetKey(KeyCode.LeftControl);
		//for objecttool, do not assume replacemode when no type selected .
	}
}