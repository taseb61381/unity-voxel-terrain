using UnityEngine;

public class LightTool : ToolSystem
{

	public GameObject LightPrefab;
	public int LightTextureID;
	public int MaxLights = 5;


	public override void EnableTool()
	{
		ReplaceMode = true;
		Interface.PreviewObject.GetComponent<MeshFilter>().sharedMesh.Clear();

		RefreshLight();
	}
	
	public override void DisableTool() {
		
	}


	private void RefreshLight()
	{

		/*int lightCount=1;

		int x, y, z;
		Vector3 lightWorldPosition;
	
		foreach (ObjectChunk chunk in Interface.Manager.Chunks.Values)
		{
			for (y = 0; y < ObjectChunk.ChunkSizeY; ++y)
			{				
				for (x = 0; x < ObjectChunk.ChunkSizeX; ++x)
				{
					for (z = 0; z < ObjectChunk.ChunkSizeZ; ++z)
					{
						if (lightCount > MaxLights)
						{
							Debug.Log( "max light reached ("+MaxLights+" lights)" );//todo limit light per region not whole world, and combine adjacent light in one big spotlight. 
							break;
						}

						if (chunk.GetTexture(x, y, z) == LightTextureID)
						{
							lightWorldPosition = chunk.WorldPosition + new Vector3(x, y, z) + new Vector3(0.5f, -0.1f, 0.5f);

							Quaternion lightOrient = Quaternion.LookRotation(Vector3.down); //todo get light block face normal, depends on shape (roof vs cube)

							GameObject lightInstance = (GameObject) Instantiate(LightPrefab, lightWorldPosition, lightOrient);
							if (chunk.HasScript)
							{
								lightInstance.transform.parent = chunk.Script.transform;
							}
							lightInstance.SetActive(true);

						}						
					}
				}
			}
		}*/

	}

	public override void InterfaceUpdate()
	{		
		Preview(true);		
	}

	public override void Preview(bool Now = false)
	{
		base.Preview(Now);
	}
}