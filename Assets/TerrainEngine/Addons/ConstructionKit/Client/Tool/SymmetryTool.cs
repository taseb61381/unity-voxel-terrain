using UnityEngine;

public class SymmetryTool : ToolSystem
{
	public GameObject Axis;

	public bool Odd;

	private void Awake()
	{
		Axis.SetActive(false);
	}

    public override bool IsActiveTool()
    {
        return false;
    }

	public override void EnableTool()
	{
		Interface.PreviewObject.GetComponent<MeshFilter>().sharedMesh.Clear();
		Axis.SetActive(true);
	}
	
	public override void DisableTool()
	{
		Axis.SetActive(false);
	}
	
	public override void OnStartDrag()
	{
		/*Vector3 axisWorld = Interface.point;

		Vector3 axisOrientation = Axis.transform.forward;

		int xOrient = (int) axisOrientation.z;

		int zOrient = (int) axisOrientation.x*-1;

		int x, y, z;

		Vector3 currentWorld;

		foreach (ObjectChunk chunk in Interface.Manager.Chunks.Values)
		{
			for (x = 0; x < ObjectChunk.ChunkSizeX; ++x)
			{
				for (z = 0; z < ObjectChunk.ChunkSizeZ; ++z)
				{
					for (y = 1; y < ObjectChunk.ChunkSizeY; ++y)
					{
						currentWorld = chunk.WorldPosition + new Vector3(x, y, z);

						if (xOrient == 1 && currentWorld.x < axisWorld.x) continue;
						if (xOrient == -1 && currentWorld.x > axisWorld.x) continue;

						if (zOrient == 1 && currentWorld.z < axisWorld.z) continue;
						if (zOrient == -1 && currentWorld.z > axisWorld.z) continue;

						byte currentType = 0;
						byte currentTex = 0;
						byte currentRot = 0;

                        chunk.GetValues(x, y, z, ref currentType, ref currentTex, ref currentRot);

						if (currentType != 0)
						{
							Vector3 mirrorWorld = currentWorld;

							if (xOrient != 0)
							{
								mirrorWorld.x = axisWorld.x + axisWorld.x - currentWorld.x;
								if (Odd) mirrorWorld.x -= xOrient;
							}
							
							if (zOrient != 0)
							{
								mirrorWorld.z = axisWorld.z + axisWorld.z - currentWorld.z;
								if (Odd) mirrorWorld.z -= zOrient;
							}

							VoxelModification vm = new VoxelModification();
							//TODO generalize shapes rotations when mirroring 
							
							if (currentType == 9 && currentRot == 3) vm.Rotation = 0;
							else if (currentType == 9 && currentRot == 2) vm.Rotation = 1;
							else if (currentType == 9 && currentRot == 0) vm.Rotation = 3;
							else if (currentType == 10 && currentRot == 0) vm.Rotation = 3;
							else if (currentType == 10 && currentRot == 2) vm.Rotation = 3;
							else if (currentType == 4 && currentRot == 3) vm.Rotation = 1;
							else if (currentType == 4 && currentRot == 1) vm.Rotation = 3;
							else if (currentType == 13 && currentRot == 3) vm.Rotation = 1;
							else if (currentType == 13 && currentRot == 1) vm.Rotation = 3;
							else if (currentType == 11 && currentRot == 0) vm.Rotation = 1;
							else if (currentType == 5 && currentRot == 1) vm.Rotation = 3;
							else if (currentType == 5 && currentRot == 3) vm.Rotation = 1;
							else if (currentType == 12 && currentRot == 1) vm.Rotation = 3;
							else vm.Rotation = currentRot;
							
							vm.Texture = currentTex;
							vm.Type = currentType;
							mirrorWorld.x = (int) mirrorWorld.x;
							mirrorWorld.y = (int) mirrorWorld.y;
							mirrorWorld.z = (int) mirrorWorld.z;
							Vector3 chunkPos = Interface.Manager.GetChunkPositionFromWorldPosition(mirrorWorld);
							vm.Chunk = Interface.Manager.GetChunk(chunkPos);

							//Debug.Log(currentWorld + " " + axisWorld + " " + mirrorWorld + " / " + chunkPos);

							vm.x = (int) mirrorWorld.x%ObjectChunk.ChunkSizeX;
							vm.y = (int) mirrorWorld.y%ObjectChunk.ChunkSizeY;
							vm.z = (int) mirrorWorld.z%ObjectChunk.ChunkSizeZ;

							if (vm.Chunk != null)
							{
								Interface._Modifications.Enqueue(vm);
							}
							//TODO create missing chunks
						}
					}
				}
			}
		}*/
	}

	public override void InterfaceUpdate()
	{
		if (Input.GetKeyDown(KeyCode.KeypadPlus))
			Axis.transform.localScale += Vector3.one/2;
		else if (Input.GetKeyDown(KeyCode.KeypadMinus))
			Axis.transform.localScale -= Vector3.one/2;

		//Axis.transform.position = Interface.point;		
        Axis.transform.position = Interface.PreviewObject.transform.position = Interface.point;

		Preview(true);
	}

	public override void Preview(bool Now = false)
	{
		base.Preview(Now);
	}

	public override void OnRotateObject(int rotation)
	{
		Axis.transform.Rotate(Axis.transform.up, 90);
	}
}