using TerrainEngine.ConstructionKit;
using UnityEngine;

public class CopyTool : ToolSystem 
{
    public GameObject PlaneA, PlaneB, PlaneC, PlaneD;
    public TextMesh SideText;
    public Vector3 StartPosition;
    public Color GUIColor = Color.black;

    public override void EnableTool()
    {
        Interface.PreviewObject.GetComponent<MeshFilter>().sharedMesh.Clear();
    }

    public override void OnStartDrag()
    {
        PlaneA.SetActive(true);
        PlaneB.SetActive(true);
        PlaneC.SetActive(true);
        PlaneD.SetActive(true);

        StartPosition = Interface.GetVoxelFromWorldPosition(Interface.point, Interface.Block,Interface.ChunkRef.ChunkId) + Interface.WorldPosition;
        if (SideText != null)
            SideText.GetComponent<Renderer>().material.color = GUIColor;
    }

    public override void OnEndDrag()
    {
        PlaneA.SetActive(false);
        PlaneB.SetActive(false);
        PlaneC.SetActive(false);
        PlaneD.SetActive(false);

        if (!Interface.ChunkRef.IsValid())
            return;

        Vector3 P = Interface.GetVoxelFromWorldPosition(Interface.point, Interface.Block, Interface.ChunkRef.ChunkId) + Interface.WorldPosition;
        P.x = (int)P.x;
        P.y = (int)P.y;
        P.z = (int)P.z;

        int ex = (int)Mathf.Max(StartPosition.x, P.x);
        int ey = (int)Mathf.Max(StartPosition.y, P.y);
        int ez = (int)Mathf.Max(StartPosition.z, P.z);

        int sx = (int)Mathf.Min(StartPosition.x, P.x);
        int sy = (int)Mathf.Min(StartPosition.y, P.y);
        int sz = (int)Mathf.Min(StartPosition.z, P.z);
        
        int X = Mathf.Abs(ex - sx);
        int Y = Mathf.Abs(ey - sy);
        int Z = Mathf.Abs(ez - sz);

        ObjectDatas Data = new ObjectDatas();
        Data.SizeX = X+1;
        Data.SizeY = Y+1;
        Data.SizeZ = Z+1;
        Data.InitArrays();

        int x, y, z;
        for (x = sx; x <= ex; ++x)
            for (y = sy; y <= ey; ++y)
                for (z = sz; z <= ez; ++z)
                {
                    Interface.Manager.GetValues(Interface.Block, Interface.Datas, x - (int)Interface.WorldPosition.x,
                        y - (int)Interface.WorldPosition.y,
                        z - (int)Interface.WorldPosition.z,
                        ref Data.Types[x - sx, y - sy, z - sz], ref Data.Textures[x - sx, y - sy, z - sz], ref Data.Rotations[x - sx, y - sy, z - sz]);
                }

        if (Data.Optimize())
        {
            Interface.ApplyObjectData(Data);
            if (Interface.LastGUISelected == this)
                Interface.LastGUISelected = null;
        }
    }

    public override void InterfaceUpdate()
    {
        if (!Interface.IsDragging)
            return;

        if (SideText != null)
        {
            SideText.transform.position = Interface.point + new Vector3(0, 3, 0);
            SideText.transform.LookAt(Interface.CurrentCam.transform);
            SideText.transform.Rotate(0, 180, 0);
            SideText.transform.position -= SideText.transform.forward * 1;
        }

        Vector3 P = Interface.GetVoxelFromWorldPosition(Interface.point, Interface.Block, Interface.ChunkRef.ChunkId) + Interface.WorldPosition;
        P.x = (int)P.x;
        P.y = (int)P.y;
        P.z = (int)P.z;

        int ex = (int)Mathf.Max(StartPosition.x, P.x);
        int ey = (int)Mathf.Max(StartPosition.y, P.y);
        int ez = (int)Mathf.Max(StartPosition.z, P.z);

        int sx = (int)Mathf.Min(StartPosition.x, P.x);
        int sy = (int)Mathf.Min(StartPosition.y, P.y);
        int sz = (int)Mathf.Min(StartPosition.z, P.z);

        int X = Mathf.Abs(ex - sx);
        int Y = Mathf.Abs(ey - sy);
        int Z = Mathf.Abs(ez - sz);

        if (SideText != null)
        {
            SideText.text = (X+1) + "x" + (Y+1) + "x" + (Z+1);
        }

        Interface.PreviewObject.transform.position = new Vector3((int)P.x < StartPosition.x ? (int)P.x : StartPosition.x,
            (int)P.y < StartPosition.y ? (int)P.y : StartPosition.y,
            (int)P.z < StartPosition.z ? (int)P.z : StartPosition.z) - new Vector3(0.5f, 0.5f, 0.5f);

        PlaneA.transform.position = PlaneB.transform.position = (Interface.PreviewObject.transform.position - new Vector3(0.05f, 0.05f, 0.05f)) + new Vector3(0, (Y + 1) * 0.5f, 0) - new Vector3(0.5f, 0.5f, 0.5f);
        PlaneC.transform.position = PlaneD.transform.position = (Interface.PreviewObject.transform.position + new Vector3((X + 1) + 0.05f, (Y + 1) + 0.05f, (Z + 1) + 0.05f)) - new Vector3(0, (Y + 1) * 0.5f, 0) - new Vector3(0.5f, 0.5f, 0.5f);

        PlaneA.transform.localScale = PlaneB.transform.localScale = PlaneC.transform.localScale = PlaneD.transform.localScale = new Vector3(100, 1, (Y + 1) * 0.15f);
    }

    public override void Preview(bool Now = false)
    {
        if (!Interface.IsDragging)
            return;

        base.Preview(Now);
    }

   
}
