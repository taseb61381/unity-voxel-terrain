using System;
using UnityEngine;


public class LineTool : ToolSystem
{
    public Vector3 StartPosition;

    public override void EnableTool()
    {
        Interface.PreviewObject.GetComponent<MeshFilter>().sharedMesh.Clear();
    }

    public override void OnStartDrag()
    {
        StartPosition = Interface.GetVoxelFromWorldPosition(Interface.point, Interface.Block, Interface.ChunkRef.ChunkId) + Interface.Block.GetChunkWorldPosition(Interface.ChunkRef.ChunkId);
    }

    public override void OnEndDrag()
    {
        if (!Interface.ChunkRef.IsValid())
            return;

        Vector3 P = Interface.GetVoxelFromWorldPosition(Interface.point, Interface.Block, Interface.ChunkRef.ChunkId) + Interface.Block.GetChunkWorldPosition(Interface.ChunkRef.ChunkId);
        P.x = (int)P.x;
        P.y = (int)P.y;
        P.z = (int)P.z;

        int ex = (int)Mathf.Max(StartPosition.x, P.x);
        int ey = (int)Mathf.Max(StartPosition.y, P.y);
        int ez = (int)Mathf.Max(StartPosition.z, P.z);

        int sx = (int)Mathf.Min(StartPosition.x, P.x);
        int sy = (int)Mathf.Min(StartPosition.y, P.y);
        int sz = (int)Mathf.Min(StartPosition.z, P.z);

        Interface.PreviewObject.transform.position = new Vector3((int)P.x < StartPosition.x ? (int)P.x : StartPosition.x,
            (int)P.y < StartPosition.y ? (int)P.y : StartPosition.y,
            (int)P.z < StartPosition.z ? (int)P.z : StartPosition.z);

        int X = Mathf.Abs(ex - sx) + 1;
        int Y = Mathf.Abs(ey - sy) + 1;
        int Z = Mathf.Abs(ez - sz) + 1;

        CKVoxelModification Vm = new CKVoxelModification();
        Vm.Chunk = Interface.ChunkRef;
        Raycast(X, Y, Z, P, (int x, int y, int z)
            =>
            {
                Vm.x = x + (int)sx - (int)Interface.WorldPosition.x;
                Vm.y = y + (int)sy - (int)Interface.WorldPosition.y;
                Vm.z = z + (int)sz - (int)Interface.WorldPosition.z;
                Vm.Type = (byte)Interface.TypeId;
                Vm.Texture = (byte)Interface.TextureId;
                Vm.Rotation = (byte)Interface.Rotation;
                Interface.Manager._Modifications.Enqueue(Vm);
            });

        Temp = null;
        Interface.PreviewObject.GetComponent<MeshFilter>().sharedMesh.Clear();
    }

    Vector3 LastP;
    public override void InterfaceUpdate()
    {
        if (!Interface.IsDragging)
            return;

        Vector3 P = Interface.GetVoxelFromWorldPosition(Interface.point, Interface.Block, Interface.ChunkRef.ChunkId) + Interface.WorldPosition;
        P.x = (int)P.x;
        P.y = (int)P.y;
        P.z = (int)P.z;
        if (LastP == P)
            return;

        LastP = P;

        int ex = (int)Mathf.Max(StartPosition.x, P.x);
        int ey = (int)Mathf.Max(StartPosition.y, P.y);
        int ez = (int)Mathf.Max(StartPosition.z, P.z);

        int sx = (int)Mathf.Min(StartPosition.x, P.x);
        int sy = (int)Mathf.Min(StartPosition.y, P.y);
        int sz = (int)Mathf.Min(StartPosition.z, P.z);

        Interface.PreviewObject.transform.position = new Vector3((int)P.x < StartPosition.x ? (int)P.x : StartPosition.x,
            (int)P.y < StartPosition.y ? (int)P.y : StartPosition.y,
            (int)P.z < StartPosition.z ? (int)P.z : StartPosition.z) - new Vector3(0.5f, 0.5f, 0.5f);

        int X = Mathf.Abs(ex - sx)+1;
        int Y = Mathf.Abs(ey - sy)+1;
        int Z = Mathf.Abs(ez - sz)+1;

        ResizeTool(X, Y, Z);
        Fill(X, Y, Z, 0, 0, 0);

        Raycast(X, Y, Z, P, ApplyToTemp);

        Preview(true);
    }

    public override void Preview(bool Now = false)
    {
        if (!Interface.IsDragging)
            return;

        base.Preview(Now);
    }

    public void ApplyToTemp(int x, int y, int z)
    {
        Temp[x + 1][y + 1][z + 1].Init((byte)Interface.TypeId, (byte)Interface.TextureId, (byte)Interface.Rotation, Interface.Manager.Voxels[Interface.TypeId], Interface.Manager.Textures[Interface.TextureId]);
    }

    public delegate void Hit(int x,int y,int z);

    public void Raycast(int X,int Y,int Z, Vector3 P, Hit OnHit)
    {
        int x = (int)StartPosition.x;
        int y = (int)StartPosition.y;
        int z = (int)StartPosition.z;

        Vector3 Direction = (P - StartPosition).normalized;

        int stepX = Direction.x < 0 ? -1 : Direction.x > 0 ? 1 : 0;
        int stepY = Direction.y < 0 ? -1 : Direction.y > 0 ? 1 : 0;
        int stepZ = Direction.z < 0 ? -1 : Direction.z > 0 ? 1 : 0;

        float tMaxx = ((x + (stepX > 0 ? 1 : 0)) - x) / Direction.x;
        float tMaxy = ((y + (stepY > 0 ? 1 : 0)) - y) / Direction.y;
        float tMaxz = ((z + (stepZ > 0 ? 1 : 0)) - z) / Direction.z;


        if (Single.IsNaN(tMaxx)) tMaxx = Single.PositiveInfinity;
        if (Single.IsNaN(tMaxy)) tMaxy = Single.PositiveInfinity;
        if (Single.IsNaN(tMaxz)) tMaxz = Single.PositiveInfinity;

        float tDeltax = (float)stepX / Direction.x;
        float tDeltay = (float)stepY / Direction.y;
        float tDeltaz = (float)stepZ / Direction.z;

        if (Single.IsNaN(tDeltax)) tDeltax = Single.PositiveInfinity;
        if (Single.IsNaN(tDeltay)) tDeltay = Single.PositiveInfinity;
        if (Single.IsNaN(tDeltaz)) tDeltaz = Single.PositiveInfinity;

        int px, py, pz;
        for (int i = 0; i < 999; i++)
        {
            px = (int)P.x < StartPosition.x ? (int)StartPosition.x - x : x - (int)StartPosition.x;
            py = (int)P.y < StartPosition.y ? (int)StartPosition.y - y : y - (int)StartPosition.y;
            pz = (int)P.z < StartPosition.z ? (int)StartPosition.z - z : z - (int)StartPosition.z;
            if ((int)P.x < StartPosition.x) px = X - px - 1;
            if ((int)P.y < StartPosition.y) py = Y - py - 1;
            if ((int)P.z < StartPosition.z) pz = Z - pz - 1;

            if (px < 0 || py < 0 || pz < 0 || px >= X || py >= Y || pz >= Z)
                break;

            OnHit(px, py, pz);
            if (tMaxx < tMaxy && tMaxx < tMaxz)
            {
                x += stepX;
                tMaxx += tDeltax;
            }
            else if (tMaxy < tMaxz)
            {
                y += stepY;
                tMaxy += tDeltay;
            }
            else
            {
                z += stepZ;
                tMaxz += tDeltaz;

            }
        }
    }

}
