﻿using UnityEngine;

public class SimpleTool : ToolSystem
{
	public int SizeX = 0, SizeY = 0, SizeZ = 0;
	public bool Sphere = false;

	 public bool PaintTex = true;
	 public bool PaintShape = true;
	 public bool PaintRotation = true;

	public override void Init()
	{
		ResizeTool(SizeX, SizeY, SizeZ);
		Fill(SizeX, SizeY, SizeZ);
		Preview(false);
	}

	public override void EnableTool()
	{
		MeshFilter Filter = Interface.PreviewObject.GetComponent<MeshFilter>();
		if (Filter.sharedMesh == null)
			Filter.sharedMesh = new Mesh();
		else
			Filter.sharedMesh.Clear();

		Fill(SizeX, SizeY, SizeZ);
		Preview(false);
	}

	public override void InterfaceUpdate()
	{
		if (Input.GetKeyDown(KeyCode.KeypadPlus))
			SetSize(SizeX + 1, SizeY + 1, SizeZ + 1);
		else if (Input.GetKeyDown(KeyCode.KeypadMinus))
			SetSize(SizeX - 1, SizeY - 1, SizeZ - 1);

		if (Input.GetKey(KeyCode.LeftShift))
		{
			if (Input.GetAxis("Mouse ScrollWheel") > 0)
				SetSize(SizeX, SizeY + 1, SizeZ);
			else if (Input.GetAxis("Mouse ScrollWheel") < 0)
				SetSize(SizeX, SizeY - 1, SizeZ);
		}

		Interface.PreviewObject.transform.position = Interface.GetWorldPosition(Interface.point) - Interface.ScaleOffset;

		if (Input.GetMouseButtonDown(0))
		{
			Modify(false);
		} else if (Input.GetKeyDown(KeyCode.Delete))
		{
			Modify(true);
		}
		
	}

	private void Modify(bool clear)
	{
		Interface.point = Interface.GetVoxelFromWorldPosition(Interface.point, Interface.Block, Interface.ChunkRef.ChunkId);

		byte currentType = 0, currentTex = 0, currentRot = 0;
		Interface.Manager.GetValues(Interface.Block, Interface.Datas, (int)Interface.point.x, (int)Interface.point.y, (int)Interface.point.z, ref currentType,
																ref currentTex, ref currentRot);

		int x, y, z;
		CKVoxelModification Vm = new CKVoxelModification();
		for (x = 1; x <= Temp.GetLength(0) - 2; ++x)
			for (y = 1; y <= Temp[0].GetLength(0) - 2; ++y)
				for (z = 1; z <= Temp[0][0].GetLength(0) - 2; ++z)
				{
					Vm.Chunk = Interface.ChunkRef;
					Vm.x = (int)(Interface.point.x + x - 1);
					Vm.y = (int)(Interface.point.y + y - 1);
					Vm.z = (int)(Interface.point.z + z - 1);

					if (clear)
					{
						Interface.Manager._Modifications.Enqueue(Vm);
						continue;
					}

					if (PaintShape)
						Vm.Type = (byte)Temp[x][y][z].Type;
					else
						Vm.Type = currentType;

					if (PaintTex)
						Vm.Texture = (byte)Temp[x][y][z].Texture;
					else
						Vm.Texture = currentTex;

					if (PaintRotation)
						Vm.Rotation = (byte)Temp[x][y][z].Rotation;
					else
						Vm.Rotation = currentRot;

					Interface.Manager._Modifications.Enqueue(Vm);
				}

	}
	
	public void SetSize(int X, int Y, int Z)
	{
		X = Mathf.Clamp(X, 1, int.MaxValue);
		Y = Mathf.Clamp(Y, 1, int.MaxValue);
		Z = Mathf.Clamp(Z, 1, int.MaxValue);

		this.SizeX = X;
		this.SizeY = Y;
		this.SizeZ = Z;

		if (!Sphere)
		{
			ResizeTool(SizeX, SizeY, SizeZ);
			Fill(SizeX, SizeY, SizeZ);
		}
		else
		{
			FillSphere();
		}

		if(IsCurrentTool())
			Preview();
	}

	public void FillSphere()
	{
		CKVoxelInformation Info = Interface.Manager.Voxels[(byte)Interface.TypeId];
		TextureInformation TInfo = Interface.Manager.Textures[(byte)Interface.TextureId];

		int radius = SizeY;

		Vector3 p = new Vector3(radius,radius,radius);
		int x1 = (int)p.x - radius;
		int y1 = (int)p.y - radius;
		int z1 = (int)p.z - radius;
		int x2 = (int)p.x + radius;
		int y2 = (int)p.y + radius;
		int z2 = (int)p.z + radius;
		ResizeTool(radius * 2 + 1, radius * 2 + 1, radius * 2 + 1);

		Vector3 v;
		float bpos;
		for (int px = x1; px < x2; ++px)
		{
			for (int py = y1; py < y2; ++py)
			{
				for (int pz = z1; pz < z2; ++pz)
				{
					v.x = px - p.x;
					v.y = py - p.y;
					v.z = pz - p.z;
					bpos = radius - v.magnitude;
					if (bpos > 0f)
					{
						Temp[px + 1][py + 1][pz + 1].Init((byte)Interface.TypeId, (byte)Interface.TextureId, (byte)Interface.Rotation, Info, TInfo);
					}
					else
						Temp[px + 1][py + 1][pz + 1].Init(0, 0, 0, null, null);
				}
			}
		}
	}

	public override void OnSelectType(int TypeId)
	{
		if (!Sphere)
			Fill(SizeX, SizeY, SizeZ);
		else
			FillSphere();

		if (IsCurrentTool())
			Preview();
	}

	public override void OnSelectTexture(int TextureId)
	{
		if (!Sphere)
			Fill(SizeX, SizeY, SizeZ);
		else
			FillSphere();

		if (IsCurrentTool())
		{
			if (Interface.TypeId == 0) Interface.TypeId = 1; //when you select a texture there is a good chance you want to do something with it

			  Preview();
		}
		   
	}

	public override void OnRotateObject(int Rotation)
	{
		if (!Sphere)
			Fill(SizeX, SizeY, SizeZ);
		else
			FillSphere();

		if (IsCurrentTool())
			Preview();
	}
}
