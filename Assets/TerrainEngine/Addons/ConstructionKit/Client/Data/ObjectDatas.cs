using System.IO;
using UnityEngine;

namespace TerrainEngine.ConstructionKit
{
    // Store the Object Datas
    public class ObjectDatas
    {
        public delegate bool HasTime();

        public string Name;
        public string Author;
        public string CreationDate;
        public int PositionX;
        public int PositionY;
        public int PositionZ;
        public int SizeX;
        public int SizeY;
        public int SizeZ;

        public byte[, ,] Types;
        public byte[, ,] Textures;
        public byte[, ,] Rotations;

        public float LoadingProgress;

        public void InitArrays()
        {
            Types = new byte[SizeX, SizeY, SizeZ];
            Textures = new byte[SizeX, SizeY, SizeZ];
            Rotations = new byte[SizeX, SizeY, SizeZ];
        }

        public void Write(BinaryWriter Writer)
        {
            Writer.Write("Name:");
            Writer.Write(Name);
            Writer.Write(";");

            Writer.Write("Author:");
            Writer.Write(Author);
            Writer.Write("|");

            Writer.Write("Date:");
            Writer.Write(CreationDate);
            Writer.Write("|");

            Writer.Write("Position:");
            Writer.Write(PositionX.ToString());
            Writer.Write(",");
            Writer.Write(PositionY.ToString());
            Writer.Write(",");
            Writer.Write(PositionZ.ToString());
            Writer.Write("|");

            Writer.Write("Size:");
            Writer.Write(SizeX.ToString());
            Writer.Write(",");
            Writer.Write(SizeY.ToString());
            Writer.Write(",");
            Writer.Write(SizeZ.ToString());
            Writer.Write("|");

            int x, y, z;
            for (x = 0; x < SizeX; ++x)
            {
                for (y = 0; y < SizeY; ++y)
                {
                    for (z = 0; z < SizeZ; ++z)
                    {
                        if (Types[x, y, z] != 0)
                        {
                            Writer.Write(Types[x, y, z]);
                            Writer.Write(Textures[x, y, z]);
                            Writer.Write(Rotations[x, y, z]);
                        }
                        else
                            Writer.Write((byte)0);
                    }
                }
            }
        }

        public void Read(TextAsset Asset)
        {
            using (MemoryStream Stream = new MemoryStream(Asset.bytes))
            {
                using (BinaryReader Reader = new BinaryReader(Stream))
                {
                    int x = 0;
                    int y = 0;
                    int z = 0;
                    Read(Reader, ref x, ref y, ref z, Time);
                }
            }
        }

        public bool Time()
        {
            return true;
        }

        public bool Read(BinaryReader Reader, ref int x,ref int y,ref int z, HasTime HasTimeFunction)
        {
            if (x == 0 && y == 0 && z == 0)
            {
                LoadingProgress = 0;
                Reader.ReadString();
                Name = Reader.ReadString();
                Reader.ReadString();

                Reader.ReadString();
                Author = Reader.ReadString();
                Reader.ReadString();

                Reader.ReadString();
                CreationDate = Reader.ReadString();
                Reader.ReadString();

                Reader.ReadString();
                PositionX = int.Parse(Reader.ReadString());
                Reader.ReadString();
                PositionY = int.Parse(Reader.ReadString());
                Reader.ReadString();
                PositionZ = int.Parse(Reader.ReadString());
                Reader.ReadString();

                Reader.ReadString();
                SizeX = int.Parse(Reader.ReadString());
                Reader.ReadString();
                SizeY = int.Parse(Reader.ReadString());
                Reader.ReadString();
                SizeZ = int.Parse(Reader.ReadString());
                Reader.ReadString();

                InitArrays();
            }

            for (; x < SizeX; ++x)
            {
                for (y = 0; y < SizeY; ++y)
                {
                    for (z = 0; z < SizeZ; ++z)
                    {
                        Types[x, y, z] = Reader.ReadByte();

                        if (Types[x, y, z] != 0)
                        {
                            Textures[x, y, z] = Reader.ReadByte();
                            Rotations[x, y, z] = Reader.ReadByte();
                        }
                    }


                    LoadingProgress = (float)Reader.BaseStream.Position / (float)Reader.BaseStream.Length * 100f;
                }


                if (!HasTimeFunction())
                {
                    ++x;
                    return false;
                }
            }

            x = y = z = 0;
            return true;
        }

        public bool Optimize()
        {
            Vector3 MinVoxel = new Vector3(int.MaxValue, int.MaxValue, int.MaxValue), MaxVoxel = new Vector3(int.MinValue, int.MinValue, int.MinValue);
            int x, y, z;
            for (x = 0; x < SizeX; ++x)
            {
                for (y = 0; y < SizeY; ++y)
                {
                    for (z = 0; z < SizeZ; ++z)
                    {
                        if (Types[x,y,z] != 0)
                        {
                            if (x < MinVoxel.x) MinVoxel.x = x;
                            if (x > MaxVoxel.x) MaxVoxel.x = x;

                            if (y < MinVoxel.y) MinVoxel.y = y;
                            if (y > MaxVoxel.y) MaxVoxel.y = y;

                            if (z < MinVoxel.z) MinVoxel.z = z;
                            if (z > MaxVoxel.z) MaxVoxel.z = z;
                        }
                    }
                }
            }

            if (MinVoxel == new Vector3(int.MaxValue, int.MaxValue, int.MaxValue) || MaxVoxel == new Vector3(int.MinValue, int.MinValue, int.MinValue))
                return false;

            if (MinVoxel != Vector3.zero || MaxVoxel != new Vector3(SizeX, SizeY, SizeZ))
            {
                int NewSizeX = (int)(MaxVoxel.x - MinVoxel.x + 1);
                int NewSizeY = (int)(MaxVoxel.y - MinVoxel.y + 1);
                int NewSizeZ = (int)(MaxVoxel.z - MinVoxel.z + 1);

                //Debug.Log("NewSize = " + NewSizeX + "," + NewSizeY + "," + NewSizeZ + "|" + MinVoxel + "|" + MaxVoxel);

                byte[, ,] NewTypes = new byte[NewSizeX, NewSizeY, NewSizeZ];
                byte[, ,] NewTextures = new byte[NewSizeX, NewSizeY, NewSizeZ];
                byte[, ,] NewRotations = new byte[NewSizeX, NewSizeY, NewSizeZ];

                for (x = 0; x < NewSizeX; ++x)
                {
                    for (y = 0; y < NewSizeY; ++y)
                    {
                        for (z = 0; z < NewSizeZ; ++z)
                        {
                            NewTypes[x, y, z] = Types[x + (int)MinVoxel.x, y + (int)MinVoxel.y, z + (int)MinVoxel.z];
                            NewTextures[x, y, z] = Textures[x + (int)MinVoxel.x, y + (int)MinVoxel.y, z + (int)MinVoxel.z];
                            NewRotations[x, y, z] = Rotations[x + (int)MinVoxel.x, y + (int)MinVoxel.y, z + (int)MinVoxel.z];
                        }
                    }
                }

                SizeX = NewSizeX;
                SizeY = NewSizeY;
                SizeZ = NewSizeZ;
                Types = NewTypes;
                Textures = NewTextures;
                Rotations = NewRotations;
            }

            return true;
        }
    }

    // Usefull for rotate the complete object
    public struct ObjectDatasRotation
    {
        public ObjectDatas Data;
        public int PositionX;
        public int PositionY;
        public int PositionZ;
        public int SizeX;
        public int SizeY;
        public int SizeZ;
        byte Rotation;

        public void Init(ObjectDatas Data, byte Rotation)
        {
            this.Data = Data;
            this.Rotation = Rotation;
            this.PositionX = Data.PositionX;
            this.PositionY = Data.PositionY;
            this.PositionZ = Data.PositionZ;

            if (this.Rotation == 1 || this.Rotation == 3)
            {
                this.SizeX = Data.SizeZ;
                this.SizeY = Data.SizeY;
                this.SizeZ = Data.SizeX;
            }
            else
            {
                this.SizeX = Data.SizeX;
                this.SizeY = Data.SizeY;
                this.SizeZ = Data.SizeZ;
            }
        }

        public void GetValues(int x, int y, int z, ref byte Type, ref byte Texture, ref byte Rot)
        {
            if (this.Rotation == 0)
            {
                Type = Data.Types[x, y, z];
                Texture = Data.Textures[x, y, z];
                Rot = Data.Rotations[x, y, z];
                return;
            }

            if (this.Rotation == 1)
            {
                Type = Data.Types[SizeZ - z - 1, y, x];
                Texture = Data.Textures[SizeZ - z - 1, y, x];
                Rot = Data.Rotations[SizeZ - z - 1, y, x];
                return;
            }

            if (this.Rotation == 2)
            {
                Type = Data.Types[SizeX - x - 1, y, SizeZ - z - 1];
                Texture = Data.Textures[SizeX - x - 1, y, SizeZ - z - 1];
                Rot = Data.Rotations[SizeX - x - 1, y, SizeZ - z - 1];
                return;
            }

            if (this.Rotation == 3)
            {
                Type = Data.Types[z, y, SizeX - x - 1];
                Texture = Data.Textures[z, y, SizeX - x - 1];
                Rot = Data.Rotations[z, y, SizeX - x - 1];
            }
        }
    }
}