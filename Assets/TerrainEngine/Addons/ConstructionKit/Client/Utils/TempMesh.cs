using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TempMesh
{
    public List<Vector3> Vertices;
    public List<Vector2> Uvs;
    public List<int> Indices;
    public List<Vector4> Tangents;
    public List<Vector3> Normals;
    public List<List<Vector3>> normal_buffer;

    public bool Valid = false;
    public bool OpositeFace; // True if this face must not be draw when next voxel is same type
    public bool FullFace;       // True if the face is full and must not be draw with any other full face
    public bool MediumFace;    // True if this face must not be draw with full face
    // Full + Full = no (Cube+Cube), Op+Op = no, Medium + Full = no

    public TempMesh()
    {
        Vertices = new List<Vector3>(64);
        Uvs = new List<Vector2>(64);
        Indices = new List<int>(64 * 3);
        Tangents = new List<Vector4>(64);
        normal_buffer = new List<List<Vector3>>(64);
        Normals = new List<Vector3>(64);
    }

    public void RecalculateNormals()
    {
        if (Vertices.Count == 0)
            return;

        normal_buffer.Clear();
        Vector3[] normals = new Vector3[Vertices.Count];

        int i = 0,j=0;
        for (i = 0; i < Vertices.Count; ++i)
            if (normal_buffer.Count <= i)
                normal_buffer.Add(new List<Vector3>(3));
            else
                normal_buffer[i].Clear();

        Vector3 vc0, vc2, vc1, vt1, vt2, normal;
        for (i = 0; i < Indices.Count; i += 3)
        {
            vc0 = Vertices[Indices[i]];
            vc2 = Vertices[Indices[i + 2]];
            vc1 = Vertices[Indices[i + 1]];

            vt1 = vc1 - vc0;
            vt2 = vc2 - vc0;
            normal = Vector3.Cross(vt1, vt2);
            normal.Normalize();

            normal_buffer[Indices[i + 0]].Add(normal);
            normal_buffer[Indices[i + 1]].Add(normal);
            normal_buffer[Indices[i + 2]].Add(normal);
        }

        Vector3 nm = new Vector3();
        for (i = 0; i < Vertices.Count; ++i)
        {
            nm.x = nm.y = nm.z = 0;
            for (j = 0; j < normal_buffer[i].Count; ++j)
                nm += normal_buffer[i][j];

            normals[i] = nm / normal_buffer[i].Count;
        }

        Normals.Clear();
        Normals.AddRange(normals);
    }

    public static void calculateMeshTangents(Mesh mesh)
    {
        //speed up math by copying the mesh arrays
        int[] triangles = mesh.triangles;
        Vector3[] vertices = mesh.vertices;
        Vector2[] uv = mesh.uv;
        Vector3[] normals = mesh.normals;

        //variable definitions
        int triangleCount = triangles.Length;
        int vertexCount = vertices.Length;

        Vector3[] tan1 = new Vector3[vertexCount];
        Vector3[] tan2 = new Vector3[vertexCount];

        Vector4[] tangents = new Vector4[vertexCount];
        Vector3 v1, v2, v3, w1, w2, w3, sdir, tdir,n,t;
        int a, i1, i2, i3;
        float x1, x2, y1, y2, z1, z2, s1, s2, t1, t2, r;

        for (a = 0; a < triangleCount; a += 3)
        {
            i1 = triangles[a + 0];
            i2 = triangles[a + 1];
            i3 = triangles[a + 2];

            v1 = vertices[i1];
            v2 = vertices[i2];
            v3 = vertices[i3];

            w1 = uv[i1];
            w2 = uv[i2];
            w3 = uv[i3];

            x1 = v2.x - v1.x;
            x2 = v3.x - v1.x;
            y1 = v2.y - v1.y;
            y2 = v3.y - v1.y;
            z1 = v2.z - v1.z;
            z2 = v3.z - v1.z;

            s1 = w2.x - w1.x;
            s2 = w3.x - w1.x;
            t1 = w2.y - w1.y;
            t2 = w3.y - w1.y;

            r = 1.0f / (s1 * t2 - s2 * t1);

            sdir.x = (t2 * x1 - t1 * x2) * r;
            sdir.y = (t2 * y1 - t1 * y2) * r;
            sdir.z = (t2 * z1 - t1 * z2) * r;

            tdir.x = (s1 * x2 - s2 * x1) * r;
            tdir.y = (s1 * y2 - s2 * y1) * r;
            tdir.z = (s1 * z2 - s2 * z1) * r;

            tan1[i1] += sdir;
            tan1[i2] += sdir;
            tan1[i3] += sdir;

            tan2[i1] += tdir;
            tan2[i2] += tdir;
            tan2[i3] += tdir;
        }


        for (a = 0; a < vertexCount; ++a)
        {
            n = normals[a];
            t = tan1[a];

            //Vector3 tmp = (t - n * Vector3.Dot(n, t)).normalized;
            //tangents[a] = new Vector4(tmp.x, tmp.y, tmp.z);
            Vector3.OrthoNormalize(ref n, ref t);
            tangents[a].x = t.x;
            tangents[a].y = t.y;
            tangents[a].z = t.z;

            tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[a]) < 0.0f) ? -1.0f : 1.0f;
        }

        mesh.tangents = tangents;
    }

    public void RecalculateTangents()
    {
        //variable definitions
        int triangleCount = Indices.Count;
        int vertexCount = Vertices.Count;

        Vector3[] tan1 = new Vector3[vertexCount];
        Vector3[] tan2 = new Vector3[vertexCount];

        Vector4[] tangents = new Vector4[vertexCount];
        Tangents.Clear();

        Vector3 v1, v2, v3, w1, w2, w3, sdir, tdir, n, t;
        int a, i1, i2, i3;
        float x1, x2, y1, y2, z1, z2, s1, s2, t1, t2, r;

        for (a = 0; a < triangleCount; a += 3)
        {
            i1 = Indices[a + 0];
            i2 = Indices[a + 1];
            i3 = Indices[a + 2];

            v1 = Vertices[i1];
            v2 = Vertices[i2];
            v3 = Vertices[i3];

            w1 = Uvs[i1];
            w2 = Uvs[i2];
            w3 = Uvs[i3];

            x1 = v2.x - v1.x;
            x2 = v3.x - v1.x;
            y1 = v2.y - v1.y;
            y2 = v3.y - v1.y;
            z1 = v2.z - v1.z;
            z2 = v3.z - v1.z;

            s1 = w2.x - w1.x;
            s2 = w3.x - w1.x;
            t1 = w2.y - w1.y;
            t2 = w3.y - w1.y;

            r = 1.0f / (s1 * t2 - s2 * t1);

            sdir.x = (t2 * x1 - t1 * x2) * r;
            sdir.y = (t2 * y1 - t1 * y2) * r;
            sdir.z = (t2 * z1 - t1 * z2) * r;

            tdir.x = (s1 * x2 - s2 * x1) * r;
            tdir.y = (s1 * y2 - s2 * y1) * r;
            tdir.z = (s1 * z2 - s2 * z1) * r;

            tan1[i1] += sdir;
            tan1[i2] += sdir;
            tan1[i3] += sdir;

            tan2[i1] += tdir;
            tan2[i2] += tdir;
            tan2[i3] += tdir;
        }


        for (a = 0; a < vertexCount; ++a)
        {
            n = Normals[a];
            t = tan1[a];

            //Vector3 tmp = (t - n * Vector3.Dot(n, t)).normalized;
            //tangents[a] = new Vector4(tmp.x, tmp.y, tmp.z);
            Vector3.OrthoNormalize(ref n, ref t);
            tangents[a].x = t.x;
            tangents[a].y = t.y;
            tangents[a].z = t.z;

            tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[a]) < 0.0f) ? -1.0f : 1.0f;
        }

        Tangents.AddRange(tangents);
    }

    public void Clear()
    {
        Vertices.Clear();
        Uvs.Clear();
        Indices.Clear();
        Tangents.Clear();
        Normals.Clear();
    }

    public void CopyFrom(TempMesh Other)
    {
        Vertices.Clear();
        Vertices.AddRange(Other.Vertices);

        Uvs.Clear();
        Uvs.AddRange(Other.Uvs);

        Indices.Clear();
        Indices.AddRange(Other.Indices);

        MediumFace = Other.MediumFace;
        OpositeFace = Other.OpositeFace;
        FullFace = Other.FullFace;
    }

    public void AddFrom(TempMesh Other)
    {
        foreach (int id in Other.Indices)
            Indices.Add(id + Vertices.Count);

        Vertices.AddRange(Other.Vertices);
        Uvs.AddRange(Other.Uvs);
    }
}
