using UnityEngine;


public class LoadAroundChunks : MonoBehaviour 
{
    public CKChunkProcessor Manager;
    public Vector3 LastChunkPosition;
    public int HorizonRange = 4;
    public int VerticalRange = 4;
    public bool DisableAfterFirstLoading = true;

	// Use this for initialization
	void Start () 
    {
        LastChunkPosition = new Vector3(-1, -1, -1);
        if (Manager == null)
            Manager = FindObjectOfType(typeof(CKChunkProcessor)) as CKChunkProcessor;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Manager == null || !Manager.IsLoaded)
            return;

        /*Vector3 Current = Manager.GetChunkPositionFromWorldPosition(transform.position);
        if (Current != LastChunkPosition)
        {
            for (int y = 0; y < VerticalRange; ++y)
            {
                LastChunkPosition.y = Current.y + y;
                for (int x = -HorizonRange; x <= HorizonRange; ++x)
                {
                    LastChunkPosition.x = Current.x + x;
                    for (int z = -HorizonRange; z <= HorizonRange; ++z)
                    {
                        LastChunkPosition.z = Current.z + z;
                        Manager.AddChunkToCreate(LastChunkPosition);
                    }
                }
            }

            LastChunkPosition = Current;
            if (DisableAfterFirstLoading)
                enabled = false;
        }*/
	}
}
