using System;
using UnityEngine;

[ExecuteInEditMode]
public class MeshEditor : MonoBehaviour 
{
    public MeshFilter Filter;
    public MeshRenderer Renderer;
    public Mesh CMesh;
    public bool CreatePlane;
    public int[] Indices;
    public Vector3[] Vertices;
    public Vector2[] Uvs;

    void Start()
    {
        if (Filter == null) Filter = GetComponent<MeshFilter>();
        if (Filter == null) Filter = gameObject.AddComponent<MeshFilter>();

        if (Renderer == null) Renderer = GetComponent<MeshRenderer>();
        if (Renderer == null) Renderer = gameObject.AddComponent<MeshRenderer>();

        if (CMesh == null) CMesh = Filter.sharedMesh;
        if (CMesh == null)
        {
            CMesh = new Mesh();
            Filter.sharedMesh = CMesh;
        }

        int Count = CMesh.vertices.Length;
        for (int i = 0; i < Count; ++i)
        {
            CreateVertex(CMesh.vertices[i], i, CMesh.uv[i].x, CMesh.uv[i].y);
        }
    }

    public void CreateVertex(Vector3 Position, int Id, float UvX, float UvY)
    {
        Quaternion Q = Quaternion.Euler(0, 45, 0);
        GameObject Vertex = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Vertex.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        Vertex.name = "Vertex:" + Id + ":" + UvX + ":" + UvY+":";
        Vertex.transform.parent = transform;
        Vertex.transform.localPosition = Q * Position;
    }

    public void AddTriangle(int a, int b, int c)
    {
        Indices = CMesh.GetIndices(0);
        if (Indices == null)
            Indices = new int[3];
        else
        {
            int Count = Indices.Length;
            Array.Resize(ref Indices, Indices.Length + 3);
            Indices[Count] = a;
            Indices[Count + 1] = a;
            Indices[Count + 2] = a;
        }

        CMesh.SetIndices(Indices, MeshTopology.Triangles, 0);
    }

    void Update()
    {
        Vertices = CMesh.vertices;
        Uvs = CMesh.uv;
        if (Vertices == null)
            Vertices = new Vector3[0];
        if(Uvs == null)
            Uvs = new Vector2[0];
        int Count = 0;

        if (CreatePlane)
        {
            Count = Vertices.Length;
            CreateVertex(new Vector3(0, 0, 1), Vertices.Length, 0, 0);
            CreateVertex(new Vector3(1, 0, 1), Vertices.Length + 1, 1, 0);
            CreateVertex(new Vector3(1, 0, 0), Vertices.Length + 2, 1, 1);
            CreateVertex(new Vector3(0, 0, 0), Vertices.Length + 3, 0, 1);
        }

        foreach (Transform tr in transform)
        {
            string Name = tr.name;
            string[] Values = Name.Split(':');

            if (Values[0] == "Vertex")
            {
                int Id = int.Parse(Values[1]);
                if (Id >= Vertices.Length)
                {
                    Array.Resize(ref Vertices, Id+1);
                    Array.Resize(ref Uvs,Id+1);
                }

                Vertices[Id] = tr.localPosition;
                Uvs[Id].x = float.Parse(Values[2]);
                Uvs[Id].y = float.Parse(Values[3]);
            }
        }

        CMesh.vertices = Vertices;
        CMesh.uv = Uvs;
        if (CreatePlane)
        {
            CreatePlane = false;
            AddTriangle(Count, Count + 1, Count+2);
            AddTriangle(Count + 1, Count + 2, Count+3);
        }
        CMesh.RecalculateNormals();
        Filter.sharedMesh = CMesh;

        name = "Vertex:" + Vertices.Length + ",Indices=" + CMesh.GetIndices(0).Length;
    }
}
