using UnityEngine;

/// <summary>
/// Utility class that let you see normals and tangent vectors for a mesh.
/// This is really useful when debugging mesh appearance
/// </summary>
[RequireComponent(typeof(MeshFilter))]
public class MeshDisplay : MonoBehaviour
{
    public bool showNormals = true;
    public bool showTangents = true;
    public float displayLengthScale = 1.0f;
    public bool RegenerateNormals = false;
    public bool RegenerateTangents = false;
    public Vector3[] Vertices;
    public Vector2[] Uvs;
    public Vector4[] Tangents;
    public Vector3[] Normals;
    public int[] Indices;
    public bool Rotate = false;

    public Color normalColor = Color.red;
    public Color tangentColor = Color.blue;

    private MeshFilter meshFilter;
    private Mesh mesh;
    void OnDrawGizmosSelected()
    {
        if(meshFilter == null)
            meshFilter = GetComponent<MeshFilter>();

        if (meshFilter == null)
        {
            Debug.LogWarning("Cannot find MeshFilter");
            return;
        }

        if(mesh == null)
            mesh = meshFilter.sharedMesh;

        if (mesh == null)
        {
            Debug.LogWarning("Cannot find mesh");
            return;
        }

        if (Rotate)
        {
            Quaternion Q = Quaternion.Euler(0,10,0);
            Vertices = mesh.vertices;
            Normals = mesh.normals;

            for (int i = 0; i < Vertices.Length; ++i)
                Vertices[i] = Q * Vertices[i];
            for (int i = 0; i < Normals.Length; ++i)
                Normals[i] = Q * Normals[i];

            mesh.vertices = Vertices;
            mesh.normals = Normals;

            RegenerateTangents = true;
            Rotate = false;
        }

        if (RegenerateNormals)
        {
            RegenerateNormals = false;
            mesh.RecalculateNormals();

            Vertices = mesh.vertices;
            Tangents = mesh.tangents;
            Uvs = mesh.uv;
            Indices = mesh.GetIndices(0);
        }

        if (RegenerateTangents)
        {
            RegenerateTangents = false;
            //CKChunkProcessor.calculateMeshTangents(mesh);
        }

        bool doShowNormals = showNormals && mesh.normals.Length == mesh.vertices.Length;
        bool doShowTangents = showTangents && mesh.tangents.Length == mesh.vertices.Length;

        foreach (int idx in mesh.triangles)
        {
            Vector3 vertex = transform.TransformPoint(mesh.vertices[idx]);

            if (doShowNormals)
            {
                Vector3 normal = transform.TransformDirection(mesh.normals[idx]);
                Gizmos.color = normalColor;
                Gizmos.DrawLine(vertex, vertex + normal * displayLengthScale);
            }
            if (doShowTangents)
            {
                Vector3 tangent = transform.TransformDirection(mesh.tangents[idx]);
                Gizmos.color = tangentColor;
                Gizmos.DrawLine(vertex, vertex + tangent * displayLengthScale);
            }
        }
    }
}