using System;
using UnityEngine;

public enum TextureTypes
{
    OPAQUE = 0,
    TRANSPARENT = 1,
}

[Serializable]
public class TextureInformation
{
    public TextureInformation()
    {

    }

    public TextureInformation(string Name, Texture2D Texture, Texture2D Normal)
    {
        this.Name = Name;
        this.Top = Bottom = Side = Texture;
        this.TopNormal = SideNormal = BottomNormal = Normal;
        this.TextureType = TextureTypes.OPAQUE;
        this.TilingBlockSize = 1;
    }

    public int Id;
    public string Name;
    public Texture2D Top;
    public Texture2D Side;
    public Texture2D Bottom;
    public int TilingBlockSize = 1;
    public bool YAxis = false;

    public Texture2D TopNormal;
    public Texture2D SideNormal;
    public Texture2D BottomNormal;
    public TextureTypes TextureType;

    [HideInInspector]
    public Rect TopUv;

    [HideInInspector]
    public Rect SideUv;

    [HideInInspector]
    public Rect BottomUv;

    [HideInInspector]
    public int TopId;

    [HideInInspector]
    public int SideId;

    [HideInInspector]
    public int BottomId;
}
