using System;
using UnityEngine;

public enum SideNames
{
    Center = 0,
    Top = 1,
    Right = 2,
    Bottom = 3,
    Left = 4,
    Forward = 5,
    Backward = 6,
}

[Serializable]
public class CKVoxelInformation  
{
    static public int Center = 0;
    static public int Top = 1;
    static public int Bottom = 2;
    static public int Left = 3;
    static public int Forward = 4;
    static public int Right = 5;
    static public int Backward = 6;

    public CKVoxelInformation()
    {
        Name = "New";
        TMesh = new TempMesh[7];
    }

    public int Id;
    public string Name;

    // Center,Top,Bottom,Left,Forward,Right,Backward
    public TempMesh[] TMesh;

    [HideInInspector]
    public string SelectedSide = "";

    [HideInInspector]
    public bool IsOpen;

    public TempMesh GetMesh(int Side, int Rotation)
    {
        Side -= Rotation;
        if (Side < 3)
            Side = 6 - (2 - Side);
        return TMesh[Side];
    }

    static public int GetSide(int Side, int Rotation)
    {
        Side -= Rotation;
        if (Side < 3)
            Side = 6 - (2 - Side);
        return Side;
    }

    public bool NoisePosition = false;
}
