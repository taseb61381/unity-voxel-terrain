﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TerrainEngine;

public struct CKVoxelModification
{
    public CKVoxelModification(TerrainChunkRef Chunk, int x, int y, int z, byte Type, byte Texture, byte Rotation)
    {
        this.Chunk = Chunk;
        this.x = x;
        this.y = y;
        this.z = z;
        this.Type = Type;
        this.Texture = Texture;
        this.Rotation = Rotation;
    }

    public TerrainChunkRef Chunk;
    public int x, y, z;
    public byte Type, Texture, Rotation;
}


namespace TerrainEngine
{
    public class CKOperator : ITerrainOperator
    {
        public struct CKVoxelInformation
        {
            public int x, y, z;
            public byte Type, Texture, Rotation;

            public CKVoxelInformation(int x, int y, int z, byte Type, byte Texture, byte Rotation)
            {
                this.x = x;
                this.y = y;
                this.z = z;
                this.Type = Type;
                this.Texture = Texture;
                this.Rotation = Rotation;
            }
        }

        public bool ModifyTerrain = false;
        public IBlockProcessor Processor = null;
        public SList<CKVoxelInformation> Voxels = new SList<CKVoxelInformation>(10);

        public void Add(int x, int y, int z, byte Type, byte Texture, byte Rotation)
        {
            Voxels.Add(new CKVoxelInformation(x, y, z, Type, Texture, Rotation));
        }

        public void Add(CKVoxelInformation Info)
        {
            Voxels.Add(Info);
        }

        public override void Execute(TerrainOperatorContainer OperatorsContainer, ITerrainContainer Container, ITerrainBlock Block, ITerrainOperator.CanModifyVoxel CanModify, ITerrainOperator.CanModifyVoxel OnModify)
        {
            CKDatas Data = Block.Voxels.GetCustomData<CKDatas>();
            if (Data == null)
            {
                GeneralLog.LogError("Invalid CustomData Type CKDatas");
                return;
            }

            CKVoxelInformation CKInfo;
            ITerrainBlock AroundBlock = null;
            VoxelsOperator Operator = null;

            byte OldType=0, OldTexture=0, OldRotation=0;
            for (int i = 0; i < Voxels.Count; ++i)
            {
                CKInfo = Voxels.array[i];

                AroundBlock = Block.GetAroundBlock(ref CKInfo.x, ref CKInfo.y, ref CKInfo.z);

                if (AroundBlock == null || AroundBlock.HasState(TerrainBlockStates.CLOSED))
                    return;

                Data.GetValues(CKInfo.x, CKInfo.y, CKInfo.z, ref OldType, ref OldTexture, ref OldRotation);

                if (ModifyTerrain)
                {
                    if (Operator == null)
                        Operator = new VoxelsOperator();

                    Operator.Voxels.Add(new VoxelPoint(Block.BlockId, CKInfo.x, CKInfo.y, CKInfo.z, 0, 0, VoxelModificationType.SET_TYPE_SET_VOLUME));
                }

                if (OldType == CKInfo.Type && OldTexture == CKInfo.Texture && OldRotation == CKInfo.Rotation)
                    continue;

                Data.SetValues(CKInfo.x, CKInfo.y, CKInfo.z, CKInfo.Type, CKInfo.Texture, CKInfo.Rotation);
            }

            if (ModifyTerrain)
            {
                if (Operator != null)
                {
                    OperatorsContainer.AddOperator(Operator);
                }
            }
        }
    }
}
