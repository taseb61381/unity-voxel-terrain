using UnityEngine;


public class CKChunkScript : TerrainSubScript
{
    public MeshCollider CCollider;

    public override void Init(int ScriptHashCode, int SubScriptHashCode, IBlockProcessor Processor, ITerrainScript Script)
    {
        base.Init(ScriptHashCode, SubScriptHashCode, Processor, Script);

        if (CCollider == null)
            CCollider = gameObject.GetComponent<MeshCollider>();

        if (CCollider == null)
            CCollider = gameObject.AddComponent<MeshCollider>();
    }

    public void Apply(TempMeshArray Array)
    {
        CCollider.sharedMesh = null;

        if (Array.Vertices != null && Array.Vertices.Length != 0 && Array.Materials != null && Array.Materials.Length != 0)
        {
            Mesh CMesh = GetMesh(true);
            CMesh.Clear(false);

            CMesh.vertices = Array.Vertices;
            CMesh.subMeshCount = Array.Materials.Length;
            for (int i = 0; i < Array.Materials.Length; ++i)
                CMesh.SetIndices(Array.Indices[i], MeshTopology.Triangles, i);

            CMesh.uv = Array.Uvs;
            CMesh.normals = Array.Normals;
            CMesh.tangents = Array.Tangents;
            CRenderer.sharedMaterials = Array.Materials;
            CCollider.sharedMesh = CMesh;
        }
        else
        {
            Mesh CMesh = GetMesh(false);
            if(CMesh != null)
                CMesh.Clear(false);
        }
    }

    public override bool CanCloseChunk()
    {
        return false;
    }

    public override void Close(bool Remove)
    {
        CCollider.sharedMesh = null;
        base.Close(Remove);
    }
}
