using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TerrainEngine;
using TerrainEngine.ConstructionKit;
using UnityEngine;

public struct TempMeshArray
{
	public TerrainChunkRef ChunkRef;
	public Vector3[] Vertices;
	public Vector2[] Uvs;
	public int[][] Indices;
	public Vector4[] Tangents;
	public Vector3[] Normals;
	public Material[] Materials;
}

public class TempVoxelPool : IPoolable
{
	public TempVoxel[][][] Temp;
	public TempMesh Mesh;

	public void Init(int SizeX, int SizeY, int SizeZ)
	{
		if (Temp == null)
		{
			Temp = new TempVoxel[SizeX][][];
			Mesh = new TempMesh();
			int x, y;
			for (x = 0; x < SizeX; ++x)
			{
				Temp[x] = new TempVoxel[SizeY][];

				for (y = 0; y < SizeY; ++y)
					Temp[x][y] = new TempVoxel[SizeZ];
			}
		}
	}

	public override void Clear()
	{

	}
}

public class CKChunkProcessor : IBlockProcessor
{
	[HideInInspector]
	public bool HasSaveManager = false;

	public string TexturesDirectory = "Textures\\";

	public GameObject ChunkPrefab;
	public CKVoxelInformation[] Voxels;
	public TextureInformation[] Textures;

	public Texture2D AtlasTexture;
	public Texture2D AtlasNormal;
	public Material AtlasMaterial;
	public Material TransparentMaterial;

	public int GeneratingCount = 0;
	public int ToApplyCount = 0;

	public bool DebugLog;
	public List<Texture2D> TempList;
	public List<Texture2D> TempNormal;
	public bool IsLoaded = false;

	public HashSet<Vector3> ChunksSet = new HashSet<Vector3>();

	public float LoadingProgress = 0f;

	[HideInInspector]
	public Material[][] Materials;

	[NonSerialized]
	private bool Loading;

	private Queue<TempMeshArray> ToApply;
	private Queue<TerrainChunkRef> ToGenerate;

	public override void OnInitManager(TerrainManager Manager)
	{
		base.OnInitManager(Manager);
		InternalDataName = "CK-" + DataName;
	}

	void FixedUpdate()
	{
		if (!IsLoaded)
			return;

		GeneratingCount = ToGenerate.Count;
		ToApplyCount = ToApply.Count;
	}

	public WWW GetTexture(int Id, string Side, bool Bump)
	{
		string Name = Path.Combine(Directory.GetCurrentDirectory(), TexturesDirectory) + Id.ToString() + "_" + Side + (Bump ? "_b" : "");
		if (File.Exists(Name + ".jpg"))
		{
			return new WWW("file://" + Name + ".jpg");
		}
		else if (File.Exists(Name + ".png"))
		{
			return new WWW("file://" + Name + ".png");
		}

		return null;
	}

	IEnumerator DelayedLoading()
	{
		///////////////////////////////////////////////////////////////////// Textures : Creating Atlas Texture

		Debug.Log("ConstructionKit : Loading Textures");
		if (!Application.isWebPlayer)
		{
			string Dir = Path.Combine(Directory.GetCurrentDirectory(), TexturesDirectory);
			if (Directory.Exists(Dir))
			{
				for (int i = 0; i < 255; ++i)
				{
					WWW TopDiffuse = GetTexture(i, "top", false);
					WWW TopBump = GetTexture(i, "top", true);

					if (TopDiffuse == null || TopBump == null)
						break;

					Debug.Log("Loading Texture : " + i);
					yield return TopDiffuse;
					yield return TopBump;

					WWW BottomDiffuse = GetTexture(i, "bottom", false);
					WWW BottomBump = GetTexture(i, "bottom", true);

					if (BottomDiffuse == null || BottomBump == null)
					{
						BottomDiffuse = TopDiffuse;
						BottomBump = TopBump;
					}

					yield return BottomDiffuse;
					yield return BottomBump;

					WWW SideDiffuse = GetTexture(i, "side", false);
					WWW SideBump = GetTexture(i, "side", true);

					if (SideDiffuse == null || SideBump == null)
					{
						SideDiffuse = TopDiffuse;
						SideBump = TopBump;
					}

					yield return SideDiffuse;
					yield return SideBump;

					TextureInformation VInfo = new TextureInformation();
					VInfo.Top = TopDiffuse.texture;
					VInfo.TopNormal = TopBump.texture;

					VInfo.Bottom = BottomDiffuse.texture;
					VInfo.BottomNormal = BottomBump.texture;

					VInfo.Side = SideDiffuse.texture;
					VInfo.SideNormal = SideBump.texture;

					if (Textures.Length <= i)
						Array.Resize(ref Textures, Textures.Length + 1);
					Textures[i] = VInfo;
				}
			}
		}

		List<Texture2D> TempTextures = new List<Texture2D>();
		List<Texture2D> TempNormals = new List<Texture2D>();
		foreach (TextureInformation TInfo in Textures)
		{
			if (TInfo.Top != null)
			{
				if (!TempTextures.Contains(TInfo.Top))
				{
					TInfo.TopId = TempTextures.Count;
					TempTextures.Add(TInfo.Top);
					if (TempNormals.Contains(TInfo.TopNormal))
						Debug.LogError("Normal Already used :" + TInfo.TopNormal.name);

					TempNormals.Add(TInfo.TopNormal);
				}
				else
				{
					TInfo.TopId = TempTextures.IndexOf(TInfo.Top);
				}
			}

			if (TInfo.Side != null)
			{
				if (!TempTextures.Contains(TInfo.Side))
				{
					TInfo.SideId = TempTextures.Count;
					TempTextures.Add(TInfo.Side);
					if (TempNormals.Contains(TInfo.SideNormal))
						Debug.LogError("Normal Already used :" + TInfo.SideNormal.name);

					TempNormals.Add(TInfo.SideNormal);
				}
				else
				{
					TInfo.SideId = TempTextures.IndexOf(TInfo.Side);
				}
			}

			if (TInfo.Bottom != null)
			{
				if (!TempTextures.Contains(TInfo.Bottom))
				{
					TInfo.BottomId = TempTextures.Count;
					TempTextures.Add(TInfo.Bottom);

					if (TempNormals.Contains(TInfo.BottomNormal))
						Debug.LogError("Normal Already used :" + TInfo.BottomNormal.name);

					TempNormals.Add(TInfo.BottomNormal);
				}
				else
				{
					TInfo.BottomId = TempTextures.IndexOf(TInfo.Bottom);
				}
			}
		}

		AtlasTexture = new Texture2D(2, 2);
		AtlasNormal = new Texture2D(2, 2);

		if (TempTextures.Count > 0)
		{
			foreach (Texture2D Txt in TempTextures)
			{
				Txt.mipMapBias = -1;
				if (Txt.mipmapCount > 1)
					Debug.LogError("Disable mimmaping on texture : " + Txt.name);
			}

			foreach (Texture2D Txt in TempNormals)
			{
				Txt.mipMapBias = -1;
				if (Txt.mipmapCount > 1)
					Debug.LogError("Disable mimmaping on texture : " + Txt.name);
			}

			if (TempTextures.Count != TempNormals.Count)
				Debug.LogError("Diffuse Count != Normal Count :" + TempTextures.Count + "/" + TempNormals.Count);

			Debug.Log("ConstructionKit : Packing " + TempTextures.Count + "Textures");
			Rect[] Uvs = AtlasTexture.PackTextures(TempTextures.ToArray(), 64);
			AtlasNormal.PackTextures(TempNormals.ToArray(), 64);

			AtlasTexture.filterMode = FilterMode.Trilinear;
			AtlasNormal.filterMode = FilterMode.Trilinear;
			AtlasTexture.wrapMode = TextureWrapMode.Clamp;
			AtlasTexture.anisoLevel = AtlasNormal.anisoLevel = 0;
			AtlasTexture.mipMapBias = -1;
			AtlasNormal.mipMapBias = -1;
			AtlasTexture.Apply();
			AtlasNormal.Apply();

			float offsetx = 5f / AtlasTexture.width;
			float offsety = 5f / AtlasTexture.height;

			for (int i = 0; i < Uvs.Length; ++i)
			{
				Uvs[i].x += offsetx;
				Uvs[i].y += offsety;
				Uvs[i].width -= offsetx;
				Uvs[i].height -= offsety;
			}
			foreach (TextureInformation TInfo in Textures)
			{
				TInfo.TopUv = Uvs[TInfo.TopId];
				TInfo.SideUv = Uvs[TInfo.SideId];
				TInfo.BottomUv = Uvs[TInfo.BottomId];
			}

			if (TransparentMaterial != null)
			{
				TransparentMaterial.mainTexture = AtlasTexture;
				TransparentMaterial.SetTexture("_BumpMap", AtlasNormal);
			}
			else
				Debug.LogError("No Transparent Material");

			if (AtlasMaterial != null)
			{
				AtlasMaterial.mainTexture = AtlasTexture;
				AtlasMaterial.SetTexture("_BumpMap", AtlasNormal);
			}
			else
				Debug.LogError("No Atlas Material");
		}
		/////////////////////////////////////////////////////////////////////
		Materials = new Material[3][];
		Materials[0] = new Material[1] { AtlasMaterial };
		Materials[1] = new Material[1] { TransparentMaterial };
		Materials[2] = new Material[2] { AtlasMaterial, TransparentMaterial };

		///////////////////////////////////////////////////////////////////// Voxels
		Debug.Log("ConstructionKit : Calculating Normals");
		for (int i = 0; i < Voxels.Length; ++i)
		{
			Voxels[i].Id = i;
			foreach (TempMesh SubMesh in Voxels[i].TMesh)
			{
				SubMesh.Valid = SubMesh.Vertices.Count != 0;
				SubMesh.RecalculateNormals();
			}
		}
		/////////////////////////////////////////////////////////////////////


		///////////////////////////////////////////////////////////////////// Threading
		ActionProcessor.AddToUnity(new ExecuteAction("ConstructionKit", UpdateUnity));
		ActionProcessor.Add(new ExecuteAction("ConstructionKit", UpdateThread), 0, false);
		/////////////////////////////////////////////////////////////////////

		Debug.Log("ConstructionKit : Loaded");
		Loading = false;
		IsLoaded = true;
		yield return null;
	}

	#region IBlockProcessor

	public override bool CanStart()
	{
		if (!Loading && !IsLoaded)
		{
			Loading = true;
			if (Voxels.Length > 67)
			{
				Debug.LogError("67 Voxels Mesh Maximum");
				return false;
			}
			if (Textures.Length > 67)
			{
				Debug.LogError("67 Textures Maximum");
				return false;
			}

			ToApply = new Queue<TempMeshArray>();
			ToGenerate = new Queue<TerrainChunkRef>();

			StartCoroutine(DelayedLoading());

			GameObject DefaultPoolObject = new GameObject();
			DefaultPoolObject.name = DataName;
			DefaultPoolObject.AddComponent<CKChunkScript>();
			DefaultPoolObject.SetActive(false);
			DefaultPoolObject.transform.parent = transform;
			DefaultPoolObject.layer = LayerMask.NameToLayer("Chunk");
			GameObjectPool.SetDefaultObject(DataName, DefaultPoolObject);
			return false;
		}
		else
			return IsLoaded;
	}

	public override IWorldGenerator GetGenerator()
	{
		CKGenerator Gen = new CKGenerator();
		Gen.Priority = Priority;
		Gen.Name = InternalName;
		Gen.DataName = "CK-" + DataName;
		return Gen;
	}

	public override void OnBlockDataCreate(TerrainDatas Data)
	{
		CKDatas DData = Data.GetCustomData<CKDatas>(InternalDataName);
		if (DData == null)
		{
			DData = new CKDatas();
			Data.RegisterCustomData(InternalDataName, DData);
		}
	}

	public override void OnTerrainLoadingStart(TerrainBlock Terrain)
	{
		CKDatas Data = Terrain.Voxels.GetCustomData<CKDatas>(InternalDataName);
		if (Data == null)
		{
			Debug.LogError("Can not find : " + InternalDataName);
			return;
		}

		ITerrainBlock.HasVolumeDelegate Delegate = Data.HasVolume;
		if (Delegate == null)
		{
			Debug.LogError("Invalid HasVolume delegate : " + Delegate);
			return;
		}
		Terrain.RegisterHasVolume(Delegate);
	}

	public override void OnTerrainThreadClose(TerrainBlock Terrain)
	{
		CKDatas Data = Terrain.Voxels.GetCustomData<CKDatas>(InternalDataName);
		if (Data == null)
		{
			Debug.LogError("Can not find : " + InternalDataName);
			return;
		}

		ITerrainBlock.HasVolumeDelegate Delegate = Data.HasVolume;
		if (Delegate == null)
		{
			Debug.LogError("Invalid HasVolume delegate : " + Delegate);
			return;
		}

		Terrain.RemoveHasVolume(Delegate);
	}

    public override void OnTerrainUnityClose(TerrainBlock Terrain)
    {
        int InternalHash = InternalDataName.GetHashCode();
        for (int i = Terrain.Script.Scripts.Count - 1; i >= 0; --i)
        {
            if (Terrain.Script.Scripts.array[i].ScriptHashCode == InternalHash)
                Terrain.Script.Scripts.array[i].Close(true);
        }
    }

	public override void OnFirstChunkGenerate(ActionThread Thread,TerrainBlock RebuildingBlock, TerrainBlock Block, int ChunkId, IMeshData Data)
	{
		GenerateChunk(Thread,Block, ChunkId, true);
	}

    public override void OnChunksGenerationDone(TerrainBlock Terrain)
    {
        CKDatas Data = Terrain.Voxels.GetCustomData<CKDatas>();
        if (Data == null)
            return;

        TerrainChunkRef Ref = new TerrainChunkRef();
        Ref.BlockId = Terrain.BlockId;
        lock (ToGenerate)
        {
            for (int i = 0; i < Terrain.Information.TotalChunksPerBlock; ++i)
            {
                if (Data.CKChunks[i] != null)
                {
                    Ref.ChunkId = i;
                    ToGenerate.Enqueue(Ref);
                }
            }
        }
    }

	public void GenerateChunk(ActionThread Thread, TerrainBlock Block, int ChunkId, bool CheckVertices)
	{
		TempVoxelPool TMP = Thread.Pool.GetData<TempVoxelPool>("TempVoxelPool");
		TMP.Init(TerrainManager.Instance.Informations.VoxelPerChunk + 2, TerrainManager.Instance.Informations.VoxelPerChunk + 2, TerrainManager.Instance.Informations.VoxelPerChunk + 2);

		TempMeshArray Array = GenerateChunk(this,Block,ChunkId, Block.Voxels.GetCustomData<CKDatas>(InternalDataName), TMP.Temp, TMP.Mesh);

		TMP.Close();

		if (!CheckVertices || (Array.Vertices != null && Array.Vertices.Length != 0))
		{
			lock (ToApply)
				ToApply.Enqueue(Array);
		}
	}

	public bool UpdateThread(ActionThread Thread)
	{
		TerrainChunkRef Chunk = new TerrainChunkRef();
		TerrainBlock Block = null;
		while (GetNextChunkToGenerate(ref Chunk) == true)
		{
			if(Chunk.IsValid(ref Block))
				GenerateChunk(Thread, Block, Chunk.ChunkId, false);

			if (!Thread.HasTime())
				break;
		}
		return false;
	}

	public bool UpdateUnity(ActionThread Thread)
	{
		TempMeshArray Array;
		TerrainBlock Block = null;
		TerrainChunkScript CScript = null;
		CKChunkScript Script;

		lock (ToApply)
		{
			while (ToApply.Count != 0)
			{
				Array = ToApply.Dequeue();
				if (!Array.ChunkRef.IsValid(ref Block))
					continue;

				if (Array.Vertices != null && Array.Vertices.Length != 0)
				{
					Block.CheckChunkGameObject(Array.ChunkRef.ChunkId, ref CScript);
                    Script = Block.Script.GetSubScript<CKChunkScript>(InternalDataName.GetHashCode(), Array.ChunkRef.ChunkId);

					if (Script == null)
					{
						Script = GameObjectPool.GetGameObject(DataName).GetComponent<CKChunkScript>();
						Script.name = DataName;
						Script.gameObject.layer = LayerMask.NameToLayer("Chunk");
                        Script.Init(InternalDataName.GetHashCode(), Array.ChunkRef.ChunkId, this, Block.Script);
						Script.transform.parent = CScript.transform;
						Script.transform.localPosition = new Vector3(-0.5f,-0.5f,-0.5f) * TerrainManager.Instance.Informations.Scale;
						Script.Apply(Array);
					}
					else
					{
						Script.Apply(Array);
					}
				}
				else
				{
					CScript = Block.GetChunkScript(Array.ChunkRef.ChunkId);

					if (CScript != null)
					{
                        Script = Block.Script.GetSubScript<CKChunkScript>(InternalDataName.GetHashCode(), Array.ChunkRef.ChunkId);
						if (Script != null)
						{
							Script.Apply(new TempMeshArray());
						}
					}
				}

				if (!Thread.HasTime())
					return false;
			}
		}
		return false;
	}

	public bool GetNextChunkToGenerate(ref TerrainChunkRef Chunk)
	{
		lock (ToGenerate)
		{
			if (ToGenerate.Count != 0)
			{
				Chunk = ToGenerate.Dequeue();
				return true;
			}
		}

		return false;
	}

	public bool HasVolume(TerrainBlock Block, int x, int y, int z)
	{
		return Block.Voxels.GetCustomData<CKDatas>(InternalDataName).GetType(x, y, z) != 0;
	}

	#endregion

	#region Modifications

	public Queue<CKVoxelModification> _Modifications = new Queue<CKVoxelModification>();
	public List<TerrainChunkRef> ModifiedChunks = new List<TerrainChunkRef>();
	public IAction CurrentAction = null;

	public void Flush(Queue<CKVoxelModification> Modifications, bool AddToUndoManager, bool CallEvents)
	{
		CKVoxelModification Mod;
		TerrainBlock Block = null,LastBlock = null;
		CKDatas Datas = null;
		TerrainChunkRef AChunk = new TerrainChunkRef();
		ModifiedChunks.Clear();

		CKFlushAction Action = null;

		if (AddToUndoManager && UndoRedoProcessor.Instance != null)
			Action = new CKFlushAction(this);

		byte OldType=0, OldTexture=0, OldRotation=0;
		int Cx, Cy, Cz;

		if(DebugLog)
			Debug.Log("Modifications.Count = " + Modifications.Count);

		VoxelsOperator Operator = null;
		int StartX = 0, StartY = 0, StartZ = 0;

		while (Modifications.Count != 0)
		{
			Mod = Modifications.Dequeue();
			if (!Mod.Chunk.IsValid(ref Block))
				continue;

			TerrainBlock.GetChunkStart(Mod.Chunk.ChunkId, ref StartX, ref StartY, ref StartZ);

			Mod.x += StartX;
			Mod.y += StartY;
			Mod.z += StartZ;

			Block = Block.GetAroundBlock(ref Mod.x, ref Mod.y, ref Mod.z) as TerrainBlock;
			if (Block == null)
				continue;

			Mod.Chunk.ChunkId = Block.GetChunkIdFromVoxel(Mod.x, Mod.y, Mod.z);
			TerrainBlock.GetChunkStart(Mod.Chunk.ChunkId, ref StartX, ref StartY, ref StartZ);

			if (LastBlock != Block)
			{
				Datas = Block.Voxels.GetCustomData<CKDatas>(InternalDataName);
				LastBlock = Block;
			}

			Datas.GetValues(Mod.x, Mod.y, Mod.z, ref OldType, ref OldTexture, ref OldRotation);

			if (CallEvents)
			{
				if (Operator == null)
					Operator = new VoxelsOperator();

				Operator.Voxels.Add(new VoxelPoint(Block.BlockId, Mod.x, Mod.y, Mod.z, 0, 0, VoxelModificationType.SET_TYPE_SET_VOLUME));
			}

			if (OldType == Mod.Type && OldTexture == Mod.Texture && OldRotation == Mod.Rotation)
				continue;

			if (Action != null)
			{
				Mod.x -= StartX;
				Mod.y -= StartY;
				Mod.z -= StartZ;
				Action.Add(Mod, OldType, OldTexture, OldRotation);
				Mod.x += StartX;
				Mod.y += StartY;
				Mod.z += StartZ;
			}

			Datas.SetValues(Mod.x, Mod.y, Mod.z, Mod.Type, Mod.Texture, Mod.Rotation);

			Cx = Mod.x % TerrainManager.Instance.Informations.VoxelPerChunk;
			Cy = Mod.y % TerrainManager.Instance.Informations.VoxelPerChunk;
			Cz = Mod.z % TerrainManager.Instance.Informations.VoxelPerChunk;

			lock (ToGenerate)
			{
				if (!ModifiedChunks.Contains(Mod.Chunk))
					ModifiedChunks.Add(Mod.Chunk);

				if (Cx == 0) Block.GetAroundChunk(Mod.Chunk.ChunkId, 0, 1, 1, ref AChunk);
				else if (Cx == TerrainManager.Instance.Informations.VoxelPerChunk - 1) Block.GetAroundChunk(Mod.Chunk.ChunkId, 2, 1, 1, ref AChunk);
				CheckAndAddToList(ModifiedChunks, ref AChunk);

				if (Cy == 0) Block.GetAroundChunk(Mod.Chunk.ChunkId, 1, 0, 1, ref AChunk);
				else if (Cy == TerrainManager.Instance.Informations.VoxelPerChunk - 1) Block.GetAroundChunk(Mod.Chunk.ChunkId, 1, 2, 1, ref AChunk);
				CheckAndAddToList(ModifiedChunks, ref AChunk);

				if (Cz == 0) Block.GetAroundChunk(Mod.Chunk.ChunkId, 1, 1, 0, ref AChunk);
				else if (Cz == TerrainManager.Instance.Informations.VoxelPerChunk - 1) Block.GetAroundChunk(Mod.Chunk.ChunkId, 1, 1, 2, ref AChunk);
				CheckAndAddToList(ModifiedChunks, ref AChunk);
			}
		}


		if (DebugLog)
			Debug.Log("Modified Chunks : " + ModifiedChunks.Count);

		if (CallEvents)
		{
			if (Operator != null)
			{
				TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
				TerrainManager.Instance.ModificationInterface.Flush(null, null, true, false);
			}
		}

		lock (ToGenerate)
		{
			for (int i = 0; i < ModifiedChunks.Count; ++i)
			{
				ToGenerate.Enqueue(ModifiedChunks[i]);
			}

			ModifiedChunks.Clear();
		}

		if (Action != null)
			UndoRedoProcessor.Instance.AddAction(Action);
	}

	static public void CheckAndAddToList(List<TerrainChunkRef> ModifiedChunks, ref TerrainChunkRef Around)
	{
		if (Around.IsValidID() && !ModifiedChunks.Contains(Around))
			ModifiedChunks.Add(Around);
	}

	public override IAction Flush(ActionThread Thread)
	{
		Flush( _Modifications, true, true);
		return null;
	}

	#endregion

	#region Mesh

	public byte GetValues(TerrainBlock Block, CKDatas Data, int px, int py, int pz, ref byte Type, ref byte Texture, ref byte Orientation)
	{
		TerrainBlock AroundBlock = Block.GetAroundBlock(ref px, ref py, ref pz) as TerrainBlock;
		if (AroundBlock != null)
		{
			if (AroundBlock != Block)
				Data = AroundBlock.Voxels.GetCustomData<CKDatas>(InternalDataName);

			return Data.GetValues(px, py, pz, ref Type, ref Texture, ref Orientation);
		}

		return 0;
	}

	public void SetValues(TerrainBlock Block, CKDatas Data, int px, int py, int pz, byte Type, byte Texture, byte Orientation)
	{
		TerrainBlock AroundBlock = Block.GetAroundBlock(ref px, ref py, ref pz) as TerrainBlock;
		if (AroundBlock != null)
		{
			if (AroundBlock != Block)
				Data = AroundBlock.Voxels.GetCustomData<CKDatas>(InternalDataName);

			Data.SetValues(px, py, pz, Type, Texture, Orientation);
		}
	}

	private TempMesh TMesh;

	static public TempMeshArray GenerateChunk(CKChunkProcessor Manager, TerrainBlock Block, int ChunkId, CKDatas CDatas, TempVoxel[][][] Temp, TempMesh TMesh)
	{
		// Preloading Datas
		int x, y, z;
		int px, py, pz;
		byte Type = 0, Texture = 0, Rotation = 0;
		TempVoxel[][] TX;
		TempVoxel[] TXY;

		TerrainBlock AroundBlock = null;
		TerrainBlock LastAroundBlock = null;
		CKDatas AroundData = null;

		int StartX = 0, StartY = 0, StartZ = 0;
		TerrainBlock.GetChunkStart(ChunkId, ref StartX, ref StartY, ref StartZ);

		int Count = 0;
		for (x = 0; x < TerrainManager.Instance.Informations.VoxelPerChunk + 2; ++x)
		{
			TX = Temp[x];
			for (y = 0; y < TerrainManager.Instance.Informations.VoxelPerChunk + 2; ++y)
			{
				TXY = TX[y];
				for (z = 0; z < TerrainManager.Instance.Informations.VoxelPerChunk + 2; ++z)
				{
					px = StartX + x - 1;
					py = StartY + y - 1;
					pz = StartZ + z - 1;

					AroundBlock = Block.GetAroundBlock(ref px, ref py, ref pz) as TerrainBlock;
					if (AroundBlock != null)
					{
						if (LastAroundBlock != AroundBlock)
						{
							AroundData = AroundBlock.Voxels.GetCustomData<CKDatas>(Manager.InternalDataName);
							LastAroundBlock = AroundBlock;
						}

						if (AroundData.GetValues(px, py, pz, ref Type, ref Texture, ref Rotation) != 0)
						{
							TXY[z].Init(Type, Texture, Rotation, Manager.Voxels[Type], Manager.Textures[Texture]);
							++Count;
							continue;
						}
					}

					TXY[z].Type = 0;
				}
			}
		}

		// Generating Mesh
		TMesh.Clear();

		TempMeshArray Array = new TempMeshArray();
		Array.ChunkRef = new TerrainChunkRef(Block, ChunkId);

		if (Count != 0)
		{
			int VPC = TerrainManager.Instance.Informations.VoxelPerChunk;
			GenerateMesh(Manager, Temp, TMesh, ref Array, VPC, VPC, VPC,
				StartX, StartY, StartZ, TerrainManager.Instance.Informations.Scale);
			TMesh.RecalculateTangents();

			Array.Vertices = TMesh.Vertices.ToArray();
			Array.Uvs = TMesh.Uvs.ToArray();
			Array.Tangents = TMesh.Tangents.ToArray();
			Array.Normals = TMesh.Normals.ToArray();
		}

		return Array;
	}

	static public void GenerateMesh(CKChunkProcessor Manager, TempVoxel[][][] Temp, TempMesh Data, ref TempMeshArray TempMesh, int SizeX, int SizeY, int SizeZ, int ChunkWorldX = 0, int ChunkWorldY = 0, int ChunkWorldZ = 0, float Scale=1)
	{
		int x, y, z;
		byte Rotation = 0;
		TempVoxel[][] TX;
		TempVoxel[] TXY;
		TempVoxel TVoxel;
		TextureInformation TextureInfo;
		CKVoxelInformation VoxelInfo, Around;
		int px, py, pz, CurrentIndice = 0, Angle = 0, WorldX, WorldY, WorldZ;
		int SideId = 0;
		int MaterialArrayId = -1;
		TempMesh.Indices = new int[2][];

		for (int i = 0; i <= (int)TextureTypes.TRANSPARENT; ++i)
		{
			if (i == 1)
				TempMesh.Indices[0] = Data.Indices.ToArray();

			Data.Indices.Clear();

			for (x = 1; x < SizeX + 1; ++x)
			{
				TX = Temp[x];
				px = x - 1;
				WorldX = x + ChunkWorldX;
				for (y = 1; y < SizeY + 1; ++y)
				{
					TXY = TX[y];
					py = y - 1;
					WorldY = y + ChunkWorldY;

					for (z = 1; z < SizeZ + 1; ++z)
					{
						TVoxel = TXY[z];
						if (TVoxel.Type == 0 || (int)TVoxel.TInfo.TextureType != i)
							continue;

						if (i == 0)
							MaterialArrayId = 0;
						else
						{
							if (MaterialArrayId == -1)
								MaterialArrayId = 1;
							else
								MaterialArrayId = 2;
						}

						pz = z - 1;
						WorldZ = z + ChunkWorldZ;

						VoxelInfo = TVoxel.Info;
						TextureInfo = TVoxel.TInfo;
						Rotation = TVoxel.Rotation;
						Angle = 90 * Rotation;

						if (TXY[z - 1].Type == 0)
						{
							SideId = CKVoxelInformation.GetSide(CKVoxelInformation.Backward, Rotation);
							AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Backward, Scale);
						}
						else
						{
							TVoxel = TXY[z - 1];
							Around = TVoxel.Info;
							SideId = CKVoxelInformation.GetSide(CKVoxelInformation.Backward, Rotation);

							if (Around == null || TVoxel.TInfo.TextureType != TextureInfo.TextureType)
								AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Backward, Scale);
							else
								Check(Data, TextureInfo, VoxelInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], Around.GetMesh(CKVoxelInformation.Forward, TVoxel.Rotation), Around, Rotation, TVoxel.Rotation, CKVoxelInformation.Backward, Scale);
						}

						if (Temp[x + 1][y][z].Type == 0)
						{
							SideId = CKVoxelInformation.GetSide(CKVoxelInformation.Right, Rotation);
							AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Right, Scale);
						}
						else
						{
							TVoxel = Temp[x + 1][y][z];
							Around = TVoxel.Info;
							SideId = CKVoxelInformation.GetSide(CKVoxelInformation.Right, Rotation);

							if (Around == null || TVoxel.TInfo.TextureType != TextureInfo.TextureType)
								AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Right, Scale);
							else
								Check(Data, TextureInfo, VoxelInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], Around.GetMesh(CKVoxelInformation.Left, TVoxel.Rotation), Around, Rotation, TVoxel.Rotation, CKVoxelInformation.Right, Scale);
						}

						if (TXY[z + 1].Type == 0)
						{
							SideId = CKVoxelInformation.GetSide(CKVoxelInformation.Forward, Rotation);
							AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Forward, Scale);
						}
						else
						{
							TVoxel = TXY[z + 1];
							Around = TVoxel.Info;
							SideId = CKVoxelInformation.GetSide(CKVoxelInformation.Forward, Rotation);

							if (Around == null || TVoxel.TInfo.TextureType != TextureInfo.TextureType)
								AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Forward, Scale);
							else
								Check(Data, TextureInfo, VoxelInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], Around.GetMesh(CKVoxelInformation.Backward, TVoxel.Rotation), Around, Rotation, TVoxel.Rotation, CKVoxelInformation.Forward, Scale);
						}

						if (Temp[x - 1][y][z].Type == 0)
						{
							SideId = CKVoxelInformation.GetSide(CKVoxelInformation.Left, Rotation);
							AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Left, Scale);
						}
						else
						{
							TVoxel = Temp[x - 1][y][z];
							Around = TVoxel.Info;
							SideId = CKVoxelInformation.GetSide(CKVoxelInformation.Left, Rotation);

							if (Around == null || TVoxel.TInfo.TextureType != TextureInfo.TextureType)
								AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Left, Scale);
							else
								Check(Data, TextureInfo, VoxelInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.SideUv, VoxelInfo.TMesh[SideId], Around.GetMesh(CKVoxelInformation.Right, TVoxel.Rotation), Around, Rotation, TVoxel.Rotation, CKVoxelInformation.Left, Scale);
						}

						AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.TopUv, VoxelInfo.TMesh[CKVoxelInformation.Center], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Center, Scale);

						if (TX[y + 1][z].Type == 0)
						{
							AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.TopUv, VoxelInfo.TMesh[CKVoxelInformation.Top], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Top, Scale);
						}
						else
						{
							TVoxel = TX[y + 1][z];
							Around = TVoxel.Info;
							if (Around == null || TVoxel.TInfo.TextureType != TextureInfo.TextureType)
								AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.TopUv, VoxelInfo.TMesh[CKVoxelInformation.Top], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Top, Scale);
							else
								Check(Data, TextureInfo, VoxelInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.TopUv, VoxelInfo.TMesh[CKVoxelInformation.Top], Around.TMesh[CKVoxelInformation.Bottom], Around, Rotation, TVoxel.Rotation, CKVoxelInformation.Top, Scale);
						}

						if (TX[y - 1][z].Type == 0)
						{
							AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.BottomUv, VoxelInfo.TMesh[CKVoxelInformation.Bottom], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Bottom, Scale);
						}
						else
						{
							TVoxel = TX[y - 1][z];
							Around = TVoxel.Info;
							if (Around == null || TVoxel.TInfo.TextureType != TextureInfo.TextureType)
								AddSide(Data, TextureInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.BottomUv, VoxelInfo.TMesh[CKVoxelInformation.Bottom], VoxelInfo.NoisePosition, Angle, CKVoxelInformation.Bottom, Scale);
							else
								Check(Data, TextureInfo, VoxelInfo, WorldX, WorldY, WorldZ, px, py, pz, ref CurrentIndice, TextureInfo.BottomUv, VoxelInfo.TMesh[CKVoxelInformation.Bottom], Around.TMesh[CKVoxelInformation.Top], Around, Rotation, TVoxel.Rotation, CKVoxelInformation.Bottom, Scale);
						}
					}
				}
			}
		}

		if (MaterialArrayId == -1)
			MaterialArrayId = 0;

		if (MaterialArrayId != 0)
			TempMesh.Indices[1] = Data.Indices.ToArray();

		if (MaterialArrayId == 1)
			TempMesh.Indices[0] = TempMesh.Indices[1];

		TempMesh.Materials = Manager.Materials[MaterialArrayId];
	}

	static public void Check(TempMesh Data, TextureInformation CurrentTexture, CKVoxelInformation Current, 
		int WorldX, int WorldY, int WorldZ, 
		int x, int y, int z, ref int CurrentIndice, Rect Uv, TempMesh Side, TempMesh Oposite, CKVoxelInformation Around, byte RotationA, byte RotationB, int SideId, float Scale)
	{
		if (!Side.Valid || (Side.FullFace && Oposite.FullFace) || (Side.MediumFace && Oposite.FullFace) || (Current == Around && Side.OpositeFace && RotationA == RotationB))
			return;

		AddSide(Data, CurrentTexture, WorldX, WorldY, WorldZ, x, y, z, ref CurrentIndice, Uv, Side, Current.NoisePosition, RotationA * 90, SideId, Scale);
	}

	static public void AddSide(TempMesh Data, TextureInformation CurrentTexture, 
		int WorldX, int WorldY, int WorldZ, 
		int x, int y, int z, ref int CurrentIndice, Rect Uv, TempMesh Side, bool Noise, int Rotation, int SideId, float Scale)
	{
		if (!Side.Valid)
			return;

		int Count = Side.Vertices.Count;
		int i;
		float Offsetx = 0;
		float Offsety = 0;
		float Offsetz = 0;

		Vector3 V = new Vector3();
		Vector2 T = new Vector2();

		if (Rotation == 0)
		{
			for (i = 0; i < Count; ++i)
			{
				V = Side.Vertices[i];
				V.x += x + Offsetx;
				V.y += y + Offsety;
				V.z += z + Offsetz;
				V *= Scale;

				// Create Some Noise , you can use perlin noise here
				//P.y = y + (((Mathf.RoundToInt(x + V.x)) % 2 == 0 && (Mathf.RoundToInt(z + V.z)) % 2 == 0) ? 0.15f : 0f);
				Data.Vertices.Add(V);
			}

			for (i = 0; i < Count; ++i)
				Data.Normals.Add(Side.Normals[i]);
		}
		else
		{
			if (Rotation == 90)
			{
				Offsetz += 1;
			}
			else if (Rotation == 180)
			{
				Offsetz += 1;
				Offsetx += 1;
			}
			else if (Rotation >= 270)
			{
				Offsetx += 1;
			}

			Quaternion Q = Quaternion.AngleAxis(Rotation, Vector3.up);

			for (i = 0; i < Count; ++i)
			{
				V = Q * Side.Vertices[i];
				V.x += x + Offsetx;
				V.y += y + Offsety;
				V.z += z + Offsetz;
				V *= Scale;
				// Create Some Noise , you can use perlin noise here
				//P.y = y + (((Mathf.RoundToInt(x + V.x)) % 2 == 0 && (Mathf.RoundToInt(z + V.z)) % 2 == 0) ? 0.15f : 0f);
				Data.Vertices.Add(V);
			}

			for (i = 0; i < Count; ++i)
				Data.Normals.Add(Q * Side.Normals[i]);
		}

		for (i = 0; i < Count; ++i)
		{
			if (SideId == CKVoxelInformation.Backward)
			{
				T.x = (((WorldX % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
				T.y = (((WorldY % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
			}
			else if (SideId == CKVoxelInformation.Right)
			{
				T.x = (((WorldZ % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
				T.y = (((WorldY % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
			}
			else if (SideId == CKVoxelInformation.Left)
			{
				T.x = ((((int.MaxValue - WorldZ - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
				T.y = (((WorldY % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
			}
			else if (SideId == CKVoxelInformation.Forward)
			{
				T.x = ((((int.MaxValue - WorldX - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
				T.y = (((WorldY % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
			}
			else if (SideId == CKVoxelInformation.Top)
			{
				if (Rotation == 0)
				{
					T.x = (((WorldX % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
					T.y = (((WorldZ % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
				}
				else if (Rotation == 90)
				{
					T.x = ((((int.MaxValue - WorldZ - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
					T.y = (((WorldX % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
				}
				else if (Rotation == 180)
				{
					T.x = ((((int.MaxValue - WorldX - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
					T.y = ((((int.MaxValue - WorldZ - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
				}
				else
				{
					T.x = (((WorldZ % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
					T.y = ((((int.MaxValue - WorldX - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
				}
			}
			else
			{
				if (Rotation == 0)
				{
					T.x = (((WorldZ % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
					T.y = (((WorldX % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
				}
				else if (Rotation == 90)
				{
					T.x = (((WorldX % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
					T.y = ((((int.MaxValue - WorldZ - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
				}
				else if (Rotation == 180)
				{
					T.x = ((((int.MaxValue - WorldZ - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
					T.y = ((((int.MaxValue - WorldX - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
				}
				else
				{
					T.x = ((((int.MaxValue - WorldX - 2) % CurrentTexture.TilingBlockSize) + Side.Uvs[i].x) / (float)CurrentTexture.TilingBlockSize);
					T.y = (((WorldZ % CurrentTexture.TilingBlockSize) + Side.Uvs[i].y) / (float)CurrentTexture.TilingBlockSize);
				}
			}

			T.x *= Uv.width;
			T.y *= Uv.height;
			T.x += Uv.x;
			T.y += Uv.y;
			Data.Uvs.Add(T);
		}

		Count = Side.Indices.Count;
		for (i = 0; i < Count; ++i)
			Data.Indices.Add(Side.Indices[i] + CurrentIndice);

		CurrentIndice += Side.Vertices.Count;
	}

	#endregion
}