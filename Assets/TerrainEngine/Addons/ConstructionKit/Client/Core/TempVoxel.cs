﻿
namespace TerrainEngine.ConstructionKit
{
    public struct TempVoxel
    {
        public void Init(byte Type, byte Texture, byte Rotation, CKVoxelInformation Info, TextureInformation TInfo)
        {
            this.Type = Type;
            this.Texture = Texture;
            this.Rotation = Rotation;
            this.Info = Info;
            this.TInfo = TInfo;
        }

        public byte Type;
        public byte Texture;
        public byte Rotation;
        public CKVoxelInformation Info;
        public TextureInformation TInfo;
    }
}
