using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CKChunkProcessor))]
public class CKChunkProcessorInspector : Editor 
{
    public TempMesh TMesh;
    public TempMesh Copy;
    MeshFilter Import;

    public override void OnInspectorGUI()
    {
        CKChunkProcessor Manager = target as CKChunkProcessor;
        foreach (TextureInformation Info in Manager.Textures)
            if (Info.Top != null)
                Info.Name = Info.Top.name;

        base.DrawDefaultInspector();

        if (Manager.TempList != null && Manager.TempNormal != null && Manager.TempList.Count == Manager.TempNormal.Count)
        {
            for (int i = 0; i < Manager.TempList.Count; ++i)
                ArrayUtility.Add(ref Manager.Textures, new TextureInformation(Manager.TempList[i].name, Manager.TempList[i], Manager.TempNormal[i]));

            Manager.TempList.Clear();
            Manager.TempNormal.Clear();
        }


        GUILayout.BeginVertical("Box");
        {
            GUILayout.Label("Voxels Mesh :" + Manager.Voxels.Length);
            int Id = 0;
            foreach (CKVoxelInformation Info in Manager.Voxels)
            {
                GUILayout.BeginVertical(Info.IsOpen ? "Window" : "Box");
                {
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Label(Id.ToString(), GUILayout.Width(20));

                        if (GUILayout.Button(Info.Name))
                            Info.IsOpen = !Info.IsOpen;


                        if (GUILayout.Button("U", GUILayout.Width(22)))
                        {
                            if (Id == 0)
                                return;

                            Manager.Voxels[Id] = Manager.Voxels[Id - 1];
                            Manager.Voxels[Id - 1] = Info;

                            return;
                        }


                        if (GUILayout.Button("D", GUILayout.Width(22)))
                        {
                            if (Id == Manager.Voxels.Length-1)
                                return;

                            Manager.Voxels[Id] = Manager.Voxels[Id + 1];
                            Manager.Voxels[Id+1] = Info;

                            return;
                        }

                        if (GUILayout.Button("X", GUILayout.Width(20)))
                        {
                            ArrayUtility.Remove(ref Manager.Voxels, Info);
                            return;
                        }
                    }
                    GUILayout.EndHorizontal();

                    if (Info.IsOpen)
                    {
                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label("Name : ",GUILayout.Width(70));
                            Info.Name = GUILayout.TextField(Info.Name);
                        }
                        GUILayout.EndHorizontal();

                        if (Info.SelectedSide == "")
                        {
                            if (Info.TMesh == null || Info.TMesh.Length < 7)
                                Info.TMesh = new TempMesh[7];

                            DrawMesh(Info, ref Info.TMesh[0], "Center");
                            DrawMesh(Info, ref Info.TMesh[1], "Top");
                            DrawMesh(Info, ref Info.TMesh[2], "Bottom");
                            DrawMesh(Info, ref Info.TMesh[3], "Left");
                            DrawMesh(Info, ref Info.TMesh[4], "Forward");
                            DrawMesh(Info, ref Info.TMesh[5], "Right");
                            DrawMesh(Info, ref Info.TMesh[6], "Backward");
                        }
                        else if (GUILayout.Button("<- Back : " + Info.SelectedSide))
                        {
                            Info.SelectedSide = "";
                            TMesh = null;
                        }
                        else
                        {
                            if (TMesh == null)
                            {
                                switch (Info.SelectedSide)
                                {
                                    case "Center":
                                        TMesh = Info.TMesh[0];
                                        break;
                                    case "Top":
                                        TMesh = Info.TMesh[1];
                                        break;
                                    case "Bottom":
                                        TMesh = Info.TMesh[2];
                                        break;
                                    case "Left":
                                        TMesh = Info.TMesh[3];
                                        break;
                                    case "Forward":
                                        TMesh = Info.TMesh[4];
                                        break;
                                    case "Right":
                                        TMesh = Info.TMesh[5];
                                        break;
                                    case "Backward":
                                        TMesh = Info.TMesh[6];
                                        break;
                                };
                            }
                            DrawMesh();
                            if (GUILayout.Button("Create Plane"))
                            {
                                TMesh.Vertices.Add(new Vector3(0, 0, 0));
                                TMesh.Vertices.Add(new Vector3(0, 0, 1));
                                TMesh.Vertices.Add(new Vector3(1, 0, 1));
                                TMesh.Vertices.Add(new Vector3(1, 0, 0));

                                TMesh.Uvs.Add(new Vector2(0, 1));
                                TMesh.Uvs.Add(new Vector2(0, 0));
                                TMesh.Uvs.Add(new Vector2(1, 0));
                                TMesh.Uvs.Add(new Vector2(1, 1));

                                TMesh.Indices.Add(0);
                                TMesh.Indices.Add(1);
                                TMesh.Indices.Add(2);

                                TMesh.Indices.Add(2);
                                TMesh.Indices.Add(3);
                                TMesh.Indices.Add(0);
                            }
                        }
                    }
                }
                GUILayout.EndVertical();
                ++Id;

            }
        }

        if (GUILayout.Button("Add Voxel"))
            ArrayUtility.Add(ref Manager.Voxels, new CKVoxelInformation());

        GUILayout.EndVertical();
    }

    public void DrawMesh(CKVoxelInformation Info, ref TempMesh TMesh, string Side)
    {
        if (TMesh == null)
            TMesh = new TempMesh();

        GUILayout.BeginHorizontal();

        if (GUILayout.Button(Side + " : " + TMesh.Vertices.Count + "," + TMesh.Uvs.Count + "," + TMesh.Indices.Count))
        {
            this.TMesh = TMesh;
            Info.SelectedSide = Side;
        }

        if (GUILayout.Button("P", GUILayout.Width(20)))
        {
            if (Copy == null)
                return;

            TMesh.CopyFrom(Copy);
            Copy = null;
        }

        if (GUILayout.Button("A", GUILayout.Width(20)))
        {
            if (Copy == null)
                return;

            TMesh.AddFrom(Copy);

            Copy = null;
        }

        if (GUILayout.Button("C", GUILayout.Width(20)))
            Copy = TMesh;

        GUI.color = TMesh.OpositeFace ? Color.green : Color.red;
        if (GUILayout.Button("O", GUILayout.Width(22)))
            TMesh.OpositeFace = !TMesh.OpositeFace;

        GUI.color = TMesh.FullFace ? Color.green : Color.red;
        if (GUILayout.Button("F", GUILayout.Width(22)))
            TMesh.FullFace = !TMesh.FullFace;

        GUI.color = TMesh.MediumFace ? Color.green : Color.red;
        if (GUILayout.Button("M", GUILayout.Width(22)))
            TMesh.MediumFace = !TMesh.MediumFace;

        GUI.color = Color.white;
        GUILayout.EndHorizontal();
    }

    public void DrawMesh()
    {
        if (TMesh == null)
            return;

        GUILayout.BeginVertical("Box");
        {
            GUILayout.Label("Vertices : " + TMesh.Vertices.Count);

            for (int i = 0; i < TMesh.Vertices.Count; ++i)
            {
                GUILayout.BeginHorizontal();
                {
                    TMesh.Vertices[i] = EditorGUILayout.Vector3Field("", TMesh.Vertices[i]);
                    if (GUILayout.Button("X", GUILayout.Width(20)))
                        TMesh.Vertices.RemoveAt(i);
                }
                GUILayout.EndHorizontal();
            }
            if (GUILayout.Button("Add Vertex"))
                TMesh.Vertices.Add(new Vector3());
        }
        GUILayout.EndVertical();

        GUILayout.BeginVertical("Box");
        {
            GUILayout.Label("Uv : " + TMesh.Uvs.Count);

            for (int i = 0; i < TMesh.Uvs.Count; ++i)
            {
                GUILayout.BeginHorizontal();
                {
                    TMesh.Uvs[i] = EditorGUILayout.Vector2Field("", TMesh.Uvs[i]);
                    if (GUILayout.Button("X", GUILayout.Width(20)))
                        TMesh.Uvs.RemoveAt(i);
                }
                GUILayout.EndHorizontal();
            }
            if (GUILayout.Button("Add Uv"))
                TMesh.Uvs.Add(new Vector2());
        }
        GUILayout.EndVertical();

        GUILayout.BeginVertical("Box");
        {
            GUILayout.Label("Indice : " + TMesh.Indices.Count);

            for (int i = 0; i < TMesh.Indices.Count; )
            {
                GUILayout.BeginHorizontal("Box");
                TMesh.Indices[i] = EditorGUILayout.IntField(TMesh.Indices[i]);
                TMesh.Indices[i + 1] = EditorGUILayout.IntField(TMesh.Indices[i + 1]);
                TMesh.Indices[i + 2] = EditorGUILayout.IntField(TMesh.Indices[i + 2]);
                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    TMesh.Indices.RemoveAt(i);
                    TMesh.Indices.RemoveAt(i);
                    TMesh.Indices.RemoveAt(i);
                }
                GUILayout.EndHorizontal();
                i += 3;
            }
            if (GUILayout.Button("Add Indice"))
            {
                TMesh.Indices.Add(0);
                TMesh.Indices.Add(0);
                TMesh.Indices.Add(0);
            }
        }
        GUILayout.EndVertical();

        GUILayout.BeginVertical("Box");
        {
            Import = EditorGUILayout.ObjectField("Import", Import, typeof(MeshFilter),true) as MeshFilter;
            if (Import != null)
            {
                TMesh.Vertices.Clear();
                TMesh.Vertices.AddRange(Import.sharedMesh.vertices);

                TMesh.Uvs.Clear();
                TMesh.Uvs.AddRange(Import.sharedMesh.uv);

                TMesh.Indices.Clear();
                TMesh.Indices.AddRange(Import.sharedMesh.GetIndices(0));

                Import = null;
            }
        }
        GUILayout.EndVertical();
    }
}
