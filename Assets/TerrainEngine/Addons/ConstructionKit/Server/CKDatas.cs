﻿using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    public class CKDatas : ICustomTerrainData
    {
        static public PoolInfoRef PoolContainer;
        static public Queue<ushort[]> PoolDatas = new Queue<ushort[]>();
        static public ushort[] GetData(int Size)
        {
            lock (PoolDatas)
            {
                if (PoolDatas.Count != 0)
                    return PoolDatas.Dequeue();
            }

            return new ushort[Size] ;
        }

        public ushort[][] CKChunks;
        public int VoxelCount;

        public override void Init(TerrainDatas Terrain)
        {
            if(PoolContainer.Index == 0)
                PoolContainer = PoolManager.Pool.GetPool("ConstructionKitData");

            base.Init(Terrain);
            VoxelCount = Terrain.Information.VoxelPerChunk * Terrain.Information.VoxelPerChunk * Terrain.Information.VoxelPerChunk;

            if (CKChunks == null)
                CKChunks = new ushort[Terrain.Information.TotalChunksPerBlock][];
            else
            {
                int i = Terrain.Information.TotalChunksPerBlock - 1;
                for (; i >= 0; --i)
                {
                    CKChunks[i] = null;
                }
            }
        }

        public override void Clear()
        {
            int i = Information.TotalChunksPerBlock - 1;
            for (; i >= 0; --i)
            {
                if (CKChunks[i] != null)
                {
                    lock (PoolDatas)
                    {
                        for (int j = 0; j < VoxelCount; ++j)
                            CKChunks[i][j] = 0;

                        PoolDatas.Enqueue(CKChunks[i]);
                    }

                    CKChunks[i] = null;
                }
            }
        }

        public byte GetValues(int x, int y, int z, ref byte Type, ref byte Texture, ref byte Orientation)
        {
            int cv = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            if (CKChunks[cv] == null)
            {
                Type = Texture = Orientation = 0;
                return 0;
            }

            cv = CKChunks[cv][(x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk)];
            Type = (byte)((cv << 25) >> 25);
            Texture = (byte)((cv << 18) >> 25);
            Orientation = (byte)(cv >> 14);
            return Type;
        }

        public void SetValues(int x, int y, int z, byte Type, byte Texture, byte Orientation)
        {
            int cv = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            if (CKChunks[cv] == null)
            {
                if(Type == 0)
                    return;

                CKChunks[cv] = GetData(VoxelCount);
            }

            Dirty = true;
            CKChunks[cv][(x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk)] = (ushort)((Orientation << 14) | Type | (Texture << 7));
        }

        public void SetType(int x, int y, int z, byte Type)
        {
            byte Texture = 0, Orientation = 0, OldType = 0;
            GetValues(x, y, z, ref OldType, ref Texture, ref Orientation);
            SetValues(x, y, z, Type, Texture, Orientation);
        }

        public void SetTexture(int x, int y, int z, byte Texture)
        {
            byte Type = 0, Orientation = 0, OldTexture = 0;
            GetValues(x, y, z, ref Type, ref OldTexture, ref Orientation);
            SetValues(x, y, z, Type, Texture, Orientation);
        }

        public void SetOrientation(int x, int y, int z, byte Orientation)
        {
            byte Type = 0, OldOrientation = 0, Texture = 0;
            GetValues(x, y, z, ref Type, ref Texture, ref OldOrientation);
            SetValues(x, y, z, Type, Texture, Orientation);
        }

        public byte GetType(int x, int y, int z)
        {
            byte Type = 0, Texture = 0, Orientation = 0;
            GetValues(x, y, z, ref Type, ref Texture, ref Orientation);
            return Type;
        }

        public byte GetTexture(int x, int y, int z)
        {
            byte Type = 0, Texture = 0, Orientation = 0;
            GetValues(x, y, z, ref Type, ref Texture, ref Orientation);
            return Texture;
        }

        public override float GetOrientation(int x, int y, int z)
        {
            byte Type = 0, Texture = 0, Orientation = 0;
            GetValues(x, y, z, ref Type, ref Texture, ref Orientation);
            return Orientation;
        }

        public bool HasVolume(int x, int y, int z)
        {
            return GetType(x, y, z) != 0;
        }

        #region Save

        public override void Save(System.IO.BinaryWriter Stream)
        {
            int j;
            bool Ok;
            ushort[] TempData;

            for (int i = 0; i < Information.TotalChunksPerBlock; ++i)
            {
                Ok = false;
                TempData = CKChunks[i];

                if (TempData != null)
                {
                    for (j = 0; j < TempData.Length; ++j)
                    {
                        if (TempData[j] != 0)
                        {
                            Ok = true;
                            break;
                        }
                    }
                }

                if (Ok)
                {
                    Stream.Write(true);

                    for (j = 0; j < TempData.Length; ++j)
                    {
                        Stream.Write(TempData[j]);
                    }
                }
                else
                    Stream.Write(false);
            }
        }

        public override void Read(System.IO.BinaryReader Stream)
        {
            int j = 0;
            ushort[] Data = null;
            for (int i = 0; i < Information.TotalChunksPerBlock; ++i)
            {
                if (Stream.ReadBoolean())
                {
                    CKChunks[i] = Data = GetData(VoxelCount);

                    for (j = 0; j < VoxelCount; ++j)
                    {
                        Data[j] = Stream.ReadUInt16();
                    }
                }
            }
        }

        #endregion
    }
}
