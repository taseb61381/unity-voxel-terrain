﻿using System;

namespace TerrainEngine
{
    [Serializable]
    public class CKGenerator : IWorldGenerator
    {
        public override void OnTerrainDataCreate(TerrainDatas Data)
        {
            CKDatas DData = Data.GetCustomData<CKDatas>(DataName);
            if (DData == null)
            {
                DData = new CKDatas();
                Data.RegisterCustomData(DataName, DData);
            }
            DData.UID = UID;
        }
    }
}
