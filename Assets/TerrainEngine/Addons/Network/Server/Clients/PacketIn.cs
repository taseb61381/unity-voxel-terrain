﻿using System;
using System.IO;

using UnityEngine;

namespace FrameWork
{
    public class PacketIn : MemoryStream
    {
        // Opcode du packet
        private UInt64 _Opcode = 0;
        private UInt64 _Size = 0;

        public UInt64 Opcode
        {
            get { return _Opcode; }
            set { _Opcode = value; }
        }
        public UInt64 Size
        {
            get { return _Size; }
            set { _Size = value; }
        }

        public PacketIn(int size)
            : base(size)
        {
        }

        public PacketIn(byte[] buf, int start, int size)
            : base(buf, start, size)
        {
        }

        public byte[] Read(int size)
        {
            byte[] buf = new byte[size];
            Read(buf, 0, size);

            return buf;
        }
        public void Skip(long num)
        {
            Seek(num, SeekOrigin.Current);
        }
        public long Remain()
        {
            return Length - Position;
        }

        public virtual byte GetUint8()
        {
            return (byte)ReadByte();
        }

        public virtual UInt16 GetUint16()
        {
            return Marshal.ConvertToUInt16(GetUint8(), GetUint8());
        }
        public virtual ushort GetUint16R()
        {
            byte v1 = GetUint8();
            byte v2 = GetUint8();

            return Marshal.ConvertToUInt16(v2, v1);
        }

        public virtual Int16 GetInt16()
        {
            byte[] tmp = { GetUint8() , GetUint8()  };
            return BitConverter.ToInt16(tmp, 0);
        }
        public virtual Int16 GetInt16R()
        {
            byte v1 = GetUint8();
            byte v2 = GetUint8();

            byte[] tmp = { v2, v1 };
            return BitConverter.ToInt16(tmp, 0);
        }

        public virtual uint GetUint32()
        {
            byte v1 = GetUint8();
            byte v2 = GetUint8();
            byte v3 = GetUint8();
            byte v4 = GetUint8();

            return Marshal.ConvertToUInt32(v1, v2, v3, v4);
        }
        public virtual uint GetUint32R()
        {
            byte v1 = GetUint8();
            byte v2 = GetUint8();
            byte v3 = GetUint8();
            byte v4 = GetUint8();

            return Marshal.ConvertToUInt32(v4, v3, v2, v1);
        }

        public virtual Int32 GetInt32()
        {
            byte[] tmp = { GetUint8(), GetUint8(), GetUint8(), GetUint8() };
            return BitConverter.ToInt32(tmp, 0);
        }
        public virtual Int32 GetInt32R()
        {
            byte v1 = GetUint8();
            byte v2 = GetUint8();
            byte v3 = GetUint8();
            byte v4 = GetUint8();

            byte[] tmp = { v4, v3, v2, v1 };
            return BitConverter.ToInt32(tmp, 0);
        }

        public UInt64 GetUint64()
        {
            UInt64 value = (GetUint32() << 24) + (GetUint32());
            return value;
        }
        public UInt64 GetUint64R()
        {

            UInt64 value = (GetUint32()) + (GetUint32() << 24);
            return value;
        }

        public Int64 GetInt64()
        {
            byte[] tmp = Read(8);
            return BitConverter.ToInt64(tmp, 0);
        }

        public Int64 GetInt64R()
        {
            byte[] tmp = Read(8);
            Array.Reverse(tmp);
            return BitConverter.ToInt64(tmp, 0);
        }

        public float GetFloat()
        {
            byte[] b = new byte[4];
            b[0] = (byte)ReadByte();
            b[1] = (byte)ReadByte();
            b[2] = (byte)ReadByte();
            b[3] = (byte)ReadByte();

            return BitConverter.ToSingle(b, 0);
        }

        private char ReadPs()
        {
            if (Length >= Position + 2)
                return BitConverter.ToChar(new byte[] { GetUint8(), GetUint8() }, 0);

            return (char)0;
        }
        public virtual string GetString()
        {
            int len = (int)GetUint32();

            var buf = new byte[len];
            Read(buf, 0, len);

            return Marshal.ConvertToString(buf);
        }
        public virtual string GetString(int maxlen)
        {
            var buf = new byte[maxlen];
            Read(buf, 0, maxlen);

            return Marshal.ConvertToString(buf);
        }
        public virtual string GetPascalString()
        {
            return GetString(GetUint8());
        }
        public virtual string GetUnicodeString()
        {
            string tmp = "";

            char tmp2 = ReadPs();
            while (tmp2 != 0x00)
            {
                tmp += tmp2;
                tmp2 = ReadPs();
            }

            return tmp;
        }
        public virtual string GetStringToZero()
        {
            string value = "";

            while (Position < Length)
            {
                char c = (char)ReadByte();
                if (c == 0)
                    break;
                value += c;

            }

            return value;
        }

        public virtual Vector2 GetVector2()
        {
            return new Vector2(GetFloat(), GetFloat());
        }
        public virtual Vector3 GetVector3()
        {
            return new Vector3(GetFloat(), GetFloat(), GetFloat());
        }
        public virtual Quaternion GetQuaternion()
        {
            return new Quaternion(GetFloat(), GetFloat(), GetFloat(), GetFloat());
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}
