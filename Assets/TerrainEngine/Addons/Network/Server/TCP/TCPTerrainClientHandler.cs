﻿using FrameWork;
using System;
using System.Collections.Generic;
using System.IO;

namespace TerrainEngine
{
    public class TCPTerrainClientHandler : ITerrainObjectHandlers
    {
        public TCPServerClient Client;

        public override void Init(TerrainObjectServer Owner)
        {

        }

        public override string GetName()
        {
            return "TCP-" + Owner.Id + ":" + Client.GetIp;
        }

        public override void SendGeneratingBlock(VectorI3 Position, float Pct)
        {
            PacketOut Packet = new PacketOut((byte)TerrainOpcodes.SEND_GENERATING);
            Packet.WriteInt32(Position.x);
            Packet.WriteInt32(Position.y);
            Packet.WriteInt32(Position.z);
            Client.SendPacket(Packet);
        }

        public override void SendGeneratingAbord(VectorI3 Position)
        {
            PacketOut Packet = new PacketOut((byte)TerrainOpcodes.SEND_ABORD);
            Packet.WriteInt32(Position.x);
            Packet.WriteInt32(Position.y);
            Packet.WriteInt32(Position.z);
            Client.SendPacket(Packet);
        }

        public override void SendAllowedBlocks(List<VectorI3> Positions)
        {
            PacketOut Packet = new PacketOut((byte)TerrainOpcodes.SEND_ALLOWED);
            Packet.WriteUInt16((ushort)Positions.Count);
            foreach (VectorI3 Position in Positions)
            {
                Packet.WriteInt32(Position.x);
                Packet.WriteInt32(Position.y);
                Packet.WriteInt32(Position.z);
            }
            Client.SendPacket(Packet);
        }

        public override void CreateBlock(TerrainBlockServer Block)
        {
            try
            {
                int DataCount = Block.Voxels.Customs.Count;
                ITerrainDataSave Data;

                for (int i = 0; i <= DataCount; ++i)
                {
                    if (i == 0)
                    {
                        Data = Block.Voxels;
                    }
                    else
                    {
                        Data = Block.Voxels.Customs.array[i - 1];
                    }

                    byte[] TData = CompressData(Data, Block.Information.CompressionMode);

                    int Size = TData.Length;
                    int Current = 0;
                    int ToSend = 0;

                    if (GeneralLog.DrawLogs)
                        GeneralLog.Log("Send Terrain to :" + Client.Id + ",Packet Size:" + Data + ",Size:" + Size + ",CurrentData:" + i + ",DataCount:" + DataCount);

                    while (Size>0)
                    {
                        ToSend = Math.Min(32000, Size);

                        PacketOut Packet = new PacketOut((byte)TerrainOpcodes.SEND_CREATE);
                        Packet.WriteInt32(Block.LocalPosition.x);
                        Packet.WriteInt32(Block.LocalPosition.y);
                        Packet.WriteInt32(Block.LocalPosition.z);
                        Packet.WriteUInt16(Block.BlockId);
                        Packet.WriteByte(Data.UID);
                        Packet.WriteByte((byte)i);
                        Packet.WriteByte((byte)DataCount);
                        Packet.WriteString(Data.Name);
                        Packet.WriteInt32(TData.Length);
                        Packet.WriteInt32(Current);
                        Packet.WriteInt32(ToSend);

                        Packet.Write(TData, Current, ToSend);
                        Client.SendPacket(Packet);

                        Size -= ToSend;
                        Current += ToSend;
                    }
                }
            }
            catch (Exception ex)
            {
                GeneralLog.LogException(ex);
            }
        }

        public override void SendOperators(SList<ITerrainOperator> Operators)
        {
            PacketOut Packet = new PacketOut((byte)TerrainOpcodes.SEND_OPERATOR);
            Packet.WriteInt32(Operators.Count);
            for (int i = 0; i < Operators.Count; ++i)
            {
                Client.TerrainServer.OperatorManager.Write(Operators.array[i], Packet);
            }
            Client.SendPacket(Packet);
        }

        public delegate void SaveFunctionDelegate(BinaryWriter Write);

        static public byte[] CompressData(ITerrainDataSave Data, CompressionModes Mode)
        {
            using (MemoryStream Stream = new MemoryStream())
            {
                using (ZOutputStream ZStream = new ZOutputStream(Stream, (int)Mode))
                {
                    ZStream.FlushMode = zlibConst.Z_FULL_FLUSH;

                    using (MemoryStream MStream = new MemoryStream())
                    {
                        using (BinaryWriter Writer = new BinaryWriter(MStream))
                        {
                            lock (Data)
                            {
                                Data.Save(Writer);
                            }

                            MStream.Position = 0;
                            byte[] Uncompresssed = new byte[MStream.Length];

                            MStream.Read(Uncompresssed, 0, Uncompresssed.Length);
                            ZStream.Write(Uncompresssed, 0, Uncompresssed.Length);
                        }
                    }
                }

                Stream.Flush();
                return Stream.ToArray();
            }
        }

        #region Distant Objects

        public override void SendMeTo(TerrainObjectServer Object)
        {

        }

        public override void OnAddObjectInRange(TerrainObjectServer Object)
        {

        }

        public override void OnRemoveObjectInRange(TerrainObjectServer Object)
        {

        }

        #endregion
    }
}
