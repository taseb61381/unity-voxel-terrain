﻿using FrameWork;
using System;

namespace TerrainEngine
{
    public class TCPServer : TCPManager
    {
        public TerrainWorldServer TerrainServer;

        public TCPServer()
            : base()
        {
            PacketOut.SizeLen = sizeof(UInt32);
            PacketOut.OpcodeInLen = false;
            PacketOut.SizeInLen = false;
            PacketOut.OpcodeReverse = false;
            PacketOut.SizeReverse = false;
            PacketOut.Struct = PackStruct.SizeAndOpcode;
        }

        protected override BaseClient GetNewClient()
        {
            return new TCPServerClient(this);
        }
    }
}
