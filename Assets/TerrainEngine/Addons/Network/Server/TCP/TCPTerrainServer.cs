﻿
using FrameWork;

namespace TerrainEngine
{
    public class TCPTerrainServer
    {
        public TerrainWorldServer TerrainServer;
        public TCPServer TCPServer;

        public bool Start(string BiomesFolder, int Port = 8000)
        {
            // Starting Terrain Server : Loading Biomes
            TerrainServer = new TerrainWorldServer();
            TerrainServer.Start(BiomesFolder);

            // Starting TCP Network : Listening on port
            if (!TCPManager.Listen<TCPServer>(Port, "TerrainEngine" + Port))
                return false;

            TCPServer = TCPManager.GetTcp<TCPServer>("TerrainEngine" + Port);
            TCPServer.TerrainServer = TerrainServer;

            GeneralLog.LogSuccess("TerrainEngine Server started on port : " + Port);
            return true;
        }

        public void Stop()
        {
            if(TerrainServer != null)
                TerrainServer.Stop(true);

            if(TCPServer != null)
                TCPServer.Stop();
        }
    }
}
