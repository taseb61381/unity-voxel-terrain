﻿
using FrameWork;

namespace TerrainEngine
{
    /// <summary>
    /// Distant client on server
    /// </summary>
    public class TCPServerClient : BaseClient
    {
        /// <summary>
        /// TerrainObject registered to the server. Contains positions.
        /// </summary>
        public TerrainPlayerServer TObject;

        /// <summary>
        /// TerrainServer, managing all terrains and clients
        /// </summary>
        public TerrainWorldServer TerrainServer
        {
            get
            {
                return (Server as TCPServer).TerrainServer;
            }
        }

        public TCPServerClient(TCPServer Server)
            : base(Server)
        {

        }

        public override void OnDisconnect()
        {
            if (TObject != null)
            {
                TObject.RemoveFromWorld();
                TObject = null;
            }
        }

        private PacketIn packet = null;
        private ushort Opcode = 0;
        private ulong PacketSize = 0;
        private bool ReadingData = false;

        protected override void OnReceive(byte[] bytes, int Len)
        {
            PacketIn _Packet = new PacketIn(bytes, 0, Len);
            packet = _Packet;

            long PacketLength = packet.Length;

            while (PacketLength > 0)
            {
                // Reading packet header
                if (!ReadingData)
                {
                    if (PacketLength < 3)
                    {
                        GeneralLog.LogError("OnReceive", "Header invalid " + PacketLength);
                        break;
                    }

                    PacketSize = packet.GetUint32();
                    Opcode = packet.GetUint8();
                    packet.Size = PacketSize;
                    packet = DeCrypt(packet);
                    PacketLength -= 5;

                    if (PacketLength > (long)PacketSize + 4)
                    {
                        GeneralLog.LogDebug("OnReceive", "Packet contains multiple opcodes " + PacketLength + ">" + (PacketSize + 2));
                    }
                    ReadingData = true;
                }
                else
                {
                    ReadingData = false;

                    if (PacketLength >= (long)PacketSize)
                    {
                        byte[] BPack = new byte[PacketSize];
                        packet.Read(BPack, 0, (int)(PacketSize));

                        PacketIn Packet = new PacketIn(BPack, 0, BPack.Length);
                        Packet.Opcode = Opcode;
                        Packet.Size = (ulong)PacketSize;
                        Server.HandlePacket(this, Packet);

                        GeneralLog.LogArray("Received Packet " + (TerrainEngine.TerrainOpcodes)Packet.Opcode, BPack, 0, BPack.Length);

                        PacketLength -= (long)PacketSize;
                    }
                    else
                    {
                        GeneralLog.LogError("OnReceive", "Total packet size invalid :" + PacketLength + "<" + PacketSize);
                        break;
                    }
                }
            }
        }
    }
}
