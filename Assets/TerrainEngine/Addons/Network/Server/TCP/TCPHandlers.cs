﻿using FrameWork;
using UnityEngine;

namespace TerrainEngine
{
    public class TCPHandlers : IPacketHandler
    {
        /// <summary>
        /// Client have modified voxels
        /// </summary>
        [PacketHandlerAttribute(PacketHandlerType.TCP, (int)TerrainOpcodes.SEND_OPERATOR, 0, "SEND_OPERATOR")]
        static public void RECEIVE_SEND_VOXELS(BaseClient client, PacketIn packet)
        {
            TCPServerClient Client = client as TCPServerClient;
            if (Client.TerrainServer == null)
                return;

            ITerrainOperatorNetworkManager Manager = Client.TerrainServer.OperatorManager;

            int Count = packet.GetInt32();

            byte OperatorId = 0;
            for (int i = 0; i < Count; ++i)
            {
                OperatorId = packet.GetUint8();
                ITerrainOperator Operator = Manager.GetNewOperator(OperatorId);
                Manager.Read(Operator, packet);
                Client.TerrainServer.AddOperator(Operator, Client.TObject.Id);
            }

            Client.TerrainServer.OperatorsContainer.IsDirty = true;
        }

        /// <summary>
        /// Client have enabled script
        /// </summary>
        [PacketHandlerAttribute(PacketHandlerType.TCP, (int)TerrainOpcodes.SEND_SCRIPT, 0, "SEND_SCRIPT")]
        static public void RECEIVE_SEND_SCRIPT(BaseClient client, PacketIn packet)
        {

        }

        /// <summary>
        /// Client position is modified
        /// </summary>
        [PacketHandlerAttribute(PacketHandlerType.TCP, (int)TerrainOpcodes.SEND_ALLOWED, 0, "SEND_ALLOWED")]
        static public void RECEIVE_SEND_ALLOWED(BaseClient client, PacketIn packet)
        {
            float x = packet.GetFloat();
            float y = packet.GetFloat();
            float z = packet.GetFloat();

            float fx = packet.GetFloat();
            float fy = packet.GetFloat();
            float fz = packet.GetFloat();

            TCPServerClient Client = client as TCPServerClient;

            // First position received by client, creating an new object on the server
            if (Client.TObject == null)
            {
                Client.TObject = new TerrainPlayerServer();
                Client.TObject.SetClient(Client);

                Client.TerrainServer.AddObject(Client.TObject);
                TCPTerrainClientHandler Handler = new TCPTerrainClientHandler();
                Handler.Client = Client;
                Client.TObject.SetHandler(Handler);
            }

            Client.TObject.SetPosition(x, y, z);
            Client.TObject.SetLoadPosition(fx, fy, fz);
        }
    }
}