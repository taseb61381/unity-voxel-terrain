﻿using FrameWork;
using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace TerrainEngine
{
    public class TCPClient : BaseClient
    {
        public string IP = "127.0.0.1";
        public int Port = 8000;

        public TCPClient() : base(null)
        {
            PacketOut.SizeLen = sizeof(UInt32);
            PacketOut.OpcodeInLen = false;
            PacketOut.SizeInLen = false;
            PacketOut.OpcodeReverse = false;
            PacketOut.SizeReverse = false;
            PacketOut.Struct = PackStruct.SizeAndOpcode;
        }

        public bool Connect()
        {
            try
            {
                if (_socket != null)
                    _socket.Close();

                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _socket.Connect(IP, Port);

                GeneralLog.Log("Connected to : " + IP + ":" + Port);

                BeginReceive();
            }
            catch (Exception e)
            {
                GeneralLog.LogError("Can not connect to server."+IP+":"+Port+"\n" + e.Message, "Server offline");
                return false;
            }

            return true;
        }

        public void Close()
        {
            try
            {
                if (_socket != null)
                    _socket.Close();

            }
            catch (Exception)
            {

            }
        }

        public bool IsConnected()
        {
            if (_socket == null || !_socket.Connected)
                return false;

            return true;
        }


        public delegate void ClientPacketHandler(PacketIn Packet);

        public Dictionary<byte, ClientPacketHandler> Handlers = new Dictionary<byte, ClientPacketHandler>();

        public void RegisterHandler(byte Opcode, ClientPacketHandler Handler)
        {
            Handlers[Opcode] = Handler;
        }

        public void OnReceive(PacketIn packet)
        {
            lock (packet)
            {
                ClientPacketHandler Handler = null;
                if (Handlers.TryGetValue((byte)packet.Opcode, out Handler))
                    Handler(packet);
            }
        }

        private byte[] Stream;
        private int Position = 0;
        private ushort Opcode = 0;
        private int PacketSize = 0;
        private bool ReadingData = true;
        private int PacketRemain
        {
            get
            {
                return PacketSize - Position;
            }
        }

        protected override void OnReceive(byte[] bytes, int Len)
        {
            int Total = Len;
            int Offset = 0;
            while (Len > 0)
            {
                Offset = (Total - Len);

                if (ReadingData)
                {
                    if (Len < 5)
                        return;

                    ReadingData = false;
                    PacketSize = (int)Marshal.ConvertToUInt32(bytes[Offset], bytes[Offset+1], bytes[Offset+2], bytes[Offset+3]);
                    Opcode = bytes[Offset+4];
                    Stream = new byte[PacketSize];
                    Len -= 5;
                }
                
                int ToAdd = Math.Min(PacketRemain, Len);
                Offset = (Total-Len);
                if (ToAdd != 0)
                {
                    Array.Copy(bytes, Offset, Stream, Position, ToAdd);
                    Position += ToAdd;
                    Len -= ToAdd;
                }

                if (PacketRemain == 0)
                {
                    ReadingData = true;
                    PacketIn Packet = new PacketIn(Stream, 0, Stream.Length);
                    Packet.Size = (ulong)PacketSize;
                    Packet.Opcode = Opcode;
                    Position = 0;
                    OnReceive(Packet);
                }
            }
        }
    }
}
