﻿using FrameWork;

namespace TerrainEngine
{
    [TerrainOperatorAttribute(0,typeof(BoxOperator),false)]
    public class BoxOperatorNetwork : ITerrainOperatorNetwork
    {
        public override void Write(ITerrainOperator Operator, object Packet)
        {
            BoxOperator CustomOperator = Operator as BoxOperator;
            PacketOut packet = Packet as PacketOut;

            // Default Header
            packet.WriteByte(OperatorId);
            packet.WriteUInt16(Operator.BlockId);
            packet.WriteInt32(Operator.x);
            packet.WriteInt32(Operator.y);
            packet.WriteInt32(Operator.z);
            packet.WriteByte(Operator.Type);
            packet.WriteFloat(Operator.Volume);
            packet.WriteByte((byte)Operator.ModType);

            // Custom Data
            packet.WriteInt32(CustomOperator.SizeX);
            packet.WriteInt32(CustomOperator.SizeY);
            packet.WriteInt32(CustomOperator.SizeZ);
        }

        public override void Read(ITerrainOperator Operator, object Packet)
        {
            BoxOperator CustomOperator = Operator as BoxOperator;
            PacketIn packet = Packet as PacketIn;

            // Default Header
            CustomOperator.BlockId = packet.GetUint16();
            CustomOperator.x = packet.GetInt32();
            CustomOperator.y = packet.GetInt32();
            CustomOperator.z = packet.GetInt32();
            CustomOperator.Type = packet.GetUint8();
            CustomOperator.Volume = packet.GetFloat();
            CustomOperator.ModType = (VoxelModificationType)packet.GetUint8();

            // Custom Data
            CustomOperator.SizeX = packet.GetInt32();
            CustomOperator.SizeY = packet.GetInt32();
            CustomOperator.SizeZ = packet.GetInt32();
        }
    }

    [TerrainOperatorAttribute(1, typeof(SphereOperator), false)]
    public class SphereOperatorNetwork : ITerrainOperatorNetwork
    {
        public override void Write(ITerrainOperator Operator, object Packet)
        {
            SphereOperator CustomOperator = Operator as SphereOperator;
            PacketOut packet = Packet as PacketOut;

            // Default Header
            packet.WriteByte(OperatorId);
            packet.WriteUInt16(Operator.BlockId);
            packet.WriteInt32(Operator.x);
            packet.WriteInt32(Operator.y);
            packet.WriteInt32(Operator.z);
            packet.WriteByte(Operator.Type);
            packet.WriteFloat(Operator.Volume);
            packet.WriteByte((byte)Operator.ModType);

            // Custom Data
            packet.WriteInt32(CustomOperator.Radius);
        }

        public override void Read(ITerrainOperator Operator, object Packet)
        {
            SphereOperator CustomOperator = Operator as SphereOperator;
            PacketIn packet = Packet as PacketIn;

            // Default Header
            CustomOperator.BlockId = packet.GetUint16();
            CustomOperator.x = packet.GetInt32();
            CustomOperator.y = packet.GetInt32();
            CustomOperator.z = packet.GetInt32();
            CustomOperator.Type = packet.GetUint8();
            CustomOperator.Volume = packet.GetFloat();
            CustomOperator.ModType = (VoxelModificationType)packet.GetUint8();

            // Custom Data
            CustomOperator.Radius = packet.GetInt32();
        }
    }

    [TerrainOperatorAttribute(2, typeof(CylinderOperator), false)]
    public class CylinderOperatorNetwork : ITerrainOperatorNetwork
    {
        public override void Write(ITerrainOperator Operator, object Packet)
        {
            CylinderOperator CustomOperator = Operator as CylinderOperator;
            PacketOut packet = Packet as PacketOut;

            // Default Header
            packet.WriteByte(OperatorId);
            packet.WriteUInt16(Operator.BlockId);
            packet.WriteInt32(Operator.x);
            packet.WriteInt32(Operator.y);
            packet.WriteInt32(Operator.z);
            packet.WriteByte(Operator.Type);
            packet.WriteFloat(Operator.Volume);
            packet.WriteByte((byte)Operator.ModType);

            // Custom Data
            packet.WriteInt32(CustomOperator.Height);
            packet.WriteInt32(CustomOperator.Size);
            packet.WriteQuaternion(CustomOperator.Rotation);
        }

        public override void Read(ITerrainOperator Operator, object Packet)
        {
            CylinderOperator CustomOperator = Operator as CylinderOperator;
            PacketIn packet = Packet as PacketIn;

            // Default Header
            CustomOperator.BlockId = packet.GetUint16();
            CustomOperator.x = packet.GetInt32();
            CustomOperator.y = packet.GetInt32();
            CustomOperator.z = packet.GetInt32();
            CustomOperator.Type = packet.GetUint8();
            CustomOperator.Volume = packet.GetFloat();
            CustomOperator.ModType = (VoxelModificationType)packet.GetUint8();

            // Custom Data
            CustomOperator.Height = packet.GetInt32();
            CustomOperator.Size = packet.GetInt32();
            CustomOperator.Rotation = packet.GetQuaternion();
        }
    }

    [TerrainOperatorAttribute(3, typeof(LineOperator), false)]
    public class LineOperatorNetwork : ITerrainOperatorNetwork
    {
        public override void Write(ITerrainOperator Operator, object Packet)
        {
            LineOperator CustomOperator = Operator as LineOperator;
            PacketOut packet = Packet as PacketOut;

            // Default Header
            packet.WriteByte(OperatorId);
            packet.WriteUInt16(Operator.BlockId);
            packet.WriteInt32(Operator.x);
            packet.WriteInt32(Operator.y);
            packet.WriteInt32(Operator.z);
            packet.WriteByte(Operator.Type);
            packet.WriteFloat(Operator.Volume);
            packet.WriteByte((byte)Operator.ModType);

            // Custom Data
            packet.WriteInt32(CustomOperator.EndX);
            packet.WriteInt32(CustomOperator.EndY);
            packet.WriteInt32(CustomOperator.EndZ);
        }

        public override void Read(ITerrainOperator Operator, object Packet)
        {
            LineOperator CustomOperator = Operator as LineOperator;
            PacketIn packet = Packet as PacketIn;

            // Default Header
            CustomOperator.BlockId = packet.GetUint16();
            CustomOperator.x = packet.GetInt32();
            CustomOperator.y = packet.GetInt32();
            CustomOperator.z = packet.GetInt32();
            CustomOperator.Type = packet.GetUint8();
            CustomOperator.Volume = packet.GetFloat();
            CustomOperator.ModType = (VoxelModificationType)packet.GetUint8();

            // Custom Data
            CustomOperator.EndX = packet.GetInt32();
            CustomOperator.EndY = packet.GetInt32();
            CustomOperator.EndZ = packet.GetInt32();
        }
    }

    [TerrainOperatorAttribute(4, typeof(TunelOperator), false)]
    public class TunelOperatorNetwork : ITerrainOperatorNetwork
    {
        public override void Write(ITerrainOperator Operator, object Packet)
        {
            TunelOperator CustomOperator = Operator as TunelOperator;
            PacketOut packet = Packet as PacketOut;

            // Default Header
            packet.WriteByte(OperatorId);
            packet.WriteUInt16(Operator.BlockId);
            packet.WriteInt32(Operator.x);
            packet.WriteInt32(Operator.y);
            packet.WriteInt32(Operator.z);
            packet.WriteByte(Operator.Type);
            packet.WriteFloat(Operator.Volume);
            packet.WriteByte((byte)Operator.ModType);

            // Custom Data
            packet.WriteInt32(CustomOperator.Height);
            packet.WriteInt32(CustomOperator.Size);
            packet.WriteInt32(CustomOperator.EmptySize);
            packet.WriteFloat(CustomOperator.EmptyVolume);
            packet.WriteQuaternion(CustomOperator.Rotation);
        }

        public override void Read(ITerrainOperator Operator, object Packet)
        {
            TunelOperator CustomOperator = Operator as TunelOperator;
            PacketIn packet = Packet as PacketIn;

            // Default Header
            CustomOperator.BlockId = packet.GetUint16();
            CustomOperator.x = packet.GetInt32();
            CustomOperator.y = packet.GetInt32();
            CustomOperator.z = packet.GetInt32();
            CustomOperator.Type = packet.GetUint8();
            CustomOperator.Volume = packet.GetFloat();
            CustomOperator.ModType = (VoxelModificationType)packet.GetUint8();

            // Custom Data
            CustomOperator.Height = packet.GetInt32();
            CustomOperator.Size = packet.GetInt32();
            CustomOperator.EmptySize = packet.GetInt32();
            CustomOperator.EmptyVolume = packet.GetFloat();
            CustomOperator.Rotation = packet.GetQuaternion();
        }
    }

    [TerrainOperatorAttribute(5, typeof(VoxelsOperator), false)]
    public class VoxelsOperatorNetwork : ITerrainOperatorNetwork
    {
        public override void Write(ITerrainOperator Operator, object Packet)
        {
            VoxelsOperator CustomOperator = Operator as VoxelsOperator;
            PacketOut packet = Packet as PacketOut;

            // Default Header
            packet.WriteByte(OperatorId);

            // Custom Data
            packet.WriteInt32(CustomOperator.Voxels.Count);
            for (int i = 0; i < CustomOperator.Voxels.Count; ++i)
            {
                packet.WriteUInt16(CustomOperator.Voxels[i].BlockId);
                packet.WriteInt32(CustomOperator.Voxels[i].x);
                packet.WriteInt32(CustomOperator.Voxels[i].y);
                packet.WriteInt32(CustomOperator.Voxels[i].z);
                packet.WriteByte(CustomOperator.Voxels[i].Type);
                packet.WriteFloat(CustomOperator.Voxels[i].Volume);
                packet.WriteByte((byte)CustomOperator.Voxels[i].ModType);
            }
        }

        public override void Read(ITerrainOperator Operator, object Packet)
        {
            VoxelsOperator CustomOperator = Operator as VoxelsOperator;
            PacketIn packet = Packet as PacketIn;

            // CustomData
            VoxelPoint P = new VoxelPoint();
            int VoxelsCount = packet.GetInt32();
            for (int i = 0; i < VoxelsCount; ++i)
            {
                // Default Header
                P.BlockId = packet.GetUint16();
                P.x = packet.GetInt32();
                P.y = packet.GetInt32();
                P.z = packet.GetInt32();
                P.Type = packet.GetUint8();
                P.Volume = packet.GetFloat();
                P.ModType = (VoxelModificationType)packet.GetUint8();
                CustomOperator.Voxels.Add(P);
            }
        }
    }
}
