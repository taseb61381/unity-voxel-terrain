﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerrainEngine
{
    public class TerrainPlayerServer : TerrainObjectServer
    {
        public TCPServerClient Client;

        public TerrainPlayerServer() : base()
        {

        }

        public void SetClient(TCPServerClient Client)
        {
            this.Client = Client;
        }
    }
}
