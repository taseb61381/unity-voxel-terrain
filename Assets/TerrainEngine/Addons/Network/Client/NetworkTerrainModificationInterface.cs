﻿using FrameWork;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;


public class NetworkTerrainModificationInterface : ITerrainNetworkInterface
{
    public Transform CTransform; // Object that will send positions to server. (Player)
    public Vector3 FixedPositions; // This position will replace the position of the player if != 0. used to only load terrain at X,Y,Z value.
    public string IP = "127.0.0.1";
    public int Port = 8000;
    public TCPClient Client;

    private float UpdateTime = 0;

    /// <summary>
    /// No server returned here
    /// </summary>
    public override TerrainWorldServer GetServer()
    {
        return null;
    }

    void OnDestroy()
    {
        if (Client != null)
        {
            Client.Close();
            Client = null;
        }
    }

    #region Client

    public override void OnManagerInited(TerrainManager Manager)
    {
        OperatorManager = new ITerrainOperatorNetworkManager();
        OperatorManager.LoadOperators();

        if (CTransform == null)
        {
            Debug.LogError(name + " : No Tranform on 'CTransform' variable. No object to load terrain around. Please link an object (player)");
            enabled = false;
        }

        Client = new TCPClient();
        Client.IP = IP;
        Client.Port = Port;
        Client.RegisterHandler((byte)TerrainOpcodes.SEND_ABORD, TerrainAbord);
        Client.RegisterHandler((byte)TerrainOpcodes.SEND_ALLOWED, AllowedTerrainList);
        Client.RegisterHandler((byte)TerrainOpcodes.SEND_CREATE, TerrainCreate);
        Client.RegisterHandler((byte)TerrainOpcodes.SEND_GENERATING, TerrainGenerating);
        Client.RegisterHandler((byte)TerrainOpcodes.SEND_SCRIPT, ActiveScript);
        Client.RegisterHandler((byte)TerrainOpcodes.SEND_OPERATOR, ReceiveOperators);
        Client.Connect();
    }

    public new void Update()
    {
        base.Update();

        if (TerrainManager.Instance == null || !TerrainManager.Instance.isStarted || Client == null || !Client.IsConnected())
            return;

        if (UpdateTime < ThreadManager.UnityTime)
        {
            UpdateTime = ThreadManager.UnityTime + 1f;

            PacketOut Out = new PacketOut((byte)TerrainOpcodes.SEND_ALLOWED);

            // Send the real position of this object
            Out.WriteFloat(CTransform.position.x);
            Out.WriteFloat(CTransform.position.y);
            Out.WriteFloat(CTransform.position.z);

            // Send the position to load around terrain
            Out.WriteFloat(FixedPositions.x == 0 ? CTransform.position.x : FixedPositions.x);
            Out.WriteFloat(FixedPositions.y == 0 ? CTransform.position.y : FixedPositions.y);
            Out.WriteFloat(FixedPositions.z == 0 ? CTransform.position.z : FixedPositions.z);
            Client.SendTCP(Out);
        }
    }

    #endregion

    #region PacketSendToServer

    public ITerrainOperatorNetworkManager OperatorManager;
    public long OperatorCountPosition;
    public int OperatorCount = 0;
    PacketOut FlushPacket;
    PacketOut CustomFlushPacket;

    public override void OnOperatorFlush(ITerrainOperator Operator)
    {
        // Operator is received from network. Don"t send it again to server
        if (Operator.IsNetwork)
            return;

        if (FlushPacket == null)
        {
            FlushPacket = new PacketOut((byte)TerrainOpcodes.SEND_OPERATOR);
            OperatorCountPosition = FlushPacket.Position;
            OperatorCount = 0;
            FlushPacket.WriteInt32(OperatorCount);
        }

        OperatorManager.Write(Operator, FlushPacket);
        ++OperatorCount;
    }

    public override void OnFlushDone()
    {
        if (FlushPacket != null)
        {
            // Write the number of operators in packet
            long OriginalPosition = FlushPacket.Position;

            FlushPacket.Position = OperatorCountPosition;
            FlushPacket.WriteInt32(OperatorCount);

            FlushPacket.Position = OriginalPosition;

            Client.SendTCP(FlushPacket);
            FlushPacket = null;
        }
    }


    #endregion

    #region PacketReceivedFromServer

    public List<VectorI3> AllowedTerrains = new List<VectorI3>();

    /// <summary>
    /// Server is generating Terrain. Update progress bar
    /// </summary>
    /// <param name="Packet"></param>
    public void TerrainGenerating(PacketIn Packet)
    {
        VectorI3 Position = new VectorI3();
        Position.x = Packet.GetInt32();
        Position.y = Packet.GetInt32();
        Position.z = Packet.GetInt32();
        //Debug.Log("Terrain Generating :" + Position);
        ProgressSystem.Instance.SetProgress("Build Terrain : " + Position.ToString(), 0, ThreadManager.UnityTime);
    }

    /// <summary>
    /// Server abord Terrain Generation. Remove terrain from progress bar
    /// </summary>
    /// <param name="Packet"></param>
    public void TerrainAbord(PacketIn Packet)
    {
        VectorI3 Position = new VectorI3();
        Position.x = Packet.GetInt32();
        Position.y = Packet.GetInt32();
        Position.z = Packet.GetInt32();
        //Debug.Log("Terrain Abord :" + Position);
        ProgressSystem.Instance.SetProgress("Build Terrain : " + Position.ToString(), 100f, ThreadManager.UnityTime);
    }

    public Dictionary<ushort, TerrainDatas> ReceivingTerrainData = new Dictionary<ushort, TerrainDatas>();
 
    /// <summary>
    /// Receive Terrain Data from server. Add Data to generation list
    /// </summary>
    public void TerrainCreate(PacketIn Packet)
    {
        VectorI3 Position = new VectorI3();
        Position.x = Packet.GetInt32();
        Position.y = Packet.GetInt32();
        Position.z = Packet.GetInt32();
        ushort BlockId = Packet.GetUint16();
        byte UID = Packet.GetUint8();
        byte CurrentData = Packet.GetUint8();
        byte TotalData = Packet.GetUint8();
        string DataName = Packet.GetString();
        int TotalSize = Packet.GetInt32();
        int CurrentPosition = Packet.GetInt32();
        int ReceivedSize = Packet.GetInt32();
        byte[] CompressedData = null;

        if (GeneralLog.DrawLogs)
            GeneralLog.Log(Position + " Received " + ReceivedSize + ",Total:" + TotalSize + ",CurrentData:" + CurrentData + ",TotalData:" + TotalData);

        TerrainDatas Voxels = null;
        if (!ReceivingTerrainData.TryGetValue(BlockId, out Voxels))
        {
            if (CurrentData == 0)
            {
                Voxels = TerrainManager.Instance.Informations.GetData(Position);
                Voxels.IsUsedByClient = true;
                Voxels.BlockId = BlockId;
                ReceivingTerrainData.Add(BlockId, Voxels);

                if (GeneralLog.DrawLogs)
                    GeneralLog.Log("Create new data");
            }
        }

        if (Voxels == null)
            return;

        if(CurrentData == 0)
        {
            if (Voxels.CompressedData == null)
                Voxels.CompressedData = new byte[TotalSize];

            CompressedData = Voxels.CompressedData;

            if (GeneralLog.DrawLogs)
                GeneralLog.Log("Create new array");
        }
        else
        {
            ICustomTerrainData Custom = Voxels.GetCustomData<ICustomTerrainData>(DataName);
            if (Custom != null)
            {
                Custom.UID = UID;
                if (Custom.CompressedData == null)
                    Custom.CompressedData = new byte[TotalSize];

                CompressedData = Custom.CompressedData;
            }
        }

        if (CompressedData != null)
        {
            if (GeneralLog.DrawLogs)
                GeneralLog.Log("Read packet at position:" + CurrentPosition);

            Packet.Read(CompressedData, CurrentPosition, ReceivedSize);
        }

        if (CurrentData == TotalData && CurrentPosition+ReceivedSize >= TotalSize)
        {
            TerrainManager.Instance.Builder.LoadBlock(Voxels);
        }
    }

    /// <summary>
    /// Receive allowed terrain list. Terrains not contained in this list are removed
    /// </summary>
    public void AllowedTerrainList(PacketIn Packet)
    {
        AllowedTerrains.Clear();
        ushort Count = Packet.GetUint16();
        VectorI3 v = new VectorI3();
        for (int i = 0; i < Count; ++i)
        {
            v.x = Packet.GetInt32();
            v.y = Packet.GetInt32();
            v.z = Packet.GetInt32();
            AllowedTerrains.Add(v);
        }

        if (GeneralLog.DrawLogs)
            GeneralLog.Log("Received allowed terrains:" + Count);

        TerrainManager.Instance.Builder.LoadBlocks(AllowedTerrains);
    }

    /// <summary>
    /// Receive voxels modification from server. Create and Apply operator
    /// </summary>
    public void ReceiveOperators(PacketIn Packet)
    {
        int OperatorsCount = Packet.GetInt32();
        
        for (int i = 0; i < OperatorsCount; ++i)
        {
            byte OperatorId = Packet.GetUint8();

            ITerrainOperator Operator = OperatorManager.GetNewOperator(OperatorId);
            OperatorManager.Read(Operator, Packet);
            Operator.IsNetwork = true;

            TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
        }

        TerrainManager.Instance.ModificationInterface.SetDirty();
    }

    public void DatasModification(PacketIn Packet)
    {
        ushort Count = Packet.GetUint16();

        byte ProcessorID;
        ushort x, y, z, UID;
        byte Type;
        float Float;
        int Int;
        uint TObject;
        byte ModType;
        TerrainBlock Block;
        for (int i = 0; i < Count; ++i)
        {
            ProcessorID = Packet.GetUint8();
            UID = Packet.GetUint16();
            Block = TerrainManager.Instance.Container.GetBlock(UID) as TerrainBlock;
            x = Packet.GetUint16();
            y = Packet.GetUint16();
            z = Packet.GetUint16();
            ModType = Packet.GetUint8();

            if(ModType <= 1)
                Type = Packet.GetUint8();

            if (ModType == 1)
                Int = Packet.GetInt32();

            if(ModType >= 1)
                Float = Packet.GetFloat();
            TObject = Packet.GetUint32();
        }

        TerrainManager.Instance.ModificationInterface.Flush(null, null, false, false);
    }

    static public void ActiveScript(PacketIn Packet)
    {
        TerrainBlock Block = TerrainManager.Instance.Container.GetBlock(Packet.GetUint16()) as TerrainBlock;
        if (Block == null)
        {
            GeneralLog.LogError("Active Script : Block == null");
            return;
        }

        GameObjectClientDatas Data = Block.Voxels.GetCustomDataByID<GameObjectClientDatas>(Packet.GetUint8());
        if (Data == null)
        {
            GeneralLog.LogError("Active Script : Data == null");
            return;
        }

        Data.ActiveScript(Packet.GetUint16(), Packet.GetUint16(), Packet.GetUint16(), Packet.GetString(), Packet.GetUint8() == 0 ? false : true);
    }

    #endregion
}
