﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TerrainEngine
{
    public class ClientAsServerScript : IBlockProcessor
    {
        public TerrainManager Manager;

        /// <summary>
        /// This script will start a server and manage server packets
        /// </summary>
        public HostTerrainModificationInterface ModificationInterface;

        /// <summary>
        /// This script will manage all players positions  and update gameobjects/terrain lod around them
        /// </summary>
        public TerrainNoCameraHandler NoCameraHandler;

        public class PlayerTracker
        {
            public TerrainObjectServer ServerObject;
            public TerrainObject ClientObject;
            public int TransformId = -1;
            public bool Disconnected = false;
            public List<VectorI3> AllowedBlocks = new List<VectorI3>();

            public void Update(ClientAsServerScript Script)
            {
                if (ClientObject == null)
                {
                    GameObject Obj = GameObject.CreatePrimitive(PrimitiveType.Capsule);
                    GameObject.Destroy(Obj.GetComponent<CapsuleCollider>());
                    Obj.name = "Player:" + ServerObject.Id;
                    ClientObject = Obj.AddComponent<TerrainObject>();
                    Obj.AddComponent<TerrainObjectColliderViewer>();
                    TransformId = Script.NoCameraHandler.Container.AddTransform(ServerObject.Position, Quaternion.identity);
                }

                (Script.Manager as TransvoxelManager).UpdateLOD(ActionProcessor.Unity);
                ClientObject.transform.position = ServerObject.Position;
                Script.NoCameraHandler.Container.UpdateTransform(TransformId,ServerObject.Position,Quaternion.identity);
            }

            public void Delete(ClientAsServerScript Script)
            {
                GameObject.Destroy(ClientObject.gameObject);
                Script.NoCameraHandler.Container.RemoveTransform(TransformId);
            }
        }

        public SList<PlayerTracker> Trackers;
        public List<VectorI3> AllowedBlocks = new List<VectorI3>();

        public void Init(TerrainManager Manager, string IP,int Port)
        {
            this.Manager = Manager;
            NoCameraHandler = Manager.gameObject.AddComponent<TerrainNoCameraHandler>();
            ModificationInterface = Manager.gameObject.AddComponent<HostTerrainModificationInterface>();
            LocalTerrainObjectServerScript Local = Manager.GetComponent<LocalTerrainObjectServerScript>();
            if (Local != null)
                GameObject.Destroy(Local);

            Manager.ModificationInterface = ModificationInterface;

            Trackers = new SList<PlayerTracker>(1);
        }

        public void Update()
        {
            lock (Trackers.array)
            {
                for (int i = Trackers.Count - 1; i >= 0; --i)
                {
                    if (Trackers.array[i].Disconnected)
                    {
                        Trackers.array[i].Delete(this);
                        Trackers.RemoveAt(i);
                        continue;
                    }
                    else
                        Trackers.array[i].Update(this);
                }
            }
        }

        public void OnGUI()
        {
            PlayerTracker Pl;

            GUI.Box(new Rect(0, 0, Screen.width / 3, 20 * Trackers.Count + 40), "");
            for(int i=0;i<Trackers.Count;++i)
            {
                Pl = Trackers.array[i];
                GUI.Label(new Rect(5,20 + i*20,Screen.width,20), "Id:" + Pl.ServerObject.Id + ",TransformId:" + Pl.TransformId + ",ServerPos:" + Pl.ServerObject.Position + ",Blocks:" + Pl.AllowedBlocks.Count);
            }
        }

        public override void OnServerAddObject(TerrainObjectServer Object)
        {
            GeneralLog.Log("OnClientServer Add Object :" + Object.Id);

            lock(Trackers.array)
            {
                PlayerTracker Tracker = new PlayerTracker();
                Tracker.ServerObject = Object;
                Trackers.Add(Tracker);
            }
        }

        public override void OnServerRemoveObject(TerrainObjectServer Object)
        {
            GeneralLog.Log("OnClientServer Remove Object :" + Object.Id);

            lock(Trackers.array)
            {
                for (int i = Trackers.Count - 1; i >= 0; --i)
                {
                    if (Trackers.array[i].ServerObject == Object)
                    {
                        Trackers.array[i].Disconnected = true;
                    }
                }
            }
        }

        public override void OnServerSendAllowedBlocks(TerrainObjectServer Object, List<VectorI3> Positions)
        {
            AllowedBlocks.Clear();

            lock (Trackers.array)
            {
                for (int i = Trackers.Count - 1; i >= 0; --i)
                {
                    if (Trackers.array[i].ServerObject == Object)
                    {
                        Trackers.array[i].AllowedBlocks.Clear();
                        Trackers.array[i].AllowedBlocks.AddRange(Positions);
                    }

                    AllowedBlocks.AddRange(Trackers.array[i].AllowedBlocks);
                }
            }

            GeneralLog.Log("OnClientServer Send Allowed Block :" + AllowedBlocks.Count);
            TerrainManager.Instance.Builder.LoadBlocks(AllowedBlocks);
        }

        public override void OnServerSendCreateBlock(TerrainObjectServer Object, TerrainBlockServer Block)
        {
            GeneralLog.Log("OnClientServer Send Create Block :" + Block);
            TerrainManager.Instance.Builder.LoadBlock(Block.Voxels);
        }
    }
}
