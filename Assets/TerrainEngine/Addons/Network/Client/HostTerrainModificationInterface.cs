﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using FrameWork;

namespace TerrainEngine
{
    public class HostTerrainModificationInterface : ITerrainNetworkInterface
    {
        public string IP = "0.0.0.0";
        public int Port = 8000;
        public TCPServer TCPServer;

        public LocalTerrainObjectServerScript LocalObject;

        public override void OnManagerInited(TerrainManager Manager)
        {
            base.OnManagerInited(Manager);

            // Starting TCP Network : Listening on port
            if (!TCPManager.Listen<TCPServer>(Port, "TerrainEngine" + Port))
            {
                Debug.Log("Can not start Host Server");
                return;
            }

            TCPServer = TCPManager.GetTcp<TCPServer>("TerrainEngine" + Port);
            TCPServer.TerrainServer = Manager.Server;
            TCPServer.TerrainServer.OperatorsContainer = OperatorsContainer;
            TCPServer.TerrainServer.CanFlushVoxels = false;

            GeneralLog.LogSuccess("TerrainEngine Server started on port : " + Port);
        }

        public override void AddOperator(ITerrainOperator Operator)
        {
            if (Operator == null || LocalObject == null || LocalObject.ServerObject == null)
                return;

            Operator.ObjectId = LocalObject.ServerObject.Id;
            base.AddOperator(Operator);
        }

        public override bool OnModifyVoxel(ITerrainOperator Operator, ref VoxelFlush Flush)
        {
            if (Operator == null || LocalObject == null || LocalObject.ServerObject == null)
                return false;

            return TCPServer.TerrainServer.OnModifyVoxel(Operator, ref Flush);
        }
    }
}
