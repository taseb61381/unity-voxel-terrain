﻿using System.Collections.Generic;
using System.IO;
using TerrainEngine;
using UnityEngine;

public class TerrainServerGUI : MonoBehaviour 
{
	public string BiomesFolder = "";
	public int Port = 8000;
	public bool UseLastConfig = true;
	public TCPTerrainServer Server;
	public List<string> Messages = new List<string>();

    public bool IsValid()
    {
        return File.Exists(Path.Combine(BiomesFolder, "WorldGenerator.xml"));
    }

	// Use this for initialization
	void Start ()
	{
		GeneralLog.Log("TerrainServerGUI : Starting");
		GeneralLog.OnLog += Log;
		GeneralLog.OnLogDebug += Log;
		GeneralLog.OnLogError += Log;
		GeneralLog.OnLogSuccess += Log;
		GeneralLog.OnLogWarning += Log;
		GeneralLog.OnLogException += Log;

		if (UseLastConfig)
			BiomesFolder = PlayerPrefs.GetString("BiomesFolder","");

		if (BiomesFolder.Length == 0)
		{
			string[] Files = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.xml", SearchOption.AllDirectories);
			foreach (string f in Files)
			{
				if (f.Contains("WorldGenerator"))
				{
					BiomesFolder = Path.GetDirectoryName(f);
					break;
				}
			}
		}
	}

	private Vector2 ScrollPosition = new Vector2();
	private string[] Tabs = new string[] { "Options", "Players", "Stats" };
	private int Selected = 0;
	void OnGUI()
	{
		GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
		{
			Selected = GUILayout.SelectionGrid(Selected, Tabs, Tabs.Length);
			if(Selected == 0)
			{
				DrawOptions();
			}
			else if(Selected == 1)
			{
				DrawPlayers();
			}
			else if (Selected == 2)
			{
				DrawStats();
			}
		}
		GUILayout.EndArea();
	}

	public void DrawOptions()
	{
		if (Server == null)
		{
			GUILayout.BeginVertical("box");
			{
				GUILayout.BeginHorizontal();
				GUILayout.Label("Server Port:", GUILayout.Width(100));
				string Value = GUILayout.TextField(Port.ToString(), GUILayout.Width(300));
				int.TryParse(Value, out Port);

				if (GUILayout.Button("Start Server",GUILayout.Width(150)))
				{
					Server = new TCPTerrainServer();
					if (!Server.Start(BiomesFolder, Port))
						Server = null;
				}
				GUILayout.EndHorizontal();

				GUILayout.BeginHorizontal();
				GUILayout.Label("Biomes Folder:", GUILayout.Width(100));
				BiomesFolder = GUILayout.TextField(BiomesFolder, GUILayout.Width(300));
				if (GUILayout.Button("Open",GUILayout.Width(70)))
				{
					OpenInFileBrowser.Open(BiomesFolder);
				}
				GUILayout.EndHorizontal();

				string Text = "";

                if (!IsValid())
				{
					GUI.color = Color.red;

					Text = "Could not find : WorldGenerator.xml";
				}
				else
				{
					GUI.color = Color.green;

					Text = "WorldGenerator.xml found";

					if (UseLastConfig)
						PlayerPrefs.SetString("BiomesFolder", BiomesFolder);
				}

				GUILayout.Label(Text);

				GUI.color = Color.white;
			}
			GUILayout.EndVertical();
		}

		if (Server != null)
		{
			GUILayout.BeginVertical("Server", GUI.skin.box);
			{
				if (Server.TerrainServer != null)
				{
					GUILayout.Label("TerrainEngine Server started");
					GUILayout.Label("Players : " + Server.TerrainServer.Objects.Count);
					GUILayout.Label("Terrains Loaded : " + Server.TerrainServer.Blocks.Count);

					if (Server.TerrainServer.Manager != null)
						GUILayout.Label("Threads : " + Server.TerrainServer.Manager.Threads.Length);
				}

				if (GUILayout.Button("Save and Stop Server"))
				{
					Server.Stop();
					Server = null;
				}
			}
			GUILayout.EndVertical();
		}

		GUILayout.BeginVertical("Logs", GUI.skin.box);
		{
			ScrollPosition = GUILayout.BeginScrollView(ScrollPosition);
			{
				int Count = 0;
				for (int i = Messages.Count - 1; i >= 0; --i)
				{
					GUILayout.Label(Messages[i]);
					if (Count > 500)
						break;
					++Count;
				}
			}
			GUILayout.EndScrollView();
		}
		GUILayout.EndVertical();
	}

	private TerrainObjectServer SelectedPlayer = null;
	public void DrawPlayers()
	{
		if (Server == null)
			return;

		GUILayout.BeginArea(new Rect(0, 40, Screen.width / 2, Screen.height - 40));
		{
			GUILayout.BeginVertical("Players:" + Server.TerrainServer.Objects.Count, GUI.skin.box, GUILayout.Height(Screen.height - 40));
			{
				GUILayout.Space(20);
				TerrainObjectServer Obj = null;
				for (int i = 0; i < Server.TerrainServer.Objects.Count; i++)
				{
					Obj = Server.TerrainServer.Objects.array[i];
					if (Obj != null && Obj.Handlers != null)
					{
						if (GUILayout.Button(Obj.Handlers.GetName()))
							SelectedPlayer = Obj;
					}
				}
			}
			GUILayout.EndVertical();
		}
		GUILayout.EndArea();

		if(SelectedPlayer != null)
		{
			GUILayout.BeginArea(new Rect(Screen.width / 2, 40, Screen.width / 2, Screen.height - 40));
			{
				GUILayout.BeginVertical("box");
				{
					GUILayout.Label("Name: " + SelectedPlayer.Handlers.GetName());
					GUILayout.Label("Position: " + SelectedPlayer.Position);
					GUILayout.Label("Current Block: " + SelectedPlayer.CurrentBlockPosition);
					GUILayout.Label("Current Chunk: " + SelectedPlayer.CurrentChunkPosition);
					GUILayout.Label("Current Voxel: " + SelectedPlayer.CurrentVoxel);
					GUILayout.Label("Around Players: " + SelectedPlayer.RangedObjects.Count);
					GUILayout.Label("Around Blocks: " + SelectedPlayer.VisibleBlocks.Count);
					GUILayout.EndVertical();
				}
			}
			GUILayout.EndArea();
		}
	}

	public void DrawStats()
	{
		if (Server == null)
			return;

		GUILayout.BeginVertical("box");
		GUILayout.Label("IsGenerating: " + Server.TerrainServer.WorldGenerator.IsGenerating);
		if (Server.TerrainServer.WorldGenerator.IsGenerating)
		{
			GUILayout.Label("Generating Blocks:" + Server.TerrainServer.WorldGenerator.GetGeneratingBlocksCount());
			GUILayout.Label("Position:" + Server.TerrainServer.WorldGenerator.GeneratingBlock.Block.LocalPosition);
			GUILayout.Label("Generated Chunks:" + Server.TerrainServer.WorldGenerator.GetGeneratedChunks());
			GUILayout.Label("Total Chunks To Generate:" + Server.TerrainServer.WorldGenerator.GetTotalChunksToGenerate());
			GUILayout.Label(Server.TerrainServer.WorldGenerator.ToString());
		}
		GUILayout.EndVertical();
	}

	public void Log(object message)
	{
		if (message != null)
			Messages.Add("[" + Messages.Count + "] : " + message.ToString());
	}
}

public static class OpenInFileBrowser
{
	public static bool IsInMacOS
	{
		get
		{
			return UnityEngine.SystemInfo.operatingSystem.IndexOf("Mac OS") != -1;
		}
	}

	public static bool IsInWinOS
	{
		get
		{
			return UnityEngine.SystemInfo.operatingSystem.IndexOf("Windows") != -1;
		}
	}

	public static void OpenInMac(string path)
	{
		bool openInsidesOfFolder = false;

		// try mac
		string macPath = path.Replace("\\", "/"); // mac finder doesn't like backward slashes

		if (System.IO.Directory.Exists(macPath)) // if path requested is a folder, automatically open insides of that folder
		{
			openInsidesOfFolder = true;
		}

		if (!macPath.StartsWith("\""))
		{
			macPath = "\"" + macPath;
		}

		if (!macPath.EndsWith("\""))
		{
			macPath = macPath + "\"";
		}

		string arguments = (openInsidesOfFolder ? "" : "-R ") + macPath;

		try
		{
			System.Diagnostics.Process.Start("open", arguments);
		}
		catch (System.ComponentModel.Win32Exception e)
		{
			// tried to open mac finder in windows
			// just silently skip error
			// we currently have no platform define for the current OS we are in, so we resort to this
			e.HelpLink = ""; // do anything with this variable to silence warning about not using it
		}
	}

	public static void OpenInWin(string path)
	{
		bool openInsidesOfFolder = false;

		// try windows
		string winPath = path.Replace("/", "\\"); // windows explorer doesn't like forward slashes

		if (System.IO.Directory.Exists(winPath)) // if path requested is a folder, automatically open insides of that folder
		{
			openInsidesOfFolder = true;
		}

		try
		{
			System.Diagnostics.Process.Start("explorer.exe", (openInsidesOfFolder ? "/root," : "/select,") + winPath);
		}
		catch (System.ComponentModel.Win32Exception e)
		{
			// tried to open win explorer in mac
			// just silently skip error
			// we currently have no platform define for the current OS we are in, so we resort to this
			e.HelpLink = ""; // do anything with this variable to silence warning about not using it
		}
	}

	public static void Open(string path)
	{
		if (IsInWinOS)
		{
			OpenInWin(path);
		}
		else if (IsInMacOS)
		{
			OpenInMac(path);
		}
		else // couldn't determine OS
		{
			OpenInWin(path);
			OpenInMac(path);
		}
	}
}