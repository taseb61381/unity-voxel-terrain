using System;

namespace TerrainEngine
{
    /// <summary>
    /// Author: Roy Triesscheijn (http://www.royalexander.wordpress.com)
    /// Class defining BreadCrumbs used in path finding to mark our routes
    /// </summary>
    public class BreadCrumb : IComparable<BreadCrumb>
    {
        public VectorI3 position;
        public BreadCrumb next;
        public int cost = Int32.MaxValue;
        public bool onClosedList = false;
        public bool onOpenList = false;

        public BreadCrumb(VectorI3 position)
        {
            this.position = position;
        }

        public BreadCrumb(VectorI3 position, BreadCrumb parent)
        {
            this.position = position;
            this.next = parent;
        }

        public void Clear()
        {
            next = null;
            cost = Int32.MaxValue;
            onClosedList = false;
            onOpenList = false;
        }

        //Overrides default Equals so we check on position instead of object memory location
        public override bool Equals(object obj)
        {
            return (obj is BreadCrumb) && ((BreadCrumb)obj).position == this.position;
        }

        //Faster Equals for if we know something is a BreadCrumb
        public bool Equals(BreadCrumb breadcrumb)
        {
            return breadcrumb.position == this.position;
        }

        public override int GetHashCode()
        {
            return position.GetHashCode();
        }

        #region IComparable<> interface
        public int CompareTo(BreadCrumb other)
        {
            return cost.CompareTo(other.cost);
        }
        #endregion
    }
}