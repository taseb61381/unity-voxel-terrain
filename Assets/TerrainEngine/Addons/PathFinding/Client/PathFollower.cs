using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

/// <summary>
/// This class is an example on how to use the pathfinding system
/// This system is not ready to use, and errors or bugs can occur
/// </summary>
public class PathFollower : MonoBehaviour 
{
    private TerrainObject TObject;
    private Transform CTransform;
    private string InternalName;
    public CharacterController Controller;

    public float Gravity = -5f;
    public PathTypes PathType = PathTypes.GROUND_PATH;
    public bool MultiThreading = true; // Path research is exec on another thread
    public bool DebugLog = false;
    public ActiveDisableContainer OnStartMoving;
    public ActiveDisableContainer OnTargetChange;
    public int VoxelHeight = 1; // Height of the object. It will check on Y axis is all voxels are empty so object can move

    public RandomInfo RandomSpeed;
    public float Speed;
    public bool IsMoving = false;
    public float TooFarTeleportDistance = 50f; // Distance from target before teleport
    public float DistanceForRecalculatePath = 4f; // If target distance is > at this value, a new path will be generated
    public bool CanMoveWhenSearching = true; // When path is searching, true is this object can continue to move to current target or wait until to find another path
    public int GeneratingMaxMSTime = 2; // Time in MS before wait next frame to continue to search path
    public RandomInfo RandomScale; // Scale of this object

    private Stack<Vector3> PathPoints = new Stack<Vector3>();
    public List<Vector3> Points = new List<Vector3>();
    private Vector3 CurrentPath = new Vector3();
    private PathInformations PathInfo;
    private System.Diagnostics.Stopwatch Watch = new System.Diagnostics.Stopwatch();
    private Vector3 LastTargetPosition;
    public List<TargetPriority> Targets = new List<TargetPriority>();
    private TargetPriority CurrentTarget;

    private bool TeleportToTarget = false;
    private ExecuteAction ThreadAction;

    void Start()
    {
        if (Controller == null)
            Controller = GetComponent<CharacterController>();

        if (CTransform == null)
            CTransform = transform;

        if (TObject == null)
            TObject = GetComponent<TerrainObject>();

        if (TObject == null)
            TObject = gameObject.AddComponent<TerrainObject>();

        TerrainObjectColliderViewer.Add(TObject); // Add this object as TerrainCollider
        PathInfo = new PathInformations();
        PathInfo.OnPathEnd += OnPathDone;
        PathInfo.OnPathStartSearching += OnPathSearch;

        InternalName = name;
        RandomSpeed.Init();
        RandomScale.Init();
        SFastRandom Rn = new SFastRandom(UnityEngine.Random.Range(0,9999));
        Speed = RandomSpeed.RandomF(ref Rn);
        float Scale = RandomScale.RandomF(ref Rn);
        CTransform.localScale = new Vector3(Scale,Scale,Scale);

        IsMoving = false;

        if (MultiThreading)
        {
            ThreadAction = new ExecuteAction("PathFinding", UpdateGeneratingPath);
        }
        else
            PathInfo.HasTime += HasGeneratingTime;
    }

    void FixedUpdate()
    {
        UpdateMovements();
    }

    void Update()
    {
        if (TObject == null || TObject.BlockObject == null)
            return;

        UpdateTargets();

        UpdateCollisions();

        if (!MultiThreading)
            UpdateGeneratingPath(ActionProcessor.Unity);
    }

    void OnDisable()
    {
        if (PathInfo != null)
            PathInfo.Stop();
    }

    #region MovementSystem

    public float DistanceForNextPath = 2f;  // Distance from current Path for change to next

    public void StartMovement()
    {
        if (!IsMoving)
        {
            if (DebugLog)
                Debug.Log(InternalName + "Start Movement");

            IsMoving = true;
            SetNextPath();
        }
    }

    public void EndMovement()
    {
        if (DebugLog)
            Debug.Log(InternalName + "End Movement");

        IsMoving = false;
        PathPoints.Clear();
        Points.Clear();
        CurrentPath = new Vector3();
        ClearStuck();
    }

    public void UpdateMovements()
    {
        if (Controller == null )
            return;

        if (IsMoving && OnStartMoving != null)
            OnStartMoving.Call();

        Vector3 Velocity = new Vector3(0, Gravity * Time.deltaTime, 0);
        Vector3 TargetLook;
        if (IsMoving)
        {
            Velocity += (CurrentPath - CTransform.position).normalized * Time.deltaTime * Speed;

            TargetLook = CurrentPath - CTransform.position;

            // Look At Next Path
            if (TargetLook != Vector3.zero)
            {
                Quaternion rotation = Quaternion.LookRotation(TargetLook);
                TargetLook = Quaternion.Slerp(CTransform.rotation, rotation, Time.deltaTime * 6f).eulerAngles;
                TargetLook.x = TargetLook.z = 0;
                CTransform.eulerAngles = TargetLook;
            }
        }
        else
        {
            // Look At Target
            if (CurrentTarget != null && CurrentTarget.IsValid())
            {
                TargetLook = CurrentTarget.TObject.CTransform.position - CTransform.position;
                if (TargetLook != Vector3.zero)
                {
                    Quaternion rotation = Quaternion.LookRotation(CurrentTarget.TObject.CTransform.position - CTransform.position);
                    CTransform.rotation = Quaternion.Slerp(CTransform.rotation, rotation, Time.deltaTime * 6f);
                }
            }
        }

        Controller.Move(Velocity);

        //CTransform.rotation = Quaternion.Euler(0, CTransform.rotation.eulerAngles.y, 0);

        // Is Distance from Target is <MaxDistane, Movement End
        if (IsMoving && CurrentTarget != null &&  CurrentTarget.TObject != null)
        {
            if (PathPoints.Count <= 1 && Vector3.Distance(CTransform.position, CurrentTarget.TObject.Position) < CurrentTarget.DistanceFromTarget)
            {
                EndMovement();
                if (CurrentTarget.OnPathEnd != null)
                    CurrentTarget.OnPathEnd.Call();
            }

            if (Vector3.Distance(CTransform.position, CurrentPath) <= DistanceForNextPath)
            {
                SetNextPath(false);
            }
        }

        if (TeleportToTarget)
        {
            if (DebugLog)
                Debug.Log(InternalName + " TeleportToTarget : Teleporting");

            TeleportToTarget = false;
            CTransform.position = CurrentTarget.TObject.Position;
        }
    }

    #endregion

    #region CollisionsSystem

    public float CollisionTimeInterval = 1f;
    public Vector3 LastCollisionPosition;
    public float LastCollisionCheckTime;

    public void UpdateCollisions()
    {
        if(!IsMoving)
            return;

        if (ThreadManager.UnityTime - LastCollisionCheckTime > CollisionTimeInterval)
        {
            if (Vector3.Distance(LastCollisionPosition, TObject.Position) < 0.2f)
            {
                if (DebugLog)
                    Debug.Log(InternalName + " Stuck : Teleporting");

                SetNextPath(true);
            }
            else
                ClearStuck();
        }
    }

    public void ClearStuck()
    {
        LastCollisionCheckTime = ThreadManager.UnityTime;
        LastCollisionPosition = TObject.Position;
    }

    #endregion

    #region PathSystem

    public void SetNextPath(bool Teleport=false)
    {
        if (PathPoints.Count == 0)
        {
            EndMovement();
            if (CurrentTarget.OnPathEnd != null)
                CurrentTarget.OnPathEnd.Call();
        }
        else
        {
            CurrentPath = PathPoints.Pop();

            if (DebugLog)
                Debug.Log(InternalName + "Go To Next Path : " + CurrentPath);

            if (Teleport)
                CTransform.position = CurrentPath * TerrainManager.Instance.Informations.Scale;

            if (!PathInfo.Template.IsValid(PathInfo, (int)(CurrentPath.x - PathInfo.Terrain.WorldVoxel.x), (int)(CurrentPath.y - PathInfo.Terrain.WorldVoxel.y), (int)(CurrentPath.z - PathInfo.Terrain.WorldVoxel.z), 0, 0, 0, VoxelHeight))
            {
                if(DebugLog)
                    Debug.Log(InternalName + "Terrain Modified : Recalculating...");
                PathInfo.Stop();
                GenerateNewPaths();
            }

            CurrentPath *= TerrainManager.Instance.Informations.Scale;
            ClearStuck();
        }
    }

    public bool IsGeneratingPath()
    {
        return PathInfo.IsSearching();
    }

    public void GenerateNewPaths()
    {
        if(CurrentTarget == null)
            EndMovement();
        else
        {
            if (DebugLog)
                Debug.Log(InternalName + "Generating new path");

            if (!IsGeneratingPath())
            {                
                LastTargetPosition = CurrentTarget.TObject.Position;
                PathInfo.Init(TObject.BlockObject, TObject.WorldVoxel, CurrentTarget.TObject.WorldVoxel, PathType, VoxelHeight);

                if (ThreadAction != null)
                    ActionProcessor.AddToThread(ThreadAction);
                else
                    PathInfo.Find();
            }
            else
            {
                PathInfo.Stop();
                
                if (DebugLog)
                    Debug.Log(InternalName + "Path is Already Generating. Waiting end...");
            }
        }
    }

    public bool HasGeneratingTime()
    {
        if (GeneratingMaxMSTime == 0)
            return true;

        return Watch.ElapsedMilliseconds < GeneratingMaxMSTime;
    }

    public bool UpdateGeneratingPath(ActionThread Thread)
    {
        PathInfo.HasTime = Thread.HasTime;
        if (PathInfo.IsSearching())
        {
            if (DebugLog)
                Debug.Log(InternalName + "UpdateGeneratingPath...");

            PathInfo.Find();
        }

        if (PathInfo.IsSearching())
            return false;
        else
        {
            return true;
        }
    }

    public void OnPathSearch(PathInformations PathInfo)
    {
        if (DebugLog)
            Debug.Log(InternalName + "Searching...");

        Watch.Stop();
        Watch.Reset();
        Watch.Start();
    }

    public void OnPathStart(PathInformations PathInfo)
    {
        if (DebugLog)
            Debug.Log(InternalName + "Starting Generting");

        Watch.Stop();
        Watch.Reset();
        Watch.Start();
    }

    public void OnPathDone(PathInformations PathInfo)
    {
        if (DebugLog)
            Debug.Log(InternalName + "Path Done :" + PathInfo.CurrentResult);

        if (PathInfo.IsSearching())
        {
            if (!CanMoveWhenSearching)
                EndMovement();
            return;
        }

        EndMovement();

        if(PathInfo.CurrentResult == PathResults.OK)
        {
            BreadCrumb Path = PathInfo.Path;
            while (Path != null && Path.next != null)
            {
                Points.Add(Path.position + PathInfo.Terrain.WorldVoxel);
                PathPoints.Push(Path.position + PathInfo.Terrain.WorldVoxel);
                Path = Path.next;
            }

            if (DebugLog)
                Debug.Log(InternalName + "Path Points :" + PathPoints.Count);

            StartMovement();
        }
        else if (PathInfo.CurrentResult == PathResults.INVALID && !PathInfo.InvalidStartOrEnd && Vector3.Distance(CurrentTarget.TObject.Position, TObject.Position) > TooFarTeleportDistance)
        {
            TeleportToTarget = true;
        }
    }

    #endregion

    #region TargetSystem

    [Serializable]
    public class TargetPriority
    {
        public float Priority = 0f;
        public TerrainObject TObject;
        public float DistanceFromTarget = 4f;
        public ActiveDisableContainer OnPathEnd;

        public TargetPriority()
        {

        }

        public TargetPriority(TerrainObject TObject, float DistanceFromTarget)
        {
            this.TObject = TObject;
            this.DistanceFromTarget = DistanceFromTarget;
        }

        public bool IsValid()
        {
            return TObject != null && TObject.HasBlock && TObject.CTransform != null;
        }
    }

    /// <summary>
    /// Get the next target to follow , depending on the priority
    /// </summary>
    /// <returns></returns>
    public TargetPriority GetNextTarget()
    {
        TargetPriority Target = null;
        TargetPriority T = null;
        for(int i=0;i<Targets.Count;++i)
        {
            if ((T = Targets[i]).TObject != null && (!T.TObject.HasChunk || T.TObject.Position == Vector3.zero))
                continue;

            if(Target == null || Target.Priority < T.Priority)
            {
                Target = T;
            }
        }
        return Target;
    }

    public void UpdateTargets()
    {
        TargetPriority Target = GetNextTarget();
        if (Target != null && (Target.TObject == null || !Target.TObject.HasBlock))
            SetTarget(null);
        else
            SetTarget(Target);
    }

    /// <summary>
    /// Set a new target to follow. ArrayID is the position of this object on the targets list
    /// </summary>
    public TargetPriority SetTarget(TerrainObject TObject, float DistanceFromObject, int ArrayID)
    {
        TargetPriority Target;
        if (ArrayID < Targets.Count)
        {
            Target = Targets[ArrayID];
            Target.TObject = TObject;
            Target.DistanceFromTarget = DistanceFromObject;
        }
        else
        {
            Target = new TargetPriority(TObject, DistanceFromObject);
            Targets.Add(Target);
        }

        SetTarget(Target);
        return Target;
    }

    /// <summary>
    /// Set the current target to follow. This function don"t modify the targets list
    /// </summary>
    public void SetTarget(TargetPriority Target)
    {
        if (CurrentTarget != Target)
        {
            if (DebugLog && Target != null)
                Debug.Log(InternalName + Target + " : Aquiring Target :" + Target.TObject.Position);

            CurrentTarget = Target;
            GenerateNewPaths();
        }
        else
        {
            if (CurrentTarget != null && CurrentTarget.TObject != null && Vector3.Distance(LastTargetPosition, CurrentTarget.TObject.Position) > DistanceForRecalculatePath)
            {
                GenerateNewPaths();
            }
        }
    }

    #endregion
}
