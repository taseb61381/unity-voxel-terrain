using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class PathFinderExample : MonoBehaviour 
{
    public PathInformations Info;
    public TerrainObject StartObject;
    public TerrainObject EndObject;
    public PathTypes PathType = PathTypes.GROUND_PATH;

    public bool Find = false;
    public bool DrawDebugSpheres = true;
    public int MaxMsTime = 5;
    public int ElapsedTime = 0;
    private System.Diagnostics.Stopwatch Watch = new System.Diagnostics.Stopwatch();

    private List<GameObject> TempObjects = new List<GameObject>();
    private Queue<GameObject> ClosedObjects = new Queue<GameObject>();

    void Awake()
    {
        Info = new PathInformations();
        Info.HasTime += HasTime;
        Info.OnPathStartSearching += OnPathStartSearching;
        Info.OnPathEnd += OnPathEnd;
    }

    void Update()
    {
        if (Info.IsSearching())
            Info.Find();

        if (Info.IsDone() && Find)
        {
            Find = false;

            if (StartObject == null || EndObject == null)
            {
                Debug.LogError("Invalid TerrainObjects");
                return;
            }

            if (StartObject.HasBlock == false || EndObject.HasBlock == false)
                return;

            ElapsedTime = 0;
            Info.Init(StartObject.BlockObject, StartObject.WorldVoxel, EndObject.WorldVoxel, PathType, 1);
            Info.Find();
        }
    }

    public bool HasTime()
    {
        if (MaxMsTime == 0)
            return true;

        return Watch.ElapsedMilliseconds < MaxMsTime;
    }

    public void OnPathStartSearching(PathInformations Info)
    {
        Watch.Stop();
        Watch.Reset();
        Watch.Start();
    }

    public void OnPathEnd(PathInformations Info)
    {
        ElapsedTime += (int)Watch.ElapsedMilliseconds;
        Watch.Stop();

        if (Info.IsSearching()) // The search is not done, HasTime returned false, so Info wait another Info.Find() to continue
            return;

        if (Info.CurrentResult == PathResults.INVALID) Debug.LogError("Path not found");
        else
        {
            Debug.Log("Path Found");

            if (DrawDebugSpheres)
            {
                {
                    foreach (GameObject Obj in TempObjects)
                    {
                        Obj.SetActive(false);
                        ClosedObjects.Enqueue(Obj);
                    }

                    TempObjects.Clear();
                }
                {
                    int Count = 0;
                    GameObject Obj = null;
                    while (Info.Path != null && Info.Path.next != null) // Loop over all voxels to use from A to B. You can add them to a list and send to your IA (Info.Path.position)
                    {
                        ++Count;
                        if (ClosedObjects.Count == 0)
                        {
                            Obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                            GameObject.Destroy(Obj.GetComponent<SphereCollider>());
                            Obj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        }
                        else
                        {
                            Obj = ClosedObjects.Dequeue();
                            Obj.SetActive(true);
                        }

                        Obj.transform.position = (Info.Path.position + Info.Terrain.WorldVoxel) * TerrainManager.Instance.Informations.Scale;
                        TempObjects.Add(Obj);
                        Info.Path = Info.Path.next;
                    }

                    Debug.Log("Number of points : " + Count);
                }
            }
        }
    }
}
