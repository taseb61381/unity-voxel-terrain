using UnityEngine;

namespace TerrainEngine
{
    public enum PathResults
    {
        OK = 0,
        INVALID = 1,
        SEARCHING = 2,
        INITING = 4,
        STOP = 8,
    };

    public enum PathTypes
    {
        GROUND_PATH = 0,
        FLUID_PATH = 1,
        FLYING_PATH = 2,
    };

    /// <summary>
    /// PathTemplate contains function for determine if a point is usable or no when PathFinder is searching.
    /// </summary>
    public abstract class PathTemplate
    {
        public VectorI3[] Surrounding;

        public PathTemplate(VectorI3[] Surrounding)
        {
            this.Surrounding = Surrounding;
        }

        public virtual bool IsValid(PathInformations Info, int px, int py, int pz, int Dx, int Dy, int Dz, int VoxelHeight)
        {
            return false;
        }

        public virtual bool Init(PathInformations Info)
        {
            return false;
        }
    }

    /// <summary>
    /// PathTemplate for ground path. Return a point as usable only if it has volume and if points around are not empty
    /// </summary>
    public class PathTemplateGround : PathTemplate
    {
        static public float MinVolumeForPath = 0.5f;

        public PathTemplateGround()
            : base(
                new VectorI3[]{                        
            new VectorI3(-1,1,1), new VectorI3(0,1,1), new VectorI3(1,1,1),
            new VectorI3(-1,1,0), new VectorI3(1,1,0),
            new VectorI3(-1,1,-1), new VectorI3(0,1,-1), new VectorI3(1,1,-1),

            new VectorI3(-1,0,1), new VectorI3(0,0,1), new VectorI3(1,0,1),
            new VectorI3(-1,0,0), new VectorI3(1,0,0),
            new VectorI3(-1,0,-1), new VectorI3(0,0,-1), new VectorI3(1,0,-1),

            new VectorI3(-1,-1,1), new VectorI3(0,-1,1),new VectorI3(1,-1,1),
            new VectorI3(-1,-1,0), new VectorI3(1,-1,0),
            new VectorI3(-1,-1,-1), new VectorI3(0,-1,-1), new VectorI3(1,-1,-1) 
        })
        {
            
        }

        public override bool Init(PathInformations Info)
        {
            // Searching Start Ground
            bool Found = false;
            bool Top = false;
            bool Ground = false;

            int y;
            for (y = 0; y < 3; ++y)
            {
                Top = Info.Terrain.HasVolume(Info.Start.x, Info.Start.y, Info.Start.z, MinVolumeForPath);
                Ground = Info.Terrain.HasVolume(Info.Start.x, Info.Start.y - 1, Info.Start.z, MinVolumeForPath);
                if (!Top && Ground)
                {
                    Found = true;
                    break;
                }

                if (!Top && !Ground)
                {
                    --Info.Start.y;
                }

                if (Top && Ground)
                {
                    ++Info.Start.y;
                }

                if (Top && !Ground)
                {
                    ++Info.Start.y;
                }
            }

            int i = 0;

            if (!Found && (Info.Terrain.HasVolume(Info.Start.x, Info.Start.y, Info.Start.z, MinVolumeForPath) || !Info.Terrain.HasVolume(Info.Start.x, Info.Start.y - 1, Info.Start.z, MinVolumeForPath)))
            {
                //Debug.Log("Start Volume Invalid " + Info.Start + ":" + Info.Terrain.GetFloat(Info.Start) + "," + Info.Terrain.GetFloat(Info.Start.x, Info.Start.y - 1, Info.Start.z));
                return false;
            }

            // Searching End Ground
            for (i = 0; i < 3; ++i)
            {
                if (Info.Terrain.HasVolume(Info.End.x, Info.End.y, Info.End.z, MinVolumeForPath))
                {
                    ++Info.End.y;
                }
                else if (!Info.Terrain.HasVolume(Info.End.x, Info.End.y - 1, Info.End.z, MinVolumeForPath))
                {
                    --Info.End.y;
                }
                else
                    break;
            }

            if (Info.Terrain.HasVolume(Info.End.x, Info.End.y, Info.End.z, MinVolumeForPath) || !Info.Terrain.HasVolume(Info.End.x, Info.End.y - 1, Info.End.z, MinVolumeForPath))
            {
                //Debug.Log("End Volume Invalid " + Info.End + ":" + Info.Terrain.GetFloat(Info.End) + "," + Info.Terrain.GetFloat(Info.End.x, Info.End.y - 1, Info.End.z));
                return false;
            }

            return true;
        }

        // P = Voxel Position, D = Direction from last voxel.
        public override bool IsValid(PathInformations Info, int px, int py, int pz, int Dx, int Dy, int Dz, int VoxelHeight)
        {
            for (; VoxelHeight > 0; --VoxelHeight)
                if (Info.Terrain.HasVolume(px, py + VoxelHeight - 1, pz, MinVolumeForPath))
                    return false;

            return Info.Terrain.HasVolume(px, py - 1, pz, MinVolumeForPath, VoxelTypes.FLUID)
                && (Dz != 0 ? (Info.Terrain.HasVolume(px - 1, py - 1, pz, MinVolumeForPath) || Info.Terrain.HasVolume(px + 1, py - 1, pz, MinVolumeForPath)) : (Dx != 0 ? (Info.Terrain.HasVolume(px, py - 1, pz - 1, MinVolumeForPath) || Info.Terrain.HasVolume(px, py - 1, pz + 1, MinVolumeForPath)) : true));
        }
    }

    /// <summary>
    /// PathTemplate for Fluid path. Return a point as usable only if it contains fluid
    /// </summary>
    public class PathTemplateFluid : PathTemplate
    {
        static public float MinVolumeForPath = 0.5f;

        public PathTemplateFluid()
            : base(
                new VectorI3[]{                        
            new VectorI3(-1,1,1), new VectorI3(0,1,1), new VectorI3(1,1,1),
            new VectorI3(-1,1,0), new VectorI3(1,1,0),
            new VectorI3(-1,1,-1), new VectorI3(0,1,-1), new VectorI3(1,1,-1),

            new VectorI3(-1,0,1), new VectorI3(0,0,1), new VectorI3(1,0,1),
            new VectorI3(-1,0,0), new VectorI3(1,0,0),
            new VectorI3(-1,0,-1), new VectorI3(0,0,-1), new VectorI3(1,0,-1),

            new VectorI3(-1,-1,1), new VectorI3(0,-1,1),new VectorI3(1,-1,1),
            new VectorI3(-1,-1,0), new VectorI3(1,-1,0),
            new VectorI3(-1,-1,-1), new VectorI3(0,-1,-1), new VectorI3(1,-1,-1) 
        })
        {
            
        }

        public override bool Init(PathInformations Info)
        {
            int i = 0;
            bool Found = false;
            // Searching Start Ground
            for (i = 0; i < 3; ++i)
            {
                if (!Info.Terrain.HasFluid(Info.Start.x, Info.Start.y, Info.Start.z, MinVolumeForPath))
                {
                    ++Info.Start.y;
                }
                else
                {
                    Found = true;
                    break;
                }
            }

            if (!Found && !Info.Terrain.HasFluid(Info.Start.x, Info.Start.y, Info.Start.z, MinVolumeForPath))
            {
                //Debug.Log("Start Volume Invalid " + Info.Start + ":" + Info.Terrain.GetFloat(Info.Start) + "," + Info.Terrain.GetFloat(Info.Start.x, Info.Start.y - 1, Info.Start.z));
                return false;
            }

            Found = false;
            // Searching End Ground
            for (i = 0; i < 3; ++i)
            {
                if (!Info.Terrain.HasFluid(Info.End.x, Info.End.y, Info.End.z, MinVolumeForPath))
                {
                    ++Info.End.y;
                }
                else
                {
                    Found = true;
                    break;
                }
            }

            if (!Found && !Info.Terrain.HasFluid(Info.End.x, Info.End.y, Info.End.z, MinVolumeForPath))
            {
                //Debug.Log("End Volume Invalid " + Info.End + ":" + Info.Terrain.GetFloat(Info.End) + "," + Info.Terrain.GetFloat(Info.End.x, Info.End.y - 1, Info.End.z));
                return false;
            }

            return true;
        }

        // P = Voxel Position, D = Direction from last voxel.
        public override bool IsValid(PathInformations Info, int px, int py, int pz, int Dx, int Dy, int Dz, int VoxelHeight)
        {
            for (; VoxelHeight > 0; --VoxelHeight)
            {
                if (!Info.Terrain.HasFluid(px, py + VoxelHeight - 1, pz, MinVolumeForPath))
                    return false;
            }

            return true;
        }
    }

    public class PathTemplateFlying : PathTemplate
    {
        static public float MinVolumeForPath = 0.5f;

        public PathTemplateFlying()
            : base(
                new VectorI3[]{                        
            /*new VectorI3(-1,1,1), new VectorI3(0,1,1), new VectorI3(1,1,1),
            new VectorI3(-1,1,0), new VectorI3(1,1,0),
            new VectorI3(-1,1,-1), new VectorI3(0,1,-1), new VectorI3(1,1,-1),*/
            new VectorI3(0,1,0),
            new VectorI3(-1,0,1), new VectorI3(0,0,1), new VectorI3(1,0,1),
            new VectorI3(-1,0,0), new VectorI3(1,0,0),
            new VectorI3(-1,0,-1), new VectorI3(0,0,-1), new VectorI3(1,0,-1),
            new VectorI3(0,-1,0),
            /*new VectorI3(-1,-1,1), new VectorI3(0,-1,1),new VectorI3(1,-1,1),
            new VectorI3(-1,-1,0), new VectorI3(1,-1,0),
            new VectorI3(-1,-1,-1), new VectorI3(0,-1,-1), new VectorI3(1,-1,-1) */
        })
        {

        }

        public override bool Init(PathInformations Info)
        {
            int i = 0;
            bool Found = false;
            // Searching Start Ground
            for (i = 0; i < 2; ++i)
            {
                if (Info.Terrain.HasVolume(Info.Start.x, Info.Start.y, Info.Start.z, MinVolumeForPath))
                {
                    ++Info.Start.y;
                }
                else
                {
                    Found = true;
                    break;
                }
            }

            if (!Found)
            {
                Info.Start.y -= 3;
                for (i = 0; i < 2; ++i)
                {
                    if (Info.Terrain.HasVolume(Info.Start.x, Info.Start.y, Info.Start.z, MinVolumeForPath))
                        --Info.Start.y;
                    else
                    {
                        Found = true; break;
                    }
                }
            }

            if (!Found && Info.Terrain.HasVolume(Info.Start.x, Info.Start.y, Info.Start.z, MinVolumeForPath))
            {
                //Debug.Log("Start Volume Invalid " + Info.Start + ":" + Info.Terrain.GetFloat(Info.Start) + "," + Info.Terrain.GetFloat(Info.Start.x, Info.Start.y - 1, Info.Start.z));
                return false;
            }

            Found = false;
            // Searching End Ground
            for (i = 0; i < 2; ++i)
            {
                if (Info.Terrain.HasVolume(Info.End.x, Info.End.y, Info.End.z, MinVolumeForPath))
                {
                    ++Info.End.y;
                }
                else
                {
                    Found = true;
                    break;
                }
            }

            if (!Found)
            {
                Info.End.y -= 3;
                for (i = 0; i < 2; ++i)
                {
                    if (Info.Terrain.HasVolume(Info.End.x, Info.End.y, Info.End.z, MinVolumeForPath))
                        --Info.End.y;
                    else
                    {
                        Found = true; break;
                    }
                }
            }

            if (!Found && Info.Terrain.HasVolume(Info.End.x, Info.End.y, Info.End.z, MinVolumeForPath))
            {
                //Debug.Log("End Volume Invalid " + Info.End + ":" + Info.Terrain.GetFloat(Info.End) + "," + Info.Terrain.GetFloat(Info.End.x, Info.End.y - 1, Info.End.z));
                return false;
            }

            return true;
        }

        // P = Voxel Position, D = Direction from last voxel.
        public override bool IsValid(PathInformations Info, int px, int py, int pz, int Dx, int Dy, int Dz, int VoxelHeight)
        {
            for (; VoxelHeight > 0; --VoxelHeight)
            {
                if (Info.Terrain.HasVolume(px, py + VoxelHeight - 1, pz, MinVolumeForPath))
                    return false;
            }

            return true;
        }
    }

    public class PathInformations
    {
        static public PathTemplateGround GroundTemplate = new PathTemplateGround();
        static public PathTemplateFluid FluidTemplate = new PathTemplateFluid();
        static public PathTemplateFlying FlyingTemplate = new PathTemplateFlying();

        public delegate bool HasTimeDelegate();
        public delegate void OnPathEventDelegate(PathInformations Information);

        public HasTimeDelegate HasTime;                     // HasTime for continue to search or must wait next Find()
        public OnPathEventDelegate OnPathEnd;               // When Path research is done ( No more time, path found or not)
        public OnPathEventDelegate OnPathStart;             
        public OnPathEventDelegate OnPathStartSearching;
        public int VoxelHeight = 1;

        public TerrainBlock Terrain;
        public VectorI3 Start;
        public VectorI3 End;

        public MinHeap<BreadCrumb> openList;
        public FastDictionary<VectorI3, BreadCrumb> brWorld;
        public PathResults CurrentResult;
        public PathResults InitingResult;
        public BreadCrumb Path;
        public PathTemplate Template;                       // Template that will define witch type of Path will be generated. Fly, Ground, undeground Aquatic.
        public bool InvalidStartOrEnd = false;                      // True if path can not be found because start or end points are not valid

        private BreadCrumb node;
        private BreadCrumb current;
        private BreadCrumb finish;
        private int LocalI = 0;

        //private Queue<BreadCrumb> BreadPool = new Queue<BreadCrumb>();

        public bool IsDone()
        {
            return CurrentResult != PathResults.SEARCHING;
        }

        public void Stop()
        {
            CurrentResult = PathResults.STOP;
            InitingResult = PathResults.STOP;
        }

        /// <summary>
        /// Return true is this path is not finished
        /// </summary>
        /// <returns></returns>
        public bool IsSearching()
        {
            return CurrentResult == PathResults.SEARCHING || InitingResult == PathResults.INITING;
        }

        public void Init(TerrainBlock Terrain, VectorI3 WorldVoxelStart, VectorI3 WorldVoxelEnd, PathTypes PathType, int VoxelHeight)
        {
            PathTemplate Template = null;
            switch (PathType)
            {
                case PathTypes.GROUND_PATH:
                    Template = PathInformations.GroundTemplate;
                    break;
                case PathTypes.FLUID_PATH:
                    Template = PathInformations.FluidTemplate;
                    break;
                case PathTypes.FLYING_PATH:
                    Template = PathInformations.FlyingTemplate;
                    break;
            }

            Init(Terrain, WorldVoxelStart, WorldVoxelEnd, Template, VoxelHeight);
        }

        public void Init(TerrainBlock Terrain, VectorI3 WorldVoxelStart, VectorI3 WorldVoxelEnd, PathTemplate Template, int VoxelHeight)
        {
            this.Terrain = Terrain;
            if (Terrain == null)
            {
                InitingResult = PathResults.OK;
                Debug.LogError("TerrainPathFinder : Terrain == null");
                return;
            }

            if (VoxelHeight < 1)
            {
                VoxelHeight = 1;
            }

            //Debug.Log("Initing");

            this.VoxelHeight = VoxelHeight;
            this.Start = (WorldVoxelStart - Terrain.WorldVoxel);
            this.End = (WorldVoxelEnd - Terrain.WorldVoxel);
            this.CurrentResult = PathResults.OK;
            this.InitingResult = PathResults.INITING;
            this.Template = Template;
            if (Template == null)
                this.Template = GroundTemplate;

            if (openList == null)
            {
                openList = new MinHeap<BreadCrumb>(32);
                brWorld = new FastDictionary<VectorI3, BreadCrumb>(new VectorI3Comparer());
            }
            else
            {
                /*foreach (KeyValuePair<VectorI3, BreadCrumb> Kp in brWorld)
                {
                    Kp.Value.Clear();
                    BreadPool.Enqueue(Kp.Value);
                }*/

                openList.Clear();
                brWorld.Clear();
            }
        }

        public BreadCrumb CreateBread(VectorI3 Position)
        {
            /*BreadCrumb Bread;
            if (BreadPool.Count != 0)
            {
                Bread = BreadPool.Dequeue();
                Bread.position = Position;
                return Bread;
            }*/

            return new BreadCrumb(Position);
        }

        public PathResults Find()
        {
            InitingResult = PathResults.OK;

            if (Terrain == null)
            {
                CurrentResult = PathResults.INVALID;
                return PathResults.INVALID;
            }

            if (CurrentResult != PathResults.STOP)
                CurrentResult = InternalFind();
            else
            {
                CurrentResult = PathResults.INVALID;
            }

            if (OnPathEnd != null)
                OnPathEnd(this);

            return CurrentResult;
        }

        private PathResults InternalFind()
        {
            //Debug.LogError("PathStart");

            if (CurrentResult == PathResults.OK)
            {
                if (OnPathStart != null)
                    OnPathStart(this);

                if (!Template.Init(this))
                {
                    InvalidStartOrEnd = true;
                    return PathResults.INVALID;
                }

                InvalidStartOrEnd = false;
                if (current == null)
                {
                    current = new BreadCrumb(Start);
                    finish = new BreadCrumb(End);
                }
                else
                {
                    current.Clear();
                    finish.Clear();

                    current.position = Start;
                    finish.position = End;
                }
                current.cost = 0;

                brWorld[current.position] = current;
                openList.Add(current);
                LocalI = 0;
            }

            //Debug.Log("OnPathStartSearching " + openList.Count + "," + brWorld.Count);
            if (OnPathStartSearching != null)
                OnPathStartSearching(this);

            lock (openList)
            {
                int TempI = LocalI;
                int diff = 0, cost = 0;
                VectorI3 tmpAround, tmp;
                int Count = Template.Surrounding.Length;
                //Debug.Log(openList.Count + "," + brWorld.Count);
                if (openList.Count > 9999)
                    return PathResults.INVALID;

                while (openList.Count > 0 && ActionProcessor.Instance.IsRunning)
                {
                    //Find best item and switch it to the 'closedList'
                    if (TempI == 0)
                    {
                        current = openList.ExtractFirst();
                        if (current == null)
                        {
                            CurrentResult = PathResults.INVALID;
                            return CurrentResult;
                        }

                        current.onClosedList = true;
                    }

                    //Find neighbours
                    for (; TempI < Count && ActionProcessor.Instance.IsRunning; ++TempI)
                    {
                        tmpAround = Template.Surrounding[TempI];
                        tmp.x = current.position.x + tmpAround.x;
                        tmp.y = current.position.y + tmpAround.y;
                        tmp.z = current.position.z + tmpAround.z;

                        if (CurrentResult == PathResults.STOP)
                            return CurrentResult;

                        //if (Arounds[tmpAround.x + 1][tmpAround.y + 1][tmpAround.z + 1] && !Arounds[tmpAround.x + 1][tmpAround.y + 2][tmpAround.z + 1])
                        if (Template.IsValid(this, tmp.x, tmp.y, tmp.z, tmpAround.x, tmpAround.y, tmpAround.z, VoxelHeight))
                        {
                            if (!brWorld.TryGetValue(tmp, out node))
                            {
                                node = CreateBread(tmp);
                                brWorld.Add(tmp, node);
                            }

                            //If the node is not on the 'closedList' check it's new score, keep the best
                            if (!node.onClosedList)
                            {
                                diff = 0;
                                if (current.position.x != node.position.x)
                                {
                                    diff += 1;
                                }
                                if (current.position.y != node.position.y)
                                {
                                    diff += 1;
                                }
                                if (current.position.z != node.position.z)
                                {
                                    diff += 1;
                                }
                                cost = current.cost + diff + node.position.GetDistanceSquared(End);

                                if (cost < node.cost)
                                {
                                    node.cost = cost;
                                    node.next = current;
                                }

                                //If the node wasn't on the openList yet, add it 
                                if (!node.onOpenList)
                                {
                                    //Check to see if we're done
                                    if (node.position == finish.position)
                                    {
                                        node.next = current;
                                        Path = node;
                                        return PathResults.OK;
                                    }

                                    node.onOpenList = true;
                                    openList.Add(node);
                                }
                            }
                        }

                        if (CurrentResult == PathResults.STOP)
                        {
                            //Debug.Log("Stop");
                            return PathResults.INVALID;
                        }

                        if (HasTime != null && !HasTime())
                        {
                            //Debug.Log("No Time");
                            LocalI = TempI;
                            return PathResults.SEARCHING;
                        }
                    }

                    TempI = 0;
                }
            }

            return PathResults.INVALID;
        }
    }
}