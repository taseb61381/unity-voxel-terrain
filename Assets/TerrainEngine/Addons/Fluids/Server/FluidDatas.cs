﻿using System;
using System.IO;

namespace TerrainEngine
{
    public struct FluidMeshData
    {
        public int ChunkId;
        public byte FluidIndex;
        public FluidStates State;
        public int MeshIndex;

        public FluidMeshData(int ChunkId, byte FluidIndex, bool Dirty)
        {
            this.ChunkId = ChunkId;
            this.FluidIndex = FluidIndex;

            if (Dirty)
                this.State = FluidStates.Dirty;
            else
                this.State = FluidStates.None;

            MeshIndex = -1;
        }

        public bool Dirty
        {
            get
            {
                return (State & FluidStates.Dirty) == FluidStates.Dirty;
            }
            set
            {
                if (value)
                    State |= FluidStates.Dirty;
                else
                    State &= ~FluidStates.Dirty;
            }
        }
        public bool HasMesh
        {
            get
            {
                return (State & FluidStates.HasMesh) == FluidStates.HasMesh;
            }
            set
            {
                if (value)
                    State |= FluidStates.HasMesh;
                else
                    State &= ~FluidStates.HasMesh;
            }
        }
        public bool Combined
        {
            get
            {
                return (State & FluidStates.Combined) == FluidStates.Combined;
            }
            set
            {
                if (value)
                {
                    State |= FluidStates.Combined;
                }
                else
                    State &= ~FluidStates.Combined;
            }
        }
    }

    [Flags]
    public enum FluidDirections : byte
    {
        NONE = 0,
        LEFT = 1,
        RIGHT = 2,
        FORWARD = 4,
        BACKWARD = 8,
        BOTTOM = 16,
        MAX = 32,
    };

    public struct FluidBlock
    {
        public byte Volume;
        public byte Direction;

        public void Init(byte Volume, byte Direction)
        {
            this.Volume = Volume;
            this.Direction = Direction;
        }
    }

    public class FluidData : IPoolable
    {
        static public int MaxVolume = 250;
        static public int RiverVolume = 251;

        public FluidBlock[] Blocks;

        public void Init(int Size)
        {
            if(Blocks == null)
                Blocks = new FluidBlock[Size];

            for (int i = Size-1; i >= 0; --i)
                Blocks[i] = new FluidBlock();
        }

        public override void Clear()
        {

        }

        public override void Delete()
        {
            Blocks = null;
        }
    }

    [Flags]
    public enum FluidStates : byte
    {
        None = 0,
        Dirty = 1,
        MeshDirty = 2,
        HasMesh = 4,
        MustReload = 8,
        Combined = 16,
    };

    public struct FluidSate
    {
        public FluidStates State;

        public bool Dirty
        {
            get
            {
                return (State & FluidStates.Dirty) == FluidStates.Dirty;
            }
            set
            {
                if (value)
                    State |= FluidStates.Dirty;
                else
                    State &= ~FluidStates.Dirty;
            }
        }
        public bool HasMesh
        {
            get
            {
                return (State & FluidStates.HasMesh) == FluidStates.HasMesh;
            }
            set
            {
                if (value)
                    State |= FluidStates.HasMesh;
                else
                    State &= ~FluidStates.HasMesh;
            }
        }
        public bool MustReload
        {
            get
            {
                return (State & FluidStates.MustReload) == FluidStates.MustReload;
            }
            set
            {
                if (value)
                {
                    State |= FluidStates.MustReload;
                    Dirty = true;
                }
                else
                    State &= ~FluidStates.MustReload;
            }
        }
    }

    public class FluidDatas : ICustomTerrainData
    {
        static public PoolInfoRef PoolContainer;

        public FluidData[] Datas; // Fluid volume and direction for each chunk
        public FluidSate[] States; // Fluid state for each chunk
        public IMeshData[] CombinedMesh; // Combined Mesh from all chunks
        public SList<FluidMeshData> FluidMesh; // Contains Mesh/States for each chunk fluid
        public SList<IMeshData> FluidMeshData; // Contains Chunk Mesh

        public int VoxelCount;
        public int GenerateCount = 0;
        public byte ThreadStep = 0; // 0 = Free, 1 = Flow Chunk, 2 = Generating Chunk
        public int ChunkId = 0; // ChunkId Generating
        public bool IsModified = false;

        public int GetMeshIndex(int ChunkId, byte FluidIndex)
        {
            for (int i = FluidMesh.Count - 1; i >= 0; --i)
            {
                if (FluidMesh.array[i].ChunkId == ChunkId && FluidMesh.array[i].FluidIndex == FluidIndex)
                    return i;
            }

            return -1;
        }

        public int GetMeshDataFreeSlot(IMeshData Data)
        {
            for (int i = FluidMeshData.Count - 1; i >= 0; --i)
            {
                if (FluidMeshData.array[i] == null)
                {
                    FluidMeshData.array[i] = Data;
                    return i;
                }
            }

            if(FluidMeshData.Count == 0)
                FluidMeshData = new SList<IMeshData>((Terrain.Information.ChunkPerBlock * Terrain.Information.ChunkPerBlock) / 3);

            FluidMeshData.Add(Data);
            return FluidMeshData.Count - 1;
        }

        public int GetChunkIndex(int ChunkId,byte FluidIndex)
        {
            for(int i=FluidMesh.Count-1;i>=0;--i)
            {
                if (FluidMesh.array[i].ChunkId == ChunkId && FluidMesh.array[i].FluidIndex == FluidIndex)
                    return i;
            }

            return -1;
        }

        public override void Init(TerrainDatas Terrain)
        {
            base.Init(Terrain);

            if (PoolContainer.Index == 0)
                PoolContainer = PoolManager.Pool.GetPool("FluidData");

            VoxelCount = Terrain.Information.TotalVoxelsPerChunk;
            UseFluidHeight = false;

            if (States == null)
            {
                States = new FluidSate[Terrain.Information.TotalChunksPerBlock];
                Datas = new FluidData[Terrain.Information.TotalChunksPerBlock];
            }

            // Mesh Combiner
            GenerateCount = 0;
            if (CombinedMesh != null)
            {
                for (int i = 0; i < CombinedMesh.Length; ++i)
                    CombinedMesh[i].VerticesCount = CombinedMesh[i].IndiceCount = 0;
            }
        }

        public override void Clear()
        {
            GenerateCount = 0;
            IsModified = false;
            ChunkId = 0;
            ThreadStep = 0;
            GenerateCount = 0;

            for (int i = Information.TotalChunksPerBlock - 1; i >= 0; --i)
            {
                if (Datas[i] != null)
                {
                    Datas[i].Close();
                    Datas[i] = null;
                }
                States[i].State = FluidStates.None;
            }

            for (int i = FluidMeshData.Count-1; i >= 0; --i)
            {
                if (FluidMeshData.array[i] != null)
                    FluidMeshData.array[i].Close();
            }

            CombinedMesh = null;
            FluidMeshData.Clear();
            FluidMesh.Clear();
            ClearCascades();
            Dirty = false;
        }

        #region FluidHeight

        public bool UseFluidHeight = false;
        public int FluidHeight = 0;

        public void SetFluidHeight(int Level)
        {
            UseFluidHeight = true;
            FluidHeight = Level;
        }

        #endregion

        #region Values

        public bool HasData(int ChunkId)
        {
            return Datas[ChunkId] != null || Terrain.WorldY <= FluidHeight;
        }

        public byte GetLevel(int x, int y, int z)
        {
            if (UseFluidHeight == false)
            {
                return 0;
            }

            x = (Terrain.WorldY + y) - FluidHeight;
            if (x == 0)
            {
                return 153;
            }
            else if (x < 0)
            {
                return (byte)FluidData.MaxVolume;
            }
            else
            {
                return 0;
            }
        }

        public void InitArray(int cv, int x, int y, int z)
        {
            Datas[cv] = PoolContainer.GetData<FluidData>();
            Datas[cv].Init(VoxelCount);

            if (!UseFluidHeight)
                return;

            int i = 0;
            int sx = (x / Information.VoxelPerChunk) * Information.VoxelPerChunk;
            int sy = (y / Information.VoxelPerChunk) * Information.VoxelPerChunk;
            int sz = (z / Information.VoxelPerChunk) * Information.VoxelPerChunk;

            int ex = sx + Information.VoxelPerChunk, ey = sy + Information.VoxelPerChunk, ez = sz + Information.VoxelPerChunk;
            int px, py, pz;
            for (px = sx; px < ex; ++px)
            {
                for (py = sy; py < ey; ++py)
                {
                    for (pz = sz; pz < ez; ++pz)
                    {
                        Datas[cv].Blocks[i].Volume = (byte)GetLevel(px, py, pz);
                        ++i;
                    }
                }
            }
        }

        public void SetVolumeAndDirection(int x, int y, int z,int ChunkId, int Volume, FluidDirections Direction)
        {
            if (Datas[ChunkId] == null)
            {
                if (Volume == 0)
                    return;

                InitArray(ChunkId, x, y, z);
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Dirty = true;
            States[ChunkId].Dirty = true;
            Datas[ChunkId].Blocks[x].Volume = (byte)Volume;
            Datas[ChunkId].Blocks[x].Direction = (byte)Direction;
        }

        public void GetVolumeAndDirection(int x, int y, int z, int ChunkId, int FlattenVoxel, ref FluidDirections Direction, ref int Volume)
        {
            if (Datas[ChunkId] == null)
            {
                Direction = FluidDirections.NONE;
                Volume = GetLevel(x, y, z);
                return;
            }

            Volume = Datas[ChunkId].Blocks[FlattenVoxel].Volume;
            Direction = (FluidDirections)Datas[ChunkId].Blocks[FlattenVoxel].Direction;
        }

        public override void Save(BinaryWriter Stream)
        {
            int j = 0;
            FluidData Data = null;
            byte Value = 0;
            byte LastValue = 0;
            byte LastCount = 0;
            for (int i = 0; i < Information.TotalChunksPerBlock; ++i)
            {
                Data = Datas[i];
                if (Data != null)
                {
                    Stream.Write(true);
                    LastValue = 0;
                    LastCount = 0;

                    // RLE Comrpession
                    for (j = 0; j < VoxelCount; ++j)
                    {
                        Value = Data.Blocks[j].Volume;

                        if (Value != LastValue || LastCount >= 255)
                        {
                            if (LastCount != 0)
                            {
                                Stream.Write(LastCount);
                                Stream.Write(LastValue);
                            }

                            LastCount = 1;
                            LastValue = Value;
                        }
                        else
                            ++LastCount;
                    }

                    if (LastCount != 0)
                    {
                        Stream.Write(LastCount);
                        Stream.Write(LastValue);
                    }
                }
                else
                    Stream.Write(false);
            }
        }

        public override void Read(BinaryReader Stream)
        {
            int i = 0;
            FluidData Data = null;
            byte Volume = 0;
            int Count = 0;
            int j = 0;
            for (int ChunkId = 0; ChunkId < Information.TotalChunksPerBlock; ++ChunkId)
            {
                if (Stream.ReadBoolean())
                {
                    Datas[ChunkId] = Data = PoolContainer.GetData<FluidData>();
                    Data.Init(VoxelCount);

                    States[ChunkId].Dirty = true;
                    Dirty = true;

                    for (i = 0; i < VoxelCount;)
                    {
                        Count = Stream.ReadByte();
                        Volume = Stream.ReadByte();
                        for (j = 0; j < Count; ++j)
                        {
                            Data.Blocks[i + j].Volume = Volume;
                        }
                        i += Count;
                    }
                }
            }
        }

        public override void GenerateGroundPoints(BiomeData Data, int ChunkX, int ChunkZ)
        {
            Fast2DArray<byte> TempDatas = Data.GetCustomData("TempDatas") as Fast2DArray<byte>;
            bool LastIsFull = false;

            int ChunkY = 0, ChunkId;
            int ChunkPerBlock = Information.ChunkPerBlock;
            int EndX = ChunkPerBlock, EndZ = ChunkPerBlock;

            int px, py, pz, f, x, y, z;
            int VoxelPerChunk = Information.VoxelPerChunk;

            byte LastType = 0;
            byte Type = 0;
            int Volume = 0;
            FluidDirections Direction = FluidDirections.LEFT;

            int StartX, StartZ;

            StartX = ChunkX * VoxelPerChunk;
            StartZ = ChunkZ * VoxelPerChunk;

            for (x = 0; x < VoxelPerChunk; ++x)
            {
                px = x + ChunkX * VoxelPerChunk;

                for (z = 0; z < VoxelPerChunk; ++z)
                {
                    pz = z + ChunkZ * VoxelPerChunk;

                    for (ChunkY = 0; ChunkY < ChunkPerBlock; ++ChunkY)
                    {
                        ChunkId = ChunkX * ChunkPerBlock * ChunkPerBlock + ChunkY * ChunkPerBlock + ChunkZ;

                        py = ChunkY * VoxelPerChunk;
                        f = x * VoxelPerChunk * VoxelPerChunk + z;


                        for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                        {
                            GetVolumeAndDirection(px, py, pz, ChunkId, f, ref Direction, ref Volume);
                            if (Volume > 0)
                            {
                                Type = Terrain.GetType(px, py, pz);
                            }
                            else
                            {
                                Type = 0;
                            }

                            AddPoint(Data, (byte)x, (ushort)py, (byte)z, Type, Volume*(128f/255f), ref LastIsFull, ref LastType, TempDatas);
                        }
                    }
                }
            }

            base.GenerateGroundPoints(Data, ChunkX, ChunkZ);
        }

        public void AddPoint(BiomeData Data, byte x, ushort y, byte z, byte Type, float Volume, ref bool LastIsFull, ref byte LastType, Fast2DArray<byte> TempDatas)
        {
            if (y == 0)
            {
                LastIsFull = Type != 0 && Volume >= TerrainInformation.Isolevel;
                LastType = Type;
                return;
            }

            //VoxelTypes VType = Server.Generator.Palette[Type].VoxelType;

            // If current voxel is full and last voxel is empty. Add TopGroud
            if (Type != 0 && Volume >= TerrainInformation.Isolevel)
            {
                LastIsFull = true;
            }
            else
            {
                if (LastIsFull)
                {
                    GroundPoint Point = new GroundPoint();
                    Point.x = (byte)(x);
                    Point.y = (ushort)(y - 1);
                    Point.z = (byte)(z);
                    Point.Type = LastType;
                    Point.Side = 2;
                    Data.GroundPoints.Add(Point);
                }

                LastIsFull = false;
            }

            if (y == Information.VoxelsPerAxis - 1 && TempDatas != null)
            {
                if (Type != 0 && Volume >= TerrainInformation.Isolevel)
                {
                    if (TempDatas.array[(x) * Information.VoxelPerChunk + (z)] == 0)
                    {
                        GroundPoint Point = new GroundPoint();
                        Point.x = (byte)(x);
                        Point.y = (ushort)y;
                        Point.z = (byte)(z);
                        Point.Type = LastType;
                        Point.Side = 2;
                        Data.GroundPoints.Add(Point);
                    }
                }
            }

            LastType = Type;
        }

        #endregion

        public void ClearCascades()
        {
            if (CascadePoints != null)
            {
                foreach (IPoolable Pool in CascadePoints)
                {
                    if (Pool != null)
                    {
                        Pool.Clear();
                    }
                }
            }
        }

        public IPoolable[] CascadePoints;
    }
}
