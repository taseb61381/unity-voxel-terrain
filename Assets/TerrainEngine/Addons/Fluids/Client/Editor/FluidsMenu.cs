﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class FluidsMenu 
{
    [MenuItem("TerrainEngine/Terrains/MarchingCubes/1-FluidProcessor")]
    static void CreateMarchingFluids()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        Debug.LogError("Creating FluidProcessor.");
        FluidProcessor Obj = GameObject.FindObjectOfType(typeof(FluidProcessor)) as FluidProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<FluidProcessor>();

        Obj.name = "1-FluidProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 1;

        if (Obj.GetComponent<FluidMarchingConstructor>() == null)
        {
            Debug.LogError("Creating FluidProcessor Mesh Constructor.");
            /*FluidMarchingConstructor Const = */
            Obj.gameObject.AddComponent<FluidMarchingConstructor>();
        }
    }


    [MenuItem("TerrainEngine/Terrains/SurfaceNets/1-FluidProcessor")]
    static void CreateSurfaceFluids()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        Debug.LogError("Creating FluidProcessor.");
        FluidProcessor Obj = GameObject.FindObjectOfType(typeof(FluidProcessor)) as FluidProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<FluidProcessor>();

        Obj.name = "1-FluidProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 1;

        if (Obj.GetComponent<FluidMarchingConstructor>() == null)
        {
            Debug.LogError("Creating FluidProcessor Mesh Constructor.");
            /*FluidMarchingConstructor Const = */
            Obj.gameObject.AddComponent<FluidMarchingConstructor>();
        }
    }

    [MenuItem("TerrainEngine/Terrains/Transvoxel/1-FluidProcessor")]
    static void CreateTransvoxelFluids()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

		Debug.Log("Creating FluidProcessor.");
        FluidProcessor Obj = GameObject.FindObjectOfType(typeof(FluidProcessor)) as FluidProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<FluidProcessor>();

        Obj.name = "1-FluidProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 1;

        if (Obj.GetComponent<FluidMarchingConstructor>() == null)
        {
            Debug.Log("Creating FluidProcessor Mesh Constructor.");
            /*FluidMarchingConstructor Const = */
            Obj.gameObject.AddComponent<FluidMarchingConstructor>();
        }
    }


    [MenuItem("TerrainEngine/Terrains/Hexagonal/1-FluidProcessor")]
    static void CreateHexagonalFluids()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        Debug.LogError("Creating FluidProcessor.");
        FluidProcessor Obj = GameObject.FindObjectOfType(typeof(FluidProcessor)) as FluidProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<FluidProcessor>();

        Obj.name = "1-FluidProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 1;

        if (Obj.GetComponent<FluidVoxelConstructor>() == null)
        {
            Debug.LogError("Creating FluidProcessor Mesh Constructor.");
            /*FluidVoxelConstructor Const =*/
            Obj.gameObject.AddComponent<FluidVoxelConstructor>();
        }
    }



    [MenuItem("TerrainEngine/Terrains/2D/1-FluidProcessor")]
    static void Create2DFluids()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        Debug.LogError("Creating FluidProcessor.");
        FluidProcessor Obj = GameObject.FindObjectOfType(typeof(FluidProcessor)) as FluidProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<FluidProcessor>();

        Obj.name = "1-FluidProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 1;

        if (Obj.GetComponent<FluidVoxelConstructor>() == null)
        {
            Debug.LogError("Creating FluidProcessor Mesh Constructor.");
            /*FluidVoxelConstructor Const = */
            Obj.gameObject.AddComponent<FluidVoxelConstructor>();
        }
    }

    [MenuItem("TerrainEngine/Terrains/Cubic/1-FluidProcessor")]
    static void CreateVoxelsFluids()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        Debug.LogError("Creating FluidProcessor.");
        FluidProcessor Obj = GameObject.FindObjectOfType(typeof(FluidProcessor)) as FluidProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<FluidProcessor>();

        Obj.name = "1-FluidProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 1;

        if (Obj.GetComponent<FluidVoxelConstructor>() == null)
        {
            Debug.LogError("Creating FluidProcessor Mesh Constructor.");
            /*FluidVoxelConstructor Const =*/
            Obj.gameObject.AddComponent<FluidVoxelConstructor>();
        }
    }
}
