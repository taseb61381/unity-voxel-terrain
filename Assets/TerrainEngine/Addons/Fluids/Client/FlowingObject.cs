﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class FlowingObject : MonoBehaviour
{
    public float density = 500;
    public int slicesPerAxis = 2;
    public bool isConcave = false;
    public int voxelsLimit = 16;
    public bool DestroyIfNoTerrain = true;

    private const float DAMPFER = 0.1f;
    private const float WATER_DENSITY = 1000;

    private float voxelHalfHeight;
    private Vector3 localArchimedesForce;
    private List<Vector3> voxels;
    private bool isMeshCollider;
    private Rigidbody CRigidBody;
    private Transform CTransform;

    public TerrainObject TObject;

    /// <summary>
    /// Provides initialization.
    /// </summary>
    private void Start()
    {
        // Store original rotation and position
        CTransform = transform;

        var originalRotation = CTransform.rotation;
        var originalPosition = CTransform.position;
        CTransform.rotation = Quaternion.identity;
        CTransform.position = Vector3.zero;

        // The object must have a collider
        if (GetComponent<Collider>() == null)
        {
            gameObject.AddComponent<MeshCollider>();
            Debug.LogWarning(string.Format("[Buoyancy.cs] Object \"{0}\" had no collider. MeshCollider has been added.", name));
        }
        isMeshCollider = GetComponent<MeshCollider>() != null;

        var bounds = GetComponent<Collider>().bounds;
        if (bounds.size.x < bounds.size.y)
        {
            voxelHalfHeight = bounds.size.x;
        }
        else
        {
            voxelHalfHeight = bounds.size.y;
        }
        if (bounds.size.z < voxelHalfHeight)
        {
            voxelHalfHeight = bounds.size.z;
        }
        voxelHalfHeight /= 2 * slicesPerAxis;

        CRigidBody = GetComponent<Rigidbody>();

        // The object must have a RidigBody
        if (CRigidBody == null)
            CRigidBody = gameObject.AddComponent<Rigidbody>();

        CRigidBody.centerOfMass = new Vector3(0, -bounds.extents.y * 0f, 0) + CTransform.InverseTransformPoint(bounds.center);

        voxels = SliceIntoVoxels(isMeshCollider && isConcave);

        // Restore original rotation and position
        CTransform.rotation = originalRotation;
        CTransform.position = originalPosition;

        float volume = GetComponent<Rigidbody>().mass / density;

        WeldPoints(voxels, voxelsLimit);

        float archimedesForceMagnitude = WATER_DENSITY * Mathf.Abs(Physics.gravity.y) * volume;
        localArchimedesForce = new Vector3(0, archimedesForceMagnitude, 0) / voxels.Count;

        if (TObject == null)
            TObject = GetComponent<TerrainObject>();

        if (TObject == null)
            TObject = gameObject.AddComponent<TerrainObject>();

        TerrainObjectColliderViewer.Add(TObject);
        TObject.UpdateCurrentVoxel = true;
    }

    /// <summary>
    /// Slices the object into number of voxels represented by their center points.
    /// <param name="concave">Whether the object have a concave shape.</param>
    /// <returns>List of voxels represented by their center points.</returns>
    /// </summary>
    private List<Vector3> SliceIntoVoxels(bool concave)
    {
        var points = new List<Vector3>(slicesPerAxis * slicesPerAxis * slicesPerAxis);

        if (concave)
        {
            var meshCol = GetComponent<MeshCollider>();

            var convexValue = meshCol.convex;
            meshCol.convex = false;

            // Concave slicing
            var bounds = GetComponent<Collider>().bounds;
            for (int ix = 0; ix < slicesPerAxis; ix++)
            {
                for (int iy = 0; iy < slicesPerAxis; iy++)
                {
                    for (int iz = 0; iz < slicesPerAxis; iz++)
                    {
                        float x = bounds.min.x + bounds.size.x / slicesPerAxis * (0.5f + ix);
                        float y = bounds.min.y + bounds.size.y / slicesPerAxis * (0.5f + iy);
                        float z = bounds.min.z + bounds.size.z / slicesPerAxis * (0.5f + iz);

                        var p = CTransform.InverseTransformPoint(new Vector3(x, y, z));

                        if (PointIsInsideMeshCollider(meshCol, p))
                        {
                            points.Add(p);
                        }
                    }
                }
            }
            if (points.Count == 0)
            {
                points.Add(bounds.center);
            }

            meshCol.convex = convexValue;
        }
        else
        {
            // Convex slicing
            var bounds = GetComponent<Collider>().bounds;
            for (int ix = 0; ix < slicesPerAxis; ix++)
            {
                for (int iy = 0; iy < slicesPerAxis; iy++)
                {
                    for (int iz = 0; iz < slicesPerAxis; iz++)
                    {
                        float x = bounds.min.x + bounds.size.x / slicesPerAxis * (0.5f + ix);
                        float y = bounds.min.y + bounds.size.y / slicesPerAxis * (0.5f + iy);
                        float z = bounds.min.z + bounds.size.z / slicesPerAxis * (0.5f + iz);

                        var p = CTransform.InverseTransformPoint(new Vector3(x, y, z));

                        points.Add(p);
                    }
                }
            }
        }

        return points;
    }

    /// <summary>
    /// Returns whether the point is inside the mesh collider.
    /// </summary>
    /// <param name="c">Mesh collider.</param>
    /// <param name="p">Point.</param>
    /// <returns>True - the point is inside the mesh collider. False - the point is outside of the mesh collider. </returns>
    private static bool PointIsInsideMeshCollider(Collider c, Vector3 p)
    {
        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        foreach (var ray in directions)
        {
            RaycastHit hit;
            if (c.Raycast(new Ray(p - ray * 1000, ray), out hit, 1000f) == false)
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Returns two closest points in the list.
    /// </summary>
    /// <param name="list">List of points.</param>
    /// <param name="firstIndex">Index of the first point in the list. It's always less than the second index.</param>
    /// <param name="secondIndex">Index of the second point in the list. It's always greater than the first index.</param>
    private static void FindClosestPoints(IList<Vector3> list, out int firstIndex, out int secondIndex)
    {
        float minDistance = float.MaxValue, maxDistance = float.MinValue;
        firstIndex = 0;
        secondIndex = 1;

        for (int i = 0; i < list.Count - 1; i++)
        {
            for (int j = i + 1; j < list.Count; j++)
            {
                float distance = Vector3.Distance(list[i], list[j]);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    firstIndex = i;
                    secondIndex = j;
                }
                if (distance > maxDistance)
                {
                    maxDistance = distance;
                }
            }
        }
    }

    /// <summary>
    /// Welds closest points.
    /// </summary>
    /// <param name="list">List of points.</param>
    /// <param name="targetCount">Target number of points in the list.</param>
    private static void WeldPoints(IList<Vector3> list, int targetCount)
    {
        if (list.Count <= 2 || targetCount < 2)
        {
            return;
        }

        while (list.Count > targetCount)
        {
            int first, second;
            FindClosestPoints(list, out first, out second);

            var mixed = (list[first] + list[second]) * 0.5f;
            list.RemoveAt(second); // the second index is always greater that the first => removing the second item first
            list.RemoveAt(first);
            list.Add(mixed);
        }
    }

    /// <summary>
    /// Returns the water level at given location.
    /// </summary>
    /// <param name="x">x-coordinate</param>
    /// <param name="z">z-coordinate</param>
    /// <returns>Water level</returns>
    private float GetWaterLevel(float x, float z)
    {
        if (TObject == null || !TObject.HasBlock || TObject.BlockObject == null)
            return 0.0f;

        VectorI3 Voxel = TObject.BlockObject.GetVoxelFromWorldPosition(new Vector3(x, 0, z), false);
        return TObject.BlockObject.GetLowerHeight(Voxel.x, int.MaxValue, Voxel.z, BlockManager.Water).y;
    }

    /// <summary>
    /// Calculates physics.
    /// </summary>
    private float k = 0f;
    private float waterLevel = 0f;
    private void FixedUpdate()
    {
        if (TObject == null)
            return;

        if (TerrainManager.Instance != null && !TObject.HasBlock)
        {
            if (DestroyIfNoTerrain)
                Destroy(gameObject);
            return;
        }

        if (TObject.CurrentVoxelType != BlockManager.Water)
            return;

        foreach (var point in voxels)
        {
            var wp = CTransform.TransformPoint(point);
            waterLevel = GetWaterLevel(wp.x, wp.z);

            if (wp.y - voxelHalfHeight < waterLevel)
            {
                k = (waterLevel - wp.y) / (2 * voxelHalfHeight) + 0.5f;
                if (k > 1)
                {
                    k = 1f;
                }
                else if (k < 0)
                {
                    k = 0f;
                }

                var velocity = CRigidBody.GetPointVelocity(wp);
                var localDampingForce = -velocity * DAMPFER * GetComponent<Rigidbody>().mass;
                var force = localDampingForce + Mathf.Sqrt(k) * localArchimedesForce;
                CRigidBody.AddForceAtPosition(force, wp);
            }
        }
    }
}