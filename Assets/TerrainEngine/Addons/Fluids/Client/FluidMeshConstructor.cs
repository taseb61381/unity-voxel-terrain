﻿
using TerrainEngine;
using UnityEngine;

public class FluidMeshConstructor : MonoBehaviour
{
    [HideInInspector]
    public FluidProcessor Processor;

    public virtual void OnStart()
    {

    }

    public virtual IMeshData GenerateMesh(ActionThread Thread, TerrainBlock Terrain, int ChunkId, FluidDatas FData, FluidInterpolation Interpolations,FluidInformation Info, byte Type)
    {
        return null;
    }

    public virtual void OnPreApply(FluidTempBlock Block)
    {

    }

    public virtual void OnCascade(FluidTempBlock Block, int ChunkId)
    {

    }

    public virtual void OnFlowStart(ActionThread Thread, FluidDatas Data, int ChunkId)
    {

    }

    public virtual void OnFlowDone(ActionThread Thread, FluidDatas Data, int ChunkId)
    {

    }
}
