using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("TerrainEngine/Water/Reflection")]
[ExecuteInEditMode] // Make mirror live-update even when not in play mode
public class TerrainEngineReflection : MonoBehaviour
{
    public Transform CTransform;
    public MeshRenderer Renderer;
    public MeshFilter CFilter;

    #region Variables
    public bool m_DisablePixelLights = true;
	public int m_TextureSize = 256;
	
	public LayerMask m_ReflectLayers = -1;

    public class CameraContainer
    {
        public int InstanceId;
        public Camera Cam;
        public Transform CamTR;
        public Skybox CamSky;
        public bool SkyCheck;
    }

    private SList<CameraContainer> m_ReflectionCameras = new SList<CameraContainer>(1);

    private CameraContainer GetCamera(int InstanceId)
    {
        for (int i = m_ReflectionCameras.Count - 1; i >= 0; --i)
        {
            if (m_ReflectionCameras.array[i].InstanceId == InstanceId)
                return m_ReflectionCameras.array[i];
        }

        return null;
    }

    private int GetCameraIndex(int InstanceId)
    {
        for(int i=m_ReflectionCameras.Count-1;i>=0;--i)
        {
            if (m_ReflectionCameras.array[i].InstanceId == InstanceId)
                return i;
        }

        return -1;
    }

    private CameraContainer SetCamera(CameraContainer Cont)
    {
        int Index = GetCameraIndex(Cont.InstanceId);
        if (Index == -1)
        {
            Cont.InstanceId = Cont.Cam.GetInstanceID();
            m_ReflectionCameras.Add(Cont);
            return Cont;
        }
        else
            return Cont;
    }
	

	private RenderTexture m_ReflectionTexture = null;
	private int m_OldReflectionTextureSize = 0;
	
	private static bool s_InsideRendering = false;
    public float m_ClipPlaneOffset = 0.07f;
    public float BoundY = -1;
    private int LastVertexCount = 0;

    static public int LastFrame = 0;

    public class YRenderTexture
    {
        public int Y;
        public RenderTexture Texture;
    }

    static public SList<YRenderTexture> Textures;

    static public RenderTexture GetTexture(int y)
    {
        for (int i = Textures.Count - 1; i >= 0; --i)
        {
            if (Textures.array[i].Y >= y && Textures.array[i].Y <= y+3)
            {
                return Textures.array[i].Texture;
            }
        }

        return null;
    }

    static public void AddTexture(int y, RenderTexture Texture)
    {
        for (int i = Textures.Count - 1; i >= 0; --i)
        {
            if (Textures.array[i].Y == y || Textures.array[i].Texture == null)
            {
                Textures.array[i].Y = y;
                Textures.array[i].Texture = Texture;
                return;
            }
        }

        Textures.Add(new YRenderTexture() { Y = y, Texture = Texture });
    }

    public void OnWillRenderObject()
    {
        if (!enabled || (object)Renderer == null || (object)Renderer.sharedMaterials == null || !Renderer.enabled)
            return;

        Camera cam = Camera.current;
        if ((object)cam == null)
            return;

        // Safeguard from recursive reflections.        
        if (s_InsideRendering)
            return;

        if ((object)CFilter == null)
        {
            CFilter = GetComponent<MeshFilter>();
            if (CFilter.sharedMesh == null)
                return;
        }

        if (CFilter.sharedMesh.vertexCount != LastVertexCount)
        {
            LastVertexCount = CFilter.sharedMesh.vertexCount;
            CFilter.sharedMesh.RecalculateBounds();
            BoundY = CFilter.sharedMesh.bounds.max.y;
        }

        if (Textures.array == null)
            Textures = new SList<YRenderTexture>(1);

        int i = 0;

        if (Time.frameCount != LastFrame)
        {
            for (i = Textures.Count - 1; i >= 0; --i)
            {
                if (Textures.array[i].Texture != null)
                    GameObject.DestroyImmediate(Textures.array[i].Texture);
            }

            LastFrame = Time.frameCount;
            Textures.ClearFast();
        }

        s_InsideRendering = true;


        // find out the reflection plane: position and normal in world space
        Vector3 pos = CTransform.position;
        pos.y += BoundY;

        int oldPixelLightCount = QualitySettings.pixelLightCount;
        m_ReflectionTexture = GetTexture((int)pos.y);

        if ((object)m_ReflectionTexture == null)
        {
            Vector3 normal = CTransform.up;
            Camera reflectionCamera = null;

            CameraContainer reflectionCont = CreateMirrorObjects(cam, out reflectionCamera);

            // Optionally disable pixel lights for reflection
            if (m_DisablePixelLights)
                QualitySettings.pixelLightCount = 0;

            UpdateCameraModes(cam, reflectionCont);

            // Render reflection
            // Reflect camera around reflection plane
            float d = -Vector3.Dot(normal, pos) - m_ClipPlaneOffset;
            Matrix4x4 reflection = Matrix4x4.zero;
            //Vector4 reflectionPlane = new Vector4(normal.x, normal.y, normal.z, d);
            //CalculateReflectionMatrix(ref reflection, reflectionPlane);

            reflection.m00 = (1F -  2F * normal.x * normal.x);
            reflection.m01 = (  -2F * normal.x * normal.y);
            reflection.m02 = (-2F * normal.x * normal.z);
            reflection.m03 = (-2F * d * normal.x);

            reflection.m10 = (-2F * normal.y * normal.x);
            reflection.m11 = (1F - 2F * normal.y * normal.y);
            reflection.m12 = (-2F * normal.y * normal.z);
            reflection.m13 = (-2F * d * normal.y);

            reflection.m20 = (-2F * normal.z * normal.x);
            reflection.m21 = (-2F * normal.z * normal.y);
            reflection.m22 = (1F - 2F * normal.z * normal.z);
            reflection.m23 = (-2F * d * normal.z);

            reflection.m30 = reflection.m31 = reflection.m32 = 0F;
            reflection.m33 = 1F;

            Transform camtransform = cam.transform;
            Vector3 oldpos = camtransform.position;
            Vector3 newpos = reflection.MultiplyPoint(oldpos);
            reflectionCamera.worldToCameraMatrix = cam.worldToCameraMatrix * reflection;

            // Setup oblique projection matrix so that near plane is our reflection
            // plane. This way we clip everything below/above it for free.
            //Vector4 clipPlane = CameraSpacePlane(reflectionCamera, pos, normal, 1.0f, m_ClipPlaneOffset);

            //Vector3 offsetPos = (pos + normal * m_ClipPlaneOffset);
            Matrix4x4 m = reflectionCamera.worldToCameraMatrix;
            Vector3 cpos = m.MultiplyPoint((pos + normal * m_ClipPlaneOffset));
            Vector3 cnormal = m.MultiplyVector(normal).normalized * 1.0f;
            Vector4 clipPlane = new Vector4(cnormal.x, cnormal.y, cnormal.z, -Vector3.Dot(cpos, cnormal));

            Matrix4x4 projection = cam.projectionMatrix;
            // CalculateObliqueMatrix(ref projection, clipPlane);
            Vector4 q = projection.inverse * new Vector4(
                (clipPlane.x < 0f ? -1f : (clipPlane.x > 0f ? 1f : 0f)),
                (clipPlane.y < 0f ? -1f : (clipPlane.y > 0f ? 1f : 0f)), 1.0f, 1.0f);

            q = clipPlane * (2.0F / (Vector4.Dot(clipPlane, q)));
            // third row = clip plane - fourth row
            projection[2] = q.x - projection[3];
            projection[6] = q.y - projection[7];
            projection[10] = q.z - projection[11];
            projection[14] = q.w - projection[15];


            reflectionCamera.projectionMatrix = projection;

            reflectionCamera.cullingMask = ~(1 << 4) & m_ReflectLayers.value; // never render water layer

            reflectionCamera.targetTexture = m_ReflectionTexture;
            //GL.invertCulling = true;        //should be used but inverts the faces of the terrain
            GL.SetRevertBackfacing(true);    //obsolete
            reflectionCont.CamTR.position = newpos;
            Vector3 euler = camtransform.eulerAngles;
            euler.x = 0;
            reflectionCont.CamTR.eulerAngles = euler;
            reflectionCamera.Render();
            reflectionCont.CamTR.position = oldpos;
            //GL.invertCulling = true;        //should be used but inverts the faces of the terrain
            GL.SetRevertBackfacing(false);   //obsolete

            AddTexture((int)pos.y, m_ReflectionTexture);
        }

        Material[] materials = Renderer.materials;
        int len = materials.Length - 1;
        for (i = len; i >= 0; --i)
        {
            materials[i].SetTexture("_ReflectionTex", m_ReflectionTexture);
        }

        // Set matrix on the shader that transforms UVs from object space into screen
        // space. We want to just project reflection texture on screen.
        pos.x = pos.y = pos.z = 0.5f;
        Matrix4x4 scaleOffset = Matrix4x4.TRS(pos, Quaternion.identity, pos);
        pos = CTransform.localScale;
        pos.x = 1.0f / pos.x;
        pos.y = 1.0f / pos.y;
        pos.z = 1.0f / pos.z;

        scaleOffset = scaleOffset * cam.projectionMatrix * cam.worldToCameraMatrix * (CTransform.localToWorldMatrix * Matrix4x4.Scale(pos));
        for (i = len; i >= 0; --i)
        {
            materials[i].SetMatrix("_ProjMatrix", scaleOffset);
        }

        // Restore pixel light count
        if (m_DisablePixelLights)
            QualitySettings.pixelLightCount = oldPixelLightCount;

        s_InsideRendering = false;
    }
	#endregion
	
    //<summary>
	// Cleans up all the objects that were possibly created
    //</summary>
    void OnDisable()
    {
        if (m_ReflectionTexture)
        {
            DestroyImmediate(m_ReflectionTexture);
            m_ReflectionTexture = null;
        }

        for (int i = m_ReflectionCameras.Count - 1; i >= 0; --i)
            if (m_ReflectionCameras.array[i].Cam != null)
                DestroyImmediate(m_ReflectionCameras.array[i].Cam.gameObject);
        m_ReflectionCameras.Clear();
    }
	
	private void UpdateCameraModes( Camera src, CameraContainer reflectionCam )
	{
		if( reflectionCam == null )
			return;

		//sets camera to clear the same way as current camera
		reflectionCam.Cam.clearFlags = src.clearFlags;
        reflectionCam.Cam.backgroundColor = src.backgroundColor; 
               
		if( src.clearFlags == CameraClearFlags.Skybox )
		{
			Skybox sky = src.GetComponent(typeof(Skybox)) as Skybox;
            if (!reflectionCam.SkyCheck && (object)reflectionCam.CamSky == null)
            {
                reflectionCam.SkyCheck = true;
                reflectionCam.CamSky = reflectionCam.Cam.GetComponent(typeof(Skybox)) as Skybox;
            }

			if( !sky || !sky.material )
			{
                reflectionCam.CamSky.enabled = false;
			}

			else
			{
                reflectionCam.CamSky.enabled = true;
                reflectionCam.CamSky.material = sky.material;
			}
		}

        ///<summary>
        ///Updates other values to match current camera.
        ///Even if camera&projection matrices are supplied, some of values are used elsewhere (e.g. skybox uses far plane)
        /// </summary>
        reflectionCam.Cam.farClipPlane = src.farClipPlane;
        reflectionCam.Cam.nearClipPlane = src.nearClipPlane;
        reflectionCam.Cam.orthographic = src.orthographic;
        reflectionCam.Cam.fieldOfView = src.fieldOfView;
        reflectionCam.Cam.aspect = src.aspect;
        reflectionCam.Cam.orthographicSize = src.orthographicSize;
	}
	
    //<summary>
	//Creates any objects needed on demand
    //</summary>
    static public System.Text.StringBuilder Builder = new System.Text.StringBuilder();
	private CameraContainer CreateMirrorObjects( Camera currentCamera, out Camera reflectionCamera )
	{
		reflectionCamera = null;
		
		//Reflection render texture
		if( !m_ReflectionTexture || m_OldReflectionTextureSize != m_TextureSize )
		{
			if( m_ReflectionTexture )
				DestroyImmediate( m_ReflectionTexture );
			m_ReflectionTexture = new RenderTexture( m_TextureSize, m_TextureSize, 16 );
			m_ReflectionTexture.name = GetInstanceID().ToString();
			m_ReflectionTexture.isPowerOfTwo = true;
			m_ReflectionTexture.hideFlags = HideFlags.HideInInspector;
			m_OldReflectionTextureSize = m_TextureSize;
		}
		
		//Camera for reflection
        CameraContainer Cont = GetCamera(currentCamera.GetInstanceID());
        if (Cont == null) // catch both not-in-dictionary and in-dictionary-but-deleted-GO
		{
            Builder.Length = 0;
            Builder.Append("Mirror Refl Camera id");
            Builder.Append(GetInstanceID());
            Builder.Append(currentCamera.GetInstanceID());
            GameObject go = new GameObject(Builder.ToString());
            
            Cont = new CameraContainer();
            Cont.CamTR = go.transform;
            Cont.Cam = reflectionCamera = go.AddComponent<Camera>();
            Cont.CamSky = go.AddComponent<Skybox>();
            Cont.SkyCheck = true;

            go.AddComponent<FlareLayer>();

			reflectionCamera.enabled = false;
            Cont.CamTR.position = CTransform.position;
            Cont.CamTR.rotation = CTransform.rotation;

			go.hideFlags = HideFlags.HideInHierarchy;
            Cont = SetCamera(Cont);
		}

        return Cont;
	}
	
    //<summary>
	//Extended sign: returns -1, 0 or 1 based on sign of a
    //</summary>
	private static float sgn(float a)
	{
		if (a > 0.0f) return 1.0f;
		if (a < 0.0f) return -1.0f;
		return 0.0f;
	}
	
    //<summary>
	//Given position/normal of the plane, calculates plane in camera space.
    //</summary>
    static private Vector4 CameraSpacePlane(Camera cam, Vector3 pos, Vector3 normal, float sideSign, float m_ClipPlaneOffset)
	{
		Vector3 offsetPos = pos + normal * m_ClipPlaneOffset;
		Matrix4x4 m = cam.worldToCameraMatrix;
		Vector3 cpos = m.MultiplyPoint( offsetPos );
		Vector3 cnormal = m.MultiplyVector( normal ).normalized * sideSign;
		return new Vector4( cnormal.x, cnormal.y, cnormal.z, -Vector3.Dot(cpos,cnormal) );
	}
	
    //<summary>
	//Adjusts the given projection matrix so that near plane is the given clipPlane
	//clipPlane is given in camera space
    //</summary>
	private static void CalculateObliqueMatrix (ref Matrix4x4 projection, Vector4 clipPlane)
	{
		Vector4 q = projection.inverse * new Vector4( sgn(clipPlane.x), sgn(clipPlane.y), 1.0f, 1.0f );
        q = clipPlane * (2.0F / (Vector4.Dot(clipPlane, q)));
		// third row = clip plane - fourth row
        projection[2] = q.x - projection[3];
        projection[6] = q.y - projection[7];
        projection[10] = q.z - projection[11];
        projection[14] = q.w - projection[15];
	}
	
    //<summary>
	//Calculates reflection matrix around the given plane
    //</summary>
	private static void CalculateReflectionMatrix (ref Matrix4x4 reflectionMat, Vector4 plane)
	{
		reflectionMat.m00 = (1F - 2F*plane.x*plane.x);
		reflectionMat.m01 = (   - 2F*plane.x*plane.y);
		reflectionMat.m02 = (   - 2F*plane.x*plane.z);
		reflectionMat.m03 = (   - 2F*plane.w*plane.x);
		
		reflectionMat.m10 = (   - 2F*plane.y*plane.x);
		reflectionMat.m11 = (1F - 2F*plane.y*plane.y);
		reflectionMat.m12 = (   - 2F*plane.y*plane.z);
		reflectionMat.m13 = (   - 2F*plane.w*plane.y);
		
		reflectionMat.m20 = (   - 2F*plane.z*plane.x);
		reflectionMat.m21 = (   - 2F*plane.z*plane.y);
		reflectionMat.m22 = (1F - 2F*plane.z*plane.z);
		reflectionMat.m23 = (   - 2F*plane.w*plane.z);
		
		reflectionMat.m30 = 0F;
		reflectionMat.m31 = 0F;
		reflectionMat.m32 = 0F;
		reflectionMat.m33 = 1F;
	}
}
