﻿
using TerrainEngine;

using UnityEngine;

[AddComponentMenu("TerrainEngine/Processors/Fluids")]
public class FluidProcessor : IBlockProcessor
{
    static public PoolInfo PoolContainer;

    public FluidInformation[] Fluids;

    public bool GenerateOcean = true;
    public bool IgnoreZ = false; // Used for 2D
    public int YOffset = 0;
    public GameObject DefaultWaterObject;
    private int DefaultPoolId;

    public override void OnInitManager(TerrainManager Manager)
    {
        base.OnInitManager(Manager);
        InternalDataName = "Fluid-" + DataName;

        if (Fluids == null || Fluids.Length <= 0)
        {
            Fluids = new FluidInformation[1];
            Fluids[0] = new FluidInformation();
            Fluids[0].VoxelName = "Water";
            Fluids[0].Viscosity = 0;
            Fluids[0].MinVolumeToFlow = 0;
            Fluids[0].VoxelID = Manager.Palette.GetVoxelId("Water");
        }

        foreach (FluidInformation Info in Fluids)
        {
            Info.Init();
            Info.VoxelID = Manager.Palette.GetVoxelId(Info.VoxelName);

            if (Info.Constructor == null)
                Info.Constructor = GetComponent<FluidMeshConstructor>();
        }
    }

    public override void OnInitSeed(int Seed)
    {
        base.OnInitSeed(Seed);
    }

    public override bool CanStart()
    {
        if (FluidTempBlock.FlowDirection == null)
        {
            FluidTempBlock.FlowDirection = new FluidTempBlock.FluidFlowDelegate[(int)FluidDirections.MAX];
            FluidTempBlock.FlowDirection[(int)FluidDirections.LEFT] = FluidTempBlock.FlowLeft;
            FluidTempBlock.FlowDirection[(int)FluidDirections.RIGHT] = FluidTempBlock.FlowRight;
            FluidTempBlock.FlowDirection[(int)FluidDirections.FORWARD] = FluidTempBlock.FlowForward;
            FluidTempBlock.FlowDirection[(int)FluidDirections.BACKWARD] = FluidTempBlock.FlowBackward;

            FluidTempBlock.FlowDirection[(int)(FluidDirections.LEFT | FluidDirections.RIGHT)] = FluidTempBlock.FlowLeftRight;
            FluidTempBlock.FlowDirection[(int)(FluidDirections.FORWARD | FluidDirections.BACKWARD)] = FluidTempBlock.FlowForwardBackward;
            FluidTempBlock.FlowDirection[(int)(FluidDirections.FORWARD | FluidDirections.LEFT)] = FluidTempBlock.FlowForwardLeft;
            FluidTempBlock.FlowDirection[(int)(FluidDirections.FORWARD | FluidDirections.RIGHT)] = FluidTempBlock.FlowForwardRight;
            FluidTempBlock.FlowDirection[(int)(FluidDirections.BACKWARD | FluidDirections.LEFT)] = FluidTempBlock.FlowBackwardLeft;
            FluidTempBlock.FlowDirection[(int)(FluidDirections.BACKWARD | FluidDirections.RIGHT)] = FluidTempBlock.FlowBackwardRight;

            FluidTempBlock.FlowDirection[(int)(FluidDirections.LEFT | FluidDirections.RIGHT | FluidDirections.FORWARD)] = FluidTempBlock.FlowLeftRightForward;
            FluidTempBlock.FlowDirection[(int)(FluidDirections.LEFT | FluidDirections.RIGHT | FluidDirections.BACKWARD)] = FluidTempBlock.FlowLeftRightBackward;
            FluidTempBlock.FlowDirection[(int)(FluidDirections.FORWARD | FluidDirections.BACKWARD | FluidDirections.LEFT)] = FluidTempBlock.FlowForwardBackwardLeft;
            FluidTempBlock.FlowDirection[(int)(FluidDirections.FORWARD | FluidDirections.BACKWARD | FluidDirections.RIGHT)] = FluidTempBlock.FlowForwardBackwardRight;

            FluidTempBlock.FlowDirection[(int)(FluidDirections.FORWARD | FluidDirections.BACKWARD | FluidDirections.LEFT | FluidDirections.RIGHT)] = FluidTempBlock.FlowAll;
        }

        EnableUnityUpdateFunction = true;
        EnableThreadUpdateFunction = true;
        InterlacedUpdate = true;

        foreach (FluidInformation Info in Fluids)
        {
            if (Info.Constructor == null)
            {
                Debug.LogError("There is no MeshConstructor for the FluidProcessor. Terrain can not start");
                return false;
            }
            else
                Info.Constructor.Processor = this;
        }

        return true;
    }

    public override void OnStart()
    {
        Debug.Log("FluidProcessor : Starting");

        GameObject DefaultPoolObject = DefaultWaterObject == null ? new GameObject() : GameObject.Instantiate(DefaultWaterObject) as GameObject;
        DefaultPoolObject.name = DataName;
        DefaultPoolObject.AddComponent<TerrainSubScript>();
        DefaultPoolObject.SetActive(false);
        DefaultPoolObject.transform.parent = transform;

        DefaultPoolId = GameObjectPool.GetPoolId(InternalDataName + DefaultPoolObject.GetInstanceID());
        GameObjectPool.SetDefaultObject(DefaultPoolId, DefaultPoolObject);

        for (int i = 0; i < Fluids.Length; ++i)
            Fluids[i].Constructor.OnStart();
    }

    public override void OnBlockDataCreate(TerrainDatas Data)
    {
        FluidDatas FData = Data.GetCustomData<FluidDatas>(InternalDataName);
        if (FData == null)
        {
            FData = new FluidDatas();
            Data.RegisterCustomData(InternalDataName, FData);
        }
    }

    public override void OnTerrainUnityClose(TerrainBlock Terrain)
    {
        ITerrainScript IScript = Terrain.Script;
        if (IScript != null && IScript.Scripts.array != null)
        {
            for(int i=0;i<IScript.Scripts.Count;++i)
            {
                if (IScript.Scripts.array[i] != null && IScript.Scripts.array[i].name == DataName)
                {
                    GameObjectPool.Pools.array[DefaultPoolId].CloseObject(IScript.Scripts.array[i].gameObject);
                }
            }
        }
    }

    public override void OnFlushVoxels(ActionThread Thread, TerrainObject Obj, ref VoxelFlush Voxel, TerrainChunkGenerateGroupAction Action)
    {
        if (Voxel.ModType == VoxelModificationType.SEND_EVENTS)
            return;

        FluidDatas FData = Voxel.Block.Voxels.GetCustomData<FluidDatas>(InternalDataName);
        if (TerrainManager.Instance.Palette.GetType(Voxel.NewType) == VoxelTypes.FLUID)
        {
            FData.SetVolumeAndDirection(Voxel.x, Voxel.y, Voxel.z,Voxel.ChunkId, (byte)Mathf.Clamp(255f * Voxel.NewVolume, 0, 255), FluidDirections.NONE);
        }
        else
            FData.SetVolumeAndDirection(Voxel.x, Voxel.y, Voxel.z,Voxel.ChunkId, 0, FluidDirections.NONE);
    }

    public override void OnChunksGenerationDone(TerrainBlock Terrain)
    {
        TerrainInformation Info = Terrain.Information;
        FluidDatas FData = Terrain.Voxels.GetCustomData<FluidDatas>(InternalDataName);
        int TotalChunksPerBlock = Info.TotalChunksPerBlock;
        int i = 0;
        for (i = TotalChunksPerBlock-1; i >= 0; --i)
        {
            FData.States[i].Dirty = true;
        }
        FData.Dirty = true;

        if (Terrain.LeftBlock != null)
        {
            TerrainBlock AroundBlock = Terrain.LeftBlock as TerrainBlock;
            FData = AroundBlock.Voxels.GetCustomData<FluidDatas>(InternalDataName);
            int ChunkPerBlock = Info.ChunkPerBlock - 1;
            for (i = 0; i < TotalChunksPerBlock; ++i)
            {
                if (Info.Chunks[i].LocalX == ChunkPerBlock)
                {
                    FData.States[i].MustReload = true;
                }
            }

            FData.Dirty = true;
        }
        if (Terrain.BackwardBlock != null)
        {
            TerrainBlock AroundBlock = Terrain.BackwardBlock as TerrainBlock;
            FData = AroundBlock.Voxels.GetCustomData<FluidDatas>(InternalDataName);
            int ChunkPerBlock = Info.ChunkPerBlock - 1;
            for (i = 0; i < TotalChunksPerBlock; ++i)
            {
                if (Info.Chunks[i].LocalZ == ChunkPerBlock)
                {
                    FData.States[i].MustReload = true;
                }
            }

            FData.Dirty = true;
        }
        if (Terrain.BottomBlock != null)
        {
            TerrainBlock AroundBlock = Terrain.BottomBlock as TerrainBlock;
            FData = AroundBlock.Voxels.GetCustomData<FluidDatas>(InternalDataName);
            int ChunkPerBlock = Info.ChunkPerBlock - 1;
            for (i = 0; i < TotalChunksPerBlock; ++i)
            {
                if (Info.Chunks[i].LocalY == ChunkPerBlock)
                {
                    FData.States[i].MustReload = true;
                }
            }

            FData.Dirty = true;
        }
    }

    public override bool OnTerrainThreadUpdate(ActionThread Thread, TerrainBlock Terrain)
    {
        if (Terrain == null || Terrain.HasState(TerrainBlockStates.CLOSED))
            return true;

        FluidDatas FData = Terrain.Voxels.GetCustomData<FluidDatas>(InternalDataName);
        if (!FData.Dirty && FData.ThreadStep == 0)
            return true;

        if (FData.ThreadStep == 0)
        {
            FData.Dirty = false;
            FData.ThreadStep = 1;
            FData.ChunkId = TerrainManager.Instance.Informations.TotalChunksPerBlock - 1;
        }

        if (FData.ThreadStep == 1)
        {
            if (ThreadManager.EnableStats)
                Thread.RecordTime();

            // Foreach Chunk : Load fluids voxels, Flow fluids
            for (; FData.ChunkId >= 0;)
            {
                FlowChunk(Thread, FData, Terrain, FData.ChunkId);
                --FData.ChunkId;

                if (!Thread.HasTime())
                {
                    if (ThreadManager.EnableStats)
                        Thread.AddStats("FLUID_FLOW", Thread.Time());

                    return false;
                }
            }

            if (ThreadManager.EnableStats)
                Thread.AddStats("FLUID_FLOW", Thread.Time());

            FData.ThreadStep = 2;
            FData.ChunkId = 0;
        }


        if (FData.ThreadStep == 2)
        {
            if (ThreadManager.EnableStats)
                Thread.RecordTime();

            for (int Index = FData.FluidMesh.Count - 1; Index >= 0;--Index )
            {
                if (FData.FluidMesh.array[Index].Dirty)
                {
                    GenerateChunk(Thread, FData, Terrain,
                        FData.FluidMesh.array[Index].ChunkId,
                        FData.FluidMesh.array[Index].FluidIndex,
                        Index);
                }
            }

            FData.ThreadStep = 3;

            if (ThreadManager.EnableStats)
                Thread.AddStats("FLUID_GENERATE", Thread.Time());
        }



        ////////////////////////////////////////////////////// MESH COMBINER /////////////////////////////////////////////////////////////////
        if (FData.ThreadStep == 3)
        {
            /*if (FData.GenerateCount == 0 && FData.CombinedMesh != null)
            {
                if (ThreadManager.EnableStats)
                    Thread.RecordTime();

                CombineChunks(Thread, FData, Terrain);

                if (ThreadManager.EnableStats)
                    Thread.AddStats("FLUID_COMBINE_MESH", Thread.Time());
            }*/

            FData.ThreadStep = 0;
        }

        //Debug.Log(Thread.ThreadId + "ThreadUpdate Done " + FData.ThreadStep);
        return true;
    }

    public override bool OnTerrainUnityUpdate(ActionThread Thread, TerrainBlock Terrain)
    {
        if (Terrain == null || Terrain.HasState(TerrainBlockStates.CLOSED))
            return true;

        FluidDatas FData = Terrain.Voxels.GetCustomData<FluidDatas>(InternalDataName);
        if (!FData.Dirty)
            return true;

        int FluidIndex;
        int Len = Fluids.Length;
        int Index = 0;

        if (FData.ThreadStep == 0)
        {
            if (FData.GenerateCount == 0 && FData.CombinedMesh != null)
            {
                IMeshData Data = null;

                for (FluidIndex = 0; FluidIndex < Len; ++FluidIndex)
                {
                    Data = FData.CombinedMesh[FluidIndex];
                    if (Data != null)
                    {
                        BuildChunkFluid(Terrain,FData, int.MaxValue, FluidIndex, true, Data);
                        FData.CombinedMesh[FluidIndex] = null;
                    }
                }
            }

            FData.ThreadStep = 1;
            FData.ChunkId = FData.FluidMesh.Count - 1;
        }

        if (FData.ThreadStep == 1)
        {
            FData.IsModified = false;
            int MeshIndex = 0;

            IMeshData Data;
            for (Index = FData.FluidMesh.Count - 1; Index >= 0; --Index)
            {
                if (!FData.FluidMesh.array[Index].Dirty)
                    continue;

                if (FData.FluidMesh.array[Index].Combined && FData.GenerateCount != 0)
                {
                    Debug.Log(Terrain.BlockId + ":" + Index + " Fluids are modified. Must Remove Combined Mesh");
                    FData.IsModified = true;
                }

                MeshIndex = FData.FluidMesh.array[Index].MeshIndex;
                Data = MeshIndex != -1 ? FData.FluidMeshData.array[MeshIndex] : null;

                if (!FData.FluidMesh.array[Index].Combined)
                {
                    BuildChunkFluid(Terrain, FData,
                        FData.FluidMesh.array[Index].ChunkId,
                        FData.FluidMesh.array[Index].FluidIndex,
                        true,
                        Data);

                    FData.FluidMesh.array[Index].HasMesh = Data != null && Data.IsValid();
                    FData.FluidMesh.array[Index].Dirty = false;

                    if (MeshIndex != -1)
                    {
                        FData.FluidMeshData.array[MeshIndex] = null;
                        FData.FluidMesh.array[Index].MeshIndex = -1;
                    }
                }
                else
                {
                    FData.FluidMesh.array[Index].HasMesh = Data != null && Data.IsValid();
                    FData.FluidMesh.array[Index].Dirty = false;
                }
            }

            FData.ThreadStep = 2;

        }

        if (FData.ThreadStep == 2)
        {
            if (FData.IsModified)
            {
                IMeshData Data;
                int MeshIndex = 0;

                for (FluidIndex = 0; FluidIndex < Len; ++FluidIndex)
                {
                    // Delete Combined Mesh
                    BuildChunkFluid(Terrain, FData, int.MaxValue, FluidIndex, false, null);
                }

                for (Index = FData.FluidMesh.Count - 1; Index >= 0; --Index)
                {
                    if (FData.FluidMesh.array[Index].Combined)
                    {
                        FData.FluidMesh.array[Index].Combined = false;

                        MeshIndex = FData.FluidMesh.array[Index].MeshIndex;
                        Data = MeshIndex != -1 ? FData.FluidMeshData.array[MeshIndex] : null;

                        BuildChunkFluid(Terrain,
                            FData, FData.FluidMesh.array[Index].ChunkId,
                            FData.FluidMesh.array[Index].FluidIndex,
                            true,
                            Data);
                    }
                }
            }

            FData.ChunkId = 0;
            FData.ThreadStep = 0;
        }

        ++FData.GenerateCount;
        return true;
    }

    public void FlowChunk(ActionThread Thread, FluidDatas FData, TerrainBlock Terrain, int ChunkId)
    {
        // Chunk has not changed
        if (!FData.States[ChunkId].Dirty)
            return;

        FData.States[ChunkId].Dirty = false;

        // Chunk doesn't have fluids
        FluidData Fdata = FData.Datas[ChunkId];
        if (Fdata == null)
            return;

        //Debug.Log(Thread.ThreadId + ",FlowChunk " + ChunkId + ",Dirty");

        if (ThreadManager.EnableStats)
            Thread.RecordTime();

        int StartX = Terrain.Chunks[ChunkId].StartX;
        int StartY = Terrain.Chunks[ChunkId].StartY;
        int StartZ = Terrain.Chunks[ChunkId].StartZ;

        FluidInformation Info;
        FluidInterpolation Interpolations;
        int FluidsLenght = Fluids.Length;
        int FlowCount = 0; // Number of voxel changed
        int NewFluidCount = 0;
        bool HasChanged = false;
        int Index = 0;
        bool IsValid = false;

        // For each fluid type (water,lava,etc) load voxels,flow
        for (int j = 0; j < FluidsLenght; ++j)
        {
            if ((Info = Fluids[j]).EnableMesh)
            {
                if (Info.CanFlow)
                {
                    Interpolations = GetInterpolations(Thread, Terrain, StartX, StartY, StartZ, FData, Info.VoxelID);
                    Info.Constructor.OnFlowStart(Thread, FData, ChunkId);

                    NewFluidCount = 0;
                    FlowCount = FlowFluids(Thread, Interpolations, Fdata, Info, ChunkId, ref NewFluidCount);
                    Info.Constructor.OnFlowDone(Thread, FData, ChunkId);

                    Interpolations.Close();
                }

                IsValid = FlowCount != 0 || FData.States[ChunkId].MustReload;
                //Debug.Log(IsValid + ", IsValid:" + ChunkId + ",Count:" + FlowCount + ",FluidCount:" + NewFluidCount);

                if (IsValid)
                {
                    Index = FData.GetMeshIndex(ChunkId, (byte)j);
                }
                else
                {
                    if (NewFluidCount != 0)
                    {
                        Index = FData.GetMeshIndex(ChunkId, (byte)j);
                        IsValid = Index == -1 || !FData.FluidMesh.array[Index].HasMesh;
                    }
                }

                // If mesh has changed or chunk doesn"t have mesh and there are fluids on chunk or chunk has mesh but they are no more fluids on it
                if (IsValid)
                {
                    //Debug.Log(FData.GenerateCount+":Chunk:"+ ChunkId + ",FlowCount:" + FlowCount + ",MustReload:" + FData.States[ChunkId].MustReload + ",NewFluidCount:" + NewFluidCount);

                    if (Index == -1)
                    {
                        if (FData.FluidMesh.Count == 0)
                            FData.FluidMesh = new SList<FluidMeshData>((Terrain.Information.ChunkPerBlock * Terrain.Information.ChunkPerBlock) / 3);

                        FData.FluidMesh.Add(new FluidMeshData(ChunkId, (byte)j, true));
                    }
                    else
                    {
                        FData.FluidMesh.array[Index].Dirty = true;
                    }

                    HasChanged = true;
                }
            }
        }

        if (HasChanged)
        {
            FData.States[ChunkId].MustReload = false;
            FData.Dirty = true;
        }

        //Debug.Log(Terrain.BlockId + " Flow Chunk : " + ChunkId + ",HasMesh:" + FData.States[ChunkId].HasMesh + ",Dirty:" + FData.States[ChunkId].Dirty);

        if (ThreadManager.EnableStats)
            Thread.AddStats("FLUID_FLOW_CHUNK", Thread.Time());
    }

    public void GenerateChunk(ActionThread Thread, FluidDatas FData, TerrainBlock Terrain, int ChunkId, byte FluidIndex, int Index)
    {
        FluidInformation Info;
        if (!(Info = Fluids[FluidIndex]).EnableMesh)
            return;

        if (ThreadManager.EnableStats)
            Thread.RecordTime();

        FluidInterpolation Interpolations;
        IMeshData ChunkMesh = null;

        int LocalX = Terrain.Chunks[ChunkId].LocalX;
        int LocalY = Terrain.Chunks[ChunkId].LocalY;
        int LocalZ = Terrain.Chunks[ChunkId].LocalZ;

        int FluidsLenght = Fluids.Length;
        bool IsRegeneratingCorners = TerrainManager.Instance.RegenerateCornersChunks;

        Interpolations = GetInterpolations(Thread, Terrain,
            LocalX * TerrainManager.Instance.Informations.VoxelPerChunk,
            LocalY * TerrainManager.Instance.Informations.VoxelPerChunk,
            LocalZ * TerrainManager.Instance.Informations.VoxelPerChunk, 
            FData, Info.VoxelID);
        ChunkMesh = Info.Constructor.GenerateMesh(Thread, Terrain, ChunkId, FData, Interpolations, Info, (byte)Info.VoxelID);

        if (ChunkMesh != null)
        {
            FData.FluidMesh.array[Index].MeshIndex = FData.GetMeshDataFreeSlot(ChunkMesh);
        }
        else
            FData.FluidMesh.array[Index].MeshIndex = -1;

        Interpolations.Close();

        ////////////////////////////////////////////////////// MESH COMBINER /////////////////////////////////////////////////////////////////
        /*if (FData.GenerateCount == 0 && Info.EnableMeshCombiner)
        {
            if (IsRegeneratingCorners)
            {
                if (LocalX == TerrainManager.Instance.Informations.ChunkPerBlock - 1
                    || LocalY == TerrainManager.Instance.Informations.ChunkPerBlock - 1
                    || LocalZ == TerrainManager.Instance.Informations.ChunkPerBlock - 1)
                    return;
            }

            if (ChunkMesh != null && ChunkMesh.IsValid())
            {
                if (FData.CombinedMesh == null)
                {
                    FData.CombinedMesh = new MeshData[Fluids.Length];
                }

                if ((CombinedMesh = FData.CombinedMesh[FluidIndex]) == null)
                {
                    CombinedMesh = FData.CombinedMesh[FluidIndex] = new MeshData();
                    CombinedMesh.Materials = ChunkMesh.Materials;
                }

                if (CombinedMesh.VerticesCount + ChunkMesh.Vertices.Length > 60000)
                    return;

                FData.FluidMesh.array[Index].Combined = true;

                CombinedMesh.VerticesCount += ChunkMesh.Vertices.Length;
                CombinedMesh.IndiceCount += ChunkMesh.Indices[0].Length;
            }
        }*/
        ////////////////////////////////////////////////////// MESH COMBINER /////////////////////////////////////////////////////////////////

        if (ThreadManager.EnableStats)
            Thread.AddStats("FLUID_GENERATE_CHUNK", Thread.Time());
    }

    public void CombineChunks(ActionThread Thread, FluidDatas FData, TerrainBlock Block)
    {
        IMeshData CombinedMesh = null;
        IMeshData Data = null;
        bool IsRegeneratingCorners = TerrainManager.Instance.RegenerateCornersChunks;
        int FluidsLenght = Fluids.Length;
        int j = 0;

        int CurrentVertex = 0;
        int CurrentIndice = 0;
        int v = 0;
        int[] Indices;
        int[] CurrentIndices;
        Vector3 LocalBlockWorldPosition;
        int ChunkId = 0, Index = 0;

        for (j = 0; j < FluidsLenght; ++j)
        {
            if (!Fluids[j].EnableMesh)
                continue;

            CombinedMesh = FData.CombinedMesh[j];

            if (CombinedMesh != null)
            {
                if (CombinedMesh.VerticesCount != 0)
                {
                    CombinedMesh.Vertices = new Vector3[CombinedMesh.VerticesCount];
                    CurrentVertex = CurrentIndice = 0;

                    if (CombinedMesh.Indices == null)
                        CombinedMesh.Indices = new int[1][];

                    CurrentIndices = CombinedMesh.Indices[0] = new int[CombinedMesh.IndiceCount];

                    for (Index = FData.FluidMesh.Count - 1; Index >= 0;--Index)
                    {
                        if (FData.FluidMesh.array[Index].FluidIndex != (byte)j || FData.FluidMesh.array[Index].Combined == false || FData.FluidMesh.array[Index].Dirty == false)
                            continue;

                        if (FData.FluidMesh.array[Index].MeshIndex == -1)
                            continue;

                        Data = FData.FluidMeshData.array[FData.FluidMesh.array[Index].MeshIndex];
                        if (Data == null || !Data.IsValid())
                            continue;

                        if (Data.Vertices.Length + CurrentVertex > CombinedMesh.VerticesCount)
                        {
                            Debug.LogError("Too many vertices:" + Data.Vertices.Length + ",VerticesCount:" + CombinedMesh.VerticesCount + ",Current:" + CurrentVertex);
                            continue;
                        }

                        ChunkId = FData.FluidMesh.array[Index].ChunkId;
                        Indices = Data.Indices[0];
                        LocalBlockWorldPosition = Block.Chunks[ChunkId].GetLocalWorldPosition(Block);

                        if (Indices.Length + CurrentVertex > CurrentIndices.Length)
                        {
                            Debug.LogError("Too many Indices:" + Indices.Length + ",VerticesCount:" + CurrentIndices.Length + ",Current:" + CurrentVertex);
                            continue;
                        }

                        for (v = 0; v < Data.Vertices.Length; ++v)
                        {
                            CombinedMesh.Vertices[CurrentVertex + v] = LocalBlockWorldPosition + Data.Vertices[v];
                        }

                        for (v = 0; v < Indices.Length; ++v)
                        {
                            CurrentIndices[v + CurrentIndice] = Indices[v] + CurrentVertex;
                        }

                        if (Data.UVs != null)
                        {
                            if (CombinedMesh.UVs == null)
                                CombinedMesh.UVs = new Vector2[CombinedMesh.VerticesCount];

                            for (v = 0; v < Data.UVs.Length; ++v)
                            {
                                CombinedMesh.UVs[CurrentVertex + v] = Data.UVs[v];
                            }
                        }

                        if (Data.Normals != null)
                        {
                            if (CombinedMesh.Normals == null)
                                CombinedMesh.Normals = new Vector3[CombinedMesh.VerticesCount];

                            for (v = 0; v < Data.Normals.Length; ++v)
                            {
                                CombinedMesh.Normals[CurrentVertex + v] = Data.Normals[v];
                            }
                        }

                        if (Data.Tangents != null)
                        {
                            if (CombinedMesh.Tangents == null)
                                CombinedMesh.Tangents = new Vector4[CombinedMesh.VerticesCount];

                            for (v = 0; v < Data.Tangents.Length; ++v)
                            {
                                CombinedMesh.Tangents[CurrentVertex + v] = Data.Tangents[v];
                            }
                        }

                        if (Data.Colors != null)
                        {
                            if (CombinedMesh.Colors == null)
                                CombinedMesh.Colors = new Color[CombinedMesh.VerticesCount];

                            for (v = 0; v < Data.Colors.Length; ++v)
                            {
                                CombinedMesh.Colors[CurrentVertex + v] = Data.Colors[v];
                            }
                        }

                        CurrentVertex += Data.Vertices.Length;
                        CurrentIndice += Data.Indices[0].Length;
                    }
                }
            }
        }
    }

    public bool BuildChunkFluid(TerrainBlock Block,FluidDatas FData, int ChunkId, int FluidIndex, bool EnableRenderer, IMeshData Data)
    {
        ITerrainScript IScript = null;

        //Debug.Log(Block.BlockId + " BuildChunkFluid : " + ChunkId + ",HasMesh:" + FData.States[ChunkId].HasMesh + ",Dirty:" + FData.States[ChunkId].Dirty + ",D:" + Data);

        if (Data != null && Data.IsValid())
        {
            Block.CheckGameObject();
            IScript = Block.Script as ITerrainScript;

            string Name = InternalDataName + FluidIndex;

            TerrainSubScript Script = IScript.GetSubScript<TerrainSubScript>(Name.GetHashCode(),ChunkId);
            if (Script == null)
            {
                GameObject Obj = GameObjectPool.Pools.array[DefaultPoolId].GetDefaultObject();
                Transform tr = Obj.transform;

                Obj.layer = LayerMask.NameToLayer("Water");
                Obj.name = DataName;

                Script = Obj.GetComponent<TerrainSubScript>();
                Script.Init(Name.GetHashCode(),ChunkId, this, IScript);
                tr.parent = IScript.transform;

                if (ChunkId != int.MaxValue)
                    tr.position = Block.GetChunkWorldPosition(ChunkId);
                else
                    tr.localPosition = new Vector3();
            }

            FluidInformation Info = Fluids[FluidIndex];
            Data.GenerateUVS = Info.GenerateUV;
            Data.GenerateTangents = Info.GenerateTangents;
            Script.Apply(Data);

            if (EnableRenderer)
                Script.CRenderer.enabled = true;
            else
                Script.CRenderer.enabled = false;
        }
        else
        {
            IScript = Block.Script as ITerrainScript;

            if (IScript != null)
            {
                string Name = InternalDataName + FluidIndex;

                TerrainSubScript Script = IScript.GetSubScript<TerrainSubScript>(Name.GetHashCode(),ChunkId);
                if (Script != null)
                {
                    Script.Apply(null);
                    Script.CRenderer.enabled = false;
                    Script.Close(true);
                    GameObjectPool.Pools.array[DefaultPoolId].CloseObject(Script.gameObject);
                }
            }
        }

        if (Data != null)
        {
            Data.Close();
        }
        return true;
    }

    public FluidInterpolation GetInterpolations(ActionThread Thread, TerrainBlock Terrain, int StartX, int StartY, int StartZ, FluidDatas FData, int FluidType)
    {
        int Size = TerrainManager.Instance.Informations.VoxelPerChunk + 2;

        FluidInterpolation Interpolations = Thread.Pool.UnityPool.FluidPool.GetData<FluidInterpolation>();
        Interpolations.Reset(Size, Size, Size);
        Interpolations.Data = FData;

        int px, py, pz = 0, f=0;
        bool sx, sy;
        int rx, ry, rz = 0;

        FluidDatas AData = null;
        TerrainBlock ATerrain = null;
        int x, y, z;
        for (x = 0; x < Size; ++x)
        {
            px = StartX + x - 1;
            sx = px > -1 && px < Terrain.Voxels.VoxelsPerAxis; // Terrain.Voxels.IsValidX();

            for (y = 0; y < Size; ++y)
            {
                py = StartY + y - 1;
                sy = py > -1 && py < Terrain.Voxels.VoxelsPerAxis;// Terrain.Voxels.IsValidY();

                for (z = 0; z < Size; ++z, ++f)
                {
                    pz = StartZ + z - 1;

                    rx = px;
                    ry = py;
                    rz = pz;

                    ATerrain = null;
                    AData = null;

                    if (!sx || !sy || !(pz > -1 && pz < Terrain.Voxels.VoxelsPerAxis))
                    {
                        ATerrain = Terrain.GetAroundBlock(ref rx, ref ry, ref rz) as TerrainBlock;

                        if (ATerrain != null)
                            AData = ATerrain.Voxels.GetCustomData<FluidDatas>(InternalDataName);
                    }
                    else
                    {
                        ATerrain = Terrain;
                        AData = FData;
                    }

                    Interpolations.Temp[f].Init(ATerrain, AData, rx, ry, rz, FluidType);
                }
            }
        }

        return Interpolations;
    }

    public int FlowFluids(ActionThread Thread, FluidInterpolation Interpolations, FluidData Fdata,FluidInformation FluidInfo, int ChunkId, ref int FluidCount)
    {
        int Count = 0,x,y,z,f=0;
        VoxelFlush Flush = new VoxelFlush();
        FluidTempBlock Block = null;

        int Size = TerrainManager.Instance.Informations.VoxelPerChunk + 2;
        int i=0;

        for (x = 0; x < Size; ++x)
        {
        for (y = 0; y < Size; ++y)
        {
                for (z = 0; z < Size; ++z)
                {
                    Interpolations.Temp[x * Size * Size + y * Size + z].Flow(FluidInfo, ChunkId, IgnoreZ);
                }
            }
        }

        if (FluidInfo.Evaporation != 0)
        {
            for (y = 1; y < Size - 1; ++y)
            {
                for (x = 0; x < Size; ++x)
                {
                    for (z = 0; z < Size; ++z)
                    {
                        if (Interpolations.Temp[x * Size * Size + (y + 1) * Size + z].IsEmpty())
                            Interpolations.Temp[x * Size * Size + y * Size + z].Evaporate(FluidInfo);
                    }
                }
            }
        }

        int ProcessorsCount = TerrainManager.Instance.Processors.Count;
        bool HasFlush = false;
        for (f = (Size * Size * Size) - 1; f >= 0; --f)
        {
            Block = Interpolations.Temp[f];
            if (Block.Apply(FluidInfo))
            {
                HasFlush = true;
                Flush.Init(Block.Block,Block.ChunkId,Block.x, Block.y, Block.z,
                        0,Block.Type,0,Block.Volume,VoxelModificationType.SEND_EVENTS);

                for (i = 0; i < ProcessorsCount; ++i)
                    TerrainManager.Instance.Processors[i].OnFlushVoxels(Thread, null,ref Flush, null);

                ++Count;
            }

            if (Block.Type == FluidInfo.VoxelID)
                ++FluidCount;
        }

        if (HasFlush)
        {
            for (i = 0; i < ProcessorsCount; ++i)
                TerrainManager.Instance.Processors[i].OnVoxelFlushDone(Thread, null);
        }

        return Count;
    }

    public override IWorldGenerator GetGenerator()
    {
        FluidGenerator Gen = new FluidGenerator();
        Gen.Priority = Priority;
        Gen.Name = InternalName;
        Gen.DataName = "Fluid-" + DataName;
        Gen.GenerateOcean = GenerateOcean;
        Gen.YOffset = YOffset;
        Gen.Fluids = Fluids;
        return Gen;
    }
}
