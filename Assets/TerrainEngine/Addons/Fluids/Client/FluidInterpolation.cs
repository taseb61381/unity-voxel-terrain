﻿using System;

namespace TerrainEngine
{
    public class FluidTempBlock
    {
        public delegate void FluidFlowDelegate(FluidTempBlock Fluid);
        static public FluidFlowDelegate[] FlowDirection;

        #region Values

        public byte Type, FluidType;
        public int Volume, OriginalVolume;
        public FluidDirections Direction;
        public bool Dirty;

        public TerrainBlock Block;
        public FluidDatas Data;
        public FluidData FData;
        public ushort x, y, z = 0;
        public float TypeVolume;

        public FluidTempBlock Bottom;
        public FluidTempBlock Left;
        public FluidTempBlock Right;
        public FluidTempBlock Forward;
        public FluidTempBlock Backward;

        public int ChunkId;
        public int FlattenVoxel;

        public void Clear()
        {
            Dirty = false;
        }

        #endregion


        public void Init(TerrainBlock Block, FluidDatas Data, int x, int y,int z, int FluidType)
        {
            this.FluidType = (byte)FluidType;
            this.Block = Block;
            this.Data = Data;
            this.x = (ushort)x;
            this.y = (ushort)y;
            this.z = (ushort)z;
            this.FlattenVoxel = 0;
            this.FData = null;
            this.Dirty = false;
            this.TypeVolume = 0;

            if (Block != null)
                this.ChunkId = Block.Information.GetChunkIdFromVoxels(x, y, z);

            if (Data != null)
            {
                FlattenVoxel = Block.Information.GetVoxelFlatten(x, y, z);
                FData = Data.Datas[ChunkId];

                if (!Block.HasRegisterVolume(x, y, z))
                {
                    Block.Voxels.GetTypeAndVolume(x, y, z, ref Type, ref TypeVolume);
                    if (TypeVolume < TerrainInformation.Isolevel)
                        Type = 0;

                    Data.GetVolumeAndDirection(x, y, z, ChunkId, FlattenVoxel, ref Direction, ref OriginalVolume);
                    Volume = OriginalVolume;
                }
                else
                {
                    Volume = FluidData.MaxVolume;
                    Type = 0;
                }
            }
            else
            {
                this.Block = null;
                this.Data = null;
                this.Direction = FluidDirections.NONE;
                this.Volume = FluidData.MaxVolume;
                this.Type = 0;
            }
        }

        public bool Apply(FluidInformation Info)
        {
            if (!Dirty)
                return false;

            Info.Constructor.OnPreApply(this);

            if (Volume > 0)
            {
                Type = FluidType;
                if (Volume > FluidData.MaxVolume)
                    Volume = FluidData.MaxVolume;
            }

            Dirty = false;

            if (FData == null)
            {
                FData = Data.Datas[ChunkId] = FluidDatas.PoolContainer.GetData<FluidData>();
                FData.Init(Data.VoxelCount);
            }

            FData.Blocks[FlattenVoxel].Init((byte)Volume, (byte)Direction);

            Data.States[ChunkId].Dirty = true;
            Data.Dirty = true;

            if (Block == null || Block.Voxels == null)
                return true;

            Block.Voxels.SetTypeAndVolume(x, y, z, Type, 0.5f + ((float)Volume / (float)FluidData.MaxVolume) * 0.5f);

            return true;
        }

        public bool IsSolid(FluidInformation Info)
        {
            return Type != 0 && Type != Info.VoxelID;
        }

        public bool IsEmpty()
        {
            return Type == 0 || Volume < TerrainInformation.Isolevel;
        }

        public void Evaporate(FluidInformation Info)
        {
            if (Bottom != null && Bottom.IsSolid(Info))
            {
                if (Volume < Info.Evaporation)
                {
                    if (Type == Info.VoxelID)
                    {
                        Dirty = true;
                        Volume = 0;
                        Type = 0;
                    }
                }
            }
        }

        public void Flow(FluidInformation Info, int ChunkId, bool IgnoreZ = false)
        {
            if (Data == null || Type != FluidType)
                return;

            FlowBottom(this);

            if (Volume == FluidData.RiverVolume)
            {
                Info.Constructor.OnCascade(this, ChunkId);
                return;
            }

            if (Volume > 0)
            {
                FluidDirections FlowDirections = FluidDirections.NONE;

                if (Left != null && (Left.Type == 0 || Left.Type == Type) && Left.Volume < Volume - Info.Viscosity && Left.Data != null)
                    FlowDirections |= FluidDirections.LEFT;

                if (Right != null && (Right.Type == 0 || Right.Type == Type) && Right.Volume < Volume - Info.Viscosity && Right.Data != null)
                    FlowDirections |= FluidDirections.RIGHT;

                if (!IgnoreZ)
                {
                    if (Forward != null && (Forward.Type == 0 || Forward.Type == Type) && Forward.Volume < Volume - Info.Viscosity && Forward.Data != null)
                        FlowDirections |= FluidDirections.FORWARD;

                    if (Backward != null && (Backward.Type == 0 || Backward.Type == Type) && Backward.Volume < Volume - Info.Viscosity && Backward.Data != null)
                        FlowDirections |= FluidDirections.BACKWARD;
                }

                if (FlowDirections == FluidDirections.NONE)
                    Direction = FluidDirections.NONE;
                else
                    FlowDirection[(int)FlowDirections](this);
            }
        }

        public void Set(int Volume, FluidDirections Direction)
        {
            if (this.Volume != Volume)
            {
                this.Volume = Volume;
                this.Dirty = true;
            }

            this.Direction = Direction;
        }

        static public bool FlowBottom(FluidTempBlock Fluid)
        {
            if (Fluid.Bottom != null && Fluid.Bottom.Data != null && Fluid.Volume != 0)
            {
                if (Fluid.Bottom.Volume < FluidData.MaxVolume && (Fluid.Bottom.Type == 0 || Fluid.Bottom.Type == Fluid.Type))
                {
                    int amountToMove = Math.Min(FluidData.MaxVolume - Fluid.Bottom.Volume, Fluid.Volume);
                    Fluid.Bottom.Set(Fluid.Bottom.Volume + amountToMove, FluidDirections.BOTTOM);
                    Fluid.Set(Fluid.Volume - amountToMove, FluidDirections.BOTTOM);
                    return true;
                }
            }
            return false;
        }

        public FluidTempBlock GetEmptyAround(ref float Orientation)
        {
            if (Left != null && Left.Type == 0 && Left.Data != null)
            {
                if (Left.Bottom != null && Left.Bottom.Type == 0 && Left.Bottom.Data != null)
                {
                    Orientation = -90;
                    return Left;
                }
            }

            if (Right != null && Right.Type == 0 && Right.Data != null)
            {
                if (Right.Bottom != null && Right.Bottom.Type == 0 && Right.Bottom.Data != null)
                {
                    Orientation = 90;
                    return Right;
                }
            }

            if (Forward != null && Forward.Type == 0 && Forward.Data != null)
            {
                if (Forward.Bottom != null && Forward.Bottom.Type == 0 && Forward.Bottom.Data != null)
                {
                    Orientation = 0;
                    return Forward;
                }
            }

            if (Backward != null && Backward.Type == 0 && Backward.Data != null)
            {
                if (Backward.Bottom != null && Backward.Bottom.Type == 0 && Backward.Bottom.Data != null)
                {
                    Orientation = -180;
                    return Backward;
                }
            }

            return null;
        }

        #region OneDirection

        static public void FlowLeft(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Left.Volume + Fluid.Volume) / 2;
            int remainder = (Fluid.Left.Volume + Fluid.Volume) % 2;

            Fluid.Left.Set(amountToSpread + remainder, FluidDirections.LEFT);
            Fluid.Set(amountToSpread, Fluid.Direction);
        }

        static public void FlowRight(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Right.Volume + Fluid.Volume) / 2;
            int remainder = (Fluid.Right.Volume + Fluid.Volume) % 2;

            Fluid.Right.Set(amountToSpread + remainder, FluidDirections.RIGHT);
            Fluid.Set(amountToSpread, Fluid.Direction);
        }

        static public void FlowForward(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Forward.Volume + Fluid.Volume) / 2;
            int remainder = (Fluid.Forward.Volume + Fluid.Volume) % 2;

            Fluid.Forward.Set(amountToSpread + remainder, FluidDirections.FORWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);
        }

        static public void FlowBackward(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Backward.Volume + Fluid.Volume) / 2;
            int remainder = (Fluid.Backward.Volume + Fluid.Volume) % 2;

            Fluid.Backward.Set(amountToSpread + remainder, FluidDirections.BACKWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);
        }

        #endregion

        #region TwoDirections

        static public void FlowLeftRight(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Left.Volume + Fluid.Right.Volume + Fluid.Volume) / 3;
            int remainder = (Fluid.Left.Volume + Fluid.Right.Volume + Fluid.Volume) % 3;

            Fluid.Left.Set(amountToSpread, FluidDirections.LEFT);
            Fluid.Right.Set(amountToSpread, FluidDirections.RIGHT);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.LEFT)
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
                else if (Fluid.Direction == FluidDirections.RIGHT)
                    Fluid.Right.Set(Fluid.Right.Volume + remainder, FluidDirections.RIGHT);
                else
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
            }
        }

        static public void FlowForwardBackward(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Forward.Volume + Fluid.Backward.Volume + Fluid.Volume) / 3;
            int remainder = (Fluid.Forward.Volume + Fluid.Backward.Volume + Fluid.Volume) % 3;

            Fluid.Forward.Set(amountToSpread, FluidDirections.FORWARD);
            Fluid.Backward.Set(amountToSpread, FluidDirections.BACKWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.FORWARD)
                    Fluid.Forward.Set(Fluid.Forward.Volume + remainder, FluidDirections.FORWARD);
                else if (Fluid.Direction == FluidDirections.RIGHT)
                    Fluid.Backward.Set(Fluid.Backward.Volume + remainder, FluidDirections.BACKWARD);
                else
                    Fluid.Forward.Set(Fluid.Forward.Volume + remainder, FluidDirections.FORWARD);
            }
        }

        static public void FlowForwardLeft(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Left.Volume + Fluid.Forward.Volume + Fluid.Volume) / 3;
            int remainder = (Fluid.Left.Volume + Fluid.Forward.Volume + Fluid.Volume) % 3;

            Fluid.Left.Set(amountToSpread, FluidDirections.LEFT);
            Fluid.Forward.Set(amountToSpread, FluidDirections.FORWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.LEFT)
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
                else if (Fluid.Direction == FluidDirections.FORWARD)
                    Fluid.Forward.Set(Fluid.Forward.Volume + remainder, FluidDirections.FORWARD);
                else
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
            }
        }

        static public void FlowForwardRight(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Right.Volume + Fluid.Forward.Volume + Fluid.Volume) / 3;
            int remainder = (Fluid.Right.Volume + Fluid.Forward.Volume + Fluid.Volume) % 3;

            Fluid.Right.Set(amountToSpread, FluidDirections.RIGHT);
            Fluid.Forward.Set(amountToSpread, FluidDirections.FORWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.RIGHT)
                    Fluid.Right.Set(Fluid.Right.Volume + remainder, FluidDirections.RIGHT);
                else if (Fluid.Direction == FluidDirections.FORWARD)
                    Fluid.Forward.Set(Fluid.Forward.Volume + remainder, FluidDirections.FORWARD);
                else
                    Fluid.Right.Set(Fluid.Right.Volume + remainder, FluidDirections.RIGHT);
            }
        }

        static public void FlowBackwardLeft(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Left.Volume + Fluid.Backward.Volume + Fluid.Volume) / 3;
            int remainder = (Fluid.Left.Volume + Fluid.Backward.Volume + Fluid.Volume) % 3;

            Fluid.Left.Set(amountToSpread, FluidDirections.LEFT);
            Fluid.Backward.Set(amountToSpread, FluidDirections.BACKWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.LEFT)
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
                else if (Fluid.Direction == FluidDirections.BACKWARD)
                    Fluid.Backward.Set(Fluid.Backward.Volume + remainder, FluidDirections.BACKWARD);
                else
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
            }
        }

        static public void FlowBackwardRight(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Right.Volume + Fluid.Backward.Volume + Fluid.Volume) / 3;
            int remainder = (Fluid.Right.Volume + Fluid.Backward.Volume + Fluid.Volume) % 3;

            Fluid.Right.Set(amountToSpread, FluidDirections.RIGHT);
            Fluid.Backward.Set(amountToSpread, FluidDirections.BACKWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.RIGHT)
                    Fluid.Right.Set(Fluid.Right.Volume + remainder, FluidDirections.RIGHT);
                else if (Fluid.Direction == FluidDirections.BACKWARD)
                    Fluid.Backward.Set(Fluid.Backward.Volume + remainder, FluidDirections.BACKWARD);
                else
                    Fluid.Right.Set(Fluid.Right.Volume + remainder, FluidDirections.RIGHT);
            }
        }

        #endregion

        #region 3Directions

        static public void FlowLeftRightForward(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Left.Volume + Fluid.Right.Volume + Fluid.Forward.Volume + Fluid.Volume) / 4;
            int remainder = (Fluid.Left.Volume + Fluid.Right.Volume + Fluid.Forward.Volume + Fluid.Volume) % 4;

            Fluid.Left.Set(amountToSpread, FluidDirections.LEFT);
            Fluid.Right.Set(amountToSpread, FluidDirections.RIGHT);
            Fluid.Forward.Set(amountToSpread, FluidDirections.FORWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.LEFT)
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
                else if (Fluid.Direction == FluidDirections.RIGHT)
                    Fluid.Right.Set(Fluid.Right.Volume + remainder, FluidDirections.RIGHT);
                else if (Fluid.Direction == FluidDirections.FORWARD)
                    Fluid.Forward.Set(Fluid.Forward.Volume + remainder, FluidDirections.FORWARD);
                else
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
            }
        }

        static public void FlowLeftRightBackward(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Left.Volume + Fluid.Right.Volume + Fluid.Backward.Volume + Fluid.Volume) / 4;
            int remainder = (Fluid.Left.Volume + Fluid.Right.Volume + Fluid.Backward.Volume + Fluid.Volume) % 4;

            Fluid.Left.Set(amountToSpread, FluidDirections.LEFT);
            Fluid.Right.Set(amountToSpread, FluidDirections.RIGHT);
            Fluid.Backward.Set(amountToSpread, FluidDirections.BACKWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.LEFT)
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
                else if (Fluid.Direction == FluidDirections.RIGHT)
                    Fluid.Right.Set(Fluid.Right.Volume + remainder, FluidDirections.RIGHT);
                else if (Fluid.Direction == FluidDirections.BACKWARD)
                    Fluid.Backward.Set(Fluid.Backward.Volume + remainder, FluidDirections.BACKWARD);
                else
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
            }
        }

        static public void FlowForwardBackwardLeft(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Left.Volume + Fluid.Forward.Volume + Fluid.Backward.Volume + Fluid.Volume) / 4;
            int remainder = (Fluid.Left.Volume + Fluid.Forward.Volume + Fluid.Backward.Volume + Fluid.Volume) % 4;

            Fluid.Left.Set(amountToSpread, FluidDirections.LEFT);
            Fluid.Forward.Set(amountToSpread, FluidDirections.FORWARD);
            Fluid.Backward.Set(amountToSpread, FluidDirections.BACKWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.LEFT)
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
                else if (Fluid.Direction == FluidDirections.FORWARD)
                    Fluid.Forward.Set(Fluid.Forward.Volume + remainder, FluidDirections.FORWARD);
                else if (Fluid.Direction == FluidDirections.BACKWARD)
                    Fluid.Backward.Set(Fluid.Forward.Volume + remainder, FluidDirections.BACKWARD);
                else
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
            }
        }

        static public void FlowForwardBackwardRight(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Right.Volume + Fluid.Forward.Volume + Fluid.Backward.Volume + Fluid.Volume) / 4;
            int remainder = (Fluid.Right.Volume + Fluid.Forward.Volume + Fluid.Backward.Volume + Fluid.Volume) % 4;

            Fluid.Right.Set(amountToSpread, FluidDirections.RIGHT);
            Fluid.Forward.Set(amountToSpread, FluidDirections.FORWARD);
            Fluid.Backward.Set(amountToSpread, FluidDirections.BACKWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.RIGHT)
                    Fluid.Right.Set(Fluid.Right.Volume + remainder, FluidDirections.RIGHT);
                else if (Fluid.Direction == FluidDirections.FORWARD)
                    Fluid.Forward.Set(Fluid.Forward.Volume + remainder, FluidDirections.FORWARD);
                else if (Fluid.Direction == FluidDirections.BACKWARD)
                    Fluid.Backward.Set(Fluid.Forward.Volume + remainder, FluidDirections.BACKWARD);
                else
                    Fluid.Forward.Set(Fluid.Forward.Volume + remainder, FluidDirections.FORWARD);
            }
        }

        #endregion

        static public void FlowAll(FluidTempBlock Fluid)
        {
            int amountToSpread = (Fluid.Left.Volume + Fluid.Right.Volume + Fluid.Forward.Volume + Fluid.Backward.Volume + Fluid.Volume) / 5;
            int remainder = (Fluid.Left.Volume + Fluid.Right.Volume + Fluid.Forward.Volume + Fluid.Backward.Volume + Fluid.Volume) % 5;

            Fluid.Left.Set(amountToSpread, FluidDirections.LEFT);
            Fluid.Right.Set(amountToSpread, FluidDirections.RIGHT);
            Fluid.Forward.Set(amountToSpread, FluidDirections.FORWARD);
            Fluid.Backward.Set(amountToSpread, FluidDirections.BACKWARD);
            Fluid.Set(amountToSpread, Fluid.Direction);

            if (remainder != 0)
            {
                if (Fluid.Direction == FluidDirections.LEFT)
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.LEFT);
                else if (Fluid.Direction == FluidDirections.RIGHT)
                    Fluid.Right.Set(Fluid.Right.Volume + remainder, FluidDirections.RIGHT);
                else if (Fluid.Direction == FluidDirections.FORWARD)
                    Fluid.Forward.Set(Fluid.Forward.Volume + remainder, FluidDirections.FORWARD);
                else if (Fluid.Direction == FluidDirections.BACKWARD)
                    Fluid.Backward.Set(Fluid.Forward.Volume + remainder, FluidDirections.BACKWARD);
                else
                    Fluid.Left.Set(Fluid.Left.Volume + remainder, FluidDirections.FORWARD);
            }
        }
    }

    public class FluidInterpolation : IPoolable
    {
        public int SizeX, SizeY, SizeZ = 0;
        public FluidTempBlock[] Temp;
        public FluidDatas Data;

        public FluidTempBlock this[int x, int y, int z]
        {
            get
            {
                return Temp[x * SizeY * SizeZ + y * SizeZ + z];
            }
            set
            {
                Temp[x * SizeY * SizeZ + y * SizeZ + z] = value;
            }
        }

        public void Reset(int SizeX, int SizeY, int SizeZ)
        {
            if (Temp == null || this.SizeX != SizeX || this.SizeY != SizeY || this.SizeZ != SizeZ)
            {
                this.SizeX = SizeX;
                this.SizeY = SizeY;
                this.SizeZ = SizeZ;

                Temp = new FluidTempBlock[SizeX * SizeY * SizeZ];

                for (int i = Temp.Length - 1; i >= 0; --i)
                    Temp[i] = new FluidTempBlock();

                RelinkBlock();
            }

            for (int i = Temp.Length - 1; i >= 0; --i)
                Temp[i].Clear();
        }

        public void RelinkBlock()
        {
            FluidTempBlock Info = null;
            int x, y, z, f=0;
            for (x = 0; x < SizeX; ++x)
            {
                for (y = 0; y < SizeY; ++y)
                {
                    for (z = 0; z < SizeZ; ++z,++f)
                    {
                        Info = Temp[f];

                        if (x != 0)
                            Info.Left = this[x - 1, y, z];

                        if (x < SizeX - 1)
                            Info.Right = this[x + 1, y, z];

                        if (y != 0)
                            Info.Bottom = this[x, y - 1, z];

                        if (z < SizeZ - 1)
                            Info.Forward = this[x, y, z + 1];

                        if (z != 0)
                            Info.Backward = this[x, y, z - 1];

                    }
                }
            }
        }

        public override void Clear()
        {

        }
    }
}
