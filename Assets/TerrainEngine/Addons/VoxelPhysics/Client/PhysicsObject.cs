﻿using System;
using UnityEngine;

namespace TerrainEngine
{
    public class PhysicsObject
    {
        static PoolInfoRef ArrayPool;

        public struct PhysicsBoxCollider
        {
            public VectorU3 Position;
            public VectorU3 Size;
        }

        public Vector3 WorldPosition;   // Unity Position Where this object will be instancied
        public SList<VectorI3> Voxels = new SList<VectorI3>(30);
        public SList<PhysicsBoxCollider> Colliders = new SList<PhysicsBoxCollider>(30);
        public IMeshData ObjectMesh;     // Mesh Data (Vertices,Indices,etc)
        public VectorI3 Min = new VectorI3(int.MaxValue,int.MaxValue,int.MaxValue), Max = new VectorI3(int.MinValue,int.MinValue,int.MinValue);

        public bool Combine(PhysicsObject Obj, int MaxSize)
        {
            if (Obj.Voxels.Count + Voxels.Count >= MaxSize)
                return false;

            for (int i = 0; i < Voxels.Count; ++i)
            {
                if (Obj.IsConnected(ref Voxels.array[i]))
                {
                    Voxels.CheckArray(Obj.Voxels.Count);
                    for (int j = 0; j < Obj.Voxels.Count; ++j)
                        AddVoxelSafe(Obj.Voxels.array[j]);
                    return true;
                }
            }

            return false;
        }

        public void GetSize(ref VectorI3 Size)
        {
            Size.x = Max.x - Min.x + 2;
            Size.y = Max.y - Min.y + 2;
            Size.z = Max.z - Min.z + 2;
        }

        public void AddVoxel(VectorI3 Voxel)
        {
            Voxels.Add(Voxel);
            Min.x = Math.Min(Voxel.x, Min.x);
            Min.y = Math.Min(Voxel.y, Min.y);
            Min.z = Math.Min(Voxel.z, Min.z);

            Max.x = Math.Max(Voxel.x, Max.x);
            Max.y = Math.Max(Voxel.y, Max.y);
            Max.z = Math.Max(Voxel.z, Max.z);
        }

        public void AddVoxelSafe(VectorI3 Voxel)
        {
            Voxels.AddSafe(Voxel);
            Min.x = Math.Min(Voxel.x, Min.x);
            Min.y = Math.Min(Voxel.y, Min.y);
            Min.z = Math.Min(Voxel.z, Min.z);

            Max.x = Math.Max(Voxel.x, Max.x);
            Max.y = Math.Max(Voxel.y, Max.y);
            Max.z = Math.Max(Voxel.z, Max.z);
        }

        public bool IsConnected(ref VectorI3 Voxel)
        {
            if (Voxels.Count != 0)
            {
                VectorI3 Around;
                for (int i = Voxels.Count - 1; i >= 0; --i)
                {
                    Around = Voxels.array[i] - Voxel;
                    if (Math.Abs(Around.x) + Math.Abs(Around.y) + Math.Abs(Around.z) <= 1)
                        return true;
                }
            }

            return false;
        }

        public void Generate(ActionThread Thread, TerrainChunkGenerateGroupAction Action, TerrainBlock Block, PhysicsFlushUpdater FlushUpdater)
        {
            VectorI3 Size = new VectorI3();
            GetSize(ref Size);
            bool JustRemove = Voxels.Count < 3;

            int i = 0;
            int ProcessorsCount = TerrainManager.Instance.Processors.Count;

            MarchingVoxelArray VoxelArray = null;

            if (!JustRemove)
            {
                if (ArrayPool.Index == 0)
                    ArrayPool = PoolManager.Pool.GetPool("MarchingVoxelArray");

                VoxelArray = ArrayPool.GetData<MarchingVoxelArray>();
                VoxelArray.Init(Size.x + 3, Size.y + 3, Size.z + 3, 3, TerrainManager.Instance.Palette.DataCount);
            }

            MarchingInterpolationInfo Info;
            TerrainBlock Around = null;
            VectorI3 Voxel, Data;
            VoxelFlush Flush = new VoxelFlush();

            for (int j = Voxels.Count - 1; j >= 0; --j)
            {
                Data = Voxel = Voxels.array[j];

                Around = Block.GetAroundBlock(ref Voxel.x, ref Voxel.y, ref Voxel.z) as TerrainBlock;
                if (Around != null)
                {
                    Flush.Init(Around, Around.GetChunkIdFromVoxel(Voxel.x, Voxel.y, Voxel.z), (ushort)Voxel.x, (ushort)Voxel.y, (ushort)Voxel.z, 0, 0, 0, 0, VoxelModificationType.SEND_EVENTS);
                    if (!JustRemove)
                    {
                        Info = VoxelArray[(Data.x - Min.x + 2), (Data.y - Min.y + 2), (Data.z - Min.z + 2)];
                        Info.ClearIndices();
                        Around.Voxels.GetTypeAndVolume(Voxel.x, Voxel.y, Voxel.z, ref Info.VType, ref Info.Volume);

                        Info.VoxelType = TerrainManager.Instance.Palette.GenericPalette[Info.VType].VoxelType;
                        if (Info.VoxelType == VoxelTypes.FLUID)
                        {
                            Info.VType = 0;
                            Info.Volume = 0;
                            Info.Full = false;
                        }
                        else
                        {
                            Info.Full = true;
                            Info.Info = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(Info.VType);
                            if (Info.Info.UseSplatMap)
                            {
                                if(Info.Info.MainVoxelId != 0)
                                    Info.VType = Info.Info.MainVoxelId;
                            }

                            VoxelArray.ActiveMaterial(Info.VType);
                            FlushUpdater.ContainsOrAddChunk(Around, Flush.ChunkId);

                            Flush.OldType = Info.VType;
                            Flush.OldVolume = Info.Volume;
                        }
                    }

                    ITerrainNetworkInterface.AddChunkVoxelToGenerate(Thread, Around, Flush.ChunkId, Voxel.x, Voxel.y, Voxel.z, Action.Chunks);

                    for (i = 0; i < ProcessorsCount; ++i)
                        TerrainManager.Instance.Processors[i].OnFlushVoxels(Thread, null, ref Flush, null);

                    Around.Voxels.SetTypeAndVolume(Voxel.x, Voxel.y, Voxel.z, 0, 0);
                }
            }


            if (!JustRemove)
            {
                TempMeshData MeshInfo = null;
                Block.Generate(Thread, ref MeshInfo, ref ObjectMesh, Vector3.zero, VoxelArray, false, true, true, false, true, false, false);
                if (MeshInfo != null)
                    MeshInfo.Close();

                if (ObjectMesh != null)
                {
                    if (ObjectMesh.IsValid())
                    {
                        WorldPosition = TerrainManager.Instance.Informations.GetVoxelWorldPosition(Block.LocalPosition, Min.x, Min.y, Min.z);

                        GenerateColliders(VoxelArray);

                        if (Colliders.Count == 0)
                        {
                            ObjectMesh.Close();
                            ObjectMesh = null;
                        }
                    }
                    else
                    {
                        ObjectMesh.Close();
                        ObjectMesh = null;
                    }
                }

                VoxelArray.Close();
            }
        }

        public void GenerateColliders(MarchingVoxelArray Array)
        {
            // Generate Colliders Box
            PhysicsBoxCollider CurrentBoxCollider = new PhysicsBoxCollider();
            bool HasCollider = false;
            int px, py, pz, j = 0;
            VectorU3 Other = new VectorU3();
            bool IsValidBlock = false;
            MarchingInterpolationInfo Info = null;
            for (px = 0; px < Array.SizeX; ++px)
            {
                for (py = 0; py < Array.SizeY; ++py)
                {
                    for (pz = 0; pz < Array.SizeZ; ++pz)
                    {
                        Info = Array[px, py, pz];
                        IsValidBlock = Info.VType != 0 && Info.Volume >= TerrainInformation.Isolevel && (
                            (Info.Right == null || Info.Right.VType == 0)
                            || (Info.Left == null || Info.Left.VType == 0)
                            || (Info.Top == null || Info.Top.VType == 0)
                            || (Info.Bottom == null || Info.Bottom.VType == 0)
                            );

                        if (IsValidBlock)
                        {
                            if (HasCollider)
                                CurrentBoxCollider.Size.z += 1;
                            else
                            {
                                HasCollider = true;
                                CurrentBoxCollider.Position = new VectorU3(px, py, pz);
                                CurrentBoxCollider.Size = new VectorU3(1, 1, 1);
                            }
                        }
                        else
                        {
                            if (HasCollider)
                            {
                                for (j = 0; j < Colliders.Count; ++j)
                                {
                                    Other = Colliders.array[j].Position;
                                    if (Other.y == (CurrentBoxCollider.Position.y - 1)
                                        && Other.x == CurrentBoxCollider.Position.x
                                        && Other.z == CurrentBoxCollider.Position.z)
                                    {
                                        if (Colliders.array[j].Size.z == CurrentBoxCollider.Size.z)
                                        {
                                            Colliders.array[j].Size.y += 1;
                                            HasCollider = false;
                                            break;
                                        }
                                    }
                                }

                                if (HasCollider)
                                {
                                    Colliders.Add(CurrentBoxCollider);
                                }
                                HasCollider = false;
                            }
                        }
                    }
                }
            }

            int MaxCount = 500;
            while (CombineColliders() && MaxCount >= 0)
            {
                --MaxCount;
            }

        }

        public bool CombineColliders()
        {
            PhysicsBoxCollider CurrentBoxCollider = new PhysicsBoxCollider();
            VectorU3 Other = new VectorU3();
            int j, i;
            bool HasCombined = false;
            // Check Collider in X
            for (j = 0; j < Colliders.Count; ++j)
            {
                CurrentBoxCollider = Colliders.array[j];

                for (i = 0; i < Colliders.Count; ++i)
                {
                    Other = Colliders.array[i].Position;

                    if (Other.x == CurrentBoxCollider.Position.x - Colliders.array[i].Size.x && Other.y == CurrentBoxCollider.Position.y && Other.z == CurrentBoxCollider.Position.z)
                    {
                        if (Colliders.array[i].Size.y == CurrentBoxCollider.Size.y && Colliders.array[i].Size.z == CurrentBoxCollider.Size.z)
                        {
                            Colliders.array[i].Size.x += CurrentBoxCollider.Size.x;
                            Colliders.RemoveAt(j);
                            HasCombined = true;
                            --j;
                            break;
                        }
                    }
                }
            }

            //Check Collider in Y
            for (j = 0; j < Colliders.Count; ++j)
            {
                CurrentBoxCollider = Colliders.array[j];

                for (i = 0; i < Colliders.Count; ++i)
                {
                    Other = Colliders.array[i].Position;

                    if (Other.y == CurrentBoxCollider.Position.y - Colliders.array[i].Size.y && Other.x == CurrentBoxCollider.Position.x && Other.z == CurrentBoxCollider.Position.z)
                    {
                        if (Colliders.array[i].Size.x == CurrentBoxCollider.Size.x && Colliders.array[i].Size.z == CurrentBoxCollider.Size.z)
                        {
                            Colliders.array[i].Size.y += CurrentBoxCollider.Size.y;
                            Colliders.RemoveAt(j);
                            HasCombined = true;
                            --j;
                            break;
                        }
                    }
                }
            }

            return HasCombined;
        }

        public void CreateGameObject()
        {
            if (ObjectMesh == null)
                return;

            Debug.Log("Create Physics Object Materials:" + ObjectMesh.Materials.Length);
            // Disable Chunks Colliders on Object Position

            GameObject Obj = new GameObject();
            Transform ObjTransform = Obj.transform;
            Obj.name = "PhysicsObject";
            Obj.transform.position = WorldPosition - Vector3.one * TerrainManager.Instance.Informations.Scale;
            TerrainObject TObj = Obj.AddComponent<TerrainObject>();
            TObj.DestroyIfNoTerrain = true;
            Obj.AddComponent<TerrainObjectColliderViewer>().TObject = TObj;

            MeshFilter Filter = Obj.AddComponent<MeshFilter>();
            MeshRenderer Renderer = Obj.AddComponent<MeshRenderer>();

            PhysicsBoxCollider PhysicsCollider;
            float Mass = 1;
            GameObject Col;
            Transform TR;
            Vector3 position = new Vector3();
            Vector3 size = new Vector3();
            for (int i = Colliders.Count - 1; i >= 0; --i)
            {
                Col = new GameObject();
                TR = Col.transform;

                PhysicsCollider = Colliders.array[i];
                TR.parent = ObjTransform;

                size.x = (PhysicsCollider.Size.x * TerrainManager.Instance.Informations.Scale) * 0.75f;
                size.y = (PhysicsCollider.Size.y * TerrainManager.Instance.Informations.Scale) * 0.75f;
                size.z = (PhysicsCollider.Size.z * TerrainManager.Instance.Informations.Scale) * 0.75f;

                position.x = ((PhysicsCollider.Position.x * TerrainManager.Instance.Informations.Scale) + size.x * 0.5f) - (1.5f * TerrainManager.Instance.Informations.Scale);
                position.y = ((PhysicsCollider.Position.y * TerrainManager.Instance.Informations.Scale) + size.y * 0.5f) - (1.5f * TerrainManager.Instance.Informations.Scale);
                position.z = ((PhysicsCollider.Position.z * TerrainManager.Instance.Informations.Scale) + size.z * 0.5f) - (1.5f * TerrainManager.Instance.Informations.Scale);

                Col.AddComponent<BoxCollider>().size = size;
                TR.localPosition = position;

                Mass += size.x * size.y * size.z * 230f;
            }
            Rigidbody Body = Obj.AddComponent<Rigidbody>();
            Body.mass = Mass;
            Body.angularDrag = 5;

            // Apply Mesh
            Mesh CMesh = new Mesh();
            CMesh.vertices = ObjectMesh.Vertices;
            CMesh.colors = ObjectMesh.Colors;
            CMesh.subMeshCount = ObjectMesh.Indices.Length;
            for (int i = ObjectMesh.Indices.Length - 1; i >= 0; --i)
                CMesh.SetIndices(ObjectMesh.Indices[i], MeshTopology.Triangles, i);

                CMesh.RecalculateNormals();

            // Replace Shader by LocalShader if exist.
            Material[] Materials = new Material[ObjectMesh.Materials.Length];
            for (int i = Materials.Length - 1; i >= 0; --i)
            {
                if (ObjectMesh.Materials[i] == null)
                {
                    Debug.LogError("Invalid material Index: " + i + ",Indices:" + ObjectMesh.Indices.Length);
                }
                else
                {
                    Materials[i] = new Material(ObjectMesh.Materials[i]);

                    Shader LocalShader = Shader.Find(ObjectMesh.Materials[i].shader.name + "Local");
                    if (LocalShader != null)
                    {
                        Materials[i].shader = LocalShader;
                    }


                    Materials[i].DisableKeyword("_TE_LOCAL");
                    Materials[i].EnableKeyword("_TE_LOCAL");

                    if (Materials[i].HasProperty("_VerticalOffset"))
                        Materials[i].SetFloat("_VerticalOffset", Obj.transform.position.y);
                }
            }
            Renderer.sharedMaterials = Materials;
            Filter.sharedMesh = CMesh;
        }
    }
}
