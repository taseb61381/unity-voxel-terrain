﻿using System;
using System.Collections.Generic;

namespace TerrainEngine
{
    public class VoxelPhysicsProcessor : IBlockProcessor
    {
        static public PoolInfoRef PhysicsUpdaterPool = new PoolInfoRef();
        static public PoolInfoRef PhysicsFlushUpdaterPool = new PoolInfoRef();

        /// <summary>
        /// Contains all current working physics. One physics = one flush call
        /// </summary>
        public Dictionary<int, PhysicsFlushUpdater> PhysicsUpdaters = new Dictionary<int, PhysicsFlushUpdater>(10);

        /// <summary>
        /// Init Pools and Internal Name
        /// </summary>
        /// <param name="Manager"></param>
        public override void OnInitManager(TerrainManager Manager)
        {
            PhysicsUpdaterPool = PoolManager.Pool.GetPool("PhysicsUpdater");
            PhysicsFlushUpdaterPool = PoolManager.Pool.GetPool("PhysicsFlushUpdate");
            InternalDataName = "Physics-" + DataName;
            base.OnInitManager(Manager);
        }

        public PhysicsFlushUpdater GetPhysicsFlushUpdater(int Id,bool Create = false)
        {
            PhysicsFlushUpdater Updater = null;
            if(!PhysicsUpdaters.TryGetValue(Id,out Updater) && Create)
            {
                Updater = PhysicsFlushUpdaterPool.GetData<PhysicsFlushUpdater>();
                PhysicsUpdaters.Add(Id, Updater);
            }
            return Updater;
        }

        public override void OnFlushVoxels(ActionThread Thread, TerrainObject Obj, ref VoxelFlush Voxel, TerrainChunkGenerateGroupAction Action)
        {
            if (Action != null && Voxel.ModType != VoxelModificationType.SEND_EVENTS)
            {
                if ((Voxel.OldType != 0 && Voxel.NewType == 0) || (Voxel.OldVolume >= TerrainInformation.Isolevel && Voxel.NewVolume < TerrainInformation.Isolevel+0.001f))
                {
                    if(Action.PhysicsFlush == null)
                        Action.PhysicsFlush = GetPhysicsFlushUpdater(Action.FlushId, true);

                    Action.PhysicsFlush.OnVoxelFlush(ref Voxel);
                }
            }
        }

        public override IAction OnVoxelFlushDone(ActionThread Thread, TerrainChunkGenerateGroupAction Action)
        {
            if (Action != null && Action.PhysicsFlush != null)
            {
                PhysicsUpdaters.Remove(Action.FlushId);
                Action.PhysicsFlush.OnVoxelFlushDone(Thread);
                Action.OnActionEvent += Action.PhysicsFlush.OnChunksGenerate;
                Action.OnActionEvent += Action.PhysicsFlush.OnChunksBuild;
            }
            return null;
        }
    }
}
