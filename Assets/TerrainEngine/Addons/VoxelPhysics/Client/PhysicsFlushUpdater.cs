﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    public class PhysicsFlushUpdater : IPoolable
    {
        public struct WaitingVoxel
        {
            public VoxelToCheck ToCheck;
            public VectorI3 VoxelWorldPosition;
        }

        public struct VoxelToCheck
        {
            public ushort BlockId, x, y, z;

            public VoxelToCheck(ushort BlockId, ushort x, ushort y, ushort z)
            {
                this.BlockId = BlockId;
                this.x = x;
                this.y = y;
                this.z = z;
            }

            public VectorI3 Voxel
            {
                get
                {
                    return new VectorI3(x, y, z);
                }
            }

            public override string ToString()
            {
                return BlockId + "," + x + "," + y + "," + z;
            }
        }

        public struct PhysicsBoxCollider
        {
            public VectorU3 Position;
            public VectorU3 Size;
        }

        public SList<VoxelToCheck> FlushedVoxels = new SList<VoxelToCheck>(500); // Current Flushed Voxel. This list contains all modified voxels
        public SList<WaitingVoxel> WaitingFlush = new SList<WaitingVoxel>(2000); // This list contains all around voxel flushed. example if voxel 1,1,1 is modified, this list will contains 27 voxels : 0,0,0:0,0,1,etc... except 1,1,1
        public SList<VoxelToCheck> ToCheck = new SList<VoxelToCheck>(50);
        public SList<KeyValuePair<TerrainBlock, int>> Chunks = new SList<KeyValuePair<TerrainBlock, int>>(3); // Chunks where PhysicsObject will be instancied. Chunks colliders will be generated instantly
        public VoxelPhysicsUpdater Updater;
        public List<PhysicsObject> PhysicsObjects = new List<PhysicsObject>();
        public int MaxObjectSize = 50;
        public int MaxPressure = 2000;

        public void OnVoxelFlush(ref VoxelFlush Voxel)
        {
            FlushedVoxels.Add(new VoxelToCheck(Voxel.Block.BlockId, Voxel.x, Voxel.y, Voxel.z));
        }

        public bool ContainsFlush(ushort BlockId, int x, int y, int z)
        {
            for (int i = FlushedVoxels.Count - 1; i >= 0; --i)
            {
                if (FlushedVoxels.array[i].x == x
                    && FlushedVoxels.array[i].y == y
                    && FlushedVoxels.array[i].z == z
                    && FlushedVoxels.array[i].BlockId == BlockId)
                    return true;
            }

            return false;
        }

        public bool ContainsWaitingFlush(ushort BlockId, int x, int y, int z)
        {
            for (int i = WaitingFlush.Count - 1; i >= 0; --i)
            {
                if (WaitingFlush.array[i].ToCheck.x == x
                    && WaitingFlush.array[i].ToCheck.y == y
                    && WaitingFlush.array[i].ToCheck.z == z
                    && WaitingFlush.array[i].ToCheck.BlockId == BlockId)
                    return true;
            }

            return false;
        }

        public bool ContainsConnectedWaitingFlush(ref VectorI3 WorldVoxel)
        {
            VectorI3 ExistingWorldVoxel;
            for (int i = WaitingFlush.Count-1; i >= 0; --i)
            {
                ExistingWorldVoxel = WaitingFlush.array[i].VoxelWorldPosition - WorldVoxel;
                if (Math.Abs(ExistingWorldVoxel.x) + Math.Abs(ExistingWorldVoxel.y) + Math.Abs(ExistingWorldVoxel.z) <= 1)
                    return true;
            }

            return false;
        }

        public void OnVoxelFlushDone(ActionThread Thread)
        {
            if (ThreadManager.EnableStats)
                Thread.RecordTime();

            VoxelToCheck CurrentVoxel;
            VoxelToCheck ToCheckVoxel = new VoxelToCheck();
            WaitingVoxel Waiting = new WaitingVoxel();
            TerrainBlock AroundBlock = null;

            int x, y, z;
            int tx, ty, tz;
            for (int i = FlushedVoxels.Count-1; i >= 0; --i)
            {
                CurrentVoxel = FlushedVoxels.array[i];

                for (x = -1; x <= 1; ++x)
                {
                    for (y = -1; y <= 1; ++y)
                    {
                        for (z = -1; z <= 1; ++z)
                        {
                            if (x != 0 || y != 0 || z != 0)
                            {
                                tx = CurrentVoxel.x + x;
                                ty = CurrentVoxel.y + y;
                                tz = CurrentVoxel.z + z;

                                if (AroundBlock == null || AroundBlock.BlockId != CurrentVoxel.BlockId)
                                    AroundBlock = TerrainManager.Instance.Container.BlocksArray[CurrentVoxel.BlockId] as TerrainBlock;

                                AroundBlock = AroundBlock.GetAroundBlock(ref tx, ref ty, ref tz) as TerrainBlock;

                                if (AroundBlock == null || !AroundBlock.HasVolume(tx, ty, tz, true))
                                    continue;

                                if (ContainsWaitingFlush(AroundBlock.BlockId, tx, ty, tz))
                                    continue;

                                ToCheckVoxel.BlockId = AroundBlock.BlockId;
                                ToCheckVoxel.x = (ushort)tx;
                                ToCheckVoxel.y = (ushort)ty;
                                ToCheckVoxel.z = (ushort)tz;
                                Waiting.ToCheck = ToCheckVoxel;
                                Waiting.VoxelWorldPosition = TerrainManager.Instance.Informations.GetWorldVoxel(AroundBlock.LocalPosition, tx, ty, tz);
                                if (!ContainsConnectedWaitingFlush(ref Waiting.VoxelWorldPosition))
                                {
                                    ToCheck.Add(ToCheckVoxel);
                                }
                                WaitingFlush.Add(Waiting);
                            }
                        }
                    }
                }
            }

            if (ThreadManager.EnableStats)
                Thread.AddStats("PhysicsFlushDone", Thread.Time());
        }

        public bool ContainsOrAddChunk(TerrainBlock Block, int ChunkId)
        {
            for (int i = Chunks.Count - 1; i >= 0; --i)
                if (Chunks.array[i].Value == ChunkId && Chunks.array[i].Key == Block)
                    return true;

            Chunks.Add(new KeyValuePair<TerrainBlock, int>(Block, ChunkId));
            return false;
        }

        public void CheckDisconnectedVoxels(ActionThread Thread, TerrainChunkGenerateGroupAction Action)
        {
            if (ThreadManager.EnableStats)
                Thread.RecordTime();

            VoxelToCheck Flush;
            ITerrainBlock Block = null;

            for (int i = 0; i < ToCheck.Count;++i)
            {
                if (Updater == null)
                {
                    Updater = VoxelPhysicsProcessor.PhysicsUpdaterPool.GetData<VoxelPhysicsUpdater>();
                }

                Flush = ToCheck.array[i];

                if (Block == null || Block.BlockId != Flush.BlockId)
                    Block = TerrainManager.Instance.Container.BlocksArray[Flush.BlockId];

                if (Block.HasVolume(Flush.x, Flush.y, Flush.z, true))
                {
                    Updater.Init(Block, Flush.Voxel, MaxPressure, false);
                    Updater.Update();

                    if (Updater.IsDone && !Updater.IsConnected)
                    {
                        RemoveDisconnectVoxels(Thread, Action, Updater);
                    }
                }

                Updater.Close();
                Updater = null;
            }

            if (ThreadManager.EnableStats)
                Thread.AddStats("PhysicsCheckDisconnectedVoxels", Thread.Time());
        }

        public void AddVoxelToObject(VectorI3 Voxel, int MaxSize)
        {
            if (PhysicsObjects.Count != 0)
            {
                PhysicsObjects[0].AddVoxel(Voxel);
                return;
            }

            for (int i = PhysicsObjects.Count - 1; i >= 0; --i)
            {
                if (PhysicsObjects[i].Voxels.Count < MaxSize && PhysicsObjects[i].IsConnected(ref Voxel))
                {
                    PhysicsObjects[i].AddVoxel(Voxel);
                    return;
                }
            }

            PhysicsObject Object = new PhysicsObject();
            PhysicsObjects.Add(Object);
            Object.AddVoxel(Voxel);
        }

        public void RemoveDisconnectVoxels(ActionThread Thread, TerrainChunkGenerateGroupAction Action, VoxelPhysicsUpdater Updater)
        {
            if (Updater.CheckedVoxels.Count == 0)
                return;

            if (ThreadManager.EnableStats)
                Thread.RecordTime();

            int i = 0, j = 0;
            int ProcessorsCount = TerrainManager.Instance.Processors.Count;

            int MaxSize = MaxObjectSize;

            for (i = Updater.CheckedVoxels.Count - 1; i >= 0; --i)
            {
                AddVoxelToObject(Updater.CheckedVoxels.array[i], MaxSize);
            }

            for (i = 0; i < PhysicsObjects.Count; ++i)
            {
                for (j = i + 1; j < PhysicsObjects.Count; ++j)
                {
                    if (PhysicsObjects[i].Combine(PhysicsObjects[j], MaxSize))
                    {
                        PhysicsObjects.RemoveAt(j);
                        j--;
                    }
                }
            }

            for (i = 0; i < PhysicsObjects.Count; ++i)
            {
                PhysicsObjects[i].Generate(Thread, Action, Updater.Block as TerrainBlock, this);
            }


            for (i = 0; i < ProcessorsCount; ++i)
                TerrainManager.Instance.Processors[i].OnVoxelFlushDone(Thread, null);

            if (ThreadManager.EnableStats)
                Thread.AddStats("PhysicsRemoveDisconnectedVoxels", Thread.Time());
        }

        /// <summary>
        /// Will find voxels in flush to check. If Voxel is not connected to ground, mesh will be generated and created in Build() function
        /// </summary>
        public void OnChunksGenerate(ActionThread Thread, IAction Action)
        {
            if (Action is TerrainChunkGenerateGroupAction)
            {
                CheckDisconnectedVoxels(Thread, Action as TerrainChunkGenerateGroupAction);
            }
        }

        /// <summary>
        /// This function will build generated mesh
        /// </summary>
        public void OnChunksBuild(ActionThread Thread, IAction Action)
        {
            if (Action is TerrainChunkBuildAction)
            {
                if ((Action as TerrainChunkBuildAction).HasCalledEvents)
                {
                    int ProcessorCount = TerrainManager.Instance.Processors.Count;
                    int i = 0, j =0;

                    for (i = Chunks.Count-1; i >= 0; --i)
                    {
                        for (j = 0; j < ProcessorCount; ++j)
                        {
                            TerrainManager.Instance.Processors[j].OnChunkModification(Chunks.array[i].Key, Chunks.array[i].Value);
                        }
                    }

                    for (i = 0; i < PhysicsObjects.Count; ++i)
                    {
                        PhysicsObjects[i].CreateGameObject();
                    }
                    Close();
                    // Here Spawn Generated Mesh
                }
            }
        }

        public override void Clear()
        {
            Chunks.Clear();
            WaitingFlush.Clear();
            FlushedVoxels.Clear();
            ToCheck.Clear();
            PhysicsObjects.Clear();
            Updater = null;
        }
    }
}
