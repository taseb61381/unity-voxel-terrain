﻿using System;
using System.Collections.Generic;

namespace TerrainEngine
{
    /// <summary>
    /// This class will check around voxel if is connected.
    /// CheckedVoxels contains list of all unconnected voxels
    /// </summary>
    public class VoxelPhysicsUpdater : IPoolable
    {
        public ITerrainBlock Block;
        public SList<VectorI3> CheckedVoxels = new SList<VectorI3>(200);
        public Queue<VectorI3> nodes = new Queue<VectorI3>(2000);
        public Queue<VectorI3> bottomNodes = new Queue<VectorI3>(2000);
        public Queue<VectorI3> topNodes = new Queue<VectorI3>(2000);

        public VectorI3 Voxel;
        public bool IsConnected;
        public bool IsDone;
        public int Pressure;
        public int MaxPressure;

        public void GetSize(out VectorI3 Min, out VectorI3 Max, out VectorI3 Size)
        {
            Min = new VectorI3(int.MaxValue, int.MaxValue, int.MaxValue);
            Max = new VectorI3(int.MinValue, int.MinValue, int.MinValue);

            int p;
            for (int i = 0; i < CheckedVoxels.Count; ++i)
            {
                p = CheckedVoxels.array[i].x;
                Min.x = Math.Min(p, Min.x);
                Max.x = Math.Max(p, Max.x);

                p = CheckedVoxels.array[i].y;
                Min.y = Math.Min(p, Min.y);
                Max.y = Math.Max(p, Max.y);

                p = CheckedVoxels.array[i].z;
                Min.z = Math.Min(p, Min.z);
                Max.z = Math.Max(p, Max.z);
            }

            Size.x = Max.x - Min.x + 2;
            Size.y = Max.y - Min.y + 2;
            Size.z = Max.z - Min.z + 2;
        }

        public void Init(ITerrainBlock Block, VectorI3 Voxel, int MaxPressure,bool CheckVolume)
        {
            this.Block = Block;
            this.Voxel = Voxel;
            this.IsConnected = false;
            this.IsDone = false;
            this.Pressure = 0;
            this.MaxPressure = MaxPressure;

            if (CheckVolume && !Block.HasVolume(Voxel.x, Voxel.y, Voxel.z))
                SetDone(true);
            else
                nodes.Enqueue(Voxel);
        }

        public override void Clear()
        {
            this.Block = null;
            this.CheckedVoxels.Clear();
            this.nodes.Clear();
            this.bottomNodes.Clear();
            this.topNodes.Clear();
        }

        public bool GetNextNode(ref VectorI3 current)
        {
            if (bottomNodes.Count != 0)
                current = bottomNodes.Dequeue();
            else if (nodes.Count != 0)
                current = nodes.Dequeue();
            else if (topNodes.Count != 0)
                current = topNodes.Dequeue();
            else
                return false;

            return true;
        }

        public bool HasChecked(int x, int y, int z)
        {
            for (int i = CheckedVoxels.Count - 1; i >= 0; --i)
            {
                if (CheckedVoxels.array[i].x == x && CheckedVoxels.array[i].z == z && CheckedVoxels.array[i].y == y)
                    return true;
            }

            return false;
        }

        public void Update()
        {
            if (IsDone)
                return;

            VectorI3 current = new VectorI3();
            VectorI3 VoxelData = Voxel;

            CheckedVoxels.Add(VoxelData);

            while (GetNextNode(ref current))
            {
                if (Block.IsUnderGround(current.x, current.y, current.z))
                {
                    SetDone(true);
                    return;
                }

                if (Pressure >= MaxPressure)
                {
                    SetDone(false);
                    return;
                }

                // 0,1,0
                if (!HasChecked(current.x, current.y + 1, current.z))
                {
                    if (Block.HasVolume(current.x, current.y + 1, current.z))
                    {
                        VoxelData.x = current.x;
                        VoxelData.y = current.y + 1;
                        VoxelData.z = current.z;
                        CheckedVoxels.Add(VoxelData);
                        topNodes.Enqueue(VoxelData);
                    }
                }

                // 0,-1,0
                if (!HasChecked(current.x, current.y-1, current.z))
                {
                    Pressure -= 8;
                    if (Block.HasVolume(current.x, current.y-1, current.z))
                    {
                        VoxelData.x = current.x;
                        VoxelData.y = current.y - 1;
                        VoxelData.z = current.z;
                        CheckedVoxels.Add(VoxelData);
                        bottomNodes.Enqueue(VoxelData);
                    }
                }

                // -1,0,0
                if (!HasChecked(current.x - 1, current.y, current.z))
                {
                    ++Pressure;
                    if (Block.HasVolume(current.x - 1, current.y, current.z))
                    {
                        VoxelData.x = current.x - 1;
                        VoxelData.y = current.y;
                        VoxelData.z = current.z;
                        CheckedVoxels.Add(VoxelData);
                        nodes.Enqueue(VoxelData);
                    }
                }

                // 1,0,0
                if (!HasChecked(current.x + 1, current.y, current.z))
                {
                    ++Pressure;
                    if (Block.HasVolume(current.x + 1, current.y, current.z))
                    {
                        VoxelData.x = current.x + 1;
                        VoxelData.y = current.y;
                        VoxelData.z = current.z;
                        CheckedVoxels.Add(VoxelData);
                        nodes.Enqueue(VoxelData);
                    }
                }

                // 0,0,-1
                if (!HasChecked(current.x, current.y, current.z - 1))
                {
                    ++Pressure;
                    if (Block.HasVolume(current.x, current.y, current.z - 1))
                    {
                        VoxelData.x = current.x;
                        VoxelData.y = current.y;
                        VoxelData.z = current.z - 1;
                        CheckedVoxels.Add(VoxelData);
                        nodes.Enqueue(VoxelData);
                    }
                }

                // 0,0,1
                if (!HasChecked(current.x, current.y, current.z + 1))
                {
                    ++Pressure;
                    if (Block.HasVolume(current.x, current.y, current.z + 1))
                    {
                        VoxelData.x = current.x;
                        VoxelData.y = current.y;
                        VoxelData.z = current.z + 1;
                        CheckedVoxels.Add(VoxelData);
                        nodes.Enqueue(VoxelData);
                    }
                }
            }

            SetDone(false);
        }

        public void SetDone(bool IsConnected)
        {
            this.IsConnected = IsConnected;
            this.IsDone = true;
            nodes.Clear();
            bottomNodes.Clear();
            topNodes.Clear();
            if (IsConnected)
            {
                CheckedVoxels.ClearFast();
            }
        }
    }
}
