﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class VoxelPhysicsMenu 
{
    [MenuItem("TerrainEngine/Processors/13-VoxelPhysicsProcessor")]
    static public void CreateVoxelPhysicsProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        VoxelPhysicsProcessor Obj = GameObject.FindObjectOfType(typeof(VoxelPhysicsProcessor)) as VoxelPhysicsProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<VoxelPhysicsProcessor>();

        Obj.name = "13-VoxelPhysicsProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 11;
    }
}
