﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class GameObjectToTerrainTreeEditor : ScriptableWizard 
{
    public Terrain Terr;
    public UnityTerrainGameObjectConverter UnityTerrainGameObject;
    public List<Transform> Objects;

    [MenuItem("TerrainEngine/Tools/GameObjects To Terrain Trees")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<GameObjectToTerrainTreeEditor>("GameObjectToTerrainTreeEditor", "Add To Terrain");
    }

    void OnWizardOtherButton()
    {

    }

    public bool IsAPrefab(GameObject Obj)
    {
        return PrefabUtility.GetPrefabParent(Obj) == null && PrefabUtility.GetPrefabObject(Obj) != null;
    }

    public GameObject GetPrefab(GameObject Obj)
    {
        string Path = AssetDatabase.GetAssetPath(PrefabUtility.GetPrefabParent(Obj));
        Debug.Log("Path:" + Path + ",Obj:" + Obj + "," + PrefabUtility.GetPrefabParent(Obj));
        return AssetDatabase.LoadAssetAtPath<GameObject>(Path);
    }

    void OnWizardCreate()
    {
        GameObject MovedGo = GameObject.Find("Moved");
        if (MovedGo == null)
            MovedGo = new GameObject("Moved");

        Transform Moved = MovedGo.transform;
        Moved.gameObject.SetActive(true);

        List<TreePrototype> Prototypes = new List<TreePrototype>(Terr.terrainData.treePrototypes);
        List<UnityTerrainGameObjectConverter.CustomTreeInstance> Instances = new List<UnityTerrainGameObjectConverter.CustomTreeInstance>();

        int i = 0;
        int Count = 0;
        foreach (Transform Tr in Objects)
        {
            Count = Prototypes.Count;
            for (i = 0; i < Count; ++i)
            {
                if (Prototypes[i].prefab.name == Tr.name)
                {
                    Instances.Add(new UnityTerrainGameObjectConverter.CustomTreeInstance() { prototypeIndex = i, heightScale = Tr.localScale.y, widthScale = Tr.localScale.x, color = Color.white, position = Tr.position, rotation = Tr.rotation.eulerAngles.y });
                    Tr.SetParent(Moved);
                    break;
                }
            }
            if (i >= Prototypes.Count)
            {
                TreePrototype Proto = new TreePrototype() { prefab = GetPrefab(Tr.gameObject) };
                Debug.Log("Create Prototype :" + Proto.prefab);
                if (Proto.prefab == null)
                    continue;

                Prototypes.Add(Proto);
                Instances.Add(new UnityTerrainGameObjectConverter.CustomTreeInstance() { prototypeIndex = i, heightScale = Tr.localScale.y, widthScale = Tr.localScale.x, color = Color.white, position = Tr.position, rotation = Tr.rotation.eulerAngles.y });
                Tr.SetParent(Moved);
            }
        }

        Debug.Log("Imported Hand Placed Objects : " + Instances.Count);
        UnityTerrainGameObject.ImportUnityTerrain(Prototypes.ToArray(), Instances.ToArray());
    }
}
