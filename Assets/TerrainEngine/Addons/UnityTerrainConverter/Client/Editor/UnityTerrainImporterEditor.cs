﻿using System;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class UnityTerrainImporterEditor : ScriptableWizard
{
    [Serializable]
    public struct TerrainTexture
    {
        public string VoxelName;
        public Texture2D texture;
        public Texture2D normal;
        public bool IsActive;
        public bool IsGround;
    }

    public Terrain UnityTerrain;
    public IVoxelPalette Palette;
    public Material SplatMaterial;
    public bool ImportGrass = true;
    public bool ImportDetails = true;
    public bool ImportTrees = true;
    public TerrainTexture[] Textures;

    //[MenuItem("TerrainEngine/Processor/Import Unity Terrain")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<UnityTerrainImporterEditor>("Import Unity Terrain", "Import", "Create Material");
    }

    protected override bool DrawWizardGUI()
    {
        if (UnityTerrain != null && (Textures == null || Textures.Length != UnityTerrain.terrainData.splatPrototypes.Length))
        {
            Textures = new TerrainTexture[UnityTerrain.terrainData.splatPrototypes.Length];

            for (int i = 0; i < UnityTerrain.terrainData.splatPrototypes.Length; ++i)
            {
                Textures[i] = new TerrainTexture();
                Textures[i].IsActive = i < 4;
                Textures[i].IsGround = true;
                Textures[i].texture = UnityTerrain.terrainData.splatPrototypes[i].texture;
                Textures[i].normal = UnityTerrain.terrainData.splatPrototypes[i].normalMap;
            }
        }
        return base.DrawWizardGUI();
    }

    void OnWizardOtherButton()
    {

    }

    void OnWizardCreate()
    {
        
    }
}
