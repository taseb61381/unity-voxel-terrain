﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class UnityTerrainConverterMenu 
{
    [MenuItem("TerrainEngine/Processors/Unity Terrain/Import Terrain")]
    static public void ImportUnityTerrain()
    {
        if (Selection.activeGameObject == null || Selection.activeGameObject.GetComponent<Terrain>() == null)
        {
            Debug.LogError("No UnityTerrain Selected in scene. Please select an unity Terrain");
            return;
        }

        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        Terrain UnityTerrain = Selection.activeGameObject.GetComponent<Terrain>();
        TerrainData UnityTerrainData = UnityTerrain.terrainData;

        {
            UnityTerrainConverterProcessor Obj = GameObject.FindObjectOfType(typeof(UnityTerrainConverterProcessor)) as UnityTerrainConverterProcessor;
            if (Obj == null)
                Obj = new GameObject().AddComponent<UnityTerrainConverterProcessor>();

            Obj.name = "0-UnityTerrainConverterProcessor";
            Obj.DataName = "Terrain";
            Obj.transform.parent = Manager.transform;
            Obj.Priority = 0;
            Obj.UnityTerrain = UnityTerrainData;
            Obj.PositionOffset = UnityTerrain.GetPosition();
        }

        {
            UnityTerrainGameObjectConverter Obj = GameObject.FindObjectOfType(typeof(UnityTerrainGameObjectConverter)) as UnityTerrainGameObjectConverter;
            if (Obj == null)
                Obj = new GameObject().AddComponent<UnityTerrainGameObjectConverter>();

            Obj.name = "1-UnityTerrainGameObjectConverter";
            Obj.DataName = "GO-" + UnityTerrain.name;
            Obj.transform.parent = Manager.transform;
            Obj.Priority = 1;
            Obj.UnityTerrain = UnityTerrainData;
            Obj.PositionOffset = UnityTerrain.GetPosition();
            Obj.ImportUnityTerrain();
            if (Obj.Container == null || Obj.Container.Objects.Count == 0)
                GameObject.DestroyImmediate(Obj.gameObject);
        }

        {
            UnityTerrainGrassConverter Obj = GameObject.FindObjectOfType(typeof(UnityTerrainGrassConverter)) as UnityTerrainGrassConverter;
            if (Obj == null)
                Obj = new GameObject().AddComponent<UnityTerrainGrassConverter>();

            Obj.name = "2-UnityTerrainGrassConverter";
            Obj.DataName = "Grass-" + UnityTerrain.name;
            Obj.transform.parent = Manager.transform;
            Obj.Priority = 2;
            Obj.UnityTerrain = UnityTerrainData;
            Obj.PositionOffset = UnityTerrain.GetPosition();
            Obj.ImportUnityTerrain();
            if (Obj.Container == null || Obj.Container.Grass.Count == 0)
                GameObject.DestroyImmediate(Obj.gameObject);
        }

        {
            UnityTerrainDetailsConverter Obj = GameObject.FindObjectOfType(typeof(UnityTerrainDetailsConverter)) as UnityTerrainDetailsConverter;
            if (Obj == null)
                Obj = new GameObject().AddComponent<UnityTerrainDetailsConverter>();

            Obj.name = "3-UnityTerrainDetailsConverter";
            Obj.DataName = "Details-" + UnityTerrain.name;
            Obj.transform.parent = Manager.transform;
            Obj.Priority = 3;
            Obj.UnityTerrain = UnityTerrainData;
            Obj.PositionOffset = UnityTerrain.GetPosition();
            Obj.ImportUnityTerrain();
            if (Obj.Container == null || Obj.Container.Details.Count == 0)
                GameObject.DestroyImmediate(Obj.gameObject);
        }

        {
            ColliderProcessor Obj = GameObject.FindObjectOfType(typeof(ColliderProcessor)) as ColliderProcessor;
            if (Obj == null)
                Obj = new GameObject().AddComponent<ColliderProcessor>();

            Obj.name = "9-ColliderProcessor";
            Obj.transform.parent = Manager.transform;
            Obj.Priority = 0;
        }

        UnityTerrain.gameObject.SetActive(false);
    }
}
