﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class UnityTerrainGameObjectConverter : GameObjectProcessor
{
    [System.Serializable]
    public struct CustomTreeInstance
    {
        // Summary:
        //     Color of this instance.
        public Color32 color;
        //
        // Summary:
        //     Height scale of this instance (compared to the prototype's size).
        public float heightScale;
        //
        // Summary:
        //     Lightmap color calculated for this instance.
        public Color32 lightmapColor;
        //
        // Summary:
        //     Position of the tree.
        public Vector3 position;
        //
        // Summary:
        //     Index of this instance in the TerrainData.treePrototypes array.
        public int prototypeIndex;
        //
        // Summary:
        //     Rotation of the tree on X-Z plane (in radians).
        public float rotation;
        //
        // Summary:
        //     Width scale of this instance (compared to the prototype's size).
        public float widthScale;

        public TreeInstance GetInstance(Vector3 TerrainSize)
        {
            Vector3 P = position;
            P.x = position.x / TerrainSize.x;
            P.y = position.y / TerrainSize.y;
            P.z = position.z / TerrainSize.z;
            return new TreeInstance() { color = color, heightScale = heightScale, lightmapColor = lightmapColor, position = P, rotation = rotation, widthScale = widthScale, prototypeIndex = prototypeIndex };
        }
    }

    public TerrainData UnityTerrain;
    private bool Loaded = false;
    private TreeInstance[] Instances;
    public Vector3 TerrainSize;
    public Vector3 PositionOffset;

    public CustomTreeInstance[] HandPlacedObjects;

    public override bool CanStart()
    {
        if (!Loaded)
        {
            Loaded = true;

            if(UnityTerrain == null)
            {
                Debug.LogError(name + " UnityTerrain is null. Can not start");
                return false;
            }

            ImportUnityTerrain();

            TerrainSize = new Vector3(UnityTerrain.size.x, UnityTerrain.heightmapScale.y, UnityTerrain.size.z);
            List<TreeInstance> TempInstances = new List<TreeInstance>(UnityTerrain.treeInstances);

            if (HandPlacedObjects != null)
            {
                for (int i = 0; i < HandPlacedObjects.Length; ++i)
                {
                    TempInstances.Add(HandPlacedObjects[i].GetInstance(TerrainSize));
                }
            }

            Instances = TempInstances.ToArray();
            Debug.Log("Loaded TreeInstance : " + Instances.Length + ",GameObjects:" + Container.Objects.Count);
        }

        return base.CanStart();
    }

    public void ImportUnityTerrain()
    {
        if (Container == null)
        {
            Container = gameObject.AddComponent<GameObjectsContainer>();
            if (Container.Objects == null)
                Container.Objects = new List<GameObjectInfo>();
        }
        else if (Container != null && Container.Objects != null && Container.Objects.Count != 0)
            return;

        GameObjectInfo Info;
        foreach (TreePrototype Proto in UnityTerrain.treePrototypes)
        {
            Info = new GameObjectInfo();
            Info.Object = Proto.prefab;
            Info.UseLod = false;
            Info.UseFading = true;
            Objects.Add(Info);
        }
    }

    public void ImportUnityTerrain(TreePrototype[] Prototypes, CustomTreeInstance[] Instances)
    {
        if (Container == null)
        {
            Container = gameObject.AddComponent<GameObjectsContainer>();
            if (Container.Objects == null)
                Container.Objects = new List<GameObjectInfo>();
        }

        GameObjectInfo Info;
        foreach (TreePrototype Proto in UnityTerrain.treePrototypes)
        {
            if (GetInfo(Proto.prefab.name) != null)
                continue;

            Info = new GameObjectInfo();
            Info.Object = Proto.prefab;
            Info.UseLod = false;
            Info.UseFading = true;
            Objects.Add(Info);
        }

        HandPlacedObjects = Instances;
    }

    public override bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        if (ChunkId != 0)
            return true;

        GameObjectDatas GData = Block.GetCustomData<GameObjectDatas>(InternalDataName);
        Vector3 WorldPositionMin = new Vector3(Block.WorldX, Block.WorldY, Block.WorldZ) * Block.Information.Scale;
        Vector3 WorldPositionMax = WorldPositionMin + new Vector3(Block.Information.BlockSizeX, Block.Information.BlockSizeY, Block.Information.BlockSizeZ);

        int x, y, z;
        Vector3 Treeposition;
        TreeInstance Tree;
        int Count = Objects.Count;
        for(int i= Instances.Length-1; i>=0;--i)
        {
            Tree = Instances[i];
            Treeposition = Tree.position;
            Treeposition.x *= TerrainSize.x;
            Treeposition.z *= TerrainSize.z;
            Treeposition.y *= TerrainSize.y;

            Treeposition.x += PositionOffset.x;
            Treeposition.y += PositionOffset.y;
            Treeposition.z += PositionOffset.z;

            if (Treeposition.x >= WorldPositionMin.x && Treeposition.y >= WorldPositionMin.y && Treeposition.z >= WorldPositionMin.z)
            {
                if (Treeposition.x < WorldPositionMax.x && Treeposition.y < WorldPositionMax.y && Treeposition.z < WorldPositionMax.z)
                {
                    x = (int)((Treeposition.x - WorldPositionMin.x) / Block.Information.Scale);
                    y = (int)((Treeposition.y - WorldPositionMin.y) / Block.Information.Scale);
                    z = (int)((Treeposition.z - WorldPositionMin.z) / Block.Information.Scale);

                    if (Tree.prototypeIndex >= Count)
                        continue;

                    GData.SetByteIntFloat(x, y, z, (byte)(Tree.prototypeIndex + 1), (int)Tree.rotation, Tree.heightScale);
                }
            }

        }
        return true;
    }

    public override IWorldGenerator.GeneratorTypes GetGeneratorType()
    {
        return IWorldGenerator.GeneratorTypes.GROUND;
    }

    public override IWorldGenerator GetGenerator()
    {
        return null;
    }
}
