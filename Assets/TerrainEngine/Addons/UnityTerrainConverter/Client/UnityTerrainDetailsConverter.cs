﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class UnityTerrainDetailsConverter : DetailProcessor
{
    public TerrainData UnityTerrain;
    private bool Loaded = false;
    private DetailPrototype[] Prototypes;
    private int[][,] DetailsLayers;
    private int SizeX, SizeY,DetailsCount;
    private float TerrainResolution;
    public Vector3 PositionOffset;

    public override bool CanStart()
    {
        if (!Loaded)
        {
            Loaded = true;
            if (UnityTerrain == null)
            {
                Debug.LogError(name + " UnityTerrain is null.Can not start");
                return false;
            }

            Prototypes = UnityTerrain.detailPrototypes;
            SizeX = UnityTerrain.detailWidth;
            SizeY = UnityTerrain.detailHeight;
            TerrainResolution = (float)UnityTerrain.detailResolution / UnityTerrain.size.x;

            ImportUnityTerrain();

            int ObjectsCount = Container.Details.Count;
            DetailsLayers = new int[ObjectsCount][,];
            ObjectsCount = 0;

            for (int i = 0; i < UnityTerrain.detailPrototypes.Length; ++i)
            {
                if (UnityTerrain.detailPrototypes[i].prototype != null && UnityTerrain.detailPrototypes[i].prototype.GetComponent<MeshFilter>() != null)
                {
                    DetailsLayers[ObjectsCount] = UnityTerrain.GetDetailLayer(0, 0, SizeX, SizeY, i);
                    ++ObjectsCount;
                }
            }
            DetailsCount = ObjectsCount;

        }
        return base.CanStart();
    }

    public void ImportUnityTerrain()
    {
        Debug.Log(name + ",Importing details " + UnityTerrain.detailPrototypes.Length);

        if (Container != null && Container.Details != null && Container.Details.Count != 0)
            return;

        int ObjectsCount = 0;
        DetailsCount = 0;
        for (int i = 0; i < UnityTerrain.detailPrototypes.Length; ++i)
        {
            if (UnityTerrain.detailPrototypes[i].prototype != null && UnityTerrain.detailPrototypes[i].prototype.GetComponent<MeshFilter>() != null)
                ++ObjectsCount;
        }

        Debug.Log(name + ",Objects :" + ObjectsCount);
        if (ObjectsCount == 0)
            return;

        if (Container == null)
        {
            Container = gameObject.AddComponent<DetailsContainer>();
            if (Container.Details == null)
                Container.Details = new List<DetailInfo>();
        }

        DetailsLayers = new int[ObjectsCount][,];
        DetailsCount = ObjectsCount;
        ObjectsCount = 0;
        DetailPrototype Proto;
        for (int i = 0; i < UnityTerrain.detailPrototypes.Length; ++i)
        {
            Proto = UnityTerrain.detailPrototypes[i];
            if (Proto.prototype != null && Proto.prototype.GetComponent<MeshFilter>() != null)
            {
                DetailInfo Info = new DetailInfo();
                Info.Object = Proto.prototype.GetComponent<MeshFilter>();
                Info.RandomScale = new RandomInfo();
                Info.RandomScale.Enabled = true;
                Info.RandomScale.Min = new Vector3(Proto.minWidth, Proto.minHeight, Proto.minWidth);
                Info.RandomScale.Max = new Vector3(Proto.maxWidth, Proto.maxHeight, Proto.maxWidth);
                Info.HeightSide = HeightSides.BOTTOM;
                Container.Details.Add(Info);
                ++ObjectsCount;
            }
        }
    }

    public override bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        int Size = Block.Information.VoxelPerChunk;
        int StartX = ChunkX * Size;
        int StartZ = ChunkZ * Size;

        SimpleTypeDatas DData = Block.GetCustomData<SimpleTypeDatas>(InternalDataName);
        int[] Ground = Data.GetGrounds(Size, Size);

        int x, y, z, f = 0, i =0;
        float px, pz;
        int Max = 0;
        int Id = 0;
        int Objects = DetailsCount - 1;
        for (x = 0; x < Size; ++x)
        {
            px = (float)(StartX + x + Block.WorldX - PositionOffset.x) * TerrainManager.Instance.Informations.Scale * TerrainResolution;
            for (z = 0; z < Size; ++z, ++f)
            {
                pz = (float)(StartZ + z + Block.WorldZ - PositionOffset.z) * TerrainManager.Instance.Informations.Scale * TerrainResolution;

                if (px < 0 || px >= SizeX || pz < 0 || pz >= SizeY)
                    continue;

                y = Ground[f] - Block.WorldY;

                if (y <= 0 || y >= Block.VoxelsPerAxis)
                    continue;

                Max = 0;
                Id = 0;

                for (i = Objects; i >= 0; --i)
                {
                    if (DetailsLayers[i][(int)pz, (int)px] > Max)
                    {
                        Max = DetailsLayers[i][(int)pz, (int)px];
                        Id = i + 1;
                    }
                }

                if (Id != 0)
                {
                    Data.SimpleObjects.Add(new BiomeData.SimpleTempObject() { Id =(byte)Id, x = (ushort)(x+StartX), y = (ushort)y, z =(ushort)(z+StartZ)});
                }
            }
        }

        if (Data.SimpleObjects.Count != 0)
        {
            DData.SetObjects(Data.SimpleObjects);
        }

        Data.SimpleObjects.ClearFast();

        return true;
    }

    public override IWorldGenerator.GeneratorTypes GetGeneratorType()
    {
        return IWorldGenerator.GeneratorTypes.GROUND;
    }

    public override IWorldGenerator GetGenerator()
    {
        return null;
    }
}
