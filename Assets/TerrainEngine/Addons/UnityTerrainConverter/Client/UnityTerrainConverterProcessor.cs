﻿using TerrainEngine;
using UnityEngine;

public class UnityTerrainConverterProcessor : IBlockProcessor 
{
    public TerrainData UnityTerrain;
    private float[] HeightMap;
    private float[,,] SplatMap;
    public float HeightScale = 0;
    public float HeightOffset = 0;
    public float Resolution = 0;
    public Vector3 PositionOffset;
    public bool OptimizeMemory = false;
    private int SizeX, SizeZ;

    public override bool CanStart()
    {
        if (HeightMap == null)
        {
            if (UnityTerrain == null)
            {
                Debug.LogError(" No Unity Terrain. Can not start");
                return false;
            }

            float Scale = TerrainManager.Instance.Informations.Scale;

            float[,] TerrainHeight = UnityTerrain.GetHeights(0, 0, UnityTerrain.heightmapWidth, UnityTerrain.heightmapHeight);
            SizeX = TerrainHeight.GetLength(0);
            SizeZ = TerrainHeight.GetLength(1);
            HeightMap = new float[SizeX * SizeZ];
            for (int i = 0; i < SizeX; ++i)
            {
                for (int j = 0; j < SizeZ; ++j)
                {
                    HeightMap[i * SizeZ + j] = TerrainHeight[j, i];
                }
            }

            SplatMap = UnityTerrain.GetAlphamaps(0, 0, UnityTerrain.alphamapWidth, UnityTerrain.alphamapHeight);
            if (HeightScale == 0)
            {
                HeightScale = UnityTerrain.heightmapScale.y / Scale;
                HeightOffset = 0.5f;
            }

            if (Resolution == 0)
                Resolution = (UnityTerrain.size.x / (float)SizeX) / Scale;

            Debug.Log("HeightMap Size :" + SizeX + ",TerrainSize:" + UnityTerrain.size.x);

        }
        return base.CanStart();
    }

    public float GetHeight(int px, int pz,ref byte Type)
    {
        Type = 1;
        return GetHeight(px, pz, SizeX, SizeZ, PositionOffset.x, PositionOffset.y, PositionOffset.z,HeightScale,HeightOffset);
    }

    public float GetHeight(float px, float pz,int SizeX,int SizeZ,float OffsetX,float OffsetY, float OffsetZ, float Scale,float HeightOffset)
    {
        px -= OffsetX;
        pz -= OffsetZ;

        int x = (int)(px / Resolution);
        int z = (int)(pz / Resolution);
        if (x < 0 || z < 0 || x >= SizeX - 1 || z >= SizeZ - 1)
            return 0;

        float sqX = (px / Resolution) - x;
        float sqZ = (pz / Resolution) - z;
        float Result = 0;

        if ((sqX + sqZ) < 1)
        {
            float triZ0 = HeightMap[x * SizeZ + z];
            Result = triZ0;
            Result += (HeightMap[(x + 1) * SizeZ + z] - triZ0) * sqX;
            Result += (HeightMap[x * SizeZ + z + 1] - triZ0) * sqZ;
        }
        else
        {
            float triZ3 = HeightMap[(x + 1)* SizeZ + z + 1];

            Result = triZ3;
            Result += (HeightMap[(x + 1)* SizeZ +  z] - triZ3) * (1.0f - sqZ);
            Result += (HeightMap[x* SizeZ +  z + 1] - triZ3) * (1.0f - sqX);
        }

        return (Result * Scale) + HeightOffset + OffsetY;
    }

    public override bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        int Size = Block.Information.VoxelPerChunk;
        int StartX = ChunkX * Size;
        int StartZ = ChunkZ * Size;

        int[] Ground = Data.GetGrounds(Size, Size);

        int lx, lz,x,z;
        int WorldX;
        float Height = 0,sqX,sqZ,triZ0,triZ3,Result;
        int f = 0;

        if(OptimizeMemory)
            Block.SetHeight(GetHeight);

        float OX = PositionOffset.x;
        float OY = PositionOffset.y;
        float OZ = PositionOffset.z;
        float Scale = HeightScale;
        float HeightOffset = this.HeightOffset;
        float Resolution = this.Resolution;

        if (!OptimizeMemory)
            Block.InitHeight();

        float px,pz;
        for (lx = 0; lx < Size; ++lx)
        {
            WorldX = Block.WorldX + lx;
            px = StartX + WorldX - OX;
            for (lz = 0; lz < Size; ++lz, ++f)
            {
                pz = StartZ + Block.WorldZ + lz - OZ;

                x = (int)(px / Resolution);
                z = (int)(pz / Resolution);
                if (x < 0 || z < 0 || x >= SizeX - 1 || z >= SizeZ - 1)
                    Height = 0;
                else
                {

                    sqX = (px / Resolution) - x;
                    sqZ = (pz / Resolution) - z;
                    Result = 0;

                    if ((sqX + sqZ) < 1)
                    {
                        triZ0 = HeightMap[x * SizeZ + z];
                        Result = triZ0;
                        Result += (HeightMap[(x + 1) * SizeZ + z] - triZ0) * sqX;
                        Result += (HeightMap[x * SizeZ + z + 1] - triZ0) * sqZ;
                    }
                    else
                    {
                        triZ3 = HeightMap[(x + 1) * SizeZ + z + 1];

                        Result = triZ3;
                        Result += (HeightMap[(x + 1) * SizeZ + z] - triZ3) * (1.0f - sqZ);
                        Result += (HeightMap[x * SizeZ + z + 1] - triZ3) * (1.0f - sqX);
                    }

                    Height = (Result * Scale) + HeightOffset + OY;
                }

                if (!OptimizeMemory)
                    Block.SetHeightFast(StartX + lx, StartZ + lz, Height, 1);

                if (Height - (float)(int)Height >= 0.5f)
                    Ground[f] = (int)Height;
                else
                    Ground[f] = (int)Height - 1;
            }
        }
        Block.Dirty = true;
            return true;
    }

    public override IWorldGenerator.GeneratorTypes GetGeneratorType()
    {
        return IWorldGenerator.GeneratorTypes.TERRAIN;
    }
}
