﻿using UnityEngine;
using System.Collections;

using TerrainEngine;

public class HeightMapTerrainConverterProcessor : IBlockProcessor 
{
    public Texture2D HeightMapTexture;
    public Texture2D RiversTexture;

    private float[] HeightMap;
    private float[] RiversMap;

    public float HeightScale = 1000;
    public float HeightOffset = 0;
    public float Resolution = 1;
    private int SizeX, SizeZ;
    public int Octaves = 4;

    public int ErodeCount = 2;
    public float Talus = 1f; 

    public override bool CanStart()
    {
        if (HeightMap == null)
        {
            float Scale = TerrainManager.Instance.Informations.Scale;

            Color[] Cols = HeightMapTexture.GetPixels();
            SizeX = HeightMapTexture.width;
            SizeZ = HeightMapTexture.height;
            HeightMap = new float[SizeX * SizeZ];
            int f = 0;
            for (int i = 0; i < SizeX; ++i)
            {
                for (int j = 0; j < SizeZ; ++j,++f)
                {
                    HeightMap[i * SizeZ + j] = Cols[i * SizeZ + j].r;
                }
            }

            HeightMap = GenerateSmoothNoise(HeightMap, Octaves, SizeX, SizeZ);
            Erode(HeightMap,SizeX, SizeZ, Talus, ErodeCount);
        }

        return true;
    }

    static public VectorI2[] neighbours = new VectorI2[] { new VectorI2(-1,-1), new VectorI2(1,-1),new VectorI2(-1,1) ,new VectorI2(1,1) };

    public bool VALID(int m,int n) 
    {
        return ((n) >= 0) && ((n) < SizeX) && ((m) >= 0) && ((m) < SizeZ);
    }

    static public void POS(float[] HeightMap,int x, int y, float value,int SizeZ)
    {
        HeightMap[(x)+(y)*SizeZ] = value;
    }

    static public float POS(float[] HeightMap,int x, int y,int SizeZ)
    {
        return HeightMap[(x) + (y) * SizeZ];
    }

    static public float SATURATE(float x) 
    {
        return ((x) < 0) ? 0 : (((x) > 1) ? 1 : (x));
    }

    public void Erode(float[] HeightMap, int height, int width, float talus_numerator, int iterations)
    {
        float t = talus_numerator / (((float)(height + width) * 0.5f));
        float di, dmax, h; 
        int l; 
        VectorI2 c;
        int j, y, x,i;
        for (j = 0; j < iterations; ++j)
        {
            for (y = 0; y < height; ++y)
            {
                for (x = 0; x < width; ++x)
                {
                    di = 0.0f; dmax = 0.0f; h = POS(HeightMap,x, y,SizeZ);
                    l = 0;
                    for (i = 0; i < 4; ++i)
                    {
                        c = neighbours[i];
                        if (VALID(x + c.x, y + c.y) == false) continue;
                        di = h - POS(HeightMap, x + c.x, y + c.y,SizeZ);
                        if (di > dmax) { dmax = di; l = i; }
                    }
                    if (dmax > 0.0f && dmax <= t)
                    {
                        c = neighbours[l];
                        dmax *= 0.5f;
                        POS(HeightMap, x, y, SATURATE(h - dmax),SizeZ);
                        POS(HeightMap, x + c.x, y + c.y, SATURATE(POS(HeightMap, x + c.x, y + c.y,SizeZ) + dmax), SizeZ);
                    }
                }
            }

        }
    }

    public static float[] GenerateSmoothNoise(float[] baseNoise, int octave, int width,int height)
    {
        float[] smoothNoise = new float[width*height];

        int samplePeriod = 1 << octave; // calculates 2 ^ k
        float sampleFrequency = 1.0f / samplePeriod;

        int sample_i0 = 0;
        int sample_i1 = 0; //wrap around
        float horizontal_blend = 0;

        //calculate the vertical sampling indices
        int sample_j0 = 0;
        int sample_j1 = 0; //wrap around
        float vertical_blend = 0;

        float top = 0;

        //blend the bottom two corners
        float bottom = 0;

        for (int i = 0; i < width; i++)
        {
            //calculate the horizontal sampling indices
            sample_i0 = (i / samplePeriod) * samplePeriod;
            sample_i1 = (sample_i0 + samplePeriod) % width; //wrap around
            horizontal_blend = (i - sample_i0) * sampleFrequency;

            for (int j = 0; j < height; j++)
            {
                //calculate the vertical sampling indices
                sample_j0 = (j / samplePeriod) * samplePeriod;
                sample_j1 = (sample_j0 + samplePeriod) % height; //wrap around
                vertical_blend = (j - sample_j0) * sampleFrequency;

                //blend the top two corners
                top = Mathf.Lerp(baseNoise[sample_i0 * height + sample_j0],
                    baseNoise[sample_i1 * height + sample_j0], horizontal_blend);

                //blend the bottom two corners
                bottom = Mathf.Lerp(baseNoise[sample_i0 * height + sample_j1],
                    baseNoise[sample_i1 * height + sample_j1], horizontal_blend);

                //final blend
                smoothNoise[i * height + j] = Mathf.Lerp(top, bottom, vertical_blend);
            }
        }

        return smoothNoise;
    }

    public float GetHeight(float px, float pz, int SizeX, int SizeZ, float OffsetX, float OffsetY, float OffsetZ, float Scale, float HeightOffset)
    {
        px -= OffsetX;
        pz -= OffsetZ;

        int x = (int)(px / Resolution);
        int z = (int)(pz / Resolution);
        if (x < 0 || z < 0 || x >= SizeX - 1 || z >= SizeZ - 1)
            return 0;

        float sqX = (px / Resolution) - x;
        float sqZ = (pz / Resolution) - z;
        float Result = 0;

        if ((sqX + sqZ) < 1)
        {
            float triZ0 = HeightMap[x * SizeZ + z];
            Result = triZ0;
            Result += (HeightMap[(x + 1) * SizeZ + z] - triZ0) * sqX;
            Result += (HeightMap[x * SizeZ + z + 1] - triZ0) * sqZ;
        }
        else
        {
            float triZ3 = HeightMap[(x + 1) * SizeZ + z + 1];

            Result = triZ3;
            Result += (HeightMap[(x + 1) * SizeZ + z] - triZ3) * (1.0f - sqZ);
            Result += (HeightMap[x * SizeZ + z + 1] - triZ3) * (1.0f - sqX);
        }

        return (Result * Scale) + HeightOffset + OffsetY;
    }

    public override bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        int Size = Block.Information.VoxelPerChunk;
        int StartX = ChunkX * Size;
        int StartZ = ChunkZ * Size;

        int[] Ground = Data.GetGrounds(Size, Size);

        int lx, lz, x, z;
        int WorldX;
        float Height = 0, sqX, sqZ, triZ0, triZ3, Result;
        int f = 0;

        int SZ = SizeZ;
        float OX = 0;
        float OY = 0;
        float OZ = 0;
        float Scale = HeightScale;
        float HeightOffset = this.HeightOffset;
        float Resolution = this.Resolution;

        Block.InitHeight();

        float px, pz;
        for (lx = 0; lx < Size; ++lx)
        {
            WorldX = Block.WorldX + lx;
            px = StartX + WorldX - OX;
            for (lz = 0; lz < Size; ++lz, ++f)
            {
                pz = StartZ + Block.WorldZ + lz - OZ;

                x = (int)(px / Resolution);
                z = (int)(pz / Resolution);
                if (x < 0 || z < 0 || x >= SizeX - 1 || z >= SizeZ - 1)
                    Height = 0;
                else
                {

                    sqX = (px / Resolution) - x;
                    sqZ = (pz / Resolution) - z;
                    Result = 0;

                    if ((sqX + sqZ) < 1)
                    {
                        triZ0 = HeightMap[x * SizeZ + z];
                        Result = triZ0;
                        Result += (HeightMap[(x + 1) * SizeZ + z] - triZ0) * sqX;
                        Result += (HeightMap[x * SizeZ + z + 1] - triZ0) * sqZ;
                    }
                    else
                    {
                        triZ3 = HeightMap[(x + 1) * SizeZ + z + 1];

                        Result = triZ3;
                        Result += (HeightMap[(x + 1) * SizeZ + z] - triZ3) * (1.0f - sqZ);
                        Result += (HeightMap[x * SizeZ + z + 1] - triZ3) * (1.0f - sqX);
                    }

                    Height = (Result * Scale) + HeightOffset + OY;
                }
                Block.SetHeightFast(StartX + lx, StartZ + lz, Height, 1);

                if (Height - (float)(int)Height >= 0.5f)
                    Ground[f] = (int)Height;
                else
                    Ground[f] = (int)Height - 1;
            }
        }
        Block.Dirty = true;
        return true;
    }

    public override IWorldGenerator.GeneratorTypes GetGeneratorType()
    {
        return IWorldGenerator.GeneratorTypes.TERRAIN;
    }
}
