﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class UnityTerrainGrassConverter : GrassProcessor
{
    public TerrainData UnityTerrain;
    public float TerrainResolution=0;
    private bool Loaded = false;
    private DetailPrototype[] Prototypes;
    private int[][,] GrassLayers;
    private int SizeX, SizeY, GrassCount;
    public Vector3 PositionOffset;

    public override bool CanStart()
    {
        if (!Loaded)
        {
            Loaded = true;
            if (UnityTerrain == null)
            {
                Debug.LogError(name + " UnityTerrain is null.Can not start");
                return false;
            }

            float Scale = TerrainManager.Instance.Informations.Scale;
            Prototypes = UnityTerrain.detailPrototypes;
            SizeX = UnityTerrain.detailWidth;
            SizeY = UnityTerrain.detailHeight;

            if (TerrainResolution == 0)
            {
                TerrainResolution = (float)UnityTerrain.detailResolution / UnityTerrain.size.x;
                TerrainResolution /= Scale;
            }

            ImportUnityTerrain();

            int ObjectsCount = Container.Grass.Count;
            GrassLayers = new int[ObjectsCount][,];
            ObjectsCount = 0;

            for (int i = 0; i < UnityTerrain.detailPrototypes.Length; ++i)
            {
                if (UnityTerrain.detailPrototypes[i].prototypeTexture != null)
                {
                    GrassLayers[ObjectsCount] = UnityTerrain.GetDetailLayer(0, 0, SizeX, SizeY, i);
                    ++ObjectsCount;
                }
            }
            GrassCount = ObjectsCount;

        }
        return base.CanStart();
    }

    public void ImportUnityTerrain()
    {
        Debug.Log(name + ",Importing Grass " + UnityTerrain.detailPrototypes.Length);

        if (Container != null && Container.Grass != null && Container.Grass.Count != 0)
            return;

        int ObjectsCount = 0;
        GrassCount = 0;
        for (int i = 0; i < UnityTerrain.detailPrototypes.Length; ++i)
        {
            if (UnityTerrain.detailPrototypes[i].prototypeTexture != null)
                ++ObjectsCount;
        }

        Debug.Log(name + ",Objects :" + ObjectsCount);
        if (ObjectsCount == 0)
            return;

        if (Container == null)
        {
            Container = gameObject.AddComponent<GrassContainer>();
            if(Container.Grass == null)
                Container.Grass = new List<GrassInfo>();
        }

        GrassCount = ObjectsCount;
        ObjectsCount = 0;
        DetailPrototype Proto;
        for (int i = 0; i < UnityTerrain.detailPrototypes.Length; ++i)
        {
            Proto = UnityTerrain.detailPrototypes[i];
            if (Proto.prototypeTexture != null)
            {
                GrassInfo Info = new GrassInfo();
                Info.Texture = Proto.prototypeTexture;
                Info.RandomScale = new RandomInfo();
                Info.RandomScale.Enabled = true;
                Info.RandomScale.Min = new Vector3(Proto.minWidth, Proto.minHeight, Proto.minWidth);
                Info.RandomScale.Max = new Vector3(Proto.maxWidth, Proto.maxHeight, Proto.maxWidth);
                Info.HeightSide = HeightSides.BOTTOM;
                Info.PlaneMin = 8;
                Info.PlaneMax = 16;
                Info.GrassType = GrassTypes.PLANES;
                Container.Grass.Add(Info);
                ++ObjectsCount;
            }
        }
    }

    public override bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        int Size = Block.Information.VoxelPerChunk;
        int StartX = ChunkX * Size;
        int StartZ = ChunkZ * Size;

        SimpleTypeDatas CData = Block.GetCustomData<SimpleTypeDatas>(InternalDataName);
        int[] Ground = Data.GetGrounds(Size, Size);

        int x, y, z, f = 0, i = 0;
        float px, pz;
        int Max = 0;
        int Id = 0;
        int Objects = GrassCount - 1;
        for (x = 0; x < Size; ++x)
        {
            px = (float)(StartX + x + Block.WorldX - PositionOffset.x) * TerrainResolution;
            for (z = 0; z < Size; ++z, ++f)
            {
                pz = (float)(StartZ + z + Block.WorldZ - PositionOffset.z) * TerrainResolution;

                if (px < 0 || px >= SizeX || pz < 0 || pz >= SizeY)
                    continue;

                y = Ground[f] - Block.WorldY;

                if (y <= 0 || y >= Block.VoxelsPerAxis)
                    continue;

                Max = 0;
                Id = 0;

                for (i = Objects; i >= 0; --i)
                {
                    if (GrassLayers[i][(int)pz, (int)px] > Max)
                    {
                        Max = GrassLayers[i][(int)pz, (int)px];
                        Id = i + 1;
                    }
                }

                if (Id != 0)
                {
                    Data.SimpleObjects.Add(new BiomeData.SimpleTempObject() { Id = (byte)Id, x = (ushort)(x+StartX), y = (ushort)y, z = (ushort)(z + StartZ) });
                }
            }
        }

        if (Data.SimpleObjects.Count != 0)
        {
            CData.SetObjects(Data.SimpleObjects);
        }

        Data.Objects.ClearFast();

        return true;
    }

    public override IWorldGenerator.GeneratorTypes GetGeneratorType()
    {
        return IWorldGenerator.GeneratorTypes.GROUND;
    }
    public override IWorldGenerator GetGenerator()
    {
        return null;
    }
}
