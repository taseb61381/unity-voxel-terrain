﻿using LibNoise;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class HeightMapGenerator : IWorldGenerator
    {
        /// <summary>
        /// This will change the generated size of the heightmap. Higher resolution will increase generation speed but decrease terrain details
        /// </summary>
        public int Resolution = 1; 

        /// <summary>
        /// Offset to apply to the generation position.
        /// </summary>
        public VectorI3 WorldOffset = new Vector3(0, 0, 0);

        /// <summary>
        /// Link your object here if you want spawn it on a certain biome
        /// </summary>
        public VectorI3 DesiredPosition;

        /// <summary>
        /// Set the name of the biome that you want your object spawn. The generator will modify the original position of the noise to target the biome desired
        /// </summary>
        public string DesiredBiomeName;

        public HeightNoisesContainer HeightNoises;
        public NoisesContainer Container;
        public NoiseObjectsContainer NoiseTypes;
        public List<NoisesContainer> ExternalContainers;

        [XmlIgnore]
        public IModule[] GlobalNoises;

        [XmlIgnore]
        public VoxelTypeOuput[] VoxelModules;

        public float OffsetX = 0;

        public void GetMainNoises(ref IModule Noises, ref VoxelTypeOuput VoxelType)
        {
            HeightNoises.GenerateModules(Information.IntSeed);
            Noises = HeightNoises.GetMainModule();
            VoxelType = HeightNoises.GetMainVoxelModule();

            Noises = new LibNoise.Modifiers.TranslateInput(Noises, WorldOffset.x, 0, WorldOffset.z);
            Noises = new LibNoise.Modifiers.ScaleInput(Noises, Information.Scale, Information.Scale, Information.Scale);
            Noises = new LibNoise.Modifiers.TranslateInput(Noises, OffsetX, 0, 0);

            if (HeightNoises != null && HeightNoises.Modules != null)
            {
                foreach (var Kp in HeightNoises.Modules)
                {
                    if (Kp.Value is IVoxelModule)
                        (Kp.Value as IVoxelModule).InitPalette(GetVoxelID);
                }
            }
        }



        public override IWorldGenerator.GeneratorTypes GetGeneratorType()
        {
            return GeneratorTypes.TERRAIN;
        }

        public override void OnServerStart(TerrainWorldServer Server)
        {
            if (HeightNoises != null)
            {
                GeneralLog.Log("[Server] Initing Noises, Threads:" + ThreadManager.Instance.Threads.Length);

                GlobalNoises = new IModule[ThreadManager.Instance.Threads.Length];
                VoxelModules = new VoxelTypeOuput[ThreadManager.Instance.Threads.Length];

                // Generate one complete noise graph for each thread, avoid error when multiple threads access to the same module variables
                for (int i = 0; i < GlobalNoises.Length; ++i)
                {
                    IModule Noises = null;
                    VoxelTypeOuput VoxelOutput = null;
                    GetMainNoises(ref Noises, ref VoxelOutput);

                    GlobalNoises[i] = Noises;
                    VoxelModules[i] = VoxelOutput;
                }
            }

            /*if (GlobalNoise == null)
                GlobalNoise = Container.GetGlobalNoise(ExternalContainers, Information.IntSeed);

            NoiseTypes.Container = Container;
            NoiseTypes.Init(this, null, Information.IntSeed);

            foreach (NoiseObjects Noise in NoiseTypes.NoisesObjects)
            {
                Noise.NoiseNameHasCode = Noise.NoiseName.GetHashCode();

                foreach (NoiseObject Obj in Noise.Objects)
                {
                    if (Obj.GroundType == 0)
                        Obj.GroundType = GetVoxelID(Obj.Name);
                }
            }*/

            InitDesiredBiome();
        }

        public void InitDesiredBiome()
        {
            if (DesiredBiomeName == null || DesiredBiomeName.Length == 0)
                return;

            GeneralLog.Log("Init Desired Biome at :" + DesiredPosition + " on biome: " + DesiredBiomeName+",Hashcode:"+DesiredBiomeName.GetHashCode());

            DesiredPosition /= Information.Scale;
            int DesiredHash = DesiredBiomeName.GetHashCode();
            float OffsetCount = 500; // We Check every 100 meters if there is the biome desired

            BiomeData Data = new BiomeData();
            bool IsValid = false;
            IModule GlobalNoise = GlobalNoises[0];
            for (int Count = 0; Count < 1000; ++Count) // Move right every step
            {
                Data.Start(); // Start Biomes
                GlobalNoise.GetValue(DesiredPosition.x + (float)(Count * OffsetCount), WorldOffset.y, DesiredPosition.z, Data); // Search from desired position to right
                Data.End(); // End Biomes

                if (Data.Contains(DesiredHash)) // Search between start and end biomes, if biome exist, stop and add offset to the global biome noise
                {
                    if (IsValid) // We Wait next 100 meters
                    {
                        GeneralLog.Log(DesiredBiomeName + " Biome found at : " + (float)(Count * OffsetCount));
                        OffsetX = (float)(Count * OffsetCount);

                        for (int i = 0; i < GlobalNoises.Length;++i)
                            GlobalNoises[i] = new LibNoise.Modifiers.TranslateInput(GlobalNoises[i], (float)(Count * OffsetCount), 0, 0);
                        return;
                    }

                    IsValid = true;
                }

                Data.CleanNoises(); // Reset saved biomes
            }

            GeneralLog.LogError("Can not find desired biome. Modify OffsetCout to search more far from DesiredPosition");
        }

        static public byte GetGroundType(VoxelTypeOuput Output, BiomeData Builder, int WorldX, int WorldZ, int Height, int Depth)
        {
            //if (Output != null)
                return Output.Input.GetVoxelId(Builder, WorldX, Height, WorldZ, Height);

            /*int i, y, Type = 1;

            NoiseObjects Objects = null;
            for (i = NoiseTypes.NoisesObjects.Count-1; i >= 0; --i)
            {
                if (!Builder.Contains(NoiseTypes.NoisesObjects[i].NoiseNameHasCode))
                    continue;

                Objects = NoiseTypes.NoisesObjects[i];
                for (y = 0; y < Objects.Objects.Count; ++y)
                {
                    if (Objects.Objects[y].IsValid(WorldX, WorldZ, Height, Depth, ref Type))
                    {
                        return (byte)Type;
                    }
                }
            }

            return 1;*/
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            IModule GlobalNoise = GlobalNoises[Thread.ThreadId];
            VoxelTypeOuput VoxelModule = VoxelModules[Thread.ThreadId];

            Data.SetResolution(Information.VoxelPerChunk, Resolution);

            int StartX = ChunkX * Information.VoxelPerChunk;
            int StartZ = ChunkZ * Information.VoxelPerChunk;
            int WorldX, WorldY, WorldZ = 0;

            Fast2DArray<byte> TempDatas = Data.GetCustomData("TempDatas") as Fast2DArray<byte>;
            //Fast2DArray<float> TempRivers = Data.GetCustomData("TempRivers") as Fast2DArray<float>;
            Fast2DArray<byte> TempTypes = Data.GetCustomData("TempTypes") as Fast2DArray<byte>;
            int[] Grounds = Data.GetGrounds(Block.Information.VoxelPerChunk, Block.Information.VoxelPerChunk);

            // Storing Volume X/Z point (biome) at y = VoxelPerChunk+1

            if (TempDatas == null)
            {
                TempDatas = new Fast2DArray<byte>(Block.Information.VoxelPerChunk, Block.Information.VoxelPerChunk);
                Data.SetCustomData("TempDatas", TempDatas);
            }

            // Storing Ground Type by X/Z point (biome)
            if (TempTypes == null)
            {
                TempTypes = new Fast2DArray<byte>(Data.VoxelPCResolution, Data.VoxelPCResolution);
                Data.SetCustomData("TempTypes", TempTypes);
            }

            Data.GetBiomes();

            int GroundLevel = Information.LocalGroundLevel;
            float Height = 0, dens;
            byte DefaultType = 0;

            Data.CleanNoises();

            int x, z, f = 0;
            if (Data.Resolution != 1)
            {
                for (x = 0; x < Data.VoxelPCResolution; ++x)
                {
                    WorldX = Block.WorldX + (x * Data.Resolution) + StartX;

                    for (z = 0; z < Data.VoxelPCResolution; ++z, ++f)
                    {
                        WorldZ = Block.WorldZ + (z * Data.Resolution) + StartZ;
                        WorldY = Block.WorldY + Block.VoxelsPerAxis;

                        Data.Start();

                        Height = (float)GlobalNoise.GetValue(WorldX, WorldZ, Data) + WorldOffset.y;
                        Data.HeightMap[x][z] = Height;
                        Data.End();
                        Data.SaveInfo(f);

                        TempTypes.array[f] = GetGroundType(VoxelModule, Data, WorldX, WorldZ, (int)Height, Mathf.Max((int)Height - WorldOffset.y, 1));
                    }
                }
            }


            f = 0;
            int ModuleIndex = 0;
            Block.InitHeight();
            for (x = 0; x < Block.Information.VoxelPerChunk; ++x)
            {
                WorldX = Block.WorldX + x + StartX;

                for (z = 0; z < Block.Information.VoxelPerChunk; ++z, ++f)
                {
                    WorldZ = Block.WorldZ + z + StartZ;
                    WorldY = Block.WorldY + Block.VoxelsPerAxis;

                    if (Data.Resolution == 1)
                    {
                        Data.Start();
                        Height = (float)GlobalNoise.GetValue(WorldX, WorldZ, Data) + WorldOffset.y;
                        Data.End();
                        Data.SaveInfo(f);

                        TempTypes.array[f] = GetGroundType(VoxelModule, Data, WorldX, WorldZ, (int)Height, Mathf.Max((int)Height - WorldOffset.y, 1));
                    }
                    else
                    {
                        Height = Data.GetHeightAndBiome(x, z, ref ModuleIndex);
                        Data.LoadInfo(ModuleIndex);
                    }

                    if (Height - (int)Height < TerrainInformation.Isolevel)
                        Grounds[f] = UnityEngine.Mathf.Max(GroundLevel, (int)Height + GroundLevel - 1);
                    else
                        Grounds[f] = UnityEngine.Mathf.Max(GroundLevel, (int)Height + GroundLevel);


                    DefaultType = GetGroundType(VoxelModule, Data, WorldX, WorldZ, (int)Height, (int)Height - WorldOffset.y);
                    Block.SetHeightFast(StartX + x, StartZ + z, Height + (float)GroundLevel, DefaultType);

                    if (WorldY <= (int)(Height + GroundLevel))
                    {
                        dens = -((Height + GroundLevel) - WorldY);
                        if (dens < 0) dens = 0f;
                        TempDatas.array[f] = (byte)(dens >= TerrainInformation.Isolevel ? DefaultType : (byte)0);
                    }
                    else
                        TempDatas.array[f] = 0;
                }
            }

            return true;
        }

        public override bool IsCallingCustomGenerators()
        {
            return true;
        }
    }
}
