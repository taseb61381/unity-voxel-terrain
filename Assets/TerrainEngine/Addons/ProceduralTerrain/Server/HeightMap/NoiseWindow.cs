﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;

using TerrainEngine;
using UnityEngine;

using LibNoise;
using LibNoise.Modifiers;
using LibNoise.Models;

namespace TerrainEngine
{
    [Serializable]
    public class NoiseWindow
    {
        public delegate bool InputDelegate(NoiseValue.ValueTypes VType, string Name, ref int Id, int Index);

        public int UniqueId;
        public string Name = "";
        public Rect Position = new Rect();
        public bool IsPreview = false;
        public bool IsContainer = false;
        public bool IsViewable = true;
        public bool IsBiome
        {
            get
            {
                return !String.IsNullOrEmpty(GetValue("BiomeName"));
            }
        }

        [NonSerialized,XmlIgnore]
        public bool IsLinked;

        public List<NoiseValue> Values = new List<NoiseValue>();

        public int GetValueIndex(string Name)
        {
            for (int i = Values.Count - 1; i >= 0; --i)
                if (Values[i].Name == Name)
                    return i;

            return -1;
        }


        public bool HasValue(string Name)
        {
            for (int i = Values.Count - 1; i >= 0; --i)
                if (Values[i].Name == Name)
                    return true;

            return false;
        }

        public bool HasInput
        {
            get
            {
                return BiomeNoisesContainer.HasInput(Name);
            }
        }

        public bool HasOuput
        {
            get
            {
                return BiomeNoisesContainer.HasOuput(Name);
            }
        }

        public void GetInputs(InputDelegate del)
        {
            NoiseValue Value = null;
            int Index = 0;
            for (; Index < Values.Count; )
            {
                Value = Values[Index];
                if (Value.ValueType == NoiseValue.ValueTypes.MODULE || Value.ValueType == NoiseValue.ValueTypes.VOXELMODULE)
                {
                    int Id = Value.IntValue;
                    if (del(Value.ValueType, Value.Name, ref Id, Index))
                    {
                        Value.IntValue = Id;
                    }
                }

                ++Index;
            }

            NoiseValue.ValueTypes VT = BiomeNoisesContainer.GetOutputType(Name);

            for (int i = 0; i < Values.Count; ++i)
            {
                for (int j = 0; j < Values[i].Names.Count; ++j)
                {
                    NoiseNameValues V = Values[i].Names[j];

                    if (del(VT, V.Name, ref V.ModuleId, Index))
                    {
                        Values[i].Names[j] = V;
                    }
                    ++Index;
                }
            }
        }

        public string GetValue(string Name)
        {
            for (int i = Values.Count - 1; i >= 0; --i)
            {
                if (Values[i].Name == Name)
                    return Values[i].Value;
            }

            return null;
        }

        public void InitObject(HeightNoisesContainer Container, object obj, int Seed = 0)
        {
            Type T, PT;
            T = obj.GetType();
            NoiseValue Value;
            for (int j = Values.Count - 1; j >= 0; --j)
            {
                Value = Values[j];
                PropertyInfo PInfo = T.GetProperty(Value.Name);
                FieldInfo FInfo = T.GetField(Value.Name);
                if (PInfo != null)
                {
                    PT = PInfo.PropertyType;
                }
                else if (FInfo != null)
                {
                    PT = FInfo.FieldType;
                }
                else
                    continue;

                try
                {
                    if (PT == typeof(bool))
                    {
                        if (PInfo != null) PInfo.SetValue(obj, Value.BoolValue, null);
                        else FInfo.SetValue(obj, Value.BoolValue);
                    }
                    else if (PT.IsEnum)
                    {
                        if (PInfo != null) PInfo.SetValue(obj, Enum.Parse(PT, Value.EnumValues[Value.IntValue]), null);
                        else FInfo.SetValue(obj, Enum.Parse(PT, Value.EnumValues[Value.IntValue]));
                    }
                    else if (PT == typeof(string))
                    {
                        if (PInfo != null) PInfo.SetValue(obj, Value.Value, null);
                        else FInfo.SetValue(obj, Value.Value);
                    }
                    else if (PT == typeof(int))
                    {
                        if (Value.Name == "Seed")
                        {
                            if (PInfo != null) PInfo.SetValue(obj, Value.IntValue + Seed, null);
                            else FInfo.SetValue(obj, Value.IntValue + Seed);
                        }
                        else
                        {
                            if (PInfo != null) PInfo.SetValue(obj, Value.IntValue, null);
                            else FInfo.SetValue(obj, Value.IntValue);
                        }
                    }
                    else if (PT == typeof(float))
                    {
                        if (PInfo != null) PInfo.SetValue(obj, Value.FloatValue, null);
                        else FInfo.SetValue(obj, Value.FloatValue);
                    }
                    else if (PT == typeof(double))
                    {
                        if (PInfo != null) PInfo.SetValue(obj, Value.DoubleValue, null);
                        else FInfo.SetValue(obj, Value.DoubleValue);
                    }
                    else if (PT == typeof(SList<NoiseNameValues>))
                    {
                        SList<NoiseNameValues> L = new SList<NoiseNameValues>(Value.Names.Count);

                        for (int i = 0; i < Value.Names.Count; ++i)
                        {
                            NoiseNameValues N = new NoiseNameValues();
                            N.MinNoise = Value.Names[i].MinNoise;
                            N.MaxNoise = Value.Names[i].MaxNoise;
                            N.Alpha = Value.Names[i].Alpha;
                            N.Name = Value.Names[i].Name;
                            N.NoiseModule = Container.GetModule(Value.Names[i].ModuleId) as IModule;
                            N.VoxelModule = Container.GetModule(Value.Names[i].ModuleId) as IVoxelModule;
                            L.Add(N);
                        }

                        if (PInfo != null) PInfo.SetValue(obj, L, null);
                        else FInfo.SetValue(obj, L);
                    }
                    else if (PT == typeof(SList<CurveControlPoint>))
                    {
                        SList<CurveControlPoint> L = new SList<CurveControlPoint>(Value.Curves.Count);
                        for (int i = 0; i < Value.Curves.Count; ++i)
                            L.Add(Value.Curves[i]);

                        if (PInfo != null) PInfo.SetValue(obj, L, null);
                        else FInfo.SetValue(obj, L);
                    }
                    else if (PT == typeof(SList<double>))
                    {
                        SList<double> L = new SList<double>(Value.Doubles.Count);
                        for (int i = 0; i < Value.Doubles.Count; ++i)
                            L.Add(Value.Doubles[i]);

                        if (PInfo != null) PInfo.SetValue(obj, L, null);
                        else FInfo.SetValue(obj, L);
                    }
                    else if (PT == typeof(AnimationCurve))
                    {
                        if (PInfo != null) PInfo.SetValue(obj, Value.AnimCurves, null);
                        else FInfo.SetValue(obj, Value.AnimCurves);
                    }
                    else if (PT == typeof(IModule))
                    {
                        if (obj is Teleporter)
                        {
                            int Id = Value.IntValue;
                            if (Value.IntValue == 0)
                            {
                                Id = Container.GetTeleporter((obj as Teleporter));
                            }

                            if (PInfo != null) PInfo.SetValue(obj, Container.GetModule(Id), null);
                            else FInfo.SetValue(obj, Container.GetModule(Id));
                        }
                        else
                        {
                            if (PInfo != null) PInfo.SetValue(obj, Container.GetModule(Value.IntValue), null);
                            else FInfo.SetValue(obj, Container.GetModule(Value.IntValue));
                        }
                    } /////////////////////////////////////////////////// VOXEL TYPE //////////////////////////////
                    else if (PT == typeof(IVoxelModule))
                    {
                        if (PInfo != null) PInfo.SetValue(obj, Container.GetModule(Value.IntValue), null);
                        else FInfo.SetValue(obj, Container.GetModule(Value.IntValue));
                    }
                }
                catch (Exception e)
                {
                    Values.RemoveAt(j);
                    Debug.LogError("TargetType:" + PT + ",Obj:" + obj + ",Id:" + UniqueId + ",Value:" + Value.IntValue + ",Name:" + Value.Name + "," + j + "\n" + e.ToString());
                }
            }

            if (obj is IVoxelModule)
                (obj as IVoxelModule).Init();
            else if (obj is IModule)
                (obj as IModule).Init();
        }
    }
}
