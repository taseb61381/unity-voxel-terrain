﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;

using TerrainEngine;
using UnityEngine;

using LibNoise;
using LibNoise.Modifiers;
using LibNoise.Models;

namespace TerrainEngine
{

    [Serializable]
    public class HeightNoisesContainer
    {
        public List<NoiseWindow> Windows = new List<NoiseWindow>();

        [NonSerialized, XmlIgnore]
        public Dictionary<int, object> Modules = new Dictionary<int, object>();

        public int GenerateId()
        {
            int MaxId = 10;
            for (int i = 0; i < Windows.Count; ++i)
                if (Windows[i].UniqueId > MaxId)
                    MaxId = Windows[i].UniqueId;

            return ++MaxId;
        }

        public NoiseWindow GetNoise(int Id)
        {
            if (Id < 0)
                return null;

            for (int i = 0; i < Windows.Count; ++i)
            {
                if (Windows[i].UniqueId == Id)
                    return Windows[i];
            }

            return null;
        }

        public void InitNoiseValues(NoiseWindow Window)
        {
            InitNoiseValues(BiomeNoisesContainer.GetConstructorId(Window.Name), Window);
        }

        public void InitNoiseValues(int Index, NoiseWindow N)
        {
            bool Inited = false;
            object t = null;
            object v = null;
            Type PT;

            MemberInfo[] Members = BiomeNoisesContainer.Constructors[Index].T.GetMembers();

            bool Exist = false;
            for (int i = N.Values.Count - 1; i >= 0; --i)
            {
                Exist = false;

                for (int j = 0; j < Members.Length; ++j)
                {
                    if (Members[j].Name == N.Values[i].Name)
                    {
                        Exist = true;
                        break;
                    }
                }

                if (!Exist)
                {
                    Debug.Log("Delete Invalid Value:" + N.Values[i].Name);
                    N.Values.RemoveAt(i);
                }
            }

            foreach (var F in Members)
            {
                if (F.MemberType == MemberTypes.Property || F.MemberType == MemberTypes.Field)
                {
                    if (F.MemberType == MemberTypes.Property)
                    {
                        PT = BiomeNoisesContainer.Constructors[Index].T.GetProperty(F.Name).PropertyType;
                        if (N.HasValue(F.Name))
                            continue;

                        if (Inited == false)
                        {
                            t = Activator.CreateInstance(BiomeNoisesContainer.Constructors[Index].T);
                            Inited = true;
                        }
                        v = BiomeNoisesContainer.Constructors[Index].T.GetProperty(F.Name).GetValue(t, null);
                    }
                    else
                    {
                        PT = BiomeNoisesContainer.Constructors[Index].T.GetField(F.Name).FieldType;
                        if (N.HasValue(F.Name))
                            continue;

                        if (Inited == false)
                        {
                            t = Activator.CreateInstance(BiomeNoisesContainer.Constructors[Index].T);
                            Inited = true;
                        }

                        v = BiomeNoisesContainer.Constructors[Index].T.GetField(F.Name).GetValue(t);
                    }

                    NoiseValue Value = new NoiseValue();
                    Value.Name = F.Name;

                    Debug.Log(F.Name + ",Default:" + v);

                    if (PT == typeof(string))
                    {
                        Value.ValueType = NoiseValue.ValueTypes.STRING;
                        Value.Value = (string)v;
                    }
                    else if (PT == typeof(int))
                    {
                        Value.ValueType = NoiseValue.ValueTypes.INT;
                        Value.IntValue = (int)v;
                    }
                    else if (PT == typeof(float))
                    {
                        Value.ValueType = NoiseValue.ValueTypes.FLOAT;
                        Value.Value = v.ToString();
                    }
                    else if (PT == typeof(double))
                    {
                        Value.ValueType = NoiseValue.ValueTypes.DOUBLE;
                        Value.Value = v.ToString();
                    }
                    else if (PT == typeof(SList<NoiseNameValues>))
                        Value.ValueType = NoiseValue.ValueTypes.NOISENAME;
                    else if (PT == typeof(SList<CurveControlPoint>))
                        Value.ValueType = NoiseValue.ValueTypes.CURVE;
                    else if (PT == typeof(AnimationCurve))
                        Value.ValueType = NoiseValue.ValueTypes.ANIMATIONCURVE;
                    else if (PT == typeof(SList<double>))
                        Value.ValueType = NoiseValue.ValueTypes.DOUBLELIST;
                    else if (PT == typeof(IModule))
                        Value.ValueType = NoiseValue.ValueTypes.MODULE;
                    else if (PT == typeof(IVoxelModule))
                        Value.ValueType = NoiseValue.ValueTypes.VOXELMODULE;
                    else if (PT == typeof(bool))
                    {
                        Value.ValueType = NoiseValue.ValueTypes.BOOL;
                        Value.Value = v.ToString();
                    }
                    else if (PT.IsEnum)
                    {
                        Value.ValueType = NoiseValue.ValueTypes.ENUM;
                        Value.EnumValues = Enum.GetNames(PT);
                    }

                    N.Values.Add(Value);
                }
            }
        }

        public void AddContainer(HeightNoisesContainer Container)
        {
            int MaxId = GenerateId();
            Debug.Log("Current MaxId : " + MaxId);

            foreach (var v in Container.Windows)
            {
                Debug.Log("Import Window Id:" + v.UniqueId + "," + v.Name);

                v.UniqueId += MaxId;

                v.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
                {
                    Debug.Log("Input:" + Name + ",Id:" + Id + ",Index:" + Index+",NewId:"+(Id+MaxId));
                    Id += MaxId;
                    return true;

                });

                Windows.Add(v);
            }
        }

        public NoiseWindow CreateNoise(int Index)
        {
            NoiseWindow N = new NoiseWindow();

            InitNoiseValues(Index, N);

            N.UniqueId = GenerateId();
            N.Name = BiomeNoisesContainer.Constructors[Index].Name;
            N.Position = new Rect(Screen.width / 2 - 20, Screen.height / 2 - 20, 120, 40);
            Windows.Add(N);
            return N;
        }

        public void CreateInstances()
        {
            Modules.Clear();
            NoiseWindow Window = null;
            int Index = 0;
            object obj;
            for (int i = 0; i < Windows.Count; ++i)
            {
                Window = Windows[i];
                Index = BiomeNoisesContainer.GetConstructorId(Window.Name);
                InitNoiseValues(Index, Window);
                obj = Activator.CreateInstance(BiomeNoisesContainer.Constructors[Index].T);
                Modules.Add(Window.UniqueId, obj);
            }
        }

        public void GenerateModules(int Seed = 0)
        {
            CreateInstances();

            foreach (var kp in Modules)
            {
                NoiseWindow Window = GetNoise(kp.Key);
                if (Window == null)
                {
                    Debug.LogError("Invalid Noise Window:" + kp.Key);
                    continue;
                }

                Window.InitObject(this, kp.Value, Seed);
            }
            foreach (var kp in Modules)
            {
                NoiseWindow Window = GetNoise(kp.Key);
                if (Window == null)
                {
                    Debug.LogError("Invalid Noise Window:" + kp.Key);
                    continue;
                }

                if (kp.Value is Teleporter)
                {
                    Window.InitObject(this, kp.Value, Seed);
                }
            }
        }

        public object GetModule(int Id)
        {
            object obj;
            Modules.TryGetValue(Id, out obj);
            return obj;
        }

        public int GetTeleporter(Teleporter Origin)
        {
            foreach (var kp in Modules)
            {
                if (kp.Value is Teleporter)
                {
                    if ((kp.Value as Teleporter).TeleporterId == Origin.DestinationId)
                    {
                        return kp.Key;
                    }
                }
            }

            return 0;
        }

        public void GetBiomes(List<string> Biomes)
        {
            if (Windows != null && Windows.Count > 0)
            {
                string Biome = "";

                for (int i = Windows.Count - 1; i >= 0; --i)
                {
                    Biome = Windows[i].GetValue("Name");
                    if (!string.IsNullOrEmpty(Biome))
                    {
                        if (!Biomes.Contains(Biome))
                            Biomes.Add(Biome);
                    }

                    int Index = Windows[i].GetValueIndex("NameNoises");

                    if (Index >= 0)
                    {
                        foreach (var v in Windows[i].Values[Index].Names)
                            if (!string.IsNullOrEmpty(v.Name))
                                if (!Biomes.Contains(v.Name))
                                    Biomes.Add(v.Name);
                    }
                }
            }
        }

        public IModule GetMainModule()
        {
            for (int i = 0; i < Windows.Count; ++i)
            {
                if (Windows[i].Name == "HeightMapOutput")
                {
                    return GetModule(Windows[i].UniqueId) as IModule;
                }
            }
            return null;
        }

        public VoxelTypeOuput GetMainVoxelModule()
        {
            for (int i = 0; i < Windows.Count; ++i)
            {
                if (Windows[i].Name == "VoxelTypeOutput")
                {
                    return GetModule(Windows[i].UniqueId) as VoxelTypeOuput;
                }
            }
            return null;
        }
    }
}
