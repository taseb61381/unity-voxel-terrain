﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;

using TerrainEngine;
using UnityEngine;

using LibNoise;
using LibNoise.Modifiers;
using LibNoise.Models;

namespace TerrainEngine
{
    [Serializable]
    public class NoiseValue
    {
        public enum ValueTypes
        {
            MODULE = 0,
            FLOAT = 1,
            INT = 2,
            CURVE = 3,
            STRING = 4,
            NOISENAME = 5,
            BOOL = 6,
            ENUM = 7,
            VOXELMODULE = 8,
            ANIMATIONCURVE = 9,
            DOUBLELIST = 10,
            DOUBLE = 11,
            BIOMENAMEMODULE = 12,
            VoxelNoiseModule = 13,
        }

        public string Name;
        public string Value = "";
        public ValueTypes ValueType;
        public string[] EnumValues;
        public List<NoiseNameValues> Names = new List<NoiseNameValues>();
        public List<CurveControlPoint> Curves = new List<CurveControlPoint>();
        public List<double> Doubles = new List<double>();

        [XmlIgnore]
        public AnimationCurve AnimCurves;

        [XmlIgnore]
        public int IntValue
        {
            get
            {
                if (string.IsNullOrEmpty(Value))
                    Value = "0";

                return int.Parse(Value);
            }
            set
            {
                Value = value.ToString();
            }
        }

        [XmlIgnore]
        public float FloatValue
        {
            get
            {
                if (string.IsNullOrEmpty(Value))
                    Value = "0";

                return float.Parse(Value);
            }
            set
            {
                Value = value.ToString();
            }
        }

        [XmlIgnore]
        public double DoubleValue
        {
            get
            {
                if (string.IsNullOrEmpty(Value))
                    Value = "0";

                return double.Parse(Value);
            }
            set
            {
                Value = value.ToString();
            }
        }

        [XmlIgnore]
        public bool BoolValue
        {
            get
            {
                if (string.IsNullOrEmpty(Value))
                    Value = "false";

                return bool.Parse(Value);
            }
            set
            {
                Value = value.ToString();
            }
        }

        public int GetNameIndex(string Name)
        {
            for (int i = Names.Count - 1; i >= 0; --i)
            {
                if (Names[i].Name == Name)
                    return i;
            }

            return -1;
        }

        public void RemoveNamesNotInList(List<string> L)
        {
            for (int i = Names.Count - 1; i >= 0; --i)
            {
                if (!L.Contains(Names[i].Name))
                    Names.RemoveAt(i);
            }
        }

        public void AddNamesFromList(List<string> L)
        {
            for (int i = 0; i < L.Count; ++i)
            {
                if (GetNameIndex(L[i]) == -1)
                {
                    NoiseNameValues NN = new NoiseNameValues();
                    NN.Name = L[i];
                    Names.Add(NN);
                }
            }
        }
    }
}
