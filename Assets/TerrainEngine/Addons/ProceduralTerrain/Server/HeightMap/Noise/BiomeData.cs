﻿using System.Collections.Generic;
using System.Text;
using TerrainEngine;


public struct GroundPoint
{
    public byte x, z;
    public ushort y;
    public byte Type;
    public byte Side;
    //public VoxelTypes VoxelType;

    public override string ToString()
    {
        return "(" + x + "," + y + "," + z + ")";
    }
}

public class TempSimpleTypeContainer : IPoolable
{
    public FList<BiomeData.SimpleTempObject> Objects = new FList<BiomeData.SimpleTempObject>(100);

    public override void Clear()
    {
        Objects.ClearFast();
    }
}

/// <summary>
/// Temp Datas used for generate a Block. Contains ground points and biome Name per X/Z coordinate
/// </summary>
public class BiomeData : IPoolable
{
    public struct SimpleTempObject
    {
        public byte Id;
        public ushort x,y,z;
    }

    public struct TempObject
    {
        public byte Id;
        public ushort x, y, z;
        public float Scale, Orientation;
    }

    public int[] Grounds = null;
    public TerrainDatas.LoopStepInformation LoopStep;

    public FList<VectorI3> SelectedObjects = new FList<VectorI3>(10);
    public FList<TempObject> Objects = new FList<TempObject>(1024);
    public FList<SimpleTempObject> SimpleObjects = new FList<SimpleTempObject>(1024);
    public FList<GroundPoint> GroundPoints = new FList<GroundPoint>(1024);
    public SFastRandom Random = new SFastRandom();

    // Generating Chunk
    public int GeneratorId = 0;
    public int InternalState = 0;
    public int ChunkIndex = 0;

    public int CustomId = 0;
    public int ChunkId = 0;
    public int ChunkX, ChunkZ;
    public int Index = 0;

    public BiomeData()
    {
        LoopStep = new TerrainDatas.LoopStepInformation();
    }

    public override void Clear()
    {
        Noises.ClearFast();
        ChunkId = ChunkX = ChunkZ = Index = 0;
        LoopStep.Clear();

        GeneratorId = InternalState = 0;
        Objects.ClearFast();
        SimpleObjects.ClearFast();
        GroundPoints.ClearFast();
        CustomNoisesData.ClearFast();
        SelectedObjects.ClearFast();
    }

    public int[] GetGrounds(int SizeX, int SizeZ)
    {
        if (Grounds == null)
        {
            Grounds = new int[SizeX * SizeZ];
        }

        return Grounds;
    }

    public FastDictionary<string, object> CustomData = new FastDictionary<string, object>();

    public void SetCustomData(string Name, object Data)
    {
        if (!CustomData.ContainsKey(Name))
        {
            CustomData.Add(Name, Data);
        }
        else
            CustomData[Name] = Data;
    }

    public object GetCustomData(string Name)
    {
        object obj = null;
        CustomData.TryGetValue(Name, out obj);
        return obj;
    }

    #region Biomes

    public struct CustomNoiseData
    {
        public int Name;
        public double Value;
    }

    public int Resolution = 1;
    public int VoxelPCResolution = 16;
    public int VoxelPerChunk = 16;
    public BiomeData.NoiseBuilderInfo[] Biomes;
    public SList<int> Noises = new SList<int>(10);
    public SList<CustomNoiseData> CustomNoisesData = new SList<CustomNoiseData>(10);
    public float[][] HeightMap;

    public void SetResolution(int Size,int Resolution)
    {
        this.Resolution = Resolution;
        this.VoxelPerChunk = Size;
        this.VoxelPCResolution = Size / Resolution;
        if(Resolution != 1)
        {
            VoxelPCResolution += 1;
            if (HeightMap == null)
            {
                HeightMap = new float[VoxelPCResolution][];
                for (int i = 0; i < VoxelPCResolution; ++i)
                    HeightMap[i] = new float[VoxelPCResolution];
            }
        }
    }

    public float GetHeightAndBiome(float px, float pz, ref int f)
    {
        int x = (int)(px / Resolution);
        int z = (int)(pz / Resolution);

        float sqX = (px / Resolution) - x;
        float sqZ = (pz / Resolution) - z;
        float Result = 0;

        if ((sqX + sqZ) < 1)
        {
            float triZ0 = HeightMap[x][z];
            f = x * VoxelPCResolution + z;
            Result = triZ0;
            Result += (HeightMap[x + 1][z] - triZ0) * sqX;
            Result += (HeightMap[x][z + 1] - triZ0) * sqZ;
        }
        else
        {
            float triZ3 = HeightMap[x + 1][z + 1];
            f = x * VoxelPCResolution + z;
            Result = triZ3;
            Result += (HeightMap[x + 1][z] - triZ3) * (1.0f - sqZ);
            Result += (HeightMap[x][z + 1] - triZ3) * (1.0f - sqX);
        }

        return Result;
    }

    public void LoadBiomeReso(int px,int pz)
    {
        LoadInfo(GetBiomeIndexReso(px,pz));
    }

    public int GetBiomeIndex(int px,int pz)
    {
        return px * VoxelPerChunk + pz;
    }

    public int GetBiomeIndexReso(int px,int pz)
    {
        return (px / Resolution) * VoxelPCResolution + (pz / Resolution);
    }

    public void SetCustomNoiseData(int Name,double Value)
    {
        for (int i = CustomNoisesData.Count - 1; i >= 0; --i)
        {
            if (CustomNoisesData.array[i].Name == Name)
            {
                CustomNoisesData.array[i].Value = Value;
                return;
            }
        }

        CustomNoisesData.Add(new CustomNoiseData() { Name = Name, Value = Value });
    }

    public double GetCustomNoiseData(int Name)
    {
        for (int i = CustomNoisesData.Count - 1; i >= 0; --i)
        {
            if (CustomNoisesData.array[i].Name == Name)
            {
                return CustomNoisesData.array[i].Value;
            }
        }
        return 0;
    }

    public BiomeData.NoiseBuilderInfo[] GetBiomes()
    {
        if (Biomes == null)
        {
            Noises = new SList<int>((VoxelPCResolution * VoxelPCResolution * 2));
            Biomes = new BiomeData.NoiseBuilderInfo[VoxelPCResolution * VoxelPCResolution];
        }

        return Biomes;
    }

    #endregion

    #region NoiseBuilder

    static public int MaxCustomNoises = 2;

    public struct NoiseBuilderInfo
    {
        public int StartIndex; // StartIndex in noises list
        public byte Count; // Number of noises from start
    }

    public NoiseBuilderInfo Info;
    public byte CustomIndex = 0;

    public void Start()
    {
        Info.StartIndex = Noises.Count;
        CustomIndex = 0;
    }

    public void End()
    {
        Info.Count = (byte)(Noises.Count - (int)Info.StartIndex);

        // Here we allocate more space for custom systems that will need to add data. Because when End() is called, it's not possible to add more data
        Noises.CheckArray(MaxCustomNoises);
        Noises.Count += MaxCustomNoises;
    }

    public void CleanNoises()
    {
        Noises.ClearFast();
        Info.StartIndex = 0;
        Info.Count = 0;
        CustomIndex = 0;
    }

    public void LoadInfo(int f)
    {
        CustomIndex = 0;
        Info = Biomes[f];
    }

    public void SaveInfo(int f)
    {
        CustomIndex = 0;
        Biomes[f] = Info;
    }

    public void AddCustom(int Id)
    {
        if (CustomIndex >= MaxCustomNoises)
        {
            GeneralLog.LogError("NoiseBuilder.cs : Can not AddCustom noise. MaxCustomNoises limit reached. Please change MaxCustomNoises value if your generators use more than 3 custom noises");
            return;
        }

        Noises.array[Info.StartIndex + Info.Count] = Id;
        ++Info.Count;
        ++CustomIndex;
    }

    public void SetIndex(int Index)
    {
        Noises.Count = Index;
    }

    public void RemoveIndex(int Start, int Size)
    {
        int Count = Noises.Count - (Start + Size);
        for (int i = Count-1; i >= 0; --i)
        {
            Noises.array[i + Start] = Noises.array[i + Start + Size];
        }

        Noises.Count = Count + Start;
    }

    public int GetIndex()
    {
        return Noises.Count;
    }

    public bool Contains(int Id)
    {
        for (int i = Info.Count - 1; i >= 0; --i)
        {
            if (Noises.array[i + Info.StartIndex] == Id)
                return true;
        }

        return false;
    }

    public bool Contains(int IdA, int IdB)
    {
        int Id;
        for (int i = Info.Count - 1; i >= 0; --i)
        {
            Id = Noises.array[i + Info.StartIndex];
            if (IdA != 0 && Id == IdA)
                IdA = 0;

            if (IdB != 0 && Id == IdB)
                IdB = 0;
        }

        return IdA == 0 && IdB == 0;
    }

    public bool ContainsList(List<int> Names)
    {
        int Count = Info.Count + Info.StartIndex;
        int i=0;

        for (int j = Names.Count - 1; j >= 0; --j)
        {
            for (i = Info.StartIndex; i < Count; ++i)
            {
                if (Noises.array[i] == Names[j])
                    return true;
            }
        }

        return false;
    }

    public override string ToString()
    {
        StringBuilder Builder = new StringBuilder();

        for (int i = Info.StartIndex; i < Info.Count + Info.StartIndex; ++i)
        {
            Builder.Append(Noises.array[i]);
            Builder.Append("+");
        }

        return Builder.ToString();
    }

    #endregion
}
