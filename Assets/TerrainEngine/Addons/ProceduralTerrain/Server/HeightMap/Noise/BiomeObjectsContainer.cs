using TerrainEngine;
using UnityEngine;

public class BiomeObjectsContainer : MonoBehaviour 
{
    /// <summary>
    /// List of objects per biome. Contains each object name and info (density,altitute required,etc) for each biome.
    /// </summary>
    public NoiseObjectsContainer Objects = new NoiseObjectsContainer();

    /// <summary>
    /// /Contains Noises (perlin,etc) used to place objects in group
    /// </summary>
    public NoisesContainer Container = new NoisesContainer();

    public void Init()
    {
        if (Container != null)
            Objects.Container = Container;
    }
}
