﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using UnityEngine;

[Serializable]
public class NoiseObjects
{
    public NoiseObjects()
    {

    }

    public NoiseObjects(string NoiseName)
    {
        this.NoiseName = NoiseName;
    }

    public string NoiseName = "Set BiomeNoise Name";
    public List<NoiseObject> Objects = new List<NoiseObject>();

    [NonSerialized,XmlIgnore]
    public int NoiseNameHasCode = 0; 
    
    public Vector2 Density = new Vector2(10, 100);

    public bool HasGameObject(string Name)
    {
        return Objects.Find(obj => obj.Name == Name) != null;
    }

    public NoiseObject GetObject(string Name)
    {
        return Objects.Find(obj => obj.Name == Name);
    }

    public byte IsValid(BiomeData Biome)
    {
        if (Biome.Contains(NoiseNameHasCode))
        {
            if (Biome.Random.randomIntAbs((int)Density.y * 100) > (int)Density.x * 100)
            {
                return 2;
            }

            return 0;
        }

        return 1;
    }
}