﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;

using TerrainEngine;
using UnityEngine;

using LibNoise;
using LibNoise.Modifiers;
using LibNoise.Models;

/// <summary>
/// This class contains noises and Reflected types to instanciate objects
/// </summary>
public class BiomeNoisesContainer : MonoBehaviour, ISerializationCallbackReceiver
{

    #region V11

    #region Constructors

    public struct NoiseConstructor
    {
        public string Name;
        public Type T;
        public string Category;
        public bool HasOuput;
        public bool HasInput;
        public NoiseValue.ValueTypes OutputType;
    }

    static public string[] Categories = new string[] { "Noises", "Modifiers", "Input", "VoxelType", "Output" };

    static public NoiseConstructor[] Constructors = new NoiseConstructor[]
    {
        new NoiseConstructor() { Name = "FastBillow", T = typeof(FastBillow), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "FastNoise", T = typeof(FastNoise), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "FastRidgedMultifractal", T = typeof(FastRidgedMultifractal), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "FastTurbulence", T = typeof(FastTurbulence), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Voronoi", T = typeof(Voronoi), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},

        new NoiseConstructor() { Name = "Billow", T = typeof(Billow), Category ="Noises" , HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Perlin", T = typeof(Perlin), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "RidgedMultifractal", T = typeof(RidgedMultifractal), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Turbulence", T = typeof(Turbulence), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "ConstantSpheres", T = typeof(ConstantSpheres), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},

        new NoiseConstructor() { Name = "NameNoise", T = typeof(NameNoise), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "VolcanoNoise", T = typeof(VolcanoNoise), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Crater", T = typeof(Crater), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "ClampNoise", T = typeof(ClampNoise), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "CustomNoiseData", T = typeof(CustomNoiseData), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Gabor", T = typeof(Gabor), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Constant", T = typeof(Constant), Category ="Noises", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},

        new NoiseConstructor() { Name = "Add", T = typeof(Add), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "AddThree", T = typeof(AddThree), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Multiply", T = typeof(Multiply), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Scale", T = typeof(ScaleOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Select", T = typeof(Select), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Power", T = typeof(Power), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},

        new NoiseConstructor() { Name = "Curve", T = typeof(CurveOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Terrace", T = typeof(Terrace), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Bias", T = typeof(BiasOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "ScaleBias", T = typeof(ScaleBiasOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "BiasScale", T = typeof(BiasScaleOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},

        new NoiseConstructor() { Name = "Clamp", T = typeof(ClampOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Smaller", T = typeof(SmallerOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Larger", T = typeof(LargerOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Blend", T = typeof(Blend), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "NoiseSelecterName", T = typeof(NoiseSelecterName), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "NoiseSelecter", T = typeof(NoiseSelecter), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Exponential", T = typeof(ExponentialOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Invert", T = typeof(InvertOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "ZeroToOne", T = typeof(ZeroToOneOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Absolute", T = typeof(AbsoluteOutput), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Cache", T = typeof(Cache), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "Teleporter", T = typeof(Teleporter), Category ="Modifiers", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},

        new NoiseConstructor() { Name = "TranslateInput", T = typeof(TranslateInput), Category ="Input", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "InvertInput", T = typeof(InvertInput), Category ="Input", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "ScaleInput", T = typeof(ScaleInput), Category ="Input", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "DisplaceInput", T = typeof(DisplaceInput), Category ="Input", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.MODULE},

        new NoiseConstructor() { Name = "HeightMapOutput", T = typeof(HeightMapModuleOuput), Category ="Output", HasInput = true, HasOuput = false,OutputType = NoiseValue.ValueTypes.MODULE},
        new NoiseConstructor() { Name = "VoxelTypeOutput", T = typeof(VoxelTypeOuput), Category ="Output", HasInput = true, HasOuput = false,OutputType = NoiseValue.ValueTypes.VOXELMODULE},

        new NoiseConstructor() { Name = "Type", T = typeof(VoxelTypeIdModule), Category ="VoxelType", HasInput = false, HasOuput = true,OutputType = NoiseValue.ValueTypes.VOXELMODULE},
        new NoiseConstructor() { Name = "Noise", T = typeof(VoxelTypeNoiseModule), Category ="VoxelType", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.VOXELMODULE},
        new NoiseConstructor() { Name = "Altitude", T = typeof(VoxelTypeAltitudeModule), Category ="VoxelType", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.VOXELMODULE},
        new NoiseConstructor() { Name = "Biome", T = typeof(VoxelTypeBiomeModule), Category ="VoxelType", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.VOXELMODULE},
        new NoiseConstructor() { Name = "BiomeSelecter", T = typeof(VoxelTypeBiomeSelecter), Category ="VoxelType", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.VOXELMODULE},
        new NoiseConstructor() { Name = "TypeNoiseSelecter", T = typeof(VoxelTypeNoiseSelecter), Category ="VoxelType", HasInput = true, HasOuput = true,OutputType = NoiseValue.ValueTypes.VOXELMODULE},
    };

    static public int GetConstructorId(string Name)
    {
        for (int i = Constructors.Length - 1; i >= 0; --i)
            if (BiomeNoisesContainer.Constructors[i].Name == Name)
                return i;

        return 0;
    }

    static public NoiseValue.ValueTypes GetOutputType(string Name)
    {
        for (int i = Constructors.Length - 1; i >= 0; --i)
            if (BiomeNoisesContainer.Constructors[i].Name == Name)
                return BiomeNoisesContainer.Constructors[i].OutputType;

        return 0;
    }

    static public bool HasInput(string Name)
    {
        for (int i = Constructors.Length - 1; i >= 0; --i)
            if (BiomeNoisesContainer.Constructors[i].Name == Name)
                return BiomeNoisesContainer.Constructors[i].HasInput;

        return false;
    }

    static public bool HasOuput(string Name)
    {
        for (int i = Constructors.Length - 1; i >= 0; --i)
            if (BiomeNoisesContainer.Constructors[i].Name == Name)
                return BiomeNoisesContainer.Constructors[i].HasOuput;

        return false;
    }

    #endregion

    public HeightNoisesContainer Noises;

    #endregion

    public void GetBiomes(List<NoisesContainer> ExternalContainers, List<string> Biomes)
    {
        if (Container != null)
            Container.GetBiomes(ExternalContainers, Biomes);

        if (Noises != null)
            Noises.GetBiomes(Biomes);
    }

    public void GetBiomes(List<NoisesContainer> ExternalContainers, Dictionary<string, BiomeNoise> Biomes)
    {
        if (Container != null)
            Container.GetBiomes(ExternalContainers, Biomes);
    }

    public NoisesContainer Container;
    public NoiseObjectsContainer NoiseTypes;
    public List<NoisesContainer> ExternalContainers;

    public void OnBeforeSerialize()
    {
        ExternalContainers.Clear();
        ReadContainers(Container);
    }
    public void OnAfterDeserialize()
    {
    }

    public void ReadContainers(NoisesContainer Container)
    {
        int StartId = ExternalContainers.Count;

        foreach (NoisePreset Preset in Container.NoisesPreset)
        {
            if (Preset.NoiseScript_Container != null)
            {
                Preset.ExternalIndex = AddExternal(Preset.NoiseScript_Container.Container);
            }
            else
                Preset.ExternalIndex = -1;
        }

        for (; StartId < ExternalContainers.Count; ++StartId)
        {
            ReadContainers(ExternalContainers[StartId]);
        }
    }

    public int AddExternal(NoisesContainer External)
    {
        if (External == null)
            return -1;

        if (ExternalContainers == null)
            ExternalContainers = new List<NoisesContainer>(1);

        for (int i = ExternalContainers.Count - 1; i >= 0; --i)
            if (ExternalContainers[i] == External)
                return i;

        ExternalContainers.Add(External);
        return ExternalContainers.Count - 1;
    }
}
