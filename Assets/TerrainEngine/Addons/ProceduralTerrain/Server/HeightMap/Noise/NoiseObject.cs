using LibNoise;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using TerrainEngine;
using UnityEngine;

[Serializable]
public struct NoiseObjectPosition
{
    public Vector3 WorldPosition;
    public Vector3 Size;

    [XmlIgnore]
    public int MinX, MinY, MinZ;

    [XmlIgnore]
    public int MaxX, MaxY, MaxZ;

    public void Init(float Scale)
    {
        MinX = (int)(WorldPosition.x / Scale);
        MinY = (int)(WorldPosition.y / Scale);
        MinZ = (int)(WorldPosition.z / Scale);

        MaxX = MinX + (int)(Size.x / Scale);
        MaxY = MinY + (int)(Size.y / Scale);
        MaxZ = MinZ + (int)(Size.z / Scale);

    }

    public bool IsValid(int x, int y, int z)
    {
        return x >= MinX && x < MaxX && y >= MinY && y < MaxY && z >= MinZ && z < MaxZ;
    }
}

[Serializable]
public class NoiseObject
{
    public NoiseObject()
    {

    }

    public NoiseObject(string Name)
    {
        this.Name = Name;
    }

    public string Name = "";
    public string RequiredNoise = "";   // Extra noise required in x,z. For example , Cave, River, etc. It will search on NoiseBuilder returned by HeightMap
    private int RequiredNoiseHashcode = 0;
    public string VoxelName = "";      // VoxelName : Grass,Water,Etc
    public int GroundType = 0;          // VoxelID (If voxelName != "", this value is automatically selected);
    public VoxelTypes VoxelType = VoxelTypes.OPAQUE;
    public int MinHeight = 0;
    public int MaxHeight = 9999;
    public bool AddHeightNoise = false;
    public int MinDepth = 0;
    public int MaxDepth = 9999;
    public bool AddDepthNoise = false;

    public int Chance = 100;
    public int MaxChance = 100;

    public string NoisePreset = "";
    public float MinNoise = 0;
    public float MaxNoise = 0;
    public bool AutomaticNoiseCreation = false;

    [NonSerialized]
    [XmlIgnore]
    public IObjectBaseInformations Object;

    [NonSerialized]
    [XmlIgnore]
    private IModule Module;

    public void Init(IWorldGenerator Generator, NoisesContainer Container,List<NoisesContainer> ExternalContainers, int Seed)
    {
        InitPositions(Generator.Information.Scale);

        if (Generator != null && VoxelName != null && VoxelName.Length > 0)
        {
            GroundType = Generator.GetVoxelID(VoxelName);
            VoxelType = Generator.Palette[GroundType].VoxelType;
        }

        if(RequiredNoise.Length != 0)
            RequiredNoiseHashcode = RequiredNoise.GetHashCode();
        else
            RequiredNoiseHashcode = 0;

        if (Container != null)
        {
            if (AutomaticNoiseCreation && Object != null)
            {
                Perlin Noise = new Perlin();
                Noise.Seed = Object.ObjectId;
                Noise.Frequency = 0.01f;
                Module = new LibNoise.Modifiers.ZeroToOneOutput(Noise);
                Object.NoiseScaleModule = Module;
            }
            else if (NoisePreset != null && NoisePreset.Length != 0)
            {
                NoisePreset Preset = Container.GetPreset(NoisePreset);
                if (Preset != null)
                {
                    Module = Preset.GetModule(Container,ExternalContainers, Seed);
                }
                
                if(Preset == null || Module == null)
                    Debug.LogError(Name + ": noise preset not found'" + NoisePreset + "'");
            }
        }
        else
            Debug.LogError(Generator + ": no NoisesContainer for NoiseObjects. Can not use noise");
    }

    public byte IsValid(BiomeData Biome, int Height, int Depth,
        TerrainDatas Terrain, int x, int y, int z, byte Type, VoxelTypes VType, HeightSides Side)
    {
        if (MaxChance == 0 || Object == null || Object.Chance == 0)
            return 1;

        if (Side != Object.HeightSide)
            return 2;

        if (GroundType != 0)
        {
            if (GroundType != Type)
            {
                return 3;
            }
        }
        else
        {
            if (VoxelType != VType)
                return 4;
        }

        if (MinHeight != MaxHeight && (Height < MinHeight || Height > MaxHeight))
        {
            return 5;
        }

        if (MinDepth != MaxDepth && (Depth < MinDepth || Depth > MaxDepth))
        {
            return 6;
        }

        if (RequiredNoiseHashcode != 0 && !Biome.Contains(RequiredNoiseHashcode))
            return 7;

        if (Chance == 0 || !Biome.Random.randomBool(Chance, MaxChance))
        {
            return 8;
        }

        if (Module != null)
        {
            double NoiseValue = Module.GetValue(Terrain.WorldX + x,Terrain.WorldZ + z);
            if (NoiseValue < MinNoise || NoiseValue > MaxNoise)
                return 9;
        }

        if (HasPositions && !IsInPosition(Terrain.WorldX + x, Terrain.WorldY + y, Terrain.WorldZ + z))
        {
            return 10;
        }

        return 0;
    }

    public bool IsValid(int WorldX,int WorldZ, int Height, int Depth,ref int Type)
    {
        double Noise = 0;
        if (MinHeight != MaxHeight)
        {
            // Here we check if height is between minnoise and maxnoise to avoid GetValue on all Heigth coords and Depth
            if (AddHeightNoise)
            {
                if (MinHeight + MinNoise < Height && MaxHeight + MaxNoise > Height)
                {
                    if (Module != null)
                    {

                        Noise = Module.GetValue(WorldX, WorldZ);

                        if (Noise < MinNoise)
                            Noise = MinNoise;
                        else if (Noise > MaxNoise)
                            Noise = MaxNoise;
                    }
                }
            }

            if (Height < MinHeight - Noise || Height > MaxHeight + Noise)
                return false;
        }

        if (MinDepth != MaxDepth)
        {
            if (AddDepthNoise && (MinDepth + MinNoise < Depth && MaxDepth + MaxNoise > Depth))
            {
                if (Noise == 0 && Module != null)
                {
                    Noise = Module.GetValue(WorldX, WorldZ);

                    if (Noise < MinNoise)
                        Noise = MinNoise;
                    else if (Noise > MaxNoise)
                        Noise = MaxNoise;
                }
            }
            else
                Noise = 0;

            if (Depth < MinDepth - Noise || Depth > MaxDepth + Noise)
                return false;
        }

        Type = GroundType;
        return true;
    }

    public void CopyFrom(NoiseObject Other)
    {
        GroundType = Other.GroundType;
        VoxelType = Other.VoxelType;
        MaxHeight = Other.MaxHeight;
        MinHeight = Other.MinHeight;
        MaxDepth = Other.MaxDepth;
        MinDepth = Other.MinDepth;
        Chance = Other.Chance;
        MaxChance = Other.MaxChance;
        RequiredNoise = Other.RequiredNoise;

        NoisePreset = Other.NoisePreset;
        MinNoise = Other.MinNoise;
        MaxNoise = Other.MaxNoise;
        AutomaticNoiseCreation = Other.AutomaticNoiseCreation;

        if (Positions != null)
            Other.Positions = Positions.Clone() as NoiseObjectPosition[];
        else
            Other.Positions = null;
    }

    public bool HasPositions = false;
    public NoiseObjectPosition[] Positions;

    public void InitPositions(float Scale)
    {
        HasPositions = Positions != null && Positions.Length > 0;
        if (HasPositions)
        {
            for (int i = 0; i < Positions.Length; ++i)
                Positions[i].Init(Scale);
        }
    }

    public bool IsInPosition(int x, int y, int z)
    {
        for (int i = Positions.Length - 1; i >= 0; --i)
            if (Positions[i].IsValid(x, y, z))
                return true;

        return false;
    }
}