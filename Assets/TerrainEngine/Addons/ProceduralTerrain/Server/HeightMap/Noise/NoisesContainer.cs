﻿using LibNoise;
using System;
using System.Collections.Generic;
using System.IO;

namespace TerrainEngine
{
    [Serializable]
    public class NoisesContainer
    {
        public List<BiomeNoise> Noises = new List<BiomeNoise>();
        public List<NoisePreset> NoisesPreset = new List<NoisePreset>();

        public bool IsValid()
        {
            return Noises != null && NoisesPreset != null;
        }

        public BiomeNoise GetNoise(string Name)
        {
            foreach (BiomeNoise Noise in Noises)
                if (Noise.Name == Name)
                    return Noise;

            return null;
        }

        public IModule GetNoiseModule(string Name)
        {
            if (String.IsNullOrEmpty(Name))
                return null;

            foreach (BiomeNoise Noise in Noises)
                if (Noise.Name == Name)
                    return Noise.GetModule();

            return null;
        }

        public int GetNoise(BiomeNoise Noise)
        {
            for (int i = 0; i < Noises.Count; ++i)
                if (Noises[i] == Noise)
                    return i;

            return 0;
        }

        public NoisePreset GetPreset(string Name)
        {
            if (Name == null || Name.Length <= 0)
                return null;

            foreach (NoisePreset Noise in NoisesPreset)
                if (Noise.Name == Name)
                    return Noise;

            return null;
        }

        public IModule GetPresetModule(NoisesContainer Biome,List<NoisesContainer> ExternalContainers, int Seed, string Name)
        {
            NoisePreset Preset = GetPreset(Name);
            if (Preset != null)
                return Preset.GetModule(Biome, ExternalContainers, Seed);
            return null;
        }

        public NoisePreset GetPreset(int Id)
        {
            if (Id >= NoisesPreset.Count)
                return null;
            else
                return NoisesPreset[Id];
        }

        public int GetPreset(NoisePreset Preset)
        {
            for (int i = 0; i < NoisesPreset.Count; ++i)
                if (NoisesPreset[i] == Preset)
                    return i;

            return 0;
        }

        public NoisePreset GetPreset(BiomeNoise Noise)
        {
            foreach (NoisePreset Preset in NoisesPreset)
                if (Preset.Computation == NoiseComputations.NOISE && Preset.NoiseName == Noise.Name)
                    return Preset;

            return null;
        }

        public bool IsNoisePreset(BiomeNoise Noise)
        {
            NoisePreset N = GetPreset(Noise);
            if (N == null)
                return false;

            foreach (NoisePreset Preset in NoisesPreset)
                if (Preset.PresetA == N.Name || Preset.PresetB == N.Name || Preset.NoiseName == Noise.Name)
                    return true;

            return false;
        }

        public void RemovePreset(NoisePreset Preset)
        {
            NoisesPreset.Remove(Preset);

            foreach (NoisePreset Check in NoisesPreset)
            {
                if (Check == null || Preset == null)
                    continue;

                if (Check.PresetA == Preset.Name)
                    Check.PresetA = "";

                if (Check.PresetB == Preset.Name)
                    Check.PresetB = "";

                if (Check.PresetC == Preset.Name)
                    Check.PresetC = "";
            }
        }

        public void RemoveNoise(BiomeNoise Noise)
        {
            Noises.Remove(Noise);

            foreach (NoisePreset Preset in NoisesPreset)
                if (Preset.NoiseName == Noise.Name)
                    Preset.NoiseName = "";
        }

        public IModule GetGlobalNoise(List<NoisesContainer> ExternalContainers, int Seed)
        {
            NoisePreset Main = GetPreset("Main");

            if (Main != null)
                return Main.GetModule(this,ExternalContainers, Seed);
            else
                return null;
        }

        /// <summary>
        /// Return all biome names used by generator. Use this for list all differents biomes names
        /// </summary>
        public void GetBiomes(List<NoisesContainer> ExternalContainers, List<string> Biomes)
        {

            foreach (BiomeNoise Noise in Noises)
            {
                if (Noise == null || !IsNoisePreset(Noise))
                    continue;

                if (Noise.NoiseType == BiomeNoise.NoiseTypes.NameNoise && !Biomes.Contains(Noise.Name))
                {
                    Biomes.Add(Noise.Name);
                }

                if (Noise.PreCalculations != null)
                {
                    foreach (BiomeNoise.PrecalculationInformations Pre in Noise.PreCalculations)
                    {
                        if (Pre.Name != BiomeNoise.BiomePreCalculationTypes.NoiseSelecterName)
                            continue;

                        foreach (NoiseNameValues SubNoises in Pre.NameNoises)
                        {
                            if (!Biomes.Contains(SubNoises.Name))
                            {
                                Biomes.Add(SubNoises.Name);
                            }
                        }
                    }
                }

                if (Noise.NoiseType == BiomeNoise.NoiseTypes.VolcanoNoise)
                {
                    string VolcanoCenter = Noise.Name + "center";
                    string VolcanoRiver = Noise.Name + "river";

                    if (!Biomes.Contains(Noise.Name))
                    {
                        Biomes.Add(Noise.Name);
                    }

                    if (!Biomes.Contains(VolcanoCenter))
                    {
                        Biomes.Add(VolcanoCenter);
                    }

                    if (!Biomes.Contains(VolcanoRiver))
                    {
                        Biomes.Add(VolcanoRiver);
                    }
                }
            }

            foreach (NoisePreset Preset in NoisesPreset)
            {
                if (Preset == null || Preset.Computation != NoiseComputations.NOISES_CONTAINER)
                    continue;

                if (Preset.ExternalIndex == -1)
                    continue;

                ExternalContainers[Preset.ExternalIndex].GetBiomes(ExternalContainers, Biomes);
            }
        }

        /// <summary>
        /// Return all biome names used by generator. Use this for list all differents biomes names
        /// </summary>
        public void GetBiomes(List<NoisesContainer> ExternalContainers, Dictionary<string, BiomeNoise> Biomes)
        {
            
            foreach (BiomeNoise Noise in Noises)
            {
                if (Noise == null || !IsNoisePreset(Noise))
                    continue;

                if (Noise.NoiseType == BiomeNoise.NoiseTypes.NameNoise && !Biomes.ContainsKey(Noise.Name))
                {
                    Biomes.Add(Noise.Name, Noise);
                }

                if (Noise.PreCalculations != null)
                {
                    foreach (BiomeNoise.PrecalculationInformations Pre in Noise.PreCalculations)
                    {
                        if(Pre.Name != BiomeNoise.BiomePreCalculationTypes.NoiseSelecterName)
                            continue;

                        foreach (NoiseNameValues SubNoises in Pre.NameNoises)
                        {
                            if (!Biomes.ContainsKey(SubNoises.Name))
                            {
                                Biomes.Add(SubNoises.Name, null);
                            }
                        }
                    }
                }

                if (Noise.NoiseType == BiomeNoise.NoiseTypes.VolcanoNoise)
                {
                    string VolcanoCenter = Noise.Name + "center";
                    string VolcanoRiver = Noise.Name + "river";

                    if (!Biomes.ContainsKey(Noise.Name))
                    {
                        Biomes.Add(Noise.Name, Noise);
                    }

                    if (!Biomes.ContainsKey(VolcanoCenter))
                    {
                        Biomes.Add(VolcanoCenter, Noise);
                    }

                    if (!Biomes.ContainsKey(VolcanoRiver))
                    {
                        Biomes.Add(VolcanoRiver, Noise);
                    }
                }
            }

            foreach (NoisePreset Preset in NoisesPreset)
            {
                if (Preset == null || Preset.Computation != NoiseComputations.NOISES_CONTAINER)
                    continue;

                if (Preset.ExternalIndex == -1)
                    continue;

                ExternalContainers[Preset.ExternalIndex].GetBiomes(ExternalContainers, Biomes);
            }
        }

        static public void Save(string Path, NoisesContainer Container)
        {
            XmlMgr.SaveXMLFile(Directory.GetCurrentDirectory() + "/" + Path, Container);
        }

        static public NoisesContainer Load(string Path)
        {
            NoisesContainer Container = XmlMgr.GetXMLFile<NoisesContainer>(Directory.GetCurrentDirectory() + "/" + Path);
            return Container;
        }
    }
}
