﻿using LibNoise;
using LibNoise.Modifiers;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace TerrainEngine
{
    public enum NoiseComputations
    {
        NOISE,
        ADD,
        MULTIPLY,
        SMALLER_OUTPUT,
        POWER,
        LARGER_OUTPUT,
        SELECT,
        BLEND,
        NOISESELECTER,
        NOISES_CONTAINER,
        TERRACE,
    };

    [Serializable]
    public class NoisePreset
    {
        public string Name = "";

        public NoiseComputations Computation;
        public string NoiseName;
        public string PresetA;
        public string PresetB;
        public string PresetC;

        public float EdgeFalloff;
        public Vector2 Bounds;
        public List<NoiseNameValues> NameNoises = new List<NoiseNameValues>();
        public List<float> ControlPoints = new List<float>();
        public bool InvertTerrace = false;

        [XmlIgnore]
        public BiomeNoisesContainer NoiseScript_Container = null;
        public int ExternalIndex = -1;
        public string MainNoise_Container = "Main"; // Preset to use from Noises_Container. This preset will return another preset contained in NoisesContainer;

        public NoisePreset()
        {
            Name = DateTime.Now.Ticks.ToString();
        }

        public NoisePreset(string Name)
        {
            this.Name = Name;
        }

        public IModule GetModule(NoisesContainer Biome,List<NoisesContainer> ExternalContainers, int Seed = 0)
        {
            try
            {
                switch (Computation)
                {
                    case NoiseComputations.ADD:
                        {
                            NoisePreset A = Biome.GetPreset(PresetA);
                            NoisePreset B = Biome.GetPreset(PresetB);
                            if (A == null || B == null)
                                return null;

                            return new Add(A.GetModule(Biome, ExternalContainers, Seed), B.GetModule(Biome, ExternalContainers, Seed));
                        }
                    case NoiseComputations.BLEND:
                        {
                            NoisePreset A = Biome.GetPreset(PresetA);
                            NoisePreset B = Biome.GetPreset(PresetB);
                            NoisePreset C = Biome.GetPreset(PresetC);
                            if (A == null || B == null || C == null)
                                return null;

                            return new Blend(A.GetModule(Biome, ExternalContainers, Seed), B.GetModule(Biome, ExternalContainers, Seed), C.GetModule(Biome, ExternalContainers, Seed));
                        }
                    case NoiseComputations.LARGER_OUTPUT:
                        {
                            NoisePreset A = Biome.GetPreset(PresetA);
                            NoisePreset B = Biome.GetPreset(PresetB);
                            if (A == null || B == null)
                                return null;

                            return new LargerOutput(A.GetModule(Biome, ExternalContainers, Seed), B.GetModule(Biome, ExternalContainers, Seed));
                        }
                    case NoiseComputations.MULTIPLY:
                        {
                            NoisePreset A = Biome.GetPreset(PresetA);
                            NoisePreset B = Biome.GetPreset(PresetB);
                            if (A == null || B == null)
                                return null;

                            return new Multiply(A.GetModule(Biome, ExternalContainers, Seed), B.GetModule(Biome, ExternalContainers, Seed));
                        }
                    case NoiseComputations.POWER:
                        {
                            NoisePreset A = Biome.GetPreset(PresetA);
                            NoisePreset B = Biome.GetPreset(PresetB);
                            if (A == null || B == null)
                                return null;

                            return new Power(A.GetModule(Biome, ExternalContainers, Seed), B.GetModule(Biome, ExternalContainers, Seed));
                        }
                    case NoiseComputations.SELECT:
                        {
                            NoisePreset A = Biome.GetPreset(PresetA);
                            NoisePreset B = Biome.GetPreset(PresetB);
                            NoisePreset C = Biome.GetPreset(PresetC);
                            if (A == null || B == null || C == null)
                                return null;

                            Select Sel = new Select(C.GetModule(Biome, ExternalContainers, Seed), A.GetModule(Biome, ExternalContainers, Seed), B.GetModule(Biome, ExternalContainers, Seed));
                            Sel.SetBounds(Bounds.x, Bounds.y);
                            Sel.EdgeFalloff = EdgeFalloff;
                            return Sel;
                        }
                    case NoiseComputations.SMALLER_OUTPUT:
                        {
                            NoisePreset A = Biome.GetPreset(PresetA);
                            NoisePreset B = Biome.GetPreset(PresetB);
                            if (A == null || B == null)
                                return null;

                            return new SmallerOutput(A.GetModule(Biome, ExternalContainers, Seed), B.GetModule(Biome, ExternalContainers, Seed));
                        }
                    case NoiseComputations.NOISESELECTER:
                        {
                            for (int i = 0; i < NameNoises.Count; ++i)
                            {
                                NoiseNameValues NN = NameNoises[i];
                                NN.NoiseModule = Biome.GetPresetModule(Biome,ExternalContainers, Seed, NameNoises[i].Name);
                                if (NN.NoiseModule == null)
                                    GeneralLog.LogError(Name + " Invalid Module :" + NN.Name);
                                NameNoises[i] = NN;
                            }

                            NoiseSelecter Sel = new NoiseSelecter(Biome.GetPresetModule(Biome,ExternalContainers, Seed, PresetC), NameNoises);
                            return Sel;
                        }
                    case NoiseComputations.NOISES_CONTAINER:
                        {
                            if (ExternalIndex == -1)
                                return null;

                            NoisePreset Preset = ExternalContainers[ExternalIndex].GetPreset(MainNoise_Container);
                            if (Preset == null)
                                return null;

                            IModule Mod = Preset.GetModule(ExternalContainers[ExternalIndex],ExternalContainers, Seed);
                            return Mod;
                        }

                    case NoiseComputations.TERRACE:
                        {
                            NoisePreset A = Biome.GetPreset(PresetA);
                            if (A == null)
                                return null;

                            Terrace Ter = new Terrace(A.GetModule(Biome, ExternalContainers, Seed));
                            Ter.ControlPoints = new SList<double>(ControlPoints.Count);
                            foreach (float f in ControlPoints)
                                Ter.ControlPoints.Add(f);
                            Ter.InvertTerraces = InvertTerrace;
                            return Ter;
                        };
                };
            }
            catch (Exception e)
            {
                GeneralLog.LogError("NoisePreset: " + Name + ",PresetA:" + PresetA + ",B:" + PresetB + ",C:" + PresetC + ",Noise:" + NoiseName + "\n" + e.ToString());
            }

            BiomeNoise Noise = Biome.GetNoise(NoiseName);
            if (Noise == null)
                return null;

            Noise.Init(Seed);

            return Noise.Noise;
        }
    }
}
