using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

/// <summary>
/// Contains all Objects per biome and all requirements for each object to be spawnable on each biome. 
/// Example :
/// On biome desert, plant01 can be spawned only upper height 50 and and on ground type sand.
/// </summary>
[Serializable]
public class NoiseObjectsContainer
{
    [NonSerialized]
    public NoisesContainer Container;

    public List<NoiseObjects> NoisesObjects = new List<NoiseObjects>();

    [NonSerialized]
    public bool HasSide0 = false;
    [NonSerialized]
    public bool HasSide1 = false;
    [NonSerialized]
    public bool HasSide2 = false;

    public NoiseObjects this[int id]
    {
        get
        {
            return NoisesObjects[id];
        }
    }

    public int Count
    {
        get
        {
            return NoisesObjects.Count;
        }
    }

    public void Init(IWorldGenerator Generator,List<NoisesContainer> ExternalContainers, int Seed)
    {
        foreach (NoiseObjects Objs in NoisesObjects)
        {
            Objs.NoiseNameHasCode = Objs.NoiseName.GetHashCode();

            foreach (NoiseObject Obj in Objs.Objects)
            {
                Obj.Init(Generator, Container,ExternalContainers, Seed);
                if(Obj.Object != null)
                {
                    if (Obj.Object.HeightSide == HeightSides.BOTTOM)
                        HasSide0 = true;
                    if (Obj.Object.HeightSide == HeightSides.TOP)
                        HasSide1 = true;
                    if (Obj.VoxelType == VoxelTypes.FLUID)
                        HasSide2 = true;
                }
            }
        }
    }

    public NoiseObjects GetNoise(string NoiseName)
    {
        foreach (NoiseObjects Info in NoisesObjects)
            if (Info.NoiseName == NoiseName)
                return Info;

        return null;
    }

    public bool HasObject(string NoiseName, string Name)
    {
        NoiseObjects Noise = GetNoise(NoiseName);
        if (Noise == null)
            return false;

        return Noise.HasGameObject(Name);
    }

    public void RemoveObject(string NoiseName, string Name)
    {
        NoiseObjects Noise = GetNoise(NoiseName);
        if (Noise == null)
            return;

        Noise.Objects.RemoveAll(obj => obj.Name == Name);
    }

    public byte IsValid(BiomeData Builder,BiomeData Data, int GlobalChance, int GlobalChanceMax)
    {
        if (GlobalChance == 0)
            return 1;

        if (!Data.Random.randomBool(GlobalChance, GlobalChanceMax))
            return 2;

        return 0;
    }
}