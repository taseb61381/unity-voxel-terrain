using LibNoise;
using LibNoise.Modifiers;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class BiomeNoise
    {
        public enum NoiseTypes
        {
            Billow,
            FastBillow,
            FastNoise,
            FastRidgedMultifractal,
            FastTurbulence,
            Perlin,
            RidgedMultifractal,
            Spheres,
            Turbulence,
            Voronoi,
            NameNoise,
            ConstantSpheres,
            VolcanoNoise,
        };

        public enum BiomePreCalculationTypes
        {
            ExponentialOutput,
            ClampOutput,
            InvertOutput,
            InvertInput,
            Power,
            BiasOutput,
            ScaleBias,
            ScaleOutput,
            ScaleInput,
            NoiseSelecterName,
            ZeroToOneOutput,
            BiasScaleOutput,
            TerraceOutput,
            CurveOutput,
            Turbulence,
        };

        [Serializable]
        public class PrecalculationInformations
        {
            public BiomePreCalculationTypes Name = BiomePreCalculationTypes.BiasOutput;
            public float LowerBound;
            public float UpperBound;
            public float Exponent;
            public float Scale;
            public float Bias;
            public int Roughtness;
            public Vector3 ScaleInput = new Vector3();
            public string OptionalNoise1;
            public string OptionalNoise2;
            public List<NoiseNameValues> NameNoises;
            public List<float> ControlPoints = new List<float>();
        }

        public delegate float FormulaFunction(float a, float b);

        public string Name = "";

        public int SeedOffset = 0;
        public NoiseTypes NoiseType = NoiseTypes.Perlin;

        public int Octaves = 9;
        public float Frequency = 1f;
        public float YFrequency = 1f;
        public float Persistence = 0.5f;
        public float Lacunarity = 2.0f;
        public float CraterSize = 0.1f;
        public float CraterHeight = 0.1f;
        public List<PrecalculationInformations> PreCalculations;

        [NonSerialized]
        private IModule _noise;
        public IModule Noise
        {
            get
            {
                return _noise;
            }
        }

        public void Init(int Seed)
        {
            switch (NoiseType)
            {
                case NoiseTypes.Perlin:
                    _noise = new Perlin();
                    ((Perlin)_noise).Seed = Seed + SeedOffset;
                    ((Perlin)_noise).Lacunarity = Lacunarity;
                    ((Perlin)_noise).OctaveCount = Octaves;
                    ((Perlin)_noise).Persistence = Persistence;
                    ((Perlin)_noise).Frequency = 0.998f;
                    break;

                case NoiseTypes.Billow:
                    _noise = new Billow();
                    ((Billow)_noise).Seed = Seed + SeedOffset;
                    ((Billow)_noise).Lacunarity = Lacunarity;
                    ((Billow)_noise).OctaveCount = Octaves;
                    ((Billow)_noise).Persistence = Persistence;
                    ((Billow)_noise).Frequency = 0.998f;
                    break;

                case NoiseTypes.FastBillow:
                    _noise = new FastBillow();
                    ((FastBillow)_noise).Seed = Seed + SeedOffset;
                    ((FastBillow)_noise).Lacunarity = Lacunarity;
                    ((FastBillow)_noise).OctaveCount = Octaves;
                    ((FastBillow)_noise).Persistence = Persistence;
                    ((FastBillow)_noise).Frequency = 0.998f;
                    break;

                case NoiseTypes.FastNoise:
                    _noise = new FastNoise();
                    ((FastNoise)_noise).Seed = Seed + SeedOffset;
                    ((FastNoise)_noise).Lacunarity = Lacunarity;
                    ((FastNoise)_noise).OctaveCount = Octaves;
                    ((FastNoise)_noise).Persistence = Persistence;
                    ((FastNoise)_noise).Frequency = 0.998f;
                    break;

                case NoiseTypes.FastRidgedMultifractal:
                    _noise = new FastRidgedMultifractal();
                    ((FastRidgedMultifractal)_noise).Seed = Seed + SeedOffset;
                    ((FastRidgedMultifractal)_noise).Lacunarity = Lacunarity;
                    ((FastRidgedMultifractal)_noise).OctaveCount = Octaves;
                    ((FastRidgedMultifractal)_noise).Frequency = 0.998f;
                    break;

                case NoiseTypes.FastTurbulence:
                    _noise = new FastTurbulence(new Perlin());
                    ((FastTurbulence)_noise).Seed = Seed + SeedOffset;
                    ((FastTurbulence)_noise).Frequency = 0.998f;
                    break;

                case NoiseTypes.RidgedMultifractal:
                    _noise = new RidgedMultifractal();
                    ((RidgedMultifractal)_noise).Seed = Seed + SeedOffset;
                    ((RidgedMultifractal)_noise).Lacunarity = Lacunarity;
                    ((RidgedMultifractal)_noise).OctaveCount = Octaves;
                    ((RidgedMultifractal)_noise).Frequency = 0.998f;
                    break;

                case NoiseTypes.Spheres:
                    _noise = new Spheres();
                    ((Spheres)_noise).Frequency = 1f;
                    break;

                case NoiseTypes.Turbulence:
                    _noise = new Turbulence(new Perlin());
                    ((Turbulence)_noise).Seed = Seed + SeedOffset;
                    ((Turbulence)_noise).Frequency = 1f;
                    break;

                case NoiseTypes.Voronoi:
                    _noise = new Voronoi();
                    ((Voronoi)_noise).Seed = Seed + SeedOffset;
                    ((Voronoi)_noise).Frequency = 1f;
                    break;

                case NoiseTypes.NameNoise:
                    _noise = new NameNoise();
                    (_noise as NameNoise).Name = Name;
                    break;
                case NoiseTypes.ConstantSpheres:
                    _noise = new ConstantSpheres();
                    ((ConstantSpheres)_noise).SphereSize = Lacunarity;
                    ((ConstantSpheres)_noise).MinNoise = Persistence;
                    ((ConstantSpheres)_noise).SphereNoise = YFrequency;
                    break;
                case NoiseTypes.VolcanoNoise:
                    _noise = new VolcanoNoise();
                    ((VolcanoNoise)_noise).VolcanoSize = Lacunarity;
                    ((VolcanoNoise)_noise).MinNoise = Persistence;
                    ((VolcanoNoise)_noise).PerlinFrequency = YFrequency;
                    ((VolcanoNoise)_noise).CraterSize = CraterSize;
                    ((VolcanoNoise)_noise).CraterHeight = CraterHeight;
                    break;
            };

            _noise.Init();

            _noise = new ScaleInput(_noise, Frequency, YFrequency, Frequency);

            AddPrecalculation(ref _noise, PreCalculations);
        }

        static public void AddPrecalculation(ref IModule Noise, List<PrecalculationInformations> PreCalculations)
        {
            if(PreCalculations != null)
            for (int i = 0; i < PreCalculations.Count; ++i)
            {
                PrecalculationInformations Info = PreCalculations[i];
                switch (Info.Name)
                {
                    case BiomePreCalculationTypes.ClampOutput:
                        Noise = new ClampOutput(Noise);
                        ((ClampOutput)Noise).SetBounds(Info.LowerBound, Info.UpperBound);
                        break;

                    case BiomePreCalculationTypes.ExponentialOutput:
                        Noise = new ExponentialOutput(Noise, Info.Exponent);
                        break;

                    case BiomePreCalculationTypes.InvertInput:
                        Noise = new InvertInput(Noise);
                        break;

                    case BiomePreCalculationTypes.InvertOutput:
                        Noise = new InvertOutput(Noise);
                        break;

                    case BiomePreCalculationTypes.ScaleBias:
                        Noise = new ScaleBiasOutput(Noise);
                        ((ScaleBiasOutput)Noise).Scale = Info.Scale;
                        ((ScaleBiasOutput)Noise).Bias = Info.Bias;
                        break;
                    case BiomePreCalculationTypes.ZeroToOneOutput:
                        Noise = new BiasScaleOutput(Noise);
                        (Noise as BiasScaleOutput).Bias = 1;
                        (Noise as BiasScaleOutput).Scale = 0.5;
                        break;

                    case BiomePreCalculationTypes.BiasOutput:
                        Noise = new BiasOutput(Noise, Info.Bias);
                        break;

                    case BiomePreCalculationTypes.ScaleInput:
                        Noise = new ScaleInput(Noise, Info.ScaleInput.x, Info.ScaleInput.y, Info.ScaleInput.z);
                        break;
                    case BiomePreCalculationTypes.BiasScaleOutput:
                        Noise = new BiasScaleOutput(Noise);
                        ((BiasScaleOutput)Noise).Scale = Info.Scale;
                        ((BiasScaleOutput)Noise).Bias = Info.Bias;
                        break;

                    case BiomePreCalculationTypes.ScaleOutput:
                        Noise = new ScaleOutput(Noise, Info.Scale);
                        break;
                    case BiomePreCalculationTypes.NoiseSelecterName:
                        Noise = new NoiseSelecterName(Noise, Info.NameNoises);
                        break;
                    case BiomePreCalculationTypes.TerraceOutput:
                        Noise = new Terrace(Noise);
                        (Noise as Terrace).ControlPoints = new SList<double>(Info.ControlPoints.Count);
                        foreach (float f in Info.ControlPoints)
                            (Noise as Terrace).ControlPoints.Add(f);
                        break;
                    case BiomePreCalculationTypes.CurveOutput:
                        Noise = new CurveOutput(Noise);
                        (Noise as CurveOutput).ControlPoints = new SList<CurveControlPoint>(Info.ControlPoints.Count/2);
                        if (Info.ControlPoints.Count % 2 != 0)
                            Info.ControlPoints.Clear();

                        for (int j = 0; j < Info.ControlPoints.Count; j += 2)
                            (Noise as CurveOutput).AddControlPoint(Info.ControlPoints[j], Info.ControlPoints[j + 1]);
                            break;
                    case BiomePreCalculationTypes.Turbulence:
                            Noise = new Turbulence(Noise);
                            (Noise as Turbulence).Power = Info.Bias;
                            (Noise as Turbulence).Frequency = Info.Scale;
                            (Noise as Turbulence).Roughness = Info.Roughtness;
                            break;
                };
            }

        }

        public float GetNoise(float x, float y, float z)
        {
            return (float)Noise.GetValue(x, y, z);
        }

        [NonSerialized, XmlIgnore]
        public Texture2D PreviewTexture;

        public IModule GetModule()
        {
            Init(0);
            return _noise;
        }

        public Texture2D GetPreview(int SizeX, int SizeY, bool Update)
        {
            IModule Mod = GetModule();
            if (Mod == null)
                return null;

            if (Update)
            {
                if (PreviewTexture == null || PreviewTexture.width != SizeX || PreviewTexture.height != SizeY)
                    PreviewTexture = new Texture2D(SizeX, SizeY);

                int x, y;
                float noise = 0;
                Color Col = new Color();
                for (x = 0; x < SizeX; ++x)
                {
                    for (y = 0; y < SizeY; ++y)
                    {
                        noise = (float)(Mod.GetValue(x, y) + 1f) * 0.5f;
                        Col.r = Col.g = Col.b = noise;
                        Col.a = 1;
                        PreviewTexture.SetPixel(x, y, Col);
                    }
                }

                PreviewTexture.Apply();
            }

            return PreviewTexture;
        }


    }
}