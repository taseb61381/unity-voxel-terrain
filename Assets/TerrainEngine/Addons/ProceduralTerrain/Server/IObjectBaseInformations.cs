﻿using LibNoise;
using System;
using System.Xml.Serialization;
using UnityEngine;

public enum HeightSides
{
    BOTTOM = 0,
    TOP = 1,
};

/// <summary>
/// Base class for all objects that can be placed by a generator on terrain. Contain name/scale/ObjectId/Max alignement clamp to terrain/Top or Bottom object/
/// </summary>
[Serializable]
public abstract class IObjectBaseInformations
{
    private string _Name;
    public string Name
    {
        get
        {
            if (_Name == null || _Name.Length == 0)
                _Name = GetName();

            return _Name;
        }
        set
        {
            _Name = value;
        }
    }
    public virtual string GetName()
    {
        return "";
    }

    [NonSerialized]
    public int ObjectId;                                    // ObjectId = position on the objects array

    public HeightSides HeightSide = HeightSides.BOTTOM;     // Bottom for objects on the ground. Top for lianas for example.
    public int Chance = 1;                                  // Chance between all objects avaiable for on voxel. Example : Grass1,Grass2,Grass3 can be spawned on Forest at x,y,z. if Grass1 have chance=3 and grass2 only 1 chance. Grass3 will be spawned more frequently
    public bool AlignToTerrain = true;                      // If this value is true, object rotation will follow current terrain alignement. It can be great for small plants or trees, but with big objects you need to use clamp.
    public float AlignmentClamp = 0.3f;                    // Y Rotation max for an object. 0 = no clamp, 1 = not rotation. 0.3 is 30% of 180°.

    public float AlignementClampGetSet
    {
        get
        {
            return AlignmentClamp;
        }
        set
        {
            AlignmentClamp = Mathf.Clamp(value, 0, 1);
        }
    }

    public Vector2 SlopeLimits = new Vector2(0, 0); 

    /// <summary>
    /// NoisePreset name used for generate random scale. used to create homogeneous bush.
    /// </summary>
    [XmlIgnore]
    public string NoiseNameScale = "";

    [XmlIgnore]
    public IModule NoiseScaleModule = null; // Moise Module, this module must return 0 to 1 value. This value will be used to generate height. MinHeight + (Max-Min)*Noise.

    public virtual void CopyFrom(IObjectBaseInformations Other)
    {
        this.HeightSide = Other.HeightSide;
        this.Chance = Other.Chance;
        this.AlignmentClamp = Other.AlignmentClamp;
        this.AlignToTerrain = Other.AlignToTerrain;
        this.NoiseNameScale = Other.NoiseNameScale;
        this.SlopeLimits = Other.SlopeLimits;
    }


    public virtual Vector3 GetScale(ref SFastRandom FRandom, int x, int y, int z)
    {
        return new Vector3(1, 1, 1);
    }

    public virtual float GetScaleF(ref SFastRandom FRandom, int x, int y, int z)
    {
        return 1f;
    }

    public virtual float GetScaleF(float Random)
    {
        return 1f;
    }

    public virtual Vector3 GetOrientation(int x, int y, int z)
    {
        return new Vector3();
    }

    public virtual Texture2D GetTexture(int x, int y, int z)
    {
        return null;
    }

    public virtual GameObject GetObject(int x, int y, int z)
    {
        return null;
    }
}
