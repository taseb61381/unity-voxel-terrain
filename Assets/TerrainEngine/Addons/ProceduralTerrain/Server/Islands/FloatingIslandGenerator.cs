﻿using LibNoise;
using System;
using System.Xml.Serialization;
using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class FloatingIslandGenerator : IWorldGenerator
    {
        public string RequiredBiome = "";
        public int HeightFromGround = 50;
        public float IslandTopScale = 30f;
        public float IslandDownScale = 5f;
        public float Frequency = 0.005f;
        public float MinNoise = 0;
        public float HeightRandomVariation = 10;
        public float HeightRandomFrequency = 0.01f;

        public bool GenerateWaterFall = true;
        public int WaterfallRandomHeight = 6;

        [XmlIgnore]
        public IModule IslandModule;

        [XmlIgnore]
        public Perlin RandomHeight;

        [XmlIgnore]
        private IModule RiversNoise;

        [XmlIgnore]
        private Perlin RiversPerlin;

        [XmlIgnore]
        private int RequiredBiomeHashCode = 0;

        public override IWorldGenerator.GeneratorTypes GetGeneratorType()
        {
            return GeneratorTypes.CUSTOM;
        }

        public override void Init()
        {
            RidgedMultifractal Fractal = new RidgedMultifractal();
            Fractal.Frequency = Frequency;
            Fractal.Seed = Information.IntSeed;
            Fractal.OctaveCount = 4;
            Fractal.Lacunarity = 1.7f;
            IslandModule = new Turbulence(Fractal);
            (IslandModule as Turbulence).SetPower(1.2f);
            if (RequiredBiome == null || RequiredBiome.Length == 0)
                RequiredBiomeHashCode = 0;
            else
                RequiredBiomeHashCode = RequiredBiome.GetHashCode();

            RandomHeight = new Perlin();
            RandomHeight.OctaveCount = 1;
            RandomHeight.Frequency = HeightRandomFrequency;

            if (GenerateWaterFall)
            {
                Billow River = new Billow();
                River.OctaveCount = 1;
                RiversNoise = River;

                RiversPerlin = new Perlin();
                RiversPerlin.OctaveCount = 1;
            }

            base.Init();
        }

        public float GetRiver(int x, int z)
        {
            if (RiversPerlin.GetValue(x * 0.01f, z * 0.01f) > 0.5f)
                return (float)RiversNoise.GetValue((float)x * 0.04f, (float)z * 0.04f);
            else
                return 1f;
        }

        public void GetRivers(ref Fast2DArray<float> TempRivers, int WorldX, int WorldZ, int SizeX, int SizeZ)
        {
            if (TempRivers == null)
                TempRivers = new Fast2DArray<float>(SizeX + 2, SizeZ + 2);

            int x, z;
            for (x = 0; x < SizeX + 2; ++x)
            {
                for (z = 0; z < SizeZ + 2; ++z)
                {
                    TempRivers[x, z] = GetRiver(WorldX + x - 1, WorldZ + z - 1);
                }
            }
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            if (!(Parent is HeightMapGenerator))
                return true;

            int WorldX = 0, WorldY = 0, WorldZ = 0;
            int StartX = ChunkX * Information.VoxelPerChunk, StartZ = ChunkZ * Information.VoxelPerChunk;
            int GroundLevel = Information.LocalGroundLevel + HeightFromGround;
            int Size = Information.VoxelPerChunk;
            float Noise = 0;
            float HeightVariation = 0;
            float MinHeight;
            float MaxHeight;
            Fast2DArray<float> TempRivers = GenerateWaterFall ? Data.GetCustomData("TempRivers") as Fast2DArray<float> : null;
            Fast2DArray<byte> TempTypes = Data.GetCustomData("TempTypes") as Fast2DArray<byte>;
            BiomeData.NoiseBuilderInfo[] Modules = Data.GetBiomes();

            if (ChunkId == 0 && GenerateWaterFall)
                GetRivers(ref TempRivers, Block.WorldX, Block.WorldZ, Block.Information.VoxelsPerAxis, Block.Information.VoxelsPerAxis);

            bool Water = false;
            float depth;
            byte GroundType = 0;

            int x, y, z, f = 0;
            int px, pz, py;

            for (x = 0; x < Size; ++x)
            {
                WorldX = Block.WorldX + StartX + x;
                px = StartX + x;

                for (z = 0; z < Size; ++z, ++f)
                {
                    if(Data.Resolution != 1)
                        f = Data.GetBiomeIndexReso(x, z);

                    if (RequiredBiomeHashCode != 0)
                    {
                        Data.Info = Modules[f];
                        if (!Data.Contains(RequiredBiomeHashCode))
                            continue;
                    }

                    WorldZ = Block.WorldZ + StartZ + z;

                    Noise = (float)IslandModule.GetValue((double)WorldX, (double)WorldZ);
                    if (Noise < MinNoise)
                        continue;

                    pz = StartZ + z;
                    GroundType = TempTypes != null ? TempTypes.array[f] : (byte)1;

                    HeightVariation = (float)RandomHeight.GetValue((double)WorldX, (double)WorldZ) * HeightRandomVariation;
                    Noise -= MinNoise;
                    MinHeight = (GroundLevel + HeightVariation) - Noise * IslandDownScale;
                    MaxHeight = (GroundLevel + HeightVariation) + Noise * IslandTopScale;

                    if (TempRivers != null)
                    {
                        if (TempRivers[px + 1 + 1, pz + 1 + 1] > 0
                        || TempRivers[px + 1 + 1, pz - 1 + 1] > 0
                        || TempRivers[px - 1 + 1, pz + 1 + 1] > 0
                        || TempRivers[px - 1 + 1, pz - 1 + 1] > 0)
                        {
                            MaxHeight += 1f;
                            Water = false;
                        }
                        else
                        {
                            depth = MaxHeight % (WaterfallRandomHeight + (int)((RiversPerlin.GetValue(WorldX * 0.002, Noise * 0.002, WorldZ * 0.002) + 1f) * WaterfallRandomHeight));
                            if (MaxHeight - depth > MinHeight)
                            {
                                MaxHeight -= depth;
                                Water = true;
                            }
                            else
                            {
                                MaxHeight = -1;
                                Water = true;
                            }
                        }
                    }

                    for (y = 0; y < Information.VoxelsPerAxis; ++y)
                    {
                        WorldY = Block.WorldY + y;
                        py = y;

                        if (WorldY > MinHeight - 1)
                        {
                            if (Water && WorldY > MinHeight + 1 && WorldY == (int)MaxHeight)
                            {
                                Block.SetTypeAndVolume(px, py, pz, BlockManager.Water, 2f);
                                break;
                            }

                            if (WorldY < MaxHeight + 1)
                            {
                                if (WorldY == (int)MaxHeight)
                                    Noise = Mathf.Clamp(MaxHeight - (float)WorldY, 0f, 2f);
                                else if (WorldY == (int)MinHeight - 1)
                                    Noise = Mathf.Clamp((float)WorldY - MinHeight, 0f, 2f);
                                else
                                    Noise = Mathf.Clamp(MaxHeight - (float)WorldY, 0f, 2f);

                                if (Noise < 2f)
                                    Noise = Mathf.Max(Block.GetVolume(px, py, pz), Noise);

                                Block.SetTypeAndVolume(px, py, pz, GroundType, Noise);
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}