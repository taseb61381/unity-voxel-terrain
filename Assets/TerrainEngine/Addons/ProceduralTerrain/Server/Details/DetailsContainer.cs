using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class provide a system for share only one gameobject between 2 differents projects that contains All Informations / objects for the Processor.
/// </summary>
public class DetailsContainer : MonoBehaviour 
{
    public List<DetailInfo> Details = new List<DetailInfo>();
    public BiomeObjectsContainer DetailsNoises;

    void Awake()
    {
        if(Details == null)
            Details = new List<DetailInfo>();
    }

    public DetailInfo GetInfo(string Name)
    {
        foreach (DetailInfo Info in Details)
        {
            if (Info.Name == Name)
                return Info;
        }

        return null;
    }
}
