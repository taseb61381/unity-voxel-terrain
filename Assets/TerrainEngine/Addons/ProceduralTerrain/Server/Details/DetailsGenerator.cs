﻿using System;
using System.Collections.Generic;



namespace TerrainEngine
{
    /// <summary>
    /// This class will Add details to the terrain randomly. Depending of Density.
    /// </summary>
    [Serializable]
    public class DetailsGenerator : IWorldGenerator
    {
        /// <summary>
        /// Density for details objects on height side = Bottom. Plants, rocks, etc
        /// </summary>
        public VectorI2 Density;
        public List<DetailInfo> DetailsObjects;

        public override IWorldGenerator.GeneratorTypes GetGeneratorType()
        {
            return GeneratorTypes.TERRAIN;
        }

        public override void Init()
        {
            for (int i = 0; i < DetailsObjects.Count; ++i)
            {
                DetailsObjects[i].ObjectId = i;
                DetailsObjects[i].RandomScale.Init();
            }

        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            int Size = Block.Information.VoxelPerChunk;
            int StartX = ChunkX * Size;
            int StartZ = ChunkZ * Size;

            SFastRandom FRandom = new SFastRandom((int)((Block.Position.x + Block.Position.y) - Block.Position.z * Block.Information.IntSeed));
            SimpleTypeDatas DData = Block.GetCustomData<SimpleTypeDatas>(DataName);
            int[] Grounds = Data.GetGrounds(Size, Size);

            int x, y = 0, z, f = 0;
            byte Type = 0;
            float Volume = 0;
            DetailInfo Info;
            int TotalChance = 0;
            int Result = 0;
            int CurrentChance = 0;
            int i = 0;
            int Count = DetailsObjects.Count;

            for (i = 0; i < Count; ++i)
                TotalChance += DetailsObjects[i].Chance;

            float Slope = 0;
            if (TotalChance != 0)
            {
                for (x = StartX; x < StartX + Size; ++x)
                {
                    for (z = StartZ; z < StartZ + Size; ++z, ++f)
                    {
                        if ((y = Grounds[f]) < 0)
                            continue;

                        if (FRandom.randomIntAbs((int)(Density.y / Information.Scale)) < (int)Density.x)
                        {
                            if (Block.GetTypeAndVolume(x, y, z, ref Type, ref Volume) != 0 && Volume >= 0.5f)
                            {
                                Result = FRandom.randomIntAbs(TotalChance);
                                CurrentChance = 0;

                                for (i = 0; i < Count; ++i)
                                {
                                    Info = DetailsObjects[i];
                                    if (Result >= CurrentChance && Result < (CurrentChance + Info.Chance))
                                    {
                                        if (Info.SlopeLimits.x != Info.SlopeLimits.y)
                                        {
                                            Slope = BlockServer.GetNormalY(x, y, z, false);
                                            if (Slope >= Info.SlopeLimits.x && Slope < Info.SlopeLimits.y)
                                                break;
                                        }

                                        DData.SetByteU(x, y, z, (byte)(i + 1));
                                        break;
                                    }

                                    CurrentChance += Info.Chance;
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        public override void OnTerrainDataCreate(TerrainDatas Data)
        {
            SimpleTypeDatas DData = Data.GetCustomData<SimpleTypeDatas>(DataName);
            if (DData == null)
            {
                DData = new SimpleTypeDatas();
                Data.RegisterCustomData(DataName, DData);
            }
            DData.UID = UID;
        }

        public override bool OnVoxelStartModification(ITerrainBlock Block, uint TObject,ref VoxelFlush Flush)
        {
            SimpleTypeDatas DData = Block.Voxels.GetCustomData<SimpleTypeDatas>(DataName);

            if (DData.GetByte(Flush.x, Flush.y, Flush.z) != 0)
            {
                DData.SetByte(Flush.x, Flush.y, Flush.z, 0);
            }

            return true;
        }


        public DetailInfo GetInfo(string Name)
        {
            foreach (DetailInfo Info in DetailsObjects)
            {
                if (Info.Name == Name)
                    return Info;
            }

            return null;
        }

    }
}
