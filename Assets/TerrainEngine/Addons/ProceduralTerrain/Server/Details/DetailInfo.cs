using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

[Serializable]
public class DetailInfo : IObjectBaseInformations
{
    [XmlIgnore]
    public MeshFilter Object;

    [HideInInspector, XmlIgnore]
    public CompleteMeshData Data;

    [HideInInspector, XmlIgnore]
    public int[] MaterialId;        // Each material have an unique id. Mesh are combined when using same material.

    [XmlIgnore, NonSerialized]
    public int RelationId;          // RelationId is another ObjectId who's using the same material. This GetObjectID() will return another RelationId if another object use the same material.

    [HideInInspector, XmlIgnore]
    public HashSet<int> Relations; // List of other objects that use the same material.

    public RandomInfo RandomScale = new RandomInfo();

    public bool GenerateWindPower = true;

    public Vector3 RotationOffset;
    public Vector3 PositionOffset;
    public bool SetVerticeZeroCenter = false; // This will move vertice to align them at Y 0

    [XmlIgnore]
    public bool GenerateColors = false;

    [XmlIgnore]
    public Color NoiseColorA = Color.white, NoiseColorB = Color.white, NoiseColorC = Color.white;

    [XmlIgnore]
    public float NoiseColorScale = 0.001f;

    [HideInInspector, XmlIgnore]
    public float TotalHeight;

    //[HideInInspector, XmlIgnore]
    public float HeightOffset;

    /// <summary>
    /// Return current object id or relation id if this object use another material
    /// </summary>
    /// <returns></returns>
    public int GetObjectId()
    {
        if (RelationId != 0)
            return RelationId;

        return ObjectId;
    }

    /// <summary>
    /// Extract Mesh data for be usable in multithreading
    /// </summary>
    public void ExtractMesh()
    {
        RandomScale.Init();
        Relations = new HashSet<int>() { ObjectId };

        if (Object == null)
        {
            return;
        }

        Name = GetName();
        Data = new CompleteMeshData();
        Mesh CMesh = Object.sharedMesh;

        if (CMesh == null)
        {
            Debug.LogError("Invalid Detail Mesh :" + Object);
            return;
        }
        int i;

        Data.CIndices = new int[CMesh.subMeshCount][];
        Data.CVertices = CMesh.vertices;
        Data.CTangents = CMesh.tangents;
        Data.CNormals = CMesh.normals;
        Data.CUVs = CMesh.uv;
        for (i = 0; i < CMesh.subMeshCount; ++i)
            Data.CIndices[i] = CMesh.GetTriangles(i);
        Data.CMaterials = Object.GetComponent<Renderer>().sharedMaterials;
        Data.VerticesCount = Data.CVertices.Length;
        Data.CColors = CMesh.colors;
        if (Data.CColors == null || Data.CColors.Length != Data.CVertices.Length)
        {
            Data.CColors = new Color[Data.CVertices.Length];
            for (i = 0; i < Data.CColors.Length; ++i)
                Data.CColors[i] = Color.white;
        }

        MaterialId = new int[Data.CMaterials.Length];
        RandomScale.UseXOnly = true;
        if (RandomScale.Min.x == 0)
            RandomScale.Min.x = 1;

        HeightOffset = 0;
        TotalHeight = 0;
        float y = 0;
        float MinY = float.MaxValue;
        float MaxY = float.MinValue;

        if (RotationOffset != Vector3.zero)
        {
            Quaternion Q = Quaternion.Euler(RotationOffset);
            for (i = 0; i < Data.VerticesCount; ++i)
            {
                Data.CVertices[i] = Q * Data.CVertices[i];
            }
        }

        for (i = 0; i < Data.VerticesCount; ++i)
        {
            Data.CVertices[i] += PositionOffset;

            y = Data.CVertices[i].y;
            if (y < HeightOffset)
                HeightOffset = y;
            if (y > TotalHeight)
                TotalHeight = y;
            if (y < MinY)
                MinY = y;
            if (y > MaxY)
                MaxY = y;
        }

        for (i = 0; i < Data.VerticesCount; ++i)
        {
            Data.CColors[i].a *= (Data.CVertices[i].y + MinY) / MaxY;

            if (SetVerticeZeroCenter)
                Data.CVertices[i].y += MaxY;
        }
    }

    public override GameObject GetObject(int x, int y, int z)
    {
        if (Object != null)
            return Object.gameObject;
        else
            return null;
    }

    public override Vector3 GetScale(ref SFastRandom FRandom, int x, int y, int z)
    {
        FRandom.InitSeed(x ^ y ^ z);
        return RandomScale.Random(ref FRandom);
    }

    public override float GetScaleF(ref SFastRandom FRandom, int x, int y, int z)
    {
        FRandom.InitSeed(x ^ y ^ z);
        return RandomScale.RandomF(ref FRandom);
    }

    public override float GetScaleF(float Random)
    {
        return RandomScale.RandomF(Random);
    }

    public override string GetName()
    {
        return Object != null ? Object.name : "";
    }
};
