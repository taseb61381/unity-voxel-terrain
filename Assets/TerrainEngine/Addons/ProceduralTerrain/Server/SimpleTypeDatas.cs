﻿using System.IO;
using System.Collections.Generic;

namespace TerrainEngine
{
    public class SimpleTypeDatas : ICustomTerrainData
    {
        static public int VoxelPerChunk = 256;
        static public int ChunkPerBlock;
        static public int Resolution;

        public struct LocalObjectPositionId
        {
            public byte Id, x, y, z;
        }

        public FList<LocalObjectPositionId>[] Chunks;

        public override void Init(TerrainDatas Terrain)
        {
            base.Init(Terrain);

            if (Chunks == null)
            {
                if (VoxelPerChunk < Information.VoxelPerChunk)
                    VoxelPerChunk = Information.VoxelPerChunk;

                if (Information.VoxelsPerAxis < VoxelPerChunk)
                {
                    ChunkPerBlock = Resolution = 1;
                }
                else
                {
                    ChunkPerBlock = Information.VoxelsPerAxis / VoxelPerChunk;
                    Resolution = VoxelPerChunk / Information.VoxelPerChunk;
                }

                Chunks = new FList<LocalObjectPositionId>[ChunkPerBlock * ChunkPerBlock * ChunkPerBlock];
            }
        }

        public override void Clear()
        {
            for (int i = Chunks.Length-1; i >= 0; --i)
            {
                if (Chunks[i] != null)
                    Chunks[i].ClearFast();
            }

            base.Clear();
        }

        public override byte GetByte(int x, int y, int z)
        {
            int cv = (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock + (y / VoxelPerChunk) * ChunkPerBlock + (z / VoxelPerChunk);

            if (Chunks[cv] == null)
                return 0;

            x -= (x / VoxelPerChunk) * VoxelPerChunk;
            y -= (y / VoxelPerChunk) * VoxelPerChunk;
            z -= (z / VoxelPerChunk) * VoxelPerChunk;

            LocalObjectPositionId[] array = Chunks[cv].array;
            for (int i = Chunks[cv].Count - 1; i >= 0; --i)
            {
                if (array[i].x == x && array[i].y == y && array[i].z == z)
                    return array[i].Id;
            }
            return 0;
        }

        public void GetObjects(int StartX,int StartY,int StartZ,int EndX,int EndY,int EndZ,FList<BiomeData.SimpleTempObject> Objects)
        {
            int ChunkX, ChunkY, ChunkZ;
            int ChunkStartX, ChunkStartY, ChunkStartZ;
            int i;

            FList<LocalObjectPositionId> Local;
            BiomeData.SimpleTempObject Obj = new BiomeData.SimpleTempObject();
            for(ChunkX = 0;ChunkX < ChunkPerBlock;++ChunkX)
            {
                ChunkStartX = ChunkX * VoxelPerChunk;
                if (ChunkStartX >= EndX || (ChunkStartX + VoxelPerChunk < StartX)) continue;

                for (ChunkY = 0; ChunkY < ChunkPerBlock; ++ChunkY)
                {
                    ChunkStartY = ChunkY * VoxelPerChunk;
                    if (ChunkStartY >= EndY || (ChunkStartY + VoxelPerChunk < StartY)) continue;

                    for (ChunkZ = 0; ChunkZ < ChunkPerBlock; ++ChunkZ)
                    {
                        ChunkStartZ = ChunkZ * VoxelPerChunk;
                        if (ChunkStartZ >= EndZ || (ChunkStartZ + VoxelPerChunk < StartZ)) continue;

                        Local = Chunks[ChunkX * ChunkPerBlock * ChunkPerBlock + ChunkY * ChunkPerBlock + ChunkZ];
                        if(Local == null || Local.Count == 0) continue;

                        for (i = Local.Count - 1; i >= 0; --i)
                        {
                            if (Local.array[i].Id == 0) continue;

                            Obj.x = (ushort)(Local.array[i].x + ChunkStartX);
                            if (Obj.x < StartX || Obj.x >= EndX) continue;

                            Obj.y = (ushort)(Local.array[i].y + ChunkStartY);
                            if (Obj.y < StartY || Obj.y >= EndY) continue;

                            Obj.z = (ushort)(Local.array[i].z + ChunkStartZ);
                            if (Obj.z < StartZ || Obj.z >= EndZ) continue;

                            Obj.Id = Local.array[i].Id;
                            Objects.Add(Obj);
                        }
                    }
                }
            }
        }

        public void GetObjects(int StartX, int StartY, int StartZ, int EndX, int EndY, int EndZ, FList<BiomeData.SimpleTempObject> Objects,HashSet<int> Ids)
        {
            int ChunkX, ChunkY, ChunkZ;
            int ChunkStartX, ChunkStartY, ChunkStartZ;
            int i;

            FList<LocalObjectPositionId> Local;
            BiomeData.SimpleTempObject Obj = new BiomeData.SimpleTempObject();
            for (ChunkX = 0; ChunkX < ChunkPerBlock; ++ChunkX)
            {
                ChunkStartX = ChunkX * VoxelPerChunk;
                if (ChunkStartX >= EndX || (ChunkStartX + VoxelPerChunk < StartX)) continue;

                for (ChunkY = 0; ChunkY < ChunkPerBlock; ++ChunkY)
                {
                    ChunkStartY = ChunkY * VoxelPerChunk;
                    if (ChunkStartY >= EndY || (ChunkStartY + VoxelPerChunk < StartY)) continue;

                    for (ChunkZ = 0; ChunkZ < ChunkPerBlock; ++ChunkZ)
                    {
                        ChunkStartZ = ChunkZ * VoxelPerChunk;
                        if (ChunkStartZ >= EndZ || (ChunkStartZ + VoxelPerChunk < StartZ)) continue;

                        Local = Chunks[ChunkX * ChunkPerBlock * ChunkPerBlock + ChunkY * ChunkPerBlock + ChunkZ];
                        if (Local == null || Local.Count == 0) continue;

                        for (i = Local.Count - 1; i >= 0; --i)
                        {
                            Obj.Id = Local.array[i].Id;
                            if (Obj.Id == 0) continue;

                            Obj.x = (ushort)(Local.array[i].x + ChunkStartX);
                            if (Obj.x < StartX || Obj.x >= EndX) continue;

                            Obj.y = (ushort)(Local.array[i].y + ChunkStartY);
                            if (Obj.y < StartY || Obj.y >= EndY) continue;

                            Obj.z = (ushort)(Local.array[i].z + ChunkStartZ);
                            if (Obj.z < StartZ || Obj.z >= EndZ) continue;

                            --Obj.Id;
                            if (!Ids.Contains(Obj.Id)) continue;

                            Objects.Add(Obj);
                        }
                    }
                }
            }
        }

        public byte SetByteIfExist(int x,int y,int z, byte Value)
        {
            int cv = (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock + (y / VoxelPerChunk) * ChunkPerBlock + (z / VoxelPerChunk);

            if (Chunks[cv] == null)
                return 0;

            x -= (x / VoxelPerChunk) * VoxelPerChunk;
            y -= (y / VoxelPerChunk) * VoxelPerChunk;
            z -= (z / VoxelPerChunk) * VoxelPerChunk;

            LocalObjectPositionId[] array = Chunks[cv].array;
            for (int i = Chunks[cv].Count - 1; i >= 0; --i)
            {
                if (array[i].x == x && array[i].y == y && array[i].z == z)
                {
                    x = array[i].Id;
                    array[i].Id = Value;
                    Dirty = true;
                    return (byte)x;
                }
            }

            return 0;
        }

        public override void SetByte(int x, int y, int z, byte Value)
        {
            int cv = (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock + (y / VoxelPerChunk) * ChunkPerBlock + (z / VoxelPerChunk);
            x -= (x / VoxelPerChunk) * VoxelPerChunk;
            y -= (y / VoxelPerChunk) * VoxelPerChunk;
            z -= (z / VoxelPerChunk) * VoxelPerChunk;

            if (Chunks[cv] == null)
            {
                if (Value == 0)
                    return;

                Chunks[cv] = new FList<LocalObjectPositionId>(VoxelPerChunk);
                LocalObjectPositionId Obj = new LocalObjectPositionId();
                Obj.x = (byte)x;
                Obj.y = (byte)y;
                Obj.z = (byte)z;
                Obj.Id = Value;
                Chunks[cv].AddSafe(Obj);
            }
            else
            {
                LocalObjectPositionId[] array = Chunks[cv].array;
                for (int i = Chunks[cv].Count - 1; i >= 0; --i)
                {
                    if (array[i].x == x && array[i].y == y && array[i].z == z)
                    {
                        array[i].Id = Value;
                        Dirty = true;
                        return;
                    }
                }

                LocalObjectPositionId Obj = new LocalObjectPositionId();
                Obj.x = (byte)x;
                Obj.y = (byte)y;
                Obj.z = (byte)z;
                Obj.Id = Value;
                Chunks[cv].Add(Obj);
            }

            Dirty = true;
        }

        public override void SetByteU(int x, int y, int z, byte Value)
        {
            int cv = (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock + (y / VoxelPerChunk) * ChunkPerBlock + (z / VoxelPerChunk);
            x -= (x / VoxelPerChunk) * VoxelPerChunk;
            y -= (y / VoxelPerChunk) * VoxelPerChunk;
            z -= (z / VoxelPerChunk) * VoxelPerChunk;

            if (Chunks[cv] == null)
            {
                if (Value == 0)
                    return;

                Chunks[cv] = new FList<LocalObjectPositionId>(VoxelPerChunk);
                LocalObjectPositionId Obj = new LocalObjectPositionId();
                Obj.x = (byte)x;
                Obj.y = (byte)y;
                Obj.z = (byte)z;
                Obj.Id = Value;
                Chunks[cv].AddSafe(Obj);
            }
            else
            {
                LocalObjectPositionId Obj = new LocalObjectPositionId();
                Obj.x = (byte)x;
                Obj.y = (byte)y;
                Obj.z = (byte)z;
                Obj.Id = Value;
                Chunks[cv].Add(Obj);
            }

            Dirty = true;
        }

        public int GetObjectCount()
        {
            int Count = 0;
            for (int i = 0; i < Chunks.Length; ++i)
                Count += Chunks[i].Count;
            return Count;
        }

        public void SetObjects(FList<BiomeData.SimpleTempObject> Objects)
        {
            if (Objects.Count == 0)
                return;

            lock (this)
            {
                LocalObjectPositionId Obj = new LocalObjectPositionId();

                int x = 0;
                int y = 0;
                int z = 0;
                int cv = 0;
                int Lastcv = -1;
                bool LastExist = false;

                for (int i = Objects.Count - 1; i >= 0; --i)
                {
                    Obj.Id = Objects.array[i].Id;
                    if (Obj.Id == 0)
                        continue;

                    x = Objects.array[i].x;
                    y = Objects.array[i].y;
                    z = Objects.array[i].z;

                    cv = (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock 
                        + (y / VoxelPerChunk) * ChunkPerBlock 
                        + (z / VoxelPerChunk);

                    x -= (x / VoxelPerChunk) * VoxelPerChunk;
                    y -= (y / VoxelPerChunk) * VoxelPerChunk;
                    z -= (z / VoxelPerChunk) * VoxelPerChunk;

                    if (Lastcv != cv)
                    {
                        LastExist = Chunks[cv] != null;
                        Lastcv = cv;
                    }

                    if (!LastExist)
                    {
                        Chunks[cv] = new FList<LocalObjectPositionId>(VoxelPerChunk);
                        Obj.x = (byte)x;
                        Obj.y = (byte)y;
                        Obj.z = (byte)z;
                        Chunks[cv].AddSafe(Obj);
                        LastExist = true;
                    }
                    else
                    {
                        Obj.x = (byte)x;
                        Obj.y = (byte)y;
                        Obj.z = (byte)z;
                        Chunks[cv].Add(Obj);
                    }
                }
            }

            Dirty = true;
        }

        #region Save

        public override void Save(BinaryWriter Stream)
        {
            LocalObjectPositionId[] array = null;
            int i, j;
            ushort count = 0;
            byte Id = 0;
            Stream.Write(VoxelPerChunk);
            Stream.Write(ChunkPerBlock);
            Stream.Write(Resolution);

            Stream.Write(Chunks.Length);
            for(i=0;i<Chunks.Length;++i)
            {
                if (Chunks[i] != null)
                {
                    array = Chunks[i].array;
                    count = (ushort)Chunks[i].Count;
                }
                else
                    count = 0;
                Stream.Write(count);

                for (j = 0; j < count; ++j)
                {
                    Id = array[j].Id;

                    if (Id != 0)
                    {
                        Stream.Write(array[j].Id);
                        Stream.Write(array[j].x);
                        Stream.Write(array[j].y);
                        Stream.Write(array[j].z);
                    }
                    else
                    {
                        Stream.Write((byte)0);
                    }
                }
            }
        }

        public override void Read(BinaryReader Stream)
        {
            LocalObjectPositionId Obj = new LocalObjectPositionId();
            int ObjectCount = 0, i, j;

            int OldVoxelPerChunk = Stream.ReadInt32();
            int OldChunkPerBlock = Stream.ReadInt32();
            int OldResolution = Stream.ReadInt32();
            int ChunksLenght = Stream.ReadInt32();

            // Use slow system if Memory management has changed
            if (OldVoxelPerChunk != VoxelPerChunk)
            {
                int x, y, z;
                byte Id = 0;
                i = 0;
                for (x = 0; x < OldChunkPerBlock; ++x)
                {
                    for (y = 0; y < OldChunkPerBlock; ++y)
                    {
                        for (z = 0; z < OldChunkPerBlock; ++z)
                        {
                            ObjectCount = Stream.ReadUInt16();

                            for (j = 0; j < ObjectCount; ++j)
                            {
                                Id = Stream.ReadByte();

                                if (Id != 0)
                                    SetByteU(Stream.ReadByte() + x * OldVoxelPerChunk, Stream.ReadByte() + y * OldVoxelPerChunk, Stream.ReadByte() + z * OldVoxelPerChunk, Id);
                            }

                            if (ObjectCount != 0)
                            {
                                Dirty = true;
                            }
                        }
                    }
                }
            }
            else
            {
                for (i = 0; i < ChunksLenght; ++i)
                {
                    ObjectCount = Stream.ReadUInt16();
                    if (ObjectCount != 0)
                    {
                        Chunks[i] = new FList<LocalObjectPositionId>(ObjectCount);
                        Dirty = true;
                    }

                    for (j = 0; j < ObjectCount; ++j)
                    {
                        Obj.Id = Stream.ReadByte();
                        if (Obj.Id == 0)
                        {
                            continue;
                        }

                        Obj.x = Stream.ReadByte();
                        Obj.y = Stream.ReadByte();
                        Obj.z = Stream.ReadByte();
                        Chunks[i].AddSafe(Obj);
                    }
                }
            }
        }

        #endregion
    }
}
