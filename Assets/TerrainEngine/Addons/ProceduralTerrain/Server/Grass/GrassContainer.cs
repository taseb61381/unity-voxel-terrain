using System.Collections.Generic;
using UnityEngine;

public class GrassContainer : MonoBehaviour 
{
    public List<GrassInfo> Grass;
    public BiomeObjectsContainer GrassNoises;

    void Awake()
    {
        if (Grass == null)
            Grass = new List<GrassInfo>();
    }

    public GrassInfo GetInfo(string Name)
    {
        foreach (GrassInfo Info in Grass)
            if (Info.Name == Name)
                return Info;

        return null;
    }
}
