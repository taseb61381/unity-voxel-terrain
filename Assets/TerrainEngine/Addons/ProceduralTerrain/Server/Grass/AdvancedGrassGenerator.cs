﻿using System;

namespace TerrainEngine
{
    [Serializable]
    public class AdvancedGrassGenerator : GrassGenerator
    {
        public NoiseObjectsContainer ObjectsRequirements;
        public NoisesContainer RequirementNoises;

        public override IWorldGenerator.GeneratorTypes GetGeneratorType()
        {
            return GeneratorTypes.GROUND;
        }

        public override bool IsUsingGroundPoints()
        {
            return true;
        }

        public override void Init()
        {
            ObjectsRequirements.Container = RequirementNoises;

            base.Init();

            foreach (NoiseObjects Noise in ObjectsRequirements.NoisesObjects)
            {
                foreach (NoiseObject Info in Noise.Objects)
                {
                    Info.Object = GetInfo(Info.Name);
                    if (Info.GroundType == -1)
                    {
                        Info.GroundType = 0;
                        Info.MaxHeight = 999;
                        Info.MinHeight = 0;
                        Info.VoxelType = VoxelTypes.OPAQUE;
                    }
                }
            }

            ObjectsRequirements.Init(this,null, Information.IntSeed);
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Biome, int ChunkId, int ChunkX, int ChunkZ)
        {
            int StartX = ChunkX * Information.VoxelPerChunk;
            int StartZ = ChunkZ * Information.VoxelPerChunk;

            SimpleTypeDatas GData = Block.GetCustomData<SimpleTypeDatas>(DataName);

            int VoxelCountYZ = Information.VoxelsPerAxis * Information.VoxelsPerAxis;

            SelecterContainer SC = new SelecterContainer();
            SC.Biome = Biome;
            SC.Data = GData;
            SC.BlockServer = BlockServer;
            SC.Block = Block;
            SC.Container = ObjectsRequirements;

            GenerateObjects(Thread, ref SC, ChunkId, StartX, StartZ, Density, Density, OnAdd);

            GData.SetObjects(Biome.SimpleObjects);
            Biome.SimpleObjects.ClearFast();

            return true;
        }
    }
}
