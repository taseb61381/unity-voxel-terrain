﻿using System;
using System.Collections.Generic;
using UnityEngine;



namespace TerrainEngine
{
    /// <summary>
    /// This class will Add grass to the terrain randomly. Depending of Density.
    /// </summary>
    [Serializable]
    public class GrassGenerator : IWorldGenerator
    {
        public int GroundType = 1;
        public VectorI2 Density;
        public List<GrassInfo> GrassObjects;

        public override IWorldGenerator.GeneratorTypes GetGeneratorType()
        {
            return GeneratorTypes.TERRAIN;
        }

        public override void Init()
        {
            for (int i = 0; i < GrassObjects.Count; ++i)
            {
                GrassObjects[i].RandomScale.Init();
                GrassObjects[i].ObjectId = i;
            }

            base.Init();
        }

        public override void OnTerrainDataCreate(TerrainDatas Data)
        {
            SimpleTypeDatas CData = Data.GetCustomData<SimpleTypeDatas>(DataName);
            if (CData == null)
            {
                CData = new SimpleTypeDatas();
                Data.RegisterCustomData(DataName, CData);
            }
            CData.UID = UID;
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            int Size = Block.Information.VoxelPerChunk;
            int StartX = ChunkX * Size;
            int StartZ = ChunkZ * Size;

            FastRandom FRandom = new FastRandom(Block.Position.GetHashCode() + Block.Information.IntSeed + ChunkId);
            SimpleTypeDatas BData = Block.GetCustomData<SimpleTypeDatas>(DataName);
            int[] Grounds = Data.GetGrounds(Size, Size);

            int x, z, i, y, Chance, CurrentChance, TotalChance, f = 0;
            GrassInfo Info;
            float Slope = 0;
            for (x = StartX; x < StartX + Size; ++x)
            {
                for (z = StartZ; z < StartZ + Size; ++z, ++f)
                {
                    y = Grounds[f] - Block.WorldY;
                    if (y <= 0 || y >= Block.Information.VoxelsPerAxis)
                        continue;

                    if (FRandom.randomIntAbs((int)Density.y) < (int)Density.x)
                    {
                        if (GroundType == 0 || Block.GetType(x, y, z) == GroundType)
                        {
                            TotalChance = 0;

                            for (i = 0; i < GrassObjects.Count; ++i)
                                TotalChance += GrassObjects[i].Chance;

                            if (TotalChance == 0)
                                continue;

                            Chance = FRandom.randomIntAbs(TotalChance);
                            CurrentChance = 0;

                            for (i = 0; i < GrassObjects.Count; ++i)
                            {
                                Info = GrassObjects[i];
                                if (Chance >= CurrentChance && Chance < (CurrentChance + Info.Chance))
                                {
                                    if (Info.SlopeLimits.x != Info.SlopeLimits.y)
                                    {
                                        Slope = BlockServer.GetNormalY(x, y, z, false);
                                        if (Slope >= Info.SlopeLimits.x && Slope <Info.SlopeLimits.y)
                                            break;
                                    }

                                    BData.SetByte(x, y, z, (byte)(i + 1));
                                    break;
                                }

                                CurrentChance += Info.Chance;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public GrassInfo GetInfo(string Name)
        {
            foreach (GrassInfo Info in GrassObjects)
            {
                if (Info.Name == Name)
                    return Info;
            }

            return null;
        }

        public override bool OnVoxelStartModification(ITerrainBlock Block, uint TObject, ref VoxelFlush Flush)
        {
            SimpleTypeDatas BData = Block.Voxels.GetCustomData<SimpleTypeDatas>(DataName);
            BData.SetByteIntFloat(Flush.x, Flush.y, Flush.z, 0, 0, 0);
            return true;
        }
    }
}
