using System;
using System.Xml.Serialization;
using UnityEngine;

public enum GrassTypes
{
    PLANES = 0,
    CROSS_PLANES = 1,
    HORIZONTAL_PLANE = 2,
    CUSTOM_MESH = 3,
};

[Serializable]
public class GrassInfo : IObjectBaseInformations
{
    public delegate Color GenerateColorDelegate(float vx, float vy, float vz, Vector3 Normal);

    [XmlIgnore]
    public Texture2D Texture = null;

    [XmlIgnore]
    public int PlaneMin = 1;

    [XmlIgnore]
    public int PlaneMax = 4;

    [XmlIgnore]
    public float Scale = 1;

    [XmlIgnore]
    public GrassTypes GrassType = GrassTypes.PLANES;

    [XmlIgnore]
    public RandomInfo RandomScale = new RandomInfo();

    [XmlIgnore]
    public bool AlignToGround = false;

    [NonSerialized]
    public Rect Uv;

    [XmlIgnore]
    public MeshFilter CustomMesh;

    [XmlIgnore]
    public Bounds Bounds;

    [XmlIgnore]
    public CompleteMeshData Data;

    [XmlIgnore]
    public int[] Indices;

    [XmlIgnore]
    public bool GenerateColors = false;

    [XmlIgnore]
    public Color NoiseColorA = Color.white, NoiseColorB = Color.white, NoiseColorC = Color.white;

    [XmlIgnore]
    public float NoiseColorScale = 0.001f;

    public void ExtractMesh()
    {
        if (CustomMesh == null || GrassType != GrassTypes.CUSTOM_MESH)
            return;

        Mesh CMesh = CustomMesh.sharedMesh;
        if (CMesh == null)
        {
            Debug.LogError("Invalid Grass Mesh (null) : " + GetName());
            return;
        }

        if (CMesh.vertices == null || CMesh.vertices.Length == 0)
        {
            Debug.LogError("Invalid Grass Mesh (Vertices == null or len == 0) : " + GetName());
            return;
        }

        if (CMesh.subMeshCount != 1)
        {
            Debug.LogError("Invalid Grass Mesh (subMeshCount != 1) : " + GetName());
            return;
        }

        if (CMesh.normals.Length == 0)
        {
            Debug.LogError("Invalid Grass Mesh (normals.Length == 0) : " + GetName());
            return;
        }

        if (CMesh.tangents.Length == 0)
        {
            Debug.LogError("Invalid Grass Mesh (tangents.Length == 0) : " + GetName());
            return;
        }

        Data = new CompleteMeshData();
        CMesh.RecalculateBounds();
        Bounds = CMesh.bounds;

        Data.CVertices = CMesh.vertices;
        Data.CUVs = CMesh.uv;
        Data.CTangents = CMesh.tangents;
        Data.CNormals = CMesh.normals;
        Data.CColors = CMesh.colors;
        Data.CIndices = new int[1][];
        Indices = Data.CIndices[0] = CMesh.GetIndices(0);

        Vector3 Tmp = new Vector3(Bounds.center.x, Bounds.max.y/5f, Bounds.center.z);
        for (int i = 0; i < Data.CVertices.Length; ++i)
        {
            Data.CVertices[i] -= Tmp;
        }

        if (!Application.isEditor)
            GameObject.Destroy(CustomMesh.gameObject);
    }

    public override Texture2D GetTexture(int x, int y, int z)
    {
        return Texture;
    }

    public override string GetName()
    {
        return Texture != null ? Texture.name : "";
    }

    public override void CopyFrom(IObjectBaseInformations Other)
    {
        base.CopyFrom(Other);
        GrassInfo Info = Other as GrassInfo;
        Chance = Info.Chance;
        RandomScale.Min = Info.RandomScale.Min;
        RandomScale.Max = Info.RandomScale.Max;
        RandomScale.Enabled = Info.RandomScale.Enabled;
        RandomScale.UseXOnly = Info.RandomScale.UseXOnly;
        GrassType = Info.GrassType;
        HeightSide = Info.HeightSide;
        PlaneMin = Info.PlaneMin;
        PlaneMax = Info.PlaneMax;
        CustomMesh = Info.CustomMesh;
    }
};