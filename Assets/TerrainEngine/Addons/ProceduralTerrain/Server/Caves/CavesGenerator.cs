﻿using LibNoise;
using System;
using System.Xml.Serialization;

namespace TerrainEngine
{
    [Serializable]
    public class CavesGenerator : IWorldGenerator
    {
        [XmlIgnore]
        public FastRidgedMultifractal Perlin;

        public float MinRidgetNoise = 0.5f;
        public float RidgedFrequency = 0.01f;
        public float GroundVariationPower = 3f;
        public float GroundOffset = -1;
        public float HeightMinSize = 5;
        public float HeightVariationPower = 6f;
        public float CaveEntryMinSize = 0.4f;
        public float CaveEntryFrequency = 0.05f;

        public float CaveNoiseFrequency = 0.01f;
        public float CaveNoiseMinValue = 0f;

        public int CaveYCount = 2;
        public float CaveHeightOffset = 4;
        public int CaveEntry = 0;
        public int Cave = 0;

        public override IWorldGenerator.GeneratorTypes GetGeneratorType()
        {
            return GeneratorTypes.CUSTOM;
        }

        public override void Init()
        {
            Perlin = new FastRidgedMultifractal();
            Perlin.OctaveCount = 3;
            SimplexNoise.Initialize();
            CaveEntry = "CaveEntry".GetHashCode();
            Cave = "Cave".GetHashCode();
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            int VPC = Information.VoxelPerChunk;
            int WorldX, WorldY, WorldZ;
            int StartX = ChunkX * VPC, StartZ = ChunkZ * VPC;
            int GroundY = 0, Depth, GroundDepth;
            float MinNoise = 0, MaxNoise = 0, EntryNoise = 0;
            int GroundLevel = Block.Information.LocalGroundLevel;
            int[] Grounds = Data.GetGrounds(VPC, VPC);
            BiomeData.NoiseBuilderInfo[] Modules = Data.GetBiomes();

            bool IsSafe = false, HasCave = false, HasCaveEntry=false,HasEntryNoise=false;
            int x, y, z, i = 0, f = 0, GroundIndex = 0, LastChunkId = -1;
            SimplexNoise.Initialize();
            int px,pz;

            for (x = VPC-1; x >= 0; --x)
            {
                WorldX = Block.WorldX + StartX + x;
                px = StartX + x;

                for (z = VPC - 1; z >= 0; --z, ++f, ++GroundIndex)
                {
                    WorldZ = Block.WorldZ + StartZ + z;
                    pz = StartZ + z;

                    if (SimplexNoise.Noise2(WorldX * CaveNoiseFrequency, WorldZ * CaveNoiseFrequency) < CaveNoiseMinValue)
                        continue;

                    HasCave = HasCaveEntry = HasEntryNoise = false;
                    GroundY = Grounds[GroundIndex];

                    for (i = 0; i < CaveYCount; ++i)
                    {
                        if (Perlin.GetValue(WorldX * RidgedFrequency + i * 180, WorldZ * RidgedFrequency + i * 180) < MinRidgetNoise)
                            continue;

                        MinNoise = 0;
                        MaxNoise = 0;

                        for (y = Block.Information.VoxelsPerAxis-1; y >= 0; --y)
                        {
                            if (Block.WorldY + y - GroundLevel <= 0)
                                continue;

                            WorldY = Block.WorldY + y;
                            GroundDepth = WorldY - GroundY;

                            if (!HasEntryNoise)
                            {
                                EntryNoise = (float)SimplexNoise.Noise2(WorldZ * CaveEntryFrequency + 480, WorldX * CaveEntryFrequency + 480);
                                HasEntryNoise = true;
                            }

                            Depth = WorldY - GroundY;
                            if (Depth < -2 || EntryNoise > CaveEntryMinSize)
                            {
                                if (MaxNoise == 0)
                                    MaxNoise = ((float)(SimplexNoise.Noise2(WorldZ * 0.05f + i * 120, WorldX * 0.05f + i * 150) + 1f) * HeightVariationPower + HeightMinSize) - i * CaveHeightOffset;

                                if (GroundDepth < MaxNoise)
                                {
                                    if (MinNoise == 0)
                                        MinNoise = ((float)(SimplexNoise.Noise2(WorldX * 0.05f + i * 150, WorldZ * 0.03f + i * 120) + 1f) * GroundVariationPower - GroundOffset) - i * CaveHeightOffset;

                                    if (GroundDepth >= (int)MinNoise)
                                    {
                                        ChunkId = ChunkX * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + ChunkZ;
                                        if (LastChunkId != ChunkId)
                                        {
                                            LastChunkId = ChunkId;
                                            IsSafe = Block.Voxels[ChunkId] != null;
                                        }

                                        if (Block.GetVolume(px, y, pz, ChunkId, ref IsSafe) > 0)
                                        {
                                            HasCave = true;

                                            if (Depth > -3)
                                                HasCaveEntry = true;

                                            if (GroundDepth == (int)MinNoise)
                                            {
                                                Block.SetVolume(px, y, pz, (MinNoise - GroundDepth), ChunkId, ref IsSafe);
                                            }
                                            else
                                                Block.SetTypeAndVolume(px, y, pz, ChunkId, 0, 0, ref IsSafe);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (Modules != null)
                    {
                        if (HasCaveEntry)
                        {
                            f = Data.GetBiomeIndexReso(x, z);
                            Data.LoadInfo(f);
                            Data.AddCustom(CaveEntry);
                        }
                        else if (HasCave)
                        {
                            f = Data.GetBiomeIndexReso(x, z);
                            Data.LoadInfo(f);
                            Data.AddCustom(Cave);
                        }

                    }

                }
            }

            return true;
        }
    }
}
