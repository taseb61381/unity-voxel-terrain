using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class provide a system for share only one gameobject between 2 differents projects that contains All Informations / objects for the Processor.
/// </summary>
public class GameObjectsContainer : MonoBehaviour 
{
    public List<GameObjectInfo> Objects =new List<GameObjectInfo>();
    public BiomeObjectsContainer ObjectsNoises = null;

    void Awake()
    {
        if (Objects == null)
            Objects = new List<GameObjectInfo>();
    }

    public GameObjectInfo GetInfo(string Name)
    {
        if (Name == "")
            return null;

        foreach (GameObjectInfo Info in Objects)
        {
            if (Info.Object != null && Info.Object.name == Name)
                return Info;
        }

        return null;
    }
}
