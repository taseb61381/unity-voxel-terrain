using System;
using UnityEngine;

public enum LodTypes
{
    GAMEOBJECT = 0,
    PLANE = 1,
    CROSS_PLANE = 2,
    BATCHING = 3,
};

[Serializable]
public class LodInfo
{
    public GameObject GeneratedObject = null;

    public GameObject LodObject = null;
    public LodTypes Type = LodTypes.GAMEOBJECT;
    public float ActiveRange = 100;
    public bool Billboard = false;
    public bool ProceduralTexture = false;
    public int Resolution = 512;
    public Texture2D PlaneTexture;
    public Material PlaneMaterial = null;
    public Material ProceduralMaterial = null;
    public float LodLightPower = 0.5f; // When generating an LOD, a directional light is added to the camera, this intensity is used
    public Color ProceduralCameraBackground = Color.white;  // Camera background color, this color is used as alpha.

    public void CopyFrom(LodInfo Other)
    {
        this.GeneratedObject = Other.GeneratedObject;
        this.LodObject = Other.LodObject;
        this.Type = Other.Type;
        this.ActiveRange = Other.ActiveRange;
        this.Billboard = Other.Billboard;
        this.ProceduralTexture = Other.ProceduralTexture;
        this.Resolution = Other.Resolution;
        this.PlaneTexture = Other.PlaneTexture;
        this.PlaneMaterial = Other.PlaneMaterial;
        this.LodLightPower = Other.LodLightPower;
        this.ProceduralCameraBackground = Other.ProceduralCameraBackground;
    }
};