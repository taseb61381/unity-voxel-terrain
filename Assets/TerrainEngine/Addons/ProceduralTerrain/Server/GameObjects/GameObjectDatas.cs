﻿using System;
using System.IO;

using UnityEngine;

namespace TerrainEngine
{
    /// <summary>
    /// Voxel Data Storage , this container only handle voxel type. Client GameObjects are handled on ClientGameObjectDatas
    /// </summary>
    public class GameObjectDatas : ICustomTerrainData
    {
        static public int VoxelPerChunk = 256;
        static public int ChunkPerBlock;
        static public int Resolution;

        public struct LocalObjectPositionId
        {
            public byte Id, x, y, z, AngleY, Scale;
        }

        public FList<LocalObjectPositionId>[] Chunks;

        static public float MaxOrientation = 359f;
        static public float MaxScale = 10f;
        
        public override void Init(TerrainDatas Terrain)
        {
            base.Init(Terrain);

            if (Chunks == null)
            {
                if (VoxelPerChunk < Information.VoxelPerChunk)
                    VoxelPerChunk = Information.VoxelPerChunk;

                if (Information.VoxelsPerAxis < VoxelPerChunk)
                {
                    ChunkPerBlock = Resolution = 1;
                }
                else
                {
                    ChunkPerBlock = Information.VoxelsPerAxis / VoxelPerChunk;
                    Resolution = VoxelPerChunk / Information.VoxelPerChunk;
                }
                Chunks = new FList<LocalObjectPositionId>[ChunkPerBlock * ChunkPerBlock * ChunkPerBlock];
            }
        }

        public void GetOffsets(int ChunkX, int ChunkY, int ChunkZ, ref int Index, ref int OffsetX, ref int OffsetY, ref int OffsetZ)
        {
            ChunkX = ChunkX / Resolution;
            ChunkY = ChunkY / Resolution;
            ChunkZ = ChunkZ / Resolution;

            Index = ChunkX * ChunkPerBlock * ChunkPerBlock + ChunkY * ChunkPerBlock + ChunkZ;

            OffsetX = ChunkX * VoxelPerChunk;
            OffsetY = ChunkY * VoxelPerChunk;
            OffsetZ = ChunkZ * VoxelPerChunk;
        }

        public override void Clear()
        {
            for (int i = Chunks.Length - 1; i >= 0; --i)
            {
                if (Chunks[i] != null)
                    Chunks[i].ClearFast();
            }

            base.Clear();
        }

        #region Voxels

        public override byte GetByte(int x, int y, int z)
        {
            int cv = (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock + (y / VoxelPerChunk) * ChunkPerBlock + (z / VoxelPerChunk);

            if (Chunks[cv] == null)
                return 0;

            x -= (x / VoxelPerChunk) * VoxelPerChunk;
            y -= (y / VoxelPerChunk) * VoxelPerChunk;
            z -= (z / VoxelPerChunk) * VoxelPerChunk;

            LocalObjectPositionId[] array = Chunks[cv].array;
            for (int i = Chunks[cv].Count - 1; i >= 0; --i)
            {
                if (array[i].x == x && array[i].y == y && array[i].z == z)
                    return array[i].Id;
            }
            return 0;
        }

        public int ConvertOrientation(byte Orientation)
        {
            return (int)((float)Orientation / (float)byte.MaxValue * (float)MaxOrientation);
        }

        public float ConvertScale(byte Scale)
        {
            return (float)Math.Round((double)(((float)Scale * MaxScale) / (float)byte.MaxValue),3);
        }

        public override void SetByte(int x, int y, int z, byte Value)
        {
            SetByteIntFloat(x, y, z, Value, 0, 0);
        }

        public override void SetByteIntFloat(int x, int y, int z, byte Id, int AngleY, float Scale)
        {
            int cv = (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock + (y / VoxelPerChunk) * ChunkPerBlock + (z / VoxelPerChunk);
            x -= (x / VoxelPerChunk) * VoxelPerChunk;
            y -= (y / VoxelPerChunk) * VoxelPerChunk;
            z -= (z / VoxelPerChunk) * VoxelPerChunk;

            if (Chunks[cv] == null)
            {
                if (Id == 0)
                    return;

                Chunks[cv] = new FList<LocalObjectPositionId>(VoxelPerChunk/2,30);
                LocalObjectPositionId Obj = new LocalObjectPositionId();
                Obj.Id = Id;
                Obj.x = (byte)x;
                Obj.y = (byte)y;
                Obj.z = (byte)z;
                Obj.Scale = (byte)(Mathf.Clamp((float)Scale, 0, MaxScale) / MaxScale * (float)byte.MaxValue);
                Obj.AngleY = (byte)(Mathf.Clamp((float)AngleY, 0, MaxOrientation) / MaxOrientation * (float)byte.MaxValue);
                Chunks[cv].AddSafe(Obj);
            }
            else
            {
                LocalObjectPositionId[] array = Chunks[cv].array;
                for (int i = Chunks[cv].Count - 1; i >= 0; --i)
                {
                    if (array[i].x == x && array[i].y == y && array[i].z == z)
                    {
                        array[i].Id = Id;
                        Dirty = true;
                        return;
                    }
                }

                LocalObjectPositionId Obj = new LocalObjectPositionId();
                Obj.Id = Id;
                Obj.x = (byte)x;
                Obj.y = (byte)y;
                Obj.z = (byte)z;
                Obj.Scale = (byte)(Mathf.Clamp((float)Scale, 0, MaxScale) / MaxScale * (float)byte.MaxValue);
                Obj.AngleY = (byte)(Mathf.Clamp((float)AngleY, 0, MaxOrientation) / MaxOrientation * (float)byte.MaxValue);
                Chunks[cv].Add(Obj);
            }

            Dirty = true;
        }

        public void SetObjects(FList<BiomeData.TempObject> Objects)
        {
            LocalObjectPositionId Obj;
            int cv, x, y, z;
            int Lastcv = -1;
            bool HasChunk = false;


            lock (this)
            {
                for (int i = Objects.Count - 1; i >= 0; --i)
                {
                    Obj.Id = Objects.array[i].Id;
                    if (Obj.Id == 0)
                        continue;

                    x = Objects.array[i].x;
                    y = Objects.array[i].y;
                    z = Objects.array[i].z;

                    cv = (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock + (y / VoxelPerChunk) * ChunkPerBlock + (z / VoxelPerChunk);

                    x -= (x / VoxelPerChunk) * VoxelPerChunk;
                    y -= (y / VoxelPerChunk) * VoxelPerChunk;
                    z -= (z / VoxelPerChunk) * VoxelPerChunk;
                    
                    if(Lastcv != cv)
                    {
                        Lastcv = cv;
                        HasChunk = Chunks[cv] != null;
                    }

                    Obj.x = (byte)x;
                    Obj.y = (byte)y;
                    Obj.z = (byte)z;
                    Obj.Scale = (byte)(Mathf.Clamp((float)Objects.array[i].Scale, 0, MaxScale) / MaxScale * (float)byte.MaxValue);
                    Obj.AngleY = (byte)(Mathf.Clamp((float)Objects.array[i].Orientation, 0, MaxOrientation) / MaxOrientation * (float)byte.MaxValue);

                    if (!HasChunk)
                    {
                        Chunks[cv] = new FList<LocalObjectPositionId>(VoxelPerChunk / 2, 30);
                        Chunks[cv].AddSafe(Obj);
                        HasChunk = true;
                    }
                    else
                    {
                        Chunks[cv].Add(Obj);
                    }
                }
            }

            Dirty = true;
        }

        public override void SetByteIntFloatU(int x, int y, int z, byte Id, int AngleY, float Scale)
        {
            int cv = (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock + (y / VoxelPerChunk) * ChunkPerBlock + (z / VoxelPerChunk);
            x -= (x / VoxelPerChunk) * VoxelPerChunk;
            y -= (y / VoxelPerChunk) * VoxelPerChunk;
            z -= (z / VoxelPerChunk) * VoxelPerChunk;

            if (Chunks[cv] == null)
            {
                if (Id == 0)
                    return;

                Chunks[cv] = new FList<LocalObjectPositionId>(VoxelPerChunk / 2, 30);
                LocalObjectPositionId Obj = new LocalObjectPositionId();
                Obj.Id = Id;
                Obj.x = (byte)x;
                Obj.y = (byte)y;
                Obj.z = (byte)z;
                Obj.Scale = (byte)(Mathf.Clamp((float)Scale, 0, MaxScale) / MaxScale * (float)byte.MaxValue);
                Obj.AngleY = (byte)(Mathf.Clamp((float)AngleY, 0, MaxOrientation) / MaxOrientation * (float)byte.MaxValue);
                Chunks[cv].AddSafe(Obj);
            }
            else
            {
                LocalObjectPositionId Obj = new LocalObjectPositionId();
                Obj.Id = Id;
                Obj.x = (byte)x;
                Obj.y = (byte)y;
                Obj.z = (byte)z;
                Obj.Scale = (byte)(Mathf.Clamp((float)Scale, 0, MaxScale) / MaxScale * (float)byte.MaxValue);
                Obj.AngleY = (byte)(Mathf.Clamp((float)AngleY, 0, MaxOrientation) / MaxOrientation * (float)byte.MaxValue);
                Chunks[cv].Add(Obj);
            }

            Dirty = true;
        }

        public int GetObjectsCount()
        {
            int Count = 0;

            for(int i=0;i<Chunks.Length;++i)
            {
                Count += Chunks[i].Count;
            }
            return Count;
        }

        public int GetObjectsCount(int Type)
        {
            int Count = 0;

            for (int i = 0; i < Chunks.Length; ++i)
            {
                for (int j = 0; j < Chunks[i].Count; ++j)
                {
                    if (Chunks[i].array[j].Id == Type)
                    {
                        ++Count;
                    }
                }
            }
            return Count;
        }

        #endregion

        #region Save

        public override void Save(BinaryWriter Stream)
        {
            LocalObjectPositionId[] array = null;
            int i, j, count;
            byte Id = 0;
            Stream.Write(VoxelPerChunk);
            Stream.Write(ChunkPerBlock);
            Stream.Write(Resolution);
            Stream.Write(Chunks.Length);

            int TotalTrees = 0;
            for (i = 0; i < Chunks.Length; ++i)
            {
                if (Chunks[i] != null)
                {
                    array = Chunks[i].array;
                    count = Chunks[i].Count;
                }
                else
                    count = 0;

                Stream.Write(count);

                for (j = 0; j < count; ++j)
                {
                    Id = array[j].Id;

                    if (Id != 0)
                    {
                        ++TotalTrees;
                        Stream.Write(array[j].Id);
                        Stream.Write(array[j].x);
                        Stream.Write(array[j].y);
                        Stream.Write(array[j].z);
                        Stream.Write(array[j].AngleY);
                        Stream.Write(array[j].Scale);
                    }
                    else
                    {
                        Stream.Write((byte)0);
                    }
                }
            }
        }

        public override void Read(BinaryReader Stream)
        {
            LocalObjectPositionId Obj = new LocalObjectPositionId();
            int ObjectCount = 0, i, j;

            int OldVoxelPerChunk = Stream.ReadInt32();
            int OldChunkPerBlock = Stream.ReadInt32();
            int OldResolution = Stream.ReadInt32();
            int ChunksLenght = Stream.ReadInt32();
            int TotalTrees = 0;
            // Use slow system if Memory management has changed
            if (OldVoxelPerChunk != VoxelPerChunk)
            {
                int x, y, z;
                byte Id = 0;
                i = 0;
                for (x = 0; x < OldChunkPerBlock; ++x)
                {
                    for (y = 0; y < OldChunkPerBlock; ++y)
                    {
                        for (z = 0; z < OldChunkPerBlock; ++z)
                        {
                            ObjectCount = Stream.ReadInt32();

                            for (j = 0; j < ObjectCount; ++j)
                            {
                                Id = Stream.ReadByte();

                                if (Id != 0)
                                {
                                    SetByteIntFloatU(Stream.ReadByte() + x * OldVoxelPerChunk, Stream.ReadByte() + y * OldVoxelPerChunk, Stream.ReadByte() + z * OldVoxelPerChunk, Id, Stream.ReadByte(), Stream.ReadByte());
                                    ++TotalTrees;
                                }
                            }

                            if (ObjectCount != 0)
                            {
                                Dirty = true;
                            }
                        }
                    }
                }
            }
            else
            {
                for (i = 0; i < ChunksLenght; ++i)
                {
                    ObjectCount = Stream.ReadInt32();
                    if (ObjectCount != 0)
                    {
                        Chunks[i] = new FList<LocalObjectPositionId>(ObjectCount);
                        Dirty = true;
                    }

                    for (j = 0; j < ObjectCount; ++j)
                    {
                        Obj.Id = Stream.ReadByte();
                        if (Obj.Id == 0)
                        {
                            continue;
                        }

                        ++TotalTrees;
                        Obj.x = Stream.ReadByte();
                        Obj.y = Stream.ReadByte();
                        Obj.z = Stream.ReadByte();
                        Obj.AngleY = Stream.ReadByte();
                        Obj.Scale = Stream.ReadByte();
                        Chunks[i].Add(Obj);
                    }
                }
            }
        }

        #endregion
    }
}
