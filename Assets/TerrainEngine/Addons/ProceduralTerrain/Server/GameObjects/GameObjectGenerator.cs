﻿using System;
using System.Collections.Generic;

using UnityEngine;


namespace TerrainEngine
{
    /// <summary>
    /// This class will Add GameObjects to the terrain randomly. Depending of Density.
    /// </summary>
    [Serializable]
    public class GameObjectGenerator : IWorldGenerator
    {
        public VectorI2 Density;
        public List<GameObjectInfo> GameObjects;

        public override IWorldGenerator.GeneratorTypes GetGeneratorType()
        {
            return GeneratorTypes.TERRAIN;
        }

        public override void Init()
        {
            if (Density.x <= 0 || Density.y <= 0)
            {
                Density.x = 0;
                Density.y = 1;
                GeneralLog.LogError(FileName + " Invalid Density " + Density);
            }
            for (int i = 0; i < GameObjects.Count; ++i)
            {
                GameObjects[i].ObjectId = i;
                GameObjects[i].RandomScale.UseXOnly = true;
                GameObjects[i].RandomOrientation.Init();
                GameObjects[i].RandomScale.Init();
            }

            base.Init();
        }

        public override void OnTerrainDataCreate(TerrainDatas Data)
        {
            GameObjectDatas GData = Data.GetCustomData<GameObjectDatas>(DataName);
            if (GData == null)
            {
                GData = new GameObjectDatas();
                Data.RegisterCustomData(DataName, GData);
            }
            GData.UID = UID;
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Biome, int ChunkId, int ChunkX, int ChunkZ)
        {
            int Size = Block.Information.VoxelPerChunk;
            int StartX = ChunkX * Size;
            int StartZ = ChunkZ * Size;

            GameObjectDatas GData = Block.GetCustomData<GameObjectDatas>(DataName);
            FastRandom FRandom = new FastRandom(Block.Position.GetHashCode() + Block.Information.IntSeed + ChunkId);
            GameObjectInfo Info = null;
            int[] Grounds = Biome.GetGrounds(Size, Size);

            int Chance, TotalChance = 0;
            int x, y, i, z, f = 0;
            Vector3 Orientation = new Vector3(), Scale = new Vector3();
            SFastRandom SRandom = new SFastRandom(1);

            BiomeData.TempObject Obj = new BiomeData.TempObject();
            float Slope = 0;
            for (x = StartX; x < StartX + Size; ++x)
            {
                for (z = StartZ; z < StartZ + Size; ++z, ++f)
                {
                    y = Grounds[f];

                    y -= Block.WorldY;

                    if (y <= 0 || y >= Block.VoxelsPerAxis)
                        continue;

                    if (FRandom.randomIntAbs((int)(Density.y / Information.Scale)) < (int)Density.x)
                    {
                        TotalChance = 0;

                        for (i = 0; i < GameObjects.Count; ++i)
                            TotalChance += GameObjects[i].Chance;

                        if (TotalChance == 0)
                            continue;

                        Chance = FRandom.randomIntAbs(TotalChance);
                        TotalChance = 0;

                        for (i = 0; i < GameObjects.Count; ++i)
                        {
                            Info = GameObjects[i];
                            if (Chance >= TotalChance && Chance < (TotalChance + Info.Chance))
                            {
                                if (Info.SlopeLimits.x != Info.SlopeLimits.y)
                                {
                                    Slope = BlockServer.GetNormalY(x, y, z, false);
                                    if (Slope >= Info.SlopeLimits.x && Slope < Info.SlopeLimits.y)
                                        break;
                                }

                                Info.RandomScale.UseXOnly = true;
                                SRandom.InitSeed(ChunkId * (x * y * z));
                                Info.RandomOrientation.Random(ref SRandom, ref Orientation);
                                Info.RandomScale.Random(ref SRandom, ref Scale);

                                Obj.Id = (byte)(i+1);
                                Obj.Orientation = Orientation.y;
                                Obj.Scale = (float)System.Math.Round((double)Scale.x, 2);
                                Obj.x = (ushort)(x);
                                Obj.y = (ushort)y;
                                Obj.z = (ushort)(z);
                                Biome.Objects.Add(Obj);
                                break;
                            }

                            TotalChance += Info.Chance;
                        }
                    }
                }
            }

            if (Biome.Objects.Count != 0)
            {
                GData.SetObjects(Biome.Objects);
            }

            Biome.Objects.ClearFast();

            return true;
        }

        public GameObjectInfo GetInfo(string Name)
        {
            if (Name == "")
                return null;

            foreach (GameObjectInfo Info in GameObjects)
            {
                if (Info.Name == Name)
                    return Info;
            }

            return null;
        }

        public override bool OnVoxelStartModification(ITerrainBlock Block, uint TObject, ref VoxelFlush Flush)
        {
            if (Flush.NewVolume <= 0.5f || Flush.NewType == 0)
            {
                GameObjectDatas GData = Block.Voxels.GetCustomData<GameObjectDatas>(DataName);
                GData.SetByte(Flush.x, Flush.y, Flush.z, 0);
            }
            return true;
        }
    }
}
