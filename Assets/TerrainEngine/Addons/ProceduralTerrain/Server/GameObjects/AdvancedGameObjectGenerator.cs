﻿using System;

namespace TerrainEngine
{
    /// <summary>
    /// This class will add gameobjects to the terrain depending of Altitude, Biome, depth, density, etc
    /// </summary>
    [Serializable]
    public class AdvancedGameObjectGenerator : GameObjectGenerator
    {
        public NoiseObjectsContainer ObjectsRequirements;
        public NoisesContainer RequirementNoises;

        public override IWorldGenerator.GeneratorTypes GetGeneratorType()
        {
            return GeneratorTypes.GROUND;
        }

        public override bool IsUsingGroundPoints()
        {
            return true;
        }

        public override void Init()
        {
            base.Init();

            ObjectsRequirements.Container = RequirementNoises;

            foreach (NoiseObjects Noise in ObjectsRequirements.NoisesObjects)
            {
                foreach (NoiseObject Info in Noise.Objects)
                {
                    Info.Object = GetInfo(Info.Name);
                    if (Info.GroundType == -1)
                    {
                        Info.GroundType = 0;
                        Info.MaxHeight = 999;
                        Info.MinHeight = 0;
                        Info.VoxelType = VoxelTypes.OPAQUE;
                    }
                }
            }

            ObjectsRequirements.Init(this,null, Information.IntSeed);

            for (int i = 0; i < GameObjects.Count; ++i)
            {
                GameObjects[i].NoiseScaleModule = ObjectsRequirements.Container.GetNoiseModule(GameObjects[i].NoiseNameScale);
            }
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Biome, int ChunkId, int ChunkX, int ChunkZ)
        {
            int StartX = ChunkX * Information.VoxelPerChunk;
            int StartZ = ChunkZ * Information.VoxelPerChunk;
            GameObjectDatas GData = Block.GetCustomData<GameObjectDatas>(DataName);

            SelecterContainer SC = new SelecterContainer();
            SC.Biome = Biome;
            SC.Data = GData;
            SC.BlockServer = BlockServer;
            SC.Block = Block;
            SC.Container = ObjectsRequirements;

            GenerateObjects(Thread, ref SC, ChunkId, StartX, StartZ, Density, Density, OnAdd);

            if (Biome.Objects.Count != 0)
            {
                GData.SetObjects(Biome.Objects);
            }

            Biome.Objects.ClearFast();

            return true;
        }

        public void OnAdd(BiomeData Biome, ICustomTerrainData Data, int x, int y, int z, int StartX, int StartZ, IObjectBaseInformations Info)
        {
            float Scale = 1f;
            if (Info.NoiseScaleModule != null)
            {
                Scale = (float)Info.NoiseScaleModule.GetValue(x + Data.Terrain.Position.x, y + Data.Terrain.Position.y, z + Data.Terrain.Position.z);
                Scale = (Info as GameObjectInfo).RandomScale.RandomF(Scale);
            }
            else
                Scale = (Info as GameObjectInfo).RandomScale.RandomNoise(Data.Terrain.WorldX+x,Data.Terrain.WorldZ+z);

            Biome.Objects.Add(new BiomeData.TempObject() { Id = (byte)(Info.ObjectId + 1), x = (ushort)x, y = (ushort)y, z = (ushort)z, Scale = Scale, Orientation = (int)(Info as GameObjectInfo).RandomOrientation.RandomF(ref Biome.Random) });
        }
    }
}
