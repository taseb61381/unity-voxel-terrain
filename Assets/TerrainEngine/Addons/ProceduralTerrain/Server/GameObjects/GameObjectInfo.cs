using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

/// <summary>
/// Class that Contains GameObject Informations for the GameObjectProcessor
/// </summary>
[Serializable]
public class GameObjectInfo : IObjectBaseInformations
{
    [XmlIgnore]
    public GameObject Object = null;

    [XmlIgnore]
    public GameObject LodObject = null;

    public RandomInfo RandomOrientation = new RandomInfo();
    public RandomInfo RandomScale = new RandomInfo();
    public bool AutoAlignToGround = true;
    public float YOffset = 0;
    public bool AutoGenerateCollider = false;
    public bool UsePooling = true;

    [XmlIgnore]
    public bool UseFading = true;

    [XmlIgnore]
    public bool UseLod = false;

    [XmlIgnore]
    public Material LODMaterial;

    [XmlIgnore]
    public float LodDistance = 100;

    [HideInInspector,XmlIgnore]
    public float MaxDrawDistance = 0;

    [XmlIgnore]
    public List<LodInfo> Lods = new List<LodInfo>();

    public override void CopyFrom(IObjectBaseInformations Info)
    {
        base.CopyFrom(Info);
        GameObjectInfo Other = Info as GameObjectInfo;

        this.RandomScale.CopyFrom(Other.RandomScale);
        this.RandomOrientation.CopyFrom(Other.RandomOrientation);

        this.AutoAlignToGround = Other.AutoAlignToGround;
        this.YOffset = Other.YOffset;
        this.AutoGenerateCollider = Other.AutoGenerateCollider;
        this.UsePooling = Other.UsePooling;
        this.UseFading = Other.UseFading;
        this.UseLod = Other.UseLod;
        this.LodDistance = Other.LodDistance;

        Lods.Clear();
        foreach (LodInfo LOD in Other.Lods)
        {
            LodInfo NewLOD = new LodInfo();
            NewLOD.CopyFrom(LOD);
            Lods.Add(NewLOD);
        }
    }

    public GameObject GetObject()
    {
        if (LodObject != null)
            return LodObject;
        else
            return Object;
    }

    public override Vector3 GetOrientation(int x, int y, int z)
    {
        return base.GetOrientation(x, y, z);
    }

    public override GameObject GetObject(int x, int y, int z)
    {
        return GetObject();
    }

    public override string GetName()
    {
        return Object != null ? Object.name : "";
    }


    #region ScriptingSystem

    public IGameObjectsScriptsContainer Container;

    #endregion
};