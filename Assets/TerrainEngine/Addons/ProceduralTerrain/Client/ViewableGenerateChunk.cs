﻿using System.Collections.Generic;


namespace TerrainEngine
{
    public class ViewableGenerateChunk : IAction
    {
        public IViewableProcessor Processor;
        public TerrainChunkRef ChunkRef;
        public int GenerateId;

        public bool Contains(ushort BlockId, int ChunkId)
        {
            lock (Processor.ActiveChunks.array)
            {
                return Processor.Contains(BlockId, ChunkId);
            }
        }

        // Generate Mesh
        public override bool OnThreadUpdate(ActionThread Thread)
        {
            TerrainBlock Block = null;
            if (ChunkRef.IsValid(ref Block))
            {
                if (Contains(ChunkRef.BlockId, ChunkRef.ChunkId))
                {
                    Processor.GenerateMesh(Thread, Block, ChunkRef.ChunkId, Processor.Datas, GenerateId);
                }
            }

            Close();
            return true;
        }

        public override void Clear()
        {
            Processor = null;
        }
    }
}
