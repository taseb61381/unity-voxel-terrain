﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BiomeNoisesContainer))]
public class BiomeNoisesContainerInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        BiomeNoisesContainer Container = (BiomeNoisesContainer)target;
        DrawObjectNoises(Container.Container);

    }

    static public string NewNoiseName = "New Noise";
    static public int PreviewSize = 100;
    static public void DrawObjectNoises(NoisesContainer Noises)
    {
        if (Noises.Noises == null)
            Noises.Noises = new List<BiomeNoise>();

        GUILayout.BeginVertical("Noises", GUI.skin.window);

        Texture2D Preview;
        BiomeNoise Noise;
        for (int i = 0; i < Noises.Noises.Count; ++i)
        {
            Noise = Noises.Noises[i];

            GUILayout.BeginHorizontal("box");

            Preview = Noise.GetPreview(PreviewSize, PreviewSize, false);
            if (Preview == null)
                Preview = Noise.GetPreview(PreviewSize, PreviewSize, true);
            GUILayout.BeginVertical("box", GUILayout.Width(120), GUILayout.Height(130));
            {
                string NewName = GUILayout.TextField(Noise.Name);
                if (NewName != Noise.Name)
                {
                    foreach (NoisePreset Preset in Noises.NoisesPreset)
                    {
                        if (Preset.NoiseName == Noise.Name)
                            Preset.NoiseName = NewName;

                        if (Preset.Name == Noise.Name)
                            Preset.Name = NewName;
                    }

                    Noise.Name = NewName;
                }

                if (GUILayout.Button(Preview, GUIStyle.none, GUILayout.Width(100), GUILayout.Height(100)))
                {
                    Noise.GetPreview(PreviewSize, PreviewSize, true);
                }
            }
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            {
                if (Noise != null)
                {
                    bool Dirty = false;
                    GUILayout.BeginHorizontal();
                    BiomeNoise.NoiseTypes NoiseType = (BiomeNoise.NoiseTypes)EditorGUILayout.EnumPopup(Noise.NoiseType);
                    if (GUILayout.Button("X", GUILayout.Width(22)))
                    {
                        Noises.RemoveNoise(Noise);
                        Noises.RemovePreset(Noises.GetPreset(Noise));
                        continue;
                    }
                    GUILayout.EndHorizontal();
                    if (NoiseType != Noise.NoiseType)
                    {
                        Dirty = true;
                        Noise.NoiseType = NoiseType;
                    }

                    if (NoiseType == BiomeNoise.NoiseTypes.NameNoise)
                    {
                        GUILayout.Label("This noise will return 0 and add name to noise names.");
                    }
                    else
                    {
                        int SeedOffset = EditorGUILayout.IntField("Seed Offset", Noise.SeedOffset);
                        int Octaves = EditorGUILayout.IntField("Octaves", Noise.Octaves);
                        float Frequency = EditorGUILayout.FloatField("Frequency", Noise.Frequency);
                        float YFrequency = EditorGUILayout.FloatField("YFrequency", Noise.YFrequency);
                        float Lacunarity = EditorGUILayout.FloatField("Lacunarity", Noise.Lacunarity);
                        float Persistence = EditorGUILayout.FloatField("Persistence", Noise.Persistence);
                        if (NoiseType == BiomeNoise.NoiseTypes.VolcanoNoise)
                        {
                            float CraterSize = EditorGUILayout.FloatField("CraterSize", Noise.CraterSize);
                            if (CraterSize != Noise.CraterSize)
                            {
                                Dirty = true;
                                Noise.CraterSize = CraterSize;
                            }

                            float CraterHeight = EditorGUILayout.FloatField("CraterHeight", Noise.CraterHeight);
                            if (CraterHeight != Noise.CraterHeight)
                            {
                                Dirty = true;
                                Noise.CraterHeight = CraterHeight;
                            }
                        }

                        if (SeedOffset != Noise.SeedOffset)
                        {
                            Dirty = true;
                            Noise.SeedOffset = SeedOffset;
                        }
                        if (Octaves != Noise.Octaves)
                        {
                            Dirty = true;
                            Noise.Octaves = Octaves;
                        }
                        if (Frequency != Noise.Frequency)
                        {
                            Noise.Frequency = Frequency;
                            Dirty = true;
                        }
                        if (Lacunarity != Noise.Lacunarity)
                        {
                            Noise.Lacunarity = Lacunarity;
                            Dirty = true;
                        }
                        if (Persistence != Noise.Persistence)
                        {
                            Dirty = true;
                            Noise.Persistence = Persistence;
                        }

                        if (YFrequency != Noise.YFrequency)
                        {
                            Dirty = true;
                            Noise.YFrequency = YFrequency;
                        }
                    }

                    if (Noise.PreCalculations == null)
                        Noise.PreCalculations = new List<BiomeNoise.PrecalculationInformations>();

                    foreach (BiomeNoise.PrecalculationInformations Info in Noise.PreCalculations)
                    {
                        GUILayout.BeginVertical("box");
                        GUILayout.BeginHorizontal();
                        Info.Name = (BiomeNoise.BiomePreCalculationTypes)EditorGUILayout.EnumPopup(Info.Name);

                        if (GUILayout.Button("X", GUILayout.Width(22)))
                        {
                            Noise.PreCalculations.Remove(Info);
                            return;
                        }
                        GUILayout.EndHorizontal();
                        switch (Info.Name)
                        {
                            case BiomeNoise.BiomePreCalculationTypes.BiasOutput:
                                Info.Bias = EditorGUILayout.FloatField("Bias", Info.Bias);
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.ScaleOutput:
                                Info.Scale = EditorGUILayout.FloatField("Scale", Info.Scale);
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.ScaleBias:
                                Info.Bias = EditorGUILayout.FloatField("Bias", Info.Bias);
                                Info.Scale = EditorGUILayout.FloatField("Scale", Info.Scale);
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.BiasScaleOutput:
                                Info.Bias = EditorGUILayout.FloatField("Bias", Info.Bias);
                                Info.Scale = EditorGUILayout.FloatField("Scale", Info.Scale);
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.ScaleInput:
                                Info.ScaleInput = EditorGUILayout.Vector3Field("Scale ", Info.ScaleInput);
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.ExponentialOutput:
                                Info.Exponent = EditorGUILayout.FloatField("Exponent", Info.Exponent);
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.ClampOutput:
                                Info.LowerBound = EditorGUILayout.FloatField("LowerBound", Info.LowerBound);
                                Info.UpperBound = EditorGUILayout.FloatField("UpperBound", Info.UpperBound);
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.TerraceOutput:
                                {
                                    GUILayout.BeginVertical("box");
                                    if (Info.ControlPoints == null)
                                        Info.ControlPoints = new List<float>();

                                    for (int j = 0; j < Info.ControlPoints.Count; ++j)
                                    {
                                        GUILayout.BeginHorizontal("box");
                                        Info.ControlPoints[j] = EditorGUILayout.FloatField(Info.ControlPoints[j], GUILayout.Width(60));
                                        if (GUILayout.Button("X", GUILayout.Width(40)))
                                            Info.ControlPoints.RemoveAt(j);
                                        GUILayout.EndHorizontal();
                                    }

                                    if (GUILayout.Button("Add"))
                                        Info.ControlPoints.Add(0);

                                    GUILayout.EndVertical();
                                }
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.CurveOutput:
                                {
                                    GUILayout.BeginVertical("box");
                                    if (Info.ControlPoints == null)
                                        Info.ControlPoints = new List<float>();

                                    if (Info.ControlPoints.Count % 2 != 0)
                                        Info.ControlPoints.Clear();

                                    for (int j = 0; j < Info.ControlPoints.Count; j+=2)
                                    {
                                        GUILayout.BeginHorizontal("box");
                                        Info.ControlPoints[j] = EditorGUILayout.FloatField(Info.ControlPoints[j], GUILayout.Width(60));
                                        Info.ControlPoints[j+1] = EditorGUILayout.FloatField(Info.ControlPoints[j+1], GUILayout.Width(60));
                                        if (GUILayout.Button("X", GUILayout.Width(40)))
                                        {
                                            Info.ControlPoints.RemoveAt(j+1);
                                            Info.ControlPoints.RemoveAt(j);
                                        }
                                        GUILayout.EndHorizontal();
                                    }

                                    if (GUILayout.Button("Add"))
                                    {
                                        Info.ControlPoints.Add(0);
                                        Info.ControlPoints.Add(1);
                                    }

                                    GUILayout.EndVertical();
                                }
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.Turbulence:
                                Info.Bias = EditorGUILayout.FloatField("Power", Info.Bias);
                                Info.Scale = EditorGUILayout.FloatField("Frequency", Info.Scale);
                                Info.Roughtness = EditorGUILayout.IntField("Roughness", Info.Roughtness);
                                break;
                            case BiomeNoise.BiomePreCalculationTypes.NoiseSelecterName:
                                {
                                    GUILayout.BeginVertical("box");
                                    if (Info.NameNoises == null)
                                        Info.NameNoises = new List<LibNoise.NoiseNameValues>();

                                    for (int j = 0; j < Info.NameNoises.Count; ++j)
                                    {
                                        LibNoise.NoiseNameValues NNoise = Info.NameNoises[j];
                                        GUILayout.BeginHorizontal("box");
                                        NNoise.Name = EditorGUILayout.TextField(NNoise.Name);
                                        NNoise.MinNoise = EditorGUILayout.FloatField(NNoise.MinNoise, GUILayout.Width(30));
                                        NNoise.MaxNoise = EditorGUILayout.FloatField(NNoise.MaxNoise, GUILayout.Width(30));
                                        NNoise.Alpha = EditorGUILayout.FloatField(NNoise.Alpha, GUILayout.Width(30));
                                        GUILayout.EndHorizontal();
                                        Info.NameNoises[j] = NNoise;
                                    }

                                    if (GUILayout.Button("Add Name Noise"))
                                        Info.NameNoises.Add(new LibNoise.NoiseNameValues());
                                    GUILayout.EndVertical();
                                }
                                break;

                        };

                        GUILayout.EndVertical();
                    }

                    if (GUILayout.Button("Add Calculation"))
                        Noise.PreCalculations.Add(new BiomeNoise.PrecalculationInformations());

                    if (Dirty)
                    {
                        Noise.GetPreview(PreviewSize, PreviewSize, true);
                    }
                }
            }
            GUILayout.EndVertical();

            GUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        NewNoiseName = GUILayout.TextField(NewNoiseName);
        if (GUILayout.Button("New"))
        {
            NoisePreset Preset = Noises.GetPreset(NewNoiseName);
            Noise = Noises.GetNoise(NewNoiseName);
            if (Preset == null)
            {
                Preset = new NoisePreset();
                Noises.NoisesPreset.Add(Preset);
            }

            if (Noise == null)
            {
                Noise = new BiomeNoise();
                Preset.Computation = NoiseComputations.NOISE;
                Noises.Noises.Add(Noise);
                Noise.Octaves = 1;
                Noise.Persistence = 1;
                Noise.NoiseType = BiomeNoise.NoiseTypes.Perlin;
                Noise.Lacunarity = 1;
                Noise.Frequency = 0.01f;
            }

            Preset.Name = Preset.NoiseName = Noise.Name = NewNoiseName;
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }
}
