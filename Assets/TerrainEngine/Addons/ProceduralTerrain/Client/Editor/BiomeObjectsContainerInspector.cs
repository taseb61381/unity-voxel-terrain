using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BiomeObjectsContainer))]
public class BiomeObjectsContainerInspector : Editor
{
    static public string CustomBiome = "";

    public List<string> NoiseNames;
    public List<string> SelectedObjects;
    public List<string> CheckedObjects;
    Vector2 TempNoises = new Vector2();

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }

    public void Draw(BiomeObjectsContainer Container, NoiseObjects Noise)
    {
        Noise.Density = EditorGUILayout.Vector2Field("Density", Noise.Density);

        foreach (NoiseObject Obj in Noise.Objects)
        {
            GUILayout.BeginVertical("box");

            GUILayout.BeginHorizontal();
            bool Exist = CheckedObjects.Contains(Obj.Name);
            bool ok = GUILayout.Toggle(Exist, "", GUILayout.Width(20));

            if (ok && !Exist)
                CheckedObjects.Add(Obj.Name);
            else if (ok == false && Exist)
                CheckedObjects.Remove(Obj.Name);

            if (GUILayout.Button(Obj.Name))
            {
                if (!SelectedObjects.Contains(Obj.Name))
                    SelectedObjects.Add(Obj.Name);
                else
                    SelectedObjects.Remove(Obj.Name);
            }
            if (GUILayout.Button("X", GUILayout.Width(22)))
            {
                Noise.Objects.Remove(Obj);
                break;
            }
            GUILayout.EndHorizontal();

            if (SelectedObjects.Contains(Obj.Name))
            {
                Obj.Name = EditorGUILayout.TextField("Name :", Obj.Name);
                Obj.NoisePreset = EditorGUILayout.TextField("NoisePreset :", Obj.NoisePreset);
                Vector2 p = EditorGUILayout.Vector2Field("Min/Max Noise", new Vector2(Obj.MinNoise, Obj.MaxNoise));
                Obj.MinNoise = p.x;
                Obj.MaxNoise = p.y;

                Vector2 C = EditorGUILayout.Vector2Field("Density ", new Vector2(Obj.Chance, Obj.MaxChance));
                Obj.Chance = (int)C.x;
                Obj.MaxChance = (int)C.y;


                if (Container.Container.NoisesPreset != null)
                {
                    GUILayout.BeginVertical("box");
                    GUILayout.Label("Noises : " + Container.Container.NoisesPreset.Count);
                    if (GUILayout.Button("None"))
                    {
                        Obj.NoisePreset = "";
                    }
                    foreach (NoisePreset Preset in Container.Container.NoisesPreset)
                    {
                        if (GUILayout.Button(Preset.Name))
                        {
                            Obj.NoisePreset = Preset.Name;
                        }
                    }
                    GUILayout.EndVertical();
                }
            }

            GUILayout.EndVertical();
        }

        if (Container.Container != null && Container.Container.NoisesPreset != null)
        {
            GUILayout.BeginHorizontal("box");
            GUILayout.Label("Selected :" + CheckedObjects.Count);
            if (GUILayout.Button("Select All"))
            {
                foreach (NoiseObject Obj in Noise.Objects)
                    if (!CheckedObjects.Contains(Obj.Name))
                        CheckedObjects.Add(Obj.Name);
            }
            if (GUILayout.Button("Select None"))
                CheckedObjects.Clear();

            GUILayout.EndHorizontal();
            GUILayout.BeginVertical("box");
            GUILayout.Label("Noises : " + Container.Container.NoisesPreset.Count);

            if (GUILayout.Button("None"))
            {
                foreach (string Name in CheckedObjects)
                {
                    Noise.GetObject(Name).NoisePreset = "";
                }
            }
            foreach (NoisePreset Preset in Container.Container.NoisesPreset)
            {
                if (GUILayout.Button(Preset.Name))
                {
                    foreach (string Name in CheckedObjects)
                    {
                        Noise.GetObject(Name).NoisePreset = Preset.Name;

                        if (TempNoises.x != 0 || TempNoises.y != 0)
                        {
                            Noise.GetObject(Name).MinNoise = TempNoises.x;
                            Noise.GetObject(Name).MaxNoise = TempNoises.y;
                        }
                    }
                }
            }

            TempNoises = EditorGUILayout.Vector2Field("Min/Max Noise", TempNoises);
            GUILayout.EndVertical();
        }
    }

    static public void DrawBiome(string CurrentName, BiomeNoisesContainer HeightContainer, BiomeObjectsContainer BiomeObjects, List<string> SelectedObjects, List<string> OpenedBiomes, string WorldNoiseObjsName)
    {
        GUILayout.BeginHorizontal();

        bool IsOpened = OpenedBiomes.Contains(WorldNoiseObjsName);
        bool NewOpened = EditorGUILayout.Foldout(IsOpened, WorldNoiseObjsName);

        if (NewOpened && !IsOpened)
            OpenedBiomes.Add(WorldNoiseObjsName);
        else if(!NewOpened && IsOpened)
            OpenedBiomes.Remove(WorldNoiseObjsName);


        if (BiomeObjects.Objects == null)
            BiomeObjects.Objects = new NoiseObjectsContainer();

        NoiseObjects NoiseObjs = BiomeObjects.Objects.GetNoise(WorldNoiseObjsName);

        NoiseObject NoiseObj = null;

        if (NoiseObjs != null)
            NoiseObj = NoiseObjs.GetObject(CurrentName);

        if (NoiseObj == null && GUILayout.Button("Add", GUILayout.Width(70)))
        {
            OpenedBiomes.Add(WorldNoiseObjsName);

            if (NoiseObjs == null)
            {
                NoiseObjs = new NoiseObjects(WorldNoiseObjsName);
                BiomeObjects.Objects.NoisesObjects.Add(NoiseObjs);
            }

            NoiseObj = new NoiseObject(CurrentName);
            NoiseObjs.Objects.Add(NoiseObj);
        }
        else if (NoiseObj != null && GUILayout.Button("Remove", GUILayout.Width(90)))
        {
            NoiseObjs.Objects.Remove(NoiseObj);
        }

        GUILayout.EndHorizontal();

        if(NewOpened)
        {
            EditorGUILayout.BeginVertical("box");
            if (NoiseObjs == null)
            {
                NoiseObjs = new NoiseObjects(WorldNoiseObjsName);
                BiomeObjects.Objects.NoisesObjects.Add(NoiseObjs);
            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Global Dens: Min/Max", GUILayout.Width(220));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            NoiseObjs.Density.x = EditorGUILayout.FloatField(NoiseObjs.Density.x, GUILayout.Width(50));
            NoiseObjs.Density.y = EditorGUILayout.FloatField(NoiseObjs.Density.y, GUILayout.Width(50));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginVertical("",GUI.skin.box);
            {
                NoiseObjs = BiomeObjects.Objects.GetNoise(WorldNoiseObjsName);
                NoiseObj = null;
                if (NoiseObjs != null)
                {
                    NoiseObj = NoiseObjs.GetObject(CurrentName);
                    if (NoiseObj != null)
                    {
                        if (NoiseObj.NoisePreset == null)
                            NoiseObj.NoisePreset = "";

                        NoiseObj.VoxelName = EditorGUILayout.TextField("Voxel Name : ", NoiseObj.VoxelName);

                        //if (NoiseObj.VoxelName == null || NoiseObj.VoxelName.Length == 0)
                        {
                            NoiseObj.GroundType = EditorGUILayout.IntField("Voxel Id : ", NoiseObj.GroundType);
                            NoiseObj.VoxelType = (VoxelTypes)EditorGUILayout.EnumPopup("Voxel Type : ", NoiseObj.VoxelType);
                        }

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("Required Noise :");
                        NoiseObj.RequiredNoise = GUILayout.TextField(NoiseObj.RequiredNoise);
                        if (GUILayout.Button("AtS", GUILayout.Width(40)))
                        {
                            foreach (string Name in SelectedObjects)
                            {
                                if (Name == CurrentName) continue;
                                NoiseObject Other = NoiseObjs.GetObject(Name);
                                if (Other == null) continue;
                                Other.RequiredNoise = NoiseObj.RequiredNoise;
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("Height: Min/Max");
                        NoiseObj.MinHeight = EditorGUILayout.IntField(NoiseObj.MinHeight, GUILayout.Width(50));
                        NoiseObj.MaxHeight = EditorGUILayout.IntField(NoiseObj.MaxHeight, GUILayout.Width(50));
                        if (GUILayout.Button("AtS",GUILayout.Width(40)))
                        {
                            foreach (string Name in SelectedObjects)
                            {
                                if (Name == CurrentName) continue;
                                NoiseObject Other = NoiseObjs.GetObject(Name);
                                if (Other == null) continue;
                                Other.MinDepth = NoiseObj.MinDepth;
                                Other.MaxDepth = NoiseObj.MaxDepth;
                                Other.MinHeight = NoiseObj.MinHeight;
                                Other.MaxHeight = NoiseObj.MaxHeight;
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("Depth: Min/Max");
                        NoiseObj.MinDepth = EditorGUILayout.IntField(NoiseObj.MinDepth, GUILayout.Width(50));
                        NoiseObj.MaxDepth = EditorGUILayout.IntField(NoiseObj.MaxDepth, GUILayout.Width(50));
                        if (GUILayout.Button("AtS", GUILayout.Width(40)))
                        {
                            foreach (string Name in SelectedObjects)
                            {
                                if (Name == CurrentName) continue;
                                NoiseObject Other = NoiseObjs.GetObject(Name);
                                if (Other == null) continue;
                                Other.MinDepth = NoiseObj.MinDepth;
                                Other.MaxDepth = NoiseObj.MaxDepth;
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("Density: Min/Max");
                        NoiseObj.Chance = EditorGUILayout.IntField(NoiseObj.Chance, GUILayout.Width(50));
                        NoiseObj.MaxChance = EditorGUILayout.IntField(NoiseObj.MaxChance, GUILayout.Width(50));
                        if (GUILayout.Button("AtS", GUILayout.Width(40)))
                        {
                            foreach (string Name in SelectedObjects)
                            {
                                if (Name == CurrentName) continue;
                                NoiseObject Other = NoiseObjs.GetObject(Name);
                                if (Other == null) continue;
                                Other.Chance = NoiseObj.Chance;
                                Other.MaxChance = NoiseObj.MaxChance;
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("Noise Name:");
                        NoiseObj.NoisePreset = GUILayout.TextField(NoiseObj.NoisePreset);
                        if (GUILayout.Button("AtS", GUILayout.Width(40)))
                        {
                            foreach (string Name in SelectedObjects)
                            {
                                if (Name == CurrentName) continue;
                                NoiseObject Other = NoiseObjs.GetObject(Name);
                                if (Other == null) continue;
                                Other.NoisePreset = NoiseObj.NoisePreset;
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("Noise: Min/Max");
                        NoiseObj.MinNoise = EditorGUILayout.FloatField(NoiseObj.MinNoise, GUILayout.Width(50));
                        NoiseObj.MaxNoise = EditorGUILayout.FloatField(NoiseObj.MaxNoise, GUILayout.Width(50));
                        if (GUILayout.Button("AtS", GUILayout.Width(40)))
                        {
                            foreach (string Name in SelectedObjects)
                            {
                                if (Name == CurrentName) continue;
                                NoiseObject Other = NoiseObjs.GetObject(Name);
                                if (Other == null) continue;
                                Other.MinNoise = NoiseObj.MinNoise;
                                Other.MaxNoise = NoiseObj.MaxNoise;
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("Automatic Noise Creation");
                        NoiseObj.AutomaticNoiseCreation = EditorGUILayout.Toggle(NoiseObj.AutomaticNoiseCreation);
                        if (GUILayout.Button("AtS", GUILayout.Width(35)))
                        {
                            foreach (string Name in SelectedObjects)
                            {
                                if (Name == CurrentName) continue;
                                NoiseObject Other = NoiseObjs.GetObject(Name);
                                if (Other == null) continue;
                                Other.AutomaticNoiseCreation = NoiseObj.AutomaticNoiseCreation;
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        GUILayout.Label("Restricted Positions");
                        if (NoiseObj.Positions != null && NoiseObj.Positions.Length > 0)
                        {
                            for (int i = 0; i < NoiseObj.Positions.Length; ++i)
                            {
                                EditorGUILayout.BeginVertical("box");
                                EditorGUILayout.Vector3Field("WorldPosition", NoiseObj.Positions[i].WorldPosition);
                                EditorGUILayout.Vector3Field("Size",NoiseObj.Positions[i].Size);
                                EditorGUILayout.EndVertical();
                            }
                            if (GUILayout.Button("Remove"))
                                System.Array.Resize(ref NoiseObj.Positions, NoiseObj.Positions.Length - 1);
                        }
                        if (GUILayout.Button("Add"))
                        {
                            if (NoiseObj.Positions == null || NoiseObj.Positions.Length == 0)
                                NoiseObj.Positions = new NoiseObjectPosition[1];
                        }



                        if (GUILayout.Button("Remove", GUILayout.Width(70)))
                        {
                            NoiseObjs.Objects.Remove(NoiseObj);
                        }
                    }
                }
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndVertical();
        }
    }

    static public List<string> Biomes = new List<string>();
    static public void DrawBiomesObjects(InspectorTempData TempData, AdvancedHeightMap HeightMap, BiomeNoisesContainer HeightContainer, BiomeObjectsContainer BiomeObjects)
    {
        if (HeightContainer == null|| BiomeObjects == null)
        {
            Debug.LogError("HeightContainer:" + HeightContainer + " or BiomesObjects:" + BiomeObjects + " = NULL");
            return;
        }

        Biomes.Clear();
        HeightContainer.GetBiomes(HeightContainer.ExternalContainers, Biomes);
        if (Biomes.Count == 0)
            HeightContainer.OnBeforeSerialize();

        for (int i = 0; i < Biomes.Count; ++i)
        {
            DrawBiome(TempData.CurrentName, HeightContainer, BiomeObjects, TempData.SelectedObjects, TempData.OpenedBiomes, Biomes[i]);
        }

        if (GUILayout.Button("Apply To Selected (" + TempData.SelectedObjects.Count + ")", GUILayout.Width(200)))
        {
            foreach (string Name in TempData.SelectedObjects)
            {
                if (Name == TempData.CurrentName)
                    continue;

                foreach (string BiomeName in Biomes)
                {
                    NoiseObjects NoiseObjs = BiomeObjects.Objects.GetNoise(BiomeName);
                    NoiseObject NoiseObj = null;
                    NoiseObject NewObj = null;
                    if (NoiseObjs != null)
                    {
                        NoiseObj = NoiseObjs.GetObject(TempData.CurrentName);
                        NewObj = NoiseObjs.GetObject(Name);
                        if (NoiseObj != null)
                        {
                            if (NewObj == null)
                            {
                                NewObj = new NoiseObject(Name);
                                NoiseObjs.Objects.Add(NewObj);
                            }

                            NewObj.CopyFrom(NoiseObj);
                        }
                        else
                        {
                            if (NewObj != null)
                                NoiseObjs.Objects.Remove(NewObj);
                        }
                    }
                }
            }
        }
    }
}
