using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class MassTreeFallEditor : ScriptableWizard 
{
    public List<GameObject> Objects;
    public PhysicMaterial PhysicMaterial;
    public GameObject ObjectToSpawnWhenFalling;
    public string RequiredName = ""; // Example : 'Snow', all objects with Snow in name will be affected

    [MenuItem("TerrainEngine/Tools/GameObjects Mass Scripts")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<MassTreeFallEditor>("MAssTreeFallEditor", "Add Script","Search");
    }

    void OnWizardOtherButton()
    {
        if (Objects == null)
            Objects = new List<GameObject>();

        GameObjectProcessor[] Processors = UnityEngine.Object.FindObjectsOfType(typeof(GameObjectProcessor)) as GameObjectProcessor[];
        Debug.Log("Processors to : " + Processors.Length);
        foreach (GameObjectProcessor Processor in Processors)
        {
            Debug.Log("Processor " + Processor + ", Objects : " + Processor.Container.Objects.Count);

            foreach (GameObjectInfo Info in Processor.Container.Objects)
            {
                if (Info.Object != null)
                {
                    Objects.Add(Info.Object);
                }
            }
        }

        Selection.objects = Objects.ToArray();
    }

    void OnWizardCreate()
    {
        Debug.Log("Adding Scripts to : " + Objects.Count + " Object(s)");

        GS_AddRigidbody Script = null;
        RequiredName = RequiredName.ToLower();
        foreach (GameObject Obj in Objects)
        {
            if (PrefabUtility.GetPrefabObject(Obj) == null)
                continue;

            if (PrefabUtility.GetPrefabType(Obj) != PrefabType.Prefab)
                continue;

            if (RequiredName != "" && !Obj.name.ToLower().Contains(RequiredName))
                continue;

            Script = Obj.GetComponent<GS_AddRigidbody>();
            if (Script == null)
                Script = Obj.AddComponent<GS_AddRigidbody>();

            if(PhysicMaterial != null)
                Script.Physic = PhysicMaterial;

            Script.ToInstanciateOnActive = ObjectToSpawnWhenFalling; 
        }
    }
}
