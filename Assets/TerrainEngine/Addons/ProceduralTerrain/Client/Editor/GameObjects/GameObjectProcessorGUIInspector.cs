using UnityEditor;

[CustomEditor(typeof(GameObjectProcessor))]
public class GameObjectProcessorGUIInspector : Editor
{
    public InspectorTempData TempData;
    public override void OnInspectorGUI()
    {
        if (TempData == null)
            TempData = new InspectorTempData();

        GameObjectProcessor Processor = (GameObjectProcessor)target;

        if (Processor.DataName == null || Processor.DataName.Length <= 0)
        {
            EditorGUILayout.HelpBox("Data Name empty", MessageType.Error);
        }

        if (Processor.Density.x == 0 || Processor.Density.y == 0)
        {
            EditorGUILayout.HelpBox("Density invalid", MessageType.Warning);
        }

        base.OnInspectorGUI();
        GameObjectsContainerInspector.DrawObjects(Processor.Container, TempData);
    }
}
