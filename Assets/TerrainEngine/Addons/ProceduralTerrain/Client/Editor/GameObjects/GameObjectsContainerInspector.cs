using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class InspectorTempData
{
    public int Selected = 0;
    public List<string> SelectedObjects = new List<string>();
    public List<string> OpenedBiomes = new List<string>();
    public List<string> FilterBiomes = new List<string>();
    public List<IObjectBaseInformations> FilterObjects = new List<IObjectBaseInformations>();
    public bool IsBiomeFilter = false;
    public Dictionary<string, bool> Foldouts = new Dictionary<string, bool>();
    public string CurrentName = "";
}

[CustomEditor(typeof(GameObjectsContainer))]
public class GameObjectsContainerInspector : Editor
{
    public InspectorTempData TempData;

    static public bool CanDrawLOD = false;

    public override void OnInspectorGUI()
    {
        if (TempData == null)
            TempData = new InspectorTempData();

        GameObjectsContainer Processor = (GameObjectsContainer)target;
        DrawObjects(Processor, TempData);
        base.OnInspectorGUI();
    }

    static public Texture2D GetAssetPreview(UnityEngine.Object Obj)
    {
        if (Obj == null)
            return null;

        return AssetPreview.GetAssetPreview(Obj);
    }

    static public GameObjectInfo DrawObjectsSelection(GameObjectsContainer Processor, InspectorTempData TempData)
    {
        GUIStyle ButtonStyle = GUI.skin.button;
        GUIStyle BoxStyle = GUI.skin.box;
        ButtonStyle.alignment = BoxStyle.alignment = TextAnchor.MiddleCenter;
        ButtonStyle.imagePosition = BoxStyle.imagePosition = ImagePosition.ImageAbove;
        GameObjectInfo GInfo;

        GUILayout.BeginVertical(GUI.skin.box);
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Ctrl+Click / Shift+Click");
                if (GUILayout.Button("All", GUILayout.Width(80)))
                {
                    TempData.SelectedObjects.Clear();
                    foreach (GameObjectInfo SubInfo in Processor.Objects)
                    {
                        string Name = SubInfo.Object != null ? SubInfo.Object.name : "";
                        TempData.SelectedObjects.Add(Name);
                    }
                }
                if (GUILayout.Button("None", GUILayout.Width(80)))
                {
                    TempData.SelectedObjects.Clear();
                }
            }
            GUILayout.EndHorizontal();

            if (Processor.Objects == null)
                Processor.Objects = new List<GameObjectInfo>();

            EditorGUILayout.BeginHorizontal();
            {
                int TotalChance = 0;

                if (TempData.Selected >= Processor.Objects.Count)
                    TempData.Selected = Processor.Objects.Count - 1;

                for (int i = 0; i < Processor.Objects.Count + 1; ++i)
                {
                    if (i % ((Screen.width - 50) / 102) == 0)
                    {
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.BeginHorizontal();
                    }

                    if (i >= Processor.Objects.Count)
                    {
                        if (GUILayout.Button("New", GUILayout.Width(90), GUILayout.Height(90)))
                        {
                            TempData.Selected = Processor.Objects.Count;
                            Processor.Objects.Add(new GameObjectInfo());
                        }
                    }
                    else
                    {
                        GInfo = Processor.Objects[i];

                        TotalChance += GInfo.Chance;

                        if (GInfo.RandomScale.Min.x <= 0) GInfo.RandomScale.Min.x = 1;
                        if (GInfo.RandomScale.Min.y <= 0) GInfo.RandomScale.Min.y = 1;

                        string Name = GInfo.Object != null ? GInfo.Object.name : "";
                        Texture2D Preview = GetAssetPreview(GInfo.Object);
                        bool ExistName = false;

                        foreach (GameObjectInfo SubInfo in Processor.Objects)
                            if (GInfo != SubInfo && GInfo.GetName() == SubInfo.GetName())
                                ExistName = true;

                        bool Sel = TempData.SelectedObjects.Contains(Name);

                        if (ExistName || GInfo.Chance <= 0)
                        {
                            GUI.backgroundColor = Color.red;
                        }
                        else
                        {
                            if (i == TempData.Selected)
                                GUI.backgroundColor = Color.blue;
                            else if (Sel)
                                GUI.backgroundColor = Color.cyan;
                        }

                        EditorGUILayout.BeginVertical(BoxStyle, GUILayout.Width(90));
                        {
                            EditorGUILayout.BeginHorizontal();
                            GInfo.Object = EditorGUILayout.ObjectField(GInfo.Object, typeof(GameObject), false, GUILayout.Width(72)) as GameObject;
                            bool Result = EditorGUILayout.Toggle(Sel);
                            EditorGUILayout.EndHorizontal();

                            if (Result != Sel)
                            {
                                if (Result)
                                    TempData.SelectedObjects.Add(Name);
                                else
                                    TempData.SelectedObjects.Remove(Name);
                            }

                            if (i == TempData.Selected)
                            {
                                GUILayout.Label(Preview, GUIStyle.none, GUILayout.Width(90), GUILayout.Height(90));
                            }
                            else
                            {
                                if (GUILayout.Button(Preview, GUIStyle.none, GUILayout.Width(90), GUILayout.Height(90)))
                                {
 
                                    if (Event.current.control)
                                    {
                                        if (!Result)
                                            TempData.SelectedObjects.Add(Name);
                                        else
                                            TempData.SelectedObjects.Remove(Name);
                                    }
                                    else if (Event.current.shift)
                                    {
                                        if (TempData.Selected < i)
                                        {
                                            for (; TempData.Selected <= i; ++TempData.Selected)
                                            {
                                                if (!TempData.SelectedObjects.Contains(Processor.Objects[TempData.Selected].Name))
                                                    TempData.SelectedObjects.Add(Processor.Objects[TempData.Selected].Name);
                                            }
                                        }
                                        else
                                        {
                                            for (int z = i; z <= TempData.Selected; ++z)
                                            {
                                                if (!TempData.SelectedObjects.Contains(Processor.Objects[z].Name))
                                                    TempData.SelectedObjects.Add(Processor.Objects[z].Name);
                                            }
                                        }
                                    }

                                    TempData.Selected = i;
                                }
                            }

                            if (ExistName)
                                EditorGUILayout.HelpBox("Name already used", MessageType.Error);
                        }
                        EditorGUILayout.EndVertical();

                        GUI.backgroundColor = Color.white;
                    }
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Separator();
        }
        GUILayout.EndVertical();

        GInfo = TempData.Selected >= 0 ? Processor.Objects[TempData.Selected] : null;
        if (GInfo == null)
            return null;

        EditorGUILayout.BeginHorizontal();
        {
            GInfo.Object = EditorGUILayout.ObjectField(GInfo.Object, typeof(GameObject), false, GUILayout.Width(200), GUILayout.Height(20)) as GameObject;
            if (Processor.Objects.Count != 0 && GUILayout.Button("Remove", GUILayout.Width(65)))
            {
                Processor.Objects.Remove(GInfo);
                return null;
            }

            if (Processor.Objects.Count != 0 && TempData.SelectedObjects.Count > 1 && GUILayout.Button("Remove Selected", GUILayout.Width(110)))
            {
                Processor.Objects.RemoveAll(info => TempData.SelectedObjects.Contains(info.Name));
                return null;
            }
        }
        EditorGUILayout.EndHorizontal();
        return GInfo;
    }

    static public void DrawObjectInfo(InspectorTempData TempData, GameObjectsContainer Processor, GameObjectInfo Info)
    {
        Info.RandomScale.Enabled = EditorGUILayout.Foldout(Info.RandomScale.Enabled, "Random Scale(Offset/Min/Max):" +Info.RandomScale.Enabled);
        if (Info.RandomScale.Enabled)
        {
            Info.NoiseNameScale = EditorGUILayout.TextField("Noise Name", Info.NoiseNameScale);
            EditorGUILayout.BeginHorizontal();
            Info.RandomScale.Offset.x = EditorGUILayout.FloatField(Info.RandomScale.Offset.x);
            Info.RandomScale.Min.x = EditorGUILayout.FloatField(Info.RandomScale.Min.x);
            Info.RandomScale.Max.x = EditorGUILayout.FloatField(Info.RandomScale.Max.x);
            EditorGUILayout.EndHorizontal();
        }

        Info.RandomOrientation.Enabled = EditorGUILayout.Foldout(Info.RandomOrientation.Enabled, "Random Orientation(Min/Max):" + Info.RandomOrientation.Enabled);
        if (Info.RandomOrientation.Enabled)
        {
            EditorGUILayout.BeginHorizontal();
            Info.RandomOrientation.Min.x = EditorGUILayout.FloatField(Info.RandomOrientation.Min.x);
            Info.RandomOrientation.Max.x = EditorGUILayout.FloatField(Info.RandomOrientation.Max.x);
            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.Space();

        //GUILayout.EndVertical();
        CanDrawLOD = EditorGUILayout.Foldout(CanDrawLOD, "LOD");
        if (!CanDrawLOD)
            return;

        GUILayout.BeginVertical("LOD :", GUI.skin.window);

        GUILayout.BeginHorizontal();

        Info.UseFading = EditorGUILayout.Toggle("LOD Fading", Info.UseFading);

        if (GUILayout.Button(Info.UseLod ? "Disable LOD" : "Active LOD"))
            Info.UseLod = !Info.UseLod;
        GUILayout.EndHorizontal();

        EditorGUILayout.Space();
        if (!Info.UseLod)
        {
            EditorGUILayout.EndVertical();
            return;
        }

        EditorGUILayout.BeginVertical(GUI.skin.box);
        {
            EditorGUILayout.BeginHorizontal();
            Info.LodDistance = EditorGUILayout.FloatField("Original: Dist 0->", Info.LodDistance);
            if (GUILayout.Button("Ats",GUILayout.Width(50)))
            {
                foreach (GameObjectInfo SubInfo in Processor.Objects)
                {
                    if (SubInfo == Info || !TempData.SelectedObjects.Contains(SubInfo.GetName()))
                        continue;

                    SubInfo.LodDistance = Info.LodDistance;
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("LOD : " + Info.Lods.Count + (Info.Lods.Count == 0 ? " (Removed After this distance)" : ""));
            float TotalRange = Info.LodDistance;

            for (int i = 0; i < Info.Lods.Count; ++i)
            {
                LodInfo LInfo = Info.Lods[i];

                EditorGUILayout.BeginVertical(GUI.skin.box);
                {
                    if (LInfo.Type == LodTypes.GAMEOBJECT)
                    {
                        EditorGUILayout.BeginHorizontal();
                        if (LInfo.LodObject != null)
                            GUILayout.Box(LInfo.LodObject != null ? AssetPreview.GetAssetPreview(LInfo.LodObject) : null, GUILayout.Width(50), GUILayout.Height(50));
                        LInfo.LodObject = EditorGUILayout.ObjectField(LInfo.LodObject, typeof(GameObject), false, GUILayout.Height(30)) as GameObject;
                        EditorGUILayout.EndHorizontal();
                    }
                    else if(LInfo.Type != LodTypes.BATCHING)
                    {
                        EditorGUILayout.BeginHorizontal();
                        if (LInfo.LodObject != null)
                            GUILayout.Box(LInfo.PlaneMaterial != null ? AssetPreview.GetAssetPreview(LInfo.PlaneMaterial) : null, GUILayout.Width(50), GUILayout.Height(50));
                        LInfo.PlaneMaterial = EditorGUILayout.ObjectField(LInfo.PlaneMaterial, typeof(Material), false, GUILayout.Height(30)) as Material;
                        EditorGUILayout.EndHorizontal();
                    }

                    if (Application.isPlaying)
                    {
                        EditorGUILayout.ObjectField(LInfo.PlaneTexture, typeof(Texture2D), true);
                    }

                    EditorGUILayout.BeginVertical();
                    {
                        LInfo.ActiveRange = EditorGUILayout.FloatField("Distance : " + TotalRange + " -> ", LInfo.ActiveRange);
                        LInfo.Type = (LodTypes)EditorGUILayout.EnumPopup("Type", LInfo.Type);

                        if (LInfo.ActiveRange <= TotalRange) LInfo.ActiveRange = TotalRange + 10;

                        if (LInfo.Type != LodTypes.GAMEOBJECT && LInfo.Type != LodTypes.BATCHING)
                        {
                            LInfo.ProceduralTexture = EditorGUILayout.Toggle("Procedural Texture", LInfo.ProceduralTexture);

                            if (LInfo.ProceduralTexture)
                            {
                                LInfo.ProceduralCameraBackground = EditorGUILayout.ColorField("Camera Background Color", LInfo.ProceduralCameraBackground);
                                LInfo.LodLightPower = EditorGUILayout.FloatField("Camera Light Power", LInfo.LodLightPower);
                                LInfo.Resolution = EditorGUILayout.IntField("Texture Resolution", LInfo.Resolution);
                            }
                        }

                        EditorGUILayout.BeginHorizontal();
                        {
                            LInfo.Billboard = EditorGUILayout.Toggle("Billboard", LInfo.Billboard);
                            if (GUILayout.Button("X", GUILayout.Width(20)))
                            {
                                Info.Lods.Remove(LInfo);
                                return;
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndVertical();

                TotalRange = LInfo.ActiveRange;
            }

            if (GUILayout.Button("New LOD"))
            {
                Info.Lods.Add(new LodInfo());
            }
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndVertical();
    }

    static public void DrawObjects(GameObjectsContainer Processor, InspectorTempData TempData)
    {
        if (Processor == null)
            return;

        GameObjectInfo Info = DrawObjectsSelection(Processor, TempData);
        if (Info == null)
            return;

        EditorGUILayout.BeginVertical("box");

        if (Info.Chance <= 0)
            GUI.backgroundColor = Color.red;

        Info.Chance = EditorGUILayout.IntField("Chance", Info.Chance);
        Info.HeightSide = (HeightSides)EditorGUILayout.EnumPopup("Ground Side : ", Info.HeightSide);

        Info.AutoAlignToGround = EditorGUILayout.Toggle("AutoAlignToGround", Info.AutoAlignToGround);
        Info.YOffset = EditorGUILayout.FloatField("Y Offset", Info.YOffset);

        EditorGUILayout.BeginHorizontal();
        Info.AlignToTerrain = EditorGUILayout.Foldout(Info.AlignToTerrain, "Align to Terrain");
        if (Info.AlignToTerrain)
        {
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            Info.AlignementClampGetSet = EditorGUILayout.FloatField("Alignment Clamp", Info.AlignementClampGetSet);

            if (GUILayout.Button("Ats(" + TempData.SelectedObjects.Count + ")", GUILayout.Width(140)))
            {
                foreach (GameObjectInfo SubInfo in Processor.Objects)
                {
                    if (SubInfo == Info || !TempData.SelectedObjects.Contains(SubInfo.GetName()))
                        continue;

                    SubInfo.AlignToTerrain = Info.AlignToTerrain;
                    SubInfo.AlignmentClamp = Info.AlignmentClamp;
                }
            }
            EditorGUILayout.EndHorizontal();
            Info.SlopeLimits = EditorGUILayout.Vector2Field("SlopeLimits", Info.SlopeLimits);
        }
        else
        {
            if (GUILayout.Button("Ats(" + TempData.SelectedObjects.Count + ")", GUILayout.Width(140)))
            {
                foreach (GameObjectInfo SubInfo in Processor.Objects)
                {
                    if (SubInfo == Info || !TempData.SelectedObjects.Contains(SubInfo.GetName()))
                        continue;

                    SubInfo.AlignToTerrain = Info.AlignToTerrain;
                    SubInfo.AlignmentClamp = Info.AlignmentClamp;
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        Info.AutoGenerateCollider = EditorGUILayout.Toggle("AutoGenerateCollider", Info.AutoGenerateCollider);
        Info.UsePooling = EditorGUILayout.Toggle("Use Pooling", Info.UsePooling);

        GUI.backgroundColor = Color.white;

        DrawObjectInfo(TempData, Processor, Info);

        if (GUILayout.Button("Apply To Selected (" + TempData.SelectedObjects.Count + ")", GUILayout.Width(200)))
        {
            foreach (GameObjectInfo SubInfo in Processor.Objects)
            {
                if (SubInfo == Info || !TempData.SelectedObjects.Contains(SubInfo.GetName()))
                    continue;

                SubInfo.CopyFrom(Info);
            }
        }

        EditorUtility.SetDirty(Processor);

        AdvancedHeightMap AdvancedHeight = FindObjectOfType(typeof(AdvancedHeightMap)) as AdvancedHeightMap;

        if (Processor.Objects != null && Info.Object != null && AdvancedHeight != null)
        {
            GUILayout.BeginVertical("'" + Info.GetName() + "' Valid Biomes", GUI.skin.window);
            {
                TempData.CurrentName = Info.GetName();
                BiomeObjectsContainerInspector.DrawBiomesObjects(TempData, AdvancedHeight, AdvancedHeight.Container, Processor.ObjectsNoises);
            }
            GUILayout.EndVertical();

            if (Processor.ObjectsNoises != null)
                BiomeNoisesContainerInspector.DrawObjectNoises(Processor.ObjectsNoises.Container);
        }
        GUILayout.EndVertical();
    }
}
