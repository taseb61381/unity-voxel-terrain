using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GrassProcessor))]
public class GrassGUIInspector : Editor
{
    public InspectorTempData TempData;

    public override void OnInspectorGUI()
    {
        if (TempData == null)
            TempData = new InspectorTempData();

        GrassProcessor Processor = (GrassProcessor)target;

        Processor.Priority = EditorGUILayout.IntField("Priority", Processor.Priority);

        Processor.DataName = EditorGUILayout.TextField("DataName", Processor.DataName);
        if (string.IsNullOrEmpty(Processor.DataName))
        {
            EditorGUILayout.HelpBox("Please, set a Data Name. This value is used to save/load/send processor data.Each Processor DataName must be different.", MessageType.Info); 
            return;
        }
        Processor.GlobalMaterial = EditorGUILayout.ObjectField("Material", Processor.GlobalMaterial, typeof(Material)) as Material;
        if (Processor.GlobalMaterial == null)
        {
            EditorGUILayout.HelpBox("Please selecter a grass material for this processor. The grass material will be used with all type of grass : Planes/Cross Plane/Leaf/Custom Mesh. Textures are combined, be sure to set all textures : 'readable'", MessageType.Info);
            return;
        }

        Processor.ViewDistance = EditorGUILayout.FloatField("ViewDistance", Processor.ViewDistance);
        Processor.ChunkSize = (TerrainEngine.VoxelSizes)EditorGUILayout.EnumPopup("ChunkSize", Processor.ChunkSize);

        Processor.FadeName = EditorGUILayout.TextField("FadeName", Processor.FadeName);

        if (!string.IsNullOrEmpty(Processor.FadeName))
        {
            if (!Processor.GlobalMaterial.HasProperty(Processor.FadeName))
            {
                EditorGUILayout.HelpBox("The material used do not contains a property named :'" + Processor.FadeName + "'.FadeName is property is used to fade objects when camera distance change", MessageType.Error);
            }
            else
                Processor.FadeTime = EditorGUILayout.FloatField("FadeTime", Processor.FadeTime);
        }

        EditorGUILayout.Separator();

        if (Processor.Container != null)
        {
            EditorGUILayout.BeginVertical("window");
            Processor.Density = EditorGUILayout.Vector2Field("Density", Processor.Density);
            GrassContainerInspector.DisplayGrass(Processor, Processor.Container, TempData);
            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.Separator();
    }
}
