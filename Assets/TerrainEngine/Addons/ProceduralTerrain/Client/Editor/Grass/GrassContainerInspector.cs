using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GrassContainer))]
public class GrassContainerInspector : Editor
{
    public InspectorTempData TempData;

    public override void OnInspectorGUI()
    {
        if (TempData == null)
            TempData = new InspectorTempData();
        //base.OnInspectorGUI();
        GrassContainer Processor = (GrassContainer)target;

        base.OnInspectorGUI();
        EditorGUILayout.Separator();
        DisplayGrass(null, Processor, TempData);
        EditorGUILayout.Separator();
    }

    static public void DisplayObjects(GrassProcessor Processor, GrassContainer Container, InspectorTempData TempData, AdvancedHeightMap AdvancedHeight)
    {
        GUIStyle ButtonStyle = GUI.skin.button;
        GUIStyle BoxStyle = GUI.skin.box;
        ButtonStyle.alignment = BoxStyle.alignment = TextAnchor.MiddleCenter;
        ButtonStyle.imagePosition = BoxStyle.imagePosition = ImagePosition.ImageAbove;

        int TotalChance = 0;
        GrassInfo Info = null;

        EditorGUILayout.BeginHorizontal();

        if (TempData.Selected >= TempData.FilterObjects.Count)
            TempData.Selected = TempData.FilterObjects.Count - 1;

        int Count = TempData.FilterObjects.Count;
        int Width = Screen.width - 100;
        if (Width < 90) Width = 90;

        for (int i = 0; i < Count + 1; ++i)
        {

            if (i % (Width / 90) == 0)
            {
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
            }

            if (i >= TempData.FilterObjects.Count)
            {
                if (GUILayout.Button("New", GUILayout.Width(90), GUILayout.Height(90)))
                {
                    TempData.Selected = Container.Grass.Count;
                    Container.Grass.Add(new GrassInfo());
                    return;
                }
            }
            else
            {
                Info = TempData.FilterObjects[i] as GrassInfo;

                TotalChance += Info.Chance;

                if (Info.RandomScale.Min.x <= 0) Info.RandomScale.Min.x = 1;
                if (Info.RandomScale.Min.y <= 0) Info.RandomScale.Min.y = 1;

                string Name = Info.GetName();
                bool Sel = TempData.SelectedObjects.Contains(Name);

                if (Info.Chance <= 0)
                    GUI.backgroundColor = Color.red;
                else if (i == TempData.Selected)
                    GUI.backgroundColor = Color.cyan;
                else if (Sel)
                    GUI.backgroundColor = Color.green;

                EditorGUILayout.BeginVertical(BoxStyle);
                {
                    EditorGUILayout.BeginHorizontal();
                    Info.Texture = EditorGUILayout.ObjectField(Info.Texture, typeof(Texture2D), false, GUILayout.Width(72)) as Texture2D;

                    bool Result = EditorGUILayout.Toggle(Sel);
                    EditorGUILayout.EndHorizontal();

                    if (Result != Sel)
                    {
                        if (Result)
                            TempData.SelectedObjects.Add(Name);
                        else
                            TempData.SelectedObjects.Remove(Name);
                    }

                    if (i == TempData.Selected)
                    {
                        GUILayout.Label(Info.Texture, GUIStyle.none, GUILayout.Width(90), GUILayout.Height(90));
                    }
                    else
                    {
                        if (GUILayout.Button(Info.Texture, GUIStyle.none, GUILayout.Width(90), GUILayout.Height(90)))
                        {
                            if (Event.current.control)
                            {
                                if (!Result)
                                    TempData.SelectedObjects.Add(Name);
                                else
                                    TempData.SelectedObjects.Remove(Name);
                            }
                            else if (Event.current.shift)
                            {
                                if (TempData.Selected < i)
                                {
                                    for (; TempData.Selected <= i; ++TempData.Selected)
                                    {
                                        if (!TempData.SelectedObjects.Contains(TempData.FilterObjects[TempData.Selected].Name))
                                            TempData.SelectedObjects.Add(TempData.FilterObjects[TempData.Selected].Name);
                                    }
                                }
                                else
                                {
                                    for (int z = i; z <= TempData.Selected; ++z)
                                    {
                                        if (!TempData.SelectedObjects.Contains(TempData.FilterObjects[z].Name))
                                            TempData.SelectedObjects.Add(TempData.FilterObjects[z].Name);
                                    }
                                }
                            }
                            TempData.Selected = i;
                        }
                    }
                }
                EditorGUILayout.EndVertical();
                GUI.backgroundColor = Color.white;
            }
        }
        GUILayout.EndHorizontal();

        Info = TempData.Selected >= 0 ? TempData.FilterObjects[TempData.Selected] as GrassInfo : null;
        if (Info == null)
            return;


        GUILayout.BeginVertical("box");
        {
            EditorGUILayout.BeginHorizontal();
            Info.Texture = EditorGUILayout.ObjectField(Info.Texture, typeof(Texture2D), false, GUILayout.Width(100), GUILayout.Height(100)) as Texture2D;
            if (GUILayout.Button("Remove") && Container.Grass.Count != 0)
            {
                TempData.Selected = 0;
                Container.Grass.Remove(Info);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical("box");
            {
                Info.RandomScale.Enabled = EditorGUILayout.Foldout(Info.RandomScale.Enabled, "Random Scale :" + Info.RandomScale.Enabled);
                if (Info.RandomScale.Enabled)
                {
                    EditorGUILayout.BeginVertical("box");

                    Info.RandomScale.Min = EditorGUILayout.Vector2Field("MinScale", Info.RandomScale.Min);
                    Info.RandomScale.Max = EditorGUILayout.Vector2Field("MaxScale", Info.RandomScale.Max);

                    if (Info.NoiseNameScale.Length != 0)
                    {
                        if (Container.GrassNoises.Container.GetPreset(Info.NoiseNameScale) != null)
                            GUI.backgroundColor = Color.green;
                        else
                        {
                            GUI.backgroundColor = Color.red;
                        }
                    }

                    if (GUI.backgroundColor == Color.red)
                    {
                        EditorGUILayout.HelpBox("Noise not found", MessageType.Error);
                    }
                    GUI.backgroundColor = Color.white;

                    Info.NoiseNameScale = EditorGUILayout.TextField("Noise Name Scale", Info.NoiseNameScale);

                    EditorGUILayout.BeginHorizontal();
                    Info.RandomScale.UseXOnly = GUILayout.Toggle(Info.RandomScale.UseXOnly, "Keep Proportions");
                    Info.AlignToGround = GUILayout.Toggle(Info.AlignToGround, "Align To Ground");
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.EndVertical();
                }
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical("box");
            {
                Info.GenerateColors = EditorGUILayout.Foldout(Info.GenerateColors, "Generate Noise Colors:" + Info.GenerateColors);

                if (Info.GenerateColors)
                {
                    Info.NoiseColorScale = EditorGUILayout.FloatField("Noise Scale", Info.NoiseColorScale);
                    EditorGUILayout.BeginHorizontal();
                    Info.NoiseColorA = EditorGUILayout.ColorField(Info.NoiseColorA);
                    Info.NoiseColorB = EditorGUILayout.ColorField(Info.NoiseColorB);
                    Info.NoiseColorC = EditorGUILayout.ColorField(Info.NoiseColorC);
                    EditorGUILayout.EndHorizontal();

                    if (GUILayout.Button("Apply To Selected (" + TempData.SelectedObjects.Count + ")", GUILayout.Width(200)))
                    {
                        foreach (GrassInfo SubInfo in Container.Grass)
                        {
                            if (SubInfo == Info || !TempData.SelectedObjects.Contains(SubInfo.GetName()))
                                continue;

                            SubInfo.GenerateColors = Info.GenerateColors;
                            SubInfo.NoiseColorA = Info.NoiseColorA;
                            SubInfo.NoiseColorB = Info.NoiseColorB;
                            SubInfo.NoiseColorC = Info.NoiseColorC;
                            SubInfo.NoiseColorScale = Info.NoiseColorScale;
                        }
                    }
                }
            }
            EditorGUILayout.EndVertical();

            Info.Chance = EditorGUILayout.IntField("Chance : " + Info.Chance + "/" + TotalChance, Info.Chance);
            Info.GrassType = (GrassTypes)EditorGUILayout.EnumPopup("Grass Type", Info.GrassType);
            Info.HeightSide = (HeightSides)EditorGUILayout.EnumPopup("Ground Side : ", Info.HeightSide);

            if (Info.GrassType == GrassTypes.PLANES || Info.GrassType == GrassTypes.HORIZONTAL_PLANE)
            {
                Vector2 planes = EditorGUILayout.Vector2Field("Planes Min/Max", new Vector2(Info.PlaneMin, Info.PlaneMax));
                Info.PlaneMin = (int)planes.x;
                Info.PlaneMax = (int)planes.y;
            }

            Info.SlopeLimits = EditorGUILayout.Vector2Field("SlopeLimits", Info.SlopeLimits);

            if (Info.GrassType == GrassTypes.CUSTOM_MESH)
            {
                Info.CustomMesh = EditorGUILayout.ObjectField("Custom Mesh Filter : ", Info.CustomMesh, typeof(MeshFilter), false) as MeshFilter;
                if (Info.CustomMesh != null)
                {
                    if (Info.CustomMesh.sharedMesh != null && (Info.CustomMesh.sharedMesh.normals.Length == 0 || Info.CustomMesh.sharedMesh.tangents.Length == 0))
                    {
                        EditorGUILayout.HelpBox("Invalid Mesh : No tangents or normals", MessageType.Error);
                    }
                    else
                        EditorGUILayout.LabelField("Vertices : " + Info.CustomMesh.sharedMesh.vertices.Length);
                }
            }

            if (GUILayout.Button("Apply To Selected (" + TempData.SelectedObjects.Count + ")", GUILayout.Width(200)))
            {
                foreach (GrassInfo SubInfo in Container.Grass)
                {
                    if (SubInfo == Info || !TempData.SelectedObjects.Contains(SubInfo.GetName()))
                        continue;

                    SubInfo.CopyFrom(Info);
                }
            }
        }
        GUILayout.EndVertical();
    }

    static public List<string> Biomes = new List<string>();
    static public void DisplayGrass(GrassProcessor Processor,GrassContainer Container, InspectorTempData TempData)
    {
        AdvancedHeightMap AdvancedHeight = FindObjectOfType(typeof(AdvancedHeightMap)) as AdvancedHeightMap;

        if (Container.Grass == null)
            Container.Grass = new List<GrassInfo>();

        GUILayout.BeginHorizontal();
        {
            GUILayout.Label("Ctrl+Click / Shift+Click");
            if (GUILayout.Button("All"))
            {
                TempData.SelectedObjects.Clear();
                foreach (GrassInfo SubInfo in Processor.Grass)
                {
                    TempData.SelectedObjects.Add(SubInfo.GetName());
                }
            }
            if (GUILayout.Button("None"))
            {
                TempData.SelectedObjects.Clear();
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        {
            GUILayout.Label("Filter:");
            if (GUILayout.Button(TempData.IsBiomeFilter ? "All" : "Biomes"))
            {
                TempData.IsBiomeFilter = !TempData.IsBiomeFilter;
                TempData.SelectedObjects.Clear();
                TempData.Selected = -1;
            }
        }
        GUILayout.EndHorizontal();

        if (AdvancedHeight == null)
            TempData.IsBiomeFilter = false;

        TempData.FilterObjects.Clear();

        if (TempData.IsBiomeFilter)
        {
            GUILayout.BeginVertical("window");

            Biomes.Clear();
            AdvancedHeight.Container.GetBiomes(null, Biomes);
            foreach (string biome in Biomes)
            {
                EditorGUILayout.BeginHorizontal();
                bool Value = GUILayout.Button(biome);
                EditorGUILayout.EndHorizontal();
                if (Value)
                {
                    TempData.Selected = -1;

                    if (!TempData.OpenedBiomes.Contains(biome))
                        TempData.OpenedBiomes.Add(biome);
                    else
                        TempData.OpenedBiomes.Remove(biome);
                }

                if (TempData.OpenedBiomes.Contains(biome))
                {
                    for (int i = 0; i < Processor.Grass.Count; ++i)
                    {
                        if (Container.GrassNoises.Objects.GetNoise(biome) != null && Container.GrassNoises.Objects.GetNoise(biome).GetObject(Processor.Grass[i].GetName()) != null)
                            TempData.FilterObjects.Add(Processor.Grass[i]);
                    }

                    DisplayObjects(Processor,Container, TempData, AdvancedHeight);
                }
            }
            GUILayout.EndVertical();
        }
        else
        {
            for (int i = 0; i < Processor.Grass.Count; ++i)
            {
                TempData.FilterObjects.Add(Processor.Grass[i]);
            }

            DisplayObjects(Processor,Container, TempData, AdvancedHeight);
        }

        GrassInfo Info = null;
        if (TempData.Selected >= 0 && TempData.Selected < Processor.Grass.Count)
            Info = Processor.Grass[TempData.Selected];

        if (Container.GrassNoises != null && Info != null && Info.Texture != null && AdvancedHeight != null)
        {
            GUILayout.BeginVertical("'" + Info.GetName() + "' Valid Biomes", GUI.skin.window);
            {
                TempData.CurrentName = Info.GetName();
                BiomeObjectsContainerInspector.DrawBiomesObjects(TempData, AdvancedHeight, AdvancedHeight.Container, Container.GrassNoises);
            }
            GUILayout.EndVertical();

            if (Container.GrassNoises.Container == null)
                Container.GrassNoises.Container = new NoisesContainer();

            BiomeNoisesContainerInspector.DrawObjectNoises(Container.GrassNoises.Container);
        }

        if (GUILayout.Button("Set Chance 1 if 0"))
            foreach (GrassInfo Obj in Processor.Grass)
                if (Obj.Chance == 0)
                    Obj.Chance = 1;

        EditorUtility.SetDirty(Processor);
    }
}
