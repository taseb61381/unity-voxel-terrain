﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.IO;

using LibNoise;
using LibNoise.Modifiers;
using LibNoise.Models;

using TerrainEngine;

public class HeightMapWindow : EditorWindow
{
    static public void curveFromTo(Rect wr, Rect wr2, Color color, Vector2 Offset, float Scale = 1f)
    {
        if (Event.current.type != EventType.Repaint)
            return;

        wr.x -= Offset.x;
        wr.y -= Offset.y;

        wr2.x -= Offset.x;
        wr2.y -= Offset.y;

        wr.x *= Scale;
        wr.y *= Scale;

        wr2.x *= Scale;
        wr2.y *= Scale;

        Drawing.bezierLine(
            new Vector2(wr.x + wr.width, wr.y + wr.height / 2),
            new Vector2(wr.x + wr.width + Mathf.Abs(wr2.x - (wr.x + wr.width)) / 2, wr.y + wr.height / 2),
            new Vector2(wr2.x, wr2.y + wr2.height / 2),
            new Vector2(wr2.x - Mathf.Abs(wr2.x - (wr.x + wr.width)) / 2, wr2.y + wr2.height / 2), color, 2, true, Mathf.Max(1,(int)(0.1f * Vector2.Distance(new Vector2(wr.x,wr.y), new Vector2(wr2.x,wr2.y)))));
    }

    [MenuItem("TerrainEngine/Processors/Procedural/HeightMapEditor")]
    public static void ShowWindow()
    {
        HeightMapWindow window = EditorWindow.GetWindow(typeof(HeightMapWindow), false, "HeightMap Editor", true) as HeightMapWindow;
        window.minSize = new Vector2(600.0f, 300.0f);
        window.wantsMouseMove = true;
        window.Show();
        EditorWindow.FocusWindowIfItsOpen(typeof(HeightMapWindow));
    }

    public int CurrentCategory = 0;
    public string CurrentContainer = "Main";
    public BiomeNoisesContainer SharedContainer;
    public HeightNoisesContainer Noises;
    public List<string> Biomes = new List<string>();
    public Dictionary<int, Texture2D> Previews = new Dictionary<int, Texture2D>();

    public int SelectedWindow = -1;
    public int SelectedValue = -1;
    public int PreviewSize = 128;
    public double PreviewZoom = 1;
    public Terrain PreviewTerrain;
    public int ThreadCount = 4;
    public TextAsset HeightMapFile;
    public bool HasChanged = false;
    public bool IsGraphPreview = false;
    public float GraphYOffset = 0, GraphXOffset=0;

    #region GUI

    #region Preview

    public Texture2D GetPreview(NoiseWindow Window, bool ForceUpdate = false, bool GenerateModule = true)
    {
        Texture2D Text = null;
        if (!Previews.TryGetValue(Window.UniqueId, out Text))
        {
            Text = new Texture2D(PreviewSize, PreviewSize);
            Previews.Add(Window.UniqueId, Text);

            UpdatePreview(Window, Text, GenerateModule);
        }
        else if (ForceUpdate)
            UpdatePreview(Window, Text, GenerateModule);

        return Text;
    }

    private double[] NoiseValues;

    public void UpdatePreview(NoiseWindow Window, Texture2D Text, bool GenerateModule = true)
    {
        SharedContainer.Noises.GenerateModules();

        try
        {
            if (Window.IsPreview)
            {
                IModule module = Noises.GetModule(Window.UniqueId) as IModule;

                if (module != null)
                {
                    double Dens = 0f;
                    if (NoiseValues == null || NoiseValues.Length != PreviewSize * PreviewSize)
                        NoiseValues = new double[PreviewSize * PreviewSize];

                    int x, y;
                    double Min = double.MaxValue, Max = double.MinValue;
                    int Index = 0;
                    for (x = 0; x < PreviewSize; ++x)
                    {
                        for (y = 0; y < PreviewSize; ++y)
                        {
                            Dens = NoiseValues[Index] = module.GetValue(x * PreviewZoom + (IsGraphPreview ? GraphXOffset*PreviewZoom : 0), IsGraphPreview ? GraphYOffset : y * PreviewZoom);
                            if (Dens < Min)
                                Min = Dens;
                            if (Dens > Max)
                                Max = Dens; 

                            if(IsGraphPreview)
                            {
                                Index += PreviewSize;
                                break;
                            }

                            ++Index;
                        }
                    }

                    Debug.Log("Min:"+Min+",Max:"+Max);

                    Index = 0;

                    if (!IsGraphPreview)
                    {
                        for (x = 0; x < PreviewSize; ++x)
                        {
                            for (y = 0; y < PreviewSize; ++y)
                            {
                                Dens = NoiseValues[Index];
                                Dens = (Dens - Min) / (Max - Min);
                                Text.SetPixel(x, y, new Color((float)Dens, (float)Dens, (float)Dens, 1f));
                                ++Index;
                            }
                        }
                    }
                    else
                    {
                        int MidSize = PreviewSize / 2;

                        for (x = 0; x < PreviewSize; ++x)
                        {
                            Dens = NoiseValues[x * PreviewSize];

                            if(Dens > 0)
                            {
                                for (y = 0; y < PreviewSize; ++y)
                                {
                                    Text.SetPixel(x, y, Color.white);


                                    if (y == (int)(MidSize + (Dens * MidSize)))
                                        Text.SetPixel(x, y, Color.black);
                                }
                            }
                            else
                            {
                                for (y = 0; y < PreviewSize; ++y)
                                {
                                    Text.SetPixel(x, y, Color.white);

                                    if (y == (int)(MidSize-(-Dens * MidSize)))
                                        Text.SetPixel(x, y, Color.black);
                                }
                            }

                            Text.SetPixel(x, MidSize, Color.red);

                        }
                    }

                    Text.Apply();
                }
            }
        }
        finally
        {

            foreach (var n in Noises.Windows)
            {
                n.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
                {
                    if (Id == Window.UniqueId)
                        GetPreview(n, true, false);
                    return false;
                });
            }
        }
    }

    #endregion

    #region Zoom

    private const float kZoomMin = 0.1f;
    private const float kZoomMax = 10.0f;

    private Rect _zoomArea = new Rect(0.0f, 0, 600.0f, 300.0f - 100.0f);
    private float RealZoom = 100f;

    private float _zoom
    {
        get
        {
            return RealZoom / 100f;
        }
    }
    private Vector2 _zoomCoordsOrigin = Vector2.zero;

    private Vector2 ConvertScreenCoordsToZoomCoords(Vector2 screenCoords)
    {
        return (screenCoords - _zoomArea.TopLeft()) / _zoom + _zoomCoordsOrigin;
    }

    private void HandleEvents()
    {
        // Allow adjusting the zoom with the mouse wheel as well. In this case, use the mouse coordinates
        // as the zoom center instead of the top left corner of the zoom area. This is achieved by
        // maintaining an origin that is used as offset when drawing any GUI elements in the zoom area.
        if (Event.current.type == EventType.ScrollWheel)
        {
            Vector2 screenCoordsMousePos = Event.current.mousePosition;
            Vector2 delta = Event.current.delta;
            Vector2 zoomCoordsMousePos = ConvertScreenCoordsToZoomCoords(screenCoordsMousePos);

            float zoomDelta = delta.y < 0 ? -0.04f : 0.04f;
            float oldZoom = _zoom;
            RealZoom += zoomDelta * 100f;
            RealZoom = Mathf.Clamp(_zoom, kZoomMin, kZoomMax) * 100f;
            RealZoom = (int)RealZoom;
            if (RealZoom >= 99f && RealZoom < 101f)
                RealZoom = 100f;

            _zoomCoordsOrigin -= (zoomCoordsMousePos - _zoomCoordsOrigin) - (oldZoom / _zoom) * (zoomCoordsMousePos - _zoomCoordsOrigin);

            Event.current.Use();
        }

        // Allow moving the zoom area's origin by dragging with the middle mouse button or dragging
        // with the left mouse button with Alt pressed.
        if (Event.current.type == EventType.MouseDrag &&
            (Event.current.button == 0 && Event.current.modifiers == EventModifiers.Alt) ||
            Event.current.button == 2)
        {
            Vector2 delta = Event.current.delta;
            delta /= _zoom;
            _zoomCoordsOrigin += delta;

            Event.current.Use();
        }
    }

    #endregion

    public GUIStyle ModuleStyle;
    public GUIStyle VoxelStyle;

    public void OnGUI()
    {
        if (Application.isPlaying)
            return;

        if (SharedContainer == null)
        {
            AdvancedHeightMap HeightMap = FindObjectOfType<AdvancedHeightMap>();
            if (HeightMap == null)
                SharedContainer = FindObjectOfType<BiomeNoisesContainer>();
            else
                SharedContainer = HeightMap.Container;

            if (SharedContainer == null)
                return;

            if (SharedContainer.Noises != null && SharedContainer.Noises.Windows != null)
            {
                foreach (var N in SharedContainer.Noises.Windows)
                    SharedContainer.Noises.InitNoiseValues(N);
            }
        }

        if (SharedContainer == null)
            return;

        if(VoxelStyle == null)
        {
            ModuleStyle = new GUIStyle(GUI.skin.box);
            VoxelStyle = new GUIStyle(GUI.skin.textArea);

            ModuleStyle.alignment = TextAnchor.UpperCenter;
            VoxelStyle.alignment = TextAnchor.UpperCenter;
        }

        if (HasChanged)
        {
            EditorApplication.MarkSceneDirty();
            EditorUtility.SetDirty(SharedContainer);
            HasChanged = true;
        }

        EditorUtility.SetDirty(SharedContainer);
        if (SharedContainer.Noises == null)
            SharedContainer.Noises = new HeightNoisesContainer();

        Noises = SharedContainer.Noises;

        _zoomArea.x = 0;
        _zoomArea.y = 0;
        _zoomArea.width = Screen.width;
        _zoomArea.height = Screen.height;
        Biomes.Clear();
        SharedContainer.Noises.GetBiomes(Biomes);

        HandleEvents();

        for (int i = 0; i < Noises.Windows.Count;++i)
        {
            Noises.Windows[i].IsLinked = false;
        }

        for (int i = 0; i < Noises.Windows.Count; ++i)
        {
            if (Noises.Windows[i].HasInput)
            {
                Noises.Windows[i].GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
                {
                    NoiseWindow TargetWindow = Noises.GetNoise(Id);
                    if (TargetWindow != null)
                    {
                        TargetWindow.IsLinked = true;
                    }

                    return false;
                });
            }

            if(!Noises.Windows[i].HasOuput)
                Noises.Windows[i].IsLinked = true;

            DrawCurves(Noises.Windows[i]);
        }

        EditorZoomArea.Begin(_zoom, _zoomArea);

        GUILayout.BeginArea(new Rect(_zoomCoordsOrigin.x, _zoomCoordsOrigin.y, 10000, 10000));

        BeginWindows();

        NoiseWindow Window = null;
        Rect NewPos = new Rect();
        for (int i = 0; i < Noises.Windows.Count; ++i)
        {
            Window = Noises.Windows[i];
            if (!Window.IsViewable)
                continue;

            DrawToggles(Window);
            NewPos = GUILayout.Window(Window.UniqueId, Window.Position, DrawWindow, Window.Name, 
                BiomeNoisesContainer.GetOutputType(Window.Name) == NoiseValue.ValueTypes.MODULE ? ModuleStyle : VoxelStyle);

            if (NewPos.x < NewPos.width / 2)
                NewPos.x = NewPos.width / 2;
            if (NewPos.y <= NewPos.height / 2)
                NewPos.y = NewPos.height / 2;

            NewPos.x -= NewPos.x % 20;
            NewPos.y -= NewPos.y % 20;

            MoveWindow(Window, NewPos.x - Window.Position.x, NewPos.y - Window.Position.y);
            Window.Position = NewPos;
        }

        EndWindows();

        GUILayout.EndArea();

        EditorZoomArea.End();


        DrawToolBar();

        if (GUI.changed)
            HasChanged = true;
    }

    public void MoveWindow(NoiseWindow Window, float x, float y)
    {
        if ((int)x == 0 && (int)y == 0)
            return;
        Window.Position.x += x;
        Window.Position.y += y;

        if (Window.HasInput)
        {
            Window.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
            {
                NoiseWindow Sub = Noises.GetNoise(Id);
                if (Sub != null && GetInputCout(Id) == 1)
                    MoveWindow(Sub, x, y);
                return false;
            });
        }
    }

    public int GetInputCout(int WindowId)
    {
        int Count = 0;
        for (int i = 0; i < Noises.Windows.Count; ++i)
        {
            if (Noises.Windows[i].HasInput)
            {
                Noises.Windows[i].GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
                {
                    if (Id == WindowId)
                        ++Count;
                    return false;
                });

            }
        }

        return Count;
    }
        

    public void ViewWindow(NoiseWindow Window,bool Value)
    {
        Window.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
            {
                NoiseWindow TargetWindow = Noises.GetNoise(Id);

                if (TargetWindow != null)
                {
                    TargetWindow.IsViewable = Value;
                    ViewWindow(TargetWindow, Value);
                }
                return false;

            });
    }

    public Texture2D UpdateTextures(NoiseWindow Window)
    {
        Texture2D Text = null;
        if (Window.IsPreview)
            Text = GetPreview(Window, true, true);

        Window.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
        {
            NoiseWindow TargetWindow = Noises.GetNoise(Id);
            if (TargetWindow != null)
            {
                UpdateTextures(TargetWindow);
            }

            return false;
        });

        return Text;
    }

    public void DrawToolBar()
    {
        Rect Area = new Rect(0, 0, Screen.width, 150 * 5);
        GUILayout.BeginArea(Area);

        GUILayout.BeginVertical("box");

        CurrentCategory = GUILayout.Toolbar(CurrentCategory, BiomeNoisesContainer.Categories, GUILayout.Width(BiomeNoisesContainer.Categories.Length * 120));

        string Cat = BiomeNoisesContainer.Categories[CurrentCategory];
        int Count = 0;

        GUILayout.BeginVertical("box", GUILayout.Width(150 * 5));
        GUILayout.BeginHorizontal();
        for (int i = 0; i < BiomeNoisesContainer.Constructors.Length; ++i)
        {
            if (BiomeNoisesContainer.Constructors[i].Category == Cat)
            {
                if (GUILayout.Button(BiomeNoisesContainer.Constructors[i].Name, GUILayout.Width(150)))
                {
                    NoiseWindow Window = Noises.CreateNoise(i);
                    Vector2 P = ConvertScreenCoordsToZoomCoords(new Vector2(Screen.width / 2 - _zoomCoordsOrigin.x, Screen.height / 2 - _zoomCoordsOrigin.y));
                    Window.Position.x = P.x;
                    Window.Position.y = P.y;
                }

                ++Count;

                if (Count != 0 && Count % 5 == 0)
                {
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                }
            }
        }
        GUILayout.EndHorizontal();
        GUILayout.Label("Current Container:" + CurrentContainer + ",MouseWheel:Zoom=" + _zoom + ",Alt+LeftMouse:Move");
        GUILayout.BeginHorizontal();
        GUILayout.Label("Biomes:", GUILayout.Width(80));
        foreach (string Biome in Biomes)
            GUILayout.Box(Biome, GUILayout.Width(100));
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        PreviewTerrain = EditorGUILayout.ObjectField("Preview Terrain", PreviewTerrain, typeof(Terrain)) as Terrain;
        if (GUILayout.Button("Apply", GUILayout.Width(80)))
        {
            UpdateTerrain(PreviewTerrain);
        }
        GUILayout.Label("Threads", GUILayout.Width(80));
        ThreadCount = EditorGUILayout.IntField(ThreadCount, GUILayout.Width(80));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        HeightMapFile = EditorGUILayout.ObjectField(HeightMapFile, typeof(TextAsset)) as TextAsset;
        if (GUILayout.Button("Export", GUILayout.Width(80)))
        {
            string Path = EditorUtility.GetAssetPath(HeightMapFile);

            TerrainEngine.XmlMgr.SaveXMLFile(Path, Noises);
            EditorUtility.SetDirty(HeightMapFile);
        }
        if (GUILayout.Button("Import", GUILayout.Width(80)))
        {
            string Path = EditorUtility.GetAssetPath(HeightMapFile);

            HeightNoisesContainer NewContainer = TerrainEngine.XmlMgr.GetXMLFile<HeightNoisesContainer>(Path);
            SharedContainer.Noises = NewContainer;
            Previews.Clear();
        }

        if (GUILayout.Button("Add", GUILayout.Width(80)))
        {
            string Path = EditorUtility.GetAssetPath(HeightMapFile);

            HeightNoisesContainer NewContainer = TerrainEngine.XmlMgr.GetXMLFile<HeightNoisesContainer>(Path);
            SharedContainer.Noises.AddContainer(NewContainer);
            Previews.Clear();
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        GUILayout.EndArea();
    }

    #region TerrainPreview

    public class TerrainThread
    {
        public int StartX, Count;
        public float[,] data;
        public Thread Tr;
        public IModule Module;
        public bool IsDone = false;

        public float Min = float.MaxValue, Max = float.MinValue;

        public int SizeY;

        public TerrainThread()
        {
        }

        public void Start()
        {
            Tr = new Thread(UpdateData);
            Tr.Start();
        }

        public void UpdateData()
        {
            try
            {
                //data = new float[Count, SizeY];
                float H = 0;
                for (int x = 0; x < Count; ++x)
                {
                    for (int y = 0; y < SizeY; ++y)
                    {
                        H = data[x + StartX, y] = (float)Module.GetValue(x + StartX, y);
                        if (H < Min)
                            Min = H;
                        if (H > Max)
                            Max = H;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }

            IsDone = true;
        }
    }

    public List<TerrainThread> Threads = new List<TerrainThread>();

    public void UpdateTerrain(Terrain PreviewTerrain)
    {
        Threads.Clear();

        System.Diagnostics.Stopwatch Watch = new System.Diagnostics.Stopwatch();
        Watch.Start();

        int TotalSize = PreviewTerrain.terrainData.heightmapWidth;
        int PartPerThread = TotalSize / ThreadCount;
        int Start = 0;
        int Count = 0;

        float[,] data = PreviewTerrain.terrainData.GetHeights(0, 0, PreviewTerrain.terrainData.heightmapWidth, PreviewTerrain.terrainData.heightmapWidth);


        for (int i = 0; i < ThreadCount; ++i)
        {
            SharedContainer.Noises.GenerateModules();
            IModule Module = Noises.GetMainModule();
            if (Module == null)
            {
                Debug.LogError("No HeightMapOutput");
                return;
            }

            Count = Mathf.Min(PartPerThread, TotalSize);
            TotalSize -= Count;
            if (TotalSize < PartPerThread)
            {
                Count += TotalSize;
                TotalSize = 0;
            }

            TerrainThread Tr = new TerrainThread();
            Tr.Module = Module;
            Tr.SizeY = PreviewTerrain.terrainData.heightmapWidth;
            Tr.StartX = Start;
            Tr.Count = Count;
            Tr.IsDone = false;
            Tr.data = data;
            Start += Count;
            Tr.Start();
            Threads.Add(Tr);
        }

        float Min = float.MaxValue, Max = float.MinValue;

        foreach (var T in Threads)
        {
            T.Tr.Join();

            if (T.Min < Min)
                Min = T.Min;
            if (T.Max > Max)
                Max = T.Max;
        }

        int Size = PreviewTerrain.terrainData.heightmapWidth;
        float H = 0;
        for (int x = 0; x < Size; ++x)
        {
            for (int y = 0; y < Size; ++y)
            {
                data[x, y] = (data[x, y] - Min) / (Max - Min);
            }
        }

        PreviewTerrain.terrainData.SetHeights(0, 0, data);
        Watch.Stop();
        Debug.Log("Elapsed:" + Watch.ElapsedMilliseconds);
    }

    #endregion

    static public readonly Color[] CurveColors = new Color[]
    {
        Color.cyan,
        Color.blue,
        Color.magenta,
        Color.yellow,
        Color.red,
    };

    public void DrawToggles(NoiseWindow Window)
    {
        Rect R = new Rect();

        if (Event.current.type == EventType.mouseDown && Event.current.button == 1)
        {
            SelectedValue = -1;
            SelectedWindow = -1;
            Repaint();
        }

        int ModuleCount = 0;
        if (Window.IsViewable && Window.HasInput)
        {
            bool HasNonViewable = false;
            Window.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
            {
                if (SelectedWindow == Window.UniqueId && SelectedValue == Index)
                    GUI.color = Color.cyan;

                R.x = Window.Position.x + Window.Position.width-2;
                R.y = Window.Position.y + ModuleCount * 15 + 10;
                R.width = 0;
                R.height = 0;

                NoiseWindow TargetWindow = Noises.GetNoise(Id);

                R.width = 20;
                R.height = 15;
                if (GUI.Toggle(R, false, new GUIContent("", Name)))
                {
                    SelectWindow(Window, Index);
                }

                if (TargetWindow != null && !TargetWindow.IsViewable)
                    HasNonViewable = true;

                ++ModuleCount;
                GUI.color = Color.white;
                return false;
            });

            if (ModuleCount != 0)
            {
                if (HasNonViewable)
                    GUI.color = Color.red;
                else
                    GUI.color = Color.green;

                R.y += 20;
                bool Result = GUI.Toggle(R, HasNonViewable, new GUIContent("", "Disable Childs"));
                GUI.color = Color.white;
                if (Result != HasNonViewable)
                {
                    ViewWindow(Window, !Result);
                }
            }
        }

        if (Window.IsViewable && Window.HasOuput)
        {
            if (SelectedWindow == Window.UniqueId && SelectedValue == -1)
                GUI.color = Color.cyan;
            else if (Window.IsBiome)
                GUI.color = Color.green;

            if (GUI.Toggle(new Rect(Window.Position.x - 15, Window.Position.y + Window.Position.height / 2 - 10, 20, 20), false, new GUIContent("", "Output")))
            {
                SelectWindow(Window, -1);
            }
        }

        GUI.color = Color.white;
    }

    public void DrawCurves(NoiseWindow Window)
    {
        if (Window.IsViewable && Window.HasInput)
        {
            Rect Start, End;

            int ModuleCount = 0;
            Color Col = Color.cyan;

            Window.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
            {
                Start = new Rect(Window.Position.x + Window.Position.width, Window.Position.y + ModuleCount * 15 + 10 + 5, 0, 0);

                NoiseWindow TargetWindow = Noises.GetNoise(Id);
                if (TargetWindow != null)
                {
                    End = new Rect(TargetWindow.Position.x - 15, TargetWindow.Position.y + TargetWindow.Position.height / 2 - 5, 0, 0);
                    curveFromTo(Start, End, CurveColors[ModuleCount % CurveColors.Length], -_zoomCoordsOrigin, _zoom);
                }

                ++ModuleCount;
                return false;
            });
        }
    }

    public void DrawWindow(int Id)
    {
        NoiseWindow Window = Noises.GetNoise(Id);

        if (!Window.IsLinked)
            GUI.color = Color.red; 
        else
            GUI.color = Color.white;

        if (GUI.Button(new Rect(0, 0, 18, 15), "X"))
        {
            RemoveInput(Window.UniqueId);
            SelectWindow(null, -1);
            Noises.Windows.Remove(Window);
            return;
        }


        GUI.color = Color.white;

        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.EndHorizontal();

        for (int i = 0; i < Window.Values.Count; ++i)
        {
            if (Window.Values[i].Name == "BiomeName")
            {
                DrawValue(Window, Window.Values[i], i);
                break;
            }
        }

        for (int i = 0; i < Window.Values.Count; ++i)
        {
            if (Window.Values[i].Name != "BiomeName")
            {
                DrawValue(Window, Window.Values[i], i);
            }
        }

        if (BiomeNoisesContainer.GetOutputType(Window.Name) == NoiseValue.ValueTypes.MODULE)
        {
            bool IsPreview = EditorGUILayout.Foldout(Window.IsPreview, "Preview");
            bool OldPreview = Window.IsPreview;
            if (IsPreview != OldPreview)
            {
                Window.IsPreview = IsPreview;
                Window.Position.height = 0;
                GUI.FocusWindow(Id);
            }

            Texture2D Text = null;

            if (GUI.changed)
            {
                Text = UpdateTextures(Window);
            }

            if (IsPreview)
            {
                if (Text == null)
                    Text = GetPreview(Window, !OldPreview);

                if (Text != null)
                {
                    GUILayout.Label(Text, GUILayout.Width(PreviewSize), GUILayout.Height(PreviewSize));
                    PreviewZoom = EditorGUILayout.Slider((float)PreviewZoom, 0.001f, 100f);

                    GUILayout.BeginVertical();
                    bool NewValue = GUILayout.Toggle(IsGraphPreview, "GraphPreview");

                    if (NewValue != IsGraphPreview)
                    {
                        Window.Position.height = 0;
                        IsGraphPreview = NewValue;
                    }
                    if (IsGraphPreview)
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("X", GUILayout.Width(20));
                        GraphXOffset = GUILayout.HorizontalSlider(GraphXOffset, 0, 3000);
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Y",GUILayout.Width(20));
                        GraphYOffset = GUILayout.HorizontalSlider(GraphYOffset, 0, 3000);
                        GUILayout.EndHorizontal();
                    }
                    GUILayout.EndVertical();
                }
            }

            if (GUI.changed)
            {
                Text = UpdateTextures(Window);
                HasChanged = true;
            }

            if (OldPreview != IsPreview)
            {
                if (IsPreview)
                    Window.Position.height += PreviewSize;
                else
                    Window.Position.height -= PreviewSize+20;
            }
        }

        GUI.DragWindow();

        if (GUI.changed)
            HasChanged = true;
    }

    public void DrawValue(NoiseWindow Window, NoiseValue Value, int i)
    {
        if (SelectedWindow == Window.UniqueId && SelectedValue == i)
            GUI.color = Color.cyan;

        switch (Value.ValueType)
        {
            case NoiseValue.ValueTypes.MODULE:
                break;
            case NoiseValue.ValueTypes.INT:
                {
                    int Val = Value.IntValue;
                    int NewVal = 0;

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(Value.Name, GUILayout.Width(80));
                    NewVal = EditorGUILayout.IntField(Val, GUILayout.Width(80));
                    if(NewVal != Val)
                        Value.IntValue = NewVal;
                    EditorGUILayout.EndHorizontal();
                } break;
            case NoiseValue.ValueTypes.FLOAT:
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(Value.Name, GUILayout.Width(80));
                    Value.FloatValue = EditorGUILayout.FloatField(Value.FloatValue, GUILayout.Width(80));
                    EditorGUILayout.EndHorizontal();
                } break;
            case NoiseValue.ValueTypes.DOUBLE:
                {
                    double Val = Value.DoubleValue;
                    double NewVal = 0;

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(Value.Name, GUILayout.Width(80));
                    NewVal = EditorGUILayout.DoubleField(Val, GUILayout.Width(80));
                    if(NewVal != Val)
                        Value.DoubleValue = NewVal;
                    EditorGUILayout.EndHorizontal();
                } break;
            case NoiseValue.ValueTypes.STRING:
                {
                    if (Value.Name == "BiomeName")
                    {
                        if (Biomes.Count == 0)
                            GUILayout.Label("No Biome");
                        else
                        {
                            int Index = 0;
                            for (int j = 0; j < Biomes.Count; ++j)
                            {
                                if (Biomes[j] == Value.Value)
                                {
                                    Index = j;
                                    break;
                                }
                            }

                            Index = GUILayout.SelectionGrid(Index, Biomes.ToArray(), 1);
                            Value.Value = Biomes[Index];
                        }
                    }
                    else
                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label(Value.Name, GUILayout.Width(80));
                        Value.Value = EditorGUILayout.TextField(Value.Value, GUILayout.Width(80));
                        EditorGUILayout.EndHorizontal();
                    }
                } break;
            case NoiseValue.ValueTypes.BOOL:
                Value.BoolValue = EditorGUILayout.Toggle(Value.Name, Value.BoolValue, GUILayout.Width(160)); break;
            case NoiseValue.ValueTypes.ENUM:
                {
                    int Val = Value.IntValue;
                    int NewVal = 0;

                    GUILayout.Label(Value.Name);
                    NewVal = GUILayout.Toolbar(Value.IntValue, Value.EnumValues, GUILayout.Width(160));
                    if(NewVal != Val)
                        Value.IntValue = NewVal;
                }
                break;
            case NoiseValue.ValueTypes.CURVE:
                {
                    EditorGUILayout.LabelField("Curves", GUILayout.Width(60));

                    CurveControlPoint C;
                    for (int j = 0; j < Value.Curves.Count; ++j)
                    {
                        C = Value.Curves[j];
                        GUILayout.BeginHorizontal();
                        C.Input = EditorGUILayout.DoubleField(C.Input);
                        C.Output = EditorGUILayout.DoubleField(C.Output);
                        Value.Curves[j] = C;

                        if (GUILayout.Button("X", GUILayout.Width(18), GUILayout.Width(18)))
                            Value.Curves.RemoveAt(j);
                        GUILayout.EndHorizontal();
                    }

                    if (GUILayout.Button("Add"))
                    {
                        if (Value.Curves.Count == 0)
                        {
                            Value.Curves.Add(new CurveControlPoint());
                            Value.Curves.Add(new CurveControlPoint());
                            Value.Curves.Add(new CurveControlPoint());
                        }

                        Value.Curves.Add(new CurveControlPoint());
                    }
                }
                break;
            case NoiseValue.ValueTypes.ANIMATIONCURVE:
                {
                    if (Value.AnimCurves == null)
                        Value.AnimCurves = new AnimationCurve();

                    if (Value.AnimCurves.length == 0)
                    {
                        Value.AnimCurves.AddKey(new Keyframe(-1f, -1f));
                        Value.AnimCurves.AddKey(new Keyframe(1f, 1f));
                    }
                    Value.AnimCurves = EditorGUILayout.CurveField(Value.AnimCurves);
                }
                break;
            case NoiseValue.ValueTypes.DOUBLELIST:
                {
                    EditorGUILayout.LabelField("Points", GUILayout.Width(60));

                    for (int j = 0; j < Value.Doubles.Count; ++j)
                    {
                        GUILayout.BeginHorizontal();

                        Value.Doubles[j] = EditorGUILayout.DoubleField(Value.Doubles[j]);

                        if (GUILayout.Button("X", GUILayout.Width(18), GUILayout.Width(18)))
                            Value.Doubles.RemoveAt(j);

                        GUILayout.EndHorizontal();
                    }

                    if (GUILayout.Button("Add"))
                    {
                        if (Value.Doubles.Count == 0)
                        {
                            Value.Doubles.Add(0);
                        }

                        Value.Doubles.Add(0);
                    }


                }
                break;
            case NoiseValue.ValueTypes.NOISENAME:
                {
                    EditorGUILayout.LabelField(Window.Name == "BiomeSelecter" ? "Biomes" : "Names", GUILayout.Width(60));
                    NoiseNameValues NN;
                    for (int j = 0; j < Value.Names.Count; ++j)
                    {
                        NN = Value.Names[j];

                        GUILayout.BeginHorizontal();
                        NN.Name = EditorGUILayout.TextField(NN.Name, GUILayout.Width(50));

                        if (Window.Name != "BiomeSelecter")
                        {
                            NN.MinNoise = EditorGUILayout.FloatField(NN.MinNoise, GUILayout.Width(35));
                            NN.MaxNoise = EditorGUILayout.FloatField(NN.MaxNoise, GUILayout.Width(35));
                            NN.Alpha = EditorGUILayout.FloatField(NN.Alpha, GUILayout.Width(35));
                        }
                        Value.Names[j] = NN;

                        if (GUILayout.Button("X", GUILayout.Width(18), GUILayout.Width(18)))
                            Value.Names.RemoveAt(j);

                        GUILayout.EndHorizontal();
                    }

                    if (GUILayout.Button("Add"))
                    {
                        Value.Names.Add(new NoiseNameValues() { Alpha = 0.5f });
                    }

                } break;
        }

        GUI.color = Color.white;
    }

    #endregion

    #region Functions

    public void SelectWindow(NoiseWindow Window, int ValueId)
    {
        if (Window == null)
        {
            SelectedValue = SelectedWindow = -1;
            return;
        }

        if (SelectedWindow == Window.UniqueId && SelectedValue == ValueId)
        {
            if (ValueId != -1)
            {
                Window.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
                {
                    if (Index != ValueId)
                        return false;

                    Id = 0;
                    return true;
                });
            }

            SelectedValue = SelectedWindow = -1;
            return;
        }

        if (Window.UniqueId != SelectedWindow)
        {
            if ((IsInput(ValueId) && IsInput(SelectedValue)) || (!IsInput(ValueId) && !IsInput(SelectedValue)))
            {
                SelectedWindow = Window.UniqueId;
                SelectedValue = ValueId;
                return;
            }

            NoiseWindow From = null;
            NoiseWindow To = null;
            int FromId = -1;

            if (ValueId == -1)
            {
                From = Noises.GetNoise(SelectedWindow);
                FromId = SelectedValue;
                To = Window;
            }
            else
            {
                From = Window;
                FromId = ValueId;
                To = Noises.GetNoise(SelectedWindow);
            }

            if (To != null && From != null)
            {
                From.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
                {
                    if (Index != FromId)
                        return false;

                    if (VType != BiomeNoisesContainer.GetOutputType(To.Name))
                    {
                        Debug.LogError("Can not select 2 different types of input/ouput, From:" + VType + "!=To:" + BiomeNoisesContainer.GetOutputType(To.Name));
                        return false;
                    }

                    Id = To.UniqueId;
                    SelectedWindow = SelectedValue = -1;
                    return true;
                });

                return;
            }
        }

        SelectedWindow = Window.UniqueId;
        SelectedValue = ValueId;
    }

    public bool IsInput(int ValueId)
    {
        return ValueId == -1;
    }

    public void RemoveInput(int UniqueId)
    {
        foreach (var w in Noises.Windows)
        {
            w.GetInputs((NoiseValue.ValueTypes VType, string Name, ref int Id, int Index) =>
            {
                if (Id == UniqueId)
                {
                    Id = -1;
                    return true;
                }

                return false;
            });
        }
    }

    #endregion
}
