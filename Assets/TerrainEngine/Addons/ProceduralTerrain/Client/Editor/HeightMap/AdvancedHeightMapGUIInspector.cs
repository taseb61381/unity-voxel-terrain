﻿using TerrainEngine;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(AdvancedHeightMap))]
public class AdvancedHeightMapGUIInspector : Editor
{
    public Terrain PreviewTerrain;
    public Vector3 PreviewOffset;
    public Transform PreviewObject;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Separator();

        GUILayout.Label("Preview Terrain : Select a terrain to preview");
        PreviewTerrain = EditorGUILayout.ObjectField(PreviewTerrain, typeof(Terrain), true) as Terrain;
        if (PreviewTerrain != null)
        {
            PreviewOffset = EditorGUILayout.Vector3Field("Preview Offset", PreviewOffset);
            PreviewObject = EditorGUILayout.ObjectField("Preview Object", PreviewObject, typeof(Transform), true) as Transform;
            if (GUILayout.Button("Update Preview"))
            {
                AdvancedHeightMap HeightMap = (AdvancedHeightMap)this.target;
                TerrainManager Manager = HeightMap.transform.parent.GetComponent<TerrainManager>();
                HeightMapGenerator Gen = HeightMap.GetGenerator() as HeightMapGenerator;
                if (Gen != null)
                {
                    LibNoise.IModule Noises = null;
                    VoxelTypeOuput Output = null;

                    Gen.Information = Manager.Informations;
                    Gen.Palette = Manager.Palette.GetVoxels();
                    Gen.Init();
                    Gen.WorldOffset += PreviewOffset;
                    if (PreviewObject != null)
                        Gen.WorldOffset += PreviewObject.position;

                    Gen.GetMainNoises(ref Noises, ref Output);
                    TerrainData TData = PreviewTerrain.terrainData;
                    float[,] Height = TData.GetHeights(0,0,TData.heightmapWidth,TData.heightmapHeight);

                    int x, z;
                    float Noise = 0;
                    float min = 999, max = -999;
                    for (x = 0; x < TData.heightmapWidth; ++x)
                    {
                        for (z = 0; z < TData.heightmapWidth; ++z)
                        {
                            Noise = (float)Noises.GetValue(x, z) + Gen.WorldOffset.y;
                            Height[x, z] = Noise;
                            if (Noise < min)
                                min = Noise;
                            if (Noise > max)
                                max = Noise;
                        }
                    }

                    for (x = 0; x < TData.heightmapWidth; ++x)
                    {
                        for (z = 0; z < TData.heightmapWidth; ++z)
                        {
                            Height[x, z] = (Height[x, z] + min) / (min + max);
                        }
                    }

                    TData.SetHeights(0, 0, Height);
                    PreviewTerrain.terrainData = TData;
                }
            }
        }
    }
}