﻿using LibNoise;
using System.Collections.Generic;
using System.Linq;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class AdvancedHeightMapGUI : EditorWindow
{
    [MenuItem("TerrainEngine/Processors/Procedural/Old/Advanced Height Editor")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(AdvancedHeightMapGUI));
    }

    public class NoiseWindow
    {
        public string EditingName = "";

        public NoiseWindow(int Id, int VariableId, NoiseWindow ParentWindow, NoisePreset Noise)
        {
            this.Id = Id;
            this.VariableId = VariableId;
            this.Noise = Noise;
            this.Position = new Rect();
            this.ParentWindow = ParentWindow;

            if (ParentWindow != null)
                Parent = ParentWindow.Noise;

            if (Noise != null)
                this.EditingName = Noise.Name;
        }

        public void RegenerateID()
        {
            Id = AdvancedHeightMapGUI.GenerateID(Noise.Name, VariableId.ToString(), ParentWindow != null ? ParentWindow.Noise.Name.ToString() : "");
        }

        public int Id;
        public int VariableId;
        public string PresetName;
        public NoiseWindow ParentWindow;
        public NoisePreset Parent;
        public NoisePreset Noise;
        public Rect Position;
        public Texture2D PreviewTexture;
        public Texture2D BigPreviewTexture;

        public bool IsValid()
        {
            if (Parent == null)
                return true;

            switch (VariableId)
            {
                case 0:
                    if (Parent.PresetA != Noise.Name)
                        return false;
                    break;
                case 1:
                    if (Parent.PresetB != Noise.Name)
                        return false;
                    break;

                case 2:
                    if (Parent.PresetC != Noise.Name)
                        return false;

                    if (Parent.Computation != NoiseComputations.BLEND && Parent.Computation != NoiseComputations.SELECT && Parent.Computation != NoiseComputations.NOISESELECTER)
                        Parent.PresetC = "";

                    break;
                default:
                    int Id = VariableId - 3;
                    if (Id >= Parent.NameNoises.Count || Parent.NameNoises[Id].Name != Noise.Name)
                        return false;

                    return true;
            }

            return true;
        }
    }

    void curveFromTo(Rect wr, Rect wr2, Color color, Color shadow, Vector2 Offset)
    {
        wr.x -= Offset.x;
        wr.y -= Offset.y;

        wr2.x -= Offset.x;
        wr2.y -= Offset.y;

        Drawing.bezierLine(
            new Vector2(wr.x + wr.width, wr.y + 3 + wr.height / 2),
            new Vector2(wr.x + wr.width + Mathf.Abs(wr2.x - (wr.x + wr.width)) / 2, wr.y + 3 + wr.height / 2),
            new Vector2(wr2.x, wr2.y + 3 + wr2.height / 2),
            new Vector2(wr2.x - Mathf.Abs(wr2.x - (wr.x + wr.width)) / 2, wr2.y + 3 + wr2.height / 2), shadow, 5, true, 20);

        Drawing.bezierLine(
            new Vector2(wr.x + wr.width, wr.y + wr.height / 2),
            new Vector2(wr.x + wr.width + Mathf.Abs(wr2.x - (wr.x + wr.width)) / 2, wr.y + wr.height / 2),
            new Vector2(wr2.x, wr2.y + wr2.height / 2),
            new Vector2(wr2.x - Mathf.Abs(wr2.x - (wr.x + wr.width)) / 2, wr2.y + wr2.height / 2), color, 2, true, 20);
    }

    public AdvancedHeightMap Biome;
    public NoisesContainer Container;
    public NoiseWindow SelectedWindow;
    public int SelectedPresetValue;
    public bool First = true;

    public float MaxWidth = 0;
    public float MaxHeight = 0;
    public Vector2 GlobalScroll = new Vector2();
    public Vector2 PresetScroll = new Vector2();
    public Vector2 NoisesScroll = new Vector2();

    public Rect NoisesRect = new Rect(0, 80, 200, 200);
    public Rect PresetRect = new Rect(0, 280, 200, 200);
    public Rect PreviewRect = new Rect(0, 480, 270, 400);

    public Texture2D PreviewTexture;
    public Texture2D BigPreviewTexture;

    public Vector3 PreviewZoom = new Vector3(1, 1, 1);
    public bool MustUpdate = true;

    public Dictionary<int, NoiseWindow> Windows = new Dictionary<int, NoiseWindow>();

    public void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 150, 20), "Biome :");
        Biome = EditorGUI.ObjectField(new Rect(0, 20, 150, 20), Biome, typeof(AdvancedHeightMap), true) as AdvancedHeightMap;

        if (Biome == null)
        {
            Biome = FindObjectOfType<AdvancedHeightMap>();
            if(Biome == null)
                return;
        }

        if (Biome.Container == null)
        {
            Debug.LogError("No BiomeNoisesContainer on Container value");
            return;
        }

        EditorUtility.SetDirty(Biome);
        EditorUtility.SetDirty(Biome.Container);
        Container = Biome.Container.Container;

        if (First)
        {
            First = false;
            MustUpdate = true;
            Windows.Clear();
            if(position.x < 0 || position.y < 0)
                position = new Rect(0, 0, 1024, 1024);
        }

        /////////////////////////////// Select/Create main preset
        if (Container != null)
        {
            NoisePreset MainPreset = Container.GetPreset("Main");
            if (MainPreset == null)
                Container.NoisesPreset.Add(MainPreset = new NoisePreset("Main"));

            int i = Container.NoisesPreset.IndexOf(MainPreset);
            if (i != 0)
            {
                NoisePreset Other = Container.NoisesPreset[0];
                Container.NoisesPreset[0] = MainPreset;
                Container.NoisesPreset[i] = Other;
            }
            ////////////////////////////////

            GetWindowPosition(MainPreset, null, 0);
        }
        MaxWidth = Mathf.Max(PresetRect.x + PresetRect.width, MaxWidth);
        MaxHeight = Mathf.Max(PresetRect.y + PresetRect.height, MaxHeight);

        GlobalScroll = GUI.BeginScrollView(new Rect(0, 0, position.width, position.height), GlobalScroll, new Rect(0, 0, MaxWidth, MaxHeight));
        {
            BeginWindows();
            {
                NoisesRect = GUI.Window(1234, NoisesRect, DrawNoisesBox, "Noises");
                PresetRect = GUI.Window(1235, PresetRect, DrawPresetsBox, "Presets");
                PreviewRect = GUI.Window(1236, PreviewRect, DrawPreviewBox, "Preview");
                IModule Module = null;
                Rect NewPosition;

                foreach (NoiseWindow Window in Windows.Values)
                {
                    if (MustUpdate || Window.PreviewTexture == null)
                    {
                        if (Window.Noise != null)
                        {
                            Module = Window.Noise.GetModule(Container, Biome.Container.ExternalContainers);
                            if (Module != null)
                                UpdateTexture(40, ref Window.PreviewTexture,ref Module);
                        }
                    }

                    NewPosition = GUI.Window(Window.Id, Window.Position, DrawPresetWindow, Window.PresetName + Window.Noise.Name);
                    MoveChilds(Window, NewPosition.x - Window.Position.x, NewPosition.y - Window.Position.y);

                    Window.Position = NewPosition;
                    if (Window.Position.x + Window.Position.width > MaxWidth)
                        MaxWidth = Window.Position.x + Window.Position.width;

                    if (Window.Position.y + Window.Position.height > MaxHeight)
                        MaxHeight = Window.Position.y + Window.Position.height;
                }
            }
            EndWindows();
        }
        GUI.EndScrollView();


        MustUpdate = false;

        if (BigPreviewTexture != null)
        {
            GUI.color = Color.black;
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "", GUI.skin.window);
            GUI.color = Color.white;

            if (GUI.Button(new Rect(Screen.width - 30, 0, 30, 30), "X"))
                BigPreviewTexture = null;
            else
                GUI.DrawTexture(new Rect(Screen.width / 2 - 250, Screen.height / 2 - 250, 500, 500), BigPreviewTexture);
        }
    }

    void DrawPresetWindow(int id)
    {
        NoiseWindow Window = GetWindow(id);
        if (Window == null)
            return;

        if (!Window.IsValid())
        {
            Windows.Remove(Window.Id);
            return;
        }

        if (Window.Noise != null && GUI.Button(new Rect(0, 0, 20, 15), "B"))
        {
            LibNoise.IModule PNoise = Window.Noise.GetModule(Container, Biome.Container.ExternalContainers);
            UpdateTexture(400, ref Window.BigPreviewTexture, ref PNoise);
            this.BigPreviewTexture = Window.BigPreviewTexture;
        }

        if (Window.Noise != null && GUI.Button(new Rect(Window.Position.width - 40, 0, 20, 15), "U"))
        {
            LibNoise.IModule PNoise = Window.Noise.GetModule(Container, Biome.Container.ExternalContainers);
            UpdateTexture(40, ref Window.PreviewTexture, ref PNoise);
        }

        if (GUI.Button(new Rect(Window.Position.width - 20, 0, 20, 15), "X"))
        {
            if (Window.Parent != null)
            {
                Windows.Remove(Window.Id);

                foreach (KeyValuePair<int, NoiseWindow> Kp in Windows.ToArray())
                    if (Kp.Value.ParentWindow == Window)
                        Windows.Remove(Kp.Key);

                switch (Window.VariableId)
                {
                    case 0:
                        Window.Parent.PresetA = "";
                        break;
                    case 1:
                        Window.Parent.PresetB = "";
                        break;
                    case 2:
                        Window.Parent.PresetC = "";
                        break;
                    /*default:
                        Window.Parent.NameNoises.RemoveAt(Window.VariableId - 3);
                        break;*/
                };
                return;
            }
        }

        NoisePreset Noise = Window.Noise;
        NoisePreset A = GetPreset(Noise.PresetA);
        NoisePreset B = GetPreset(Noise.PresetB);
        NoisePreset C = GetPreset(Noise.PresetC);


        if(Window.PreviewTexture != null)
            GUI.DrawTexture(new Rect(10, 20, 40, 40), Window.PreviewTexture);

        Window.EditingName = EditorGUI.TextField(new Rect(50, 20, Window.Position.width - 60, 20), Window.EditingName);
        if (Window.Parent != null && Event.current.keyCode == KeyCode.Return && Window.EditingName != Noise.Name)
        {
            if (Window.EditingName.Length > 0 && GetPreset(Window.EditingName) == null)
            {
                Debug.Log("Name Updated : " + Window.EditingName);
                if (Window.Parent.PresetA == Noise.Name)
                    Window.Parent.PresetA = Window.EditingName;
                if (Window.Parent.PresetB == Noise.Name)
                    Window.Parent.PresetB = Window.EditingName;
                if (Window.Parent.PresetC == Noise.Name)
                    Window.Parent.PresetC = Window.EditingName;

                if (Window.VariableId < Window.Parent.NameNoises.Count && Window.VariableId >= 3)
                {
                    NoiseNameValues NN = Window.Parent.NameNoises[Window.VariableId - 3];
                    NN.Name = Window.EditingName;
                    Window.Parent.NameNoises[Window.VariableId - 3] = NN;
                }

                Windows.Remove(Window.Id);

                Noise.Name = Window.EditingName;
                Window.RegenerateID();
                Windows.Add(Window.Id, Window);

                foreach (KeyValuePair<int, NoiseWindow> Kp in Windows.ToArray())
                {
                    if (Kp.Value.ParentWindow == Window)
                    {
                        Windows.Remove(Kp.Value.Id);
                        Kp.Value.RegenerateID();
                        Windows.Add(Kp.Value.Id, Kp.Value);
                    }
                }

                Event.current.Use();
                Repaint();
            }
            else
            {
                Window.EditingName = Window.Noise.Name;
                Debug.LogError("Invalid name or name already used.");
            }
        }
        Noise.Computation = (NoiseComputations)EditorGUI.EnumPopup(new Rect(50, 40, Window.Position.width-60, 20), Noise.Computation);

        if (Noise.Computation == NoiseComputations.NOISE)
        {
            CheckSelectedColor(Window, 3);
            if (GUI.Button(new Rect(10, 60, Window.Position.width - 20, 20), Noise.NoiseName == "" ? "NoiseName" : Noise.NoiseName))
                SelectPreset(Window, 3);
        }
        else if (Noise.Computation == NoiseComputations.NOISES_CONTAINER)
        {
            Noise.NoiseScript_Container = EditorGUI.ObjectField(new Rect(10, 60, Window.Position.width - 20, 20), Noise.NoiseScript_Container, typeof(BiomeNoisesContainer)) as BiomeNoisesContainer;
            if (GUI.Button(new Rect(10, 80, Window.Position.width - 20, 20), "Update") && Noise.NoiseScript_Container != null)
            {
                Noise.ExternalIndex = Biome.Container.AddExternal(Noise.NoiseScript_Container.Container);
                Debug.Log(Noise.Name + " : Container Updated");
            }

            GUI.Label(new Rect(10, 100, Window.Position.width - 20, 20), "Main Name :");
            Noise.MainNoise_Container = GUI.TextField(new Rect(10, 120, Window.Position.width - 20, 20), Noise.MainNoise_Container);

            if (Noise.ExternalIndex != -1)
            {
                GUI.Label(new Rect(10, 140, Window.Position.width - 20, 20), "Presets :" + Biome.Container.ExternalContainers[Noise.ExternalIndex].NoisesPreset.Count);
                GUI.Label(new Rect(10, 160, Window.Position.width - 20, 20), "Noises :" + Biome.Container.ExternalContainers[Noise.ExternalIndex].Noises.Count);
            }
        }
        else
        {
            float y = 60;

            if (Noise.Computation != NoiseComputations.NOISESELECTER)
            {
                CheckSelectedColor(Window, 0);
                if (GUI.Button(new Rect(10, y, Window.Position.width - 20, 20), A == null ? "PresetA" : A.Name))
                    SelectPreset(Window, 0);
                y += 20;

                if (Noise.Computation != NoiseComputations.TERRACE)
                {
                    CheckSelectedColor(Window, 1);
                    if (GUI.Button(new Rect(10, y, Window.Position.width - 20, 20), B == null ? "PresetB" : B.Name))
                        SelectPreset(Window, 1);
                    y += 20;
                }
            }
            else
            {
                NoiseNameValues NoiseName;
                for (int i = 0; i < Window.Noise.NameNoises.Count; ++i)
                {
                    NoiseName = Window.Noise.NameNoises[i];
                    NoisePreset Preset = GetPreset(NoiseName.Name);

                    CheckSelectedColor(Window, i + 3);
                    if (GUI.Button(new Rect(10, y, Window.Position.width - 135, 20), Preset == null ? "New" + i : NoiseName.Name))
                        SelectPreset(Window, i + 3);

                    NoiseName.MinNoise = EditorGUI.FloatField(new Rect(Window.Position.width - 125, y, 50, 20), NoiseName.MinNoise);
                    NoiseName.MaxNoise = EditorGUI.FloatField(new Rect(Window.Position.width - 80, y, 50, 20), NoiseName.MaxNoise);

                    if (GUI.Button(new Rect(Window.Position.width - 30, y, 20, 20), "X"))
                    {
                        Window.Noise.NameNoises.RemoveAt(i);
                        return;
                    }
                    y += 20;
                }

                CheckSelectedColor(Window, -1);
                if (GUI.Button(new Rect(10, y, Window.Position.width - 20, 20), "Add"))
                    Window.Noise.NameNoises.Add(new NoiseNameValues());
                y += 20;

                CheckSelectedColor(Window, 2);
                if (GUI.Button(new Rect(10, y, Window.Position.width - 20, 20), C == null ? "Controller" : C.Name))
                    SelectPreset(Window, 2);
                y += 20;

                Window.Position.width = 250;
            }

            switch (Noise.Computation)
            {
                case NoiseComputations.BLEND:
                    CheckSelectedColor(Window, 2);
                    if (GUI.Button(new Rect(10, y, Window.Position.width - 20, 20), C == null ? "Controller" : C.Name))
                        SelectPreset(Window, 2);
                    y += 20;
                    break;
                case NoiseComputations.SELECT:
                    GUI.Label(new Rect(10, y, 100, 20), "EdgeFallof :");
                    Window.Noise.EdgeFalloff = EditorGUI.FloatField(new Rect(110, y, Window.Position.width - 115, 20), Window.Noise.EdgeFalloff);
                    y += 20;
                    Window.Noise.Bounds = EditorGUI.Vector2Field(new Rect(0, y, Window.Position.width - 5, 20), "Bounds :", Window.Noise.Bounds);
                    y += 35;
                    goto case NoiseComputations.BLEND;
                case NoiseComputations.TERRACE:
                    {
                        for (int i = 0; i < Window.Noise.ControlPoints.Count; ++i)
                        {
                            Window.Noise.ControlPoints[i] = EditorGUI.FloatField(new Rect(10, y, 80, 20), (float)Window.Noise.ControlPoints[i]);
                            if (GUI.Button(new Rect(110, y, 20, 20), "X"))
                                Window.Noise.ControlPoints.RemoveAt(i);
                            y += 20;
                        }

                        if (GUI.Button(new Rect(10, y, Window.Position.width-20, 20), "Add Control Point"))
                            Window.Noise.ControlPoints.Add(0);
                    } break;
            };
        }

        CheckSelectedColor(Window, -2);

        Window.Position.height = GetWindowHeight(Noise);
        GUI.DragWindow();
    }

    void DrawPresetsBox(int id)
    {
        PresetScroll = GUI.BeginScrollView(new Rect(0, 20, PresetRect.width, PresetRect.height), PresetScroll, new Rect(0, 20, PresetRect.width - 30, Container.NoisesPreset.Count * 20 + 40));
        {
            for (int i = 0; i < Container.NoisesPreset.Count; ++i)
            {
                if (GUI.Button(new Rect(5, 20 + i * 20, 120, 20), Container.NoisesPreset[i].Name))
                {
                    SetSelectedValue(Container.NoisesPreset[i]);
                }

                if (GUI.Button(new Rect(125, 20 + i * 20, 20, 20), "X"))
                {
                    Container.RemovePreset(Container.NoisesPreset[i]);
                    return;
                }
            }

            if (GUI.Button(new Rect(5, 20 + Container.NoisesPreset.Count * 20, 100, 20), "New"))
                Container.NoisesPreset.Add(new NoisePreset());
        }
        GUI.EndScrollView();
        GUI.DragWindow();
    }

    void DrawNoisesBox(int id)
    {
        NoisesScroll = GUI.BeginScrollView(new Rect(0, 20, NoisesRect.width, NoisesRect.height), NoisesScroll, new Rect(0, 20, NoisesRect.width, 40 + Container.Noises.Count * 20));
        for (int i = 0; i < Container.Noises.Count; ++i)
        {
            if (GUI.Button(new Rect(5, 20 + i * 20, 120, 20), Container.Noises[i].Name))
            {
                if (SelectedWindow != null)
                {
                    if (SelectedPresetValue == 3)
                        SelectedWindow.Noise.NoiseName = Container.Noises[i].Name;
                    else
                    {
                        foreach (NoisePreset Preset in Container.NoisesPreset)
                        {
                            if (Preset.Computation == NoiseComputations.NOISE && Preset.NoiseName == Container.Noises[i].Name)
                            {
                                SelectedWindow.Noise.NoiseName = Container.Noises[i].Name;
                                return;
                            }
                        }

                        NoisePreset NewPreset = new NoisePreset("Noise" + Container.Noises[i].Name);
                        NewPreset.Computation = NoiseComputations.NOISE;
                        NewPreset.NoiseName = Container.Noises[i].Name;
                        Container.NoisesPreset.Add(NewPreset);
                    }
                }
            }

            if (GUI.Button(new Rect(125, 20 + i * 20, 20, 20), "X"))
            {
                Container.RemoveNoise(Container.Noises[i]);
                return;
            }
        }

        if (GUI.Button(new Rect(5, 20 + Container.Noises.Count * 20, 100, 20), "New"))
            Container.Noises.Add(new BiomeNoise());

        GUI.EndScrollView();

        GUI.DragWindow();
    }

    private IModule Noise;
    void DrawPreviewBox(int id)
    {
        PreviewZoom = EditorGUI.Vector3Field(new Rect(5, 20, PreviewRect.width - 10, 40), "Zoom :", PreviewZoom);

        if (PreviewTexture == null || GUI.Button(new Rect(5, 60, PreviewRect.width - 10, 20), "Update"))
        {
            /*if (Noise == null)
                //Noise = ComplexPlanetGenerator.GetPlanetGenerator();*/
            Noise = Container.GetGlobalNoise(Biome.Container.ExternalContainers, 0);

            float WaterLevel = UpdateTexture(256, ref PreviewTexture,ref Noise);
            GenerateTexture(Noise, PreviewTexture, 0, 0, WaterLevel);

            MustUpdate = true;
        }

        if (PreviewTexture != null)
        {
            GUI.DrawTexture(new Rect(5, 90, 256, 256), PreviewTexture);
        }
        GUI.DragWindow();
    }

    static public void GenerateTexture(LibNoise.IModule Noise, Texture2D Texture, double OffsetX, double OffsetZ, float WaterLevel)
    {
        /*RidgeAxisPicking Ridge = new RidgeAxisPicking();
        Ridge.m_htmap = new RidgeAxisPicking.HeightMap();
        Ridge.m_htmap.Load(Noise, 0, 0, true, 256, 256, 5);
        Ridge.m_branchReduction = 16;
        Ridge.m_profileLength = 2;
        Ridge.m_htmap.WaterLevel -= Ridge.m_htmap.WaterLevel * 0.1f;
        Ridge.MaxHeight = -Ridge.m_htmap.WaterLevel;
        Ridge.Calculate();
        Ridge.DrawToTexture(Texture, new Color(100,0,256), 1);
        Texture.Apply();*/
    }

    static public int GenerateID(string Name, string VariableId, string ParentName)
    {
        return (Name + VariableId + ParentName).GetHashCode();
    }

    public NoiseWindow GetWindow(int Id)
    {
        NoiseWindow Window = null;
        Windows.TryGetValue(Id, out Window);
        return Window;
    }

    public NoiseWindow CreateWindow(NoisePreset Noise, NoiseWindow Parent, int VariableId)
    {
        int Id = GenerateID(Noise.Name, VariableId.ToString(), Parent != null ? Parent.Noise.Name.ToString() : "");

        NoiseWindow Window = null;
        if (!Windows.TryGetValue(Id, out Window))
        {
            Window = new NoiseWindow(Id, VariableId, Parent, Noise);
            Windows.Add(Id, Window);
        }

        return Window;
    }

    public void MoveChilds(NoiseWindow Window, float x, float y)
    {
        foreach (NoiseWindow Child in Windows.Values)
        {
            if (Child.ParentWindow != null && Child.ParentWindow == Window)
            {
                Child.Position.x += x;
                Child.Position.y += y;
                MoveChilds(Child, x, y);
            }
        }
    }

    public NoisePreset GetPreset(string Name)
    {
        return Container.GetPreset(Name);
    }

    public void SetSelectedValue(NoisePreset Preset)
    {
        if (SelectedWindow != null && SelectedPresetValue != -1)
        {
            if (SelectedWindow.Noise == Preset)
                return;

            string Name = Preset != null ? Preset.Name : "";

            if (SelectedPresetValue == 0)
                SelectedWindow.Noise.PresetA = Name;
            else if (SelectedPresetValue == 1)
                SelectedWindow.Noise.PresetB = Name;
            else if (SelectedPresetValue == 2)
                SelectedWindow.Noise.PresetC = Name;
            else
            {
                SelectedPresetValue -= 3;
                if (SelectedPresetValue < SelectedWindow.Noise.NameNoises.Count)
                {
                    NoiseNameValues NN = SelectedWindow.Noise.NameNoises[SelectedPresetValue];
                    NN.Name = Name;
                    SelectedWindow.Noise.NameNoises[SelectedPresetValue] = NN;
                }
            }
        }
    }

    public float GetWindowHeight(NoisePreset Noise)
    {
        if (Noise == null)
            return 0;

        if (Noise.Computation == NoiseComputations.NOISE)
            return 90;
        else if (Noise.Computation == NoiseComputations.BLEND)
            return 130;
        else if (Noise.Computation == NoiseComputations.SELECT)
            return 180;
        else if (Noise.Computation == NoiseComputations.NOISESELECTER)
        {
            return 110 + Noise.NameNoises.Count * 21;
        }
        else if (Noise.Computation == NoiseComputations.TERRACE)
        {
            return 110 + Noise.ControlPoints.Count * 20;
        }
        else if (Noise.Computation == NoiseComputations.NOISES_CONTAINER)
            return 180;
        else
            return 110;
    }

    public NoiseWindow GetWindowPosition(NoisePreset Noise, NoiseWindow Parent, int VariableId)
    {
        if (Noise == null)
            return null;

        NoiseWindow Window = CreateWindow(Noise, Parent != null ? Parent : null, VariableId);

        if (Window.Position.width == 0)
        {
            Window.Position = new Rect(300, 20, 170, 130);
            if (Parent != null)
            {
                Window.Position.x = Parent.Position.x + Parent.Position.width + 40;
                Window.Position.y = Parent.Position.y;

                if (Window.VariableId == 1)
                    Window.Position.y += GetWindowHeight(GetPreset(Parent.Noise.PresetA)) + 20;
                else if (Window.VariableId == 2)
                    Window.Position.y += GetWindowHeight(GetPreset(Parent.Noise.PresetA)) + 20 + GetWindowHeight(GetPreset(Parent.Noise.PresetB)) + 20;
                else
                {
                    for (int i = 0; i < Parent.Noise.NameNoises.Count; ++i)
                    {
                        if (i == VariableId-3)
                            break;

                        Window.Position.y += GetWindowHeight(GetPreset(Parent.Noise.NameNoises[i].Name)) + 20;
                    }
                }
            }
        }

        if (Noise.PresetA == Noise.Name)
            Noise.PresetA = "";

        if (Noise.PresetB == Noise.Name)
            Noise.PresetB = "";

        if (Noise.PresetC == Noise.Name)
            Noise.PresetC = "";

        NoiseWindow A = null;
        NoiseWindow B = null;
        NoiseWindow C = null;

        if (Noise.Computation != NoiseComputations.NOISE)
        {
            if (Noise.Computation != NoiseComputations.NOISESELECTER)
            {
                A = GetWindowPosition(GetPreset(Noise.PresetA), Window, 0);
                B = GetWindowPosition(GetPreset(Noise.PresetB), Window, 1);
                C = GetWindowPosition(GetPreset(Noise.PresetC), Window, 2);

                if (A != null)
                {
                    A.PresetName = "A:";
                    curveFromTo(Window.Position, A.Position, Color.white, Color.black, GlobalScroll);
                }

                if (B != null)
                {
                    B.PresetName = "B:";
                    curveFromTo(Window.Position, B.Position, Color.white, Color.black, GlobalScroll);
                }

                if (C != null)
                {
                    C.PresetName = "C:";
                    curveFromTo(Window.Position, C.Position, Color.blue, Color.blue, GlobalScroll);
                }
            }
            else
            {
                for (int i = 0; i < Noise.NameNoises.Count; ++i)
                {
                    A = GetWindowPosition(GetPreset(Noise.NameNoises[i].Name), Window, i + 3);
                    if (A != null)
                    {
                        A.PresetName = i + ":";
                        curveFromTo(Window.Position, A.Position, Color.green, Color.black, GlobalScroll);
                    }
                }

                C = GetWindowPosition(GetPreset(Noise.PresetC), Window, 2);

                if (C != null)
                {
                    C.PresetName = "C:";
                    curveFromTo(Window.Position, C.Position, Color.blue, Color.blue, GlobalScroll);
                }
            }
        }

        return Window;
    }

    // Return water level
    public float UpdateTexture(int Size, ref Texture2D Texture,ref LibNoise.IModule Noise, double OffsetX = 0, double OffsetZ = 0)
    {
        if (Noise == null)
        {
            return 0;
        }

        if (Texture == null)
            Texture = new Texture2D(Size, Size);

        IModule Result = new LibNoise.Modifiers.ScaleInput(Noise, PreviewZoom.x, PreviewZoom.y, PreviewZoom.z);
        Result = new LibNoise.Modifiers.TranslateInput(Result, Biome.WorldOffset.x, 0, Biome.WorldOffset.z);
        double MaxValue = int.MinValue;
        double MinValue = int.MaxValue;
        double[,] Density = new double[Size, Size];
        double Dens = 0;

        int x, z;
        for (x = 0; x < Size; ++x)
        {
            for (z = 0; z < Size; ++z)
            {
                Dens = Result.GetValue((double)(x + OffsetX), 0, (double)(z + OffsetZ));
                Density[x, z] = Dens;
                if (Dens < MinValue) MinValue = Dens;
                else if (Dens > MaxValue) MaxValue = Dens;
            }
        }

        double WaterLevel = (1f / System.Math.Abs(MinValue - MaxValue)) * (-MinValue);
        MinValue = System.Math.Abs(MinValue);
        MaxValue = System.Math.Abs(MaxValue);
        Color col = new Color();
        col.a = 1;
        for (x = 0; x < Size; ++x)
        {
            for (z = 0; z < Size; ++z)
            {
                Dens = (Density[x, z] + MinValue) / (MaxValue + MinValue);
                col.r = col.g = col.b = (float)Dens;
                if (Density[x, z] < 0)
                    col.b = 1f;

                Texture.SetPixel(x, z, col);
            }
        }

        Texture.Apply();
        return (float)WaterLevel;
    }

    public void CheckSelectedColor(NoiseWindow Preset, int Value)
    {
        if (SelectedWindow == Preset && SelectedPresetValue == Value)
            GUI.backgroundColor = Color.blue;
        else
            GUI.backgroundColor = Color.white;
    }

    public void SelectPreset(NoiseWindow Window, int Value)
    {
        SelectedWindow = Window;
        SelectedPresetValue = Value;
    }
}
