using UnityEditor;

[CustomEditor(typeof(DetailProcessor))]
public class DetailGUIInspector : Editor
{
    public InspectorTempData TempData;

    public override void OnInspectorGUI()
    {
        if (TempData == null)
            TempData = new InspectorTempData();

        //base.OnInspectorGUI();
        DetailProcessor Processor = (DetailProcessor)target;

        Processor.Priority = EditorGUILayout.IntField("Priority", Processor.Priority);
        Processor.DataName = EditorGUILayout.TextField("DataName", Processor.DataName);
        if (string.IsNullOrEmpty(Processor.DataName))
        {
            EditorGUILayout.HelpBox("Please, set a Data Name. This value is used to save/load/send processor data.Each Processor DataName must be different.", MessageType.Info);
            return;
        }

        Processor.ViewDistance = EditorGUILayout.FloatField("ViewDistance", Processor.ViewDistance);
        Processor.ChunkSize = (TerrainEngine.VoxelSizes)EditorGUILayout.EnumPopup("ChunkSize", Processor.ChunkSize);

        Processor.FadeName = EditorGUILayout.TextField("FadeName", Processor.FadeName);

        Processor.ReceiveShadow = EditorGUILayout.Toggle("Receive Shadow", Processor.ReceiveShadow);
        Processor.CastShadow = EditorGUILayout.Toggle("Cast Shadow", Processor.ReceiveShadow);

        //base.OnInspectorGUI();

        if (Processor.Container != null)
        {
            EditorGUILayout.BeginVertical("window");
            Processor.Density = EditorGUILayout.Vector2Field("Density", Processor.Density);
            if(Processor is AdvancedDetails)
                (Processor as AdvancedDetails).TopDensity = EditorGUILayout.Vector2Field("Top Density", (Processor as AdvancedDetails).TopDensity);
            DetailsContainerInspector.DisplayDetail(Processor, Processor.Container, TempData);
            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.Separator();
    }
}