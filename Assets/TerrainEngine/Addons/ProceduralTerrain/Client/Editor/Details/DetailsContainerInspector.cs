
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(DetailsContainer))]
public class DetailsContainerInspector : Editor
{
    public InspectorTempData TempData;

    public override void OnInspectorGUI()
    {
        if (TempData == null)
            TempData = new InspectorTempData();
        DetailsContainer Container = (DetailsContainer)target;



        EditorGUILayout.Separator();
        DisplayDetail(null, Container, TempData);
        EditorGUILayout.Separator();

        base.OnInspectorGUI();
    }

    static public void DisplayObjects(DetailProcessor Processor, DetailsContainer Container, InspectorTempData TempData, AdvancedHeightMap AdvancedHeight)
    {
        GUIStyle ButtonStyle = GUI.skin.button;
        GUIStyle BoxStyle = GUI.skin.box;
        ButtonStyle.alignment = BoxStyle.alignment = TextAnchor.MiddleCenter;
        ButtonStyle.imagePosition = BoxStyle.imagePosition = ImagePosition.ImageAbove;

        int TotalChance = 0;
        int Count = TempData.FilterObjects.Count;
        int Width = Screen.width - 100;
        if (Width < 90) Width = 90;
        DetailInfo Info;

         EditorGUILayout.BeginHorizontal();
        {
            for (int i = 0; i < Count + 1; ++i)
            {
                if (i % (Width / 90) == 0)
                {
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                }

                if (i >= TempData.FilterObjects.Count)
                {
                    if (GUILayout.Button("New", GUILayout.Width(90), GUILayout.Height(90)))
                    {
                        TempData.Selected = Container.Details.Count;
                        Container.Details.Add(new DetailInfo());
                        return;
                    }
                }
                else
                {
                    Info = TempData.FilterObjects[i] as DetailInfo;

                    TotalChance += Info.Chance;

                    if (Info.RandomScale.Min.x <= 0) Info.RandomScale.Min.x = 1;
                    if (Info.RandomScale.Min.y <= 0) Info.RandomScale.Min.y = 1;

                    string Name = Info.GetName();
                    Texture2D Preview = Info.Object != null ? AssetPreview.GetAssetPreview(Info.Object.gameObject) : null;

                    bool Sel = TempData.SelectedObjects.Contains(Name);

                    if (Info.Chance == 0)
                        GUI.backgroundColor = Color.red;
                    else if (i == TempData.Selected)
                        GUI.backgroundColor = Color.cyan;
                    else if (Sel)
                        GUI.backgroundColor = Color.green;

                    EditorGUILayout.BeginVertical(BoxStyle);
                    {
                        EditorGUILayout.BeginHorizontal();
                        Info.Object = EditorGUILayout.ObjectField(Info.Object, typeof(MeshFilter), false, GUILayout.Width(72)) as MeshFilter;
                        bool Result = EditorGUILayout.Toggle(Sel, GUILayout.Width(15));

                        EditorGUILayout.EndHorizontal();

                        if (Result != Sel)
                        {
                            if (Result)
                                TempData.SelectedObjects.Add(Name);
                            else
                                TempData.SelectedObjects.Remove(Name);
                        }

                        if (i == TempData.Selected)
                        {
                            GUILayout.Label(Preview, GUIStyle.none, GUILayout.Width(90), GUILayout.Height(90));
                        }
                        else
                        {
                            string DetailLabel = "Vert:" + (Info.Object != null ? Info.Object.sharedMesh.vertices.Length : 0);

                            if (Processor != null && Info.Object != null)
                            {
                                bool CanCastShadow = true;
                                bool CanReceiveShadow = true;
                                MeshRenderer[] Renderers = Info.Object.GetComponentsInChildren<MeshRenderer>(true);
                                if (Renderers != null)
                                {
                                    foreach (MeshRenderer Renderer in Renderers)
                                    {
                                        foreach (Material Mat in Renderer.sharedMaterials)
                                        {
                                            CanCastShadow = true;
                                            CanReceiveShadow = true;

                                            if (Mat != null)
                                                Processor.GetMaterialInfo(Mat.GetInstanceID(), ref CanCastShadow, ref CanReceiveShadow);

                                            if (!CanCastShadow)
                                            {
                                                DetailLabel += "/C";
                                            }
                                            if (!CanReceiveShadow)
                                            {
                                                DetailLabel += "/R";
                                            }
                                        }
                                    }
                                }
                            }


                            if (GUILayout.Button(new GUIContent(DetailLabel, Preview), GUI.skin.button, GUILayout.Width(90), GUILayout.Height(90)))
                            {
                                if (Event.current.control)
                                {
                                    if (!Result)
                                        TempData.SelectedObjects.Add(Name);
                                    else
                                        TempData.SelectedObjects.Remove(Name);
                                }
                                else if (Event.current.shift)
                                {
                                    if (TempData.Selected < i)
                                    {
                                        for (; TempData.Selected <= i; ++TempData.Selected)
                                        {
                                            if (!TempData.SelectedObjects.Contains(TempData.FilterObjects[TempData.Selected].Name))
                                                TempData.SelectedObjects.Add(TempData.FilterObjects[TempData.Selected].Name);
                                        }
                                    }
                                    else
                                    {
                                        for (int z = i; z <= TempData.Selected; ++z)
                                        {
                                            if (!TempData.SelectedObjects.Contains(TempData.FilterObjects[z].Name))
                                                TempData.SelectedObjects.Add(TempData.FilterObjects[z].Name);
                                        }
                                    }
                                }

                                TempData.Selected = i;
                            }
                        }
                    }
                    EditorGUILayout.EndVertical();
                    GUI.backgroundColor = Color.white;
                }
            }
        }
        EditorGUILayout.EndHorizontal();
    }

    static public List<string> Biomes = new List<string>();
    static public void DisplayDetail(DetailProcessor Processor, DetailsContainer Container, InspectorTempData TempData)
    {
        if (Container.Details == null)
            return;

        AdvancedHeightMap AdvancedHeight = FindObjectOfType(typeof(AdvancedHeightMap)) as AdvancedHeightMap;

        GUILayout.BeginVertical(GUI.skin.box);
        {
                GUILayout.BeginVertical();
                GUILayout.Label("Ctrl+Click|Shift+Click|Vert:Vertices");
                GUILayout.Label("C:CastShadow=false|R:ReceiveShadow=false");
                GUILayout.EndVertical();

                GUILayout.BeginHorizontal();
                {
                if (GUILayout.Button("All"))
                {
                    TempData.SelectedObjects.Clear();
                    foreach (DetailInfo SubInfo in Processor.Details)
                    {
                        TempData.SelectedObjects.Add(SubInfo.GetName());
                    }
                }
                if (GUILayout.Button("None"))
                {
                    TempData.SelectedObjects.Clear();
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Filter:");
                if (GUILayout.Button(TempData.IsBiomeFilter ? "All" : "Biomes"))
                {
                    TempData.IsBiomeFilter = !TempData.IsBiomeFilter;
                    TempData.SelectedObjects.Clear();
                    TempData.Selected = -1;
                }
            }
            GUILayout.EndHorizontal();

            if (AdvancedHeight == null)
                TempData.IsBiomeFilter = false;

            TempData.FilterObjects.Clear();

            if (TempData.IsBiomeFilter)
            {
                GUILayout.BeginVertical("window");

                Biomes.Clear();
                AdvancedHeight.Container.GetBiomes(null, Biomes);
                foreach (string biome in Biomes)
                {
                    EditorGUILayout.BeginHorizontal();
                    bool Value = GUILayout.Button(biome);
                    EditorGUILayout.EndHorizontal();
                    if (Value)
                    {
                        TempData.Selected = -1;

                        if (!TempData.OpenedBiomes.Contains(biome))
                            TempData.OpenedBiomes.Add(biome);
                        else
                            TempData.OpenedBiomes.Remove(biome);
                    }

                    if (TempData.OpenedBiomes.Contains(biome))
                    {
                        for (int i = 0; i < Processor.Details.Count; ++i)
                        {
                            if (Container.DetailsNoises.Objects.GetNoise(biome) != null && Container.DetailsNoises.Objects.GetNoise(biome).GetObject(Processor.Details[i].GetName()) != null)
                                TempData.FilterObjects.Add(Processor.Details[i]);
                        }

                        DisplayObjects(Processor, Container, TempData, AdvancedHeight);
                    }
                }
                GUILayout.EndVertical();
            }
            else
            {
                for (int i = 0; i < Processor.Details.Count; ++i)
                {
                    TempData.FilterObjects.Add(Processor.Details[i]);
                }

                DisplayObjects(Processor,Container, TempData, AdvancedHeight);
            }
        }
        GUILayout.EndVertical();

        DetailInfo Info = null;
        if (TempData.Selected >= 0 && TempData.Selected < Processor.Details.Count)
            Info = Processor.Details[TempData.Selected];

        if (Info == null)
            return;


        EditorGUILayout.BeginVertical("box");
        {
            EditorGUILayout.BeginHorizontal();
            {
                Info.Object = EditorGUILayout.ObjectField(Info.Object, typeof(MeshFilter), false, GUILayout.Width(200), GUILayout.Height(20)) as MeshFilter;
                if (GUILayout.Button("Remove", GUILayout.Width(80)) && Container.Details.Count != 0)
                {
                    Container.Details.Remove(Info);
                    TempData.Selected = 0;
                }
            }
            EditorGUILayout.EndHorizontal();

            Info.Chance = EditorGUILayout.IntField("Chance", Info.Chance);
            Info.HeightSide = (HeightSides)EditorGUILayout.EnumPopup("Ground Side : ", Info.HeightSide);

            EditorGUILayout.BeginVertical("box");
            {
                Info.RandomScale.Enabled = EditorGUILayout.Foldout(Info.RandomScale.Enabled, "Random Scale :" + Info.RandomScale.Enabled);
                if (Info.RandomScale.Enabled)
                {
                    EditorGUILayout.BeginVertical("box");

                    Info.RandomScale.Min = EditorGUILayout.Vector2Field("MinScale", Info.RandomScale.Min);
                    Info.RandomScale.Max = EditorGUILayout.Vector2Field("MaxScale", Info.RandomScale.Max);

                    if (Info.NoiseNameScale.Length != 0)
                    {
                        if (Container.DetailsNoises.Container.GetPreset(Info.NoiseNameScale) != null)
                            GUI.backgroundColor = Color.green;
                        else
                        {
                            GUI.backgroundColor = Color.red;
                        }
                    }

                    if (GUI.backgroundColor == Color.red)
                    {
                        EditorGUILayout.HelpBox("Noise not found", MessageType.Error);
                    }
                    GUI.backgroundColor = Color.white;

                    Info.NoiseNameScale = EditorGUILayout.TextField("Noise Name Scale", Info.NoiseNameScale);
                    Info.RandomScale.UseXOnly = true;
                    EditorGUILayout.EndVertical();
                }
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical("box");
            Info.AlignToTerrain = EditorGUILayout.Foldout(Info.AlignToTerrain, "Align To Terrain:" + Info.AlignToTerrain);
            if (Info.AlignToTerrain)
            {
                Info.AlignementClampGetSet = EditorGUILayout.FloatField("Alignment Clamp", Info.AlignementClampGetSet);
                Info.SlopeLimits = EditorGUILayout.Vector2Field("SlopeLimits", Info.SlopeLimits);
            }
            EditorGUILayout.EndVertical();

            Info.PositionOffset = EditorGUILayout.Vector3Field("Position Offset: ", Info.PositionOffset);
            Info.RotationOffset = EditorGUILayout.Vector3Field("Rotation Offset: ", Info.RotationOffset);
            Info.SetVerticeZeroCenter = EditorGUILayout.Toggle("Set Vertice 0 Center", Info.SetVerticeZeroCenter);

            EditorGUILayout.BeginVertical("box");

            Info.GenerateColors = EditorGUILayout.Foldout(Info.GenerateColors, "Generate Noise Colors");
            if (Info.GenerateColors)
                Info.NoiseColorScale = EditorGUILayout.FloatField("Noise Scale", Info.NoiseColorScale);

            if (Info.GenerateColors)
            {
                EditorGUILayout.BeginHorizontal();
                Info.NoiseColorA = EditorGUILayout.ColorField(Info.NoiseColorA);
                Info.NoiseColorB = EditorGUILayout.ColorField(Info.NoiseColorB);
                Info.NoiseColorC = EditorGUILayout.ColorField(Info.NoiseColorC);
                EditorGUILayout.EndHorizontal();

                if (GUILayout.Button("Apply To Selected (" + TempData.SelectedObjects.Count + ")", GUILayout.Width(200)))
                {
                    foreach (DetailInfo SubInfo in Container.Details)
                    {
                        if (SubInfo == Info || !TempData.SelectedObjects.Contains(SubInfo.GetName()))
                            continue;

                        SubInfo.GenerateColors = Info.GenerateColors;
                        SubInfo.NoiseColorA = Info.NoiseColorA;
                        SubInfo.NoiseColorB = Info.NoiseColorB;
                        SubInfo.NoiseColorC = Info.NoiseColorC;
                        SubInfo.NoiseColorScale = Info.NoiseColorScale;
                    }
                }
            }
            EditorGUILayout.EndVertical();

            bool Open = false;

            if (Info.Object != null)
            {
                TempData.Foldouts.TryGetValue("Materials", out Open);
                Open = EditorGUILayout.Foldout(Open, "Shadows");
                TempData.Foldouts["Materials"] = Open;
            }

            if (Open)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(20);
                EditorGUILayout.BeginVertical("box");

                bool CanCastShadow = true;
                bool CanReceiveShadow = true;

                MeshRenderer[] Renderers = Info.Object.gameObject.GetComponentsInChildren<MeshRenderer>(true);

                if (Renderers != null)
                {
                    foreach (MeshRenderer Renderer in Renderers)
                    {
                        foreach (Material Mat in Renderer.sharedMaterials)
                        {
                            CanCastShadow = true;
                            CanReceiveShadow = true;
                            Processor.GetMaterialInfo(Mat.GetInstanceID(), ref CanCastShadow, ref CanReceiveShadow);
                            bool CCS = EditorGUILayout.Toggle(Mat.name + " CanCastShadow", CanCastShadow);
                            bool CRS = EditorGUILayout.Toggle(Mat.name + " CanReceiveShadow", CanReceiveShadow);
                            if (CCS != CanCastShadow || CRS != CanReceiveShadow)
                                Processor.SetMaterialInfo(Mat.GetInstanceID(), CCS, CRS);
                        }
                    }
                }

                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
            }

            if (GUILayout.Button("Apply To Selected (" + TempData.SelectedObjects.Count + ")", GUILayout.Width(200)))
            {
                foreach (DetailInfo SubInfo in Container.Details)
                {
                    if (SubInfo == Info || !TempData.SelectedObjects.Contains(SubInfo.GetName()))
                        continue;

                    SubInfo.Chance = Info.Chance;
                    SubInfo.HeightSide = Info.HeightSide;
                    SubInfo.RandomScale.Min = Info.RandomScale.Min;
                    SubInfo.RandomScale.Max = Info.RandomScale.Max;
                    SubInfo.RandomScale.Enabled = Info.RandomScale.Enabled;
                    SubInfo.RandomScale.UseXOnly = Info.RandomScale.UseXOnly;
                    SubInfo.RotationOffset = Info.RotationOffset;
                    SubInfo.NoiseNameScale = Info.NoiseNameScale;
                    SubInfo.AlignmentClamp = Info.AlignmentClamp;
                }
            }
        }
        EditorGUILayout.EndVertical();

        if (Processor is AdvancedDetails && Container.DetailsNoises != null && Info.Object != null && AdvancedHeight != null)
        {
            GUILayout.BeginVertical("'" + Info.GetName() + "' Valid Biomes", GUI.skin.window);
            {
                TempData.CurrentName = Info.GetName();
                BiomeObjectsContainerInspector.DrawBiomesObjects(TempData, AdvancedHeight, AdvancedHeight.Container, Container.DetailsNoises);
            }
            GUILayout.EndVertical();

            BiomeNoisesContainerInspector.DrawObjectNoises(Container.DetailsNoises.Container);
        }

        if (GUILayout.Button("Set Chance 1 if 0", GUILayout.Width(150)))
        {
            foreach (DetailInfo Detail in Container.Details)
            {
                if (Detail.Chance == 0)
                    Detail.Chance = 1;
            }
        }

        EditorUtility.SetDirty(Container);
    }

    static public string NewBiomeName = "";
}