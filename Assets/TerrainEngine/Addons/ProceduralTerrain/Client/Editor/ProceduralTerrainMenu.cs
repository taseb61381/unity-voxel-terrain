﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class ProceduralTerrainMenu
{
    #region AdvancedHeightMap

    [MenuItem("TerrainEngine/Processors/Procedural/Advanced/0-HeightMap")]
    static public void CreateHeightMap()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        AdvancedHeightMap Obj = GameObject.FindObjectOfType(typeof(AdvancedHeightMap)) as AdvancedHeightMap;
        if (Obj == null)
            Obj = new GameObject().AddComponent<AdvancedHeightMap>();

        Obj.name = "0-HeightMap";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 0;

        if (Obj.Container == null)
        {
            Debug.Log("Creating HeightMap Biomes Container. This Script contains all biomes details.");

            GameObject C = new GameObject();
            C.name = "0-Biomes Noises";
            C.transform.parent = Obj.transform;
            Obj.Container = C.AddComponent<BiomeNoisesContainer>();

            NoisesContainer Container = Obj.Container.Container = new NoisesContainer();
            Obj.Container.NoiseTypes = new NoiseObjectsContainer();
            Obj.Container.ExternalContainers = new List<NoisesContainer>();

            BiomeNoise Ground = CreateBiomeNoise(Container, "Ground", true, 1, 0.003f, BiomeNoise.NoiseTypes.Perlin);
            BiomeNoise Valleys = CreateBiomeNoise(Container, "Valleys", false, 4, 0.003f, BiomeNoise.NoiseTypes.Perlin);
            BiomeNoise Mountains = CreateBiomeNoise(Container, "Mountains", false, 4, 0.0005f, BiomeNoise.NoiseTypes.RidgedMultifractal);

            SetBiomePreCalculation(Ground, 1f, 4f);
            SetBiomePreCalculation(Valleys, 0.5f, 10f);
            SetBiomePreCalculation(Mountains, 1f, 64f);

            NoisePreset ValleyMountains = new NoisePreset();
            ValleyMountains.Name = "ValleysMountains";
            ValleyMountains.Computation = NoiseComputations.ADD;
            ValleyMountains.PresetA = "Valleys";
            ValleyMountains.PresetB = "Mountains";

            NoisePreset Main = new NoisePreset();
            Main.Name = "Main";
            Main.Computation = NoiseComputations.ADD;
            Main.PresetA = "Ground";
            Main.PresetB = "ValleysMountains";

            Container.NoisesPreset.Add(ValleyMountains);
            Container.NoisesPreset.Add(Main);
        }
    }

    static public BiomeNoise CreateBiomeNoise(NoisesContainer Container, string Name, bool IsBiome, int Octaves, float Frequency, BiomeNoise.NoiseTypes Type)
    {
        BiomeNoise Noise = new BiomeNoise();
        Noise.NoiseType = Type;
        Noise.Name = Name;
        Noise.Octaves = Octaves;
        Noise.Frequency = Frequency;
        Container.Noises.Add(Noise);

        NoisePreset Preset = new NoisePreset();
        Preset.Computation = NoiseComputations.NOISE;
        Preset.Name = Name;
        Preset.NoiseName = Name;
        Container.NoisesPreset.Add(Preset);
        return Noise;
    }

    static public void SetBiomePreCalculation(BiomeNoise Noise, float Bias, float Scale)
    {
        if (Noise.PreCalculations == null)
            Noise.PreCalculations = new List<BiomeNoise.PrecalculationInformations>();

        BiomeNoise.PrecalculationInformations PreCal = new BiomeNoise.PrecalculationInformations();
        PreCal.Name = BiomeNoise.BiomePreCalculationTypes.BiasScaleOutput;
        PreCal.Bias = Bias;
        PreCal.Scale = Scale;
        Noise.PreCalculations.Add(PreCal);
    }

    [MenuItem("TerrainEngine/Processors/Procedural/Advanced/0-FloatingIslands")]
    static public void CreateIslands()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        AdvancedIslands Obj = GameObject.FindObjectOfType(typeof(AdvancedIslands)) as AdvancedIslands;
        if (Obj == null)
            Obj = new GameObject().AddComponent<AdvancedIslands>();

        Obj.name = "0-FloatindIslands";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 0;
    }

    [MenuItem("TerrainEngine/Processors/Procedural/Advanced/1-Caves")]
    static public void CreateCaves()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        AdvancedCaves Obj = GameObject.FindObjectOfType(typeof(AdvancedCaves)) as AdvancedCaves;
        if (Obj == null)
            Obj = new GameObject().AddComponent<AdvancedCaves>();

        Obj.name = "1-Caves";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 1;
    }

    [MenuItem("TerrainEngine/Processors/Procedural/Advanced/2-AdvancedTreesProcessor")]
    static public void CreateTreesProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        CreateHeightMap();

        AdvancedGameObjects Obj = new GameObject().AddComponent<AdvancedGameObjects>();

        Obj.name = "2-AdvancedTreesProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 2;

        Obj.Container = new GameObject().AddComponent<GameObjectsContainer>();
        Obj.Container.name = "Objects Container";
        Obj.Container.transform.parent = Obj.transform;

        Obj.Container.ObjectsNoises = Obj.Container.gameObject.AddComponent<BiomeObjectsContainer>();
    }

    [MenuItem("TerrainEngine/Processors/Procedural/Advanced/3-AdvancedDetailsProcessor")]
    static public void CreateDetailsProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        CreateHeightMap();

        AdvancedDetails Obj = new GameObject().AddComponent<AdvancedDetails>();

        Obj.name = "3-AdvancedDetailsProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 3;

        Obj.Container = new GameObject().AddComponent<DetailsContainer>();
        Obj.Container.name = "Objects Container";
        Obj.Container.transform.parent = Obj.transform;

        Obj.Container.DetailsNoises = Obj.Container.gameObject.AddComponent<BiomeObjectsContainer>();
    }

    [MenuItem("TerrainEngine/Processors/Procedural/Advanced/4-AdvancedGrassProcessor")]
    static public void CreateGrassProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        CreateHeightMap();

        AdvancedGrass Obj = new GameObject().AddComponent<AdvancedGrass>();

        Obj.name = "4-AdvancedGrassProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 4;

        Obj.Container = new GameObject().AddComponent<GrassContainer>();
        Obj.Container.name = "Objects Container";
        Obj.Container.transform.parent = Obj.transform;

        Obj.Container.GrassNoises = Obj.Container.gameObject.AddComponent<BiomeObjectsContainer>();
    }

    #endregion

    #region Random

    [MenuItem("TerrainEngine/Processors/Procedural/Random/2-RandomGameObjectsProcessor")]
    static public void CreateRandomGameObjectsProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        GameObjectProcessor Obj = new GameObject().AddComponent<GameObjectProcessor>();

        Obj.name = "2-RandomGameObjectProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 4;

        Obj.Container = new GameObject().AddComponent<GameObjectsContainer>();
        Obj.Container.name = "Objects Container";
        Obj.Container.transform.parent = Obj.transform;

        Obj.Container.ObjectsNoises = Obj.Container.gameObject.AddComponent<BiomeObjectsContainer>();
    }


    [MenuItem("TerrainEngine/Processors/Procedural/Random/3-RandomDetailsProcessor")]
    static public void CreateRamdomDetailsProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        CreateHeightMap();

        DetailProcessor Obj = new GameObject().AddComponent<DetailProcessor>();

        Obj.name = "3-RandomDetailsProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 3;

        Obj.Container = new GameObject().AddComponent<DetailsContainer>();
        Obj.Container.name = "Objects Container";
        Obj.Container.transform.parent = Obj.transform;

        Obj.Container.DetailsNoises = Obj.Container.gameObject.AddComponent<BiomeObjectsContainer>();
    }

    [MenuItem("TerrainEngine/Processors/Procedural/Random/4-RandomGrassProcessor")]
    static public void CreateRandomGrassProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        GrassProcessor Obj = new GameObject().AddComponent<GrassProcessor>();

        Obj.name = "4-RandomGrassProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 4;

        Obj.Container = new GameObject().AddComponent<GrassContainer>();
        Obj.Container.name = "Objects Container";
        Obj.Container.transform.parent = Obj.transform;

        Obj.Container.GrassNoises = Obj.Container.gameObject.AddComponent<BiomeObjectsContainer>();
    }

    #endregion

    [MenuItem("TerrainEngine/TerrainObject/Setup Main Camera")]
    static public void InitMainCamera()
    {
        Camera Cam = GameObject.FindObjectOfType(typeof(Camera)) as Camera;
        if (Cam != null)
        {
            if (Cam.gameObject.GetComponent<TerrainObject>() == null)
                Cam.gameObject.AddComponent<TerrainObject>();

            if (Cam.gameObject.GetComponent<TerrainObjectGrassViewer>() == null)
                Cam.gameObject.AddComponent<TerrainObjectGrassViewer>();

            if (Cam.gameObject.GetComponent<TerrainObjectDetailViewer>() == null)
                Cam.gameObject.AddComponent<TerrainObjectDetailViewer>();

            Debug.Log(Cam.gameObject + " Inited. Added TerrainObject,TerrainObjectGrassViewer,TerrainObjectDetailViewer");
        }
        else
            Debug.Log("No camera found");
    }
}
