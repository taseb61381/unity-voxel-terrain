﻿
using LibNoise;
using TerrainEngine;

public class AdvancedCaves : IBlockProcessor
{
    public float MinRidgetNoise = 0.5f;
    public float RidgedFrequency = 0.01f;
    public float GroundVariationPower = 3f;
    public float GroundOffset = -1;
    public float HeightMinSize = 5;
    public float HeightVariationPower = 6f;

    // Perlin that allow cave to join surface
    public float CaveEntryMinSize = 0.4f;
    public float CaveEntryFrequency = 0.05f;

    // Perlin that determine if cave can be created
    public float CaveNoiseFrequency = 0.005f;
    public float CaveNoiseMinValue = 0.42f;

    public int CaveYCount = 2;
    public float CaveHeightOffset = 4;

    public override IWorldGenerator GetGenerator()
    {
        CavesGenerator Gen = new CavesGenerator();
        Gen.MinRidgetNoise = MinRidgetNoise;
        Gen.RidgedFrequency = RidgedFrequency;
        Gen.GroundVariationPower = GroundVariationPower;
        Gen.GroundOffset = GroundOffset;
        Gen.HeightMinSize = HeightMinSize;
        Gen.HeightVariationPower = HeightVariationPower;
        Gen.CaveEntryMinSize = CaveEntryMinSize;
        Gen.CaveEntryFrequency = CaveEntryFrequency;

        Gen.CaveNoiseFrequency = CaveNoiseFrequency;
        Gen.CaveNoiseMinValue = CaveNoiseMinValue;

        Gen.CaveYCount = CaveYCount;
        Gen.CaveHeightOffset = CaveHeightOffset;
        Gen.Name = InternalName;
        Gen.DataName = "Caves-"+DataName;
        return Gen;
    }

}
