﻿using TerrainEngine;

public class AdvancedIslands : IBlockProcessor 
{
    public string RequiredBiome = "";
    public int HeightFromGround = 80;
    public float IslandTopScale = 8f;
    public float IslandDownScale = 40f;
    public float Frequency = 0.007f;
    public float MinNoise = 0.5f;
    public float HeightRandomVariation = 30;
    public float HeightRandomFrequency = 0.005f;

    public bool GenerateWaterFall = true;
    public int WaterfallRandomHeight = 4;

    public override IWorldGenerator GetGenerator()
    {
        FloatingIslandGenerator Island = new FloatingIslandGenerator();
        Island.Name = DataName;
        Island.DataName = DataName;
        Island.RequiredBiome = RequiredBiome;
        Island.HeightFromGround = HeightFromGround;
        Island.IslandTopScale = IslandTopScale;
        Island.IslandDownScale = IslandDownScale;
        Island.Frequency = Frequency;
        Island.MinNoise = MinNoise;
        Island.HeightRandomVariation = HeightRandomVariation;
        Island.HeightRandomFrequency = HeightRandomFrequency;
        Island.GenerateWaterFall = GenerateWaterFall;
        Island.WaterfallRandomHeight = WaterfallRandomHeight;
        return Island as IWorldGenerator;
    }
}
