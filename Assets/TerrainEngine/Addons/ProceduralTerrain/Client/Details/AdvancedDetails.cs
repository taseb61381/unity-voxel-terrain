
using TerrainEngine;
using UnityEngine;

public class AdvancedDetails : DetailProcessor
{
    /// <summary>
    /// Density for details objects on height side = Top. Liana, Roots, etc
    /// </summary>
    public Vector2 TopDensity = new Vector2(20, 100);

    public BiomeObjectsContainer ObjectsContainer
    {
        get
        {
            return Container.DetailsNoises;
        }
    }

    public override void OnInitSeed(int seed)
    {
        EnableGenerationFunction = false;
    }

    public override IWorldGenerator GetGenerator()
    {
        AdvancedDetailsGenerator Gen = new AdvancedDetailsGenerator();
        Gen.Density = Density;
        Gen.TopDensity = TopDensity;
        Gen.Name = InternalName;
        Gen.DataName = "Details-"+DataName;
        Gen.DetailsObjects = Details;

        if (ObjectsContainer != null)
        {
            Gen.ObjectsRequirements = ObjectsContainer.Objects;
            Gen.RequirementNoises = ObjectsContainer.Container;
        }

        return Gen;
    }
}
