using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Processors/Details")]
public class DetailProcessor : IViewableProcessor
{
    [Serializable]
    public struct MaterialInfo
    {
        public int InstanceId;
        public bool CanCastShadow;
        public bool CanReceiveShadow;
    }

    public Vector2 Density; // Chance / Chance Max Density. Example : 10/1000. 10 Objects every 1000 voxels.
    public bool ReceiveShadow = true;
    public bool CastShadow = false;
    public DetailsContainer Container;
    public List<DetailInfo> Details
    {
        get
        {
            return Container.Details;
        }
    }
    public List<MaterialInfo> MaterialsInfo;
    private bool IsInited = false;

    private List<Material> DetailMaterials; // List of materials, used for batching.Only unique materials listed
    private SList<byte> DetailMaterialsProperty; // One byte for each material, define if material has Property or not 0 -> 4

    public DetailInfo GetInfo(string Name)
    {
        if (Container != null)
            return Container.GetInfo(Name);
        else
            return null;
    }

    public int GetMaterialInfo(int InstanceId,ref bool CanCastShadow,ref bool CanReceiveShadow)
    {
        if (MaterialsInfo == null)
            return -1;

        for(int i=MaterialsInfo.Count-1;i>=0;--i)
        {
            if (MaterialsInfo[i].InstanceId == InstanceId)
            {
                CanCastShadow = MaterialsInfo[i].CanCastShadow;
                CanReceiveShadow = MaterialsInfo[i].CanReceiveShadow;
                return i;
            }
        }

        return -1;
    }

    public int GetMaterialInfo(int InstanceId)
    {
        if (MaterialsInfo == null)
            return -1;

        for (int i = MaterialsInfo.Count - 1; i >= 0; --i)
        {
            if (MaterialsInfo[i].InstanceId == InstanceId)
            {
                return i;
            }
        }

        return -1;
    }

    public void SetMaterialInfo(int InstanceId, bool CanCastShadow,bool CanReceiveShadow)
    {
        int Index = GetMaterialInfo(InstanceId);
        if (Index == -1)
        {
            if (CanCastShadow && CanReceiveShadow)
                return;

            MaterialInfo Info = new MaterialInfo();
            Info.CanCastShadow = CanCastShadow;
            Info.CanReceiveShadow = CanReceiveShadow;
            Info.InstanceId = InstanceId;
            MaterialsInfo.Add(Info);
        }
        else
        {
            if (CanCastShadow && CanReceiveShadow)
                MaterialsInfo.RemoveAt(Index);
            else
            {
                MaterialInfo Info = MaterialsInfo[Index];
                Info.CanCastShadow = CanCastShadow;
                Info.CanReceiveShadow = CanReceiveShadow;
                MaterialsInfo[Index] = Info;
            }
        }
    }

    public override void OnInitManager(TerrainManager Manager)
    {
        base.OnInitManager(Manager);
        InternalDataName = "Details-" + DataName;
    }

    public override void OnBlockDataCreate(TerrainDatas Data)
    {
        SimpleTypeDatas CData = Data.GetCustomData<SimpleTypeDatas>(InternalDataName);
        if (CData == null)
        {
            CData = new SimpleTypeDatas();
            Data.RegisterCustomData(InternalDataName, CData);
        }
    }

    public bool IsSharingSameMaterial(DetailInfo Info, DetailInfo SubInfo)
    {
        foreach (int MatId in Info.MaterialId)
        {
            foreach (int SubMatId in SubInfo.MaterialId)
            {
                if (MatId == SubMatId)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public override bool CanStart()
    {
        if (Details.Count > 255)
        {
            Debug.LogError("Details : 255 Mesh maximum.");
            return false;
        }

        if (!IsInited)
        {
            GraphicMesh._ColorId = Shader.PropertyToID("_Color");
            GraphicMesh._CutoffId = Shader.PropertyToID("_Cutoff");
            GraphicMesh._FadeColorId = Shader.PropertyToID("_FadeColor");

            IsInited = true;
            if (Container == null || Container.Details == null)
            {
                Debug.LogError("No Objects in DetailsProcessor.");
                enabled = false;
                return false;
            }

            DetailMaterials = new List<Material>(10);
            DetailMaterialsProperty = new SList<byte>(10);

            int i, j, id;
            DetailInfo Detail;
            for (i = 0; i < Details.Count; ++i)
            {
                (Detail = Details[i]).ObjectId = i;
                Detail.RelationId = 0;
                Detail.ExtractMesh();
                if (Container.DetailsNoises != null)
                    Detail.NoiseScaleModule = Container.DetailsNoises.Container.GetNoiseModule(Detail.NoiseNameScale);

                if (Detail.Data == null)
                {
                    Debug.LogError("Invalid Detail Mesh : " + Detail.GetName());
                    continue;
                }

                for (j = 0; j < Detail.MaterialId.Length; ++j)
                {
                    if (Detail.Data == null || Detail.Data.CMaterials == null || Detail.Data.CMaterials[j] == null)
                        continue;

                    id = DetailMaterials.IndexOf(Detail.Data.CMaterials[j]);
                    if (id < 0)
                    {
                        id = DetailMaterials.Count;
                        DetailMaterials.Add(Detail.Data.CMaterials[j]);

                        if (Detail.Data.CMaterials[j].HasProperty(FadeName))
                        {
                            DetailMaterialsProperty.Add(1);
                        }
                        else if (Detail.Data.CMaterials[j].HasProperty(GraphicMesh._CutoffId))
                        {
                            DetailMaterialsProperty.Add(2);
                        }
                        else if (Detail.Data.CMaterials[j].HasProperty(GraphicMesh._FadeColorId))
                        {
                            DetailMaterialsProperty.Add(3);
                        }
                        else if (Detail.Data.CMaterials[j].HasProperty(GraphicMesh._ColorId))
                        {
                            DetailMaterialsProperty.Add(4);
                        }
                        else
                            DetailMaterialsProperty.Add(0);

                    }

                    Detail.MaterialId[j] = id;
                }
            }

            foreach (DetailInfo Info in Details)
            {
                if (Info.RelationId != 0)
                    continue;

                foreach (DetailInfo SubInfo in Details)
                {
                    if (SubInfo == Info)
                        continue;

                    if (IsSharingSameMaterial(Info, SubInfo))
                    {
                        //Debug.Log(name + " : Object " + Info.Object.name + " Sharing Material(s) with : " + SubInfo.Object.name);
                        SubInfo.RelationId = Info.GetObjectId();
                        Details[SubInfo.RelationId].Relations.Add(SubInfo.ObjectId);
                    }
                }
            }
        }

        return base.CanStart();
    }

    public override void OnStart()
    {
        if (!Application.isEditor)
        {
            foreach (DetailInfo Info in Details)
            {
                if (Info != null && Info.Object != null)
                    GameObject.DestroyImmediate(Info.Object.gameObject);
            }
        }

        base.OnStart();
    }

    public override IWorldGenerator GetGenerator()
    {
        DetailsGenerator Gen = new DetailsGenerator();
        Gen.Density = Density;
        Gen.Name = InternalName;
        Gen.DataName = "Details-" + DataName;
        Gen.DetailsObjects = Details;
        return Gen;
    }

    public override void GenerateMesh(ActionThread Thread, TerrainBlock Block, int ChunkId, Stack<TempMeshDataId> MDatas, int GenerateId)
    {
        if (Block.Voxels == null)
            return;

        SimpleTypeDatas DData = Block.Voxels.GetCustomData<SimpleTypeDatas>(InternalDataName);
        int LocalX = Block.Chunks[ChunkId].LocalX;
        int LocalY = Block.Chunks[ChunkId].LocalY;
        int LocalZ = Block.Chunks[ChunkId].LocalZ;
        int Size = Block.Information.VoxelPerChunk;
        TempSimpleTypeContainer TempObjects = Thread.Pool.GetData<TempSimpleTypeContainer>("TempSimpleTypeContainer");

        if (GenerateId == 0)
        {
            DetailInfo Info;
            for (int i = 0; i < Details.Count; ++i)
            {
                if ((Info = Details[i]).RelationId != 0)
                    continue;

                TempMeshDataId Tmp = Thread.Pool.GetData<TempMeshDataId>("TempMeshDataId");
                Tmp.GenerateId = Info.ObjectId;
                Tmp.ChunkRef = new TerrainChunkRef(Block, ChunkId);
                Tmp.Data = GenerateMeshId(Block, ChunkId, DData, Info.ObjectId, LocalX * Size, LocalY * Size, LocalZ * Size, TempObjects);
                TempObjects.Clear();

                lock (MDatas)
                    MDatas.Push(Tmp);
            }
        }
        else
        {
            //Debug.Log("Generating Chunk " + Chunk.WorldPosition + " : " + GenerateId);
            TempMeshDataId Tmp = Thread.Pool.GetData<TempMeshDataId>("TempMeshDataId");
            Tmp.GenerateId = GenerateId;
            Tmp.ChunkRef = new TerrainChunkRef(Block, ChunkId);
            Tmp.Data = GenerateMeshId(Block, ChunkId, DData, (byte)GenerateId, LocalX * Size, LocalY * Size, LocalZ * Size, TempObjects);
            
            lock (MDatas)
                MDatas.Push(Tmp);
        }

        TempObjects.Close();
    }

    // --------------------- V10 --------------------------//
    public class TempDetailsData : IPoolable
    {
        public TempDetailsData() : base()
        {

        }

        public int[] ActiveSubMeshs;
        public int[] ActiveMaterials;
        public SList<BiomeData.SimpleTempObject>[] Objects; // V10, Store objects temporary for avoid read on compressed data 2 times.

        public void ClearObjects(int Size)
        {
            if (Objects == null || Objects.Length != Size)
            {
                Objects = new SList<BiomeData.SimpleTempObject>[Size];
                for (Size = Size - 1; Size >= 0; --Size)
                    Objects[Size] = new SList<BiomeData.SimpleTempObject>(100);
            }
            else
            {
                for (Size = Size - 1; Size >= 0; --Size)
                    Objects[Size].ClearFast();
            }
        }

        public void ClearSubMesh(int Size)
        {
            if (ActiveSubMeshs == null || ActiveSubMeshs.Length != Size)
                ActiveSubMeshs = new int[Size];
            else
                for (Size = Size - 1; Size >= 0; --Size)
                    ActiveSubMeshs[Size] = 0;
        }

        public void ClearMaterials(int Size)
        {
            if (ActiveMaterials == null || ActiveMaterials.Length != Size)
            {
                ActiveMaterials = new int[Size];
            }
            else
            {
                for (Size = Size - 1; Size >= 0; --Size)
                {
                    ActiveMaterials[Size] = 0;
                }
            }
        }

        public override void Clear()
        {

        }
    }

    public class MaterialProperties
    {
        public Material[] Materials;
        public bool[] AllowedMaterials;
        public byte[] Properties;
    }

    public List<MaterialProperties> MaterialsProperties = new List<MaterialProperties>(5);

    public PoolInfoRef TempDetailsDataInfo;

    public MaterialProperties GetProperties(int MatCount, int[] ActiveMaterials, int TotalMaterials)
    {
        lock (MaterialsProperties)
        {
            MaterialProperties Mat;
            int j = 0, Index = 0;

            if (MaterialsProperties.Count != 0)
            {

                for (int i = MaterialsProperties.Count - 1; i >= 0; --i)
                {
                    if (MaterialsProperties[i].Materials.Length != MatCount)
                        continue;

                    Mat = MaterialsProperties[i];

                    for (j = 0; j < TotalMaterials; ++j)
                    {
                        if ((ActiveMaterials[j] != 0) != Mat.AllowedMaterials[j])
                        {
                            j = -1;
                            break;
                        }
                    }

                    if (j != -1)
                        return Mat;
                }
            }

            Mat = new MaterialProperties();
            Mat.Materials = new Material[MatCount];
            Mat.Properties = new byte[MatCount];
            Mat.AllowedMaterials = new bool[TotalMaterials];

            Index = 0;
            for (j = 0; j < TotalMaterials; ++j)
            {
                if ((Mat.AllowedMaterials[j] = (ActiveMaterials[j] != 0)))
                {
                    Mat.Materials[Index] = DetailMaterials[j];
                    Mat.Properties[Index] = DetailMaterialsProperty.array[j];
                    ++Index;
                }
            }

            MaterialsProperties.Add(Mat);
            return Mat;
        }
    }

    public IMeshData GenerateMeshId(TerrainBlock Block, int ChunkId, SimpleTypeDatas DData, int GenerateId, int StartX, int StartY, int StartZ,TempSimpleTypeContainer TempObjects)
    {
        if (GenerateId >= Details.Count)
        {
            Debug.LogError("Invalid GenerateID : " + GenerateId + ",Max:" + Details.Count);
            return null;
        }

        if (TempDetailsDataInfo.Index == 0)
            TempDetailsDataInfo = PoolManager.Pool.GetPool("TempDetailsData");

        TempDetailsData TempData = null;

        int DetailCount = 0, i, j;
        DetailInfo Info = Details[GenerateId];
        byte Id = 0;
        TerrainInformation Information = DData.Information;

        PropertiesMeshDatas CData = null;
        int MaterialId = 0;

        int LocalX = Information.Chunks[ChunkId].LocalX;
        int LocalY = Information.Chunks[ChunkId].LocalY;
        int LocalZ = Information.Chunks[ChunkId].LocalZ;

        int Index;
        int ActiveMaterials = DetailMaterials.Count;
        int DetailsCount = Details.Count;

        int EndX = StartX + (int)Resolution * Information.VoxelPerChunk,
            EndY = StartY + (int)Resolution * Information.VoxelPerChunk,
            EndZ = StartZ + (int)Resolution * Information.VoxelPerChunk;

        TempObjects.Objects.ClearFast();
        DData.GetObjects(StartX, StartY, StartZ, EndX, EndY, EndZ, TempObjects.Objects, Info.Relations);

        for (Index = TempObjects.Objects.Count - 1; Index >= 0; --Index)
        {
            Id = TempObjects.Objects.array[Index].Id;
            Info = Details[Id];

            if (CData == null)
            {
                CData = PoolManager.Pool.GetData<PropertiesMeshDatas>(InternalDataName);
                (CData as PropertiesMeshDatas).EraseDatas = true;

                TempData = TempDetailsDataInfo.GetData<TempDetailsData>();
                TempData.ClearMaterials(ActiveMaterials);
                TempData.ClearSubMesh(DetailsCount);
                TempData.ClearObjects(DetailsCount);
            }

            TempData.Objects[Id].Add(TempObjects.Objects.array[Index]);
            ++TempData.ActiveSubMeshs[Id];

            for (j = 0; j < Info.MaterialId.Length; ++j)
            {
                MaterialId = Info.MaterialId[j];

                if (TempData.ActiveMaterials[MaterialId] == 0)
                    ++DetailCount;

                TempData.ActiveMaterials[MaterialId] += Info.Data.CIndices[j].Length;
            }
        }

        if (DetailCount != 0)
        {
            MaterialProperties MatProps = GetProperties(DetailCount, TempData.ActiveMaterials, ActiveMaterials);

            (CData as PropertiesMeshDatas).CMaterials = MatProps.Materials;
            (CData as PropertiesMeshDatas).Properties = MatProps.Properties;
            CData.CIndices = new int[DetailCount][];
            DetailCount = 0;

            for (i = 0; i < ActiveMaterials; ++i)
            {
                if (TempData.ActiveMaterials[i] == 0)
                    continue;

                CData.CIndices[DetailCount] = new int[TempData.ActiveMaterials[i]];
                CData.CMaterials[DetailCount] = DetailMaterials[i];
                TempData.ActiveMaterials[i] = DetailCount;
                ++DetailCount;
            }

            GenerateDetailMesh(Block, ChunkId, DData, CData, TempData, DetailCount, StartX, StartY, StartZ);
            TempData.Close();
        }

        return CData;
    }

    public void GenerateDetailMesh(TerrainBlock Block, int ChunkId, SimpleTypeDatas DData, IMeshData IData,TempDetailsData TempData, int MaterialCount, int StartX, int StartY, int StartZ)
    {
        int VerticeCount = 0, i = 0;
        int DetailsCount = Details.Count;
        bool HasTangents = false, HasNormals =false, HasColor = false;
        DetailInfo Object = null;
        PropertiesMeshDatas Data = IData as PropertiesMeshDatas;

        for (i = 0; i < DetailsCount; ++i)
        {
            if (TempData.ActiveSubMeshs[i] == 0)
                continue;

            Object = Details[i];

            if (VerticeCount + Object.Data.CVertices.Length * TempData.ActiveSubMeshs[i] > 65000)
            {
                Debug.LogError("Details Mesh VerticesCount:" + (VerticeCount + Object.Data.CVertices.Length * TempData.ActiveSubMeshs[i]) + " > 65000 limit reached.");
                TempData.ActiveSubMeshs[i] = (65000 - VerticeCount) / Object.Data.CVertices.Length - 1;
            }

            if (!HasTangents)
                HasTangents = Object.Data.CTangents != null && Object.Data.CTangents.Length != 0;

            if (!HasColor)
                HasColor = Object.Data.CColors != null && Object.Data.CColors.Length != 0;

            if (!HasNormals)
                HasNormals = Object.Data.CNormals != null && Object.Data.CNormals.Length != 0;
            VerticeCount += Object.Data.CVertices.Length * TempData.ActiveSubMeshs[i];
        }

        Vector3 Position = new Vector3();

        byte TopType = 0, CurrentType=0;
        int px = 0, py = 0, pz = 0;

        int Lenght = 0, StartIndice = 0,v = 0;
        int SubMeshId = 0, MaterialId = 0, IndiceId = 0;

        float CurrentVolume = 0, TopVolume = 0, Orientation = 0;

        int[] TempIndices = null;
        int[] CurrentOffset = new int[MaterialCount];
        Data.CVertices = new Vector3[VerticeCount];
        Data.CUVs = new Vector2[VerticeCount];
        Data.CNormals = HasNormals ? new Vector3[VerticeCount] : null;
        Data.CTangents = HasTangents ? new Vector4[VerticeCount] : null;
        Data.CColors = HasColor ? new Color[VerticeCount] : null;

        VerticeCount = 0;

        float S=0, MaxRandomPosition=Block.Information.Scale*0.5f;
        SFastRandom FRandom = new SFastRandom(0);
        Quaternion GroundNormal, Q = Quaternion.Euler(90, 0, 0);
        Vector3 ColorPosition;
        Vector3 BlockPosition = Block.WorldPosition;
        int VerticesLenght = 0;

        SList<BiomeData.SimpleTempObject> Objects;
        for (i = 0; i < DetailsCount; ++i)
        {
            if (TempData.ActiveSubMeshs[i] == 0)
                continue;

            Object = Details[i];
            Objects = TempData.Objects[i];
            VerticesLenght = Object.Data.CVertices.Length;

            for (int Index = Objects.Count-1; Index >= 0; --Index)
            {
                px = Objects.array[Index].x;
                py = Objects.array[Index].y;
                pz = Objects.array[Index].z;

                Position.x = (float)(px-StartX) * DData.Information.Scale;
                Position.y = (float)(py-StartY) * DData.Information.Scale;
                Position.z = (float)(pz-StartZ) * DData.Information.Scale;

                if (Object.NoiseScaleModule == null)
                    S = Object.GetScaleF(ref FRandom, px, py, pz);
                else
                {
                    S = (float)Object.NoiseScaleModule.GetValue(px + DData.Terrain.Position.x, py + DData.Terrain.Position.y, pz + DData.Terrain.Position.z);
                    S = Object.GetScaleF(S);
                }

                if (Object.HeightSide == HeightSides.TOP)
                {
                    Position.y -= (Object.TotalHeight) * S;

                    Block.Voxels.GetTypeAndVolumeU(px, py - 1, pz, ref CurrentType, ref CurrentVolume);
                    Block.Voxels.GetTypeAndVolume(px, py, pz, ref TopType, ref TopVolume);

                    if (CurrentType != TopType)
                    {
                        Position.y += 1f - Block.InterpolateHeight(CurrentType, CurrentVolume, TopVolume);// Mathf.Lerp(0, 1, (TerrainInformation.Isolevel - CurrentVolume) / (TopVolume - CurrentVolume)) * DData.Informations.Scale;
                    }

                    Position.y += 0.01f;
                }
                else
                {
                    Block.Voxels.GetTypeAndVolume(px, py, pz, ref CurrentType, ref CurrentVolume);
                    Block.Voxels.GetTypeAndVolumeU(px, py + 1, pz, ref TopType, ref TopVolume);
                    if (CurrentType != TopType && TopVolume > TerrainInformation.Isofix)
                        TopVolume = 0;

                    Position.y += Block.InterpolateHeight(CurrentType, TopVolume, CurrentVolume); //Mathf.Lerp(1, 0, (TerrainInformation.Isolevel - TopVolume) / (CurrentVolume - TopVolume)) * DData.Informations.Scale;
                    Position.y -= 0.01f;
                }

                FRandom.InitSeed((long)(((long)px ^ (long)py ^ (long)pz) * 3.33333));
                Orientation = FRandom.randomIntAbs(360);
                Block.GetPositionOffset(ref Position.x, ref Position.y, ref Position.z);
                FRandom.randomVector3fXZ(ref Position, MaxRandomPosition);

                if (Object.AlignToTerrain)
                {
                    GroundNormal = Quaternion.LookRotation(Block.GetNormal(px, py, pz));

                    for (v = VerticesLenght - 1; v >= 0; --v)
                    {
                        ColorPosition = Data.CVertices[v + VerticeCount] = Position + (GroundNormal * Q * MeshUtils.RotateVertice(Object.Data.CVertices[v] * S, -Orientation));

                        if (Object.GenerateColors)
                        {
                            ColorPosition += BlockPosition;
                            Data.CColors[v + VerticeCount] = Object.Data.CColors[v] * GenerateNoiseColor(Object, ColorPosition.x, ColorPosition.y, ColorPosition.z);
                        }
                    }

                    if (HasNormals)
                    {
                        for (v = VerticesLenght - 1; v >= 0; --v)
                        {
                            Data.CNormals[v + VerticeCount] = GroundNormal * Q * MeshUtils.RotateVertice(Object.Data.CNormals[v] * S, -Orientation);
                        }
                    }

                    if (HasTangents)
                    {
                        for (v = VerticesLenght - 1; v >= 0; --v)
                        {
                            Data.CTangents[v + VerticeCount] = GroundNormal * Q * MeshUtils.RotateVertice(Object.Data.CTangents[v] * S, -Orientation);
                        }
                    }
                }
                else
                {
                    for (v = VerticesLenght - 1; v >= 0; --v)
                    {
                        ColorPosition = Data.CVertices[v + VerticeCount] = Position + (MeshUtils.RotateVertice(Object.Data.CVertices[v] * S, -Orientation));
                        if (Object.GenerateColors)
                        {
                            ColorPosition += BlockPosition;
                            Data.CColors[v + VerticeCount] = Object.Data.CColors[v] * GenerateNoiseColor(Object, ColorPosition.x, ColorPosition.y, ColorPosition.z);
                        }
                    }

                    if (HasNormals)
                    {
                        for (v = VerticesLenght - 1; v >= 0; --v)
                        {
                            Data.CNormals[v + VerticeCount] = MeshUtils.RotateVertice(Object.Data.CNormals[v] * S, -Orientation);
                        }
                    }

                    if (HasTangents)
                    {
                        for (v = VerticesLenght - 1; v >= 0; --v)
                        {
                            Data.CTangents[v + VerticeCount] = MeshUtils.RotateVertice(Object.Data.CTangents[v] * S, -Orientation);
                        }
                    }
                }

                Array.Copy(Object.Data.CUVs, 0, Data.CUVs, VerticeCount, Object.Data.VerticesCount);

                if(!Object.GenerateColors)
                    Array.Copy(Object.Data.CColors, 0, Data.CColors, VerticeCount, Object.Data.VerticesCount);

                for (SubMeshId = 0; SubMeshId < Object.Data.CIndices.Length; ++SubMeshId)
                {
                    MaterialId = Object.MaterialId[SubMeshId];
                    IndiceId = TempData.ActiveMaterials[MaterialId];
                    StartIndice = CurrentOffset[IndiceId];
                    Lenght = Object.Data.CIndices[SubMeshId].Length;
                    TempIndices = Data.CIndices[IndiceId];
                    for (v = 0; v < Lenght; ++v)
                        TempIndices[StartIndice + v] = (ushort)(VerticeCount + Object.Data.CIndices[SubMeshId][v]);

                    CurrentOffset[IndiceId] += Lenght;
                }

                VerticeCount += Object.Data.VerticesCount;
            }
        }
    }

    static public Color GenerateNoiseColor(DetailInfo GInfo, float x, float y, float z)
    {
        float Noise = (float)SimplexNoise.Noise(x * GInfo.NoiseColorScale, y * GInfo.NoiseColorScale, z * GInfo.NoiseColorScale);

        if (Noise < 0)
        {
            return Color.Lerp(GInfo.NoiseColorB, GInfo.NoiseColorA, -Noise);
        }

        return Color.Lerp(GInfo.NoiseColorB, GInfo.NoiseColorC, Noise);
    }

    public override void ApplyMesh(TerrainBlock Block, int ChunkId, IViewableProcessor.TempMeshDataId TData, int GenerateId)
    {
        string Name = InternalDataName + TData.GenerateId;
        GraphicMesh Mesh = Block.GetChunkMesh(ChunkId, Name);

        if (Mesh == null)
        {
            if (TData.Data != null && TData.Data.IsValid())
            {
                bool CanReceiveShadow = true;
                bool CanCastShadow = true;

                bool CRS = true;
                bool CCS = true;

                if (TData.Data.Materials != null)
                {
                    for (int j = TData.Data.Materials.Length - 1; j >= 0; --j)
                    {
                        if (TData.Data.Materials[j] != null)
                            GetMaterialInfo(TData.Data.Materials[j].GetInstanceID(), ref CanCastShadow, ref CanReceiveShadow);

                        if (!CanCastShadow) CCS = false;
                        if (!CanReceiveShadow) CRS = false;
                    }
                }

                GraphicMeshContainer Container = Block.GetChunkMeshContainer(ChunkId, true);
                Mesh = Container.AddMesh(Name, false);
                Mesh.PoolMaterials = false;
                Mesh.Tag = InternalDataName;
                Mesh.ReceiveShadow = ReceiveShadow && CRS;
                Mesh.CastShadow = CastShadow && CCS;
                Mesh.UseTangents = true;
                Mesh.FadeTime = FadeTime;
                Mesh.FadeStart = ThreadManager.UnityTime;
                Mesh.FadeName = FadeName;
                Mesh.Topology = MeshTopology.Triangles;
                Mesh.Resolution = (byte)Resolution;

                Container.SetMeshActive(Mesh, true);
                Container.UpdateMesh(Mesh, TData.Data);
            }
        }
        else
        {
            if (TData.Data != null)
            {
                if (!TData.Data.IsValid())
                {
                    Block.GetChunkMeshContainer(ChunkId, false).SetMeshActive(Mesh, false);
                    Mesh.CMesh.Clear(false);
                }
                else
                {
                    Block.GetChunkMeshContainer(ChunkId, false).UpdateMesh(Mesh, TData.Data);
                }
            }
            else
            {
                Block.GetChunkMeshContainer(ChunkId, false).SetMeshActive(Mesh, false);
            }
        }
    }

    public override void DesactiveChunk(TerrainBlock Block, int ChunkId)
    {
        base.DesactiveChunk(Block, ChunkId);

        GraphicMeshContainer Container = Block.GetChunkMeshContainer(ChunkId);
        if (Container != null)
            Container.RemoveMeshByTag(InternalDataName);
    }

    public override void OnFlushVoxels(ActionThread Thread, TerrainObject Obj, ref VoxelFlush Voxel, TerrainChunkGenerateGroupAction Action)
    {
        SimpleTypeDatas DData = Voxel.Block.Voxels.GetCustomData<SimpleTypeDatas>(InternalDataName);

        int Id = DData.SetByteIfExist(Voxel.x, Voxel.y, Voxel.z, 0);

        if (Id != 0)
        {
            DetailInfo Info = Details[Id - 1];
            OnFlushEvent(Voxel.Block, DData, Info, Id, 0, Voxel.x, Voxel.y, Voxel.z);

            DData.SetByte(Voxel.x, Voxel.y, Voxel.z, 0);

            AddToFlush(new TerrainChunkRef(Voxel.Block, Voxel.ChunkId), Info.GetObjectId());
        }
    }


    #region CustomData Modification

    public override int GetDataIdByName(string Name)
    {
        return GetInfo(Name).ObjectId;
    }

    public override KeyValuePair<string, int>[] GetDatas()
    {
        KeyValuePair<string, int>[] Kps = new KeyValuePair<string, int>[Details.Count];
        for (int i = 0; i < Details.Count; ++i)
        {
            Kps[i] = new KeyValuePair<string, int>(Details[i].Object.name, i);
        }

        return Kps;
    }

    public override void ApplyPoint(ProcessorVoxelPoint Point)
    {
        SimpleTypeDatas GData = Point.Block.Voxels.GetCustomData<SimpleTypeDatas>(InternalDataName);
        if (GData == null)
            return;

        switch (Point.ModifyType)
        {
            case ProcessorVoxelChangeType.SET:
                byte CurrentDetail = GData.GetByte(Point.x, Point.y, Point.z);
                if (CurrentDetail != Point.Id + 1)
                {
                    OnFlushEvent(Point.Block, GData, CurrentDetail == 0 ? null : Details[CurrentDetail - 1], CurrentDetail, Point.Id + 1, Point.x, Point.y, Point.z);

                    GData.SetByteIntFloat(Point.x, Point.y, Point.z, (byte)(Point.Id + 1), 0, Point.Rotation);
                    TerrainChunkRef Ref = new TerrainChunkRef(Point.Block, Point.ChunkId);
                    ConvertChunkResolution(Point.Block, ref Ref.ChunkId);

                    if (!Chunks.Contains(Ref))
                        Chunks.Add(Ref);
                }

                break;
        };
    }

    #endregion
}
