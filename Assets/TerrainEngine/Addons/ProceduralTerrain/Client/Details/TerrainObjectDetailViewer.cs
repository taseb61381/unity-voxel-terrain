using UnityEngine;

/// <summary>
/// Register current TerrainObject to DetailProcessor (Will generate details around it. Plants, Rocks, etc..)
/// Attach it to any camera.
/// </summary>
[AddComponentMenu("TerrainEngine/TerrainObject/Details Viewer (Generate Details around)")]
public class TerrainObjectDetailViewer : MonoBehaviour 
{
    private bool Inited = true;

    void OnEnable()
    {
        Inited = false;
    }

    void Update()
    {
        if (!Inited)
        {
            TerrainObject TObject = GetComponent<TerrainObject>();

            if (TObject == null)
            {
                Debug.LogError("No TerrainObject on DetailObjectViewer gameObject " + name);
                return;
            }

            if (TerrainManager.Instance == null)
                return;

            foreach (IBlockProcessor Processor in TerrainManager.Instance.Processors)
            {
                if (Processor is DetailProcessor && TObject.AddProcessor(Processor))
                {
                    Debug.Log(name + " Added Processor : " + Processor);
                    Processor.OnObjectMoveChunk(TObject);
                }
            }

            Inited = true;
        }
    }
}
