using TerrainEngine;
using UnityEngine;

public class AdvancedHeightMap : IBlockProcessor 
{
    static public int IntSeed;

    public enum HeightMapResolutions
    {
        SIZE_1 = 1,
        SIZE_2 = 2,
        SIZE_4 = 4,
        SIZE_8 = 8,
    }

    /// <summary>
    /// This will change the generated size of the heightmap. Higher resolution will increase generation speed but decrease terrain details
    /// </summary>
    public HeightMapResolutions Resolution = HeightMapResolutions.SIZE_1; 

    /// <summary>
    /// Offset to apply to the generation position.
    /// </summary>
    public Vector3 WorldOffset = new Vector3(0, 0, 0);

    /// <summary>
    /// Link your object here if you want spawn it on a certain biome
    /// </summary>
    public Transform DesiredPosition;

    /// <summary>
    /// Set the name of the biome that you want your object spawn. The generator will modify the original position of the noise to target the biome desired
    /// </summary>
    public string DesiredBiomeName;

    public BiomeNoisesContainer Container;

    public override void OnStart()
    {
        foreach (NoiseObjects Noise in Container.NoiseTypes.NoisesObjects)
        {
            foreach (NoiseObject Obj in Noise.Objects)
            {
                Obj.GroundType = BlockManager.getBlock(Obj.Name);
            }
        }

        base.OnStart();
    }

    [HideInInspector]
    public HeightMapGenerator Gen;
    public override IWorldGenerator GetGenerator()
    {
        Gen = new HeightMapGenerator();
        Gen.Priority = Priority;
        Gen.Name = InternalName;
        Gen.DataName = DataName;
        Gen.Resolution = (int)Resolution;
        Gen.WorldOffset = WorldOffset;
        Gen.Container = Container.Container;
        Gen.NoiseTypes = Container.NoiseTypes;
        Gen.ExternalContainers = Container.ExternalContainers;
        if (DesiredPosition != null && DesiredBiomeName != null && DesiredBiomeName.Length != 0)
        {
            Gen.DesiredPosition = DesiredPosition.transform.position;
            Gen.DesiredBiomeName = DesiredBiomeName;
        }
        else
        {
            Gen.DesiredPosition = new Vector3();
            Gen.DesiredBiomeName = null;
        }

        // V11
        Gen.HeightNoises = Container.Noises;
        return Gen;
    }
}
