﻿using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

[ExecuteInEditMode]
public class AdvancedHeighMapPreview : MonoBehaviour 
{
	[Serializable]
	public class BiomeColor
	{
		public BiomeColor()
		{

		}

		public BiomeColor(string Name)
		{
			this.BiomeName = Name;
		}

		public string BiomeName;
		public Color Color;

		[NonSerialized]
		public int BiomeNameHash;
	}

	public AdvancedHeightMap HeightMap;
	public Vector2 RectSize = new Vector2(100, 100);
	public float RectScale = 2;
	public Transform ToFollow;
	public float PreviewScale = 1;
	public List<BiomeColor> Colors = new List<BiomeColor>();

	public Texture2D PreviewTexture;
	private BiomeData Biome;
	private Vector3 Position;
	private Vector3 LastPosition;
	private float LastScale = 0;
	private float[,] Heights;
	public float DistanceBeforeUpdate = 0;
	public string ErrorMessage="";
    public float MinScale = 0.1f;
    public float MaxScale = 1000f;

	// Use this for initialization
	void Start () 
	{
		if (Application.isPlaying)
		{
			foreach (BiomeColor Color in Colors)
			{
				Color.Color.a = 1f;
				Color.BiomeNameHash = Color.BiomeName.GetHashCode();
			}
		}

		if (ToFollow == null)
		{
			Debug.LogError(this + " ToFollow field is empty");
			return;
		}
	}

	void OnGUI()
	{
		if (!DebugGUIProcessor.DrawGUI)
			return;

		if (PreviewTexture != null)
		{
			Rect P = new Rect(Screen.width - (RectSize.x * RectScale) - 30, 30, (RectSize.x * RectScale), (RectSize.y * RectScale));
			GUI.DrawTexture(P, PreviewTexture);
            PreviewScale = GUI.HorizontalSlider(new Rect(P.x, P.y + P.height, P.width, 20), PreviewScale, MinScale, MaxScale);
			GUIUtility.RotateAroundPivot(ToFollow.eulerAngles.y - 90, new Vector2(P.x + P.width / 2, P.y + P.height / 2));

			GUI.color = Color.black;
			GUI.Toggle(new Rect(P.x + P.width / 2 - 10, P.y + P.height / 2 - 10, 50, 20), false, "===");
		}
	}

	public Color GetColor(int HashCode)
	{
		for (int i = Colors.Count - 1; i > 0; --i)
		{
			if (Colors[i].BiomeNameHash == HashCode)
				return Colors[i].Color;
		}

		return Color.blue;
	}

    public LibNoise.IModule Noises;
    public VoxelTypeOuput VoxelOutput;

	public void UpdateTexture()
	{
		if (HeightMap == null)
		{
			ErrorMessage = "No HeightMap";
			return;
		}

		if (HeightMap.Gen == null)
		{
			ErrorMessage = "No Generator on HeightMap";
			return;
		}

		if (HeightMap.Gen.GlobalNoises == null)
		{
			ErrorMessage = "No GlobalNoise on HeightMap Generator";
			return;
		}

		if (PreviewTexture == null)
		{
			PreviewTexture = new Texture2D((int)RectSize.x, (int)RectSize.y);
			Heights = new float[(int)RectSize.x, (int)RectSize.y];
			Biome = new BiomeData();
		}

        if (Noises == null)
        {
            HeightMap.Gen.GetMainNoises(ref Noises, ref VoxelOutput);
        }

		DistanceBeforeUpdate = (PreviewTexture.width * PreviewScale) / (3 + PreviewScale*3);
		if (DistanceBeforeUpdate < 30)
			DistanceBeforeUpdate = 30;

		LastPosition = Position;
		int x, z, i;
		double Height;
		float MinHeight = float.MaxValue, MaxHeight = float.MinValue;
		bool Ok = false;

		Vector3 P = new Vector3();
		for (x = 0; x < PreviewTexture.width; ++x)
		{
			P.x = ((Position.x - (PreviewTexture.width / 2) * PreviewScale) + x * PreviewScale);

			for (z = 0; z < PreviewTexture.height; ++z)
			{
				P.z = ((Position.z - (PreviewTexture.height / 2) * PreviewScale) + z * PreviewScale);

				Biome.Start();
                Height = Noises.GetValue(P.x, P.z, Biome);

				Heights[x, z] = (float)Height;
				if (Height < MinHeight) MinHeight = (float)Height;
				if (Height > MaxHeight) MaxHeight = (float)Height;

				Biome.End();

				Ok = false;

				if (Height <= 0)
				{
					PreviewTexture.SetPixel(x, z, Color.cyan);
					Ok = true;
				}
				else
				{
					for (i = Colors.Count - 1; i >= 0; --i)
					{
						if (Biome.Contains(Colors[i].BiomeNameHash))
						{
							PreviewTexture.SetPixel(x, z, Colors[i].Color);
							Ok = true;
							break;
						}
					}
				}

				if (!Ok)
					PreviewTexture.SetPixel(x, z, Color.green);

				Biome.CleanNoises();
			}
		}

		Color Col = new Color();
		float Value = 0;
		Col.a = 1;
		for (x = 0; x < PreviewTexture.width; ++x)
		{
			for (z = 0; z < PreviewTexture.height; ++z)
			{
				Value = (Heights[x, z] + MinHeight) / (MaxHeight + MinHeight);
				Col.r = Col.g = Col.b = 0.5f + (Value*0.5f);
				PreviewTexture.SetPixel(x,z,PreviewTexture.GetPixel(x,z) * Col);
			}
		}

		PreviewTexture.Apply();
	}

	// Update is called once per frame
    private List<string> Biomes = new List<string>();
	void Update() 
	{
		if (ToFollow == null)
			return;

		if (Application.isPlaying)
		{
			if (!DebugGUIProcessor.DrawGUI)
				return;

			if (TerrainManager.Instance == null || !TerrainManager.Instance.isInited)
				return;

			if (HeightMap == null)
				return;

			Position = ToFollow.position / TerrainManager.Instance.Informations.Scale;

			if (PreviewScale != LastScale || PreviewTexture == null)
			{
				LastScale = PreviewScale;
				UpdateTexture();
			}
			else if (PreviewTexture != null && Vector3.Distance(LastPosition, Position) > DistanceBeforeUpdate)
				UpdateTexture();
		}
		else
		{
			if (HeightMap == null)
			{
				HeightMap = FindObjectOfType<AdvancedHeightMap>();
				if (HeightMap == null)
					return;
			}

			if (Colors == null)
				Colors = new List<BiomeColor>();

			Biomes.Clear();
			HeightMap.Container.GetBiomes(HeightMap.Container.ExternalContainers, Biomes);

            for (int i = Colors.Count-1; i >= 0; --i)
                if (!Biomes.Contains(Colors[i].BiomeName))
                    Colors.RemoveAt(i);

            if (Colors.Count == 0 || Colors.Count != Biomes.Count)
            {
                    foreach (var BiomeName in Biomes)
                    {
                        if (Colors.Find(info => info.BiomeName == BiomeName) == null)
                            Colors.Add(new BiomeColor(BiomeName));
                    }
            }
		}
	}
}
