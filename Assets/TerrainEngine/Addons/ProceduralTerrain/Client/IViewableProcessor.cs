﻿using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class ViewableUpdateObject : IAction
{
    public IViewableProcessor Processor;
    public TerrainBlock Block;
    public VectorI3 CurrentLocalChunk;
    public Vector3 Position;

    public void Init(IViewableProcessor Processor, TerrainBlock Block, VectorI3 CurrentLocalChunk, Vector3 Position)
    {
        this.Processor = Processor;
        this.Block = Block;
        this.CurrentLocalChunk = CurrentLocalChunk;
        this.Position = Position;
    }

    public override bool Execute(ActionThread Thread)
    {
        TerrainChunk.GetChunksIteration(Block, CurrentLocalChunk.x, CurrentLocalChunk.y, CurrentLocalChunk.z, Position, Processor.DrawChunkCount, Processor.ActiveChunkDelegate, false, (int)Processor.Resolution);
        Processor.UpdateActiveChunks();
        Close();
        return true;
    }

    public override void Clear()
    {
        Processor = null;
        Block = null;
        base.Clear();
    }
}

public class IViewableProcessor : IBlockProcessor
{
    [Serializable]
    public enum MeshResolutions : byte
    {
        RESOLUTION_1 = 1,
        RESOLUTION_2 = 2,
        RESOLUTION_4 = 4,
        RESOLUTION_8 = 8,
        RESOLUTION_16 = 16,
    };

    public class TempMeshDataId : IPoolable
    {
        public TempMeshDataId()
        {

        }

        public TempMeshDataId(int GenerateId, IMeshData Data)
        {
            this.GenerateId = GenerateId;
            this.Data = Data;
        }

        public TerrainChunkRef ChunkRef;
        public int GenerateId;
        public IMeshData Data;

        public override void Clear()
        {
            if (Data != null)
                Data.Close();
            Data = null;
        }
    }

    public struct TempActiveChunk
    {
        public TerrainChunkRef ChunkRef;
        public Vector3 WorldPosition; // Chunk position in world

        public TempActiveChunk(ushort BlockId, int ChunkId, VectorI3 WorldPosition)
        {
            this.ChunkRef = new TerrainChunkRef(BlockId,ChunkId);
            this.WorldPosition = WorldPosition;
        }
    }

    public float ViewDistance = 100;

    public int DrawChunkCount = 3;
    public int RemoveChunkCount
    {
        get
        {
            return DrawChunkCount + 1;
        }
    }
    public int ActiveChunksCount = 0;
    public bool GenerateAllChunks = false;
    public VoxelSizes ChunkSize = VoxelSizes.SIZE_16; // This Value must be higher than ChunkSize of Terrain (TerrainInformation)
    public float FadeTime = 3f; // Time for chunks to appear
    public string FadeName = "_Fade"; //  Material Property name to use for apply fading (_Color,_Cutoff,_Alpha,etc)

    internal bool ActiveChunksDirty = false;
    internal FList<TempActiveChunk> ActiveChunks; // Generated and active chunks
    internal FastDictionary<TerrainChunkRef, List<int>> FlushedChunks;
    internal PoolInfoRef ViewableGenerateChunkPool;
    internal PoolInfoRef ViewableUpdateObjectPool;

    [HideInInspector]
    public MeshResolutions Resolution = MeshResolutions.RESOLUTION_1;

    #region IBlockProcessor

    public override void OnInitManager(TerrainManager Manager)
    {
        ActiveChunks = new FList<TempActiveChunk>(50);
        FlushedChunks = new FastDictionary<TerrainChunkRef, List<int>>(50);
        ViewableGenerateChunkPool = PoolManager.Pool.GetPool("ViewableGenerateChunk");
        ViewableUpdateObjectPool = PoolManager.Pool.GetPool("ViewableUpdateObjectPool");
        if (ChunkSize < Manager.Informations.VoxelPerChunkXYZ)
        {
            Debug.LogWarning(this + ",ChunkSize must be higher than Terrain VoxelPerChunkXYZ. ChunkSize set to " + Manager.Informations.VoxelPerChunkXYZ);
            ChunkSize = Manager.Informations.VoxelPerChunkXYZ;
        }
        
        Resolution = (MeshResolutions)((int)ChunkSize / (int)Manager.Informations.VoxelPerChunkXYZ);

        if(!Enum.IsDefined(typeof(MeshResolutions), Resolution))
            Resolution = MeshResolutions.RESOLUTION_1;

        DrawChunkCount = (int)(ViewDistance / TerrainManager.Instance.Informations.MaxChunkSize);
        if (ViewDistance > 0.5f && DrawChunkCount == 0)
            DrawChunkCount = 1;

        InterlacedUpdate = false;
        EnableUnityUpdateFunction = true;

        base.OnInitManager(Manager);
    }

    public override void OnObjectMoveChunk(TerrainObject Obj)
    {
        if (ITerrainCameraHandler.Instance == null || !ITerrainCameraHandler.Instance.IsUsingCamera())
            return;

        if (GenerateAllChunks)
            return;

        if (!Obj.HasBlock || !Obj.ChunkRef.IsValid())
            return;

        VectorI3 LocalChunkPosition = Obj.CurrentLocalChunk;
        ConvertLocalChunkResolution(ref LocalChunkPosition);

        ViewableUpdateObject Action = ViewableUpdateObjectPool.GetData<ViewableUpdateObject>();
        Action.Init(this, Obj.BlockObject, LocalChunkPosition, Obj.Position);
        ActionProcessor.AddFast(Action);
    }

    public override void OnFirstChunkBuild(TerrainBlock Block, int ChunkId, IMeshData Data)
    {
        ActiveChunk(Block, ChunkId, true);
    }

    public override void OnTerrainThreadClose(TerrainBlock Terrain)
    {
        base.OnTerrainThreadClose(Terrain);
        lock (ActiveChunks.array)
        {
            for (int i = ActiveChunks.Count - 1; i >= 0; --i)
            {
                if (ActiveChunks.array[i].ChunkRef.BlockId == Terrain.BlockId)
                {
                    ActiveChunks.RemoveAt(i);
                }
            }
        }
    }

    #endregion

    #region ActiveChunks

    /// <summary>
    /// Return true if chunk is active (checked and generated)
    /// </summary>
    public bool Contains(ushort BlockId, int ChunkId)
    {
        for (int i = ActiveChunks.Count - 1; i >= 0; --i)
        {
            if (ActiveChunks.array[i].ChunkRef.ChunkId == ChunkId && ActiveChunks.array[i].ChunkRef.BlockId == BlockId)
                return true;
        }

        return false;
    }

    /// <summary>
    /// Check and replace ChunkId depending of resolution
    /// 0 1 2 3 4 X
    /// 1
    /// 2
    /// 3
    /// 4
    /// Y
    /// Chunk 1,1 at resolution 1 will return ChunkId 1,1
    /// Chunk 1,1 at resolution 2 will return ChunkId 0,0
    /// Chunk 3,3 at resolution 2 will return ChunkId 2,2
    /// Chunk 3,3 at resolution 4 will return ChunkId 0;0
    /// </summary>
    public void ConvertChunkResolution(TerrainBlock Block, ref int ChunkId)
    {
        if (Resolution == MeshResolutions.RESOLUTION_1)
            return;

        int LocalX = TerrainManager.Instance.Informations.Chunks[ChunkId].LocalX;
        int LocalY = TerrainManager.Instance.Informations.Chunks[ChunkId].LocalY;
        int LocalZ = TerrainManager.Instance.Informations.Chunks[ChunkId].LocalZ;

        LocalX = LocalX - (LocalX % (int)Resolution);
        LocalY = LocalY - (LocalY % (int)Resolution);
        LocalZ = LocalZ - (LocalZ % (int)Resolution);

        ChunkId = LocalX * TerrainManager.Instance.Informations.ChunkPerBlock * TerrainManager.Instance.Informations.ChunkPerBlock + LocalY * TerrainManager.Instance.Informations.ChunkPerBlock + LocalZ;
    }

    public void ConvertChunkResolution(ref VectorI3 ChunkPosition)
    {
        if ((int)Resolution == 1)
            return;

        TerrainInformation Info = TerrainManager.Instance.Informations;

        int StartX = (ChunkPosition.x / Info.ChunkPerBlock) * Info.ChunkPerBlock;
        int StartY = (ChunkPosition.y / Info.ChunkPerBlock) * Info.ChunkPerBlock;
        int StartZ = (ChunkPosition.z / Info.ChunkPerBlock) * Info.ChunkPerBlock;

        int LocalX = ChunkPosition.x - StartX;
        int LocalY = ChunkPosition.y - StartY;
        int LocalZ = ChunkPosition.z - StartZ;

        LocalX = LocalX - (LocalX % (int)Resolution);
        LocalY = LocalY - (LocalY % (int)Resolution);
        LocalZ = LocalZ - (LocalZ % (int)Resolution);

        ChunkPosition.x = LocalX + StartX;
        ChunkPosition.y = LocalY + StartY;
        ChunkPosition.z = LocalZ + StartZ;
    }

    public void ConvertLocalChunkResolution(ref VectorI3 ChunkPosition)
    {
        ChunkPosition.x = ChunkPosition.x - (ChunkPosition.x % (int)Resolution);
        ChunkPosition.y = ChunkPosition.y - (ChunkPosition.y % (int)Resolution);
        ChunkPosition.z = ChunkPosition.z - (ChunkPosition.z % (int)Resolution);
    }

    /// <summary>
    /// Return true if ChunkPosition is inside view range
    /// </summary>
    public bool IsOnRange(Vector3 ChunkWorldPosition, int MaxRange)
    {
        TerrainObject Near = null;
        if (!GetNearObject(ref Near, ChunkWorldPosition))
            return false;

        VectorI3 ViewChunkPosition = TerrainManager.Instance.Informations.GetChunkGlobalPosition(Near.Position);
        ConvertChunkResolution(ref ViewChunkPosition);

        VectorI3 ChunkPosition = TerrainManager.Instance.Informations.GetChunkGlobalPosition(ChunkWorldPosition);

        int Distance = Math.Abs(ViewChunkPosition.x - ChunkPosition.x);
        if (Distance > MaxRange)
            return false;

        Distance = Math.Abs(ViewChunkPosition.y - ChunkPosition.y);
        if (Distance > MaxRange)
            return false;

        Distance = Math.Abs(ViewChunkPosition.z - ChunkPosition.z);
        if (Distance > MaxRange)
            return false;

        return true;
    }


    /// <summary>
    /// Check if chunk is inside view range and call ActiveChunk(); Use resolution
    /// </summary>
    public void ActiveChunkDelegate(TerrainBlock Block, int ChunkId)
    {
        ActiveChunk(Block, ChunkId, true);
    }

    /// <summary>
    /// Check and Remove all chunks not inside visible range
    /// </summary>
    public void UpdateActiveChunks()
    {
        if (!ActiveChunksDirty || GenerateAllChunks)
            return;

        ActiveChunksDirty = false;
        TerrainBlock Block = null;

        lock (ActiveChunks.array)
        {
            for (int i = 0; i < ActiveChunks.Count; )
            {
                if (!IsOnRange(ActiveChunks.array[i].WorldPosition,RemoveChunkCount))
                {
                    ActiveChunksDirty = true;
                    if (ActiveChunks.array[i].ChunkRef.IsValid(ref Block))
                        DesactiveChunk(Block, ActiveChunks.array[i].ChunkRef.ChunkId);
                    ActiveChunks.RemoveAt(i);
                    continue;
                }

                ++i;
            }
        }

        ActiveChunksCount = ActiveChunks.Count;
    }


    #endregion

    /// <summary>
    /// Check if chunk is in view distance and active it. If CheckExist = true, will only add chunk to generate if chunk is not already active. Set false if you want regenerate a chunk
    /// </summary>
    public void ActiveChunk(TerrainBlock Block,int ChunkId, bool CheckExist)
    {
        if (!ITerrainCameraHandler.Instance.IsUsingCamera())
            return;

        ConvertChunkResolution(Block, ref ChunkId);

        Vector3 WorldPosition = Block.GetChunkWorldPosition(ChunkId);
        if (!IsOnRange(WorldPosition, DrawChunkCount))
        {
            return;
        }

        lock (ActiveChunks.array)
        {
            if (CheckExist)
            {
                if (!Contains(Block.BlockId, ChunkId))
                {
                    ActiveChunksDirty = true;
                    ActiveChunks.Add(new TempActiveChunk(Block.BlockId, ChunkId, WorldPosition));
                    TerrainChunkRef ChunkRef = new TerrainChunkRef(Block, ChunkId);
                    ActiveChunk(ChunkRef, 0);
                }
            }
            else
            {
                if (!Contains(Block.BlockId, ChunkId))
                {
                    ActiveChunksDirty = true;
                    ActiveChunks.Add(new TempActiveChunk(Block.BlockId, ChunkId, WorldPosition));
                }

                TerrainChunkRef ChunkRef = new TerrainChunkRef(Block, ChunkId);
                ActiveChunk(ChunkRef, 0);
            }
        }
    }

    /// <summary>
    /// Add chunk to the Generate list. GenerateMesh and ApplyMesh will be called on another thread
    /// </summary>
    private void ActiveChunk(TerrainChunkRef ChunkRef, int GenerateId, bool Force=false)
    {
        if (!ITerrainCameraHandler.Instance.IsUsingCamera())
            return;

        ActiveChunksDirty = true;
        ViewableGenerateChunk GenerateAction = ViewableGenerateChunkPool.GetData<ViewableGenerateChunk>();
        GenerateAction.Processor = this;
        GenerateAction.ChunkRef = ChunkRef;
        GenerateAction.GenerateId = GenerateId;
        ActionProcessor.AddToThread(GenerateAction);
    }

    public override bool OnTerrainUnityUpdate(ActionThread Thread, TerrainBlock Terrain)
    {
        lock (Datas)
        {
            while (Datas.Count != 0)
            {
                TerrainBlock Block = null;
                TempMeshDataId Tmp = Datas.Pop();
                if (Tmp.ChunkRef.IsValid(ref Block))
                {
                    ApplyMesh(Block, Tmp.ChunkRef.ChunkId, Tmp, Tmp.GenerateId);
                }
                Tmp.Close();
            }
        }
        return true;
    }

    /// <summary>
    /// Function to call for disable a chunk. Must be overrided by your function that disable chunk system
    /// </summary>
    public virtual void DesactiveChunk(TerrainBlock Block, int ChunkId)
    {

    }

    /// <summary>
    /// This function is called by an other Thread. MDatas contains multiple mesh data generated
    /// </summary>
    public virtual void GenerateMesh(ActionThread Thread, TerrainBlock Block, int ChunkId, Stack<TempMeshDataId> MDatas, int GenerateId)
    {

    }

    /// <summary>
    /// This function is called by Unity thread. Used to apply all Meshes generated on Datas
    /// </summary>
    public virtual void ApplyMesh(TerrainBlock Block, int ChunkId, TempMeshDataId Data, int GenerateId)
    {

    }

    public List<TerrainChunkRef> Chunks = new List<TerrainChunkRef>();
    public Stack<IViewableProcessor.TempMeshDataId> Datas = new Stack<TempMeshDataId>();

    public virtual void AddToFlush(TerrainChunkRef Ref, int GenerateId)
    {
        List<int> L = null;

        lock (FlushedChunks)
        {
            if (!FlushedChunks.TryGetValue(Ref, out L))
            {
                L = PoolManager.UPool.GetList();
                FlushedChunks.Add(Ref, L);
            }
        }


        if (!L.Contains(GenerateId))
            L.Add(GenerateId);
    }

    public override IAction OnVoxelFlushDone(ActionThread Thread, TerrainChunkGenerateGroupAction Action)
    {
        lock (FlushedChunks)
        {
            TerrainBlock Block = null;
            TerrainChunkRef ChunkRef;
            foreach (KeyValuePair<TerrainChunkRef, List<int>> Kp in FlushedChunks)
            {
                if (!Kp.Key.IsValid(ref Block))
                    continue;

                ChunkRef = Kp.Key;

                ConvertChunkResolution(Block, ref ChunkRef.ChunkId);

                if (IsOnRange(Block.GetChunkWorldPosition(ChunkRef.ChunkId), DrawChunkCount))
                {
                    for (int i = Kp.Value.Count - 1; i >= 0; --i)
                        ActiveChunk(ChunkRef, Kp.Value[i]);
                }
                PoolManager.UPool.CloseList(Kp.Value);
            }

            FlushedChunks.Clear();
        }

        if (Chunks.Count != 0)
        {
            for (int i = Chunks.Count-1; i>=0; --i)
            {
                ActiveChunk(Chunks[i], 0);
            }

            Chunks.Clear();

            return null;
        }

        return null;
    }
}
