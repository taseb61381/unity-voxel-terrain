using System;
using System.Collections;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Processors/GameObjects")]
public class GameObjectProcessor : IBlockProcessor
{
    static public GameObjectProcessor ProcessorGeneratingLod;

    public Vector2 Density; // Chance / Chance Max Density. Example : 10/1000. 10 Objects every 1000 voxels.
    public GameObjectsContainer Container; // Reference to objects containing all GameObjectsInfo and Noise
    public List<GameObjectInfo> Objects
    {
        get
        {
            return Container.Objects;
        }
    }
    public string LODFileFolder = "LOD\\";
    public bool CombineLODPlane = false;
    public Shader CombineLODShader;

    private bool IsGeneratingLod = false;
    private bool LodGenerationDone = false;
    private bool AddedToProgress = false;

    private string ClientDataName;
    private string FileFolder;
    private bool ObjectsInited = false;

    void Awake()
    {
        EnableGenerationFunction = true;
    }

    public GameObjectInfo GetInfo(string Name)
    {
        if (Container != null)
            return Container.GetInfo(Name);
        else
            return null;
    }

    public void Start()
    {
        FileFolder = System.IO.Path.Combine(Environment.CurrentDirectory, LODFileFolder);
    }

    public override void OnInitManager(TerrainManager Manager)
    {
        ClientDataName = "GO-Client-" + DataName;
        InternalDataName = "GO-" + DataName;

        base.OnInitManager(Manager);
    }

    public override bool CanStart()
    {
        EnableThreadUpdateFunction = true;
        EnableUnityUpdateFunction = true;

        if (Container == null)
        {
            Debug.LogError("No objects in GameObjectProcessor.");
            enabled = false;
            return false;
        }

        if (!ObjectsInited)
        {
            ObjectsInited = true;
            for (int i = 0; i < Objects.Count; ++i)
            {
                Objects[i].ObjectId = i;
                Objects[i].RandomScale.UseXOnly = true;
                Objects[i].RandomOrientation.Init();
                Objects[i].RandomScale.Init();
            }
        }

        if (!AddedToProgress)
        {
            AddedToProgress = true;
            TerrainManager.Instance.SubProgress.Name = "Generating LOD";
            foreach (GameObjectInfo Info in Objects)
                TerrainManager.Instance.SubProgress.SetProgress(Info.Name, 0, ThreadManager.UnityTime);
        }

        if (Objects.Count == 0 || Objects.Count >= 255)
        {
            Debug.LogError(name + " GameObjects must have 255 gameobjects max");
            return false;
        }

        if (IsGeneratingLod && !LodGenerationDone)
            return false;
        else if (IsGeneratingLod && LodGenerationDone)
        {
            if (ProcessorGeneratingLod == this)
                ProcessorGeneratingLod = null;

            return true;
        }

        if (ProcessorGeneratingLod != null && ProcessorGeneratingLod != this)
            return false;

        ProcessorGeneratingLod = this;
        IsGeneratingLod = true;
        LodGenerationDone = false;
        StartCoroutine(GenerateScriptLOD());
        return false;
    }

    public override void OnBlockDataCreate(TerrainDatas Data)
    {
        GameObjectClientDatas CData = Data.GetCustomData<GameObjectClientDatas>(ClientDataName);
        if (CData == null)
        {
            CData = new GameObjectClientDatas();
            Data.RegisterCustomData(ClientDataName, CData);
        }

        GameObjectDatas GData = Data.GetCustomData<GameObjectDatas>(InternalDataName);
        if (GData == null)
        {
            GData = new GameObjectDatas();
            Data.RegisterCustomData(InternalDataName, GData);
        }

        CData.Processor = this;
        CData.Data = GData;
    }

    public override IWorldGenerator GetGenerator()
    {
        GameObjectGenerator Gen = new GameObjectGenerator();
        Gen.Priority = Priority;
        Gen.Name = InternalName;
        Gen.DataName = "GO-" + DataName;
        Gen.Density = Density;
        Gen.GameObjects = Objects;
        return Gen;
    }

    public override void OnChunksGenerationDone(TerrainBlock Block)
    {
        GameObjectClientDatas CData = Block.Voxels.GetCustomData<GameObjectClientDatas>(ClientDataName);
        GameObjectDatas GData = CData.Data;

        GameObjectSpawnInfo SPI = new GameObjectSpawnInfo();
        Quaternion Q = Quaternion.Euler(90, 0, 0);
        Quaternion GroundNormal;
        SFastRandom FRandom = new SFastRandom();

        GameObjectDatas.LocalObjectPositionId Spawn;
        GameObjectInfo Info;

        int CX, CY, CZ, i = 0, Index = 0;
        for (CX = 0; CX < GameObjectDatas.ChunkPerBlock; ++CX)
        {
            for (CY = 0; CY < GameObjectDatas.ChunkPerBlock; ++CY)
            {
                for (CZ = 0; CZ < GameObjectDatas.ChunkPerBlock; ++CZ,++Index)
                {
                    if (GData.Chunks[Index] == null) continue;

                    for (i = GData.Chunks[Index].Count - 1; i >= 0; --i)
                    {
                        Spawn = GData.Chunks[Index].array[i];
                        if (Spawn.Id == 0)
                            continue;

                        Info = Objects[Spawn.Id - 1];

                        SPI.InfoId = (byte)(Spawn.Id - 1);
                        SPI.x = (ushort)(Spawn.x + CX * GameObjectDatas.VoxelPerChunk);
                        SPI.y = (ushort)(Spawn.y + CY * GameObjectDatas.VoxelPerChunk);
                        SPI.z = (ushort)(Spawn.z + CZ * GameObjectDatas.VoxelPerChunk);
                        SPI.MaxDrawDistance = Info.MaxDrawDistance;

                        if (Info.AlignToTerrain)
                        {
                            FRandom.InitSeed(Spawn.Id);
                            GroundNormal = Quaternion.LookRotation(Block.GetNormal(SPI.x, SPI.y, SPI.z, Info.AlignmentClamp * ((FRandom.randomFloat() + 1f) * 0.5f), false));
                            SPI.Orientation = GroundNormal * Q * Quaternion.Euler(0, GData.ConvertOrientation(Spawn.AngleY), 0);
                        }
                        else
                            SPI.Orientation = Quaternion.Euler(0, GData.ConvertOrientation(Spawn.AngleY), 0);

                        SPI.Scale = GData.ConvertScale(Spawn.Scale);
                        SPI.WorldPosition = Info.HeightSide == HeightSides.BOTTOM ?
                            Block.GetWorldPosition(SPI.x, SPI.y, SPI.z, Info.AutoAlignToGround)
                            :
                            Block.GetTopWorldPosition(SPI.x, SPI.y, SPI.z, Info.AutoAlignToGround);
                        SPI.WorldPosition.y += Info.YOffset;
                        Block.GetPositionOffset(ref SPI.WorldPosition.x, ref SPI.WorldPosition.y, ref SPI.WorldPosition.z);


                        CData.WaitingObjects.AddUnsafe(SPI);
                    }
                }
            }
        }
    }

    /// <summary>
    /// When Terrain is closing, searching all gameobjects instanciated on it and add action to remove them
    /// </summary>
    public override void OnTerrainThreadClose(TerrainBlock Terrain)
    {
        if (Terrain == null)
            return;

        GameObjectClientDatas CData = Terrain.Voxels.GetCustomData<GameObjectClientDatas>(ClientDataName);
        if (CData.Objects.Count == 0)
            return;

        GameObjectPoolRemoveListAction Action = PoolManager.UPool.GameObjectPoolRemoveActionListPool.GetData<GameObjectPoolRemoveListAction>();
        Action.Init(this);
        Action.RemoveFromClientData = false;
        Action.Objects.AddRange(CData.Objects);
        Action.GameObjects.AddRange(CData.GameObjects);

        if (Action.Objects.Count == 0)
        {
            Action.Close();
        }
        else
        {
            ActionProcessor.AddToUnity(Action);
        }

        CData.ClearGameObjects();
    }

    public override void OnFlushVoxels(ActionThread Thread, TerrainObject Obj, ref VoxelFlush Voxel, TerrainChunkGenerateGroupAction Action)
    {
        GameObjectClientDatas CData = Voxel.Block.Voxels.GetCustomData<GameObjectClientDatas>(ClientDataName);
        IGameObjectsScriptsContainer Container = CData.GetScripts(Voxel.x, Voxel.y, Voxel.z);
        if ((Container as object) != null && Container.OnVoxelChange(Voxel))
        {
            CData.SetObject(Voxel.x, Voxel.y, Voxel.z, null, null);
        }
    }

    public override bool OnTerrainThreadUpdate(ActionThread Thread, TerrainBlock Terrain)
    {
        if (ITerrainCameraHandler.Instance.IsUsingCamera() && !ITerrainCameraHandler.Instance.HasCamera)
            return true;        
        GameObjectClientDatas Data = Terrain.Voxels.GetCustomData<GameObjectClientDatas>(ClientDataName);
        Data.UpdateThread(Thread);
        return true;
    }

    public override bool OnTerrainUnityUpdate(ActionThread Thread, TerrainBlock Terrain)
    {
        GameObjectClientDatas Data = Terrain.Voxels.GetCustomData<GameObjectClientDatas>(ClientDataName);
        Data.UpdateUnity(Thread, Terrain);
        return true;
    }

    #region LOD Update

    static public bool UseCellSystem = true;

    static private System.Diagnostics.Stopwatch Watch = new System.Diagnostics.Stopwatch();
    static public List<GraphicLODMesh> DirtyLODContainer = new List<GraphicLODMesh>();
    static public Queue<GraphicLODMesh> DirtyLODQueue = new Queue<GraphicLODMesh>();
    static private int LastFrameUpdate = 0;
    static public long ElapsedMS = 0;

    static public string GetStats()
    {
        if (UseCellSystem)
            return "Data:" + Cells.Count + "Cells:"+GetCellsCount()+",Swi:" + GetCellsObjectsCount() + ",Time:" + ElapsedMS;
        else
            return "Swi:" + Switchers.Count + ",Time:" + ElapsedMS;
    }

    /// <summary>
    /// Modify this value to allow more time to fading or increase the FPS
    /// </summary>
    static public readonly int MaxUpdateTime = 3;

    void Update()
    {
        if (ITerrainCameraHandler.Instance == null)
            return;

        if (LastFrameUpdate != Time.frameCount && (ITerrainCameraHandler.Instance.HasCamera || !ITerrainCameraHandler.Instance.IsUsingCamera()))
        {
            LastFrameUpdate = Time.frameCount;
            Watch.Start();

            for (int i = DirtyLODContainer.Count - 1; i >= 0; --i)
                DirtyLODQueue.Enqueue(DirtyLODContainer[i]);
            DirtyLODContainer.Clear();

            GraphicLODMesh M = null;
            while (DirtyLODQueue.Count != 0 && Watch.ElapsedMilliseconds < MaxUpdateTime)
            {
                M = DirtyLODQueue.Dequeue();
                M.Dirty = true;
                M.Enabled = true;
                if (!M.UpdateProceduralMesh())
                {
                    DirtyLODContainer.Add(M);
                }
            }

            if (UseCellSystem)
                UpdateCells();
            else
                UpdateSwitchers();

            ElapsedMS = Watch.ElapsedMilliseconds;
            Watch.Stop();
            Watch.Reset();
        }
    }

    #region SwitchersSystem

    static private List<GS_LODSwitcher> Switchers = new List<GS_LODSwitcher>();

    static public int SwitchersCount
    {
        get
        {
            return Switchers.Count;
        }
    }

    static private int LastLODId = 0;

    static public void UpdateSwitchers()
    {
        int Count = Switchers.Count;
        int LastId = LastLODId;
        int Start = LastLODId;

        for (; LastId < Count && Watch.ElapsedMilliseconds < MaxUpdateTime; ++LastId)
        {
            Switchers[LastId].UnityUpdate();
        }

        for (LastId = 0; LastId < Start && LastId < Count && Watch.ElapsedMilliseconds < MaxUpdateTime; ++LastId)
        {
            Switchers[LastId].UnityUpdate();
        }

        if (LastId >= Count)
            LastId = 0;

        LastLODId = LastId;
    }

    static public void AddLodSwitcher(GS_LODSwitcher Switcher, GameObjectClientDatas.ClientGameObjectSpawn Spawn)
    {
        if(!UseCellSystem)
            Switchers.Add(Switcher);
        else
        {
            Switcher.Container.GetClientData().AddSwitcher(Spawn);
        }
    }

    static public void RemoveLodSwitcher(GS_LODSwitcher Switcher, GameObjectClientDatas.ClientGameObjectSpawn Spawn)
    {
        if(!UseCellSystem)
            Switchers.Remove(Switcher);
        else
        {
            GameObjectClientDatas Data = Switcher.Container.GetClientData();
            if(Data != null)
                Switcher.Container.GetClientData().RemoveSwitcher(Spawn);
        }
    }

    #endregion

    #region CellSystem

    static public int LastCellIndex = 0;
    static public FList<GameObjectClientDatas> Cells = new FList<GameObjectClientDatas>(1);
    static public int GetCellsObjectsCount()
    {
        int Count = 0;
        for (int i = Cells.Count - 1; i >= 0; --i)
            Count += Cells.array[i].GetCellObjectsCount();
        return Count;
    }

    static public int GetCellsCount()
    {
        int Count = 0;
        for (int i = Cells.Count - 1; i >= 0; --i)
            Count += Cells.array[i].Cells.Count;
        return Count;
    }

    public void UpdateCells()
    {
        int Count = Cells.Count;
        int LastId = LastCellIndex;
        int Start = LastId;

        for (; LastId < Count; ++LastId)
        {
            Cells.array[LastId].UpdateCells(ActionProcessor.Instance.UnityThread, Watch,MaxUpdateTime);
            if (Watch.ElapsedMilliseconds >= MaxUpdateTime)
                break;
        }

        for (LastId = 0; LastId < Start && LastId < Count; ++LastId)
        {
            Cells.array[LastId].UpdateCells(ActionProcessor.Instance.UnityThread, Watch, MaxUpdateTime);
            if (Watch.ElapsedMilliseconds >= MaxUpdateTime)
                break;
        }

        if (LastId >= Count)
        {
            LastId = 0;
        }
        LastCellIndex = LastId;
    }

    public override void OnChunksStartBuilding(TerrainBlock Terrain)
    {
        if (!UseCellSystem)
            return;

        GameObjectClientDatas CData = Terrain.Voxels.GetCustomData<GameObjectClientDatas>(ClientDataName);
        if (CData != null)
        {
            Cells.Add(CData);
        }
    }

    public override void OnTerrainUnityClose(TerrainBlock Terrain)
    {
        if (!UseCellSystem)
            return;

        GameObjectClientDatas CData = Terrain.Voxels.GetCustomData<GameObjectClientDatas>(ClientDataName);
        if (CData != null)
        {
            Cells.Remove(CData);
        }
    }

    #endregion

    #endregion

    #region LOD Generation

    [HideInInspector]
    public List<GS_MaterialContainer> MaterialContainers = new List<GS_MaterialContainer>();

    public GS_MaterialContainer GetContainer(MeshRenderer Renderer)
    {
        for(int i=0;i<MaterialContainers.Count;++i)
        {
            if (MaterialContainers[i].Contains(Renderer))
                return MaterialContainers[i];
        }
         

        GS_MaterialContainer M = gameObject.AddComponent<GS_MaterialContainer>();
        M.SetRenderer(Renderer);
        M.Index = MaterialContainers.Count;
        MaterialContainers.Add(M);
        return M;
    }

    public class GameObjectLODInfo
    {
        public GameObjectLODInfo(GameObject Obj, float MinRange, float MaxRange, bool IsLodTexture, bool Billboard, Vector3 ProceduralTextureOffset = new Vector3())
        {
            this.Obj = Obj;
            this.MinRange = MinRange;
            this.MaxRange = MaxRange;
            this.Billboard = Billboard;
            this.IsLodTexture = IsLodTexture;
            this.ProceduralTextureOffset = ProceduralTextureOffset;
            if (Obj != null)
            {
                MeshFilter[] Filters = Obj.transform.GetComponentsInChildren<MeshFilter>();
                if (Filters != null && Filters.Length > 1)
                    UseGameObject = true;
                else
                {
                    CRenderer = Obj.GetComponent<MeshRenderer>();
                    CFilter = Obj.GetComponent<MeshFilter>();

                    if (CRenderer != null && CFilter != null)
                        UseGameObject = false;
                }
            }
        }

        public GameObject Obj;
        public float MinRange;
        public float MaxRange;
        public bool Billboard;
        public bool IsLodTexture;
        public Vector3 ProceduralTextureOffset;

        public MeshRenderer CRenderer;
        public MeshFilter CFilter;
        public bool UseGameObject = false;
    };

    public GS_AddCollider GenerateColliders(GameObject Parent, GameObject Obj)
    {
        GS_AddCollider Script = null;
        if (Obj.GetComponent<CapsuleCollider>() != null)
        {
            CapsuleCollider Collider = Obj.GetComponent<CapsuleCollider>();
            Script = Obj.AddComponent<GS_AddCollider>();
            Script.Center = Collider.center;
            Script.Radius = Collider.radius;
            Script.Height = Collider.height;
            GameObject.Destroy(Collider);
            return Script;
        }

        Obj.SetActive(true);

        MeshFilter Filter = Obj.GetComponent<MeshFilter>();
        MeshRenderer Renderer = Obj.GetComponent<MeshRenderer>();

        if (Filter != null)
        {
            Mesh CMesh = Filter.sharedMesh;
            if (CMesh != null)
            {
                float MinX = int.MaxValue, MaxX = int.MinValue;
                float MinZ = int.MaxValue, MaxZ = int.MinValue;
                float MinY = int.MaxValue, MaxY = int.MinValue;

                int IndiceBark = -1;
                for (int i = 0; i < Renderer.sharedMaterials.Length; ++i)
                {
                    if (Renderer.sharedMaterials[i] != null)
                    {
                        if (Renderer.sharedMaterials[i].shader.name.ToLower().Contains("bark") || Renderer.sharedMaterials[i].name.ToLower().Contains("bark") || Renderer.sharedMaterials[i].name.ToLower().Contains("trunk") || Renderer.sharedMaterials[i].shader.name.ToLower().Contains("trunk"))
                        {
                            IndiceBark = i;
                            break;
                        }
                    }
                }

                Vector3[] Vertices = CMesh.vertices;

                if (IndiceBark != -1)
                {
                    foreach (int Indice in CMesh.GetIndices(IndiceBark))
                    {
                        Vector3 v = Vertices[Indice];

                        if (v.y < MinY) MinY = v.y;
                        if (v.y > MaxY) MaxY = v.y;

                        if (v.y >= 1)
                            continue;

                        if (v.x < MinX) MinX = v.x;
                        if (v.x > MaxX) MaxX = v.x;
                        if (v.z < MinZ) MinZ = v.z;
                        if (v.z > MaxZ) MaxZ = v.z;
                    }
                }
                else
                {
                    foreach (Vector3 v in Vertices)
                    {
                        if (v.y < MinY) MinY = v.y;
                        if (v.y > MaxY) MaxY = v.y;

                        if (v.y >= 1)
                            continue;

                        if (v.x < MinX) MinX = v.x;
                        if (v.x > MaxX) MaxX = v.x;
                        if (v.z < MinZ) MinZ = v.z;
                        if (v.z > MaxZ) MaxZ = v.z;
                    }
                }

                Script = Obj.AddComponent<GS_AddCollider>();
                float CenterX = ((Mathf.Abs(MinX) + Mathf.Abs(MaxX)) * 0.5f) + MinX;
                float CenterY = ((Mathf.Abs(MinY) + Mathf.Abs(MaxY)) * 0.5f) + MinY;
                float CenterZ = ((Mathf.Abs(MinZ) + Mathf.Abs(MaxZ)) * 0.5f) + MinZ;
                Script.Center = new Vector3(CenterX, CenterY, CenterZ);//;new Bounds(new Vector3(MinX + MaxX * 0.5f, MinY + MaxY * 0.5f, MinZ + MaxZ * 0.5f), new Vector3(MaxX - MinX, MaxY - MinY, MaxZ - MaxZ));
                Script.Radius = Mathf.Max(Mathf.Abs(MaxX) + Mathf.Abs(MinX), Mathf.Abs(MaxZ) + Mathf.Abs(MinZ)) * 0.5f;
                Script.Height = MaxY - MinY;
                return Script;
            }
        }

        Obj.SetActive(false);
        return Script;
    }

    public IEnumerator GenerateScriptLOD()
    {
        Debug.Log(name + " : LOD Textures : Generating ...");

        // Disabling all Light, used for generate correct LOD plane color
        Light[] Light = FindObjectsOfType<Light>();
        bool[] LightStates = null;
        int i = 0;

        if (Light != null)
        {
            LightStates = new bool[Light.Length];

            foreach (Light L in Light)
            {
                LightStates[i] = L.enabled;
                L.enabled = false;
                ++i;
            }
        }

        float LastRange = 0;
        float FadeLenght = 40;
        List<IGameObjectSpawnScript> SubScripts = new List<IGameObjectSpawnScript>();
        GameObject Container;
        GS_LODSwitcher Switcher;
        GS_ActiveDisable OriginalScript;
        GS_LODFadeOut FadeOut;
        GS_LODFadeOut FadeOutDone;
        GS_ActiveDisable LODScript = null;

        GameObject Original;
        MeshFilter[] OriginalFilters;
        MeshRenderer[] OriginalMeshRenderer;
        Mesh CreatedMesh;
        GameObject Obj = null;
        MeshFilter Filter = null;
        MeshRenderer Renderer = null;

        int UniqueScriptId = 0;

        foreach (GameObjectInfo Info in Objects)
        {
            SubScripts.Clear();
            Original = null;
            Info.MaxDrawDistance = 0;

            if (Info.UseLod)
            {
                LastRange = 0;

                GameObject ScriptParentContainer = new GameObject();
                ScriptParentContainer.name = Info.Object.name;
                ScriptParentContainer.transform.parent = transform;

                Container = new GameObject();
                Container.transform.parent = ScriptParentContainer.transform;
                Container.name = Info.Object.name + "(Container)";
                Info.LodObject = Container;


                Switcher = Container.AddComponent<GS_LODSwitcher>();
                Switcher.ScriptId = --UniqueScriptId;
                SubScripts.Add(Switcher);
                Switcher.enabled = false;

                Original = GameObject.Instantiate(Info.Object) as GameObject;
                Original.name += "LOD";
                Original.transform.parent = ScriptParentContainer.transform;
                Original.transform.localPosition = new Vector3();
                if (Info.AutoGenerateCollider)
                {
                    GS_AddCollider Col = GenerateColliders(Container, Original);
                    if (Col != null)
                    {
                        Col.ScriptId = --UniqueScriptId;
                        Col.SetRanges(0, Info.LodDistance/2);
                        SubScripts.Add(Col);
                    }
                }

                OriginalScript = Container.AddComponent<GS_ActiveDisable>();
                OriginalScript.ScriptId = --UniqueScriptId;
                SubScripts.Add(OriginalScript);

                OriginalScript.Prefab = Original;
                OriginalScript.SetRanges(0, Info.LodDistance);
                LastRange = Info.LodDistance;

                OriginalFilters = Original.GetComponentsInChildren<MeshFilter>();
                OriginalMeshRenderer = Original.GetComponentsInChildren<MeshRenderer>();
                SubScripts.AddRange(Original.GetComponentsInChildren<IGameObjectSpawnScript>());

                if (Info.UseFading)
                {
                    for (i = 0; i < OriginalMeshRenderer.Length; ++i)
                    {
                        FadeOut = Original.AddComponent<GS_LODFadeOut>();
                        FadeOut.ScriptId = --UniqueScriptId;
                        FadeOut.ParentScriptId = OriginalScript.ScriptId;
                        SubScripts.Add(FadeOut);

                        FadeOut.SetRanges(OriginalScript.MaxRange, OriginalScript.MaxRange+FadeLenght);
                        FadeOut.FadeLenght = FadeLenght;
                        FadeOut.MatContainers = GetContainer(OriginalMeshRenderer[i]);
                    }

                    OriginalScript.SetRanges(OriginalScript.MinRange, OriginalScript.MaxRange+FadeLenght);

                }

                CreatedMesh = null;
                foreach (LodInfo LInfo in Info.Lods)
                {
                    Obj = null;
                    LODScript = null;
                    if (LInfo.Type == LodTypes.GAMEOBJECT)
                    {
                        Obj = GameObject.Instantiate(LInfo.LodObject) as GameObject;
                        SubScripts.AddRange(Obj.GetComponentsInChildren<IGameObjectSpawnScript>());
                    }
                    else if(LInfo.Type == LodTypes.BATCHING)
                    {
                        for (i = 0; i < OriginalFilters.Length; ++i)
                        {
                            // Batching
                            GS_Batching Batching = Container.AddComponent<GS_Batching>();
                            Batching.ScriptId = --UniqueScriptId;
                            Batching.SetRanges(0, LastRange + LInfo.ActiveRange);
                            Batching.CMesh = OriginalFilters[i].sharedMesh;
                            Batching.MatContainer = GetContainer(OriginalMeshRenderer[i]);
                            Batching.RotationOffset = OriginalMeshRenderer[i].transform.rotation;
                            SubScripts.Add(Batching);

                            OriginalMeshRenderer[i].enabled = false;
                        }

                        LastRange += LInfo.ActiveRange;
                    }
                    else
                    {
                        if (LInfo.ProceduralTexture && OriginalFilters == null || OriginalFilters.Length == 0)
                        {
                            Debug.LogError("Can not create texture without a Mesh in gameobject : " + Info.Object.name);
                            continue;
                        }

                        Original.SetActive(true);

                        Obj = new GameObject();
                        Obj.name = Info.Object.name + "-LOD-" + LastRange;
                        Filter = Obj.AddComponent<MeshFilter>();
                        Renderer = Obj.AddComponent<MeshRenderer>();
                        Renderer.receiveShadows = true;
                        Renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                        CreatedMesh = Filter.mesh;
                        Filter.sharedMesh = CreatedMesh;

                        LODTextureMaker Maker = gameObject.AddComponent<LODTextureMaker>();
                        Maker.Init(null, OriginalFilters, LInfo.Resolution, OriginalMeshRenderer[0].bounds);

                        if (LInfo.ProceduralTexture)
                        {
                            LInfo.PlaneTexture = GetLODTexture(Info);

                            if (LInfo.PlaneTexture == null)
                            {
                                if (InitCamera())
                                {
                                    yield return new WaitForEndOfFrame();
                                }

                                Cam.backgroundColor = LInfo.ProceduralCameraBackground;
                                CamLight.intensity = LInfo.LodLightPower;
                                Maker.Init(Cam, OriginalFilters, LInfo.Resolution, OriginalMeshRenderer[0].bounds);
                                Maker.Generate();

                                while (!Maker.IsDone)
                                {
                                    yield return new WaitForEndOfFrame();
                                    TerrainManager.Instance.SubProgress.SetProgress(Info.Name, Maker.Progress, ThreadManager.UnityTime);
                                }

                                LInfo.PlaneTexture = Maker.Result;
                                SaveLODTexture(Info, Maker.Result);
                            }
                        }
                        else
                            yield return new WaitForEndOfFrame();

                       
                        if (LInfo.Type == LodTypes.PLANE)
                            GeneratePlaneMesh(CreatedMesh, Maker.Center, Maker.Extend, Maker.YOffset);
                        else
                            GenerateCrossPlaneMesh(CreatedMesh, Maker.Center, Maker.Extend, Maker.YOffset);

                        if (LInfo.PlaneMaterial == null)
                        {
                            Debug.LogError("Invalid LOD Material : " + Info.Object + ",Using Default Diffuse Shader");
                            LInfo.PlaneMaterial = new Material(Shader.Find("Hidden/Diffuse"));
                        }

                        if (LInfo.ProceduralTexture)
                            LInfo.ProceduralMaterial = new Material(LInfo.PlaneMaterial);
                        else
                            LInfo.ProceduralMaterial = LInfo.PlaneMaterial;

                        if (LInfo.ProceduralMaterial == null)
                            Debug.LogError("Invalid Material :" + Info.Object.name);

                        LInfo.ProceduralMaterial.mainTexture = LInfo.PlaneTexture;
                        Renderer.sharedMaterial = LInfo.ProceduralMaterial;

                        GameObject.DestroyImmediate(Maker);
                    }

                    if (Obj != null)
                    {
                        LODScript = Container.AddComponent<GS_ActiveDisable>();
                        LODScript.ScriptId = --UniqueScriptId;
                        LODScript.Prefab = Obj;
                        LODScript.SetRanges(LastRange, LastRange + LInfo.ActiveRange);

                        float Start = LastRange;
                        float End = LastRange + LInfo.ActiveRange;

                        LastRange += LInfo.ActiveRange;

                        Obj.transform.parent = ScriptParentContainer.transform;
                        Obj.transform.localPosition = new Vector3();

                        if (LInfo.ProceduralTexture)
                        {
                            if (Info.UseFading)
                            {
                                MeshRenderer[] LODRenderers = Obj.GetComponentsInChildren<MeshRenderer>();

                                for (i = 0; i < LODRenderers.Length; ++i)
                                {
                                    GS_LODFadeIn FadeIn = Obj.AddComponent<GS_LODFadeIn>();
                                    FadeIn.ScriptId = --UniqueScriptId;
                                    FadeIn.ParentScriptId = LODScript.ScriptId;
                                    SubScripts.Add(FadeIn);

                                    FadeIn.SetRanges(Start-FadeLenght*0.5f, Start + FadeLenght*0.5f);
                                    FadeIn.FadeLenght = FadeLenght;
                                    // Original Materials are stored for fading system. When fading is done , shared mats are applied
                                    FadeIn.MatContainers = GetContainer(LODRenderers[i]);


                                    /*FadeOutDone = Obj.AddComponent<GS_LODFadeOut>();
                                    FadeOutDone.ScriptId = --UniqueScriptId;
                                    FadeOutDone.ParentScriptId = LODScript.ScriptId;
                                    SubScripts.Add(FadeOutDone);
                                    FadeOutDone.SetRanges(End - FadeLenght * 0.5f, End+FadeLenght*0.5f);
                                    FadeOutDone.FadeLenght = FadeLenght;
                                    FadeOutDone.Renderer = FadeIn.Renderer;
                                    FadeOutDone.MatContainers = FadeIn.MatContainers;*/
                                }

                                LODScript.SetRanges(Start-FadeLenght*0.5f, LODScript.MaxRange);
                            }


                            if (CombineLODPlane)
                            {
                                // Batching
                                GS_Batching Batching = Container.AddComponent<GS_Batching>();
                                Batching.ScriptId = --UniqueScriptId;
                                Batching.SetRanges(LODScript.MinRange + FadeLenght-1f, LODScript.MaxRange);
                                Batching.CMesh = CreatedMesh;
                                GS_MaterialContainer MatCont = gameObject.AddComponent<GS_MaterialContainer>();
                                MatCont.MatArray.SharedMaterials = new Material[1] { new Material(LInfo.ProceduralMaterial) };
                                MatCont.Index = MaterialContainers.Count;
                                MaterialContainers.Add(MatCont);
                                Batching.MatContainer = MatCont;
                                if (CombineLODShader != null)
                                {
                                    MatCont.MatArray.SharedMaterials[0] = new Material(LInfo.ProceduralMaterial);
                                    MatCont.MatArray.SharedMaterials[0].shader = CombineLODShader;
                                }

                                Batching.InstantMeshUpdate = true;
                                Batching.RotationOffset = Container.transform.rotation;
                                SubScripts.Add(Batching);

                                LODScript.SetRanges(LODScript.MinRange, LODScript.MinRange + FadeLenght+1f);
                            }
                        }

                        Obj.SetActive(false);
                        Original.SetActive(false);
                        SubScripts.Add(LODScript);
                    }
                }

                Switcher.enabled = true;
                Container.SetActive(false);

                Info.MaxDrawDistance = LastRange;
            }
            else
            {
                if (Info.Object == null)
                    Debug.LogError(name + ":" + DataName + " Invalid GameObject:" + Info.Name);

                Info.LodObject = GameObject.Instantiate(Info.Object) as GameObject;
                SubScripts.AddRange(Info.LodObject.GetComponentsInChildren<IGameObjectSpawnScript>());
                Info.LodObject.transform.parent = transform;
                Info.LodObject.transform.localPosition = new Vector3();
                if (Info.AutoGenerateCollider)
                    GenerateColliders(Info.LodObject, Info.LodObject);
            }

            GameObject Default = Info.GetObject();

            Default.SetActive(true);

            IGameObjectsScriptsContainer ScriptContainer = null;
            ScriptContainer = Default.GetComponent<IGameObjectsScriptsContainer>();
            if (ScriptContainer == null)
                ScriptContainer = Default.AddComponent<IGameObjectsScriptsContainer>();

            Default.SetActive(false);

            if (SubScripts.Count != 0)
            {
                List<IGameObjectSpawnScript> Scripts = new List<IGameObjectSpawnScript>();

                if (ScriptContainer.Scripts != null)
                    Scripts.AddRange(ScriptContainer.Scripts);

                foreach (IGameObjectSpawnScript Script in SubScripts)
                {
                    if (!Scripts.Contains(Script))
                        Scripts.Add(Script);
                }

                ScriptContainer.Scripts = Scripts.ToArray();
            }

            if (ScriptContainer.Scripts == null)
                    ScriptContainer.Scripts = new IGameObjectSpawnScript[0];

            ScriptContainer.ScriptsCount = ScriptContainer.Scripts.Length - 1;
            for (int j = 0; j < ScriptContainer.Scripts.Length; ++j)
            {
                ScriptContainer.Scripts[j].ScriptIndex = j;
                if (ScriptContainer.Scripts[j].gameObject != Default)
                    ScriptContainer.Scripts[j] = null;
            }

            TerrainManager.Instance.SubProgress.SetProgress(Info.Name, 100f, ThreadManager.UnityTime);
        }

        if (Light != null)
        {
            i = 0;
            foreach (Light L in Light)
            {
                L.enabled = LightStates[i];
                ++i;
            }
        }

        LodGenerationDone = true;

        if (Cam != null)
        {
            Cam.targetTexture = null;
            GameObject.Destroy(CamLight.gameObject);
            GameObject.Destroy(Cam.gameObject);
        }
    }

    public Texture2D GetLODTexture(GameObjectInfo Info)
    {
        if (!ThreadManager.IsWebPlayer)
        {
            string URL = System.IO.Path.Combine(FileFolder, DataName.GetHashCode() + "-" + Info.GetName() + ".png");
            if (System.IO.File.Exists(URL))
            {
                byte[] bytes = System.IO.File.ReadAllBytes(URL);
                Texture2D PlaneTexture = new Texture2D(2, 2);
                PlaneTexture.LoadImage(bytes);
                PlaneTexture.Apply(true, true);
                return PlaneTexture;
            }
        }

        return null;
    }

    public void SaveLODTexture(GameObjectInfo Info, Texture2D Texture)
    {
        if (Texture == null)
            return;

        try
        {
            System.IO.Directory.CreateDirectory(FileFolder);
            string URL = System.IO.Path.Combine(FileFolder, DataName.GetHashCode() + "-" + Info.GetName() + ".png");
            byte[] png = Texture.EncodeToPNG();
            using (System.IO.FileStream Stream = System.IO.File.Create(URL))
            {
                Stream.Write(png, 0, png.Length);
                Stream.Flush();
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }

    private Camera Cam;
    private Light CamLight;
    private RenderTexture RTexture;
    public bool InitCamera()
    {
        if (Cam == null)
        {
            RenderSettings.ambientLight = new Color(0.5f, 0.5f, 0.5f);
            GameObject CamObject = new GameObject();
            CamObject.transform.parent = transform;
            Cam = CamObject.gameObject.AddComponent<Camera>();
            Cam.orthographic = true;
            Cam.transform.rotation = new Quaternion(0, 0, 0, 1);
            Cam.nearClipPlane = 0.01f;
            Cam.farClipPlane = 50f;
            Cam.fieldOfView = 60;
            Cam.depth = -99;
            Cam.clearFlags = CameraClearFlags.Color;

            GameObject CamObj = new GameObject();
            CamLight = CamObj.AddComponent<Light>();
            CamLight.type = LightType.Directional;
            CamLight.shadows = LightShadows.Soft;
            CamObj.transform.eulerAngles = new Vector3(45, 0, 0);
            return true;
        }

        return false;
    }

    public void GeneratePlaneMesh(Mesh CreatedMesh, Vector3 Center, Vector3 Extend, float YOffset = 0)
    {
        CreatedMesh.vertices = new Vector3[4]
            {
                new Vector3(-Extend.x, YOffset, 0),
                new Vector3(Extend.x, YOffset, 0),
                new Vector3(-Extend.x, Extend.y * 2+YOffset, 0),
                new Vector3(Extend.x, Extend.y * 2+YOffset, 0),
            };

        CreatedMesh.SetIndices(PlaneIndice, MeshTopology.Quads, 0);
        CreatedMesh.uv = PlaneUV;
        CreatedMesh.normals = PlaneNormals;
    }
    public void GenerateCrossPlaneMesh(Mesh CreatedMesh, Vector3 Center, Vector3 Extend, float YOffset = 0)
    {
        CreatedMesh.vertices = new Vector3[8]
            {
                new Vector3(-Extend.x, YOffset, 0),
                new Vector3(Extend.x, YOffset, 0),
                new Vector3(-Extend.x, Extend.y * 2+YOffset, 0),
                new Vector3(Extend.x, Extend.y * 2+YOffset, 0),

                MeshUtils.RotateVertice(new Vector3(-Extend.x, YOffset, 0),90),
                MeshUtils.RotateVertice(new Vector3(Extend.x, YOffset, 0),90),
                MeshUtils.RotateVertice(new Vector3(-Extend.x, Extend.y * 2+YOffset, 0),90),
                MeshUtils.RotateVertice(new Vector3(Extend.x, Extend.y * 2+YOffset, 0),90),
            };

        CreatedMesh.SetIndices(CrossPlaneIndice, MeshTopology.Quads, 0);
        CreatedMesh.uv = CrossPlaneUvs;
        CreatedMesh.normals = CrossPlaneNormals;
        //CreatedMesh.tangents = CrossPlaneTangents;
        //MeshUtils.calculateMeshTangents(CreatedMesh);
    }

    static public int[] PlaneIndice =  new int[4] { 0, 1, 3, 2 };
    static public int[] CrossPlaneIndice = new int[16] {
        0, 1, 3, 2,
        4, 5, 7, 6,
        2,3,1,0,
        6,7,5,4 };
    static public Vector3[] PlaneNormals = new Vector3[4]
            {
                -Vector3.up,
                -Vector3.up,
                Vector3.up,
                Vector3.up,
            };
    static public Vector2[] PlaneUV = new Vector2[4] { new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1), };
    static public Vector3[] CrossPlaneNormals = new Vector3[8]
            {
                -Vector3.up*0.2f,
                -Vector3.up*0.2f,
                Vector3.up,
                Vector3.up,
                -Vector3.up*0.2f,
                -Vector3.up*0.2f,
                Vector3.up,
                Vector3.up,
            };
    static public Vector4[] CrossPlaneTangents = new Vector4[8]
            {
                new Vector4(1,0,0,-1),new Vector4(1,0,0,-1),new Vector4(1,0,0,-1),new Vector4(1,0,0,-1),
                new Vector4(0,0,-1,-1),new Vector4(0,0,-1,-1),new Vector4(0,0,-1,-1),new Vector4(0,0,-1,-1),
            };
    static public Vector2[] CrossPlaneUvs = new Vector2[8] 
        { 
            new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1),
            new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1)
        };

    #endregion

    #region CustomData Modification

    public WaitActionDone UpdateAction;

    public override int GetDataIdByName(string Name)
    {
        return GetInfo(Name).ObjectId;
    }

    public override GameObject GetCursor(int Id)
    {
        return Objects[Id].Object;
    }

    public override KeyValuePair<string, int>[] GetDatas()
    {
        KeyValuePair<string, int>[] Kps = new KeyValuePair<string, int>[Objects.Count];
        for (int i = 0; i < Objects.Count; ++i)
        {
            Kps[i] = new KeyValuePair<string, int>(Objects[i].GetObject().name, i);
        }

        return Kps;
    }

    public override void ApplyPoint(ProcessorVoxelPoint Point)
    {
        GameObjectDatas GData = Point.Block.Voxels.GetCustomData<GameObjectDatas>(InternalDataName);
        if (GData == null)
            return;

        if (UpdateAction == null)
            UpdateAction = new WaitActionDone();

        switch (Point.ModifyType)
        {
            case ProcessorVoxelChangeType.ROTATE:

                break;

            case ProcessorVoxelChangeType.SCALE:

                break;

            case ProcessorVoxelChangeType.SET:
                GameObjectClientDatas ClientData = Point.Block.Voxels.GetCustomData<GameObjectClientDatas>(ClientDataName);
                UpdateAction.AddAction(RemoveObject(Point.Block, GData, ClientData, Point.x, Point.y, Point.z));
                UpdateAction.AddAction(AddObject(Point.Block, GData, ClientData, Point.x, Point.y, Point.z, Point.Id, Point.Rotation, Point.Scale, Point.UseRandom));
                break;
        };
    }

    public override IAction OnVoxelFlushDone(ActionThread Thread, TerrainChunkGenerateGroupAction FlushAction)
    {
        if (UpdateAction == null)
            return null;

        WaitActionDone Result = UpdateAction;
        UpdateAction = null;

        foreach (IAction Action in Result.Actions)
            ActionProcessor.AddToUnity(Action);
        ActionProcessor.AddToUnity(Result);
        return Result;
    }

    public virtual IAction RemoveObject(TerrainBlock Terrain, GameObjectDatas Data, GameObjectClientDatas ClientData, int x, int y, int z)
    {
        int CurrentId = Data.GetByte(x, y, z);
        if (CurrentId != 0)
        {
            GameObjectPoolRemoveAction Action = PoolManager.Pool.GetData<GameObjectPoolRemoveAction>("GameObjectPoolRemoveAction");
            Action.Init(this, ClientData.GetObject(x, y, z));
            ClientData.SetObject(x, y, z, null, null);
            Data.SetByte(x, y, z, 0);
            return Action;
        }

        return null;
    }

    public virtual IAction AddObject(TerrainBlock Terrain, GameObjectDatas Data, GameObjectClientDatas ClientData, int x, int y, int z, int Id, float Rotation, float ScaleModification, bool UseRandom)
    {
        GameObjectInfo Info = Objects[Id];

        SFastRandom SRandom = new SFastRandom(UnityEngine.Random.Range(0, int.MaxValue));

        Info.RandomScale.UseXOnly = true;
        Vector3 Orientation;
        Vector3 Scale;

        if (UseRandom)
        {
            Orientation = Info.RandomOrientation.Random(ref SRandom);
            Scale = Info.RandomScale.Random(ref SRandom);
        }
        else
        {
            Orientation = new Vector3(0, Rotation, 0);
            Scale = new Vector3(ScaleModification, ScaleModification, ScaleModification);
        }

        Data.SetByteIntFloat(x, y, z, (byte)(Id + 1), (int)Orientation.y, Scale.x);
        ClientData.WaitingObjects.AddUnsafe(new GameObjectSpawnInfo((byte)Id, (ushort)x, (ushort)y, (ushort)z, Terrain.GetWorldPosition(x, y, z, Info.AutoAlignToGround) + new Vector3(0,Info.YOffset,0), Quaternion.Euler(Orientation), Scale.x,Objects[Id].MaxDrawDistance));
        return null;
    }

    #endregion
}
