﻿
using TerrainEngine;
using UnityEngine;

public abstract class IGameObjectSpawnScript : MonoBehaviour
{
    public int ScriptId = -1;
    public int ParentScriptId = -1;
    public int ScriptIndex = -1;

    [HideInInspector]
    public IGameObjectsScriptsContainer Container;

    public virtual void OnInit(TerrainBlock Terrain, Vector3 Position, float Scale, Quaternion Rotation, int SpawnIndex, int Index)
    {

    }

    public virtual void OnRemove(GameObjectClientDatas.ClientGameObjectSpawn Spawn)
    {

    }

    public virtual float OnLODUpdate(GS_LODSwitcher Switcher, float Range)
    {
        return 10f;
    }

    public virtual float UnityUpdate()
    {
        return 10f;
    }

    /// <summary>
    /// Function to override to active script systems
    /// </summary>
    public virtual void SetActive()
    {

    }

    /// <summary>
    /// Use if function is called from another thread. Will can SetActive() on Unity Thread
    /// </summary>
    public void SetActiveThread()
    {
        ExecuteAction Action = new ExecuteAction("SetActive", SetActive);
        ActionProcessor.AddToUnity(Action);
    }

    /// <summary>
    /// Override this function to share this state to all clients over the network
    /// </summary>
    public virtual void Active(bool IsNetwork)
    {
        if (!IsNetwork)
            TerrainManager.Instance.ModificationInterface.OnCustomScriptActive(this, true);
    }

    public virtual void Disable(bool IsNetwork)
    {
        if (!IsNetwork)
            TerrainManager.Instance.ModificationInterface.OnCustomScriptActive(this, false);
    }

    /// <summary>
    /// Called when underground voxel is modified. Return truc if gameobject will be removed by this script
    /// </summary>
    /// <param name="Flush"></param>
    /// <returns></returns>
    public virtual bool OnVoxelChange(VoxelFlush Flush)
    {
        return false;
    }
}
