﻿
using UnityEngine;

namespace TerrainEngine
{
    public class GameObjectPoolRemoveAction : IAction
    {
        public GameObjectProcessor Processor;
        public GameObject Obj;
        public int GoPool = -1;

        public void Init(GameObjectProcessor Processor, GameObject Obj)
        {
            this.Processor = Processor;
            this.Obj = Obj;
        }

        public void Init(int Pool, GameObject Obj)
        {
            this.GoPool = Pool;
            this.Obj = Obj;
        }

        public override bool OnUpdate(ActionThread Thread)
        {
            if (Obj != null)
            {
                if (GoPool == -1)
                {
                    IGameObjectsScriptsContainer Container = Obj.GetComponentInChildren<IGameObjectsScriptsContainer>();
                    if (Container != null)
                    {
                        Container.Despawn(false, false, Container.Spawn, true, true);
                        GameObjectPool.CloseGameObject(Container.OriginalName, Obj);
                    }
                    else
                        GameObjectPool.CloseGameObject(Obj.name, Obj);
                }
                else
                    GameObjectPool.Pools.array[GoPool].CloseObject(Obj);
            }

            Close();
            return true;
        }

        public override void Clear()
        {
            Processor = null;
            Obj = null;
            GoPool = -1;
        }
    }
}
