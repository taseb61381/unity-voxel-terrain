﻿

using TerrainEngine;

public class AdvancedGameObjects : GameObjectProcessor
{
    public BiomeObjectsContainer ObjectsContainer
    {
        get
        {
            return Container.ObjectsNoises;
        }
    }

    public override void OnInitSeed(int seed)
    {
        EnableGenerationFunction = false;
        base.OnInitSeed(seed);
    }

    public override IWorldGenerator GetGenerator()
    {
        AdvancedGameObjectGenerator Gen = new AdvancedGameObjectGenerator();
        Gen.Priority = Priority;
        Gen.Name = InternalName;
        Gen.DataName = "GO-" + DataName;
        Gen.Density = Density;
        Gen.GameObjects = Objects;

        if (ObjectsContainer != null)
        {
            Gen.ObjectsRequirements = ObjectsContainer.Objects;
            Gen.RequirementNoises = ObjectsContainer.Container;
        }

        return Gen;
    }
}
