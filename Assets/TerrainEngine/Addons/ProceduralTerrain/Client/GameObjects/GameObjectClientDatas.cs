﻿using System.Collections.Generic;
using System.Diagnostics;

using UnityEngine;

namespace TerrainEngine
{
    public struct GameObjectSpawnInfo
    {
        public GameObjectSpawnInfo(byte InfoId, ushort x,ushort y,ushort z, Vector3 WorldPosition, Quaternion Orientation, float Scale,float MaxDrawDistance)
        {
            this.InfoId = InfoId;
            this.x = x;
            this.y = y;
            this.z = z;
            this.WorldPosition = WorldPosition;
            this.Orientation = Orientation;
            this.Scale = Scale;
            this.MaxDrawDistance = MaxDrawDistance;
        }

        public byte InfoId;
        public ushort x, y, z;
        public float Scale,MaxDrawDistance;
        public Vector3 WorldPosition;
        public Quaternion Orientation;

        public bool IsViewable(GameObjectProcessor Processor, Vector3 CameraPosition)
        {
            if (MaxDrawDistance < 1f || Vector3.Distance(CameraPosition, WorldPosition) < MaxDrawDistance)
            {
                return true;
            }

            return false;
        }
    };

    public class GameObjectClientDatas : ICustomTerrainData
    {
        public struct ClientGameObjectSpawn
        {
            public int Index;
            public ushort x, y, z;

            public GameObject GetObject(GameObjectClientDatas Data)
            {
                if (Data.GameObjects.array[Index] != null)
                    return Data.GameObjects.array[Index].gameObject;
                return null;
            }

            public IGameObjectsScriptsContainer GetScript(GameObjectClientDatas Data)
            {
                return Data.GameObjects.array[Index];
            }
        }

        //public GameObjectProcessor Processor;
        public FList<ClientGameObjectSpawn> Objects;
        public int VoxelCountX, VoxelCountY, VoxelCountZ;
        public GameObjectDatas Data;
        public GameObjectProcessor Processor;
        public FList<IGameObjectsScriptsContainer> GameObjects;
        public bool IsStarted = false;

        public override void Init(TerrainDatas Terrain)
        {
            base.Init(Terrain);
            VoxelCountX = Information.VoxelsPerAxis;
            VoxelCountY = Information.VoxelsPerAxis;
            VoxelCountZ = Information.VoxelsPerAxis;

            CellSize = Information.VoxelsPerAxis / 2;

            if (WaitingObjects == null)
            {
                GameObjects = new FList<IGameObjectsScriptsContainer>(Information.VoxelsPerAxis);
                Objects = new FList<ClientGameObjectSpawn>(Information.VoxelsPerAxis);
                WaitingObjects = new TransitionList<GameObjectSpawnInfo>();
                SpawningObjects = new TransitionQueue<GameObjectSpawnInfo>();
            }
            else
            {
                ClearGameObjects();
            }
        }

        public override void Clear()
        {
            IsStarted = false;
            LastCount = 0;
            NextUpdate = 0f;
            ClearGameObjects();
            base.Clear();
        }

        public void ClearGameObjects()
        {
            ClearCells();
            WaitingObjects.Clear();
            SpawningObjects.Clear();
            Objects.Clear();
            GameObjects.Clear();
        }

        public int GetIndex(int x,int y,int z)
        {
            for (int i = Objects.Count - 1; i >= 0; --i)
            {
                if (Objects.array[i].y == y && Objects.array[i].x == x && Objects.array[i].z == z)
                    return i;
            }

            return -1;
        }

        public string GetStats()
        {
            return "Objects:" + Objects.Count + ",WaitingObjects:" + (WaitingObjects.Count + WaitingObjects.WaitingCount) + ",SpawningObjects:" + (SpawningObjects.Count + SpawningObjects.WaitingCount) + ",NextUpdate:" + NextUpdate + ",LastCount:" + LastCount;
        }

        #region GameObjects

        public TransitionList<GameObjectSpawnInfo> WaitingObjects; // Objects waiting to be checked if is in distance needed to be spawned
        public TransitionQueue<GameObjectSpawnInfo> SpawningObjects; // Objects waiting to be instancied depending of fps

        public bool IsSpawning()
        {
            return SpawningObjects.List.Count != 0 || SpawningObjects.Interdata.Count != 0;
        }

        public bool IsReady()
        {
            return IsStarted && !IsSpawning() && !HasCellDirty();
        }

        private int LastCount = 0;
        private float NextUpdate = 0;
        private ITerrainCameraHandler.PositionMovementManager MovementManager = new ITerrainCameraHandler.PositionMovementManager();
        public void UpdateThread(ActionThread Thread)
        {
            if (NextUpdate > ThreadManager.UnityTime)
                return;

            NextUpdate = ThreadManager.UnityTime + 2f;

            int i = 0;

            ITerrainCameraHandler.Instance.Container.GetAllPositions(delegate(int Index, Vector3 CameraPosition)
            {
                if (MovementManager.HasPositionChanged(Index, CameraPosition, 5f))
                {
                    WaitingObjects.Process();

                    for (i = WaitingObjects.List.Count - 1; i >= 0; --i)
                    {
                        if (WaitingObjects.List[i].IsViewable(Processor, CameraPosition))
                        {
                            SpawningObjects.AddUnsafe(WaitingObjects.List[i]);
                            WaitingObjects.List.RemoveAt(i);
                        }
                    }

                    LastCount = WaitingObjects.List.Count;
                }

                return false;
            });

            IsStarted = true;
        }

        /// <summary>
        /// Instanciate objects in list
        /// </summary>
        public void UpdateUnity(ActionThread Thread, TerrainBlock Terrain)
        {
            if (SpawningObjects.List.Count == 0)
                SpawningObjects.Process();

            if(SpawningObjects.List.Count == 0)
                return;


            while (SpawningObjects.List.Count != 0 && Thread.HasTime())
            {
                GameObjectSpawnInfo Info = SpawningObjects.List.Dequeue();

                if (GetScripts(Info.x, Info.y, Info.z) != null)
                    return;

                GameObjectInfo GInfo = Processor.Objects[Info.InfoId];
                GameObject ToInstanciate = GInfo.GetObject();
                if (ToInstanciate == null)
                    return;

                int GOPool = GameObjectPool.GetPoolId(ToInstanciate.name, true);
                Quaternion Orientation = Info.Orientation * ToInstanciate.transform.rotation;

                GameObjectPool.Pools.array[GOPool].EnablePooling = GInfo.UsePooling;

                GameObject Obj = GameObjectPool.Pools.array[GOPool].GetObject(ToInstanciate, Info.WorldPosition, Orientation);
                Transform transform = Obj.transform;

                Terrain.CheckGameObject();

                transform.parent = Terrain.Script._transform;
                transform.localScale = new Vector3(Info.Scale, Info.Scale, Info.Scale);

                IGameObjectsScriptsContainer Container = Obj.GetComponent<IGameObjectsScriptsContainer>();

                if (Container == null)
                    Container = Obj.AddComponent<IGameObjectsScriptsContainer>();

                int Index = SetObject(Info.x, Info.y, Info.z, Obj, Container);
                Container.Init(Terrain, this, Index, Info.WorldPosition, Info.Scale, Orientation, ToInstanciate.name);

                if (ThreadManager.IsEditor)
                    Obj.name = ToInstanciate.name + ":" + Info.x + "," + Info.y + "," + Info.z;

                Obj.SetActive(true);
            }
        }

        #endregion

        #region Access

        public int SetObject(int x, int y, int z, GameObject Obj, IGameObjectsScriptsContainer Scripts)
        {
            int Index = GetIndex(x, y, z);
            if (Index == -1)
            {
                if (Obj as object == null)
                {
                    return -1;
                }

                ClientGameObjectSpawn Data = new ClientGameObjectSpawn();
                Data.x = (ushort)x;
                Data.y = (ushort)y;
                Data.z = (ushort)z;
                Data.Index = Objects.Count;
                Objects.Add(Data);
                GameObjects.Add(Scripts);
                return Data.Index;
            }
            else
            {
                if (Obj as object == null)
                {
                    Objects.array[Index].Index = -1;
                    return -1;
                }

                GameObjects.array[Index] = Scripts;
                return Index;
            }
        }

        public GameObject GetObject(int x, int y, int z, bool Remove = false)
        {
            for (int i = Objects.Count - 1; i >= 0; --i)
            {
                if (Objects.array[i].y == y && Objects.array[i].x == x && Objects.array[i].z == z)
                {
                    IGameObjectsScriptsContainer Container = GameObjects.array[i];

                    if (Remove)
                    {
                        Objects.array[i].Index = -1;
                        GameObjects.array[i] = null;
                    }

                    if (Container != null)
                        return Container.gameObject;
                }
            }

            return null;
        }

        public IGameObjectsScriptsContainer GetScripts(int x, int y, int z)
        {
            for (int i = Objects.Count - 1; i >= 0; --i)
            {
                if (Objects.array[i].y == y && Objects.array[i].x == x && Objects.array[i].z == z)
                {
                    return GameObjects.array[i];
                }
            }

            return null;
        }

        /// <summary>
        /// Call Active Function to a script of a gameobject. Used for networking
        /// </summary>
        public void ActiveScript(ushort x, ushort y, ushort z, string ScriptName, bool Active)
        {
            IGameObjectsScriptsContainer Container = GetScripts(x, y, z);
            if ((Container as object) != null)
            {
                foreach (IGameObjectSpawnScript Script in Container.Scripts)
                {
                    if (Script.GetType().Name.Contains(ScriptName))
                    {
                        if (Active)
                            Script.Active(true);
                        else
                            Script.Disable(true);
                    }
                }
            }
        }

        #endregion

        #region Cells

        public int CellSize = 256;

        public class Cell
        {
            public int x, z;
            public FList<int> Objects = new FList<int>(50);
            public float NextUpdate = 0;
            public int LastIndex = 0;
            public float LastWaitTime = 0;
            public bool Dirty = false;

            public void Update(ActionThread Thread, GameObjectClientDatas Data, float CurrentTime, Stopwatch Watch, long MaxUpdateTime)
            {
                if (x < 0)
                    return;

                float WaitTime = 0;
                float MinWaitTime = float.MaxValue;

                int LastId = LastIndex;
                int Start = LastIndex;

                for (; LastId < Objects.Count && Watch.ElapsedMilliseconds < MaxUpdateTime; ++LastId)
                {
                    WaitTime = Data.GameObjects.array[Objects.array[LastId]].Switcher.UnityUpdate();
                    if (WaitTime < MinWaitTime) MinWaitTime = WaitTime;
                }

                if (LastId >= Objects.Count)
                {
                    LastId = 0;
                    Dirty = false;
                }

                for (LastId = 0; LastId < Start && LastId < Objects.Count && Watch.ElapsedMilliseconds < MaxUpdateTime; ++LastId)
                {
                    WaitTime = Data.GameObjects.array[Objects.array[LastId]].Switcher.UnityUpdate();
                    if (WaitTime < MinWaitTime) MinWaitTime = WaitTime;
                }

                if (LastId >= Objects.Count)
                {
                    LastId = 0;
                    Dirty = false;
                }

                LastIndex = LastId;

                if (MinWaitTime >= float.MaxValue-10f)
                    MinWaitTime = 0;

                NextUpdate = CurrentTime + MinWaitTime;
                LastWaitTime = MinWaitTime;
            }

            public void AddObject(int Index)
            {
                Dirty = true;
                Objects.Add(Index);
                NextUpdate = ThreadManager.UnityTime + 2f;
            }

            public void RemoveObject(int Index)
            {
                Objects.Remove(Index);
            }
        }

        public FList<Cell> Cells = new FList<Cell>(1);

        public void ClearCells()
        {
            for(int i=Cells.Count-1;i>=0;--i)
            {
                Cells.array[i].NextUpdate = float.MaxValue;
                Cells.array[i].x = -1;
                Cells.array[i].Objects.ClearFast();
            }
        }

        public Cell GetCell(int x, int z, bool Create=true)
        {
            x /= CellSize;
            z /= CellSize;

            for (int i = Cells.Count - 1; i >= 0; --i)
            {
                if (Cells.array[i].x < 0)
                {
                    Cells.array[i].x = x;
                    Cells.array[i].z = z;
                }

                if (Cells.array[i].x == x && Cells.array[i].z == z)
                {
                    return Cells.array[i];
                }
            }

            if (Create)
            {
                Cell C = new Cell() { x = x, z = z };
                Cells.Add(C);
                return C;
            }

            return null;
        }

        public int GetCellObjectsCount()
        {
            int Count = 0;
            for (int i = Cells.Count - 1; i >= 0; --i)
                Count += Cells.array[i].Objects.Count;
            return Count;
        }

        public void AddSwitcher(ClientGameObjectSpawn Spawn)
        {
            Cell C = GetCell(Spawn.x, Spawn.z, true);
            C.AddObject(Spawn.Index);
        }

        public void RemoveSwitcher(ClientGameObjectSpawn Spawn)
        {
            Cell C = GetCell(Spawn.x, Spawn.z, false);
            if(C != null)
                C.RemoveObject(Spawn.Index);
        }

        public bool HasCellDirty()
        {
            for (int i = Cells.Count - 1; i >= 0; --i)
                if (Cells.array[i].Dirty)
                    return true;
            return false;
        }

        public int LastCellIndex = 0;
        public void UpdateCells(ActionThread Thread, Stopwatch Watch, long MaxUpdateTime)
        {
            for (; LastCellIndex < Cells.Count; ++LastCellIndex)
            {
                if (ThreadManager.UnityTime < Cells.array[LastCellIndex].NextUpdate)
                    continue;

                Cells.array[LastCellIndex].Update(Thread, this, ThreadManager.UnityTime, Watch, MaxUpdateTime);
                if (Watch.ElapsedMilliseconds >= MaxUpdateTime)
                    return;
            }
            LastCellIndex = 0;
        }


        #endregion
    }
}
