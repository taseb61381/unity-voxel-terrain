using TerrainEngine;
using UnityEngine;
using System;

public class GS_LODFadeIn : GS_LOD 
{
    public float FadeLenght = 5f;
    public int MatContainerIndex;
    public GS_LOD ParentScript
    {
        get
        {
            for (int i = Container.ScriptsCount; i >= 0; --i)
                if (Container.Scripts[i] != null && Container.Scripts[i].ScriptId == ParentScriptId)
                    return Container.Scripts[i] as GS_LOD;
            return null;
        }
    }
    public GS_MaterialContainer MatContainers
    {
        get
        {
            return Container.GetClientData().Processor.MaterialContainers[MatContainerIndex];
        }
        set
        {
            MatContainerIndex = value.Index;
        }
    }

    [NonSerialized]
    private Material[] Materials;

    public override float OnLODUpdate(GS_LODSwitcher Switcher, float Range)
    {
        float t = base.OnLODUpdate(Switcher, Range);

        if (State == false)
            return t;

        UpdateDistance(Switcher, Range);
        return 0;
    }

    public void UpdateDistance(GS_LODSwitcher Switcher, float Range)
    {
        float Power = Mathf.Clamp((Range - MinRange) / FadeLenght, 0f, 1f);
        int i;

        if (Materials == null)
            Materials = GetComponent<Renderer>().materials;

        for (i = Materials.Length - 1; i >= 0; --i)
        {
            if (MatContainers.MatArray.FadeType[i] == RendererMaterialsArray.FadeTypes._COLOR)
            {
                Color Col = MatContainers.MatArray.DefaultColor[i];
                Col.a = Power;
                Materials[i].SetColor("_Color", Col);
            }
            else if (MatContainers.MatArray.FadeType[i] == RendererMaterialsArray.FadeTypes._FADE)
            {
                Materials[i].SetFloat("_Fade", Power);
            }
            else if (MatContainers.MatArray.FadeType[i] == RendererMaterialsArray.FadeTypes._CUTOFF)
            {
                float Default = MatContainers.MatArray.DefaultFloat[i];

                float New = Default + (1f - Default) * (1f - Power);
                if (New < Default) New = Default;

                Materials[i].SetFloat("_Cutoff", New);
            }
        }
    }

    public override void Active(GS_LODSwitcher Switcher, float Range)
    {
        Materials = null;

        GS_LOD LOD_Script = ParentScript;

        if (LOD_Script != null)
        {
            UpdateDistance(Switcher, Range);
        }
    }

    public override void Disable(GS_LODSwitcher Switcher)
    {
        Materials = null;

        GetComponent<Renderer>().sharedMaterials = MatContainers.MatArray.SharedMaterials;
    }
}
