﻿using System;
using TerrainEngine;
using UnityEngine;
using System.Collections.Generic;

public class GS_ActiveDisable : GS_LOD
{
    static public List<IGameObjectSpawnScript> Scripts = new List<IGameObjectSpawnScript>();

    public GameObject Prefab;
    public GameObject Instance;
    public int PoolId = -1;

    public override void OnInit(TerrainBlock Terrain, Vector3 Position, float Scale, Quaternion Rotation, int SpawnIndex, int Index)
    {
        base.OnInit(Terrain, Position, Scale, Rotation, SpawnIndex,Index);

        if (Instance != null)
        {
            GameObjectPool.Pools.array[PoolId].CloseObject(Instance);
            Instance = null;
        }
    }

    public override void Active(GS_LODSwitcher Switcher, float Range)
    {
        if (Instance == null)
        {
            if (PoolId == -1)
                PoolId = GameObjectPool.GetPoolId(Prefab.name);

            Instance = GameObjectPool.Pools.array[PoolId].GetObject(Prefab);
            Instance.GetComponentsInChildren(Scripts);
            for (int i = Scripts.Count - 1; i >= 0; --i)
            {
                Container.Scripts[Scripts[i].ScriptIndex] = Scripts[i];
                Scripts[i].Container = Container;
                Scripts[i].OnLODUpdate(Switcher, Range);
            }
            Scripts.Clear();

            Transform TR = Instance.transform;
            TR.parent = Container.transform;
            TR.localScale = Vector3.one;
            TR.localRotation = Quaternion.identity;
            TR.localPosition = Vector3.zero;
        }
        else
            Instance.SetActive(true);
        base.Active(Switcher, Range);
    }

    public override void Disable(GS_LODSwitcher Switcher)
    {
        if (PoolId != -1 && Instance != null)
        {
            for (int i = Container.ScriptsCount; i >= 0; --i)
            {
                if ((object)Container.Scripts[i] == null)
                    continue;

                if (Container.Scripts[i].ParentScriptId == ScriptId)
                {
                    Container.Scripts[i].Disable(false);
                    Container.Scripts[i] = null;
                }
            }

            Instance.SetActive(false);
            GameObjectPool.Pools.array[PoolId].CloseObject(Instance);
            Instance = null;
        }

        base.Disable(Switcher);
    }
}
