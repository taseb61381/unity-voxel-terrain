using TerrainEngine;
using UnityEngine;

public class GS_Batching : GS_LOD
{
    public GraphicLODMesh.LODPosition LODPosition;  // LOD World Position
    public Mesh CMesh;
    public int MatContainerIndex;
    public GS_MaterialContainer MatContainer
    {
        get
        {
            return Container.GetClientData().Processor.MaterialContainers[MatContainerIndex];
        }
        set
        {
            MatContainerIndex = value.Index;
        }
    }
    public Quaternion RotationOffset;
    public bool InstantMeshUpdate = false;

    public override void OnInit(TerrainBlock Terrain, Vector3 Position, float Scale, Quaternion Rotation, int SpawnIndex, int Index)
    {
        GraphicLODMesh LODMesh = Terrain.LODContainer.AddMesh(Container.OriginalName) as GraphicLODMesh;
        LODMesh.InstantUpdate = InstantMeshUpdate;

        LODMesh.SetMesh(CMesh, MatContainer.MatArray.SharedMaterials);
        LODMesh.ReceiveShadow = false;
        LODMesh.CastShadow = true;

        LODPosition.Position = Position - Terrain.WorldPosition;
        LODPosition.Rotation = Rotation * RotationOffset;
        LODPosition.Scale = Scale;
        LODPosition.Id = -1;

        base.OnInit(Terrain, Position, Scale, Rotation, SpawnIndex, Index);
    }

    public override float OnLODUpdate(GS_LODSwitcher Switcher, float Range)
    {
        CheckRange(Switcher, Range);

        if (Range > MaxRange)
        {
            return ((Range - MaxRange) / 50f) - 0.1f; // 100 meters = wait 1 second
        }
        else if (Range < MinRange)
        {
            return ((MinRange - Range) / 50f) - 0.1f; // 100 meters
        }
        else
        {
            return (Mathf.Min(Range - MinRange, MaxRange - Range) / 50f) - 0.1f;
        }
    }

    public override void Active(GS_LODSwitcher Switcher, float Range)
    {
        GraphicLODMesh LODMesh = Container.Terrain.LODContainer.GetMesh(Container.OriginalName) as GraphicLODMesh;
        LODPosition.Id = LODMesh.AddPosition(LODPosition);

        if(!GameObjectProcessor.DirtyLODContainer.Contains(LODMesh))
            GameObjectProcessor.DirtyLODContainer.Add(LODMesh);
    }

    public override void Disable(GS_LODSwitcher Switcher)
    {
        GraphicLODMesh LODMesh = Container.Terrain.LODContainer.GetMesh(Container.OriginalName) as GraphicLODMesh;
        if (LODMesh != null)
        {
            LODMesh.RemovePosition(LODPosition.Id);
            LODPosition.Id = -1;

            if (!GameObjectProcessor.DirtyLODContainer.Contains(LODMesh))
                GameObjectProcessor.DirtyLODContainer.Add(LODMesh);
        }
    }
}
