﻿using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

[Serializable]
public struct RendererMaterialsArray
{
    public enum FadeTypes : byte
    {
        _NONE = 0,
        _FADE = 1,
        _CUTOFF = 2,
        _COLOR = 3,
    }

    public Material[] SharedMaterials;
    public float[] DefaultFloat;
    public Color[] DefaultColor;
    public FadeTypes[] FadeType;

    public void SetMaterials(Material[] Materials)
    {
        this.SharedMaterials = Materials;
        int Len = Materials.Length;
        DefaultFloat = new float[Len];
        DefaultColor = new Color[Len];
        FadeType = new FadeTypes[Len];

        for (int i = 0; i < Materials.Length; ++i)
        {
            if (Materials[i].HasProperty("_Fade"))
            {
                FadeType[i] = FadeTypes._FADE;
                DefaultFloat[i] = Materials[i].GetFloat("_Fade");
            }
            else if (Materials[i].HasProperty("_Cutoff"))
            {
                FadeType[i] = FadeTypes._CUTOFF;
                DefaultFloat[i] = Materials[i].GetFloat("_Cutoff");
            }
            else if (Materials[i].HasProperty("_Color"))
            {
                FadeType[i] = FadeTypes._COLOR;
            }
            else
                FadeType[i] = FadeTypes._NONE;

            if(Materials[i].HasProperty("_Color"))
                DefaultColor[i] = Materials[i].GetColor("_Color");

            //Debug.Log(Materials[i].name + ",Type:" + FadeType[i] + ",Color:" + DefaultColor[i]);
        }
    }
}

public class GS_MaterialContainer : MonoBehaviour
{
    public int Index;
    //public MeshRenderer Renderer;
    public RendererMaterialsArray MatArray;

    public Stack<Material[]> PoolMaterials = new Stack<Material[]>();

    public Material[] Get()
    {
        if (PoolMaterials.Count != 0)
        {
            ++PoolManager.UPool.UniqueMaterials.DequeueData;
            return PoolMaterials.Pop();
        }

        return null;
    }

    public bool Contains(MeshRenderer Renderer)
    {
        if (Renderer == null)
            return false;

        if (MatArray.SharedMaterials != Renderer.sharedMaterials)
            return false;

        return true;
    }

    public void SetRenderer(MeshRenderer Renderers)
    {
        MatArray = new RendererMaterialsArray();
        MatArray.SetMaterials(Renderers.sharedMaterials);
    }
}
