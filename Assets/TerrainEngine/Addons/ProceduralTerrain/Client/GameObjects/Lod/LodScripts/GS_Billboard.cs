using UnityEngine;


public class GS_Billboard : IGameObjectSpawnScript
{
    public Transform[] Transforms;
    //public bool IsVisible;

    void LateUpdate()
    {
        if (!ITerrainCameraHandler.Instance.IsUsingCamera() || !ITerrainCameraHandler.Instance.HasCamera)
            return;

        int i = Transforms.Length - 1;
        Vector3 v = new Vector3();
        Transform Tr;
        Transform Camera = ITerrainCameraHandler.Instance.GetMainCameraTObject().CTransform;
        for (; i >= 0; --i)
        {
            if (Camera.InverseTransformPoint((Tr = Transforms[i]).position).z >= 0)
            {
                v.y = Camera.position.y - Tr.position.y;
                Tr.LookAt(Camera.position - v);
                //Transforms[i].forward = -Container.Processor.Manager.CamObject.CTransform.forward;
            }
        }
    }

    /*void OnBecameVisible()
    {
        IsVisible = true;
    }

    void OnBecameInvisible()
    {
        IsVisible = false;
    }*/
}
