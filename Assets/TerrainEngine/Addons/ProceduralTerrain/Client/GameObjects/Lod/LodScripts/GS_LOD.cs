using System;
using TerrainEngine;
using UnityEngine;

public class GS_LOD : IGameObjectSpawnScript 
{
    [Flags]
    public enum GS_LODStates : byte
    {
        NONE = 0,
        FORCE_ACTIVE = 1,
        FORCE_DISABLE = 2,
    }

    public GS_LODStates States = GS_LODStates.NONE;

    public bool ForceActive
    {
        get
        {
            return HasState(GS_LODStates.FORCE_ACTIVE);
        }
        set
        {
            SetState(GS_LODStates.FORCE_ACTIVE, value);
        }
    }
    public bool ForceDisable
    {
        get
        {
            return HasState(GS_LODStates.FORCE_DISABLE);
        }
        set
        {
            SetState(GS_LODStates.FORCE_DISABLE, value);
        }
    }
    public bool State = false;

    public float MinRange;
    public float MaxRange;

    public void SetRanges(float MinRange, float MaxRange)
    {
        this.MinRange = MinRange;
        this.MaxRange = MaxRange;
    }

    public void SetState(GS_LODStates NewState, bool Value)
    {
        if (Value)
            States |= NewState;
        else
            States &= ~NewState;
    }

    public bool HasState(GS_LODStates NewState)
    {
        return (States & NewState) == NewState;
    }

    public override void OnInit(TerrainBlock Terrain, Vector3 Position, float Scale, Quaternion Rotation, int SpawnIndex, int Index)
    {
        States = GS_LODStates.NONE;
        State = false;
    }

    public override float OnLODUpdate(GS_LODSwitcher Switcher, float Range)
    {
         CheckRange(Switcher, Range);

         if (State != true)
         {
             if (Range > MaxRange)
             {
                 return ((Range - MaxRange) / 50f) - 0.1f; // 100 meters = wait 1 second
             }
             else if (Range < MinRange)
             {
                 return ((MinRange - Range) / 50f) - 0.1f; // 100 meters
             }
         }

         return 0f;
    }

    public virtual void CheckRange(GS_LODSwitcher Switcher, float Range)
    {
        if (IsInRange(Switcher, Range))
        {
            if (State != true)
            {
                State = true;
                Active(Switcher, Range);
            }
        }
        else
        {
            if (State != false)
            {
                State = false;
                Disable(Switcher);
            }
        }
    }

    public virtual bool IsInRange(GS_LODSwitcher Switcher, float Range)
    {
        if (ForceActive)
            return true;

        if (ForceDisable)
            return false;


        if (Switcher.LastRange < MinRange && Range > MaxRange)
        {
            return true;
        }

        if (Switcher.LastRange > MaxRange && Range < MinRange)
        {
            return true;
        }

        if (Range >= MinRange && Range < MaxRange)
        {
            return true;
        }

        return false;
    }

    public virtual void Active(GS_LODSwitcher Switcher, float Range)
    {
        State = true;
    }

    public virtual void Disable(GS_LODSwitcher Switcher)
    {
        State = false;
    }
}
