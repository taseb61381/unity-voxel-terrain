using TerrainEngine;
using UnityEngine;

/// <summary>
/// Check distance from camera , and active or disable GS_LOD scripts (Fading, Active or Disable gameobject, etc)
/// </summary>
public class GS_LODSwitcher : IGameObjectSpawnScript
{
    public Transform CTransform;
    public float LastRange;
    private float NextCheckTime = 0;
    private float LastWait = 0;

    public override float UnityUpdate()
    {
        if (NextCheckTime < ThreadManager.UnityTime)
        {
            Vector3 P = CTransform.position;
            float Range = Vector2.Distance(ITerrainCameraHandler.Instance.Container.GetClosest(P, true), new Vector2(P.x, P.z));

            if (Range < 0.1f || LastRange == Range)
            {
                NextCheckTime = ThreadManager.UnityTime + LastWait;
                return LastWait;
            }

            LastRange = Range;

            float WaitTime = 8f;
            float TempTime = 0;
            for (int i = Container.ScriptsCount; i >= 0; --i)
            {
                if ((object)Container.Scripts[i] == null)
                    continue;

                TempTime = Container.Scripts[i].OnLODUpdate(this, Range);
                if (TempTime < WaitTime)
                    WaitTime = TempTime;
            }

            LastWait = WaitTime;
            NextCheckTime = ThreadManager.UnityTime + WaitTime;
            return WaitTime;
        }

        return NextCheckTime-ThreadManager.UnityTime;
    }

    public override void OnInit(TerrainBlock Terrain, Vector3 Position, float Scale, Quaternion Rotation,int SpawnIndex, int ScriptIndex)
    {
        CTransform = Container.transform;

        Container.Switcher = this;
        LastRange = 0;
        NextCheckTime = 0;
        // Register this script for Thread Update
        GameObjectProcessor.AddLodSwitcher(this,Container.Spawn);
    }

    public override void OnRemove(GameObjectClientDatas.ClientGameObjectSpawn Spawn)
    {
        // Remove this script from Thread Update
        if(Spawn.Index != -1)
            GameObjectProcessor.RemoveLodSwitcher(this,Spawn);

        for (int i = Container.ScriptsCount; i >= 0; --i)
        {
            if ((object)Container.Scripts[i] != null)
                Container.Scripts[i].Disable(this);
        }
    }
}
