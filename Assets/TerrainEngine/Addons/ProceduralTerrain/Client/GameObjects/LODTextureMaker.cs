﻿using System.Collections;
using UnityEngine;


public class LODTextureMaker : MonoBehaviour
{
    public float Progress = 0f;
    public bool IsDone = false;
    public Texture2D Result;

    public int Resolution;
    public Camera Cam;
    public RenderTexture RTexture;
    public MeshFilter[] Filters;
    public Bounds bounds;
    public Vector3 Center, Extend;
    public float YOffset = 0, Y = 0;

    public void Init(Camera Cam, MeshFilter[] Filters, int Resolution, Bounds bounds)
    {
        this.Cam = Cam;
        this.Filters = Filters;
        this.Resolution = Resolution;
        this.IsDone = false;
        this.Result = null;
        this.bounds = bounds;
        Center = Vector3.zero;
        Extend = Vector3.zero;
        YOffset = 0;
        Y = 0;

        Vector3[] Vertices;
        MeshFilter OriginalFilter;
        int i = 0;

        for (int j = 0; j < Filters.Length; ++j)
        {
            Mesh SharedMesh = (OriginalFilter = Filters[j]).sharedMesh;
            if (SharedMesh == null)
            {
                Debug.LogError(OriginalFilter + " Invalid GameObject Mesh : " + name);
                continue;
            }

            Center = Filters[j].transform.rotation * SharedMesh.bounds.center;
            Vector3 TExtend = Filters[j].transform.rotation * SharedMesh.bounds.extents;
            Extend.x = Mathf.Max(Extend.x, TExtend.x);
            Extend.y = Mathf.Max(Extend.y, TExtend.y);
            Extend.z = Mathf.Max(Extend.z, TExtend.z);
            Vertices = SharedMesh.vertices;

            for (i = 0; i < Vertices.Length; ++i)
            {
                Y = (OriginalFilter.transform.rotation * Vertices[i]).y;
                if (Y < 0 && Y < YOffset)
                    YOffset = Y;
            }
        }
    }

    public void Generate()
    {
        StartCoroutine(GenerateTexture());
    }

    private IEnumerator GenerateTexture()
    {
        RTexture = RenderTexture.GetTemporary(Resolution, Resolution, 0);
        Cam.targetTexture = RTexture;
        Progress = 30f;
        yield return new WaitForEndOfFrame();

        //Cam.isOrthoGraphic = true;
        Cam.transform.position = new Vector3(bounds.center.x, bounds.center.y, -1.0f + (bounds.min.z * 2.0f));
        Cam.orthographic = true;
        Cam.nearClipPlane = 0.5f;
        Cam.farClipPlane = -Cam.transform.position.z + 10.0f + bounds.max.z;

        Cam.transform.position = new Vector3(bounds.center.x, bounds.center.y, -1.0f + (bounds.min.z * 2.0f));
        //make clip planes fairly optimal and enclose whole mesh
        Cam.nearClipPlane = 0.5f;
        Cam.farClipPlane = -Cam.transform.position.z + 10.0f + bounds.max.z;
        //set camera size to just cover entire mesh
        Cam.orthographicSize = 1.01f * Mathf.Max((bounds.max.y - bounds.min.y) / 2.0f, (bounds.max.x - bounds.min.x) / 2.0f);
        Cam.rect = new Rect(Cam.rect.x, Cam.rect.y, Cam.rect.width, Cam.rect.height);
        Cam.transform.position = new Vector3(Cam.transform.position.x, Cam.transform.position.y + Cam.orthographicSize * 0.05f, Cam.transform.position.z);//render
        Progress = 60f;
        yield return new WaitForEndOfFrame();

        Texture2D ResultTexture = new Texture2D(RTexture.width, RTexture.height);
        RenderTexture.active = Cam.targetTexture;
        ResultTexture.ReadPixels(new Rect(0, 0, RTexture.width, RTexture.height), 0, 0);
        ResultTexture.Apply();
        Progress = 80f;
        yield return new WaitForEndOfFrame();

        VectorI2 Min = new VectorI2(ResultTexture.width, ResultTexture.height), Max = VectorI2.zero;
        Color col;
        int x, y;
        for (x = 0; x < ResultTexture.width; ++x)
        {
            for (y = 0; y < ResultTexture.height; ++y)
            {
                col = ResultTexture.GetPixel(x, y);
                if (col.r >= Cam.backgroundColor.r - 0.01f && col.r <= Cam.backgroundColor.r + 0.01f
    && col.g >= Cam.backgroundColor.g - 0.01f && col.g <= Cam.backgroundColor.g + 0.01f
    && col.b >= Cam.backgroundColor.b - 0.01f && col.b <= Cam.backgroundColor.b + 0.01f)
                {
                }
                else
                {
                    Min.x = Mathf.Min(x, Min.x);
                    Min.y = Mathf.Min(y, Min.y);

                    Max.x = Mathf.Max(x, Max.x);
                    Max.y = Mathf.Max(y, Max.y);
                }
            }
        }

        VectorI2 Size = Max - Min;


        if (Size.x < 0)
            Size.x = -Size.x;
        if (Size.y < 0)
            Size.y = -Size.y;

        Result = new Texture2D(Size.x, Size.y, TextureFormat.ARGB32, false);
        for (x = 0; x < Size.x; ++x)
        {
            for (y = 0; y < Size.y; ++y)
            {
                col = ResultTexture.GetPixel(x + Min.x, y + Min.y);
                if (col.r >= Cam.backgroundColor.r - 0.01f && col.r <= Cam.backgroundColor.r + 0.01f
                    && col.g >= Cam.backgroundColor.g - 0.01f && col.g <= Cam.backgroundColor.g + 0.01f
                    && col.b >= Cam.backgroundColor.b - 0.01f && col.b <= Cam.backgroundColor.b + 0.01f)
                {
                    col.r = col.g = col.b = 0.0f;
                    col.a = 0.0f;
                }
                else
                    col.a = 0.8f;

                Result.SetPixel(x, y, col);
            }
        }

        for (int i = 0; i < 10; ++i)
        {
            for (x = 1; x < Size.x - 1; ++x)
            {
                for (y = 1; y < Size.y - 1; ++y)
                {
                    col = Result.GetPixel(x, y);
                    if (col.a == 0) continue;

                    col.a = (Result.GetPixel(x, y - 1).a
                        + Result.GetPixel(x - 1, y).a
                        + Result.GetPixel(x, y).a
                        + Result.GetPixel(x, y + 1).a
                        + Result.GetPixel(x + 1, y).a) / 5;
                    Result.SetPixel(x, y, col);
                }
            }
            yield return new WaitForEndOfFrame();
        }

        Result.Apply();

        RenderTexture.active = null;
        Cam.targetTexture = null;
        RTexture.Release();
        IsDone = true;
    }
}
