using TerrainEngine;
using UnityEngine;

public class GS_ActiveDisableEvent : IGameObjectSpawnScript 
{
    public ActiveDisableContainer InitEvents;
    public ActiveDisableContainer RemoveEvents;

    public override void OnInit(TerrainBlock Terrain, Vector3 Position, float Scale, Quaternion Rotation, int SpawnIndex, int Index)
    {
        if (InitEvents != null)
            InitEvents.Call();
    }

    public override void OnRemove(GameObjectClientDatas.ClientGameObjectSpawn Spawn)
    {
        if(RemoveEvents != null)
            RemoveEvents.Call();
    }
}
