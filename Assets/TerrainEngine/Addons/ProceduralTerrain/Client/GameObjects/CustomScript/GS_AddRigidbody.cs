﻿using TerrainEngine;
using UnityEngine;

public class GS_AddRigidbody : IGameObjectSpawnScript
{
    public PhysicMaterial Physic;
    public GameObject ToInstanciateOnActive;
    public bool DeleteFromVoxelsWhenActive = true;

    public bool IsActive = false;
    private TerrainObject TObject;
    private Rigidbody Body;

    public override void SetActive()
    {
        IsActive = true;

        if (Body == null)
        {
            Collider Col = Container.gameObject.GetComponentInChildren<Collider>();
            if (Col != null)
                Col.material = Physic;

            if (TObject == null)
                TObject = Container.gameObject.AddComponent<TerrainObject>();

            TerrainObjectColliderViewer.Add(TObject);
            TObject.enabled = true;

            Body = GetComponent<Rigidbody>();

            if (Body == null)
            {
                Body = Container.gameObject.AddComponent<Rigidbody>();
                Body.mass = 1000;
                Body.drag = 0;
                Body.angularDrag = 0;
                Body.maxAngularVelocity = 1.5f;
                Body.useGravity = true;
                Body.centerOfMass = Vector3.up * 5;
            }

            if (ToInstanciateOnActive != null)
                GameObject.Instantiate(ToInstanciateOnActive, transform.position, transform.rotation);
        }
    }

    public override void OnRemove(GameObjectClientDatas.ClientGameObjectSpawn Spawn)
    {
        IsActive = false;

        if (TObject != null)
            TObject.enabled = false;

        if (Body != null)
            GameObject.Destroy(Body);
    }

    public override void Active(bool IsNetwork)
    {
        if (DeleteFromVoxelsWhenActive && IsNetwork)
        {
            GameObjectClientDatas.ClientGameObjectSpawn S = Container.Spawn;
            if (S.Index != -1)
                Container.Data.SetByte(S.x, S.y, S.z, 0);
        }
        SetActiveThread();
        base.Active(IsNetwork);
    }

    public override bool OnVoxelChange(VoxelFlush Flush)
    {
        if (Flush.NewType == 0 || Flush.NewVolume <= TerrainInformation.Isolevel)
            SetActiveThread();

        return false;
    }
}
