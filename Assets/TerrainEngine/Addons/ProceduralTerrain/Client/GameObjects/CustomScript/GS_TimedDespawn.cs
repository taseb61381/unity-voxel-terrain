using TerrainEngine;
using UnityEngine;

public class GS_TimedDespawn : IGameObjectSpawnScript 
{
    public float DespawnTime = 30f;
    public bool DeleteFromVoxels = false; // Remove permanantly the gameobject from the world.
    private float NextDespawn = 0;

    public void Update()
    {
        if (NextDespawn == 0)
            return;

        if (NextDespawn <= Time.realtimeSinceStartup)
        {
            NextDespawn = 0;
            GameObjectClientDatas.ClientGameObjectSpawn S = Container.Spawn;
            Container.Despawn(true, false, S);
            if (DeleteFromVoxels)
            {
                if (S.Index != -1)
                    Container.Processor.ModifyPoint(Container.Terrain, S.x, S.y, S.z, 0, 0, 0, false, IBlockProcessor.ProcessorVoxelChangeType.SET, false);
                Container.Processor.OnVoxelFlushDone(ActionProcessor.Instance.UnityThread, null);
            }
        }
    }

    public override void OnRemove(GameObjectClientDatas.ClientGameObjectSpawn Spawn)
    {
        NextDespawn = 0;
    }

    public override void Active(bool IsNetwork)
    {
        NextDespawn = Time.realtimeSinceStartup + DespawnTime;
        base.Active(IsNetwork);
    }

    public override bool OnVoxelChange(VoxelFlush Flush)
    {
        if (Flush.NewType == 0 || Flush.NewVolume <= TerrainInformation.Isolevel)
        {
            NextDespawn = Time.realtimeSinceStartup + DespawnTime;
            return true;
        }

        return false;
    }
}
