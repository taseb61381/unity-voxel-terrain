﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class GS_AddCollider : GS_LOD
{
    public Vector3 Center;
    public float Radius, Height;
    public CapsuleCollider Collider;

    public override void Active(GS_LODSwitcher Switcher, float Range)
    {
        if (Collider == null)
        {
            Collider = gameObject.GetComponent<CapsuleCollider>();
            if (Collider == null)
            {
                Collider = gameObject.AddComponent<CapsuleCollider>();
            }
            Collider.center = Center;
            Collider.radius = Radius;
            Collider.height = Height;
        }

        Collider.enabled = true;

        base.Active(Switcher,Range);
    }

    public override void Active(bool IsNetwork)
    {
        if (Collider == null)
        {
            Collider = gameObject.GetComponent<CapsuleCollider>();
            if (Collider == null)
            {
                Collider = gameObject.AddComponent<CapsuleCollider>();
            }
            Collider.center = Center;
            Collider.radius = Radius;
            Collider.height = Height;
        }

        Collider.enabled = true;

        base.Active(IsNetwork);
    }

    public override void Disable(bool IsNetwork)
    {
        if (Collider != null)
        {
            Collider.enabled = false;
        }
        base.Disable(IsNetwork);
    }
}