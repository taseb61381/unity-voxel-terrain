﻿using System;
using TerrainEngine;
using UnityEngine;

public class IGameObjectsScriptsContainer : MonoBehaviour
{
    public string OriginalName;
    public int SpawnIndex;
    public int BlockId;
    public int CustomIndex;
    public IGameObjectSpawnScript[] Scripts;
    public GS_LODSwitcher Switcher;
    public int ScriptsCount;

    public GameObjectClientDatas GetClientData()
    {
        return Terrain.Voxels.Customs.array[CustomIndex] as GameObjectClientDatas;
    }

    public GameObjectClientDatas.ClientGameObjectSpawn Spawn
    {
        get
        {
            return GetClientData().Objects.array[SpawnIndex];
        }
    }

    public TerrainBlock Terrain
    {
        get
        {
            return TerrainManager.Instance.Container.BlocksArray[BlockId] as TerrainBlock;
        }
    }

    public GameObjectDatas Data
    {
        get
        {
            return GetClientData().Data;
        }
    }
    public GameObjectProcessor Processor
    {
        get
        {
            return GetClientData().Processor as GameObjectProcessor;
        }
    }

    public void Init(TerrainBlock Terrain, GameObjectClientDatas ClientData,int SpawnIndex,
        Vector3 Position, float Scale, Quaternion Rotation, string OriginalName)
    {
        this.BlockId = Terrain.BlockId;
        this.CustomIndex = ClientData.CustomIndex; 
        this.OriginalName = OriginalName;
        this.SpawnIndex = SpawnIndex;

        if (Scripts != null)
        {
            for (int i = Scripts.Length-1; i >= 0; --i)
            {
                if ((object)Scripts[i] == null)
                    continue;

                Scripts[i].Container = this;
                Scripts[i].OnInit(Terrain, Position, Scale, Rotation, SpawnIndex, i);
            }
        }
    }

    public void Despawn(bool UsePool, bool Destroy,GameObjectClientDatas.ClientGameObjectSpawn Spawn, bool RemoveFromClientData=true, bool RemoveFromData=true)
    {
        if (UsePool)
        {
            GameObjectPoolRemoveAction Action = PoolManager.Pool.GetData<GameObjectPoolRemoveAction>("GameObjectPoolRemoveAction");
            Action.Init(Processor, gameObject);
            ActionProcessor.AddToUnity(Action);
        }
        else
        {
            if (Scripts != null)
            {
                IGameObjectSpawnScript Script = null;
                for (int i = Scripts.Length - 1; i >= 0; --i)
                {
                    if ((Script = Scripts[i]) != null)
                        Script.OnRemove(Spawn);
                }
            }


            if (RemoveFromClientData && Spawn.Index != -1)
            {
                GetClientData().SetObject(Spawn.x, Spawn.y, Spawn.z, null, null);
            }

            if (RemoveFromData && Spawn.Index != -1)
            {
                if (Data != null)
                    Data.SetByte(Spawn.x, Spawn.y, Spawn.z, 0);
            }

            if (Destroy)
                GameObject.Destroy(gameObject);
        }
    }

    public virtual bool OnVoxelChange(VoxelFlush Flush)
    {
        bool Result = false;

        if (Scripts != null)
        {
            for (int i = Scripts.Length-1; i >= 0; --i)
                if ((object)Scripts[i] != null && Scripts[i].OnVoxelChange(Flush))
                    Result = true;
        }

        return Result;
    }
}