﻿using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    public class GameObjectPoolRemoveListAction : IAction
    {
        public GameObjectProcessor Processor;
        public FList<GameObjectClientDatas.ClientGameObjectSpawn> Objects = new FList<GameObjectClientDatas.ClientGameObjectSpawn>(2);
        public FList<IGameObjectsScriptsContainer> GameObjects = new FList<IGameObjectsScriptsContainer>(2);

        public int GoPool = -1;
        public int CurrentIndex = 0;
        public bool RemoveFromClientData = true;

        public void Init(GameObjectProcessor Processor)
        {
            this.Processor = Processor;
        }

        public void Init(int Pool)
        {
            this.GoPool = Pool;
        }

        public override bool OnUpdate(ActionThread Thread)
        {
            GameObjectClientDatas.ClientGameObjectSpawn Spawn;
            IGameObjectsScriptsContainer Container = null;
            while (CurrentIndex < Objects.Count && Thread.HasTime())
            {
                Spawn = Objects.array[CurrentIndex];
                Container = GameObjects.array[CurrentIndex];
                Spawn.Index = -1;
                ++CurrentIndex;

                if (Container != null)
                {
                    if (GoPool == -1)
                    {
                        Container.Despawn(false, false, Spawn, RemoveFromClientData);
                        GameObjectPool.CloseGameObject(Container.OriginalName, Container.gameObject);
                    }
                    else
                        GameObjectPool.Pools.array[GoPool].CloseObject(Container.gameObject);
                }
            }

            if (CurrentIndex < Objects.Count)
                return false;

            CurrentIndex = 0;
            Objects.Clear();
            Close();
            return true;
        }

        public override void Clear()
        {
            CurrentIndex = 0;
            GameObjects.Clear();
            Objects.Clear();
            Processor = null;
            GoPool = -1;
        }
    }
}
