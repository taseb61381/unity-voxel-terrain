﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    public class DefaultTreeGenerator : IWorldGenerator
    {
        static public int Seed = 0;
        static public FastRandom FRandom;
        static public List<byte[, ,]> Trees = new List<byte[, ,]>();
        public int GroundOffset;

        public override void Init()
        {
            int Seed = World.Information.IntSeed;

            if (FRandom == null)
            {
                FRandom = new FastRandom(Seed);
                DefaultTreeGenerator.Seed = Seed;

                for (int i = 0; i < 50; ++i)
                {
                    Trees.Add(CreateTree(0, 50 % 4));
                }
            }
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            if (ChunkId != 0)
                return true;

            FastRandom Random = new FastRandom(DefaultTreeGenerator.Seed + (int)Block.Position.x * (int)Block.Position.z - (int)Block.Position.y);
            int[] Ground = Data.GetGrounds(Block.Information.VoxelsPerAxis, Block.Information.VoxelsPerAxis);

            int x, z, f = 0;
            VectorI3 Pos = new VectorI3();
            int WorldY = Block.WorldY;
            for (x = 0; x < Block.Information.VoxelsPerAxis; ++x)
            {
                for (z = 0; z < Block.Information.VoxelsPerAxis; ++z,++f)
                {
                    if ((Pos.y = Ground[f]) == -1)
                        continue;

                    Pos.x = x;
                    Pos.z = z;

                    if (Random.randomIntAbs(200) >= 199)
                    {
                        Ground[f] = -1;

                        if (Block.WorldY + Pos.y < World.Information.WorldGroundLevel + GroundOffset)
                            continue;

                        if (Pos.y >= 3 && Block.GetType(Pos.x, Pos.y - WorldY, Pos.z) == BlockManager.Grass)
                            Decorate(Pos.x, Pos.y - WorldY, Pos.z, Block, Random);
                    }
                }
            }

            return true;
        }

        public void Decorate(int x, int y, int z, TerrainDatas Block, FastRandom Random)
        {
            int tree = Random.randomIntAbs(Trees.Count - 2);
            byte[, ,] Tree;
            int sx, sz;
            for (int i = tree; i < tree + 2; ++i)
            {
                Tree = Trees[i];
                sx = Tree.GetLength(0) / 2;
                sz = Tree.GetLength(2) / 2;
                if (HasSpace(x - sx, y + 1, z - sz, Tree, Block))
                {
                    Block.Set(x - sx, y - 2, z - sz, Tree);
                    break;
                }
            }
        }

        public bool HasSpace(int x, int y, int z, byte[, ,] Tree, TerrainDatas Block)
        {
            int Width = Tree.GetLength(0);
            int Height = Tree.GetLength(1);
            int Deph = Tree.GetLength(2);

            if (Width + x >= Block.Information.VoxelsPerAxis || x <= 0)
                return false;

            if (Height + y - 1 >= Block.Information.VoxelsPerAxis || y <= 0)
                return false;

            if (Deph + z >= Block.Information.VoxelsPerAxis || z <= 0)
                return false;

            /*for (int px = 0; px < Width; ++px)
                for (int py = 0; py < Height; ++py)
                    for (int pz = 0; pz < Deph; ++pz)
                    {
                        if (Tree[px, py, pz] != BlockManager.None)
                        {
                            if (Block.GetByte(px + x, py + y, pz + z, false) != BlockManager.None)
                                return false;
                        }
                    }*/

            return true;
        }

        static public byte[, ,] CreateTree(byte ID, int Size)
        {
            List<SimpleVoxelPoint> Points = new List<SimpleVoxelPoint>();

            int TronSize = Size;
            int TronHeight = 8 + Size + FRandom.randomIntAbs(Size * 2);
            int LeaveHeight = TronSize + 2;
            int LeavesCount = Size - 1;
            int x, y, z, i,px,py,pz;
            // Ici le somet avec les feuilles
            Vector3 p = new Vector3();
            {
                px = TronSize / 2;
                py = TronHeight;
                pz = TronSize / 2;

                for (x = 0 - LeaveHeight; x <= 0 + LeaveHeight; x++)
                {
                    p.x = x;
                    for (y = 0 - LeaveHeight; y <= 0 + LeaveHeight; y++)
                    {
                        p.y = y;
                        for (z = 0 - LeaveHeight; z <= 0 + LeaveHeight; z++)
                        {
                            p.z = z;
                            if (Vector3.Distance(Vector3.zero, p) <= LeaveHeight)
                            {
                                Set(px + x, py + y, pz + z, BlockManager.Leaves, ref Points);
                            }
                        }
                    }
                }

            }

            int dir;
            VectorI3 Direction;
            for (i = 0; i < LeavesCount; ++i)
            {
                dir = FRandom.randomIntAbs(4);
                if (dir == 2 || dir == 3)
                    dir += 2;

                Direction = BlockHelper.direction[dir];

                px = Direction.x * LeaveHeight / 2 + TronSize / 2;
                py = Direction.y + TronHeight - LeaveHeight / 2;
                pz = Direction.z * LeaveHeight / 2 + TronSize / 2;

                for (x = 0 - LeaveHeight; x <= 0 + LeaveHeight; x++)
                {
                    p.x = x;
                    for (y = 0 - LeaveHeight; y <= 0 + LeaveHeight; y++)
                    {
                        p.y = y;
                        for (z = 0 - LeaveHeight; z <= 0 + LeaveHeight; z++)
                        {
                            p.z = z;
                            if (Vector3.Distance(Vector3.zero, p) <= LeaveHeight)
                            {
                                Set(px + x, py + y, pz + z, BlockManager.Leaves, ref Points);
                            }
                        }
                    }
                }
            }

            // Ici on contruit le tron droit de l'arbre
            for (x = 0; x < TronSize; ++x)
                for (y = 0; y < TronHeight + 1; ++y)
                    for (z = 0; z < TronSize; ++z)
                        Set(x, y, z, BlockManager.Wood, ref Points);

            // Ici la base du tron en escalier
            int len;
            for (y = 0; y < Size; ++y)
            {
                len = Size - y;

                for (x = -len; x < TronSize + len; ++x)
                    for (z = 0; z < TronSize; ++z)
                    {
                        Set(x, y - Size, z, BlockManager.Wood, ref Points);
                    }

                for (z = -len; z < TronSize + len; ++z)
                    for (x = 0; x < TronSize; ++x)
                    {
                        Set(x, y - Size, z, BlockManager.Wood, ref Points);
                    }
            }

            return ConvertToBytes(ref Points);
        }

        static public void Set(int x, int y, int z, byte Type, ref List<SimpleVoxelPoint> Points)
        {
            for (int i = 0; i < Points.Count; ++i)
                if (Points[i].x == x && Points[i].y == y && Points[i].z == z)
                {
                    Points[i] = new SimpleVoxelPoint(x, y, z, Type);
                    return;
                }

            Points.Add(new SimpleVoxelPoint(x, y, z, Type));
        }

        static public byte Get(int x, int y, int z, ref List<SimpleVoxelPoint> Points)
        {
            foreach (SimpleVoxelPoint pt in Points)
                if (pt.x == x && pt.y == y && pt.z == z)
                    return pt.Type;

            return BlockManager.None;
        }

        static public byte[, ,] ConvertToBytes(ref List<SimpleVoxelPoint> Points)
        {
            VectorI3 MinValues = new VectorI3();
            VectorI3 MaxValues = new VectorI3();

            foreach (SimpleVoxelPoint pt in Points)
            {
                if (pt.x < MinValues.x) MinValues.x = pt.x;
                if (pt.y < MinValues.y) MinValues.y = pt.y;
                if (pt.z < MinValues.z) MinValues.z = pt.z;

                if (pt.x > MaxValues.x) MaxValues.x = pt.x;
                if (pt.y > MaxValues.y) MaxValues.y = pt.y;
                if (pt.z > MaxValues.z) MaxValues.z = pt.z;
            }

            VectorI3 AbsMinValues = new VectorI3(Math.Abs(MinValues.x), Math.Abs(MinValues.y), Math.Abs(MinValues.z));
            byte[, ,] Tree = new byte[AbsMinValues.x + MaxValues.x + 1, AbsMinValues.y + MaxValues.y + 1, AbsMinValues.z + MaxValues.z + 1];

            foreach (SimpleVoxelPoint pt in Points)
                Tree[pt.x + AbsMinValues.x, pt.y + AbsMinValues.y, pt.z + AbsMinValues.z] = pt.Type;

            return Tree;
        }
    }
}
