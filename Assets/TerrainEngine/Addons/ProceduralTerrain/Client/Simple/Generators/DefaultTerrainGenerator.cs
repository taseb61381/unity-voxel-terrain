﻿using UnityEngine;

namespace TerrainEngine
{
    public class DefaultTerrainGenerator : IWorldGenerator
    {
        private static int SAMPLE_RATE_3D_HOR = 8;
        private static int SAMPLE_RATE_3D_VERT = 8;

        private PerlinNoise _pGen1, _pGen2, _pGen3, _pGen4, _pGen5, _pGen8;

        public float MountainLevel = 6;
        public int GroundLevel;
        public int GroundOffset;

        private IWorldBiome biomeProvider;

        public override void Init()
        {
            base.Init();

            int seed = World.Information.IntSeed;
            if (_pGen1 == null)
            {
                _pGen1 = new PerlinNoise(seed);
                _pGen1.setOctaves(2); // 8 Default

                _pGen2 = new PerlinNoise(seed + 1);
                _pGen2.setOctaves(2); // 8 Default

                _pGen3 = new PerlinNoise(seed + 2);
                _pGen3.setOctaves(2); // 8 Default

                _pGen4 = new PerlinNoise(seed + 3);
                _pGen4.setOctaves(2);  // NULL Default

                _pGen5 = new PerlinNoise(seed + 4);
                _pGen5.setOctaves(2); // NULL Default

                _pGen8 = new PerlinNoise(seed + 7);
                _pGen8.setOctaves(2);// NULL Default

                biomeProvider = new WorldBiome(seed);
            }
        }

        static public int GetFlatten(int x, int y, int z,int DensitySize,int DensitySizeY)
        {
            return z + y * DensitySize + x * DensitySizeY * DensitySize;
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            GroundLevel = (int)(World.Information.WorldGroundLevel / World.Information.Scale);
            int SIZE = Block.Information.VoxelPerChunk;
            int SIZE_Y = Block.Information.VoxelsPerAxis;
            int StartX = ChunkX * Information.VoxelPerChunk;
            int StartZ = ChunkZ * Information.VoxelPerChunk;

            int DensitySize = SIZE + SAMPLE_RATE_3D_HOR;
            int DensitySizeY = Block.Information.VoxelsPerAxis + SAMPLE_RATE_3D_VERT;

            float[] Density = Data.GetCustomData("DensityMap") as float[];
            if (Density == null)
            {
                Density = new float[(SIZE + SAMPLE_RATE_3D_HOR) * (SIZE_Y + SAMPLE_RATE_3D_VERT) * (SIZE + SAMPLE_RATE_3D_HOR)];
                Data.SetCustomData("DensityMap", Density);
            }

            if (Data.InternalState == 0)
            {
                //TerrainManager.Log("Generate : Step 1 " + Block.Position);
                int WorldX, WorldY, WorldZ, plateauArea;
                float height, river, temp, humidity, mIntens, densityMountains, densityHills, flatten;
                Vector2 DistanceMountains = new Vector2();

                int x, y, z;
                for (x = 0; x <= SIZE; x += SAMPLE_RATE_3D_HOR)
                {
                    WorldX = Block.WorldX + x + StartX;
                    for (z = 0; z <= SIZE; z += SAMPLE_RATE_3D_HOR)
                    {
                        WorldZ = Block.WorldZ + z + StartZ;
                        for (y = 0; y <= SIZE_Y; y += SAMPLE_RATE_3D_VERT)
                        {
                            WorldY = Block.WorldY + y - GroundLevel;

                            height = calcBaseTerrain(WorldX, WorldZ);
                            river = calcRiverTerrain(WorldX, WorldZ);

                            temp = biomeProvider.getTemperatureAt(WorldX, WorldZ);
                            humidity = biomeProvider.getHumidityAt(WorldX, WorldZ) * temp;
                            DistanceMountains.x = temp - 0.25f;
                            DistanceMountains.y = humidity - 0.35f;

                            mIntens = MathHelper.clamp(1.0f - DistanceMountains.magnitude * 3.0f);

                            densityMountains = calcMountainDensity(WorldX, WorldY, WorldZ) * mIntens;
                            densityHills = calcHillDensity(WorldX, WorldY, WorldZ) * (1.0f - mIntens);

                            plateauArea = (int)(SIZE_Y * MountainLevel * 0.10f);
                            flatten = MathHelper.clamp(((SIZE_Y * MountainLevel) - WorldY) / plateauArea);

                            Density[GetFlatten(x, y, z,DensitySize,DensitySizeY)] = (float)(-WorldY + (((32.0f + height * 32.0f) * MathHelper.clamp(river + 0.25f) * MathHelper.clamp(0 + 0.25f)) + densityMountains * 1024.0f + densityHills * 128.0f) * flatten);
                        }
                    }
                }

                Data.InternalState = 1;
                return false;
            }
            else if (Data.InternalState == 1)
            {
                triLerpDensityMap(Density, new VectorI3(0, 0, 0), new VectorI3(SIZE, SIZE_Y, SIZE),DensitySize,DensitySizeY);
                Data.InternalState = 2;
                return false;
            }
            else
            {
                int[] Grounds = Data.GetGrounds(SIZE, SIZE);

                //TerrainManager.Log("Generate : Step 2 " + Block.Position);
                Data.InternalState = 0;

                int firstBlockHeight = 0;
                Biome type = Biome.MOUNTAINS;
                bool FirstGrass = true;
                float dens = 0;
                int WorldX, WorldZ, f = 0;

                int x, y, z;
                for (x = 0; x < SIZE; x++)
                {
                    WorldX = Block.WorldX + x + StartX;
                    for (z = 0; z < SIZE; z++, ++f)
                    {
                        WorldZ = Block.WorldZ + z + StartZ;

                        Grounds[f] = -1;
                        type = biomeProvider.getBiomeAt(WorldX, WorldZ);
                        firstBlockHeight = -1;

                        FirstGrass = true;

                        for (y = SIZE_Y - 1; y >= 0; y--)
                        {
                            dens = Density[GetFlatten(x, y, z, DensitySize, DensitySizeY)];

                            if ((dens >= 0 && dens < 32))
                            {
                                if (firstBlockHeight == -1)
                                    firstBlockHeight = y;

                                if (calcCaveDensity(WorldX, y, WorldZ) > -0.7)
                                    GenerateOuterLayer(StartX + x, y, StartZ + z,f, firstBlockHeight, Block, type, Data, ref FirstGrass);
                            }
                            else if (dens >= 32)
                            {
                                if (firstBlockHeight == -1)
                                    firstBlockHeight = y;

                                if (calcCaveDensity(WorldX, y, WorldZ) > -0.6)
                                    GenerateInnerLayer(StartX + x, y, StartZ + z, Block, type);
                            }
                            else
                                firstBlockHeight = -1;
                        }
                    }
                }

                return true;
            }
        }

        public bool MustRemove(TerrainDatas c, byte Type, int x, int z, int y)
        {
            byte t0 = c.GetType(x - 1, y, z);
            byte t1 = c.GetType(x + 1, y, z);

            byte t2 = c.GetType(x, y, z - 1);
            byte t3 = c.GetType(x, y, z + 1);

            if (t0 != 0 && t0 == Type)
                return false;

            if (t1 != 0 && t1 == Type)
                return false;

            if (t2 != 0 && t2 == Type)
                return false;

            if (t3 != 0 && t3 == Type)
                return false;

            return true;
        }

        public bool IsOnMountain(int SIZE, ref int[,] hs, int x, int z)
        {
            int px, pz;
            for (px = x - 1; px < x + 2; ++px)
                for (pz = z - 1; pz < z + 2; ++pz)
                {
                    if (px < 0 || pz < 0 || px >= SIZE || pz >= SIZE)
                        continue;

                    if (hs[px, pz] != hs[x, z] && hs[x, z] != -1)
                        return true;
                }

            return false;
        }

        private void GenerateInnerLayer(int x, int y, int z, TerrainDatas c, Biome type)
        {
            c.SetTypeAndVolume(x, y, z, BlockManager.Stone, 1);
        }

        private void GenerateOuterLayer(int x, int y, int z, int f, int firstBlockHeight, TerrainDatas c, Biome type, BiomeData Data, ref bool FirstGrass)
        {
            int depth = (firstBlockHeight - y);
            int WorldY = c.GetWorldY(y);

            switch (type)
            {
                case Biome.FOREST:
                case Biome.PLAINS:
                case Biome.MOUNTAINS:
                    // Beach
                    /*if (y >= 28 && y <= 34)
                    {
                        c.setBlock(x, y, z, BlockManager.getBlock("Sand"));
                    }
                    else*/
                    if (depth == 0 && WorldY >= GroundLevel && WorldY < GroundLevel + 128)
                    {
                        c.SetTypeAndVolume(x, y, z, BlockManager.Grass, 1);

                        if (FirstGrass)
                        {
                            FirstGrass = false;
                            Data.Grounds[f] = WorldY;
                        }
                    }
                    else if (depth == 0 && WorldY >= GroundLevel + 128)
                    {
                        // Grass on top
                        c.SetTypeAndVolume(x, y, z, BlockManager.Snow, 1);
                    }
                    else if (depth > 32)
                    {
                        // Stone
                        c.SetTypeAndVolume(x, y, z, BlockManager.Stone, 1);
                    }
                    else
                    {
                        // Dirt
                        c.SetTypeAndVolume(x, y, z, BlockManager.Dirt, 1);
                    }

                    break;
                case Biome.SNOW:
                    if (depth == 0 && WorldY > GroundLevel + 32)
                    {
                        // Snow on top
                        c.SetTypeAndVolume(x, y, z, BlockManager.Snow, 1);
                    }
                    else if (depth > 32)
                    {
                        // Stone
                        c.SetTypeAndVolume(x, y, z, BlockManager.Stone, 1);
                    }
                    else
                    {
                        // Dirt
                        c.SetTypeAndVolume(x, y, z, BlockManager.Grass, 1);
                    }

                    break;

                case Biome.DESERT:
                    if (depth > 8)
                    {
                        // Stone
                        c.SetTypeAndVolume(x, y, z, BlockManager.Stone, 1);
                    }
                    else
                    {
                        c.SetTypeAndVolume(x, y, z, BlockManager.Sand, 1);
                    }

                    break;
            }
        }

        static private void triLerpDensityMap(float[] densityMap, VectorI3 Start, VectorI3 End,int DensitySize,int DensitySizeY)
        {
            int offsetX, offsetY, offsetZ;
            int x, y, z;
            for (x = Start.x; x < End.x; x++)
            {
                offsetX = (x / SAMPLE_RATE_3D_HOR) * SAMPLE_RATE_3D_HOR;
                for (y = Start.y; y < End.y; y++)
                {
                    offsetY = (y / SAMPLE_RATE_3D_VERT) * SAMPLE_RATE_3D_VERT;
                    for (z = Start.z; z < End.z; z++)
                    {
                        if (!(x % SAMPLE_RATE_3D_HOR == 0 && y % SAMPLE_RATE_3D_VERT == 0 && z % SAMPLE_RATE_3D_HOR == 0))
                        {
                            offsetZ = (z / SAMPLE_RATE_3D_HOR) * SAMPLE_RATE_3D_HOR;

                            densityMap[GetFlatten(x, y, z, DensitySize, DensitySizeY)] = MathHelper.triLerpf(x, y, z,
                                densityMap[GetFlatten(offsetX, offsetY, offsetZ, DensitySize, DensitySizeY)],
                                densityMap[GetFlatten(offsetX, SAMPLE_RATE_3D_VERT + offsetY, offsetZ, DensitySize, DensitySizeY)],
                                densityMap[GetFlatten(offsetX, offsetY, offsetZ + SAMPLE_RATE_3D_HOR, DensitySize, DensitySizeY)],
                                densityMap[GetFlatten(offsetX, offsetY + SAMPLE_RATE_3D_VERT, offsetZ + SAMPLE_RATE_3D_HOR, DensitySize, DensitySizeY)],
                                densityMap[GetFlatten(SAMPLE_RATE_3D_HOR + offsetX, offsetY, offsetZ, DensitySize, DensitySizeY)],
                                densityMap[GetFlatten(SAMPLE_RATE_3D_HOR + offsetX, offsetY + SAMPLE_RATE_3D_VERT, offsetZ, DensitySize, DensitySizeY)],
                                densityMap[GetFlatten(SAMPLE_RATE_3D_HOR + offsetX, offsetY, offsetZ + SAMPLE_RATE_3D_HOR, DensitySize, DensitySizeY)],
                                densityMap[GetFlatten(SAMPLE_RATE_3D_HOR + offsetX, offsetY + SAMPLE_RATE_3D_VERT, offsetZ + SAMPLE_RATE_3D_HOR, DensitySize, DensitySizeY)],
                                offsetX, SAMPLE_RATE_3D_HOR + offsetX, offsetY, SAMPLE_RATE_3D_VERT + offsetY, offsetZ, offsetZ + SAMPLE_RATE_3D_HOR);
                        }
                    }
                }
            }
        }

        private float calcBaseTerrain(float x, float z)
        {
            return MathHelper.clamp((_pGen1.fBm(0.004f * x, 0, 0.004f * z) + 1.0f) / 2.0f);
        }

        private float calcOceanTerrain(float x, float z)
        {
            return MathHelper.clamp(_pGen2.fBm(0.0009f * x, 0, 0.0009f * z) * 8.0f);
        }

        private float calcRiverTerrain(float x, float z)
        {
            return MathHelper.clamp(
                (
                Mathf.Sqrt(
                    Mathf.Abs(
                        _pGen3.fBm(0.0008f * x, 0, 0.0008f * z)
                        )
                    ) - 0.1f)
                * 7.0f);
        }

        private float calcMountainDensity(float x, float y, float z)
        {
            float x1, y1, z1;

            x1 = x * 0.002f;
            y1 = y * 0.001f;
            z1 = z * 0.002f;

            float result = _pGen4.fBm(x1, y1, z1);

            return result > 0.0 ? result : 0;
        }

        private float calcHillDensity(float x, float y, float z)
        {
            float x1, y1, z1;

            x1 = x * 0.008f;
            y1 = y * 0.006f;
            z1 = z * 0.008f;

            float result = _pGen5.fBm(x1, y1, z1) - 0.1f;

            return result > 0.0 ? result : 0;
        }

        private float calcCaveDensity(float x, float y, float z)
        {
            return _pGen8.fBm(x * 0.02f, y * 0.02f, z * 0.02f);
        }
    }
}
