using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Biomes/Perlin")]
public class PerlinBiome : IBlockProcessor
{
    static public PerlinNoise PNoise;

    public override void OnInitSeed(int seed)
    {
        if (PNoise == null)
            PNoise = new PerlinNoise(seed);

        SimplexNoise.Initialize();
    }

    public override bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        if (ChunkId != 0)
            return true;

        float noise = 0f;

        float px = 0;
        float py = 0;
        float pz = 0;

        int blockX = 0;
        int blockZ = 0;

        for (int x = 0; x < Block.Information.VoxelsPerAxis; x++)
        {
            blockX = Block.WorldX + x;

            for (int z = 0; z < Block.Information.VoxelsPerAxis; z++)
            {
                blockZ = Block.WorldZ + z;

                for (int y = Block.Information.VoxelsPerAxis - 1; y >= 0; y--)
                {
                    px = blockX;
                    py = Block.WorldY + y;
                    pz = blockZ;

                    noise = (float)SimplexNoise.Noise(px * 0.1f, py * 0.1f, pz * 0.1f) * 0.5f;
                    noise += (float)SimplexNoise.Noise(px * .025f, py * .025f, pz * .025f) * 0.5f;
                    noise += 0.5f;


                    Block.SetTypeAndVolume(x, y, z, BlockManager.Grass, noise);

                }
            }
        }

        return true;
    }
}
