using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Biomes/Simplex")]
public class SimplexBiome : IBlockProcessor
{
    public float DetailSize = 0.004f;
    public float HeightScale = 0.5f;

    public override void OnInitSeed(int seed)
    {
        ImprovedPerlinNoise.Initialize();
        SimplexNoise.Initialize();
    }

    public override bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        if (ChunkId != 0)
            return true;

        int GroundLevel = (int)(TerrainManager.Instance.Informations.WorldGroundLevel / Block.Information.Scale);
        int blockX = 0;
        int blockZ = 0;
        int WorldY = 0;
        float Height = 0;
        float Noise = 0;
        float pnoise = 0;
        int[] Grounds = Data.GetGrounds(Block.Information.VoxelsPerAxis, Block.Information.VoxelsPerAxis);

        int x, y, z, f = 0;
        for (x = 0; x < Block.Information.VoxelsPerAxis; x++)
        {
            blockX = Block.WorldX + x;

            for (z = 0; z < Block.Information.VoxelsPerAxis; z++,++f)
            {
                blockZ = Block.WorldZ + z;

                Noise = (float)SimplexNoise.Noise(blockX * DetailSize, 0, blockZ * DetailSize);

                if (Noise > 0)
                    Height = Block.Information.VoxelsPerAxis * 2 * Noise;
                else
                    Height = Block.Information.VoxelsPerAxis * (Noise * Noise);

                Height *= HeightScale;
                Height += GroundLevel;

                for (y = 0; y < Block.Information.VoxelsPerAxis; ++y)
                {
                    WorldY = Block.WorldY + y;

                    if (WorldY < Height)
                    {
                        Grounds[f] = y;
                        pnoise = (float)SimplexNoise.Noise(blockX * 0.05f, WorldY * 0.05f, blockZ * 0.05f);

                        if (pnoise > 0f)
                            Block.SetType(x, y, z, BlockManager.Grass);
                        else
                            Block.SetType(x, y, z, BlockManager.Stone);
                        
                        //Block.SetFloat(x, y, z, Mathf.Clamp((Height - (float)WorldY), 0, 1f) - (pnoise + pnoise + 0.4f) * 0.5f);
                    }

                   /* if (WorldY < Height - 1)
                    {
                        float pnoise = PNoise.noise(blockX * 0.04f, WorldY * 0.04f, blockZ * 0.04f);

                        if (pnoise < -0.5f)
                        {
                            Block.SetByte(x, y, z, BlockManager.None);
                        }
                        else if (pnoise < -0.4)
                        {
                            Block.SetByte(x, y, z, BlockManager.Stone);
                            Block.SetFloat(x, y, z, 1f);
                        }
                        else
                        {
                            Block.SetByte(x, y, z, BlockManager.Grass);
                            Block.SetFloat(x, y, z, 1f);
                        }
                    }
                    else if (WorldY >= Height - 1 && WorldY <= Height + 1)
                    {
                        float pnoise = PNoise.noise(blockX * 0.04f, WorldY * 0.04f, blockZ * 0.04f);

                        if (pnoise < -0.5f)
                        {
                            Block.SetByte(x, y, z, BlockManager.None);
                        }
                        else if (pnoise < -0.2f)
                        {
                            Block.SetByte(x, y, z, BlockManager.Dirt);
                            Block.SetFloat(x, y, z, Height - (float)WorldY);
                        }
                        else
                        {
                            Block.SetByte(x, y, z, BlockManager.Grass);
                            Block.SetFloat(x, y, z, Height - (float)WorldY);
                        }

                        float pnoise = PNoise.noise(blockX * 0.04f, WorldY * 0.04f, blockZ * 0.04f);

                        if (pnoise < -0.5f)
                        {
                            Block.SetByte(x, y, z, BlockManager.None);
                            //Block.SetFloat(x, y, z, Height - (float)WorldY + pnoise);
                        }
                        else
                        {
                            Block.SetByte(x, y, z, BlockManager.Grass);
                            Block.SetFloat(x, y, z, Height - (float)WorldY);
                        }
                    }*/
                }
            }
        }

        return true;
    }
}
