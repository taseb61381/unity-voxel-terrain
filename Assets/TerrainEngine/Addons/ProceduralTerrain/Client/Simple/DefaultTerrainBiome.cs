﻿
using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Biomes/DefaultTerrain")]
public class DefaultTerrainBiome : IBlockProcessor
{
    public float MountainLevel = 6;
    public int GroundOffset;

    public override IWorldGenerator GetGenerator()
    {
        DefaultTerrainGenerator Gen = new DefaultTerrainGenerator();
        Gen.MountainLevel = MountainLevel;
        Gen.GroundOffset = GroundOffset;
        return Gen;
    }
}