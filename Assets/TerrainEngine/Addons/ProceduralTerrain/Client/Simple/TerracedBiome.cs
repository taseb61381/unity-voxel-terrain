using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Biomes/Terraced")]
public class TerracedBiome : IBlockProcessor
{
    static public int Seed;

    public int WorldSkyLevel = 10050;
    public float MaxS = 16f;
    public float MultiplierS = 2f;
    public float Frequency = 0.1f;
    public float FrequencyY = 0.1f;
    public string VoxelName;
    private byte VoxelType;

    public override void OnInitSeed(int seed)
    {
        SimplexNoise.Initialize();
        Seed = seed;
        VoxelType = TerrainManager.Instance.Palette.GetVoxelId(VoxelName);
    }

    public override bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        int SkyLevel = (int)(WorldSkyLevel / Block.Information.Scale);

        float noise = 0f;

        float px = 0;
        float py = 0;
        float pz = 0;

        byte Type = 1;
        float Volume = 0;

        float blockX = 0;
        float blockZ = 0;

        int StartX = ChunkX * Block.Information.VoxelPerChunk;
        int StartZ = ChunkZ * Block.Information.VoxelPerChunk;

        int x, y, z;
        float s, ppx, ppy, ppz;
        for (x = StartX; x < StartX + Block.Information.VoxelPerChunk; x++)
        {
            blockX = Block.WorldX + x;

            for (z = StartZ; z < StartZ + Block.Information.VoxelPerChunk; z++)
            {
                blockZ = Block.WorldZ + z;

                for (y = Block.Information.VoxelsPerAxis - 1; y >= 0; y--)
                {

                    py = Block.WorldY + y;

                    /*if (py < GroundLevel)
                    {
                        continue;
                    }
                    else*/ if (py > SkyLevel)
                    {
                        continue;
                    }

                    //Block.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
                    if (Type == 0 || Volume < 0.5f)
                    {
                        px = blockX * Frequency;
                        pz = blockZ * Frequency;
                        py *= FrequencyY;
                        noise = 0f;

                        for (s = 1f; s < MaxS; s *= MultiplierS)
                        {
                            ppx = px + Mathf.Sin((py * 4f) - (int)(py * 4f)) * Frequency;
                            ppy = (int)((py) * 4f);
                            ppz = pz - Mathf.Cos((int)(py * 8f)) * Frequency;

                            noise += s * (float)SimplexNoise.Noise(ppx, ppy, ppz);
                            px *= .5f;
                            py *= .5f;
                            pz *= .5f;

                        }

                        Block.SetTypeAndVolume(x, y, z, VoxelType, Mathf.Pow((noise / 15f) - 1f, 5f) + 1f);
                    }
                }
            }
        }

        return true;
    }
}
