﻿
namespace TerrainEngine
{
    public enum Biome
    {
        MOUNTAINS = 0,
        SNOW = 1,
        DESERT = 2,
        FOREST = 3,
        PLAINS = 4,
    }

    public interface IWorldBiome
    {
        float getHumidityAt(int x, int z);

        float getTemperatureAt(int x, int z);

        float getFog(float time);

        Biome getBiomeAt(int x, int z);

        Biome getBiomeAt(float x, float z);
    }
}
