﻿
using UnityEngine;

namespace TerrainEngine
{
    static public class TerrainMath
    {

        public static float RAD_TO_DEG = (float)(180.0f / Mathf.PI);
        public static float DEG_TO_RAD = (float)(Mathf.PI / 180.0f);

        public static int fastAbs(int i)
        {
            return (i >= 0) ? i : -i;
        }

        public static float fastAbs(float d)
        {
            return (d >= 0) ? d : -d;
        }

        public static double fastAbs(double d)
        {
            return (d >= 0) ? d : -d;
        }

        public static double fastFloor(double d)
        {
            int i = (int)d;
            return (d < 0 && d != i) ? i - 1 : i;
        }

        public static float fastFloor(float d)
        {
            int i = (int)d;
            return (d < 0 && d != i) ? i - 1 : i;
        }

        public static float clamp(float value)
        {
            if (value > 1.0f)
                return 1.0f;
            if (value < 0.0f)
                return 0.0f;
            return value;
        }

        public static double clamp(double value, double min, double max)
        {
            if (value > max)
                return max;
            if (value < min)
                return min;
            return value;
        }

        public static float clamp(float value, float min, float max)
        {
            if (value > max)
                return max;
            if (value < min)
                return min;
            return value;
        }

        public static int clamp(int value, int min, int max)
        {
            if (value > max)
                return max;
            if (value < min)
                return min;
            return value;
        }

        public static double biLerp(double x, double y, double q11, double q12, double q21, double q22, double x1, double x2, double y1, double y2)
        {
            double r1 = lerp(x, x1, x2, q11, q21);
            double r2 = lerp(x, x1, x2, q12, q22);
            return lerp(y, y1, y2, r1, r2);
        }

        public static double lerp(double x, double x1, double x2, double q00, double q01)
        {
            return ((x2 - x) / (x2 - x1)) * q00 + ((x - x1) / (x2 - x1)) * q01;
        }

        public static float lerpf(float x, float x1, float x2, float q00, float q01)
        {
            return ((x2 - x) / (x2 - x1)) * q00 + ((x - x1) / (x2 - x1)) * q01;
        }

        public static double lerp(double x1, double x2, double p)
        {
            return x1 * (1.0 - p) + x2 * p;
        }

        public static float lerpf(float x1, float x2, float p)
        {
            return x1 * (1.0f - p) + x2 * p;
        }

        public static double triLerp(double x, double y, double z, double q000, double q001, double q010, double q011, double q100, double q101, double q110, double q111, double x1, double x2, double y1, double y2, double z1, double z2)
        {
            double x00 = lerp(x, x1, x2, q000, q100);
            double x10 = lerp(x, x1, x2, q010, q110);
            double x01 = lerp(x, x1, x2, q001, q101);
            double x11 = lerp(x, x1, x2, q011, q111);
            double r0 = lerp(y, y1, y2, x00, x01);
            double r1 = lerp(y, y1, y2, x10, x11);
            return lerp(z, z1, z2, r0, r1);
        }

        public static float triLerpf(float x, float y, float z, float q000, float q001, float q010, float q011, float q100, float q101, float q110, float q111, float x1, float x2, float y1, float y2, float z1, float z2)
        {
            float x00 = lerpf(x, x1, x2, q000, q100);
            float x10 = lerpf(x, x1, x2, q010, q110);
            float x01 = lerpf(x, x1, x2, q001, q101);
            float x11 = lerpf(x, x1, x2, q011, q111);
            float r0 = lerpf(y, y1, y2, x00, x01);
            float r1 = lerpf(y, y1, y2, x10, x11);
            return lerpf(z, z1, z2, r0, r1);
        }

        public static int mapToPositive(int x)
        {
            if (x >= 0)
                return x * 2;

            return -x * 2 - 1;
        }

        public static int redoMapToPositive(int x)
        {
            if (x % 2 == 0)
            {
                return x / 2;
            }

            return -(x / 2) - 1;
        }

        public static int cantorize(int k1, int k2)
        {
            return ((k1 + k2) * (k1 + k2 + 1) / 2) + k2;
        }

        public static int cantorX(int c)
        {
            int j = (int)(Mathf.Sqrt(0.25f + 2 * c) - 0.5);
            return j - cantorY(c);
        }

        public static int cantorY(int c)
        {
            int j = (int)(Mathf.Sqrt(0.25f + 2 * c) - 0.5);
            return c - j * (j + 1) / 2;
        }

        public static int ceilPowerOfTwo(int val)
        {
            val--;
            val = (val >> 1) | val;
            val = (val >> 2) | val;
            val = (val >> 4) | val;
            val = (val >> 8) | val;
            val = (val >> 16) | val;
            val++;
            return val;
        }

        public static int sizeOfPower(int val)
        {
            int power = 0;
            while (val > 1)
            {
                val = val >> 1;
                power++;
            }
            return power;
        }

        public static int floorToInt(float val)
        {
            int i = (int)val;
            return (val < 0 && val != i) ? i - 1 : i;
        }

        public static int ceilToInt(float val)
        {
            int i = (int)val;
            return (val >= 0 && val != i) ? i + 1 : i;
        }
    }
}