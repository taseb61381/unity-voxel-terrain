﻿
namespace TerrainEngine
{
    public class WorldBiome : IWorldBiome
    {
        PerlinNoise temperatureNoise;
        PerlinNoise humidityNoise;
        PerlinNoise fogNoise;

        public WorldBiome(int worldSeed)
        {
            temperatureNoise = new PerlinNoise(worldSeed + 5);
            humidityNoise = new PerlinNoise(worldSeed + 6);
            fogNoise = new PerlinNoise(worldSeed + 12);
            fogNoise.setOctaves(8);
        }

        public float getHumidityAt(int x, int z)
        {
            float result = humidityNoise.fBm(x * 0.0005f, 0, 0.0005f * z);
            return MathHelper.clamp((result + 1.0f) / 2.0f);
        }

        public float getTemperatureAt(int x, int z)
        {
            float result = temperatureNoise.fBm(x * 0.0005f, 0, 0.0005f * z);
            return MathHelper.clamp((result + 1.0f) / 2.0f);
        }

        public float getFog(float time)
        {
            return MathHelper.clamp(fogNoise.fBm(time * 0.372891f, time * 0.578291f, time * 0.78319f) * 10.0f, 0.0f, 15.0f);
        }

        public Biome getBiomeAt(int x, int z)
        {
            double temp = getTemperatureAt(x, z);
            double humidity = getHumidityAt(x, z);

            if (temp >= 0.5 && humidity < 0.3)
            {
                return Biome.DESERT;
            }
            else if (humidity >= 0.3 && humidity <= 0.6 && temp >= 0.5)
            {
                return Biome.PLAINS;
            }
            else if (temp <= 0.3 && humidity > 0.5)
            {
                return Biome.SNOW;
            }
            else if (humidity >= 0.2 && humidity <= 0.6 && temp < 0.5)
            {
                return Biome.MOUNTAINS;
            }

            return Biome.FOREST;
        }

        public Biome getBiomeAt(float x, float z)
        {
            return getBiomeAt((int)x, (int)z);
        }
    }
}