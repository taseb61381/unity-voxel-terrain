﻿

using TerrainEngine;

public class TreeDecorator : IBlockProcessor
{
    public int GroundOffset;
    public override IWorldGenerator GetGenerator()
    {
        DefaultTreeGenerator Gen = new DefaultTreeGenerator();
        Gen.GroundOffset = GroundOffset;
        return Gen;
    }
}
