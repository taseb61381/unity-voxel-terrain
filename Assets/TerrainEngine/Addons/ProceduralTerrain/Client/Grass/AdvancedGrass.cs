

using TerrainEngine;

public class AdvancedGrass : GrassProcessor 
{
    public BiomeObjectsContainer ObjectsContainer
    {
        get
        {
            return Container.GrassNoises;
        }
    }

    public override void OnInitSeed(int seed)
    {
        EnableGenerationFunction = false;
    }

    public override byte GetGroundType(string Name)
    {
        int Count = ObjectsContainer.Objects.Count;
        int i, j;
        NoiseObject Info;
        NoiseObjects Infos;
        for (i = 0; i < Count; ++i)
        {
            Infos = ObjectsContainer.Objects.NoisesObjects[i];
            for (j = 0; j < Infos.Objects.Count; ++j)
            {
                if ((Info = Infos.Objects[j]).Name == Name)
                {
                    if (Info.GroundType < 0 || Info.GroundType > 255)
                        return 0;
                    else
                        return (byte)Info.GroundType;
                }
            }
        }

        return 0;
    }

    public override IWorldGenerator GetGenerator()
    {
        AdvancedGrassGenerator Gen = new AdvancedGrassGenerator();
        Gen.GroundType = GroundType;
        Gen.Priority = Priority;
        Gen.Density = Density;
        Gen.Name = InternalName;
        Gen.DataName = "Grass-"+DataName;
        Gen.GrassObjects = Container.Grass;

        if (ObjectsContainer != null)
        {
            Gen.ObjectsRequirements = ObjectsContainer.Objects;
            Gen.RequirementNoises = ObjectsContainer.Container;
        }

        return Gen;
    }
}
