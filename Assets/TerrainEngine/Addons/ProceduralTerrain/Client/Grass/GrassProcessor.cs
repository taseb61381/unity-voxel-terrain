using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Processors/Grass")]
public class GrassProcessor : IViewableProcessor
{
    public int GroundType = 1;
    public Vector2 Density = new Vector2(50, 100); // Chance / Chance Max Density. Example : 10/1000. 10 Objects every 1000 voxels.
    public float Scale = 1;
    public Vector2 UvOffsets;
    public bool CastShadow = false;

    public Material GlobalMaterial;

    public GrassContainer Container;
    public List<GrassInfo> Grass
    {
        get
        {
            return Container.Grass;
        }
    }

    [HideInInspector]
    public Texture2D GlobalTexture;

    [HideInInspector]
    public Rect[] Uvs;

    public int AnisoLevel = 0;
    public FilterMode Filter = FilterMode.Trilinear;
    public bool IsInited = false;

    private PoolInfoRef GrassPool;
    public Material[] GeneratedMaterials;
    public byte[] GeneratedProperty;

    public GrassInfo GetInfo(string Name)
    {
        if (Container != null)
            return Container.GetInfo(Name);
        else
            return null;
    }

    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.J))
    //    {
    //        TerrainObject TObj = TerrainManager.Instance.CamObject;
    //        TerrainChunkRef Ref = TObj.ChunkRef;

    //        TerrainBlock Block = null;
    //        Ref.IsValid(ref Block);
    //        ConvertChunkResolution(Block, ref Ref.ChunkId);

    //        SimpleTypeDatas BData = null;
    //        foreach (KeyValuePair<string, ICustomTerrainData> Custom in Block.Voxels.Customs)
    //        {
    //            if (Custom.Value is SimpleTypeDatas)
    //                BData = Custom.Value as SimpleTypeDatas;
    //        }

    //        int LocalX = 0, LocalY = 0, LocalZ = 0;
    //        Block.Information.GetChunkPositionFromChunkId(Ref.ChunkId, ref LocalX, ref LocalY, ref LocalZ);

    //        int Size = Block.Information.VoxelPerChunk;
    //        int ChunkPerBlock = Block.Information.ChunkPerBlock;
    //        int ChunkIndex = 0, Index = 0;
    //        int OffsetX = 0, OffsetY = 0, OffsetZ = 0;
    //        int StartX = LocalX * Size,
    //            StartY = LocalY * Size,
    //            StartZ = LocalZ * Size;
    //        int EndX = StartX + (int)Resolution * Block.Information.VoxelPerChunk,
    //            EndY = StartY + (int)Resolution * Block.Information.VoxelPerChunk,
    //            EndZ = StartZ + (int)Resolution * Block.Information.VoxelPerChunk;
    //        int px, py, pz = 0;
    //        byte Id = 0;
    //        LocalObjectPositionId[] array;

    //        BData.GetOffsets(LocalX, LocalY, LocalZ, ref ChunkIndex, ref OffsetX, ref OffsetY, ref OffsetZ);

    //        int Count = 0;
    //        array = BData.Chunks[ChunkIndex].array;
    //        if (array != null)
    //        {
    //            for (Index = BData.Chunks[ChunkIndex].Count - 1; Index >= 0; --Index)
    //            {
    //                px = (int)array[Index].x + OffsetX;
    //                if (px < StartX || px >= EndX)
    //                    continue;

    //                pz = (int)array[Index].z + OffsetZ;
    //                if (pz < StartZ || pz >= EndZ)
    //                    continue;

    //                py = (int)array[Index].y + OffsetY;
    //                if (py < StartY || py >= EndY)
    //                    continue;

    //                Id = array[Index].Id;
    //                if (Id == 0)
    //                    continue;

    //                ++Count;
    //            }
    //        }

    //        Debug.Log("Count:" + Count + ",ChunkInList:" + Contains(Ref.BlockId, Ref.ChunkId));
    //    }
    //}

    public override void OnInitManager(TerrainManager Manager)
    {
        base.OnInitManager(Manager);
        InternalDataName = "Grass-" + DataName;

        GameObject Obj = new GameObject();
        Obj.AddComponent<TerrainSubScript>();
        Obj.transform.parent = transform;
        Obj.SetActive(false);
        GameObjectPool.SetDefaultObject(InternalDataName, Obj);
    }

    public override bool CanStart()
    {
        if(!IsInited)
        {
            IsInited = true;
            if (GlobalMaterial == null)
            {
                Debug.LogError("No grass material.");
                enabled = false;
                return false;
            }

            Texture2D[] Textures = new Texture2D[Grass.Count];
            GrassInfo Info = null;

            for (int i = 0; i < Grass.Count; ++i)
            {
                Info = Grass[i];

                Info.RandomScale.Min.z = Info.RandomScale.Min.x;
                Info.RandomScale.Max.z = Info.RandomScale.Max.x;
                Info.RandomScale.Init();

                Textures[i] = Info.Texture;
                Info.Name = Info.Texture.name;
                Info.ObjectId = i;
                if (Container.GrassNoises != null && Container.GrassNoises.Container != null)
                    Info.NoiseScaleModule = Container.GrassNoises.Container.GetNoiseModule(Info.NoiseNameScale);
                Info.ExtractMesh();
            }

            GlobalTexture = new Texture2D(2, 2);
            Uvs = GlobalTexture.PackTextures(Textures, 0);
            GlobalTexture.wrapMode = TextureWrapMode.Clamp;
            GlobalTexture.filterMode = Filter;
            GlobalTexture.anisoLevel = AnisoLevel;
            GlobalTexture.Apply();
            SimplexNoise.Initialize();

            UvOffsets += new Vector2(0.001f, 0.001f);

            for (int i = 0; i < Grass.Count; ++i)
            {
                Grass[i].Uv = Uvs[i];
            }

            Material NewMat = new Material(GlobalMaterial);
            NewMat.mainTexture = GlobalTexture;
            GeneratedMaterials = new Material[1] { NewMat };
            GeneratedProperty = new byte[1];

            if (NewMat.HasProperty(FadeName))
            {
                GeneratedProperty[0] = 1;
            }
            else if (NewMat.HasProperty("_Cutoff"))
            {
                GeneratedProperty[0] = 2;
            }
            else if (NewMat.HasProperty("_FadeColor"))
            {
                GeneratedProperty[0] = 3;
            }
            else if (NewMat.HasProperty("_Color"))
            {
                GeneratedProperty[0] = 4;
            }
        }

        if (Density.x <= 0)
        {
            Debug.LogError("Grass : Density <= 0");
            enabled = false;
            return true;
        }

        if (Grass.Count > 255)
        {
            Debug.LogError("Grass : 255 Textures maximum.");
            return false;
        }

        if (GrassPool.Index == 0)
            GrassPool = PoolManager.Pool.GetPool("GrassMeshData");

        return base.CanStart();
    }

    public override void OnBlockDataCreate(TerrainDatas Data)
    {
        SimpleTypeDatas CData = Data.GetCustomData<SimpleTypeDatas>(InternalDataName);
        if (CData == null)
        {
            CData = new SimpleTypeDatas();
            Data.RegisterCustomData(InternalDataName, CData);
        }
    }

    public override IWorldGenerator GetGenerator()
    {
        GrassGenerator Gen = new GrassGenerator();
        Gen.GroundType = GroundType;
        Gen.Priority = Priority;
        Gen.Density = Density;
        Gen.Name = InternalName;
        Gen.DataName = "Grass-" + DataName;
        Gen.GrassObjects = Container.Grass;
        return Gen;
    }

    public override void GenerateMesh(ActionThread Thread, TerrainBlock Block, int ChunkId, Stack<TempMeshDataId> MDatas, int GenerateId)
    {
        if (Block.Voxels == null)
            return;

        SimpleTypeDatas BData = Block.Voxels.GetCustomData<SimpleTypeDatas>(InternalDataName);
        TempMeshData Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
        TempSimpleTypeContainer TempObjects = Thread.Pool.GetData<TempSimpleTypeContainer>("TempSimpleTypeContainer");

        GenerateGrass(BData, Info, Block, ChunkId, Scale, TempObjects);

        PropertiesMeshDatas Data = null;
        if (Info.Vertices.Count != 0)
        {
            Data = GrassPool.GetData<PropertiesMeshDatas>();
            Data.EraseDatas = false;

            if (Data.CIndices == null)
            {
                Data.CIndices = new int[1][];
            }

            Data.CIndices[0] = Info.Triangles.Indices.ToArray();
            Data.VerticesCount = Info.Vertices.Count;
            Data.CVertices = Info.Vertices.ToArray();
            Data.CNormals = Info.Normals.ToArray();
            Data.CUVs = Info.Uvs.ToArray();
            Data.CTangents = Info.Tangents.ToArray();
            Data.CColors = Info.Colors.ToArray();
            Data.CMaterials = this.GeneratedMaterials;
            Data.Properties = this.GeneratedProperty;
            //MeshUtils.calculateMeshTangents(Data);
        }

        TempMeshDataId tmp = Thread.Pool.GetData<TempMeshDataId>("TempMeshDataId");
        tmp.ChunkRef = new TerrainChunkRef(Block, ChunkId);
        tmp.GenerateId = 0;
        tmp.Data = Data;

        TempObjects.Close();
        Info.Close();

        lock (MDatas)
            MDatas.Push(tmp);
    }

    public void GenerateGrass(SimpleTypeDatas BData, TempMeshData Info, TerrainBlock Block, int ChunkId, float GlobalScale,TempSimpleTypeContainer TempObjects)
    {
        TerrainInformation Information = TerrainManager.Instance.Informations;

        byte Id = 0;
        GrassInfo GInfo = null;
        Vector3 Left = new Vector3();
        Vector3 Right = new Vector3();
        Vector3 LocalWorldPosition = new Vector3(); // World Position inside the current Block. 0 -> ChunkSize
        Vector3 ChunkWorldPosition = new Vector3();
        Vector3 RandomPosition = new Vector3();
        Vector3 RScale = new Vector3();
        Vector2 TempUv = new Vector2();
        Rect Uv = new Rect();
        byte GroundType = 0;
        float Orientation = 0, wy = 0;
        int PlaneCount = 0;
        int count = 0;
        int LastId = -1;
        Vector3 WorldPosition = Block.GetChunkWorldPosition(ChunkId);
        float Scale = Information.Scale;
        float VoxelHeight;
        bool HasColors = true;

        int VoxelPerChunk = Information.VoxelPerChunk;
        int px, py, pz = 0;
        int ErrorCount = 0;

        SFastRandom FRandom = new SFastRandom(0);
        ITerrainBlock TempBlock = null;
        byte TempType = 0;
        Color WindColor = new Color(1, 1, 1, 0);
        Quaternion AngleAxis;
        Vector3 AngleAxisResult;

        int LocalX = Block.Chunks[ChunkId].LocalX;
        int LocalY = Block.Chunks[ChunkId].LocalY;
        int LocalZ = Block.Chunks[ChunkId].LocalZ;

        int len = 0;

        int StartX = LocalX * VoxelPerChunk,
            StartY = LocalY * VoxelPerChunk,
            StartZ = LocalZ * VoxelPerChunk;
        int EndX = StartX + (int)Resolution * VoxelPerChunk,
            EndY = StartY + (int)Resolution * VoxelPerChunk,
            EndZ = StartZ + (int)Resolution * VoxelPerChunk;
        float NoiseScale = 1f;

        BData.GetObjects(StartX, StartY, StartZ, EndX, EndY, EndZ, TempObjects.Objects);

        if (TempObjects.Objects.Count != 0)
        {
            for (int Index = TempObjects.Objects.Count - 1; Index >= 0; --Index)
            {
                Id = (byte)(TempObjects.Objects.array[Index].Id - 1);
                px = TempObjects.Objects.array[Index].x;
                py = TempObjects.Objects.array[Index].y;
                pz = TempObjects.Objects.array[Index].z;

                LocalWorldPosition.x = px * Scale;
                LocalWorldPosition.y = py * Scale;
                LocalWorldPosition.z = pz * Scale;
                wy = (py - StartY) * Scale;

                FRandom.InitSeed((long)((2f+SimplexNoise.Noise2((Block.Voxels.WorldX+px)*0.01,(Block.Voxels.WorldZ+pz)*0.01))*10000));

                if (LastId != Id)
                {
                    LastId = Id;
                    GInfo = Grass[Id];
                    GroundType = GetGroundType(GInfo.Name);

                    if (GInfo.Data != null && GInfo.Data.CVertices != null)
                        len = GInfo.Data.CVertices.Length;
                    else
                        len = 0;
                }

                if (GInfo.NoiseScaleModule != null)
                {
                    NoiseScale = (float)GInfo.NoiseScaleModule.GetValue(px + BData.Terrain.Position.x, py + BData.Terrain.Position.y, pz + BData.Terrain.Position.z);
                    NoiseScale = GInfo.RandomScale.RandomY(NoiseScale);
                }

                Orientation = 0;
                Uv = GInfo.Uv;
                RandomPosition.x = RandomPosition.z = RandomPosition.y = 0;

                switch (GInfo.GrassType)
                {
                    case GrassTypes.PLANES:
                        {
                            PlaneCount = (GInfo.PlaneMax != 0 ? FRandom.randomInt(GInfo.PlaneMax) : 0) + GInfo.PlaneMin;

                            for (count = 0; count < PlaneCount; ++count)
                            {
                                Orientation = FRandom.randomInt(90);
                                RScale = GInfo.RandomScale.RandomNoise3(Block.Voxels.WorldX+px,Block.Voxels.WorldZ+pz);
                                FRandom.randomVector3fXZ(ref RandomPosition, Scale);

                                Left.x = Left.z = -0.5f;
                                Left.y = Right.y = 0;
                                Right.x = Right.z = 0.5f;

                                Left.x *= RScale.x * GlobalScale;
                                Right.x *= RScale.x * GlobalScale;

                                Left = GetVertex(Block, StartX, StartY, StartZ, LocalWorldPosition + MeshUtils.RotateVertice(Left, Orientation) + RandomPosition, WorldPosition.y, GroundType, GInfo.HeightSide, RScale.y, Scale);
                                if (Left.y == -1f || Mathf.Abs((float)wy - Left.y) > Scale) continue;

                                Right = GetVertex(Block, StartX, StartY, StartZ, LocalWorldPosition + MeshUtils.RotateVertice(Right, Orientation) + RandomPosition, WorldPosition.y, GroundType, GInfo.HeightSide, RScale.y, Scale);
                                if (Right.y == -1f || Mathf.Abs((float)wy - Right.y) > Scale) continue;

                                if (Mathf.Abs(Left.y - Right.y) > RScale.x + Scale)
                                    continue;

                                if (!GInfo.AlignToGround)
                                {
                                    Left.y = Mathf.Min(Left.y, Right.y);
                                    Right.y = Left.y;
                                }

                                Orientation = FRandom.randomInt(45);
                                Info.Vertices.CheckArray(4);
                                Info.Vertices.AddSafe(Left);

                                AngleAxis = Quaternion.AngleAxis(Orientation, Vector3.forward);
                                AngleAxisResult = AngleAxis * Vector3.up;

                                if (GInfo.NoiseScaleModule != null)
                                {
                                    Info.Vertices.AddSafe(Left + AngleAxisResult * NoiseScale);
                                    Info.Vertices.AddSafe(Right + AngleAxisResult * NoiseScale);
                                }
                                else
                                {
                                    Info.Vertices.AddSafe(Left + AngleAxisResult * RScale.y);
                                    Info.Vertices.AddSafe(Right + AngleAxisResult * RScale.y);
                                }

                                Info.Vertices.AddSafe(Right);
                                AddPlane(Info, Uv, UvOffsets);

                                // Colors
                                if (GInfo.GenerateColors)
                                {
                                    if (!HasColors)
                                    {
                                        HasColors = true;
                                        Info.Colors.CheckArray(Info.Vertices.Count - 4);

                                        for (Index = 0; Index < Info.Vertices.Count - 4; ++Index)
                                            Info.Colors.AddSafe(Color.white);
                                    }

                                    Info.Colors.CheckArray(4);
                                    Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 0));

                                    Left = Info.Vertices.array[Info.Vertices.Count - 3];
                                    Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 1));

                                    Left = Info.Vertices.array[Info.Vertices.Count - 2];
                                    Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 1));

                                    Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Right.x + WorldPosition.x, Right.y + WorldPosition.y, Right.z + WorldPosition.z, 0));
                                }
                                else if (HasColors)
                                {
                                    Info.Colors.AddSafe(WindColor, Color.white, Color.white, WindColor);
                                }
                            }
                        } break;

                    case GrassTypes.CROSS_PLANES:
                        {
                            RScale = GInfo.RandomScale.RandomNoise3(Block.Voxels.WorldX + px, Block.Voxels.WorldZ + pz);
                            FRandom.randomVector3fXZ(ref RandomPosition, Scale);

                            Left.x = Left.z = -0.5f;
                            Right.x = Right.z = 0.5f;
                            Left.y = Right.y = 0;

                            Left.x *= RScale.x * GlobalScale;
                            Right.x *= RScale.x * GlobalScale;

                            Left = GetVertex(Block, StartX, StartY, StartZ, LocalWorldPosition + MeshUtils.RotateVertice(Left, Orientation) + RandomPosition, WorldPosition.y, GroundType, GInfo.HeightSide, RScale.y, Scale);
                            if (Left.y == -1f || Mathf.Abs((float)wy - Left.y) > Scale) continue;

                            Right = GetVertex(Block, StartX, StartY, StartZ, LocalWorldPosition + MeshUtils.RotateVertice(Right, Orientation) + RandomPosition, WorldPosition.y, GroundType, GInfo.HeightSide, RScale.y, Scale);
                            if (Right.y == -1f || Mathf.Abs((float)wy - Right.y) > Scale) continue;

                            if (Mathf.Abs(Left.y - Right.y) > RScale.x + Scale)
                                continue;

                            if (!GInfo.AlignToGround)
                            {
                                Left.y = Mathf.Min(Left.y, Right.y);
                                Right.y = Left.y;
                            }

                            Info.Vertices.CheckArray(4);
                            Info.Vertices.AddSafe(Left);

                            if (GInfo.NoiseScaleModule != null)
                            {
                                Info.Vertices.AddSafe(Left + Vector3.up * NoiseScale);
                                Info.Vertices.AddSafe(Right + Vector3.up * NoiseScale);
                            }
                            else
                            {
                                Left.y += RScale.y;
                                Info.Vertices.AddSafe(Left);

                                Right.y += RScale.y;
                                Info.Vertices.AddSafe(Right);
                                Right.y -= RScale.y;
                            }
                            Info.Vertices.AddSafe(Right);
                            AddPlane(Info, Uv, UvOffsets);

                            // Colors
                            if (GInfo.GenerateColors)
                            {
                                if (!HasColors)
                                {
                                    HasColors = true;
                                    Info.Colors.CheckArray(Info.Vertices.Count - 4);

                                    for (Index = 0; Index < Info.Vertices.Count - 4; ++Index)
                                        Info.Colors.AddSafe(Color.white);
                                }

                                Info.Colors.CheckArray(4);
                                Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 0));

                                Left = Info.Vertices.array[Info.Vertices.Count - 3];
                                Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 1));

                                Left = Info.Vertices.array[Info.Vertices.Count - 2];
                                Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 1));

                                Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Right.x + WorldPosition.x, Right.y + WorldPosition.y, Right.z + WorldPosition.z, 0));
                            }
                            else if (HasColors)
                            {
                                Info.Colors.AddSafe(WindColor, Color.white, Color.white, WindColor);
                            }

                            /////////////////////////////////////////////////////////////////////////

                            Left.x = Left.z = -0.5f;
                            Right.x = Right.z = 0.5f;
                            Left.y = Right.y = 0;
                            Left.x *= RScale.x;
                            Right.x *= RScale.x;

                            Left = GetVertex(Block, StartX, StartY, StartZ, LocalWorldPosition + MeshUtils.RotateVertice(Left, Orientation + 90) + RandomPosition, WorldPosition.y, GroundType, GInfo.HeightSide, RScale.y, Scale);
                            if (Left.y == -1f) continue;

                            Right = GetVertex(Block, StartX, StartY, StartZ, LocalWorldPosition + MeshUtils.RotateVertice(Right, Orientation + 90) + RandomPosition, WorldPosition.y, GroundType, GInfo.HeightSide, RScale.y, Scale);
                            if (Right.y == -1f) continue;

                            if (Mathf.Abs(Left.y - Right.y) > RScale.x + Scale)
                                continue;

                            if (!GInfo.AlignToGround)
                            {
                                Left.y = Mathf.Min(Left.y, Right.y);
                                Right.y = Left.y;
                            }

                            Info.Vertices.CheckArray(4);
                            Info.Vertices.AddSafe(Left);

                            if (GInfo.NoiseScaleModule != null)
                            {
                                Info.Vertices.AddSafe(Left + Vector3.up * NoiseScale);
                                Info.Vertices.AddSafe(Right + Vector3.up * NoiseScale);
                            }
                            else
                            {
                                Left.y += RScale.y;
                                Info.Vertices.AddSafe(Left);

                                Right.y += RScale.y;
                                Info.Vertices.AddSafe(Right);
                                Right.y -= RScale.y;
                            }
                            Info.Vertices.AddSafe(Right);
                            AddPlane(Info, Uv, UvOffsets);

                            // Colors
                            if (GInfo.GenerateColors)
                            {
                                if (!HasColors)
                                {
                                    HasColors = true;
                                    Info.Colors.CheckArray(Info.Vertices.Count - 4);

                                    for (Index = 0; Index < Info.Vertices.Count - 4; ++Index)
                                        Info.Colors.AddSafe(Color.white);
                                }

                                Info.Colors.CheckArray(4);
                                Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 0));

                                Left = Info.Vertices.array[Info.Vertices.Count - 3];
                                Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 1));

                                Left = Info.Vertices.array[Info.Vertices.Count - 2];
                                Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 1));

                                Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Right.x + WorldPosition.x, Right.y + WorldPosition.y, Right.z + WorldPosition.z, 0));
                            }
                            else if (HasColors)
                            {
                                Info.Colors.AddSafe(WindColor, Color.white, Color.white, WindColor);
                            }

                        } break;

                    case GrassTypes.HORIZONTAL_PLANE:
                        {
                            PlaneCount = (GInfo.PlaneMax != 0 ? FRandom.randomInt(GInfo.PlaneMax) : 0) + GInfo.PlaneMin;

                            for (count = 0; count < PlaneCount; ++count)
                            {
                                Orientation = FRandom.randomInt(90);
                                RScale = GInfo.RandomScale.RandomNoise3(Block.Voxels.WorldX + px, Block.Voxels.WorldZ + pz);
                                FRandom.randomVector3fXZ(ref RandomPosition, Scale);

                                Left.z = Right.z = Left.x = -0.5f;
                                Right.x = 0.5f;
                                Left.y = Right.y = 0.005f * count;
                                Left.x *= RScale.x * GlobalScale;
                                Right.x *= RScale.x * GlobalScale;

                                Left = GetVertex(Block, StartX, StartY, StartZ, LocalWorldPosition + MeshUtils.RotateVertice(Left, Orientation) + RandomPosition, WorldPosition.y, GroundType, GInfo.HeightSide, RScale.y, Scale);
                                if (Left.y == -1f || Mathf.Abs((float)wy - Left.y) > Scale) continue;

                                Right = GetVertex(Block, StartX, StartY, StartZ, LocalWorldPosition + MeshUtils.RotateVertice(Right, Orientation) + RandomPosition, WorldPosition.y, GroundType, GInfo.HeightSide, RScale.y, Scale);
                                if (Right.y == -1f || Mathf.Abs((float)wy - Right.y) > Scale) continue;

                                if (Mathf.Abs(Left.y - Right.y) > RScale.x + Scale)
                                    continue;

                                Info.Vertices.AddSafe(Left,
                                    Left + MeshUtils.RotateVertice(Vector3.forward * RScale.y, Orientation),
                                    Right + MeshUtils.RotateVertice(Vector3.forward * RScale.y, Orientation),
                                    Right);
                                AddPlane(Info, Uv, UvOffsets);

                                // Colors
                                if (GInfo.GenerateColors)
                                {
                                    if (!HasColors)
                                    {
                                        HasColors = true;
                                        Info.Colors.CheckArray(Info.Vertices.Count - 4);

                                        for (Index = 0; Index < Info.Vertices.Count - 4; ++Index)
                                            Info.Colors.AddSafe(Color.white);
                                    }

                                    Info.Colors.CheckArray(4);
                                    Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 0));

                                    Left = Info.Vertices.array[Info.Vertices.Count - 3];
                                    Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 1));

                                    Left = Info.Vertices.array[Info.Vertices.Count - 2];
                                    Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 1));

                                    Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Right.x + WorldPosition.x, Right.y + WorldPosition.y, Right.z + WorldPosition.z, 0));
                                }
                                else if (HasColors)
                                {
                                    Info.Colors.AddSafe(WindColor, Color.white, Color.white, WindColor);
                                }
                            }
                        } break;

                    case GrassTypes.CUSTOM_MESH:
                        {
                            Orientation = FRandom.randomInt(360);
                            RScale = GInfo.RandomScale.RandomNoise3(Block.Voxels.WorldX + px, Block.Voxels.WorldZ + pz);
                            FRandom.randomVector3fXZ(ref RandomPosition, Scale);

                            // ChunkWorldPosition is the world position inside the chunk
                            ChunkWorldPosition.x = LocalWorldPosition.x - (WorldPosition.x - Block.WorldPosition.x) + RandomPosition.x;
                            ChunkWorldPosition.y = LocalWorldPosition.y - (WorldPosition.y - Block.WorldPosition.y);
                            ChunkWorldPosition.z = LocalWorldPosition.z - (WorldPosition.z - Block.WorldPosition.z) + RandomPosition.z;

                            if ((GInfo.Data.CColors != null && GInfo.Data.CColors.Length != 0) || GInfo.GenerateColors)
                            {
                                if (!HasColors)
                                {
                                    HasColors = true;
                                    Info.Colors.CheckArray(Info.Vertices.Count - Info.Colors.Count);

                                    for (Index = Info.Vertices.Count - Info.Colors.Count; Index > 0; --Index)
                                        Info.Colors.AddSafe(Color.white);

                                }

                                Info.Colors.CheckArray(GInfo.Data.CColors.Length);
                            }

                            Info.Vertices.CheckArray(GInfo.Data.CVertices.Length);
                            Info.Normals.CheckArray(GInfo.Data.CVertices.Length);
                            Info.Tangents.CheckArray(GInfo.Data.CVertices.Length);
                            Info.Uvs.CheckArray(GInfo.Data.CVertices.Length);
                            Info.Triangles.Indices.CheckArray(GInfo.Indices.Length);

                            TempType = GroundType;
                            ErrorCount = 0;
                            Quaternion Q = Quaternion.Euler(0, Orientation, 0);
                            for (count = 0; count < len; ++count)
                            {
                                Left = Vector3.Scale(Q * GInfo.Data.CVertices[count], RScale);

                                if (GInfo.HeightSide == HeightSides.BOTTOM)
                                {
                                    Right = Block.GetLocalHeight(ref TempBlock, Left + LocalWorldPosition, ref TempType);
                                    if (Right.y != -1)
                                    {
                                        VoxelHeight = Right.y - WorldPosition.y - wy;
                                        if (VoxelHeight < -Scale * 1.5f)
                                        {
                                            ++ErrorCount;
                                            Left.y = -Scale * 1.5f;
                                        }
                                        else if (VoxelHeight > Scale * 1.5f)
                                        {
                                            ++ErrorCount;
                                            Left.y = Scale * 1.5f;
                                        }
                                        else
                                            Left.y += VoxelHeight;
                                    }
                                    else
                                        ++ErrorCount;
                                }

                                Left.x += ChunkWorldPosition.x;
                                Left.y += ChunkWorldPosition.y;
                                Left.z += ChunkWorldPosition.z;

                                Info.Vertices.AddSafe(Left);
                                Info.Normals.AddSafe(Q * GInfo.Data.CNormals[count]);
                                Info.Tangents.AddSafe(Q * GInfo.Data.CTangents[count]);

                                TempUv = GInfo.Data.CUVs[count];
                                TempUv.x = GInfo.Uv.x + GInfo.Uv.width * TempUv.x;
                                TempUv.y = GInfo.Uv.y + GInfo.Uv.height * TempUv.y;
                                Info.Uvs.AddSafe(TempUv);

                                if (GInfo.Data.CColors != null && GInfo.Data.CColors.Length != 0)
                                {
                                    if (GInfo.GenerateColors)
                                    {
                                        Info.Colors.AddSafe(GInfo.Data.CColors[count] * GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 1));
                                    }
                                    else
                                        Info.Colors.AddSafe(GInfo.Data.CColors[count]);
                                }
                                else if (GInfo.GenerateColors)
                                {
                                    Info.Colors.AddSafe(GenerateNoiseColor(GInfo, Left.x + WorldPosition.x, Left.y + WorldPosition.y, Left.z + WorldPosition.z, 0));
                                }
                                else if (HasColors)
                                    Info.Colors.AddSafe(Color.white);
                            }

                            if (ErrorCount >= GInfo.Data.CVertices.Length / 3)
                            {
                                Info.Vertices.Count -= GInfo.Data.CVertices.Length;
                                Info.Normals.Count -= GInfo.Data.CVertices.Length;
                                Info.Tangents.Count -= GInfo.Data.CVertices.Length;
                                Info.Uvs.Count -= GInfo.Data.CVertices.Length;

                                if (HasColors)
                                {
                                    Info.Colors.Count -= GInfo.Data.CVertices.Length;
                                }
                            }
                            else
                            {
                                for (count = 0; count < GInfo.Indices.Length; ++count)
                                {
                                    Info.Triangles.Indices.AddSafe(GInfo.Indices[count] + Info.TriangleIndex);
                                }

                                Info.TriangleIndex += GInfo.Data.CVertices.Length;
                            }

                        } break;
                }
            }
        }
    }

    static public Color GenerateNoiseColor(GrassInfo GInfo, float x, float y, float z, float a)
    {
        float Noise = (float)SimplexNoise.Noise(x * GInfo.NoiseColorScale, y * GInfo.NoiseColorScale, z * GInfo.NoiseColorScale);
        Color Col;
        if (Noise < 0)
            Col = Color.Lerp(GInfo.NoiseColorB, GInfo.NoiseColorA, -Noise);

        Col = Color.Lerp(GInfo.NoiseColorB, GInfo.NoiseColorC, Noise);
        Col.a = a;
        return Col;
    }

    static public void AddPlane(TempMeshData Info, Rect Uv, Vector2 UvOffsets)
    {
        Info.Triangles.Indices.CheckArray(6);
        Info.Triangles.Indices.AddSafe(Info.TriangleIndex + 0);
        Info.Triangles.Indices.AddSafe(Info.TriangleIndex + 1);
        Info.Triangles.Indices.AddSafe(Info.TriangleIndex + 2);

        Info.Triangles.Indices.AddSafe(Info.TriangleIndex + 2);
        Info.Triangles.Indices.AddSafe(Info.TriangleIndex + 3);
        Info.Triangles.Indices.AddSafe(Info.TriangleIndex + 0);

        Vector2 v = new Vector2();
        Info.Uvs.CheckArray(4);

        v.x = Uv.x + UvOffsets.x; v.y = Uv.y + UvOffsets.y;
        Info.Uvs.AddSafe(v);

        v.x = Uv.x + UvOffsets.x; v.y = Uv.height + Uv.y - UvOffsets.y;
        Info.Uvs.AddSafe(v);

        v.x = Uv.x + Uv.width + UvOffsets.x; v.y = Uv.y + Uv.height - UvOffsets.y;
        Info.Uvs.AddSafe(v);

        v.x = Uv.x + Uv.width - UvOffsets.x; v.y = Uv.y + UvOffsets.y;
        Info.Uvs.AddSafe(v);

        Info.Normals.AddSafe(Vector3.up, Vector3.up, Vector3.up, Vector3.up);

        Vector4 t = new Vector4(0, 1, 0, 1);
        Info.Tangents.AddSafe(t,t,t,t);

        Info.TriangleIndex += 4;
    }

    public Vector3 GetVertex(ITerrainBlock Block, int StartX, int StartY, int StartZ, Vector3 LocalWorldPosition, float WorldPositionY, byte GroundType, HeightSides Side, float Height, float Scale)
    {
        Vector3 Result = Side == HeightSides.BOTTOM ? Block.GetLocalHeight(ref Block, LocalWorldPosition, ref GroundType) : Block.GetTopLocalHeight(ref Block, LocalWorldPosition, ref GroundType);
        if (Result.y == -1)
            return Result;

        LocalWorldPosition.y = Result.y - WorldPositionY;
        LocalWorldPosition.x -= (StartX * Scale);
        LocalWorldPosition.z -= (StartZ * Scale);
        LocalWorldPosition.y -= Side == HeightSides.BOTTOM ? 0 : Height;
        Block.GetPositionOffset(ref LocalWorldPosition.x, ref LocalWorldPosition.y, ref LocalWorldPosition.z);
        return LocalWorldPosition;
    }

    public override void ApplyMesh(TerrainBlock Block, int ChunkId, IViewableProcessor.TempMeshDataId TData, int GenerateId)
    {
        IMeshData Data = null;
        if (TData != null)
            Data = TData.Data;
        
        GraphicMesh Mesh = Block.GetChunkMesh(ChunkId, InternalDataName);
        if (Mesh == null)
        {
            if (Data != null && Data.IsValid())
            {
                GraphicMeshContainer Container = Block.GetChunkMeshContainer(ChunkId, true);
                Mesh = Container.AddMesh(InternalDataName, false);
                Mesh.PoolMaterials = false;
                Mesh.ReceiveShadow = true;
                Mesh.CastShadow = CastShadow;
                Mesh.UseTangents = Data.Tangents != null;
                Mesh.FadeTime = FadeTime;
                Mesh.FadeStart = ThreadManager.UnityTime;
                Mesh.FadeName = FadeName;
                Mesh.Topology = MeshTopology.Triangles;
                Mesh.Tag = null;
                Mesh.Resolution = (byte)Resolution;

                Container.UpdateMesh(Mesh, Data);
            }
        }
        else
        {
            Mesh.ReceiveShadow = true;

            if (Data != null)
            {
                if (!Data.IsValid())
                {
                    Block.GetChunkMeshContainer(ChunkId, false).SetMeshActive(Mesh, false);
                }
                else
                {
                    Block.GetChunkMeshContainer(ChunkId, false).UpdateMesh(Mesh, Data);
                }
            }
            else
            {
                Block.GetChunkMeshContainer(ChunkId, false).SetMeshActive(Mesh, false);
            }
        }

        // Use Unity GameObjects
        /*TerrainChunkScript ChunkScript = null;
        Block.CheckChunkGameObject(ChunkId, ref ChunkScript);

        GrassScript Script = ChunkScript.GetSubScript<GrassScript>(InternalDataName);
        if (Script == null)
        {
            GameObject Obj = GameObjectPool.GetGameObject(InternalDataName);
            Script = Obj.GetComponent<GrassScript>();
            Script.Init(InternalDataName, this, ChunkScript);
            Script.transform.parent = ChunkScript.CTransform;
            Script.transform.localPosition = new Vector3();
            ChunkScript.AddSubScript(InternalDataName, Script);
        }
        Script.GenerateMesh(Data);*/
    }

    public override void DesactiveChunk(TerrainBlock Block, int ChunkId)
    {
        base.DesactiveChunk(Block, ChunkId);

        GraphicMeshContainer Container = Block.GetChunkMeshContainer(ChunkId);
        if (Container != null)
            Container.RemoveMesh(InternalDataName);


        /*TerrainChunkScript ChunkScript = Block.GetChunkScript(ChunkId);

        if (ChunkScript != null)
        {
            GrassScript Script = ChunkScript.GetSubScript<GrassScript>(InternalDataName);
            if (Script != null)
                Script.Close(true);
        }*/
    }

    public override void OnFlushVoxels(ActionThread Thread, TerrainObject Obj, ref VoxelFlush Voxel, TerrainChunkGenerateGroupAction Action)
    {
        SimpleTypeDatas BData = Voxel.Block.Voxels.GetCustomData<SimpleTypeDatas>(InternalDataName);

        if (BData.SetByteIfExist(Voxel.x, Voxel.y, Voxel.z, 0) != 0)
        {
            AddToFlush(new TerrainChunkRef(Voxel.Block, Voxel.ChunkId), 0);
        }
    }

    public override void OnFlushChunk(ActionThread Thread, TerrainChunkRef ChunkRef)
    {
        AddToFlush(ChunkRef, 0);
    }

    #region CustomData Modification

    public virtual byte GetGroundType(string Name)
    {
        return (byte)GroundType;
    }

    [NonSerialized]
    public List<TerrainChunkRef> ModifiedChunks = new List<TerrainChunkRef>();

    public override int GetDataIdByName(string Name)
    {
        return GetInfo(Name).ObjectId;
    }

    public override KeyValuePair<string, int>[] GetDatas()
    {
        KeyValuePair<string, int>[] Kps = new KeyValuePair<string, int>[Grass.Count];
        for (int i = 0; i < Grass.Count; ++i)
        {
            Kps[i] = new KeyValuePair<string, int>(Grass[i].Texture.name, i);
        }

        return Kps;
    }

    public override void ApplyPoint(ProcessorVoxelPoint Point)
    {
        SimpleTypeDatas GData = Point.Block.Voxels.GetCustomData<SimpleTypeDatas>(InternalDataName);
        if (GData == null)
            return;

        switch (Point.ModifyType)
        {
            case ProcessorVoxelChangeType.SET:
                byte CurrentGrass = GData.GetByte(Point.x, Point.y, Point.z);
                if (CurrentGrass != Point.Id + 1)
                {
                    GData.SetByteIntFloat(Point.x, Point.y, Point.z, (byte)(Point.Id + 1), 0, Point.Rotation);
                    TerrainChunkRef Ref = new TerrainChunkRef(Point.Block, Point.ChunkId);
                    ConvertChunkResolution(Point.Block, ref Ref.ChunkId);
                    if (!ModifiedChunks.Contains(Ref))
                        ModifiedChunks.Add(Ref);
                }

                break;
        };
    }

    #endregion
}
