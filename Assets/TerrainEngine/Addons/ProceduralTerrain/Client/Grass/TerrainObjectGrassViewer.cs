using UnityEngine;

/// <summary>
/// Register current TerrainObject to GrassProcessor (Will generate grass around it)
/// Attach it to any camera.
/// </summary>
[AddComponentMenu("TerrainEngine/TerrainObject/Grass Viewer (Generate Grass around)")]
public class TerrainObjectGrassViewer : MonoBehaviour 
{
    public bool DeformGrass = true;
    public float AffectDist=3;
    public float BendAmount = 5;
    public Vector3 BendPositionOffset = new Vector3(0, 2.5f, 0);
    private TerrainObject TObject;
    private bool Inited = true;

    void OnEnable()
    {
        Inited = false;
    }

    void Update()
    {
        if (!Inited)
        {
            if(TObject == null)
                TObject = GetComponent<TerrainObject>();

            if (TObject == null)
            {
                Debug.LogError("No TerrainObject on GrassObjectViewer gameObject " + name);
                return;
            }

            if (TerrainManager.Instance == null)
                return;

            foreach (IBlockProcessor Processor in TerrainManager.Instance.Processors)
            {
                if (Processor is GrassProcessor && TObject.AddProcessor(Processor))
                {
                    Debug.Log(name + " Added Processor : " + Processor);
                    Processor.OnObjectMoveChunk(TObject);
                }
            }

            Inited = true;
        }
        else if (DeformGrass)
        {
            Shader.SetGlobalVector("_obstacle", TObject.Position + BendPositionOffset);
            Shader.SetGlobalFloat("_affectDist", AffectDist);
            Shader.SetGlobalFloat("_bendAmount", BendAmount);
        }
    }
}
