

using TerrainEngine;

public class Voxel2DPalette : IVoxelPalette
{
    public Voxel2DInformation[] VoxelBlocks;

    public override T GetVoxelData<T>(byte Type)
    {
        return VoxelBlocks[Type] as T;
    }

    public override int GetDataCount()
    {
        return VoxelBlocks.Length;
    }
}