using System;
using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class Voxel2DBlock : TerrainBlock
    {
        public Voxel2DBlock(TerrainManager Manager)
            : base(Manager.Informations)
        {

        }

        public override VectorI3 GetVoxelFromWorldPosition(Vector3 WorldPosition, bool Remove)
        {
            return WorldPosition - this.WorldPosition;
        }

        public override void Generate(ActionThread Thread,ref TempMeshData Info,ref IMeshData Data, int ChunkId)
        {
            Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
            Data = PoolManager.UPool.GetSimpleMeshData();

            int StartX = Chunks[ChunkId].StartX;
            int StartY = Chunks[ChunkId].StartY;
            int StartZ = Chunks[ChunkId].StartZ;
            SFastRandom SRandom = new SFastRandom((StartX) ^ (StartY) ^ (StartZ) + 1);
            int RandomId;
            int x, y, z, px, py, pz,d;
            Voxel2DInformation BlockInfo;

            bool[] activeSubmesh = new bool[TerrainManager.Instance.Palette.GetDataCount()];
            int MatId = 0;
            VectorI2 diri;

            byte BlockType = 0;
            byte LightPower = 0;
            Color LightColor;

            int FullAroundFlag = 0;
            int EmptyAroundFlag = 0;
            int AffinityFullAroundFlag = 0;
            int EqualFullAroundFlag = 0;
            Voxel2DTile Tile = null;
            Vector2 TilePosition = new Vector2(-1, -1);
            Vector2 TileOffset = new Vector2(0, 0);

            Voxel2DTempInformation[][,] Blocks = new Voxel2DTempInformation[Information.VoxelPerChunk][,];

            #region Generating Temp Data

            for (z = 0; z < Information.VoxelPerChunk; ++z)
            {
                Blocks[z] = new Voxel2DTempInformation[Information.VoxelPerChunk + 6, Information.VoxelPerChunk + 6];

                for (x = -3; x < Information.VoxelPerChunk + 3; ++x)
                {
                    for (y = -3; y < Information.VoxelPerChunk + 3; ++y)
                    {
                        px = x + StartX;
                        py = y + StartY;
                        pz = z + StartZ;

                        BlockType = GetByte(px, py, pz);
                        Blocks[z][x + 3, y + 3] = new Voxel2DTempInformation(BlockType, (TerrainManager.Instance.Palette.GetVoxelData<Voxel2DInformation>(BlockType)));

                        if (x >= 0 && y >= 0 && x < Information.VoxelPerChunk && y < Information.VoxelPerChunk)
                        {
                            if (BlockType != 0 && activeSubmesh[BlockType] == false)
                            {
                                activeSubmesh[BlockType] = true;
                                ++MatId;
                            }
                        }
                    }
                }

                if ((z == 0 && (TerrainManager.Instance as Voxel2DManager).GenerateLight))
                    Voxel2DHelper.GenerateLight(ref Blocks[z]);
                else
                    Voxel2DHelper.GenerateWhiteLight(ref Blocks[z]);
            }

            #endregion

            #region Generating Materials

            Data.Materials = new Material[MatId];
            Data.Indices = new int[MatId][];
            MatId = 0;

            for (int i = 0; i < activeSubmesh.Length; ++i)
            {
                if (activeSubmesh[i])
                {
                    Data.Materials[MatId] = TerrainManager.Instance.Palette.GetVoxelData<Voxel2DInformation>((byte)i).TileMaterial;
                    ++MatId;
                }
            }

            MatId = 0;

            #endregion

            #region Generating Mesh

            for (int i = 0; i < activeSubmesh.Length; ++i)
            {
                if (activeSubmesh[i] == false)
                    continue;

                SRandom.InitSeed((StartX) ^ (StartY) ^ (StartZ) + 1);

                for (z = 0; z < Information.VoxelPerChunk; ++z)
                {
                    for (x = 0; x < Information.VoxelPerChunk; ++x)
                    {
                        for (y = 0; y < Information.VoxelPerChunk; ++y)
                        {
                            RandomId = SRandom.randomIntAbs();

                            BlockType = Blocks[z][x + 3, y + 3].Type;

                            if (BlockType == 0 || BlockType != i)
                                continue;

                            BlockInfo = TerrainManager.Instance.Palette.GetVoxelData<Voxel2DInformation>(BlockType);

                            if (BlockInfo.VoxelType == VoxelTypes.FLUID)
                                continue;

                            LightPower = Blocks[z][x + 3, y + 3].Light;

                            if (LightPower > z * 40)
                                LightPower -= (byte)(z * 40);
                            else
                                LightPower = 0;

                            LightColor = new Color32(LightPower, LightPower, LightPower, 255);

                            FullAroundFlag = 0;
                            EmptyAroundFlag = 0;
                            AffinityFullAroundFlag = 0;
                            EqualFullAroundFlag = 0;

                            for (d = 0; d < Voxel2DHelper.CheckDirections.Length; ++d)
                            {
                                diri = Voxel2DHelper.CheckDirections[d];

                                if (Blocks[z][x + diri.x + 3, y + diri.y + 3].Type == BlockManager.None)
                                {
                                    EmptyAroundFlag += Voxel2DHelper.DirectionFlag((Voxel2DHelper.Directions)d);
                                }
                                else if (Blocks[z][x + diri.x + 3, y + diri.y + 3].Type == BlockType)
                                {
                                    FullAroundFlag += Voxel2DHelper.DirectionFlag((Voxel2DHelper.Directions)d);
                                    EqualFullAroundFlag += Voxel2DHelper.DirectionFlag((Voxel2DHelper.Directions)d);
                                }
                                else if (BlockInfo.IsAffinity(Blocks[z][x + diri.x + 3, y + diri.y + 3].Info))
                                {
                                    FullAroundFlag += Voxel2DHelper.DirectionFlag((Voxel2DHelper.Directions)d);
                                    AffinityFullAroundFlag += Voxel2DHelper.DirectionFlag((Voxel2DHelper.Directions)d);
                                }
                                else
                                    EmptyAroundFlag += Voxel2DHelper.DirectionFlag((Voxel2DHelper.Directions)d);
                            }

                            Tile = BlockInfo.Decoder.GetTile(EmptyAroundFlag, FullAroundFlag, AffinityFullAroundFlag, EqualFullAroundFlag);
                            if (Tile != null)
                            {
                                TilePosition = Tile.GetTile(RandomId);
                                TileOffset = Tile.TileOffset;
                            }
                            else
                            {
                                TilePosition.x = -1;
                                TileOffset.x = 0;
                                TileOffset.y = 0;
                            }

                            if (TilePosition.x != -1)
                            {
                                Rect Uv = BlockInfo.Uvs[(int)TilePosition.x, (int)TilePosition.y];
                                AddFace(x, y, z, ref Info, LightColor, BlockInfo.BlockSize, TileOffset);
                                Info.Uvs.Add(new Vector2(Uv.x + BlockInfo.Epsilon.x, Uv.y + Uv.height - BlockInfo.Epsilon.y)); // Left Bottom
                                Info.Uvs.Add(new Vector2(Uv.x + Uv.width - BlockInfo.Epsilon.x, Uv.y + Uv.height - BlockInfo.Epsilon.y)); // Right Bottom

                                Info.Uvs.Add(new Vector2(Uv.x + BlockInfo.Epsilon.x, Uv.y + BlockInfo.Epsilon.y)); // Left Top
                                Info.Uvs.Add(new Vector2(Uv.x + Uv.width - BlockInfo.Epsilon.x, Uv.y + BlockInfo.Epsilon.y)); // Right Top

                            }

                            // Add Mesh borders for collisions
                            for ( d = 0; d < 8; d += 2)
                            {
                                diri = Voxel2DHelper.CheckDirections[d];

                                if (Blocks[z][x + diri.x + 3, y + diri.y + 3].Type == 0)
                                    AddAroundFaces(d / 2, x, y, z, ref Info);
                            }
                        }
                    }
                }

                Data.Indices[MatId] = Info.Triangles.Indices.ToArray();
                Info.Triangles.Indices.ClearFast();
                ++MatId;
            }

            Data.UVs = Info.Uvs.ToArray();
            Data.Vertices = Info.Vertices.ToArray();
            Data.Colors = Info.Colors.ToArray();

            #endregion
        }

        static public void AddFace(int x, int y, int z, ref TempMeshData Info, Color Light, Vector2 BlockSize, Vector2 BlockOffset, float OffHeight = 0)
        {
            float PX = BlockSize.x * 0.5f;
            float PY = BlockSize.y * 0.5f;

            float OffX = BlockOffset.x * 0.5f;
            float OffY = BlockOffset.y * 0.5f;

            Vector3 TopLeft = new Vector3(x + 0.5f - PX + OffX, y + 0.5f + PY + OffY - OffHeight, z);
            Vector3 TopRight = new Vector3(x + 0.5f + PX + OffX, y + 0.5f + PY + OffY - OffHeight, z);
            Vector3 Left = new Vector3(x + 0.5f - PX + OffX, y + 0.5f - PY + OffY, z);
            Vector3 Right = new Vector3(x + 0.5f + PX + OffX, y + 0.5f - PY + OffY, z);

            Info.Vertices.Add(TopLeft);
            Info.Vertices.Add(TopRight);
            Info.Vertices.Add(Left);
            Info.Vertices.Add(Right);

            Info.Triangles.Indices.Add(Info.TriangleIndex + 0);
            Info.Triangles.Indices.Add(Info.TriangleIndex + 1);
            Info.Triangles.Indices.Add(Info.TriangleIndex + 3);

            Info.Triangles.Indices.Add(Info.TriangleIndex + 3);
            Info.Triangles.Indices.Add(Info.TriangleIndex + 2);
            Info.Triangles.Indices.Add(Info.TriangleIndex + 0);

            Info.Colors.Add(Light);
            Info.Colors.Add(Light);
            Info.Colors.Add(Light);
            Info.Colors.Add(Light);

            Info.TriangleIndex += 4;
        }

        public void AddAroundFaces(int d, int x, int y, int z, ref TempMeshData Info)
        {
            Info.Vertices.Add(Voxel2DHelper.BlockBorders[d][0] + new Vector3(x, y, z));
            Info.Vertices.Add(Voxel2DHelper.BlockBorders[d][1] + new Vector3(x, y, z));

            Info.Vertices.Add(Voxel2DHelper.BlockBorders[d][2] + new Vector3(x, y, z));
            Info.Vertices.Add(Voxel2DHelper.BlockBorders[d][3] + new Vector3(x, y, z));

            Info.Triangles.Indices.Add(Info.TriangleIndex + 3);
            Info.Triangles.Indices.Add(Info.TriangleIndex + 1);
            Info.Triangles.Indices.Add(Info.TriangleIndex + 0);

            Info.Triangles.Indices.Add(Info.TriangleIndex + 0);
            Info.Triangles.Indices.Add(Info.TriangleIndex + 2);
            Info.Triangles.Indices.Add(Info.TriangleIndex + 3);
            Info.TriangleIndex += 4;

            Info.Uvs.Add(Vector2.zero);
            Info.Uvs.Add(Vector2.zero);
            Info.Uvs.Add(Vector2.zero);
            Info.Uvs.Add(Vector2.zero);

            Info.Colors.Add(Color.black);
            Info.Colors.Add(Color.black);
            Info.Colors.Add(Color.black);
            Info.Colors.Add(Color.black);
        }

        public bool IsSafe(int px, int py, int pz)
        {
            return (px < Information.VoxelPerChunk - 1 && px >= 1)
                    && (py < Information.VoxelPerChunk - 1 && py >= 1)
                    && (pz < Information.VoxelPerChunk - 1 && pz >= 1);
        }
    }
}