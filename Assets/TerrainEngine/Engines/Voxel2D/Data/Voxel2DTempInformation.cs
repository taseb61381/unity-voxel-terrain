
namespace TerrainEngine
{
    public struct Voxel2DTempInformation
    {
        public byte Type;
        public byte Light;
        public Voxel2DInformation Info;

        public Voxel2DTempInformation(byte Type, Voxel2DInformation Info)
        {
            this.Type = Type;
            this.Light = 0;
            this.Info = Info;
        }
    }
}