using UnityEngine;

namespace TerrainEngine
{
    static public class Voxel2DHelper
    {
        #region Uvs

        public enum Directions
        {
            Left = 0,
            TopLeft = 1,
            Top = 2,
            TopRight = 3,
            Right = 4,
            BottomRight = 5,
            Bottom = 6,
            BottomLeft = 7,
        }

        static public VectorI2[] CheckDirections = new VectorI2[]
        {
            new VectorI2(-1,0), // Left 0
            new VectorI2(-1,1), // TopLeft 1
            new VectorI2(0,1), // Top 2
            new VectorI2(1,1), // TopRight 3
            new VectorI2(1,0), // Right 4
            new VectorI2(1,-1), // RightBottom 5
            new VectorI2(0,-1), // Bottom 6
            new VectorI2(-1,-1), // BottomLeft 7
        };

        static public int DirectionFlag(Directions Dir)
        {
            return 1 << ((int)Dir + 1);
        }

        #endregion

        #region Mesh

        static public Vector3[] LeftBorder = new Vector3[]
        {
            new Vector3(0,1,1),
            new Vector3(0,0,1),
            new Vector3(0,1,0),
            new Vector3(0,0,0),

        };

        static public Vector3[] TopBorder = new Vector3[]
        {
            new Vector3(0,1,0),
            new Vector3(1,1,0),
            new Vector3(0,1,1),
            new Vector3(1,1,1),
        };

        static public Vector3[] RightBorder = new Vector3[]
        {
            new Vector3(1,1,0),
            new Vector3(1,0,0),
            new Vector3(1,1,1),
            new Vector3(1,0,1),
        };

        static public Vector3[] BottomBorder = new Vector3[]
        {
            new Vector3(0,0,1),
            new Vector3(1,0,1),
            new Vector3(0,0,0),
            new Vector3(1,0,0),
        };

        static public Vector3[][] BlockBorders = new Vector3[][]
        {
            LeftBorder,
            TopBorder,
            RightBorder,
            BottomBorder,
        };

        #endregion

        #region Light

        static public bool IsValid(int x, int y, int SizeX, int SizeY)
        {
            if (x < 0 || y < 0 || x >= SizeX || y >= SizeY)
                return false;

            return true;
        }

        static public void GenerateLight(ref Voxel2DTempInformation[,] Data)
        {
            int SizeX = Data.GetLength(0);
            int SizeY = Data.GetLength(1);

            for (int i = 0; i < 2; ++i)
            {
                for (int y = SizeY - 2; y >= 1; --y)
                {
                    for (int x = 1; x < SizeX - 1; ++x)
                    {
                        // Generate Around Light of Empty Blocks
                        if (i == 0)
                        {
                            if (Data[x, y].Type != 0)
                                continue;

                            Data[x, y].Light = 255;
                            foreach (Vector2 v in Voxel2DHelper.CheckDirections)
                            {
                                if (Data[x + (int)v.x, y + (int)v.y].Type != 0)
                                {
                                    Data[x + (int)v.x, y + (int)v.y].Light = 255;
                                }
                            }
                        }
                        else // Generate Around Light of complete Blocks
                        {
                            if (Data[x, y].Type == 0 || Data[x, y].Light == 255)
                                continue;

                            byte MaxLight = 0;

                            foreach (Vector2 v in Voxel2DHelper.CheckDirections)
                            {
                                if (Data[x + (int)v.x, y + (int)v.y].Type != 0 && MaxLight < Data[x + (int)v.x, y + (int)v.y].Light)
                                {
                                    MaxLight = Data[x + (int)v.x, y + (int)v.y].Light;
                                }
                            }

                            if (MaxLight >= 60)
                                Data[x, y].Light = (byte)(MaxLight - 60);
                            else
                                Data[x, y].Light = 0;
                        }
                    }
                }
            }
        }

        static public void GenerateWhiteLight(ref Voxel2DTempInformation[,] Data)
        {
            int SizeX = Data.GetLength(0);
            int SizeY = Data.GetLength(1);

            for (int y = SizeY - 1; y >= 0; --y)
                for (int x = 0; x < SizeX; ++x)
                    Data[x, y].Light = 255;
        }

        #endregion
    }
}
