using System;
using System.Xml.Serialization;
using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class Voxel2DInformation : VoxelInformation
    {
        public Texture2D Tiles;
        public Voxel2DTileDecoder Decoder;
        public Vector2 TileSize = new Vector2(16, 16);
        public int TileSeparator = 2;
        public string TypeAffinity = "";
        public string BackGroundTile = "";

        public Material TileMaterial;
        [XmlIgnore]
        public Material FluidMaterial;

        public Vector2 BlockSize = new Vector2(1, 1); // % off 16 px

        [HideInInspector]
        public byte TypeAffinityId = 0;

        [HideInInspector]
        public byte BackgroundTileId = 0;

        [HideInInspector]
        public Vector2 Epsilon;

        [HideInInspector]
        public int TileCountX = 0;

        [HideInInspector]
        public int TileCountY = 0;

        [HideInInspector]
        public Rect[,] Uvs;

        [HideInInspector]
        public Vector2 UvSize;

        [HideInInspector]
        public Vector2 UvSeparatorSize;

        public void Generate(Shader TileShader)
        {
            if (Tiles == null)
                return;

            TileCountX = 0;
            TileCountY = 0;

            int SizeX = Tiles.width;
            int SizeY = Tiles.height;

            float TextureWidth = Tiles.width;
            float TextureHeight = Tiles.height;

            Epsilon = new Vector2(0.5f / TextureWidth, 0.5f / TextureHeight);

            if (TileSeparator != 0)
                UvSeparatorSize = new Vector2((float)TileSeparator / TextureWidth, (float)TileSeparator / TextureHeight);

            while (SizeX > 0)
            {
                ++TileCountX;
                SizeX -= (int)TileSize.x + TileSeparator;
            }

            while (SizeY > 0)
            {
                ++TileCountY;
                SizeY -= (int)TileSize.y + TileSeparator;
            }

            UvSize = new Vector2((float)TileSize.x / TextureWidth, (float)TileSize.y / TextureHeight);

            if (TileShader == null && TileMaterial == null)
                return;

            if (TileMaterial == null)
                TileMaterial = new Material(TileShader);

            TileMaterial.mainTexture = Tiles;
        }

        public bool IsAffinity(Voxel2DInformation Info)
        {
            if (Info == null)
                return false;

            if (TypeAffinityId != 0 || Info.TypeAffinityId != 0)
            {
                if (TypeAffinityId == Info.Type || Info.TypeAffinityId == Type)
                    return true;
            }

            return false;
        }
    }
}