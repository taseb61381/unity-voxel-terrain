﻿using System;

using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class Voxel2DTile
    {
        public string Name = "New Tile";
        public Voxel2DHelper.Directions[] EmptyAround = new Voxel2DHelper.Directions[0];
        public Voxel2DHelper.Directions[] FullAround = new Voxel2DHelper.Directions[0];
        public Voxel2DHelper.Directions[] AffinityFullAround = new Voxel2DHelper.Directions[0];
        public Voxel2DHelper.Directions[] EqualFullAround = new Voxel2DHelper.Directions[0];
        public Vector2[] Tiles = new Vector2[0];
        public Vector2 TileOffset = new Vector2(0, 0);
        public int EmptyFlag = 0;
        public int FullFlag = 0;
        public int AffinityFullFlag = 0;
        public int EqualFullFlag = 0;
        public int ConstrainCount = 0;

        // Renvoi la position d'un tile sur l'image
        public Vector2 GetTile(int RandomId)
        {
            if (Tiles.Length <= 0)
                return new Vector2(-1, -1);
            else
            {
                if (RandomId >= 0)
                    return Tiles[RandomId % Tiles.Length];
                else
                    return Tiles[0];
            }
        }

        public void GenerateFlags()
        {
            EmptyFlag = 0;
            FullFlag = 0;
            AffinityFullFlag = 0;
            EqualFullFlag = 0;
            ConstrainCount = 0;

            ConstrainCount += EmptyAround.Length;
            for (int i = 0; i < EmptyAround.Length; ++i)
                EmptyFlag += Voxel2DHelper.DirectionFlag(EmptyAround[i]);

            ConstrainCount += FullAround.Length;
            for (int i = 0; i < FullAround.Length; ++i)
                FullFlag += Voxel2DHelper.DirectionFlag(FullAround[i]);

            ConstrainCount += AffinityFullAround.Length;
            for (int i = 0; i < AffinityFullAround.Length; ++i)
                AffinityFullFlag += Voxel2DHelper.DirectionFlag(AffinityFullAround[i]);

            ConstrainCount += EqualFullAround.Length;
            for (int i = 0; i < EqualFullAround.Length; ++i)
                EqualFullFlag += Voxel2DHelper.DirectionFlag(EqualFullAround[i]);
        }
    }
}
