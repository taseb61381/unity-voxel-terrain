﻿
using TerrainEngine;
using UnityEngine;

//[ExecuteInEditMode]
public class Voxel2DTileDecoder : MonoBehaviour
{
    public string Name;
    public Voxel2DTile[] Tiles; // Liste de tous les types de tiles possible , gauche a droite, etcc

    public void GenerateFlags()
    {
        foreach (Voxel2DTile Tile in Tiles)
            Tile.GenerateFlags();
    }

    // Renvoi la liste de tous les uvs pour les tiles
    public Rect[,] DecodeUvs(Voxel2DInformation Info)
    {
        Rect[,] Uvs = new Rect[Info.TileCountX,Info.TileCountY];

        for (int x = 0; x < Info.TileCountX; ++x)
        {
            for (int y = 0; y < Info.TileCountY; ++y)
            {
                float OffsetX = x * Info.UvSeparatorSize.x;
                float OffsetY = (y+1) * Info.UvSeparatorSize.y;

                OffsetX += x * Info.UvSize.x;
                OffsetY += y * Info.UvSize.y;

                Uvs[x, Info.TileCountY - y - 1] = new Rect(OffsetX, OffsetY, Info.UvSize.x, Info.UvSize.y);
            }
        }

        return Uvs;
    }

    public Voxel2DTile GetTile(int EmptyFlag, int FullFlag, int AffinityFullFlag, int EqualFullAroundFlag)
    {
        Voxel2DTile Tile = null;
        int MaxConstrain = -1;

        for (int i = Tiles.Length - 1; i >= 0; --i)
        {
            if ((EmptyFlag & Tiles[i].EmptyFlag) == Tiles[i].EmptyFlag
                  && (FullFlag & Tiles[i].FullFlag) == Tiles[i].FullFlag
                && (AffinityFullFlag & Tiles[i].AffinityFullFlag) == Tiles[i].AffinityFullFlag
                && (EqualFullAroundFlag & Tiles[i].EqualFullFlag) == Tiles[i].EqualFullFlag)
            {
                if (MaxConstrain < Tiles[i].ConstrainCount)
                {
                    Tile = Tiles[i];
                    MaxConstrain = Tiles[i].ConstrainCount;

                    if (EmptyFlag == Tiles[i].EmptyFlag 
                        && FullFlag == Tiles[i].FullFlag 
                        && AffinityFullFlag == Tiles[i].AffinityFullFlag
                        && EqualFullAroundFlag == Tiles[i].EqualFullFlag)
                    {
                        Tile = Tiles[i];
                        break;
                    }
                }
            }
        }

        return Tile;
    }
}
