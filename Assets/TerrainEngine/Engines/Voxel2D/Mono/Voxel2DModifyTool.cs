using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class Voxel2DModifyTool : MonoBehaviour {

    public TerrainObject TObject;
    public Light SunLight;
    public MoveControl MoveController;

    public int ToolSize = 0;
    public int SelectedID = 0;
    public float Light = 0.5f;
    public int Power = 10;
    public int ModifyDepth = 0;

    GUIContent[] Buttons;
    public int[] Items;
    public HashSet<VectorI3> TerrainList = new HashSet<VectorI3>();

    void Start()
    {
        Items = new int[TerrainManager.Instance.Palette.GetDataCount()];
        Buttons = new GUIContent[TerrainManager.Instance.Palette.GetDataCount()];
        //EventProcessor.AddEvent("OnFlushVoxel", EventDistances.NONE, new Vector3(), 0, OnVoxelFlush);

        for (int i = 0; i < Buttons.Length; ++i)
            Buttons[i] = new GUIContent(TerrainManager.Instance.Palette.GetVoxelData<VoxelInformation>((byte)i).Name, "T");

        if (TObject == null)
            TObject = GetComponent<TerrainObject>();
    }

    void OnGUI()
    {
        if (!DebugGUIProcessor.DrawGUI)
            return;

        SelectedID = GUI.SelectionGrid(new Rect(0, 30, Screen.width, 60), SelectedID, Buttons, Buttons.Length / 2);

        GUI.Label(new Rect(0, 100, 100, 20), new GUIContent("Tool Size : " + ToolSize,"T"));
        ToolSize = (int)GUI.HorizontalSlider(new Rect(0, 120, 100, 20), ToolSize, 0, 80);

        if (SunLight != null)
        {
            GUI.Label(new Rect(110, 100, 100, 20), new GUIContent("Light : " + Light,"T"));
            float L = GUI.HorizontalSlider(new Rect(110, 120, 100, 20), Light, 0f, 1f);
            if (L != Light)
            {
                SunLight.intensity = L;
                Light = L;
            }
        }

        if (MoveController != null)
        {
            GUI.Label(new Rect(220, 100, 150, 20), new GUIContent("Camera Speed : " + Power, "T"));
            int Move = (int)GUI.HorizontalSlider(new Rect(220, 120, 150, 20), Power, 1, 40);
            if (Move != Power)
            {
                Power = Move;
                MoveController.Power = Power;
            }
        }

        (TerrainManager.Instance as Voxel2DManager).GenerateLight = GUI.Toggle(new Rect(400, 100, 100, 20), (TerrainManager.Instance as Voxel2DManager).GenerateLight, "Generate Light");

        if (GUI.Button(new Rect(0, 140, 200, 20), new GUIContent("Regenerate Grass","T")))
            RegenerateGrass();

        if (GUI.Button(new Rect(0, 160, 200, 20), new GUIContent("Regenerate Flore","T")))
            RegenerateFlore();

        GUI.Box(new Rect(0, 180, 200, Screen.height - 180), "Items");

        float Y = 200;
        for (int i = 0; i < Items.Length; ++i)
        {
            if (Items[i] > 0)
            {
                GUI.Box(new Rect(0, Y, 200, 20), "");
                GUI.Label(new Rect(0, Y, 100, 20), TerrainManager.Instance.Palette.GetVoxelData<VoxelInformation>((byte)i).Name);
                GUI.Label(new Rect(110, Y, 90, 20), Items[i].ToString());
                Y += 20;
            }
        }
    }

    private IAction CurrentFlushAction;
    void Update()
    {
        if (GUIUtility.hotControl != 0 || GUIUtility.keyboardControl != 0 || GUI.tooltip != "")
            return;

        if (CurrentFlushAction != null && !CurrentFlushAction.IsDone())
            return;

        if (InputManager.GetMouseButton(0) || InputManager.GetMouseButton(1))
        {
            Vector3 VoxelPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            TerrainBlock Block = TerrainManager.Instance.GetBlockXYFromWorldPosition(VoxelPosition);

            if (Block != null)
            {
                VectorI3 Voxel = VoxelPosition - Block.WorldPosition;

                VoxelsOperator Operator = new VoxelsOperator();

                for(int x=-ToolSize;x<ToolSize+1;++x)
                    for (int y = -ToolSize; y < ToolSize + 1; ++y)
                    {
                        Operator.Voxels.Add(new VoxelPoint(Block.BlockId, (int)(Voxel.x + x), (int)(Voxel.y + y), (int)(ModifyDepth), InputManager.GetMouseButton(1) ? (byte)0 : (byte)SelectedID, 1f, VoxelModificationType.SET_TYPE_SET_VOLUME));
                    }

                TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
                CurrentFlushAction = TerrainManager.Instance.ModificationInterface.Flush(null, null, false, false);
            }
        }
    }

    void RegenerateGrass()
    {
        TerrainList.Clear();
        TerrainManager.Instance.Container.GetAllBlocks(ref TerrainList);
        VoxelsOperator Operator = new VoxelsOperator();

        foreach (VectorI3 BlockPos in TerrainList)
        {
            TerrainBlock TBlock = TerrainManager.Instance.Container.GetBlock(BlockPos) as TerrainBlock;
            if (TBlock != null && TBlock.HasState(TerrainBlockStates.CLOSED) == false)
            {
                List<ComplexVoxelPoint> Points = new List<ComplexVoxelPoint>();
                Voxel2DBiome.GenerateGrass(TBlock.Voxels, Points, TBlock);
                Operator.Add(Points);
            }
        }

        TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
        TerrainManager.Instance.ModificationInterface.Flush(ActionProcessor.Instance.UnityThread, TObject, false);
    }

    void RegenerateFlore()
    {
        TerrainList.Clear();
        TerrainManager.Instance.Container.GetAllBlocks(ref TerrainList);
        VoxelsOperator Operator = new VoxelsOperator();
        foreach (VectorI3 BlockPos in TerrainList)
        {
            TerrainBlock TBlock = TerrainManager.Instance.Container.GetBlock(BlockPos) as TerrainBlock;
            if (TBlock != null && TBlock.HasState(TerrainBlockStates.CLOSED) == false)
            {
                List<ComplexVoxelPoint> Points = new List<ComplexVoxelPoint>();
                Voxel2DBiome.GenerateFlore(TBlock.Voxels, Points, new FastRandom(0), TBlock);
                Operator.Add(Points);
            }
        }

        TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
        TerrainManager.Instance.ModificationInterface.Flush(ActionProcessor.Instance.UnityThread, TObject, false);
    }

    public bool OnVoxelFlush(object Obj, Vector3 Distance, object Data)
    {
        List<VoxelFlush> Points = (List<VoxelFlush>)Data;

        foreach (VoxelFlush Info in Points)
        {
            // Remove Tile From Map
            if (Info.OldType != 0 && Info.NewType == 0)
            {
                ++Items[Info.OldType];
            } // Add Tile
            else if (Info.OldType == 0 && Info.NewType != 0)
            {
                if (Items[Info.NewType] > 0)
                    --Items[Info.NewType];
            }
        }

        return false;
    }
}
