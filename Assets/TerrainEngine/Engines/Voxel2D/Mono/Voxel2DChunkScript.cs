
using UnityEngine;


public class Voxel2DChunkScript : TerrainChunkScript 
{
    public override void Apply(IMeshData Data)
    {
        Mesh CMesh = GetMesh();
        CMesh.Clear(false);
        CMesh.vertices = Data.Vertices;
        CMesh.uv = Data.UVs;
        CMesh.colors = Data.Colors;
        CMesh.subMeshCount = Data.Materials.Length;
        for (int i = 0; i < Data.Indices.Length; ++i)
            CMesh.SetTriangles(Data.Indices[i], i);
        CRenderer.sharedMaterials = Data.Materials;
        base.Apply(Data);
        CMesh.RecalculateNormals();
    }
}
