﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class FluidGenerator2D : FluidMeshConstructor
{
    public override IMeshData GenerateMesh(ActionThread Thread, TerrainBlock Block, int ChunkId, FluidDatas FData, FluidInterpolation Interpolations, FluidInformation FInfo, byte Type)
    {
        TempMeshData Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
        IMeshData Data = PoolManager.UPool.GetSimpleMeshData();

        List<Material> Materials = new List<Material>();
        List<int[]> Indices = new List<int[]>();

        int x,y, cellLevel = 0;

        int MatId = 0;
        float Size;

        foreach (Voxel2DInformation BlockInfo in (TerrainManager.Instance.Palette as Voxel2DPalette).VoxelBlocks)
        {
            if (BlockInfo.VoxelType != VoxelTypes.FLUID || BlockInfo.Type != Type)
                continue;

            Data.Materials[MatId] = BlockInfo.TileMaterial;
            Rect Uv = BlockInfo.Uvs[0, 0];
            for (x = 0; x < Block.Information.VoxelsPerAxis; ++x)
            {
                for (y = 0; y < Block.Information.VoxelsPerAxis; ++y)
                {
                    if (Interpolations[x,y,0].FluidType != BlockInfo.Type)
                        continue;

                    cellLevel = Interpolations[x,y,0].Volume;
                    if (cellLevel > 0 && Interpolations[x,y,0].FluidType == BlockInfo.Type)
                    {
                        Size = (float)cellLevel / (float)255f;
                        if (Size > 0.8f) Size = 1.0f;
                        else if (Size < 0.2f) Size = 0.2f;

                        Voxel2DBlock.AddFace(x, y, 0, ref Info, Color.white, new Vector2(1, 1), new Vector2(), 1f - Size);
                        Info.Uvs.Add(new Vector2(Uv.x + BlockInfo.Epsilon.x, Uv.y + Uv.height - BlockInfo.Epsilon.y)); // Left Bottom
                        Info.Uvs.Add(new Vector2(Uv.x + Uv.width - BlockInfo.Epsilon.x, Uv.y + Uv.height - BlockInfo.Epsilon.y)); // Right Bottom

                        Info.Uvs.Add(new Vector2(Uv.x + BlockInfo.Epsilon.x, Uv.y + BlockInfo.Epsilon.y)); // Left Top
                        Info.Uvs.Add(new Vector2(Uv.x + Uv.width - BlockInfo.Epsilon.x, Uv.y + BlockInfo.Epsilon.y)); // Right Top
                    }
                }
            }

            Indices.Add(Info.Triangles.Indices.ToArray());
            Materials.Add(BlockInfo.FluidMaterial);
            Info.Triangles.Indices.ClearFast();
            ++MatId;
        }

        Data.Materials = Materials.ToArray();
        Data.Indices = Indices.ToArray();

        Data.Colors = Info.Colors.ToArray();
        Data.Vertices = Info.Vertices.ToArray();
        Data.UVs = Info.Uvs.ToArray();
        if (!Data.IsValid())
        {
            Data.Close();
            Data = null;
        }

        return Data;
    }


    /*public override void Build(TerrainBlock Block, ICustomTerrainData IData)
    {
        Mesh CMesh = Block.Script.GetComponent<MeshFilter>().sharedMesh;
        MeshRenderer CRenderer = Block.Script.GetComponent<MeshRenderer>();
        
        CMesh.Clear(false);

        MeshData Data = IData.CData;
        if (Data == null || Block.Closed)
            return;

        CMesh.vertices = Data.Vertices;
        CMesh.uv = Data.Uvs;
        CMesh.colors = Data.Colors;
        CMesh.subMeshCount = Data.Materials.Length;
        for (int i = 0; i < Data.Indices.Length; ++i)
            CMesh.SetTriangles(Data.Indices[i], i);
        CRenderer.sharedMaterials = Data.Materials;
        CMesh.RecalculateNormals();
    }

    public override MeshData Generate(TerrainBlock Block, ICustomTerrainData Data)
    {
        if (Block.Closed)
            return null;

        TempMeshData Info = new TempMeshData(true);
        MeshData MeshData = new MeshData();

        int cellLevel = 0;
        MeshData.Materials = new Material[Processor.BlocksId.Length];
        MeshData.Indices = new int[Processor.BlocksId.Length][];

        int MatId = 0;

        for (int i = 0; i < Processor.BlocksId.Length; ++i)
        {
            byte BlockId = (byte)Processor.BlocksId[i];
            Voxel2DInformation BlockInfo = Processor.Blocks[i] as Voxel2DInformation;
            MeshData.Materials[MatId] = BlockInfo.TileMaterial;
            Rect Uv = BlockInfo.Uvs[0, 0];
            for (int x = 0; x < Block.Information.VoxelCountX; ++x)
            {
                for (int y = 0; y < Block.Information.VoxelCountY; ++y)
                {
                    cellLevel = Data.GetInt(x, y, 0);
                    if (cellLevel > 0 && Block.Voxels.GetByte(x, y, 0) == BlockId)
                    {
                        float Size = (float)cellLevel / (float)EulerianFluidData.MaxLevel;
                        if (Size > 0.8f)
                            Size = 1.0f;
                        else if (Size < 0.2f)
                            Size = 0.2f;

                        Voxel2DChunk.AddFace(x, y, 0, ref Info, Color.white, new Vector2(1, 1), new Vector2(), 1f - Size);
                        Info.Uvs.Add(new Vector2(Uv.x + BlockInfo.Epsilon.x, Uv.y + Uv.height - BlockInfo.Epsilon.y)); // Left Bottom
                        Info.Uvs.Add(new Vector2(Uv.x + Uv.width - BlockInfo.Epsilon.x, Uv.y + Uv.height - BlockInfo.Epsilon.y)); // Right Bottom

                        Info.Uvs.Add(new Vector2(Uv.x + BlockInfo.Epsilon.x, Uv.y + BlockInfo.Epsilon.y)); // Left Top
                        Info.Uvs.Add(new Vector2(Uv.x + Uv.width - BlockInfo.Epsilon.x, Uv.y + BlockInfo.Epsilon.y)); // Right Top
                    }
                }
            }

            MeshData.Indices[MatId] = Info.Indices.ToArray();
            Info.Indices.Clear();
            ++MatId;
        }

        MeshData.Colors = Info.Colors.ToArray();
        MeshData.Vertices = Info.Vertices.ToArray();
        MeshData.Uvs = Info.Uvs.ToArray();
        return MeshData;
    }*/
}