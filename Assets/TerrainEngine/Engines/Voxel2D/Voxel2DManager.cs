
using TerrainEngine;
using UnityEngine;

public class Voxel2DManager : TerrainManager
{
    public bool GenerateLight = false;
    public Shader TileShader;

    public override bool InitTextures()
    {
        CombineBlockChunks = false;

        if (TileShader == null)
            TileShader = Shader.Find("Transparent/Diffuse");

        Voxel2DPalette VPalette = this.Palette as Voxel2DPalette;
        for (int i = 0; i < VPalette.VoxelBlocks.Length; ++i)
        {
            VPalette.VoxelBlocks[i].Type = i;
        }

        for (int i = 0; i < VPalette.VoxelBlocks.Length; ++i)
        {
            if (VPalette.VoxelBlocks[i] != null)
            {
                VPalette.VoxelBlocks[i].Generate(TileShader);
                VPalette.VoxelBlocks[i].TypeAffinityId = (byte)Palette.GetVoxelId(VPalette.VoxelBlocks[i].TypeAffinity);
                VPalette.VoxelBlocks[i].BackgroundTileId = (byte)Palette.GetVoxelId(VPalette.VoxelBlocks[i].BackGroundTile);
                if (VPalette.VoxelBlocks[i].Decoder != null)
                {
                    VPalette.VoxelBlocks[i].Decoder.GenerateFlags();
                    VPalette.VoxelBlocks[i].Uvs = VPalette.VoxelBlocks[i].Decoder.DecodeUvs(VPalette.VoxelBlocks[i]);
                }
            }
        }

        return true;
    }

    public override TerrainBlock CreateTerrain()
    {
        return new Voxel2DBlock(this);
    }
}
