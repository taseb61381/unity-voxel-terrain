﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class Voxel2DBiome : IBlockProcessor
{
    static public TerrainManager StaticManager;
    static public int GroundLevel = 0;
    static private IWorldBiome biomeProvider;
    static public int Seed = 0;

    public bool CanGenerateBackGround = true;
    public bool CanGenerateCopper = true;
    public bool CanGenerateOre = true;
    public bool CanGenerateStone = true;
    public bool CanGenerateCaves = true;
    public bool CanGenerateSpiders = true;
    public bool CanGenerateLianas = true;
    public bool CanGenerateGrass = true;
    public bool CanGenerateFlore = true;
    public bool CanGenerateMineral = true;
    public bool CanGenerateTree = true;

    public override void OnInitManager(TerrainManager Manager)
    {
        base.OnInitManager(Manager);
        Voxel2DBiome.StaticManager = Manager;
        GroundLevel = (int)(TerrainManager.Instance.Informations.WorldGroundLevel / TerrainManager.Instance.Informations.Scale);
    }

    public override void OnInitSeed(int Seed)
    {
        Voxel2DBiome.Seed = Seed;

        if (biomeProvider == null)
            biomeProvider = new WorldBiome(Seed);
    }

    public override bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        if (ChunkId != 0)
            return true;

        FastRandom FRandom = new FastRandom(Seed);

        int WorldX = 0;
        int WorldY = 0;
        byte BlockType = BlockManager.None;
        float BlockVolume = 0;

        int[] Grounds = Data.GetGrounds(Block.Information.VoxelsPerAxis, Block.Information.VoxelsPerAxis);

        int x, y;
        float groundHeight;
        // Generate Ground
        for (x = 0; x < Block.Information.VoxelsPerAxis; ++x)
        {
            WorldX = Block.WorldX + x;

            groundHeight = GetBlockNoise(WorldX, 0) + GroundLevel;

            Grounds[x*Block.VoxelsPerAxis] = (int)groundHeight;

            for (y = Block.Information.VoxelsPerAxis - 1; y >= 0; --y)
            {
                WorldY = Block.WorldY + y;
                BlockType = 0;
                BlockVolume = 0;

                if (WorldY <= groundHeight)
                {
                    BlockType = BlockManager.Dirt;
                    BlockVolume = 1.0f;
                }

                Block.SetTypeAndVolume(x, y, 0, BlockType, BlockVolume);
            }
        }

        if(CanGenerateBackGround)
            GenerateBackground(Block, null);

        if(CanGenerateCopper)
            GenerateMineral(Block, null, FRandom, (byte)TerrainManager.Instance.Palette.GetVoxelId("Copper"), new Vector2(0.015f, 0.100f), new VectorI2(200, 198));

        if(CanGenerateStone)
            GenerateMineral(Block, null, FRandom, (byte)TerrainManager.Instance.Palette.GetVoxelId("Stone"), new Vector2(-0.100f, 0.015f), new VectorI2(200, 198));

        if(CanGenerateMineral)
            GenerateMineral(Block, null, FRandom, (byte)TerrainManager.Instance.Palette.GetVoxelId("Ore"), new Vector2(-0.200f, -0.100f), new VectorI2(300, 298));

        if(CanGenerateCaves)
            GenerateCaves(Block, null, FRandom);

        if(CanGenerateGrass)
            GenerateGrass(Block, null, null);

        if(CanGenerateTree)
            GenerateTree(Block, null, FRandom);

        if(CanGenerateFlore)
            GenerateFlore(Block, null, FRandom, null);

        if(CanGenerateLianas)
            GenerateLianes(Block, null, FRandom, null);

        if(CanGenerateSpiders)
            GenerateSpider(Block, null, FRandom, null);

        return true;
    }

    static public void GenerateGrass(TerrainDatas Block, List<ComplexVoxelPoint> Modified, TerrainBlock TBlock)
    {
        // Generate Grass
        for (int x = 0; x < Block.Information.VoxelsPerAxis; ++x)
            for (int y = Block.Information.VoxelsPerAxis - 1; y >= 0; --y)
                if (Block.GetType(x, y, 0) == 2 && (x != 0 && y != 0)) // Dirt
                {
                    bool ToGrass = false;
                    foreach (Vector2 dir in Voxel2DHelper.CheckDirections)
                    {

                        if (!Block.IsValid(x + (int)dir.x, y + (int)dir.y, 0))
                            continue;

                        byte Value = Block.GetType(x + (int)dir.x, y + (int)dir.y, 0);
                        if (Value == 0)
                        {
                            ToGrass = true;
                            break;
                        }
                    }

                    if (ToGrass)
                    {
                        if (Modified == null)
                            Block.SetType(x, y, 0, BlockManager.Grass);
                        else
                            Modified.Add(new ComplexVoxelPoint(TBlock.BlockId, x, y, 0, BlockManager.Grass));
                    }

                }
    }

    static public void GenerateFlore(TerrainDatas Block, List<ComplexVoxelPoint> Modified, FastRandom FRandom, TerrainBlock TBlock)
    {
        byte[] Plants = new byte[3];

        Plants[0] = BlockManager.Flore;
        Plants[1] = (byte)StaticManager.Palette.GetVoxelId("BigPlant01");
        Plants[2] = (byte)StaticManager.Palette.GetVoxelId("BigPlant02");

        // Generate Flore
        for (int x = 0; x < Block.Information.VoxelsPerAxis; ++x)
            for (int y = Block.Information.VoxelsPerAxis - 2; y >= 0; --y)
            {
                if (Block.GetType(x, y, 0) != BlockManager.Grass)
                    continue;

                if (Block.GetType(x, y + 1, 0) == 0 && FRandom.randomInt(100) > -25)
                {
                    if (Modified == null)
                        Block.SetType(x, y + 1, 1, Plants[FRandom.randomIntAbs(3)]);
                    else
                        Modified.Add(new ComplexVoxelPoint(TBlock.BlockId, x, (y + 1), 1, (byte)Plants[FRandom.randomIntAbs(3)]));

                    break;
                }
            }
    }

    static public void GenerateLianes(TerrainDatas Block, List<ComplexVoxelPoint> Modified, FastRandom FRandom, TerrainBlock TBlock)
    {
        byte Lianes = (byte)StaticManager.Palette.GetVoxelId("Lianes");

        // Generate Flore
        for (int x = 0; x < Block.Information.VoxelsPerAxis; ++x)
            for (int y = Block.Information.VoxelsPerAxis - 2; y >= 0; --y)
            {
                if (Block.GetType(x, y, 0) != BlockManager.Dirt && Block.GetType(x, y, 0) != BlockManager.Grass)
                    continue;

                if (Block.GetTypeU(x, y - 1, 0) != 0)
                    continue;

                if (FRandom.randomInt(100) < -50)
                    continue;

                for (int px = -2; px < 2; ++px)
                {
                    if (Block.GetTypeU(x + px, y, 0) != BlockManager.Dirt && Block.GetTypeU(x + px, y, 0) != BlockManager.Grass)
                        break;

                    if (Block.GetTypeU(x + px, y - 1, 0) != 0)
                        break;

                    int Size = FRandom.randomIntAbs(10) + 2;

                    for (int i = 0; i < Size; ++i)
                    {
                        if (Block.GetTypeU(x + px, y - 1 - i, 0) != 0)
                            break;

                        if (Modified == null)
                            Block.SetTypeU(x + px, y - 1 - i, 0, Lianes);
                        else
                            Modified.Add(new ComplexVoxelPoint(TBlock.BlockId, (x + px), (y - 1 - i), 0, Lianes));
                    }
                }
            }
    }

    static public void GenerateSpider(TerrainDatas Block, List<ComplexVoxelPoint> Modified, FastRandom FRandom, TerrainBlock TBlock)
    {
        byte Spider = (byte)StaticManager.Palette.GetVoxelId("Spider");

        // Generate Flore
        for (int x = 0; x < Block.Information.VoxelsPerAxis; ++x)
            for (int y = Block.Information.VoxelsPerAxis - 2; y >= 0; --y)
            {
                if (Block.GetType(x, y, 0) != BlockManager.Dirt && Block.GetType(x, y, 0) != BlockManager.Grass)
                    continue;

                if (Block.GetTypeU(x, y - 1, 0) != 0)
                    continue;

                if (FRandom.randomInt(100) < -75)
                    continue;

                for (int px = -2; px < 2; ++px)
                {
                    if (Block.GetTypeU(x + px, y, 0) != BlockManager.Dirt && Block.GetTypeU(x + px, y, 0) != BlockManager.Grass)
                        break;

                    if (Block.GetTypeU(x + px, y - 1, 0) != 0)
                        break;

                    int Size = FRandom.randomIntAbs(10) + 2;

                    for (int i = 0; i < Size; ++i)
                    {
                        if (Block.GetTypeU(x + px, y - 1 - i, 0) != 0)
                            break;

                        if (Modified == null)
                            Block.SetTypeU(x + px, y - 1 - i, 0, Spider);
                        else
                            Modified.Add(new ComplexVoxelPoint(TBlock.BlockId, (x + px), (y - 1 - i), 0, Spider));
                    }
                }
            }
    }

    static public void GenerateTree(TerrainDatas Block, List<ComplexVoxelPoint> Modified, FastRandom FRandom)
    {
        byte Tree = (byte)StaticManager.Palette.GetVoxelId("Tree");
        byte TreeBranche = (byte)StaticManager.Palette.GetVoxelId("TreeBranche");
        byte TreeTop = (byte)StaticManager.Palette.GetVoxelId("TreeTop");

        int z = 2;

        int WorldX = 0;
        // Generate Flore
        for (int y = Block.Information.VoxelsPerAxis - 6; y >= 0; --y)
        {
            for (int x = 2; x < Block.Information.VoxelsPerAxis - 2; ++x)
            {
                if (Block.GetType(x, y, 0) != BlockManager.Grass)
                    continue;

                if (Block.GetType(x, y + 1, 0) != BlockManager.None)
                    continue;

                if (FRandom.randomIntAbs(100) < 40)
                    continue;

                int Size = FRandom.randomIntAbs(20) + 4;

                if (Size + y >= Block.Information.VoxelsPerAxis)
                    Size = Block.Information.VoxelsPerAxis - y - 1;

                if (Size <= 4)
                    continue;

                WorldX = Block.WorldX + x;
                float groundHeight = GetBlockNoise(WorldX, 0) + GroundLevel;
                if (Block.WorldY + y < groundHeight - 5)
                    continue;
                
                for (int i = 1; i <= Size; ++i)
                {
                    if (Block.GetType(x - 1, y + i, z) == Tree
                        || Block.GetType(x - 2, y + i, z) == Tree
                        || Block.GetType(x + 1, y + i, z) == Tree
                        || Block.GetType(x + 2, y + i, z) == Tree
                        || Block.GetType(x, y + i, 0) != 0)
                    {
                        Size = 0;
                        break;
                    }

                }

                if (Size == 0)
                    continue;

                for (int i = 1; i <= Size; ++i)
                {
                    // Generate Ground Tree
                    if (i == 1)
                    {
                        if (Block.GetType(x + 1, y + i, z) == 0 && Block.GetType(x + 1, y + i - 1, z) != 0)
                            Block.SetType(x + 1, y + i, z, Tree);

                        if (Block.GetType(x - 1, y + i, z) == 0 && Block.GetType(x - 1, y + i - 1, z) != 0)
                            Block.SetType(x - 1, y + i, z, Tree);
                    }

                    // Genereate Branches
                    if (i < Size - 1 && i != 1)
                    {
                        if (Block.GetType(x + 2, y + i, z) != Tree && (Block.GetType(x + 1, y + i - 1, z) != Tree && Block.GetType(x + 1, y + i - 1, z) != TreeBranche) && FRandom.randomIntAbs(3) == 0)
                        {
                            Block.SetType(x + 1, y + i, z, FRandom.randomIntAbs(2) == 0 ? TreeBranche : Tree);
                        }

                        if (Block.GetType(x - 2, y + i, z) != Tree && (Block.GetType(x - 1, y + i - 1, z) != Tree && Block.GetType(x - 1, y + i - 1, z) != TreeBranche) && FRandom.randomIntAbs(3) == 0)
                        {
                            Block.SetType(x - 1, y + i, z, FRandom.randomIntAbs(2) == 0 ? TreeBranche : Tree);
                        }
                    }

                    if (i != Size)
                        Block.SetType(x, y + i, z, Tree);
                    else
                        Block.SetType(x, y + i, z, FRandom.randomIntAbs(2) == 0 ? TreeTop : Tree);
                }
            }
        }
    }

    static public void GenerateCaves(TerrainDatas Block, List<ComplexVoxelPoint> Modified, FastRandom FRandom)
    {
        int WorldX = 0;
        int WorldY = 0;

        for (int x = 0; x < Block.Information.VoxelsPerAxis; ++x)
        {
            WorldX = Block.WorldX + x;

            for (int y = Block.Information.VoxelsPerAxis - 1; y >= 0; --y)
            {
                byte Value = Block.GetType(x, y, 0);

                if (Value == 0)
                    continue;

                WorldY = Block.WorldY + y;

                int noiseX = WorldX;
                float octave1 = PerlinSimplexNoise.noise(noiseX * 0.009f, WorldY * 0.009f, 1 * 0.009f) * 0.25f;

                float initialNoise = octave1 + PerlinSimplexNoise.noise(noiseX * 0.04f, WorldY * 0.04f, 1 * 0.04f) * 0.15f;
                initialNoise += PerlinSimplexNoise.noise(noiseX * 0.08f, WorldY * 0.08f, 1 * 0.08f) * 0.05f;

                if (initialNoise > 0.05f)
                    Block.SetType(x, y, 0, BlockManager.None);
            }
        }
    }

    static public void GenerateMineral(TerrainDatas Block, List<ComplexVoxelPoint> Modified, FastRandom FRandom, byte Type, Vector2 Noise, VectorI2 Chances)
    {
        int WorldX = 0;
        int WorldY = 0;

        for (int x = 0; x < Block.Information.VoxelsPerAxis; ++x)
        {
            WorldX = Block.WorldX + x;

            for (int y = Block.Information.VoxelsPerAxis - 1; y >= 0; --y)
            {
                byte Value = Block.GetType(x, y, 0);

                if (Value == 0)
                    continue;

                WorldY = Block.WorldY + y;

                int noiseX = WorldX;
                float octave1 = PerlinSimplexNoise.noise(noiseX * 0.009f, WorldY * 0.009f, 1 * 0.009f) * 0.25f;

                float initialNoise = octave1 + PerlinSimplexNoise.noise(noiseX * 0.04f, WorldY * 0.04f, 1 * 0.04f) * 0.15f;
                initialNoise += PerlinSimplexNoise.noise(noiseX * 0.08f, WorldY * 0.08f, 1 * 0.08f) * 0.05f;

                int Rand = FRandom.randomIntAbs(Chances.x);
                if (initialNoise > Noise.x && initialNoise < Noise.y && Rand > Chances.y)
                {
                    for (int i = 0; i < 4; ++i)
                    {
                        int SizeX = FRandom.randomIntAbs(8);
                        int SizeY = FRandom.randomIntAbs(8);

                        for (int px = -SizeX / 2; px < SizeX / 2; ++px)
                        {
                            for (int py = -SizeY / 2; py < SizeY / 2; ++py)
                            {
                                Block.SetTypeU(x + px + FRandom.randomInt(3), y + py + FRandom.randomInt(3), 0, Type);
                            }
                        }
                    }
                }
            }
        }
    }

    static public void GenerateBackground(TerrainDatas Block, List<ComplexVoxelPoint> Modified)
    {
        byte Type = 0;
        Voxel2DInformation Info = null;

        for (int x = 0; x < Block.Information.VoxelsPerAxis; ++x)
        {
            for (int y = Block.Information.VoxelsPerAxis - 1; y >= 0; --y)
            {
                Type = Block.GetType(x, y, 0);

                if (Type == BlockManager.None)
                    continue;

                Info = StaticManager.Palette.GetVoxelData<Voxel2DInformation>(Type);

                if (Info != null && Info.BackgroundTileId != BlockManager.None)
                    Block.SetType(x, y, 2, Info.BackgroundTileId);
            }
        }
    }

    static private float GetBlockNoise(int width, int depth)
    {
        float mediumDetail = PerlinSimplexNoise.noise(width / 300.0f, depth / 300.0f, 20);
        float fineDetail = PerlinSimplexNoise.noise(width / 80.0f, depth / 80.0f, 30);
        float bigDetails = PerlinSimplexNoise.noise(width / 800.0f, depth / 800.0f);

        float temp = biomeProvider.getTemperatureAt(width, depth);
        float humidity = biomeProvider.getHumidityAt(width, depth) * temp;

        Vector2 distanceToMountainBiome = new Vector2(temp - 0.25f, humidity - 0.35f);
        float mIntens = (float)MathHelper.clamp(1.0f - distanceToMountainBiome.magnitude * 3.0f);


        float noise = bigDetails * 32.0f * mIntens + mediumDetail * 16.0f + fineDetail * 8.0f; // *(bigDetails
        // *
        // 64.0f);
        return noise * 3 + 16;
    }
}
