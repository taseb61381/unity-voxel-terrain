﻿
using UnityEngine;



public class Voxel2DTouchModifyTool : TouchControl
{
    public override void UpdateCursor()
    {
        if (CursorObject == null || Manager == null)
            return;

        CursorObject.transform.position = TerrainManager.Instance.Informations.GetWorldVoxelPosition(cameraTransform.position + cameraTransform.transform.forward * (CursorDistance + ToolSize * 2)) + CursorOffset + new Vector3(0.5f,0.5f,0.5f);
        float t = ToolSize + ToolSize + 1;

        CursorObject.transform.localScale = new Vector3(t, t, t);
    }

    public override void OnLeftDown()
    {
        ModType = VoxelModificationType.SET_TYPE_REMOVE_VOLUME;
        Vector3 Position = GetCursorVoxel();
        Modify(Manager.GetBlockFromWorldPosition(Position), Position, new Vector3(), ToolSize, ToolSize, 0, false);
    }

    public override void OnRightDown()
    {
        ModType = VoxelModificationType.SET_TYPE_ADD_VOLUME;
        Vector3 Position = GetCursorVoxel();
        Modify(Manager.GetBlockFromWorldPosition(Position), Position, new Vector3(), ToolSize, ToolSize, 0, false);
    }

    public override void UpdateDistance()
    {
        if (RangeIncButton.IsKeyDown())
            ToolSize++;
        else if (RangeDecButton.IsKeyDown())
            ToolSize--;

        if (ToolSize < 0)
            ToolSize = 0;

        Mathf.Clamp(ToolSize, 0, 10);
    }

    public override void UpdateCamera()
    {
        Vector3 v = Vector3.zero;
        v += targetRigid.transform.right * LeftJoystik.position.x;
        v += targetRigid.transform.up * LeftJoystik.position.y;
        v *= Speed;
        targetRigid.velocity = v;

        if (RightJoystik.IsKeyDown())
        {
            if(RightJoystik.position.x > 0.4f)
                CursorOffset.x += 1;
            else if(RightJoystik.position.x < -0.4f)
                CursorOffset.x -= 1;

            if (RightJoystik.position.y > 0.4f)
                CursorOffset.y += 1;
            else if (RightJoystik.position.y < -0.4f)
                CursorOffset.y -= 1;

            CursorOffset.x = Mathf.Clamp(CursorOffset.x, -20, 20);
            CursorOffset.y = Mathf.Clamp(CursorOffset.y, -20, 20);
        }
    }

    public override Vector3 GetCursorVoxel()
    {
        Vector3 Positon;
        if (CursorObject != null)
            Positon = CursorObject.transform.position;
        else
            Positon = gameObject.transform.position;
        Positon.z = 9999;
        return Positon;
    }
}