using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class Voxel2DTileEditor : EditorWindow
{
    [MenuItem("TerrainEngine/Terrains/2D/Tile Editor")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(Voxel2DTileEditor));
    }

    public GameObject DecoderObj;
    public Voxel2DTileDecoder Decoder;
    public Texture2D DemoTexture;
    public Texture2D[,] PackTiles;
    public Voxel2DInformation TempInfo;

    public int SelectedTileID = 0;

    public Vector2 TileSize = new Vector2(16, 16);
    public int TileSeparator = 2;

    public void OnGUI()
    {
        GameObject Obj = EditorGUI.ObjectField(new Rect(0, 0, 200, 20), Decoder, typeof(GameObject), true) as GameObject;
        if (Obj != null)
        {
            if (Decoder == null || DecoderObj != Obj)
            {
                Decoder = Obj.GetComponent<Voxel2DTileDecoder>();
                DecoderObj = Obj;
            }
        }

        if (Decoder == null)
            return;

        TileSize = EditorGUI.Vector2Field(new Rect(220, 0, 180, 20), "TileSize", TileSize);
        TileSeparator = EditorGUI.IntField(new Rect(400, 0, 200, 20), "TileSeparator", TileSeparator);

        Texture2D NewTexture = EditorGUI.ObjectField(new Rect(600, 0, 200, 20), DemoTexture, typeof(Texture2D), true) as Texture2D;
        if (DemoTexture == null || NewTexture != DemoTexture)
        {
            if (NewTexture != null)
            {
                DemoTexture = NewTexture;

                TempInfo = new Voxel2DInformation();
                TempInfo.TileSize = TileSize;
                TempInfo.TileSeparator = TileSeparator;
                TempInfo.Tiles = DemoTexture;
                TempInfo.Generate(null);

                PackTiles = new Texture2D[TempInfo.TileCountX, TempInfo.TileCountY];
                for (int x = 0; x < TempInfo.TileCountX; ++x)
                {
                    for (int y = 0; y < TempInfo.TileCountY; ++y)
                    {
                        int OffsetX = x * TileSeparator;
                        int OffsetY = (y + 1) * TileSeparator;

                        OffsetX += x * (int)TileSize.x;
                        OffsetY += y * (int)TileSize.y;

                        Texture2D t = new Texture2D((int)TileSize.x, (int)TileSize.y);
                        Color[] cols = DemoTexture.GetPixels(OffsetX, OffsetY, (int)TileSize.x, (int)TileSize.y);
                        t.SetPixels(cols);
                        t.Apply();
                        PackTiles[x, TempInfo.TileCountY - y - 1] = t;
                    }
                }
            }
        }

        DrawLeftMenu();

        if (Decoder.Tiles.Length > 0)
        {
            DrawMidleMenu();
            DrawRightMenu();
            EditorUtility.SetDirty(DecoderObj);
        }
    }

    public float Height = 0;
    public Vector2 LeftScroll = new Vector2();
    public void DrawLeftMenu()
    {
        LeftScroll = GUI.BeginScrollView(new Rect(0, 20, 220, Screen.height - 40), LeftScroll, new Rect(0, 20, 200, Height));

        for (int i = 0; i < Decoder.Tiles.Length; ++i)
        {
            if (SelectedTileID == i)
                GUI.backgroundColor = Color.blue;

            bool Select = GUI.Button(new Rect(0, 20 + i * 20, 200, 20), Decoder.Tiles[i].Name + ":" + i);

            GUI.backgroundColor = Color.white;

            if (Select)
                SelectedTileID = i;
        }


        GUI.color = Color.green;
        if (GUI.Button(new Rect(0, Height, 200, 20), "Add New Tile"))
        {
            Array.Resize(ref Decoder.Tiles, Decoder.Tiles.Length + 1);
            Decoder.Tiles[Decoder.Tiles.Length - 1] = new Voxel2DTile();
        }
        GUI.color = Color.white;

        GUI.EndScrollView();

        Height = 20 + Decoder.Tiles.Length * 20;
    }


    public void DrawMidleMenu()
    {
        float StartY = 40;
        Voxel2DTile Tile = Decoder.Tiles[SelectedTileID];
        Tile.Name = EditorGUI.TextField(new Rect(220, StartY, 200, 20), Tile.Name);

        StartY += 20;
        Tile.TileOffset = EditorGUI.Vector2Field(new Rect(220, StartY, 200, 40), "Offset", Tile.TileOffset);
        StartY += 20;

        GUI.Label(new Rect(220, StartY + 20, 100, 20), "Empty");
        StartY += 10;
        DrawToggle(new Rect(220, StartY + 30, 20, 20), Voxel2DHelper.Directions.TopLeft, ref Tile.EmptyAround);
        DrawToggle(new Rect(240, StartY + 30, 20, 20), Voxel2DHelper.Directions.Top, ref Tile.EmptyAround);
        DrawToggle(new Rect(260, StartY + 30, 20, 20), Voxel2DHelper.Directions.TopRight, ref Tile.EmptyAround);

        DrawToggle(new Rect(220, StartY + 50, 20, 20), Voxel2DHelper.Directions.Left, ref Tile.EmptyAround);
        if (DemoTexture != null && Tile.Tiles.Length > 0)
            GUI.DrawTexture(new Rect(240, StartY + 50, 20, 20), PackTiles[(int)Tile.Tiles[0].x, (int)Tile.Tiles[0].y]);
        DrawToggle(new Rect(260, StartY + 50, 20, 20), Voxel2DHelper.Directions.Right, ref Tile.EmptyAround);

        DrawToggle(new Rect(220, StartY + 70, 20, 20), Voxel2DHelper.Directions.BottomLeft, ref Tile.EmptyAround);
        DrawToggle(new Rect(240, StartY + 70, 20, 20), Voxel2DHelper.Directions.Bottom, ref Tile.EmptyAround);
        DrawToggle(new Rect(260, StartY + 70, 20, 20), Voxel2DHelper.Directions.BottomRight, ref Tile.EmptyAround);

        if (GUI.Button(new Rect(220, StartY + 100, 100, 20), "Remove"))
        {
            List<Voxel2DTile> Tiles = new List<Voxel2DTile>();
            Tiles.AddRange(Decoder.Tiles);
            Tiles.Remove(Tile);
            Decoder.Tiles = Tiles.ToArray();
            SelectedTileID = 0;
            return;
        }

        StartY -= 10;
        GUI.Label(new Rect(290, StartY + 20, 100, 20), "Full");
        StartY += 10;

        DrawToggle(new Rect(290, StartY + 30, 20, 20), Voxel2DHelper.Directions.TopLeft, ref Tile.FullAround);
        DrawToggle(new Rect(310, StartY + 30, 20, 20), Voxel2DHelper.Directions.Top, ref Tile.FullAround);
        DrawToggle(new Rect(330, StartY + 30, 20, 20), Voxel2DHelper.Directions.TopRight, ref Tile.FullAround);

        DrawToggle(new Rect(290, StartY + 50, 20, 20), Voxel2DHelper.Directions.Left, ref Tile.FullAround);
        if (DemoTexture != null && Tile.Tiles.Length > 0)
            GUI.DrawTexture(new Rect(310, StartY + 50, 20, 20), PackTiles[(int)Tile.Tiles[0].x, (int)Tile.Tiles[0].y]);
        DrawToggle(new Rect(330, StartY + 50, 20, 20), Voxel2DHelper.Directions.Right, ref Tile.FullAround);

        DrawToggle(new Rect(290, StartY + 70, 20, 20), Voxel2DHelper.Directions.BottomLeft, ref Tile.FullAround);
        DrawToggle(new Rect(310, StartY + 70, 20, 20), Voxel2DHelper.Directions.Bottom, ref Tile.FullAround);
        DrawToggle(new Rect(330, StartY + 70, 20, 20), Voxel2DHelper.Directions.BottomRight, ref Tile.FullAround);


        StartY -= 10;
        GUI.Label(new Rect(360, StartY + 20, 100, 20), "Affinity");
        StartY += 10;

        DrawToggle(new Rect(360, StartY + 30, 20, 20), Voxel2DHelper.Directions.TopLeft, ref Tile.AffinityFullAround);
        DrawToggle(new Rect(380, StartY + 30, 20, 20), Voxel2DHelper.Directions.Top, ref Tile.AffinityFullAround);
        DrawToggle(new Rect(400, StartY + 30, 20, 20), Voxel2DHelper.Directions.TopRight, ref Tile.AffinityFullAround);

        DrawToggle(new Rect(360, StartY + 50, 20, 20), Voxel2DHelper.Directions.Left, ref Tile.AffinityFullAround);
        if (DemoTexture != null && Tile.Tiles.Length > 0)
            GUI.DrawTexture(new Rect(380, StartY + 50, 20, 20), PackTiles[(int)Tile.Tiles[0].x, (int)Tile.Tiles[0].y]);
        DrawToggle(new Rect(400, StartY + 50, 20, 20), Voxel2DHelper.Directions.Right, ref Tile.AffinityFullAround);

        DrawToggle(new Rect(360, StartY + 70, 20, 20), Voxel2DHelper.Directions.BottomLeft, ref Tile.AffinityFullAround);
        DrawToggle(new Rect(380, StartY + 70, 20, 20), Voxel2DHelper.Directions.Bottom, ref Tile.AffinityFullAround);
        DrawToggle(new Rect(400, StartY + 70, 20, 20), Voxel2DHelper.Directions.BottomRight, ref Tile.AffinityFullAround);


        StartY += 70;
        GUI.Label(new Rect(360, StartY + 20, 100, 20), "Equal");
        StartY += 10;

        DrawToggle(new Rect(360, StartY + 30, 20, 20), Voxel2DHelper.Directions.TopLeft, ref Tile.EqualFullAround);
        DrawToggle(new Rect(380, StartY + 30, 20, 20), Voxel2DHelper.Directions.Top, ref Tile.EqualFullAround);
        DrawToggle(new Rect(400, StartY + 30, 20, 20), Voxel2DHelper.Directions.TopRight, ref Tile.EqualFullAround);

        DrawToggle(new Rect(360, StartY + 50, 20, 20), Voxel2DHelper.Directions.Left, ref Tile.EqualFullAround);
        if (DemoTexture != null && Tile.Tiles.Length > 0)
            GUI.DrawTexture(new Rect(380, StartY + 50, 20, 20), PackTiles[(int)Tile.Tiles[0].x, (int)Tile.Tiles[0].y]);
        DrawToggle(new Rect(400, StartY + 50, 20, 20), Voxel2DHelper.Directions.Right, ref Tile.EqualFullAround);

        DrawToggle(new Rect(360, StartY + 70, 20, 20), Voxel2DHelper.Directions.BottomLeft, ref Tile.EqualFullAround);
        DrawToggle(new Rect(380, StartY + 70, 20, 20), Voxel2DHelper.Directions.Bottom, ref Tile.EqualFullAround);
        DrawToggle(new Rect(400, StartY + 70, 20, 20), Voxel2DHelper.Directions.BottomRight, ref Tile.EqualFullAround);

        StartY += 80;

        for (int i = 0; i < Tile.Tiles.Length; ++i)
        {
            if (DemoTexture != null)
                GUI.DrawTexture(new Rect(220, StartY + i * TileSize.y, TileSize.x, TileSize.y), PackTiles[(int)Tile.Tiles[i].x, (int)Tile.Tiles[i].y]);

            GUI.Label(new Rect(220 + TileSize.x, StartY + i * TileSize.y, 100, TileSize.y), Tile.Tiles[i].ToString());
        }
    }

    public bool HasValue(Voxel2DHelper.Directions[] Values, Voxel2DHelper.Directions Dir)
    {
        foreach (Voxel2DHelper.Directions d in Values)
            if (d == Dir)
                return true;

        return false;
    }

    public void DrawToggle(Rect Pos, Voxel2DHelper.Directions Dir, ref Voxel2DHelper.Directions[] Values)
    {
        bool Value = HasValue(Values, Dir);
        bool NewResult = GUI.Toggle(Pos, Value, new GUIContent("", Dir.ToString()));

        if (NewResult != Value)
        {
            if (NewResult) // Add New Direction
            {
                List<Voxel2DHelper.Directions> Dirs = new List<Voxel2DHelper.Directions>();
                foreach (Voxel2DHelper.Directions d in Values)
                    if (!Dirs.Contains(d))
                        Dirs.Add(d);

                if (!Dirs.Contains(Dir))
                    Dirs.Add(Dir);

                Values = Dirs.ToArray();
            }
            else // Remove Direction
            {
                List<Voxel2DHelper.Directions> Dirs = new List<Voxel2DHelper.Directions>();
                foreach (Voxel2DHelper.Directions d in Values)
                    if (!Dirs.Contains(d))
                        Dirs.Add(d);

                Dirs.Remove(Dir);
                Values = Dirs.ToArray();
            }
        }
    }


    public Vector2 MousePosition;
    public void DrawRightMenu()
    {
        if (DemoTexture == null)
            return;

        Voxel2DTile Tile = Decoder.Tiles[SelectedTileID];

        MousePosition = Event.current.mousePosition;
        GUI.DrawTexture(new Rect(420, 20, DemoTexture.width, DemoTexture.height), DemoTexture);

        if (Event.current.type == EventType.MouseDown)
        {
            Vector2 Selection = new Vector2();

            int SizeX = (int)MousePosition.x - 420;
            int SizeY = (int)MousePosition.y - 20;

            if (SizeX <= 0 || SizeY <= 0 || SizeX >= DemoTexture.width || SizeY >= DemoTexture.height)
                return;


            while (SizeX >= TileSize.x + TileSeparator)
            {
                ++Selection.x;
                SizeX -= (int)TileSize.x + TileSeparator;
            }

            while (SizeY >= TileSize.y + TileSeparator)
            {
                ++Selection.y;
                SizeY -= (int)TileSize.y + TileSeparator;
            }

            List<Vector2> Tiles = new List<Vector2>();
            Tiles.AddRange(Tile.Tiles);

            if (Tiles.Contains(Selection))
                Tiles.Remove(Selection);
            else
                Tiles.Add(Selection);

            Tile.Tiles = Tiles.ToArray();
        }

        foreach (Voxel2DTile T in Decoder.Tiles)
        {
            foreach (Vector2 p in T.Tiles)
            {
                if (T != Tile)
                {
                    GUI.color = Color.black;
                    GUI.Label(GetTile((int)p.x, (int)p.y), "X");
                }
                else
                {
                    GUI.color = Color.red;
                    GUI.Label(GetTile((int)p.x, (int)p.y), "O");
                }
                GUI.color = Color.white;
            }
        }
    }

    public Rect GetTile(int x, int y)
    {
        int OffsetX = x * TileSeparator;
        int OffsetY = (y + 1) * TileSeparator;

        OffsetX += x * (int)TileSize.x;
        OffsetY += y * (int)TileSize.y;

        return new Rect(OffsetX + 420, OffsetY + 20, TileSize.x, TileSize.y);
    }
}
