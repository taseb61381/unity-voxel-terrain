﻿using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class Voxel2DMenu
{
    #region Voxels

    [MenuItem("TerrainEngine/Terrains/2D/New Terrain")]
    static void Create2DTerrain()
    {
        TerrainMenuEditor.CheckObjects();

        TerrainManager ExitingManager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (ExitingManager != null && !(ExitingManager is Voxel2DManager))
        {
            Debug.LogError("You can not create a new terrain. A Terrain Already exist : " + ExitingManager);
            return;
        }

        Voxel2DPalette Palette = GameObject.FindObjectOfType(typeof(Voxel2DPalette)) as Voxel2DPalette;
        if (Palette != null)
        {
            Debug.LogWarning("Using existing Palette :" + Palette);
        }
        else
        {
            GameObject Obj = new GameObject();
            Palette = Obj.AddComponent<Voxel2DPalette>();
        }

        Voxel2DManager Manager = GameObject.FindObjectOfType(typeof(Voxel2DManager)) as Voxel2DManager;
        if (Manager == null)
        {
            GameObject Obj = new GameObject();
            Manager = Obj.AddComponent<Voxel2DManager>();
        }

        Manager.Informations = new TerrainInformation();
        Manager.Informations.DataType = TerrainDataType.TYPE_DATA;
        Palette.name = "3-Palette";
        Manager.name = "4-TerrainManager";
        Manager.Palette = Palette;

        if (Palette.VoxelBlocks == null || Palette.VoxelBlocks.Length <= 0)
        {
            Palette.VoxelBlocks = new Voxel2DInformation[1];
            Palette.VoxelBlocks[0] = new Voxel2DInformation();
            Palette.VoxelBlocks[0].Name = "None";
        }

        if (Manager.ChunkPrefab == null)
        {
            Manager.ChunkPrefab = new GameObject();
            Manager.ChunkPrefab.name = "-ChunkPrefab";
            Manager.ChunkPrefab.transform.parent = Manager.transform;
            Manager.ChunkPrefab.layer = LayerMask.NameToLayer("Chunk");

            Voxel2DChunkScript Script = Manager.ChunkPrefab.AddComponent<Voxel2DChunkScript>();
            Script.CFilter = Manager.ChunkPrefab.AddComponent<MeshFilter>();
            Script.CRenderer = Manager.ChunkPrefab.AddComponent<MeshRenderer>();
            Script.CCollider = Manager.ChunkPrefab.AddComponent<MeshCollider>();
        }
    }

    #endregion
}
