﻿
using TerrainEngine;
using UnityEngine;

public class HexagonalManager : VoxelManager
{
    public override TerrainBlock CreateTerrain()
    {
        return new HexagonalBlock(this);
    }

    public override bool InitOffsets()
    {
        Informations.ChunkSizeOffset = new Vector3(Informations.VoxelPerChunk * HexagonalBlockHelper.h, 0, (Informations.VoxelPerChunk * 2 * (HexagonalBlockHelper.r - HexagonalBlockHelper.h)));
        return true;
    }
}
