using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class HexagonalMenu
{
    #region Voxels

    [MenuItem("TerrainEngine/Terrains/Hexagonal/New Terrain")]
    static void CreateVoxelTerrain()
    {
        TerrainMenuEditor.CheckObjects();

        TerrainManager ExitingManager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (ExitingManager != null && !(ExitingManager is HexagonalManager))
        {
            Debug.LogError("You can not create a new terrain. A Terrain Already exist : " + ExitingManager);
            return;
        }

        VoxelPalette Palette = GameObject.FindObjectOfType(typeof(VoxelPalette)) as VoxelPalette;
        if (Palette != null)
        {
            Debug.LogWarning("Using existing Palette :" + Palette);
        }
        else
        {
            GameObject Obj = new GameObject();
            Palette = Obj.AddComponent<VoxelPalette>();
        }

        HexagonalManager Manager = GameObject.FindObjectOfType(typeof(HexagonalManager)) as HexagonalManager;
        if (Manager == null)
        {
            GameObject Obj = new GameObject();
            Manager = Obj.AddComponent<HexagonalManager>();
        }

        Manager.Informations = new TerrainInformation();
        Manager.Informations.DataType = TerrainDataType.TYPE_DATA;
        Palette.name = "3-Palette";
        Manager.name = "4-TerrainManager";
        Manager.Palette = Palette;

        if (Palette.VoxelBlocks == null || Palette.VoxelBlocks.Length <= 0)
        {
            Palette.VoxelBlocks = new BlockInformation[1];
            Palette.VoxelBlocks[0] = new BlockInformation();
            Palette.VoxelBlocks[0].Name = "None";
        }


        if (Manager.ChunkPrefab == null)
        {
            Manager.ChunkPrefab = new GameObject();
            Manager.ChunkPrefab.name = "-ChunkPrefab";
            Manager.ChunkPrefab.transform.parent = Manager.transform;
            Manager.ChunkPrefab.layer = LayerMask.NameToLayer("Chunk");

            HexagonalChunkScript Script = Manager.ChunkPrefab.AddComponent<HexagonalChunkScript>();
            Script.CFilter = Manager.ChunkPrefab.AddComponent<MeshFilter>();
            Script.CRenderer = Manager.ChunkPrefab.AddComponent<MeshRenderer>();
            Script.CCollider = Manager.ChunkPrefab.AddComponent<MeshCollider>();
        }
    }

    #endregion
}
