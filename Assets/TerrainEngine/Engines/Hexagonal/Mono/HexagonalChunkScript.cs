
using UnityEngine;


public class HexagonalChunkScript : TerrainChunkScript
{
    public override void Apply(IMeshData Data)
    {
        CRenderer.sharedMaterials = (TerrainManager.Instance as VoxelManager).WorldSharedMaterial;

        Mesh CMesh = GetMesh();
        CMesh.Clear(false);
        CMesh.vertices = Data.Vertices;
        CMesh.uv = Data.UVs;
        CMesh.colors = Data.Colors;
        CMesh.triangles = Data.Indices[0];

        CMesh.RecalculateNormals();
        base.Apply(Data);
    }
}
