using System;

using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class HexagonalBlock : TerrainBlock
    {
        public HexagonalBlock(TerrainManager Manager)
            : base(Manager.Informations)
        {

        }

        public override VectorI3 GetVoxelFromWorldPosition(Vector3 WorldPosition, bool Remove)
        {
            WorldPosition -= this.WorldPosition;
            WorldPosition /= Information.Scale;
            WorldPosition.x = WorldPosition.x / Information.BlockSizeX * (float)Information.VoxelsPerAxis;
            WorldPosition.z = WorldPosition.z / Information.BlockSizeZ * (float)Information.VoxelsPerAxis;
            return WorldPosition;
        }

        public override void Generate(ActionThread Thread,ref TempMeshData Info,ref IMeshData Data, int ChunkId)
        {
            Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
            Data = PoolManager.UPool.GetSimpleMeshData();

            int px = 0;
            int py = 0;
            int pz = 0;
            int StartX = 0, StartY = 0, StartZ = 0;
            GetChunkStart(ChunkId, ref StartX, ref StartY, ref StartZ);

            int[] CreatedVertex = new int[HexagonalBlockHelper.TopBottomVertex];
            int Indice = 0;
            VectorI3 Direction;
            int TopIndice = 0;
            int TotalGray = 0;
            bool Ambient = false;
            byte BlockType = 0;
            BlockInformation BlockInfo = null;
            int Face = 0;
            int VertexID = 0;
            int Ti = 0;
            int SideID = 0;
            int P = 0;
            int i = 0;
            int x, y, z;
            for (x = 0; x < Information.VoxelPerChunk; x++)
            {
                px = x + StartX;
                for (y = 0; y < Information.VoxelPerChunk; y++)
                {
                    py = y + StartY;
                    for (z = 0; z < Information.VoxelPerChunk; z++)
                    {
                        pz = z + StartZ;

                        BlockType = Voxels.GetType(px, py, pz);

                        if (BlockType == BlockManager.None)
                            continue;

                        P = (x % 2 == 0 ? 0 : 1);

                        TopIndice = -1;
                        TotalGray = -1;
                        Ambient = false;

                        for (SideID = 0; SideID < HexagonalBlockHelper.Facecount; ++SideID)
                        {
                            Direction = HexagonalBlockHelper.AroundOffsets[P][SideID];

                            if (TopIndice != -1 && SideID > 1)
                            {
                                if (GetByte(px + Direction.x, py + Direction.y + 1, pz + Direction.z) != 0)
                                {
                                    Ti = SideID - 2;

                                    Info.Colors.array[TopIndice + Ti] *= Color.gray;

                                    if (Ti + 1 < 6)
                                        Info.Colors.array[TopIndice + Ti + 1] *= Color.gray;
                                    else
                                        Info.Colors.array[TopIndice] *= Color.gray;

                                    ++TotalGray;
                                }
                            }

                            if (TerrainManager.Instance.OptimizeBlockBorders && HasChunkState(ChunkId, ChunkStates.Border))
                            {
                                if (!IsValidDirection(this, ChunkId, px + Direction.x, py + Direction.y, pz + Direction.z, Direction))
                                    continue;
                            }

                            if (GetByte(px + Direction.x, py + Direction.y, pz + Direction.z) == 0)
                            {
                                if (SideID == 0)
                                    TopIndice = Indice;

                                if (SideID > 1)
                                    Ambient = GetByte(px + Direction.x, (py - 1) + Direction.y, pz + Direction.z) != 0;

                                BlockInfo = TerrainManager.Instance.Palette.GetVoxelData<BlockInformation>(BlockType);
                                Face = (int)HexagonalBlockHelper.facesEnum[SideID];

                                for (i = 0; i < HexagonalBlockHelper.VertexSide[SideID].Length; ++i)
                                {
                                    VertexID = HexagonalBlockHelper.VertexSide[SideID][i];
                                    Color col = Color.white;

                                    foreach (IColorGenerator ColorGen in BlockInfo.ColorGenerators)
                                        if (ColorGen.IsAllowedFace(HexagonalBlockHelper.facesEnum[SideID]))
                                            col = ColorGen.GenerateColor(GetWorldVoxelX(px), GetWorldVoxelY(py), GetWorldVoxelZ(pz), SideID, col);

                                    if (Ambient && i > 1)
                                        col *= Color.gray;

                                    CreatedVertex[VertexID] = Indice;
                                    Info.Vertices.Add(HexagonalBlockHelper.GetVertex(HexagonalBlockHelper.VertexSide[SideID][i], x, y, z, Information.Scale));
                                    Info.Uvs.Add(TerrainManager.Instance.Palette.GetUv((int)BlockType, Face, i));
                                    Info.Colors.Add(col);
                                    Indice++;
                                }

                                for (i = 0; i < HexagonalBlockHelper.VertexOrder[SideID].Length; ++i)
                                    Info.Triangles.Indices.Add(CreatedVertex[HexagonalBlockHelper.VertexOrder[SideID][i]]);
                            }
                        }

                        if (TotalGray >= 2)
                        {
                            Info.Colors.array[TopIndice + 6] *= Color.gray;
                        }
                    }
                }
            }

            Data.Indices = new int[1][];
            Data.Indices[0] = Info.Triangles.Indices.ToArray();
            Data.Colors = Info.Colors.ToArray();
            Data.UVs = Info.Uvs.ToArray();
            Data.Vertices = Info.Vertices.ToArray();
            Data.Materials = (TerrainManager.Instance as VoxelManager).WorldSharedMaterial;
        }

        public bool IsValidDirection(TerrainBlock Block, int ChunkId, int px, int py, int pz, VectorI3 Direction)
        {
            TerrainChunkRef Ref = new TerrainChunkRef();
            if (px >= Information.VoxelsPerAxis || px < 0
                || pz >= Information.VoxelsPerAxis || pz < 0
                || py >= Information.VoxelsPerAxis || py < 0)
            {
                if (!Block.GetAroundChunk(ChunkId, 1 + Direction.x, 1 + Direction.y, 1 + Direction.z, ref Ref))
                    return false;
            }

            return true;
        }
    }
}
