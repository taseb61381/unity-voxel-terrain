﻿using System;
using TerrainEngine;
using UnityEngine;

public class MarchingManager : TerrainManager
{
    public bool OptimizeEncloseVoxels = true;
    public bool OptimizeMesh = true;
    public bool GenerateUVs = false;
    public bool GenerateTangents = false;
    public bool SmoothVoxelsTransition = true;
    public bool GenerateColors = false;
    public bool PolyColor = false;

    [HideInInspector,NonSerialized]
    public bool GenerateAmbientOcclusion = false;

    public override TerrainBlock CreateTerrain()
    {
        return new MarchingBlock(this);
    }
}
