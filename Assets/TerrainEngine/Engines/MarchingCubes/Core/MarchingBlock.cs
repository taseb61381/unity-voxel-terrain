﻿using System;

using UnityEngine;

namespace TerrainEngine
{
    public class MarchingBlock : TerrainBlock
    {
        public MarchingBlock(TerrainManager Manager)
            : base(Manager.Informations)
        {

        }

        public MarchingBlock(TerrainInformation Information)
            : base(Information)
        {

        }

        public override VectorI3 GetVoxelFromWorldPosition(Vector3 WorldPosition, bool Remove)
        {
            Vector3 blockPos = (WorldPosition - this.WorldPosition) / Information.Scale;
            return new VectorI3(blockPos.x, blockPos.y, blockPos.z);
        }

        #region Generation

        public byte GetMarchingInterpolation(int x, int y, int z, ref MarchingInterpolationInfo Info, bool Safe)
        {
            if (Safe)
            {
                Voxels.GetTypeAndVolume(x, y, z, ref Info.VType, ref Info.Volume);
                return Info.VType;
            }

            TerrainBlock AroundBlock = GetAroundBlock(ref x, ref y, ref z) as TerrainBlock;
            if (AroundBlock == null || AroundBlock.HasState(TerrainBlockStates.CLOSED))
            {
                Info.Border = true;
                return 0;
            }

            AroundBlock.Voxels.GetTypeAndVolume(x, y, z, ref Info.VType, ref Info.Volume);
            return Info.VType;
        }

        public virtual MarchingVoxelArray GetInterpolations(ActionThread Thread, int StartX, int StartY, int StartZ, 
            int SizeX, int SizeY, int SizeZ, int PaletteCount, ref bool IsValidChunk)
        {
            bool sx, sy, vx = false, vy = false, IsInChunk=false;
            byte ValType, LastType = 0;
            int px, py, pz, Count = 0, Current=0, i =0;
            MarchingInformation LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(0);

            MarchingVoxelArray Interpolations = Thread.Pool.UnityPool.MarchingPool as MarchingVoxelArray;
            if (Interpolations == null)
            {
                Interpolations = new MarchingVoxelArray();
                Thread.Pool.UnityPool.MarchingPool = Interpolations;
            }

            SizeX += 3;
            SizeY += 3;
            SizeZ += 3;
            Interpolations.Init(SizeX, SizeY, SizeZ, 3, PaletteCount);
            MarchingInterpolationInfo Info = null;

            // Border size. Used for check around if voxel is enclose or generate marching cubes (+1)

            int x, y, z;
            for (x = 0; x < SizeX; ++x)
            {
                px = StartX + x - 1;
                sx = px > -1 && px < Voxels.VoxelsPerAxis; // Voxels.IsValidX();
                vx = x != 0 && x < SizeX-1;

                for (y = 0; y < SizeY; ++y)
                {
                    py = StartY + y - 1;
                    sy = py > -1 && py < Voxels.VoxelsPerAxis; // Voxels.IsValidY();
                    vy = y != 0 && y < SizeY-1;

                    for (z = 0; z < SizeZ; ++z)
                    {
                        pz = StartZ + z - 1;

                        Info = Interpolations.Temp[i];
                        Info.ClearIndices();
                        ++i;
                        Info.State = MarchingInterpolationInfo.InterpolationState.NONE;
                        IsInChunk = vx && vy && z != 0 && z < SizeZ-1;

                        if (!HasState(TerrainBlockStates.CLOSED) && (ValType = GetMarchingInterpolation(px, py, pz, ref Info, (sx && sy && (pz > -1 && pz < Voxels.VoxelsPerAxis)))) != BlockManager.None)
                        {
                            if (ValType != LastType)
                            {
                                LastType = ValType;
                                LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(ValType);
                            }

                            Info.Info = LastVoxelType;
                            if (LastVoxelType.UseSplatMap && LastVoxelType.MainVoxelId != 0)
                                Info.VType = LastVoxelType.MainVoxelId;

                            if ((Info.VoxelType = LastVoxelType.VoxelType) == VoxelTypes.FLUID)
                            {
                                Info.Volume = 0;
                                Info.VType = 0;
                                Info.Full = false;
                                Info.Info = null;
                            }
                            else
                            {
                                if (IsInChunk && !IsValidChunk)
                                {
                                    if (Info.Volume >= TerrainInformation.Isolevel)
                                    {
                                        Info.Full = true;
                                        IsValidChunk = Info.VoxelType == VoxelTypes.TRANSLUCENT;
                                        Count += 1;
                                    }
                                    else
                                        Info.Full = false;
                                }
                                else
                                    Info.Full = Info.Volume >= TerrainInformation.Isolevel;

                                if (IsInChunk)
                                {
                                    Interpolations.ActiveMaterial(ValType);
                                }
                            }
                        }
                        else
                        {
                            Info.VoxelType = VoxelTypes.OPAQUE;
                            Info.VType = 0;
                            Info.Volume = 0;
                        }

                        if (IsInChunk && !IsValidChunk)
                        {
                            ++Current;

                            if (Count != 0 && Count != Current)
                                IsValidChunk = true;
                        }
                    }
                }
            }

            return Interpolations;
        }

        public override void Generate(ActionThread Thread,ref TempMeshData Info,ref IMeshData Data, int ChunkId)
        {
            TerrainChunk Chunk = Chunks[ChunkId];

            int SizeX = Information.VoxelPerChunk;
            int SizeY = Information.VoxelPerChunk;
            int SizeZ = Information.VoxelPerChunk;

            bool IsValidChunk = false;

            MarchingVoxelArray Interpolations = GetInterpolations(Thread, Chunk.StartX, Chunk.StartY, Chunk.StartZ, SizeX, SizeY, SizeZ, TerrainManager.Instance.Palette.DataCount, ref IsValidChunk);

            if (!IsValidChunk)
            {
                Interpolations.Close();
                return;
            }

            bool OptimizeEnclose = (TerrainManager.Instance as MarchingManager).OptimizeEncloseVoxels;
            bool OptimizeMesh = (TerrainManager.Instance as MarchingManager).OptimizeMesh;
            bool CheckBorders = TerrainManager.Instance.OptimizeBlockBorders;
            bool SmoothTransition = (TerrainManager.Instance as MarchingManager).SmoothVoxelsTransition;
            bool GenerateColors = (TerrainManager.Instance as MarchingManager).GenerateColors;
            bool GenerateUV = (TerrainManager.Instance as MarchingManager).GenerateUVs;
            Generate(Thread, ref Info, ref Data, GetChunkWorldPosition(ChunkId), Interpolations, HasChunkState(ChunkId, ChunkStates.Border)
                ,OptimizeEnclose,OptimizeMesh,CheckBorders,SmoothTransition,GenerateColors,GenerateUV);
        }

        public override void Generate(ActionThread Thread, ref TempMeshData Info, ref IMeshData Data, Vector3 WorldPosition, IVoxelArray Array, bool BorderChunk
            , bool OptimizeEnclose, bool OptimizeMesh, bool CheckBorders, bool SmoothTransition, bool GenerateColors, bool GenerateUV)
        {
            Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
            Data = PoolManager.UPool.GetSimpleMeshData();

            // Assigning local variable, speed optimization*
            int i, x, y, z, id;
            float Scale = Information.Scale;
            MarchingInterpolationInfo val0 = null;

            int SizeX = Array.SizeX - Array.BorderSize;
            int SizeY = Array.SizeY - Array.BorderSize;
            int SizeZ = Array.SizeZ - Array.BorderSize;

            MarchingVoxelArray Interpolations = Array as MarchingVoxelArray;

            if (OptimizeEnclose)
            {
                for (x = 1; x <= SizeX + 1; ++x)
                {
                    for (y = 1; y <= SizeY + 1; ++y)
                    {
                        i = Interpolations.GetId(x, y, 1);
                        for (z = 1; z <= SizeZ + 1; ++z, ++i)
                        {
                            if (!(val0 = Interpolations.Temp[i]).Full || val0.VoxelType != VoxelTypes.OPAQUE)
                                continue;

                            val0.Enclose = IsFullyEnclose(val0);
                        }
                    }
                }
            }


            Interpolations.MaterialCount = 0;

            MarchingInformation SplatInfo;
            for (i = 0; i < Interpolations.PaletteCount; ++i)
            {
                if (Interpolations.ActiveSubMesh[i] == false)
                    continue;

                SplatInfo = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>((byte)i);
                if (SplatInfo.UseSplatMap)
                {
                    GenerateColors = true;

                    if (SplatInfo.MainVoxelId != 0)
                    {
                        Interpolations.ActiveSubMesh[SplatInfo.MainVoxelId] = true;
                        Interpolations.ActiveSubMesh[i] = false;
                    }
                }
            }

            for (i = 0; i < Interpolations.PaletteCount; ++i)
            {
                if (Interpolations.ActiveSubMesh[i] == false)
                    continue;

                SplatInfo = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>((byte)i);

                if (SplatInfo.UseSplatMap)
                {
                    if (SplatInfo.MainVoxelId != 0)
                        continue;

                    if (OptimizeEnclose)
                    {
                        for (x = 0; x < SizeX; ++x)
                        {
                            for (y = 0; y < SizeY; ++y)
                            {
                                id = Interpolations.GetId(x + 1, y + 1, 1);
                                for (z = 0; z < SizeZ; ++z, ++id)
                                {
                                    if (((val0 = Interpolations.Temp[id]).Enclose || val0.Border) && (val0.ForwardTopRight.Enclose || val0.ForwardTopRight.Border))
                                        continue;

                                    GetTriangles(this, WorldPosition, BorderChunk, x, y, z, TerrainInformation.Isofix, i, CheckBorders, SmoothTransition, Interpolations, ref Info, val0, Scale, OptimizeMesh, GenerateUV, true);
                                }
                            }
                        }
                    }
                    else
                    {
                        for (x = 0; x < SizeX; ++x)
                            for (y = 0; y < SizeY; ++y)
                                for (z = 0; z < SizeZ; ++z)
                                    GetTriangles(this, WorldPosition, BorderChunk, x, y, z, TerrainInformation.Isofix, i, CheckBorders, SmoothTransition, Interpolations, ref Info, Interpolations[x + 1, y + 1, z + 1], Scale, OptimizeMesh, GenerateUV, true);
                    }
                }
                else
                {
                    if (OptimizeEnclose)
                    {
                        for (x = 0; x < SizeX; ++x)
                        {
                            for (y = 0; y < SizeY; ++y)
                            {
                                id = Interpolations.GetId(x + 1, y + 1, 1);
                                for (z = 0; z < SizeZ; ++z, ++id)
                                {
                                    if (((val0 = Interpolations.Temp[id]).Enclose || val0.Border) && (val0.ForwardTopRight.Enclose || val0.ForwardTopRight.Border))
                                        continue;

                                    GetTriangles(this, WorldPosition, BorderChunk, x, y, z, TerrainInformation.Isofix, i, CheckBorders, SmoothTransition, Interpolations, ref Info, val0, Scale, OptimizeMesh, GenerateUV, GenerateColors);
                                }
                            }
                        }
                    }
                    else
                    {
                        for (x = 0; x < SizeX; ++x)
                            for (y = 0; y < SizeY; ++y)
                                for (z = 0; z < SizeZ; ++z)
                                    GetTriangles(this, WorldPosition, BorderChunk, x, y, z, TerrainInformation.Isofix, i, CheckBorders, SmoothTransition, Interpolations, ref Info, Interpolations[x + 1, y + 1, z + 1], Scale, OptimizeMesh, GenerateUV, GenerateColors);
                    }
                }

                if (Info.Triangles.Indices.Count > 0)
                {
                    Info.Vertices.AddRange(Info.Triangles.Vertices);
                    Info.Normals.AddRange(Info.Triangles.Normals);
                    Info.TriangleIndex = (ushort)Info.Vertices.Count;
                    Info.TempMaterials.Add(SplatInfo.MaterialInfo);
                    Info.TempIndices.Add(Info.Triangles.Indices.ToArray());
                    Info.Triangles.Clear();
                    ++Interpolations.MaterialCount;
                }
            }

            if (Interpolations.MaterialCount != 0)
            {
                Data.Vertices = Info.Vertices.ToArray();
                Data.UVs = Info.Uvs.ToArray();
                Data.Colors = Info.Colors.ToArray();
                Data.Indices = PoolManager.UPool.GetIndicesArray(Interpolations.MaterialCount);
                Data.Materials = PoolManager.UPool.GetMaterials(Info.TempMaterials);

                for (i = 0; i < Data.Materials.Length; ++i)
                {
                    Data.Indices[i] = Info.TempIndices.array[i];
                }

                if (!Data.IsValid())
                {
                    PoolManager.UPool.CloseIndicesArray(Data.Indices);
                    Data.Indices = null;
                }
                else
                {
                    if ((TerrainManager.Instance as MarchingManager).GenerateAmbientOcclusion)
                        GenerateAmbientOcculsion(Interpolations, Data);
                }
            }


            Interpolations.Close();
        }

        public void GenerateMesh(Vector3 WorldPosition,bool BorderChunk, MarchingVoxelArray Interpolations, TempMeshData Info, IMeshData Data, 
            bool OptimizeEnclose, bool OptimizeMesh, bool CheckBorders, int TypeId, bool GenerateUV, bool GenerateColor, int Offset = 0)
        {
            float Scale = Information.Scale;
            MarchingInterpolationInfo val0 = null;

            int SizeX = Information.VoxelPerChunk;
            int SizeY = Information.VoxelPerChunk;
            int SizeZ = Information.VoxelPerChunk;
            int x, y, z;

            if (OptimizeEnclose)
            {
                for (x = 0; x < SizeX + Offset; ++x)
                    for (y = 0; y < SizeY + Offset; ++y)
                        for (z = 0; z < SizeZ + Offset; ++z)
                        {
                            val0 = Interpolations[x + 1,y + 1,z + 1];
                            if ((val0.Enclose || val0.Border) && (val0.ForwardTopRight.Enclose || val0.ForwardTopRight.Border))
                                continue;

                            GetTriangles(this,WorldPosition, BorderChunk,
                                x, y, z, TerrainInformation.Isofix, TypeId, CheckBorders, false, Interpolations, ref Info, val0, Scale, OptimizeMesh, GenerateUV, GenerateColor);
                        }
            }
            else
            {
                for (x = 0; x < SizeX + Offset; ++x)
                {
                    for (y = 0; y < SizeY + Offset; ++y)
                    {
                        for (z = 0; z < SizeZ + Offset; ++z)
                        {
                            GetTriangles(this,WorldPosition, BorderChunk, x, y, z, TerrainInformation.Isofix, TypeId, CheckBorders, false, Interpolations, ref Info, Interpolations[x + 1, y + 1, z + 1], Scale, OptimizeMesh, GenerateUV, GenerateColor);
                        }
                    }
                }
            }

            Info.Vertices.AddRange(Info.Triangles.Vertices);
            Info.Normals.AddRange(Info.Triangles.Normals);
            Info.TriangleIndex = Info.Vertices.Count;
            Info.CurrentIndices[Interpolations.MaterialCount] = Info.Triangles.Indices.ToArray();
            Info.Triangles.Clear();
            ++Interpolations.MaterialCount;
        }

        static public bool IsFullyEnclose(MarchingInterpolationInfo Info)
        {
            if (!Info.Top.Full
                || !Info.Bottom.Full
                || !Info.Left.Full
                || !Info.Right.Full
                || !Info.Forward.Full
                || !Info.Backward.Full
                || Info.Top.VoxelType != Info.VoxelType
                || Info.Bottom.VoxelType != Info.VoxelType
                || Info.Left.VoxelType != Info.VoxelType
                || Info.Right.VoxelType != Info.VoxelType
                || Info.Forward.VoxelType != Info.VoxelType
                || Info.Backward.VoxelType != Info.VoxelType)
                return false;

            return true;
        }

        static public bool GetTriangles(MarchingBlock Block,Vector3 WorldPosition, bool BorderChunk, int x, int y, int z, float isofix, int vtype,
            bool CheckBorders, bool SmoothVoxelsTransition, MarchingVoxelArray Interpolations, ref TempMeshData Info, MarchingInterpolationInfo val0, float Scale, bool OptimizeMesh, bool GenerateUV, bool GenerateColor)
        {
            #region Isolevel Calculation

            MarchingInterpolationInfo Right = val0.Right;
            MarchingInterpolationInfo TopRight = val0.TopRight;
            MarchingInterpolationInfo Top = val0.Top;
            MarchingInterpolationInfo Forward = val0.Forward;
            MarchingInterpolationInfo ForwardRight = val0.ForwardRight;
            MarchingInterpolationInfo ForwardTopRight = val0.ForwardTopRight;
            MarchingInterpolationInfo ForwardTop = val0.ForwardTop;

            if (CheckBorders && BorderChunk)
            {
                if (Right.Border || Top.Border || Forward.Border)
                {
                    return false;
                }
            }

            float v0 = val0.Volume;
            float v1 = Right.Volume;
            float v2 = TopRight.Volume;
            float v3 = Top.Volume;
            float v4 = Forward.Volume;
            float v5 = ForwardRight.Volume;
            float v6 = ForwardTopRight.Volume;
            float v7 = ForwardTop.Volume;

            int cubeindex = 0;
            MarchingInformation MarchingInfo = val0.Info;

            if (!SmoothVoxelsTransition)
            {
               if (val0.VType != vtype)
                {
                    if (v0 > isofix)
                        v0 = isofix;
                }
                else if (val0.Full)
                {
                    cubeindex |= 1;
                }

                if (Right.VType != vtype)
                {
                    if (v1 > isofix)
                        v1 = isofix;
                }
                else if (Right.Full)
                {
                    cubeindex |= 2;
                }

                if (TopRight.VType != vtype)
                {
                    if (v2 > isofix)
                        v2 = isofix;
                }
                else if (TopRight.Full)
                {
                    cubeindex |= 4;
                }

                if (Top.VType != vtype)
                {
                    if (v3 > isofix)
                        v3 = isofix;
                }
                else if (Top.Full)
                {
                    cubeindex |= 8;
                }

                if (Forward.VType != vtype)
                {
                    if (v4 > isofix)
                        v4 = isofix;
                }
                else if (Forward.Full)
                {
                    cubeindex |= 16;
                }

                if (ForwardRight.VType != vtype)
                {
                    if (v5 > isofix)
                        v5 = isofix;
                }
                else if (ForwardRight.Full)
                {
                    cubeindex |= 32;
                }

                if (ForwardTopRight.VType != vtype)
                {
                    if (v6 > isofix)
                        v6 = isofix;
                }
                else if (ForwardTopRight.Full)
                {
                    cubeindex |= 64;
                }

                if (ForwardTop.VType != vtype)
                {
                    if (v7 > isofix)
                        v7 = isofix;
                }
                else if (ForwardTop.Full)
                {
                    cubeindex |= 128;
                }
            }
            else
            {
                bool HasType = false;
                HasType = (val0.VType == vtype && val0.Full)
                    || (Right.VType == vtype && Right.Full)
                    || (TopRight.VType == vtype && TopRight.Full)
                    || (Top.VType == vtype && Top.Full)
                    || (Forward.VType == vtype && Forward.Full)
                    || (ForwardRight.VType == vtype && ForwardRight.Full)
                    || (ForwardTopRight.VType == vtype && ForwardTopRight.Full)
                    || (ForwardTop.VType == vtype && ForwardTop.Full);

                if (HasType)
                {
                    if (val0.Full && val0.VType != 0)
                    {
                        cubeindex |= 1;
                    }

                    if (Right.Full && Right.VType != 0)
                    {
                        cubeindex |= 2;
                        if (MarchingInfo == null)
                            MarchingInfo = Right.Info;
                    }

                    if (TopRight.Full && TopRight.VType != 0)
                    {
                        cubeindex |= 4;
                    }

                    if (Top.Full && Top.VType != 0)
                    {
                        cubeindex |= 8;
                        if (MarchingInfo == null)
                            MarchingInfo = Top.Info;
                    }

                    if (Forward.Full && Forward.VType != 0)
                    {
                        cubeindex |= 16;
                        if (MarchingInfo == null)
                            MarchingInfo = Forward.Info;
                    }

                    if (ForwardRight.Full && ForwardRight.VType != 0)
                    {
                        cubeindex |= 32;
                    }

                    if (ForwardTopRight.Full && ForwardTopRight.VType != 0)
                    {
                        cubeindex |= 64;
                    }

                    if (ForwardTop.Full && ForwardTop.VType != 0)
                    {
                        cubeindex |= 128;
                    }
                }
            }

            if (_edgeTable[cubeindex] == 0)
                return false;

            if ((TerrainManager.Instance as MarchingManager).PolyColor || GenerateColor)
            {
                if (MarchingInfo == null)
                    MarchingInfo = val0.Bottom.Info;

                if (MarchingInfo == null)
                    MarchingInfo = ForwardTop.Info;

                if (MarchingInfo == null)
                    MarchingInfo = ForwardTopRight.Info;

                if (MarchingInfo == null)
                    MarchingInfo = ForwardRight.Info;

                if (GenerateColor)
                {
                    if (MarchingInfo != null && MarchingInfo.UseSplatMap)
                        val0.CurrentColor = MarchingInfo.SplatColor;
                }
            }

            #endregion

            #region Trigangulation

            if ((_edgeTable[cubeindex] & 1) > 0 && val0._tempIndices[0] == 0)
            {
                Interpolations._tmpVertices[0] = Vector3.Lerp(val0.p, Right.p, (TerrainInformation.Isolevel - v0) / (v1 - v0)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 2) > 0 && val0._tempIndices[1] == 0)
            {
                Interpolations._tmpVertices[1] = Vector3.Lerp(Right.p, TopRight.p, (TerrainInformation.Isolevel - v1) / (v2 - v1)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 4) > 0 && val0._tempIndices[2] == 0)
            {
                Interpolations._tmpVertices[2] = Vector3.Lerp(TopRight.p, Top.p, (TerrainInformation.Isolevel - v2) / (v3 - v2)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 8) > 0 && val0._tempIndices[3] == 0)
            {
                Interpolations._tmpVertices[3] = Vector3.Lerp(Top.p, val0.p, (TerrainInformation.Isolevel - v3) / (v0 - v3)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 16) > 0 && val0._tempIndices[4] == 0)
            {
                Interpolations._tmpVertices[4] = Vector3.Lerp(Forward.p, ForwardRight.p, (TerrainInformation.Isolevel - v4) / (v5 - v4)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 32) > 0 && val0._tempIndices[5] == 0)
            {
                Interpolations._tmpVertices[5] = Vector3.Lerp(ForwardRight.p, ForwardTopRight.p, (TerrainInformation.Isolevel - v5) / (v6 - v5)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 64) > 0 && val0._tempIndices[6] == 0)
            {
                Interpolations._tmpVertices[6] = Vector3.Lerp(ForwardTopRight.p, ForwardTop.p, (TerrainInformation.Isolevel - v6) / (v7 - v6)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 128) > 0 && val0._tempIndices[7] == 0)
            {
                Interpolations._tmpVertices[7] = Vector3.Lerp(ForwardTop.p, Forward.p, (TerrainInformation.Isolevel - v7) / (v4 - v7)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 256) > 0 && val0._tempIndices[8] == 0)
            {
                Interpolations._tmpVertices[8] = Vector3.Lerp(val0.p, Forward.p, (TerrainInformation.Isolevel - v0) / (v4 - v0)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 512) > 0 && val0._tempIndices[9] == 0)
            {
                Interpolations._tmpVertices[9] = Vector3.Lerp(Right.p, ForwardRight.p, (TerrainInformation.Isolevel - v1) / (v5 - v1)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 1024) > 0 && val0._tempIndices[10] == 0)
            {
                Interpolations._tmpVertices[10] = Vector3.Lerp(TopRight.p, ForwardTopRight.p, (TerrainInformation.Isolevel - v2) / (v6 - v2)) * Scale;
            }

            if ((_edgeTable[cubeindex] & 2048) > 0 && val0._tempIndices[11] == 0)
            {
                Interpolations._tmpVertices[11] = Vector3.Lerp(Top.p, ForwardTop.p, (TerrainInformation.Isolevel - v3) / (v7 - v3)) * Scale;
            }

            #endregion

            #region MeshGeneration

            int[] tri = _triTable[cubeindex];
            if (OptimizeMesh)
            {
                for (int i = 0; tri[i] != -1; i += 3)
                {
                    AddOptimizedVertex(Block, WorldPosition, val0, Interpolations, Info, tri[i], x + 1, y + 1, z + 1, (byte)vtype, GenerateUV, GenerateColor);
                    AddOptimizedVertex(Block, WorldPosition, val0, Interpolations, Info, tri[i + 2], x + 1, y + 1, z + 1, (byte)vtype, GenerateUV, GenerateColor);
                    AddOptimizedVertex(Block, WorldPosition, val0, Interpolations, Info, tri[i + 1], x + 1, y + 1, z + 1, (byte)vtype, GenerateUV, GenerateColor);
                }
            }
            else
            {
                if ((TerrainManager.Instance as MarchingManager).PolyColor)
                {
                    Color Col;
                    Vector3 A, B, C, Normal;
                    for (int i = 0; tri[i] != -1; i += 3)
                    {
                        A = AddSimpleVertex(Block, WorldPosition, val0, Interpolations, Info, tri[i], x + 1, y + 1, z + 1, (byte)vtype, GenerateUV);
                        B = AddSimpleVertex(Block, WorldPosition, val0, Interpolations, Info, tri[i + 2], x + 1, y + 1, z + 1, (byte)vtype, GenerateUV);
                        C = AddSimpleVertex(Block, WorldPosition, val0, Interpolations, Info, tri[i + 1], x + 1, y + 1, z + 1, (byte)vtype, GenerateUV);

                        if (MarchingInfo != null && MarchingInfo.ColorFunction != null)
                        {
                            Normal = Vector3.Cross(B - A, C - A).normalized;
                            A = (A+B+C)/3;
                            Col = MarchingInfo.ColorFunction(A.x + WorldPosition.x, A.y + WorldPosition.y, A.z + WorldPosition.z, Normal);
                            Info.Colors.CheckArray(3);
                            Info.Colors.AddSafe(Col);
                            Info.Colors.AddSafe(Col);
                            Info.Colors.AddSafe(Col);
                        }
                        else
                        {
                            Info.Colors.CheckArray(3);
                            Info.Colors.AddSafe(Color.white);
                            Info.Colors.AddSafe(Color.white);
                            Info.Colors.AddSafe(Color.white);
                        }
                    }
                }
                else
                {
                    for (int i = 0; tri[i] != -1; i += 3)
                    {
                        AddVertex(Block, WorldPosition, val0, Interpolations, Info, tri[i], x + 1, y + 1, z + 1, (byte)vtype, GenerateUV, GenerateColor);
                        AddVertex(Block, WorldPosition, val0, Interpolations, Info, tri[i + 2], x + 1, y + 1, z + 1, (byte)vtype, GenerateUV, GenerateColor);
                        AddVertex(Block, WorldPosition, val0, Interpolations, Info, tri[i + 1], x + 1, y + 1, z + 1, (byte)vtype, GenerateUV, GenerateColor);
                    }
                }
            }

            #endregion

            return true;
        }

        static public Vector3 AddSimpleVertex(MarchingBlock Block, Vector3 WorldPosition, MarchingInterpolationInfo val0, MarchingVoxelArray Interpolations, TempMeshData Info, int Index, int x, int y, int z, byte vType, bool GenerateUV)
        {
            Vector3 TempVector = Interpolations._tmpVertices[Index];
            Info.Triangles.Vertices.Add(TempVector);
            Info.Triangles.Indices.Add(Info.TriangleIndex++);

            if (GenerateUV)
            {
                TempVector.x += WorldPosition.x - 1;
                TempVector.y += WorldPosition.y - 1;
                TempVector.z += WorldPosition.z - 1;
                Info.Uvs.Add(new Vector2(TempVector.x + TempVector.y, TempVector.y + TempVector.z));
            }

            return TempVector;
        }

        static public void AddVertex(MarchingBlock Block, Vector3 WorldPosition, MarchingInterpolationInfo val0, MarchingVoxelArray Interpolations, TempMeshData Info, int Index, int x, int y, int z, byte vType, bool GenerateUV, bool GenerateColor)
        {
            if (val0._tempIndices[Index] == 0)
            {
                Vector3 TempVector = Interpolations._tmpVertices[Index];

                val0._tempIndices[Index] = Info.TriangleIndex;

                Info.Triangles.Vertices.Add(TempVector);
                Info.Triangles.Indices.Add(Info.TriangleIndex++);

                if (GenerateUV)
                {
                    TempVector.x += WorldPosition.x - 1;
                    TempVector.y += WorldPosition.y - 1;
                    TempVector.z += WorldPosition.z - 1;
                    Info.Uvs.Add(new Vector2(TempVector.x + TempVector.y, TempVector.y + TempVector.z));
                }

                if (GenerateColor)
                {
                    if (val0.Info != null && val0.Info.ColorFunction != null)
                        Info.Colors.Add(val0.Info.ColorFunction(TempVector.x + WorldPosition.x, TempVector.y + WorldPosition.y, TempVector.z + WorldPosition.z, Vector3.zero));
                    else
                        Info.Colors.Add(val0.CurrentColor);
                }
            }
            else
                Info.Triangles.Indices.Add(val0._tempIndices[Index]);
        }

        static public void AddOptimizedVertex(MarchingBlock Block, Vector3 WorldPosition, MarchingInterpolationInfo val0, MarchingVoxelArray Interpolations, TempMeshData Info, int Index, int x, int y, int z, byte vType, bool GenerateUV, bool GenerateColor)
        {
            if (val0._tempIndices[Index] == 0)
            {
                int Indice = 0;
                Vector3 TempVector = Interpolations._tmpVertices[Index];

                Indice = val0.Backward.GetIndice(val0.VType, TempVector);
                if (Indice != 0)
                {
                    val0._tempIndices[Index] = Indice;
                    val0._tempVertices[Index] = TempVector;
                    Info.Triangles.Indices.Add(Indice);
                    return;
                }

                Indice = val0.Bottom.GetIndice(val0.VType, TempVector);
                if (Indice != 0)
                {
                    val0._tempIndices[Index] = Indice;
                    val0._tempVertices[Index] = TempVector;
                    Info.Triangles.Indices.Add(Indice);
                    return;
                }

                Indice = val0.Top.Backward.GetIndice(val0.VType, TempVector);
                if (Indice != 0)
                {
                    val0._tempIndices[Index] = Indice;
                    val0._tempVertices[Index] = TempVector;
                    Info.Triangles.Indices.Add(Indice);
                    return;
                }

                Indice = val0.Left.GetIndice(val0.VType, TempVector);
                if (Indice != 0)
                {
                    val0._tempIndices[Index] = Indice;
                    val0._tempVertices[Index] = TempVector;
                    Info.Triangles.Indices.Add(Indice);
                    return;
                }

                val0._tempIndices[Index] = Info.TriangleIndex;
                val0._tempVertices[Index] = TempVector;

                Info.Triangles.Vertices.Add(TempVector);
                Info.Triangles.Indices.Add(Info.TriangleIndex++);

                if (GenerateUV)
                {
                    TempVector.x += WorldPosition.x - 1;
                    TempVector.y += WorldPosition.y - 1;
                    TempVector.z += WorldPosition.z - 1;
                    Info.Uvs.Add(new Vector2(TempVector.x + TempVector.y, TempVector.y + TempVector.z));
                }

                if (GenerateColor)
                {
                    if (val0.Info != null && val0.Info.ColorFunction != null)
                        Info.Colors.Add(val0.Info.ColorFunction(TempVector.x + WorldPosition.x, TempVector.y + WorldPosition.y, TempVector.z + WorldPosition.z, Vector3.zero));
                    else
                        Info.Colors.Add(val0.CurrentColor);
                }
            }
            else
                Info.Triangles.Indices.Add(val0._tempIndices[Index]);
        }

        #endregion

        #region AmbientOcclusion

        public void GenerateAmbientOcculsion(MarchingVoxelArray Interpolations, IMeshData Data)
        {
            if (Data == null || Data.Vertices.Length < 1)
                return;	// no vertices? then we have nothing to do.

            //Debug.Log("Generating Ambient occlusion");

            const int RAYS = 32;	// the number of rays we will cast per vertex
            const float INV_RAYS = 1f / (float)RAYS;
            const float cells_to_skip_at_start = 1f;
            float invVoxelDim = 1f / (float)Information.VoxelPerChunk;
            float amboRayCellDist = (float)2f;
            int AMBO_STEPS = Mathf.FloorToInt(amboRayCellDist); // * 1.4f);	// let the diagnols reach the edge too
            int STEP_SIZE = occlusion_amt.Length / AMBO_STEPS;	// calculate a step size that gets through our complete table, regardless of gutterSize
            STEP_SIZE = STEP_SIZE < 4 ? 4 : STEP_SIZE;
            float INV_AMBO_STEPS = 1f / AMBO_STEPS;
            int Len = Data.Vertices.Length;

            if (Data.Colors == null || Data.Colors.Length != Len)
            {
                Data.Colors = new Color[Len];
                for (int i = 0; i < Len; ++i)
                    Data.Colors[i] = new Color(1f, 1f, 1f, 1f);
            }

            Vector3 ray_start;
            Vector3 ray_dir;
            Vector3 ray_now;
            Vector3 ray_delta;
            int ray_index;
            int j;
            float ambo;
            float ambo_this;
            int k;

            for (int i = 0; i < Len; i++)
            {
                ray_start = Data.Vertices[i] / Information.Scale;
                ray_start.x += 1;
                ray_start.y += 1;
                ray_start.z += 1;

                //Debug.Log("Ray start  voxel:" + ray_start + ",position:" + Data.Vertices[i] + ",amboRayCellDist:" + amboRayCellDist);
                ambo = 0f;
                for (j = 0; j < RAYS; j++)
                {
                    ray_index = j * 3;

                    ray_dir.x = g_ray_dirs_32[ray_index];
                    ray_dir.y = g_ray_dirs_32[ray_index + 1];
                    ray_dir.z = g_ray_dirs_32[ray_index + 2];

                    ray_now = ray_start + ray_dir * invVoxelDim * cells_to_skip_at_start;
                    ray_delta = ray_dir * amboRayCellDist; // then over 160f?? That seems very small.

                    ambo_this = 1;

                    // SHORT RANGE:
                    //  -step along the ray at AMBO_STEPS points,
                    //     sampling the density volume texture
                    //  -occlusion_amt[] LUT makes closer occlusions have more weight than far ones
                    //  -start sampling a few cells away from the vertex, to reduce noise.
                    ray_delta *= INV_AMBO_STEPS;
                    for (k = 0; k < occlusion_amt.Length - 3; k += STEP_SIZE)
                    {
                        ray_now += ray_delta;
                        ambo_this = Mathf.Lerp(ambo_this, 0, 
                            Mathf.Clamp01(TrilinearSample.SampleClipped(Interpolations, ray_now) * 20f) * occlusion_amt[k + 1]); //* pow(1-j/(float)AMBO_STEPS,0.4)
                    }

                    // could do LONG RANGE next...

                    ambo_this *= 1.5f; //1.4f;	// Not sure about this scalar, but it is about right to bring up the highlights

                    ambo += ambo_this;
                }
                ambo *= INV_RAYS;
                Data.Colors[i].a = ambo;
            }
        }

        public static float[] g_ray_dirs_32 = new float[] {
              // pick one of the following.
              // ** make sure the # of points exactly matches the # of rays
              //    being cast, to get full and even coverage! **
              // (see shaders\hypernoise_on_gpu.xsh)
          
                  // 32 rays with a nice poisson distribution on a sphere:
                  0.286582f, 	 0.257763f, 	-0.922729f,
                  -0.171812f, 	 -0.888079f,	0.426375f,
                  0.440764f, 	 -0.502089f, 	-0.744066f,
                  -0.841007f, 	-0.428818f, 	-0.329882f,
                  -0.380213f, 	-0.588038f, 	-0.713898f,
                  -0.055393f, 	-0.207160f, 	 -0.976738f,
                  -0.901510f, 	 -0.077811f, 	 0.425706f,
                  -0.974593f, 	 0.123830f, 	 -0.186643f,
                  0.208042f, 	 -0.524280f, 	 0.825741f,
                  0.258429f, 	 -0.898570f, 	 -0.354663f,
                  -0.262118f, 	 0.574475f, 	 -0.775418f,
                  0.735212f, 	 0.551820f, 	 0.393646f,
                  0.828700f, 	 -0.523923f, 	 -0.196877f,
                  0.788742f, 	 0.005727f,	 	-0.614698f,
                  -0.696885f, 	 0.649338f, 	 -0.304486f,
                  -0.625313f, 	 0.082413f, 	 -0.776010f,
                  0.358696f, 	 0.928723f, 	 0.093864f,
                  0.188264f, 	 0.628978f, 	 0.754283f,
                  -0.495193f, 	 0.294596f, 	 0.817311f,
                  0.818889f, 	 0.508670f, 	 -0.265851f,
                  0.027189f,	 0.057757f, 	 0.997960f,
                  -0.188421f, 	 0.961802f, 	 -0.198582f,
                  0.995439f, 	 0.019982f, 	 0.093282f,
                  -0.315254f, 	 -0.925345f,  	-0.210596f,
                  0.411992f, 	 -0.877706f,	 0.244733f,
                  0.625857f, 	 0.080059f, 	 0.775818f,
                  -0.243839f, 	 0.866185f, 	 0.436194f,
                  -0.725464f, 	 -0.643645f, 	 0.243768f,
                  0.766785f, 	 -0.430702f, 	 0.475959f,
                  -0.446376f, 	 -0.391664f, 	 0.804580f,
                  -0.761557f, 	 0.562508f, 	 0.321895f,
                  0.344460f, 	 0.753223f, 	 -0.560359f};

        public static float[] occlusion_amt = new float[64] {
				// .x is (1-i/16)^2.5
                  // .y is (1-i/16)^0.3
                  // .z is (1-i/16)^0.4
                  // .w is (1-i/16)^0.5
                  1f,           1f,				1f,				1f,
                  0.850997317f, 0.980824675f,	0.97451496f,	0.968245837f,
                  0.716176609f, 0.960732353f,	0.947988832f,	0.935414347f,
                  0.595056802f, 0.93960866f,	0.920299843f,	0.901387819f,
                  0.48713929f,  0.917314755f,	0.891301229f,	0.866025404f,
                  0.391905859f, 0.893679531f,	0.860813523f,	0.829156198f,
                  0.308816178f, 0.868488366f,	0.828613504f,	0.790569415f ,
                  0.237304688f, 0.841466359f,	0.794417881f,	0.75f        ,
                  0.176776695f, 0.812252396f,	0.757858283f,	0.707106781f ,
                  0.126603334f, 0.780357156f,	0.718441189f,	0.661437828f ,
                  0.086114874f, 0.745091108f,	0.675480019f,	0.612372436f ,
                  0.054591503f, 0.705431757f,	0.627971608f,	0.559016994f ,
                  0.03125f,     0.659753955f,	0.574349177f,	0.5f         ,
                  0.015223103f, 0.605202038f,	0.511918128f,	0.433012702f ,
                  0.005524272f, 0.535886731f,	0.435275282f,	0.353553391f ,
                  0.000976563f, 0.435275282f,	0.329876978f,	0.25f
	};

        #endregion



        #region Tables

        static public int[] _edgeTable = new int[]
	        {
                0x0  , 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
                0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
                0x190, 0x99 , 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
                0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
                0x230, 0x339, 0x33 , 0x13a, 0x636, 0x73f, 0x435, 0x53c,
                0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
                0x3a0, 0x2a9, 0x1a3, 0xaa , 0x7a6, 0x6af, 0x5a5, 0x4ac,
                0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
                0x460, 0x569, 0x663, 0x76a, 0x66 , 0x16f, 0x265, 0x36c,
                0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
                0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff , 0x3f5, 0x2fc,
                0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
                0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55 , 0x15c,
                0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
                0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc ,
                0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
                0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
                0xcc , 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
                0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
                0x15c, 0x55 , 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
                0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
                0x2fc, 0x3f5, 0xff , 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
                0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
                0x36c, 0x265, 0x16f, 0x66 , 0x76a, 0x663, 0x569, 0x460,
                0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
                0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa , 0x1a3, 0x2a9, 0x3a0,
                0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
                0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33 , 0x339, 0x230,
                0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
                0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99 , 0x190,
                0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
                0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0
	        };

        /// <summary>
        /// Marching Cubes lookup table for indices.
        /// Note: -1 is a terminator.
        /// </summary>
        static public int[][] _triTable = new int[][]
        {
            new int[] {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1},
            new int[] {8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1},
            new int[] {3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1},
            new int[] {4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1},
            new int[] {4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
            new int[] {5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1},
            new int[] {9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1},
            new int[] {10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1},
            new int[] {5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1},
            new int[] {5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1},
            new int[] {10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1},
            new int[] {8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1},
            new int[] {2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1},
            new int[] {7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1},
            new int[] {2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1},
            new int[] {11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1},
            new int[] {5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1},
            new int[] {11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1},
            new int[] {11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1},
            new int[] {5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1},
            new int[] {2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
            new int[] {5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1},
            new int[] {6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1},
            new int[] {3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1},
            new int[] {6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1},
            new int[] {5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
            new int[] {10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1},
            new int[] {6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1},
            new int[] {8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1},
            new int[] {7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1},
            new int[] {3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
            new int[] {5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1},
            new int[] {0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1},
            new int[] {9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1},
            new int[] {8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1},
            new int[] {5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1},
            new int[] {0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1},
            new int[] {6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1},
            new int[] {10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1},
            new int[] {10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1},
            new int[] {8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1},
            new int[] {1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1},
            new int[] {0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1},
            new int[] {10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1},
            new int[] {3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1},
            new int[] {6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1},
            new int[] {9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1},
            new int[] {8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1},
            new int[] {3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1},
            new int[] {6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1},
            new int[] {10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1},
            new int[] {10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1},
            new int[] {2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1},
            new int[] {7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1},
            new int[] {7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1},
            new int[] {2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1},
            new int[] {1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1},
            new int[] {11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1},
            new int[] {8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1},
            new int[] {0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1},
            new int[] {7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
            new int[] {10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
            new int[] {6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1},
            new int[] {7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1},
            new int[] {10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1},
            new int[] {10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1},
            new int[] {0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1},
            new int[] {7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1},
            new int[] {6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1},
            new int[] {8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1},
            new int[] {6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1},
            new int[] {4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1},
            new int[] {10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1},
            new int[] {8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1},
            new int[] {1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1},
            new int[] {8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1},
            new int[] {10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1},
            new int[] {10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
            new int[] {5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
            new int[] {11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1},
            new int[] {9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
            new int[] {6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1},
            new int[] {7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1},
            new int[] {3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1},
            new int[] {7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1},
            new int[] {3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1},
            new int[] {6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1},
            new int[] {9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1},
            new int[] {1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1},
            new int[] {4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1},
            new int[] {7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1},
            new int[] {6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1},
            new int[] {0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1},
            new int[] {6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1},
            new int[] {0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1},
            new int[] {11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1},
            new int[] {6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1},
            new int[] {5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1},
            new int[] {9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1},
            new int[] {1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1},
            new int[] {10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1},
            new int[] {0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1},
            new int[] {5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1},
            new int[] {10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1},
            new int[] {11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1},
            new int[] {9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1},
            new int[] {7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1},
            new int[] {2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1},
            new int[] {8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1},
            new int[] {9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1},
            new int[] {9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1},
            new int[] {1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1},
            new int[] {5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1},
            new int[] {0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1},
            new int[] {10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1},
            new int[] {2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1},
            new int[] {0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1},
            new int[] {0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1},
            new int[] {9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1},
            new int[] {5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1},
            new int[] {5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1},
            new int[] {8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1},
            new int[] {9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1},
            new int[] {1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1},
            new int[] {3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1},
            new int[] {4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1},
            new int[] {9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1},
            new int[] {11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1},
            new int[] {11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1},
            new int[] {2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1},
            new int[] {9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1},
            new int[] {3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1},
            new int[] {1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1},
            new int[] {4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1},
            new int[] {0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1},
            new int[] {9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1},
            new int[] {1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            new int[] {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}
        };

        #endregion
    }
}
