﻿using UnityEngine;

namespace TerrainEngine
{
    public class TrilinearSample
    {
        public static float SampleRaw(MarchingVoxelArray data, Vector3 xyz)
        {
            //	    xyz.x = (Mathf.Repeat(xyz.x, data.Length-0.05f)); //Mathf.Max((xyz.x % data.Length-2), 0)); 
            int i = Mathf.FloorToInt(xyz.x);
            float u = xyz.x - i;
            //		xyz.y = (Mathf.Repeat(xyz.y, data[i].Length-0.05f));
            int j = Mathf.FloorToInt(xyz.y); //Mathf.Max((xyz.y % data[i].Length-2), 0)); 
            float v = xyz.y - j;
            //		xyz.z = (Mathf.Repeat(xyz.z, data[i][j].Length-0.05f));;
            int k = Mathf.FloorToInt(xyz.z); //Mathf.Max((xyz.z % data[i][j].Length-2), 0)); 
            float w = xyz.z - k;

            //		Debug.Log(i + "," + j + "," + k);
            int i1 = (i + 1) % data.SizeX;
            int j1 = (j + 1) % data.SizeY;
            int k1 = (k + 1) % data.SizeZ;

            float a = data[i, j, k].Volume - TerrainInformation.Isolevel;
            float b = data[i1, j, k].Volume - TerrainInformation.Isolevel;
            float c = data[i, j1, k].Volume - TerrainInformation.Isolevel;
            float d = data[i1, j1, k].Volume - TerrainInformation.Isolevel;
            float e = data[i, j, k1].Volume - TerrainInformation.Isolevel;
            float f = data[i1, j, k1].Volume - TerrainInformation.Isolevel;
            float g = data[i, j1, k1].Volume - TerrainInformation.Isolevel;
            float h = data[i1, j1, k1].Volume - TerrainInformation.Isolevel;

            float invU = 1 - u;
            float invV = 1 - v;
            float invW = 1 - w;

            float Vxyz = a * (invU) * (invV) * (invW) +
                    b * u * (invV) * (invW) +
                    c * (invU) * v * (invW) +
                    e * (invU) * (invV) * w +
                    f * u * (invV) * w +
                    g * (invU) * v * w +
                    d * u * v * (invW) +
                    h * u * v * w;
            return Vxyz;
        }

        public static float Sample(MarchingVoxelArray data, Vector3 xyz)
        {
            while (xyz.x < 0)
                xyz.x += data.SizeX;
            xyz.x = xyz.x % data.SizeX;
            while (xyz.y < 0)
                xyz.y += data.SizeY;
            xyz.y = xyz.y % data.SizeY;
            while (xyz.z < 0)
                xyz.z += data.SizeZ;
            xyz.z = xyz.z % data.SizeZ;
            return SampleRaw(data, xyz);
        }

        // return a sample, but return defaultValue if xyz is out of range
        public static float SampleClipped(MarchingVoxelArray data, Vector3 xyz, float defaultValue = 0)
        {
            if (xyz.x < 0 || xyz.y < 0 || xyz.z < 0 || xyz.x > data.SizeX - 1 || xyz.y > data.SizeY - 1 || xyz.z > data.SizeZ - 1)
                return defaultValue;
            else return SampleRaw(data, xyz);	// the Repeat calls will now be redundant, but probably not worth duplicating code to eliminate in this case.
        }

        private static float SampleWrapped(MarchingVoxelArray data, int x, int y, int z)
        {
            x = (int)Mathf.PingPong(x, data.SizeX);
            y = (int)Mathf.PingPong(y, data.SizeY);
            return data[x,y,(int)Mathf.PingPong(z, data.SizeZ)].Volume;
        }

        public static Vector3 SampleRaw(Vector3[][][] data, Vector3 xyz)
        {
            int i = Mathf.FloorToInt(xyz.x);
            float u = xyz.x - i;

            int j = Mathf.FloorToInt(xyz.y); //Mathf.Max((xyz.y % data[i].Length-2), 0)); 
            float v = xyz.y - j;

            int k = Mathf.FloorToInt(xyz.z); //Mathf.Max((xyz.z % data[i][j].Length-2), 0)); 
            float w = xyz.z - k;

            int i1 = (i + 1) % data.Length; // (int)(Mathf.Repeat(i+1, data.Length));
            int j1 = (j + 1) % data[i1].Length; //(int)(Mathf.Repeat(j+1, data[i].Length));
            int k1 = (k + 1) % data[i1][j1].Length; //(int)(Mathf.Repeat(k+1, data[i][j].Length));

            Vector3 a = data[i][j][k];
            Vector3 b = data[i1][j][k];
            Vector3 c = data[i][j1][k];
            Vector3 d = data[i1][j1][k];
            Vector3 e = data[i][j][k1];
            Vector3 f = data[i1][j][k1];
            Vector3 g = data[i][j1][k1];
            Vector3 h = data[i1][j1][k1];

            float invU = 1 - u;
            float invV = 1 - v;
            float invW = 1 - w;

            Vector3 Vxyz = a * (invU) * (invV) * (invW) +
                    b * u * (invV) * (invW) +
                    c * (invU) * v * (invW) +
                    e * (invU) * (invV) * w +
                    f * u * (invV) * w +
                    g * (invU) * v * w +
                    d * u * v * (invW) +
                    h * u * v * w;
            return Vxyz;
        }
    }

}
