﻿
using TerrainEngine;
using UnityEngine;

public class MarchingPalette : IVoxelPalette
{
    public MarchingInformation[] VoxelBlocks;

    void Start()
    {
        for (int i = 0; i < VoxelBlocks.Length; ++i)
        {
            VoxelBlocks[i].MaterialInfo = new MaterialIdInformation()
            {
                Mat = VoxelBlocks[i].Material,
                InstanceId = (VoxelBlocks[i].Material != null ? VoxelBlocks[i].Material.GetInstanceID() : -1)
            };

            if (VoxelBlocks[i].ColorGenerator != null)
                VoxelBlocks[i].ColorFunction = VoxelBlocks[i].ColorGenerator.GenerateColor;

            InitSplatMap(VoxelBlocks[i], i, i + 1);
        }
    }

    public void InitSplatMap(MarchingInformation Info,int VoxelId, int Id)
    {
        if (!Info.UseSplatMap || Info.MainVoxelId != 0)
            return;

        Info.SplatColor = Info.GetSplatColor();
        Info.SplatVoxels.Add(VoxelId);

        int Count = 0;
        for (; Id < VoxelBlocks.Length; ++Id)
        {
            if (VoxelBlocks[Id].UseSplatMap && VoxelBlocks[Id].Material == Info.Material && VoxelBlocks[Id].MainVoxelId == 0)
            {
                Debug.Log("Voxel:" + Info.Name + ",Use SplatMap with same material of voxel :" + VoxelBlocks[Id].Name+",SplatColor:"+VoxelBlocks[Id].GetSplatColor());

                if (Count == 3)
                {
                    Debug.LogError("Max SplatMap support reached : " + VoxelBlocks[Id].Name);
                    return;
                }

                VoxelBlocks[Id].SplatColor = VoxelBlocks[Id].GetSplatColor();
                Info.SplatVoxels.Add(Id);
                VoxelBlocks[Id].MainVoxelId = (byte)VoxelId;
                ++Count;
            }
        }
    }

    public override T GetVoxelData<T>(byte Type)
    {
        return VoxelBlocks[Type] as T;
    }

    public override int GetDataCount()
    {
        return VoxelBlocks.Length;
    }
}
