﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class MarchingInformation : VoxelInformation
    {
        [Serializable]
        public enum ColorTypes
        {
            COLOR_R = 0,
            COLOR_G = 1,
            COLOR_B = 2,
            COLOR_A = 3,
        }

        public Material Material;
        public Shader LocalShader;
        public IColorGenerator ColorGenerator;
        public bool UseSplatMap = false;
        public ColorTypes Color;

        [HideInInspector]
        public MaterialIdInformation MaterialInfo;

        [HideInInspector]
        public Color SplatColor;

        public Color GetSplatColor()
        {
            switch (Color)
            {
                case ColorTypes.COLOR_R:
                    return new Color(1f, 0, 0, 0);
                case ColorTypes.COLOR_G:
                    return new Color(0, 1f, 0, 0);
                case ColorTypes.COLOR_B:
                    return new Color(0, 0, 1f, 0);
                case ColorTypes.COLOR_A:
                    return new Color(0, 0, 0, 1f);
            }

            return new Color(1, 0, 0, 0);
        }

        [HideInInspector]
        public byte MainVoxelId = 0;

        [HideInInspector]
        public List<int> SplatVoxels = new List<int>();

        [HideInInspector]
        public IColorGenerator.GenerateColorDelegate ColorFunction;
    }
}
