﻿using System;

using UnityEngine;

namespace TerrainEngine
{
    public struct Triangle
    {
        public Vector3[] Position;
        public int[] Indices;

        public Triangle(int Size)
        {
            Position = new Vector3[Size];
            Indices = new int[Size];
        }
    }

    public class MarchingVoxelArray : IVoxelArray
    {
        public int ISizeX;
        public int ISizeY;
        public int ISizeZ;

        public Vector3[] _tmpVertices;
        public MarchingInterpolationInfo[] Temp;

        private int LastSizeX, LastSizeY, LastSizeZ;

        public MarchingVoxelArray()
        {

        }

        public override void Init(int SizeX, int SizeY, int SizeZ, int BorderSize, int PaletteCount)
        {
            base.Init(SizeX, SizeY, SizeZ, BorderSize, PaletteCount);
            this.MaterialCount = 0;

            this.ISizeX = SizeX - 1;
            this.ISizeY = SizeY - 1;
            this.ISizeZ = SizeZ - 1;

            if(this._tmpVertices == null)
                this._tmpVertices = new Vector3[12];

            InitArray();
        }

        #region JaggedArray 3.00 ms 16*16*16 Chunk

        public void InitArray()
        {
            bool IsNew = false;
            if (Temp == null)
            {
                Temp = new MarchingInterpolationInfo[SizeX * SizeY * SizeZ];
                IsNew = true;
            }
            else if (Temp.Length < SizeX * SizeY * SizeZ)
            {
                for (int i = Temp.Length - 1; i >= 0; --i)
                {
                    Temp[i].Init(0, 0, 0, 0, 0);
                }

                Array.Resize<MarchingInterpolationInfo>(ref Temp, SizeX * SizeY * SizeZ);
            }

            if (IsNew || LastSizeX != SizeX || LastSizeY != SizeY || LastSizeZ != SizeZ)
            {
                LastSizeX = SizeX;
                LastSizeY = SizeY;
                LastSizeZ = SizeZ;
                FillArray(IsNew);
            }
        }

        public void FillArray(bool IsNew)
        {
            int x, y, z, i = 0;

            if (!IsNew)
            {
                for (x = 0; x < SizeX; ++x)
                {
                    for (y = 0; y < SizeY; ++y)
                    {
                        for (z = 0; z < SizeZ; ++z)
                        {
                            if (Temp[i] == null)
                                Temp[i] = new MarchingInterpolationInfo(x, y, z, 0, 0);
                            else
                                Temp[i].Init(x, y, z, 0, 0);
                            ++i;
                        }
                    }
                }
            }
            else
            {
                for (x = 0; x < SizeX; ++x)
                {
                    for (y = 0; y < SizeY; ++y)
                    {
                        for (z = 0; z < SizeZ; ++z)
                        {
                            Temp[i] = new MarchingInterpolationInfo(x, y, z, 0, 0);
                            ++i;
                        }
                    }
                }
            }

            i = 0;
            for (x = 0; x < SizeX; ++x)
            {
                for (y = 0; y < SizeY; ++y)
                {
                    for (z = 0; z < SizeZ; ++z)
                    {
                        Temp[i].Link(this);
                        ++i;
                    }
                }
            }
        }

        public int GetId(int x, int y, int z)
        {
            return x * SizeY * SizeZ + y * SizeZ + z;
        }

        public MarchingInterpolationInfo this[int x, int y, int z]
        {
            get
            {
                return Temp[x * SizeY * SizeZ + y * SizeZ + z];
            }
            set
            {
                Temp[x * SizeY * SizeZ + y * SizeZ + z] = value;
            }
        }


        #endregion
    }

    public class MarchingInterpolationInfo
    {
        [Flags]
        public enum InterpolationState : byte
        {
            NONE = 0,
            BORDER = 1,
            ENCLOSE = 2,
            FULL = 4,
        }

        public byte VType;
        public float Volume;
        public InterpolationState State = InterpolationState.NONE;
        public MarchingInformation Info;
        public VoxelTypes VoxelType = VoxelTypes.OPAQUE;
        public Vector3 p;
        public int[] _tempIndices = new int[12];
        public Vector3[] _tempVertices = new Vector3[12];
        public Color CurrentColor = Color.red;

        int x
        {
            get
            {
                return (int)p.x + 1;
            }
        }
        int y
        {
            get
            {
                return (int)p.y + 1;
            }
        }
        int z
        {
            get
            {
                return (int)p.z + 1;
            }
        }

        public MarchingInterpolationInfo(int x, int y,int z, float volume, byte vtype)
        {
            this.Volume = volume;
            this.VType = vtype;
            this.State = (int)InterpolationState.NONE;
            this.p.x = x - 1;
            this.p.y = y - 1;
            this.p.z = z - 1;
        }

        public void Init(int x, int y, int z, float volume, byte vtype)
        {
            this.Volume = volume;
            this.VType = vtype;
            this.State = (int)InterpolationState.NONE;
            this.p.x = x - 1;
            this.p.y = y - 1;
            this.p.z = z - 1;

            Left = null;
            Right = null;
            Top = null;
            Bottom = null;
            Forward = null;
            Backward = null;
            ClearIndices();
        }

        public void ClearIndices()
        {
            _tempIndices[0]
            = _tempIndices[1]
            = _tempIndices[2]
            = _tempIndices[3]
            = _tempIndices[4]
            = _tempIndices[5]
            = _tempIndices[6]
            = _tempIndices[7]
            = _tempIndices[8]
            = _tempIndices[9]
            = _tempIndices[10]
            = _tempIndices[11] = 0;
        }

        public int GetIndice(byte Type, Vector3 Vector)
        {
            if (VType != 0 && Type != 0 && VType != Type)
                return 0;

            Vector3 t;
            for (int i = 0; i < 12; ++i)
            {
                if (_tempIndices[i] != 0)
                {
                    t = _tempVertices[i];
                    if (Vector.x == t.x && Vector.y == t.y && Vector.z == t.z)
                        return _tempIndices[i];
                }
            }

            return 0;
        }

        public bool Full
        {
            get
            {
                return (State & InterpolationState.FULL) == InterpolationState.FULL;
            }
            set
            {
                if (value)
                    State |= InterpolationState.FULL;
            }
        }

        public bool Enclose
        {
            get
            {
                return (State & InterpolationState.ENCLOSE) == InterpolationState.ENCLOSE;
            }
            set
            {
                if (value)
                    State |= InterpolationState.ENCLOSE;
            }
        }

        public bool Border
        {
            get
            {
                return (State & InterpolationState.BORDER) == InterpolationState.BORDER;
            }
            set
            {
                if (value)
                    State |= InterpolationState.BORDER;
            }
        }

        public override string ToString()
        {
            return "V=" + Volume + ",T=" + VType + ",B=" + Border + ",E=" + Enclose + ",F=" + Full;
        }

        public void Link(MarchingVoxelArray Temp)
        {
            if (x != 0)
                Left = Temp[x - 1, y, z];
            else
                Left = null;

            if (x != Temp.ISizeX)
                Right = Temp[x + 1, y, z];
            else
                Right = null;

            if (y != Temp.ISizeY)
                Top = Temp[x, y + 1, z];
            else
                Top = null;

            if (y != 0)
                Bottom = Temp[x, y - 1, z];
            else
                Bottom = null;

            if (z != Temp.ISizeZ)
                Forward = Temp[x, y, z + 1];
            else
                Forward = null;

            if (z != 0)
                Backward = Temp[x, y, z - 1];
            else
                Backward = null;
        }

        // Enclose Check
        public MarchingInterpolationInfo Left;

        public MarchingInterpolationInfo Right;

        public MarchingInterpolationInfo Top;

        public MarchingInterpolationInfo Bottom;

        public MarchingInterpolationInfo Forward;

        public MarchingInterpolationInfo Backward;

        // Marching Calculation
        public MarchingInterpolationInfo TopRight
        {
            get
            {
                if (Top != null)
                    return Top.Right;
                return null;
            }
        }

        public MarchingInterpolationInfo ForwardRight
        {
            get
            {
                if (Forward != null)
                    return Forward.Right;
                return null;
            }
        }

        public MarchingInterpolationInfo ForwardTopRight
        {
            get
            {
                if (Forward != null && Forward.Top !=null)
                    return Forward.Top.Right;
                return null;
            }
        }

        public MarchingInterpolationInfo ForwardTop
        {
            get
            {
                if (Forward != null)
                    return Forward.Top;
                return null;
            }
        }
    }
}
