﻿using System;
using TerrainEngine;
using UnityEngine;

public class LavaMarchingConstructor : FluidMeshConstructor
{
    public GameObject CollisionParticles;
    public float OnNewBlockPctChance = 50f;
    public float OnGroundCollisionPctChance = 50f;
    public float OnWaterCollisionPctChance = 50f;
    public float RemoveTime = 1;
    public int MaxParticlesObjects = 500;
    public bool NoiseColor = true;

    private FastRandom FRandom = new FastRandom(1852);

    [HideInInspector]
    public PoolInfoRef FluidPool;
    private int ObjectPool;

    public LibNoise.Perlin Perlin;

    public override void OnStart()
    {
        FluidPool = PoolManager.Pool.GetPool("LavaParticle");

        if (CollisionParticles == null)
            OnNewBlockPctChance = OnGroundCollisionPctChance = OnWaterCollisionPctChance = 0;
        else
            ObjectPool = GameObjectPool.GetPoolId(CollisionParticles.name);

        Perlin = new LibNoise.Perlin();
        Perlin.Frequency = 0.05;
        Perlin.OctaveCount = 2;
    }

    public override IMeshData GenerateMesh(ActionThread Thread, TerrainBlock Terrain, int ChunkId, FluidDatas FData, FluidInterpolation FluidInterpolations, FluidInformation FInfo, byte Type)
    {
        MarchingVoxelArray Interpolations = GetInterpolations(Thread, Terrain as MarchingBlock, ChunkId, FluidInterpolations, TerrainManager.Instance.Palette.DataCount, Type);
        if (Interpolations.MaterialCount == 0)
            return null;

        TempMeshData Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
        IMeshData Data = PoolManager.UPool.GetSimpleMeshData();
        Info.CurrentIndices = PoolManager.UPool.GetIndicesArray(Interpolations.MaterialCount);
        Interpolations.MaterialCount = 0;

        int i;
        for (i = 0; i < Interpolations.PaletteCount; ++i)
        {
            if (Interpolations.ActiveSubMesh[i])
            {
                Info.TempMaterials.Add(TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>((byte)i).MaterialInfo);
                ++Interpolations.MaterialCount;
            }
        }

        Data.Materials = PoolManager.UPool.GetMaterials(Info.TempMaterials);
        Interpolations.MaterialCount = 0;

        int SizeX = TerrainManager.Instance.Informations.VoxelPerChunk + 2;
        int SizeY = TerrainManager.Instance.Informations.VoxelPerChunk + 2;
        int SizeZ = TerrainManager.Instance.Informations.VoxelPerChunk + 2;

        MarchingInterpolationInfo val0 = null;
        int x, y, z;

        Color Red = new Color(0, 0, 0, 0.95f); // Red
        Color Yellow = new Color(0, 0, 0, 0); // Yellow

        int StartX = Terrain.Chunks[ChunkId].StartX;
        int StartZ = Terrain.Chunks[ChunkId].StartZ;

        for (x = 0; x < SizeX; ++x)
        {
            for (y = 0; y < SizeY; ++y)
            {
                for (z = 0; z < SizeZ; ++z)
                {
                    val0 = Interpolations[x, y, z];

                    if ((val0.Top != null && val0.Top.VoxelType == VoxelTypes.FLUID)
                        || (val0.Left != null && val0.Left.VoxelType == VoxelTypes.FLUID)
                        || (val0.Right != null && val0.Right.VoxelType == VoxelTypes.FLUID)
                        || (val0.Bottom != null && val0.Bottom.VoxelType == VoxelTypes.FLUID)
                        || (val0.Forward != null && val0.Forward.VoxelType == VoxelTypes.FLUID)
                        || (val0.TopRight != null && val0.TopRight.VoxelType == VoxelTypes.FLUID)
                        || (val0.Backward != null && val0.Backward.VoxelType == VoxelTypes.FLUID)
                        || (val0.ForwardTop != null && val0.ForwardTop.VoxelType == VoxelTypes.FLUID)
                        || (val0.ForwardRight != null && val0.ForwardRight.VoxelType == VoxelTypes.FLUID)
                        || (val0.ForwardTopRight != null && val0.ForwardTopRight.VoxelType == VoxelTypes.FLUID))
                        val0.Enclose = false;
                    else
                        val0.Enclose = true;

                    if (val0.Left != null && !val0.Left.Border && (val0.Left.VType != Type || val0.Left.Volume < 0.5f))
                        val0.CurrentColor = Red;
                    else if (val0.Right != null && !val0.Right.Border && (val0.Right.VType != Type || val0.Right.Volume < 0.5f))
                        val0.CurrentColor = Red;
                    else if (val0.Forward != null && !val0.Forward.Border && (val0.Forward.VType != Type || val0.Forward.Volume < 0.5f))
                        val0.CurrentColor = Red;
                    else if (val0.Backward != null && !val0.Backward.Border && (val0.Backward.VType != Type || val0.Backward.Volume < 0.5f))
                        val0.CurrentColor = Red;
                    else if (val0.Bottom != null && !val0.Bottom.Border && (val0.Bottom.VType != Type || val0.Bottom.Volume < 0.5f))
                        val0.CurrentColor = Red;
                    else
                    {
                        if (!NoiseColor || Perlin.GetValue(Terrain.GetWorldVoxelX(StartX + x - 1), 0, Terrain.GetWorldVoxelZ(StartZ + z - 1)) < 0)
                            val0.CurrentColor = Yellow;
                        else
                            val0.CurrentColor = Red;
                    }
                }
            }
        }

        for (i = 0; i < Interpolations.PaletteCount; ++i)
        {
            if (Interpolations.ActiveSubMesh[i] == false)
                continue;

            (Terrain as MarchingBlock).GenerateMesh(Terrain.GetChunkWorldPosition(ChunkId), Terrain.HasChunkState(ChunkId, ChunkStates.Border), 
                Interpolations, Info, Data, true, true, true, i, FInfo.GenerateUV, true, 0);
        }

        //Info.GenerateNormals();

        Data.Vertices = Info.Vertices.ToArray();
        Data.UVs = Info.Uvs.ToArray();
        Data.Indices = Info.CurrentIndices;
        Data.Normals = Info.Normals.ToArray();
        Data.Colors = Info.Colors.ToArray();

        Interpolations.Close();
        Info.Close();

        if (!Data.IsValid())
        {
            PoolManager.UPool.CloseIndicesArray(Data.Indices);
            Data.Close();
            Data = null;
        }

        return Data;
    }

    public MarchingVoxelArray GetInterpolations(ActionThread Thread, MarchingBlock Block, int ChunkId, FluidInterpolation FluidInterpolations, int PaletteCount, byte Type)
    {
        byte ValType = 0;

        int SizeX = TerrainManager.Instance.Informations.VoxelPerChunk;
        int SizeY = TerrainManager.Instance.Informations.VoxelPerChunk;
        int SizeZ = TerrainManager.Instance.Informations.VoxelPerChunk;

        MarchingVoxelArray Interpolations = Thread.Pool.UnityPool.MarchingFluidPool.GetData<MarchingVoxelArray>();
        Interpolations.Init(SizeX + 2, SizeY + 2, SizeZ + 2, 2, PaletteCount);
        MarchingInterpolationInfo Info = null;
        FluidTempBlock TempInfo = null;

        int x, y, z, f = 0;
        for (x = 0; x < SizeX + 2; ++x)
        {
            for (y = 0; y < SizeY + 2; ++y)
            {
                for (z = 0; z < SizeZ + 2; ++z,++f)
                {
                    Info = Interpolations[x, y, z];
                    Info.State = MarchingInterpolationInfo.InterpolationState.NONE;
                    Info.ClearIndices();

                    TempInfo = FluidInterpolations.Temp[f];
                    ValType = Info.VType = TempInfo.Type;
                    Info.Border = TempInfo.Block == null;

                    if (ValType == Type && (Info.VoxelType = TerrainManager.Instance.Palette.GetType(ValType)) == VoxelTypes.FLUID)
                    {
                        Info.Volume = 0.50f + ((float)TempInfo.Volume / (float)FluidData.MaxVolume) * 0.50f;
                        Info.Full = true;

                        Interpolations.ActiveMaterial(ValType);
                    }
                    else
                    {
                        if (TempInfo.TypeVolume < TerrainInformation.Isolevel)
                        {
                            Info.VType = 0;
                            Info.Volume = 0;
                        }
                    }
                }
            }
        }

        return Interpolations;
    }

    public override void OnPreApply(FluidTempBlock Block)
    {
        // Collision over ground
        if (Block.OriginalVolume == 0)
        {
            if (OnNewBlockPctChance != 0)
            {
                if (FRandom.randomIntAbs(100) < OnNewBlockPctChance)
                {
                    //Debug.Log("NewVolume");
                    Collide(Block);
                }
            }
            else
            {
                switch (Block.Direction)
                {
                    case FluidDirections.LEFT:
                        CheckParticlesCollisions(Block, Block.Left, Block.Direction);
                        break;
                    case FluidDirections.RIGHT:
                        CheckParticlesCollisions(Block, Block.Right, Block.Direction);
                        break;
                    case FluidDirections.FORWARD:
                        CheckParticlesCollisions(Block, Block.Forward, Block.Direction);
                        break;
                    case FluidDirections.BACKWARD:
                        CheckParticlesCollisions(Block, Block.Backward, Block.Direction);
                        break;
                    case FluidDirections.BOTTOM:
                        CheckParticlesCollisions(Block, Block.Bottom, Block.Direction);
                        break;
                };
            }
        }
        else if (OnWaterCollisionPctChance != 0)// Check Collision over Fluids
        {
            switch (Block.Direction)
            {
                case FluidDirections.LEFT:
                    if (Block.Left != null && Block.Left.Direction == FluidDirections.RIGHT && Block.Left.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Left :" + Block.Left.Volume + ",Volume=" + Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
                case FluidDirections.RIGHT:
                    if (Block.Right != null && Block.Right.Direction == FluidDirections.LEFT && Block.Right.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Right :" + Block.Right.Volume + ",Volume=" + Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
                case FluidDirections.FORWARD:
                    if (Block.Forward != null && Block.Forward.Direction == FluidDirections.BACKWARD && Block.Forward.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Forward :" + Block.Forward.Volume + ",Volume=" + Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
                case FluidDirections.BACKWARD:
                    if (Block.Backward != null && Block.Backward.Direction == FluidDirections.FORWARD && Block.Backward.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Backward :" + Block.Backward.Volume + ",Volume=" + Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
                case FluidDirections.BOTTOM:
                    if (Block.Bottom != null && Block.Bottom.Direction != FluidDirections.BOTTOM && Block.Bottom.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Bottom :" + Block.Bottom.Volume +",Volume="+Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
            };
        }
    }

    public void CheckParticlesCollisions(FluidTempBlock Block, FluidTempBlock CloseBlock, FluidDirections Direction)
    {
        if (CloseBlock == null || CloseBlock.Data == null)
            return;

        if (CloseBlock.Type != 0 && CloseBlock.Type != Block.FluidType)
        {
            if (FRandom.randomIntAbs(100) < OnGroundCollisionPctChance)
                Collide(Block);
        }
    }

    public void Collide(FluidTempBlock Block)
    {
        if (GameObjectPool.Pools.array[ObjectPool].QueueCount <= 0)
        {
            if (GameObjectPool.Pools.array[ObjectPool].CreatedObject >= MaxParticlesObjects)
                return;

            if (FluidPool.CreatedObject >= MaxParticlesObjects)
                return;
        }

        CreateGameObjectAction Action = FluidPool.GetData<CreateGameObjectAction>();
        Action.Init(CollisionParticles, TerrainManager.Instance.Informations.GetVoxelWorldPosition(Block.Block.LocalPosition, Block.x, Block.y, Block.z), Quaternion.identity, Vector3.one, RemoveTime, true);
        ActionProcessor.AddToUnity(Action);
    }
}
