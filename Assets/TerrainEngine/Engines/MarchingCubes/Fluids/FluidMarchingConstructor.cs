using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;


public class WaterfallConstructor : IPoolable
{
    public List<KeyValuePair<Vector3, GameObject>> Waterfalls = new List<KeyValuePair<Vector3, GameObject>>();
    public List<KeyValuePair<Vector3,float>> Points = new List<KeyValuePair<Vector3, float>>();
    public List<Vector3> CreatedPoints = new List<Vector3>();
    public FluidMarchingConstructor Constructor;

    public WaterfallConstructor(FluidMarchingConstructor Constructor)
    {
        this.Constructor = Constructor;
    }

    public void Register(FluidTempBlock Fluid, float Orientation)
    {
        if (Fluid != null && Fluid.Block != null)
        {
            Points.Add(new KeyValuePair<Vector3,float>(Fluid.Block.GetWorldPosition(Fluid.x, Fluid.y, Fluid.z, false),Orientation));
        }
    }

    public void Update(FluidMarchingConstructor Constructor)
    {
        int i;

        lock (Waterfalls)
        {
            for (i = 0; i < Waterfalls.Count; )
            {
                if (!HasNearPoint(Waterfalls[i].Key))
                {
                    GameObjectPoolRemoveAction PAction = Constructor.RemoveCascadePool.GetData<GameObjectPoolRemoveAction>();
                    PAction.Init(Constructor.WaterfallPool, Waterfalls[i].Value);
                    ActionProcessor.AddToUnity(PAction);

                    Waterfalls.RemoveAt(i);
                    continue;
                }

                ++i;
            }

            foreach (KeyValuePair<Vector3,float> KP in Points)
            {
                if (!HasNear(KP.Key))
                {
                    Create(Constructor, KP.Key, KP.Value);
                }
            }
        }

        Clean();
    }

    public void Clean()
    {
        CreatedPoints.Clear();
        Points.Clear();
    }

    public override void Clear()
    {
        Clean();
        for (int i = 0; i < Waterfalls.Count; ++i)
        {
            GameObjectPoolRemoveAction PAction = Constructor.RemoveCascadePool.GetData<GameObjectPoolRemoveAction>();
            PAction.Init(Constructor.WaterfallPool, Waterfalls[i].Value);
            ActionProcessor.AddToUnity(PAction);
        }
        Waterfalls.Clear();
    }

    public void RemoveCascade(GameObject Go)
    {

    }

    public bool HasNearPoint(Vector3 position)
    {
        foreach (KeyValuePair<Vector3, float> KP in Points)
        {
            if (Vector3.Distance(position, KP.Key) <= Constructor.MinWaterFallDistance)
                return true;
        }

        return false;
    }

    public bool HasNear(Vector3 P)
    {
        foreach (KeyValuePair<Vector3, GameObject> KP in Waterfalls)
        {
            if (Vector3.Distance(KP.Key, P) <= Constructor.MinWaterFallDistance)
            {
                return true;
            }
        }

        foreach (Vector3 TempPoint in CreatedPoints)
        {
            if (Vector3.Distance(TempPoint, P) <= Constructor.MinWaterFallDistance)
            {
                return true;
            }
        }


        return false;
    }

    public void Create(FluidMarchingConstructor Constructor, Vector3 P, float Orientation)
    {
        CreateGameObjectAction Action = Constructor.FluidPool.GetData<CreateGameObjectAction>();
        Action.Init(Constructor.WaterfallParticles, P, Quaternion.Euler(0, Orientation, 0), Vector3.one, 0, true, Constructor.WaterfallPool);
        Action.OnEnd = OnParticleCreated;
        ActionProcessor.AddToUnity(Action);
        CreatedPoints.Add(Action.Position);
    }

    public void OnParticleCreated(CreateGameObjectAction Action)
    {
        lock(Waterfalls)
            Waterfalls.Add(new KeyValuePair<Vector3, GameObject>(Action.Position, Action.CreatedObject));
    }
}

public class FluidMarchingConstructor : FluidMeshConstructor
{
    public GameObject CollisionParticles;
    public float OnNewBlockPctChance = 50f;
    public float OnGroundCollisionPctChance = 50f;
    public float OnWaterCollisionPctChance = 50f;
    public float RemoveTime = 1;
    public int MaxParticlesObjects = 500;

    private FastRandom FRandom = new FastRandom(1852);

    [HideInInspector]
    public PoolInfoRef FluidPool;
    private int ObjectPool;

    public GameObject WaterfallParticles;
    public float MinWaterFallDistance = 4;

    [HideInInspector]
    public int WaterfallPool=-1;
    public PoolInfoRef RemoveCascadePool;

    public override void OnStart()
    {
        FluidPool = PoolManager.Pool.GetPool("FluidParticle");
        RemoveCascadePool = PoolManager.Pool.GetPool("GameObjectPoolRemoveAction");

        if (CollisionParticles == null)
            OnNewBlockPctChance = OnGroundCollisionPctChance = OnWaterCollisionPctChance = 0;
        else
            ObjectPool = GameObjectPool.GetPoolId(CollisionParticles.name, true);

        if (WaterfallParticles != null)
            WaterfallPool = GameObjectPool.GetPoolId(WaterfallParticles.name, true);
    }

    public override IMeshData GenerateMesh(ActionThread Thread, TerrainBlock Terrain, int ChunkId, FluidDatas FData, FluidInterpolation FluidInterpolations, FluidInformation FInfo, byte Type)
    {
        MarchingVoxelArray Interpolations = GetInterpolations(Thread, Terrain as MarchingBlock, ChunkId, FluidInterpolations, TerrainManager.Instance.Palette.DataCount, Type);

        if (Interpolations.MaterialCount == 0)
            return null;

        TempMeshData Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
        IMeshData Data = PoolManager.UPool.GetCompleteMeshData();

        Info.CurrentIndices = PoolManager.UPool.GetIndicesArray(Interpolations.MaterialCount);
        Interpolations.MaterialCount = 0;

        Info.TempMaterials.Clear();

        if (Interpolations.ActiveSubMesh[Type])
        {
            Info.TempMaterials.Add(TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>((byte)Type).MaterialInfo);
            ++Interpolations.MaterialCount;
        }

        Data.Materials = PoolManager.UPool.GetMaterials(Info.TempMaterials);
        Interpolations.MaterialCount = 0;

        int SizeX = TerrainManager.Instance.Informations.VoxelPerChunk + 2;
        int SizeY = TerrainManager.Instance.Informations.VoxelPerChunk + 2;
        int SizeZ = TerrainManager.Instance.Informations.VoxelPerChunk + 2;

        MarchingInterpolationInfo val0 = null;

        for (int f = (SizeX * SizeY * SizeZ) - 1; f >= 0; --f)
        {
            val0 = Interpolations.Temp[f];
            if (val0.Right != null && val0.Right.VType != 0 && val0.Right.VType != Type)
            {
                val0.Enclose = false;
                continue;
            }

            if (val0.Left != null && val0.Left.VType != 0 && val0.Left.VType != Type)
            {
                val0.Enclose = false;
                continue;
            }

            if (val0.Forward != null && val0.Forward.VType != 0 && val0.Forward.VType != Type)
            {
                val0.Enclose = false;
                continue;
            }

            if (val0.Backward != null && val0.Backward.VType != 0 && val0.Backward.VType != Type)
            {
                val0.Enclose = false;
                continue;
            }

            if (val0.Top != null && val0.Top.VType != 0 && val0.Top.VType != Type)
            {
                val0.Enclose = false;
                continue;
            }

            if (val0.VType == 0)
            {
                val0.Enclose = false;
            }
            else
                val0.Enclose = true;
        }

        if (Interpolations.ActiveSubMesh[Type] != false)
        {
            (Terrain as MarchingBlock).GenerateMesh(Terrain.GetChunkWorldPosition(ChunkId), Terrain.HasChunkState(ChunkId, ChunkStates.Border), 
                Interpolations, Info, Data, true, (TerrainManager.Instance as MarchingManager).OptimizeMesh, true, Type, FInfo.GenerateUV, (TerrainManager.Instance as MarchingManager).GenerateColors, 0);
        }

        //Info.GenerateNormals();
        Data.Vertices = Info.Vertices.ToArray();
        Data.UVs = Info.Uvs.ToArray();
        Data.Indices = Info.CurrentIndices;
        Data.Normals = Info.Normals.ToArray();
        Data.Colors = Info.Colors.ToArray();

        if (!Data.IsValid())
        {
            PoolManager.UPool.CloseIndicesArray(Data.Indices);
            Data.Close();
            Data = null;
        }

        Interpolations.Close();
        Info.Close();

        return Data;
    }

    public MarchingVoxelArray GetInterpolations(ActionThread Thread, MarchingBlock Block,int ChunkId, FluidInterpolation FluidInterpolations, int PaletteCount, byte Type)
    {
        byte ValType = 0;

        int Size = TerrainManager.Instance.Informations.VoxelPerChunk + 2;

        MarchingVoxelArray Interpolations = Thread.Pool.UnityPool.MarchingFluidPool.GetData<MarchingVoxelArray>();
        Interpolations.Init(Size, Size, Size, 2, PaletteCount);
        MarchingInterpolationInfo Info = null;
        FluidTempBlock TempInfo = null;
        MarchingInformation TypeInfo = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(Type);

        int f = 0;
        for (f = (Size * Size * Size) - 1; f >= 0; --f)
        {
            Info = Interpolations.Temp[f];
            Info.State = MarchingInterpolationInfo.InterpolationState.NONE;
            Info.ClearIndices();

            TempInfo = FluidInterpolations.Temp[f];
            ValType = Info.VType = TempInfo.Type;
            Info.Border = TempInfo.Block == null;

            if (ValType == Type)
            {
                Info.Info = TypeInfo;

                if (Info.Info.VoxelType == VoxelTypes.FLUID)
                {
                    Info.VoxelType = VoxelTypes.FLUID;
                    Info.Volume = 0.5f + (float)TempInfo.Volume / (float)FluidData.MaxVolume;
                    Info.Full = true;

                    Interpolations.ActiveMaterial(ValType);
                }
            }
            else
            {
                Info.VoxelType = VoxelTypes.OPAQUE;
            }
        }

        return Interpolations;
    }

    public override void OnPreApply(FluidTempBlock Block)
    {
        // Collision over ground
        if (Block.OriginalVolume == 0)
        {
            if (OnNewBlockPctChance != 0)
            {
                if (FRandom.randomIntAbs(100) < OnNewBlockPctChance)
                {
                    //Debug.Log("NewVolume");
                    Collide(Block);
                }
            }
            else
            {
                switch (Block.Direction)
                {
                    case FluidDirections.LEFT:
                        CheckParticlesCollisions(Block, Block.Left, Block.Direction);
                        break;
                    case FluidDirections.RIGHT:
                        CheckParticlesCollisions(Block, Block.Right, Block.Direction);
                        break;
                    case FluidDirections.FORWARD:
                        CheckParticlesCollisions(Block, Block.Forward, Block.Direction);
                        break;
                    case FluidDirections.BACKWARD:
                        CheckParticlesCollisions(Block, Block.Backward, Block.Direction);
                        break;
                    case FluidDirections.BOTTOM:
                        CheckParticlesCollisions(Block, Block.Bottom, Block.Direction);
                        break;
                };
            }
        }
        else if(OnWaterCollisionPctChance != 0)// Check Collision over Fluids
        {
            switch (Block.Direction)
            {
                case FluidDirections.LEFT:
                    if (Block.Left != null && Block.Left.Direction == FluidDirections.RIGHT && Block.Left.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Left :" + Block.Left.Volume + ",Volume=" + Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
                case FluidDirections.RIGHT:
                    if (Block.Right != null && Block.Right.Direction == FluidDirections.LEFT && Block.Right.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Right :" + Block.Right.Volume + ",Volume=" + Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
                case FluidDirections.FORWARD:
                    if (Block.Forward != null && Block.Forward.Direction == FluidDirections.BACKWARD && Block.Forward.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Forward :" + Block.Forward.Volume + ",Volume=" + Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
                case FluidDirections.BACKWARD:
                    if (Block.Backward != null && Block.Backward.Direction == FluidDirections.FORWARD && Block.Backward.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Backward :" + Block.Backward.Volume + ",Volume=" + Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
                case FluidDirections.BOTTOM:
                    if (Block.Bottom != null && Block.Bottom.Direction != FluidDirections.BOTTOM && Block.Bottom.Volume > Block.Volume)
                    {
                        //Debug.Log("OverFluid Bottom :" + Block.Bottom.Volume +",Volume="+Block.Volume);
                        if (FRandom.randomIntAbs(100) < OnWaterCollisionPctChance)
                            Collide(Block);
                    }
                    break;
            };
        }
    }

    public void CheckParticlesCollisions(FluidTempBlock Block, FluidTempBlock CloseBlock, FluidDirections Direction)
    {
        if (CloseBlock == null || CloseBlock.Data == null)
            return;

        if (CloseBlock.Type != 0 && CloseBlock.Type != Block.FluidType)
        {
            if (FRandom.randomIntAbs(100) < OnGroundCollisionPctChance)
                Collide(Block);
        }
    }

    public void Collide(FluidTempBlock Block)
    {
        if (GameObjectPool.Pools.array[ObjectPool].QueueCount <= 0)
        {
            if (GameObjectPool.Pools.array[ObjectPool].CreatedObject >= MaxParticlesObjects)
                return;

            if (FluidPool.CreatedObject >= MaxParticlesObjects)
                return;
        }

        CreateGameObjectAction Action = FluidPool.GetData<CreateGameObjectAction>();
        Action.Init(CollisionParticles, TerrainManager.Instance.Informations.GetVoxelWorldPosition(Block.Block.LocalPosition, Block.x, Block.y, Block.z), Quaternion.identity, Vector3.one, RemoveTime, true);
        ActionProcessor.AddToUnity(Action);
    }

    public override void OnCascade(FluidTempBlock Fluid, int ChunkId)
    {
        float Orienation = 0;
        Fluid = Fluid.GetEmptyAround(ref Orienation);

        if (Fluid != null && WaterfallPool != -1 && Fluid.Data != null)
        {
            if (Fluid.Data != null && Fluid.Data.CascadePoints == null)
                Fluid.Data.CascadePoints = new IPoolable[TerrainManager.Instance.Informations.TotalChunksPerBlock];

            if (Fluid.Data.CascadePoints[ChunkId] == null)
                Fluid.Data.CascadePoints[ChunkId] = new WaterfallConstructor(this);

            (Fluid.Data.CascadePoints[ChunkId] as WaterfallConstructor).Register(Fluid, Orienation);
        }
    }

    public override void OnFlowDone(ActionThread Thread, FluidDatas Data, int ChunkId)
    {
        if (Data != null && Data.CascadePoints != null && Data.CascadePoints[ChunkId] != null)
            (Data.CascadePoints[ChunkId] as WaterfallConstructor).Update(this);
    }
}
