using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class MarchingCubeMenu 
{
    #region Marching Cubes

    [MenuItem("TerrainEngine/Terrains/MarchingCubes/New Terrain")]
    static public void CreateMarchingTerrain()
    {
        TerrainMenuEditor.CheckObjects();

        TerrainManager ExitingManager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (ExitingManager != null && !(ExitingManager is MarchingManager))
        {
            Debug.LogError("You can not create a new terrain. A Terrain Already exist : " + ExitingManager);
            return;
        }

        MarchingPalette Palette = GameObject.FindObjectOfType(typeof(MarchingPalette)) as MarchingPalette;
        if (Palette != null)
        {
            Debug.LogWarning("Using existing Palette :" + Palette);
        }
        else
        {
            GameObject Obj = new GameObject();
            Palette = Obj.AddComponent<MarchingPalette>();
        }

        MarchingManager Manager = GameObject.FindObjectOfType(typeof(MarchingManager)) as MarchingManager;
        if (Manager == null)
        {
            GameObject Obj = new GameObject();
            Manager = Obj.AddComponent<MarchingManager>();
        }

        Palette.name = "3-Palette";
        Manager.name = "4-TerrainManager";
        Manager.Palette = Palette;

        if (Palette.VoxelBlocks == null || Palette.VoxelBlocks.Length <= 0)
        {
            Palette.VoxelBlocks = new MarchingInformation[1];
            Palette.VoxelBlocks[0] = new MarchingInformation();
            Palette.VoxelBlocks[0].Name = "None";
        }


        if (Manager.ChunkPrefab == null)
        {
            Manager.ChunkPrefab = new GameObject();
            Manager.ChunkPrefab.name = "-ChunkPrefab";
            Manager.ChunkPrefab.transform.parent = Manager.transform;
            Manager.ChunkPrefab.layer = LayerMask.NameToLayer("Chunk");

            TerrainChunkScript Script = Manager.ChunkPrefab.AddComponent<TerrainChunkScript>();
            Script.CFilter = Manager.ChunkPrefab.AddComponent<MeshFilter>();
            Script.CRenderer = Manager.ChunkPrefab.AddComponent<MeshRenderer>();
            Script.CCollider = Manager.ChunkPrefab.AddComponent<MeshCollider>();
        }
    }

    #endregion
}
