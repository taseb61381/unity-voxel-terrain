using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class MiniMapProcessor : IBlockProcessor 
{
    public class TerrainMinimap
    {
        public TerrainMinimap(TerrainBlock Terrain)
        {
            this.Position = Terrain.LocalPosition;
            this.IsValid = true;
            this.HasMinimap = false;
        }
        public VectorI3 Position;
        public TerrainBlock Terrain;
        public IMeshData Data;
        public GameObject Minimap;
        public bool HasMinimap;
        public bool IsValid;
        public bool IsGenerating;
    }

    public Dictionary<VectorI3, TerrainMinimap> Minimaps = new Dictionary<VectorI3, TerrainMinimap>(new VectorI3Comparer());
    public Queue<TerrainMinimap> ToDelete = new Queue<TerrainMinimap>();
    public Queue<TerrainMinimap> ToGenerate = new Queue<TerrainMinimap>();
    public bool GenerateBorders = true;

    public TerrainMinimap GetMinimap(TerrainBlock Terrain, bool Create)
    {
        TerrainMinimap Minimap = null;

        lock (Minimaps)
        {
            if (!Minimaps.TryGetValue(Terrain.LocalPosition, out Minimap) && Create)
            {
                Minimap = new TerrainMinimap(Terrain);
                Minimaps.Add(Terrain.LocalPosition, Minimap);
            }
        }

        return Minimap;
    }

    public TerrainObject PlayerTObject;
    public Camera MinimapCamera;
    public FollowTransformScript Follower;
    public RenderTexture RTexture;

    public bool AllMinimapLoaded = false;

    void Start()
    {
        EnableThreadUpdateFunction = true;
        AllMinimapLoaded = false;

        ActionProcessor.AddToThread(new ExecuteAction("MapGeneration", UpdateGeneratingMaps));
    }

    public override void OnTerrainLoadingDone(TerrainBlock Terrain)
    {
        AllMinimapLoaded = false;
    }

    public override void OnTerrainUnityClose(TerrainBlock Terrain)
    {
        TerrainMinimap Minimap = GetMinimap(Terrain, false);

        if (Minimap != null)
            ToDelete.Enqueue(Minimap);

        AllMinimapLoaded = false;
    }

    public bool UpdateGeneratingMaps(ActionThread Thread)
    {        
        TerrainMinimap Minimap = null;

        lock (ToGenerate)
            if (ToGenerate.Count != 0)
                Minimap = ToGenerate.Dequeue();

        if (Minimap == null)
            return false;

        VoxelBlock Block = Minimap.Terrain as VoxelBlock;
        if (Minimap.Terrain == null)
        {
            Debug.LogError("Invalid Minimap Terrain");
            return false;
        }

        if (Minimap == null || Minimap.HasMinimap)
            return false;

        Minimap.HasMinimap = true;
        Debug.Log("Generating Minimap :" + Block.WorldPosition);

        IMeshData Data = PoolManager.UPool.GetSimpleMeshData();
        TempMeshData Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();

        VoxelTempData Datas = Block.GetTempData(Thread, 0, 0, 0, TerrainManager.Instance.Informations.VoxelsPerAxis, TerrainManager.Instance.Informations.VoxelsPerAxis, TerrainManager.Instance.Informations.VoxelsPerAxis, 2);
        VoxelBlock.Generate(Block, Info, Data, Datas, Block.WorldVoxel.x, Block.WorldVoxel.y, Block.WorldVoxel.z, TerrainManager.Instance.Informations.VoxelsPerAxis, TerrainManager.Instance.Informations.VoxelsPerAxis, TerrainManager.Instance.Informations.VoxelsPerAxis, 2, !GenerateBorders);
        if (Data.IsValid())
            Minimap.Data = Data;
        else
        {
            Minimap.Data = null;
            Data.Close();
        }

        Datas.Close();
        Info.Close();
        return false;
    }

    void Update()
    {
        if (ToDelete.Count != 0)
        {
            TerrainMinimap Minimap = ToDelete.Dequeue();

            Debug.Log("Closing Minimap :" + Minimap.Terrain.WorldPosition);

            if (Minimap.Data != null)
                Minimap.Data.Close();

            lock (Minimaps)
                Minimaps.Remove(Minimap.Position);

            if (Minimap.Minimap != null)
                GameObject.Destroy(Minimap.Minimap);

            Minimap.HasMinimap = false;

            return;
        }

        if (!ITerrainCameraHandler.Instance.IsUsingCamera() || PlayerTObject == null || PlayerTObject.BlockObject == null || AllMinimapLoaded)
            return;

        Follower.ToFollow = ITerrainCameraHandler.Instance.GetMainCameraTObject().CTransform;

        AllMinimapLoaded = true;
        TerrainBlock Block = null;
        for (int x = 0; x < 3; ++x)
        {
            for (int y = 0; y < 3; ++y)
            {
                for (int z = 0; z < 3; ++z)
                {
                    Block = PlayerTObject.BlockObject[x, y, z] as TerrainBlock;
                    if (Block == null)
                        continue;

                    TerrainMinimap Minimap = GetMinimap(Block, true);
                    Minimap.Terrain = Block;

                    if (!Minimap.HasMinimap)
                    {
                        if (!Minimap.IsGenerating)
                        {
                            Debug.Log("Adding Minimap Terrain :" + Minimap.Terrain);
                            Minimap.IsGenerating = true;

                            lock (ToGenerate)
                                ToGenerate.Enqueue(Minimap);
                        }

                        AllMinimapLoaded = false;
                    }

                    Minimap.IsValid = true;
                    if (Minimap.Minimap != null || Minimap.Data == null)
                        continue;

                    Minimap.IsGenerating = false;
                    Minimap.HasMinimap = true;
                    Minimap.Minimap = new GameObject("Minimap :" + Block.WorldPosition);
                    Minimap.Minimap.layer = LayerMask.NameToLayer("Minimap");
                    Minimap.Minimap.transform.position = Block.WorldPosition;
                    MeshRenderer CRenderer = Minimap.Minimap.AddComponent<MeshRenderer>();
                    MeshFilter CFilter = Minimap.Minimap.AddComponent<MeshFilter>();
                    Mesh CMesh = CFilter.mesh;

                    Minimap.Data.Apply(CMesh);

                    CRenderer.sharedMaterials = (TerrainManager.Instance as VoxelManager).WorldSharedMaterial;
                    CMesh.RecalculateNormals();

                    Minimap.Data.Close();
                    Minimap.Data = null;
                }
            }
        }

        foreach (KeyValuePair<VectorI3, TerrainMinimap> Kp in Minimaps)
            if (Kp.Value.IsValid == false)
                ToDelete.Enqueue(Kp.Value);
            else
                Kp.Value.IsValid = false;
    }
}
