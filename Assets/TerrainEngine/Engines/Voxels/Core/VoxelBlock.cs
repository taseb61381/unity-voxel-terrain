﻿using System;
using System.Collections;

using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class VoxelBlock : TerrainBlock
    {
        public VoxelBlock(TerrainManager Manager)
            : base(Manager.Informations)
        {

        }

        public byte GetVoxelTemp(ref VoxelTempInfo Info, int x, int y, int z, bool Safe)
        {
            if (Safe)
            {
                Info.Type = Voxels.GetType(x, y, z);
                return Info.Type;
            }

            ITerrainBlock AroundBlock = GetAroundBlock(ref x, ref y, ref z);
            if (AroundBlock == null)
            {
                Info.Border = true;
                return 0;
            }

            Info.Type = AroundBlock.Voxels.GetType(x, y, z);
            return Info.Type;
        }


        public VoxelTempData GetTempData( ActionThread Thread,  int StartX, int StartY, int StartZ, int SizeX, int SizeY, int SizeZ, int LodSize)
        {
            bool sx, sy, sz = false;
            int px, py, pz = 0;

            VoxelTempData Datas = Thread.Pool.GetData<VoxelTempData>("VoxelTempData" + LodSize);
            VoxelTempInfo Info = null;
            Datas.Init((SizeX / LodSize) + 2, (SizeY / LodSize) + 2, (SizeZ / LodSize) + 2);
            BlockInformation LastBlockInfo = null;
            byte LastBlockType = 0;

            Datas.IsEmpty = true;
            Datas.IsFull = true;

            int x, y, z;
            for (x = 0; x < (SizeX / LodSize) + 2; ++x)
            {
                px = StartX + (x - 1) * LodSize;
                sx = Voxels.IsValid(px);

                for (y = 0; y < (SizeY / LodSize)+ 2; ++y)
                {
                    py = StartY + (y - 1) * LodSize;
                    sy = Voxels.IsValid(py);

                    for (z = 0; z < (SizeZ / LodSize) + 2; ++z)
                    {
                        pz = StartZ + (z - 1) * LodSize;
                        sz = Voxels.IsValid(pz);

                        Info = Datas[x, y, z];
                        Info.Border = false;

                        if (GetVoxelTemp(ref Info, px, py, pz, (sx && sy && sz)) != 0)
                        {
                            if (x > 0 && y > 0 && z > 0 && x <= SizeX && y <= SizeY && z <= SizeZ)
                                Datas.IsEmpty = false;

                            if (Info.Type == LastBlockType)
                                Info.Info = LastBlockInfo;
                            else
                            {
                                Info.Info = TerrainManager.Instance.Palette.GetVoxelData<BlockInformation>(Info.Type);
                                LastBlockInfo = Info.Info;
                                LastBlockType = Info.Type;

                                if (LastBlockInfo.VoxelType != VoxelTypes.OPAQUE)
                                    Datas.IsFull = false;
                            }
                        }
                        else
                        {
                            Info.Type = 0;
                            Info.Info = null;
                            Datas.IsFull = false;
                        }
                    }
                }
            }

            return Datas;
        }

        public override VectorI3 GetVoxelFromWorldPosition(Vector3 WorldPosition, bool Remove)
        {
            /*if (Remove)
                WorldPosition -= Normal * Information.Scale * 0.5f;
            else
                WorldPosition += Normal * Information.Scale * 0.5f;
            */
            Vector3 blockPos = (WorldPosition - this.WorldPosition) / Information.Scale;
            blockPos.x += 1;
            blockPos.z += 1;
            blockPos.y += 1;
            return blockPos;
        }

        public override void Generate(ActionThread Thread,ref TempMeshData Info,ref IMeshData Data, int ChunkId)
        {
            Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
            Data = PoolManager.UPool.GetSimpleMeshData();
            Data.Materials = (TerrainManager.Instance as VoxelManager).WorldSharedMaterial;

            int SizeX = Information.VoxelPerChunk;
            int SizeY = Information.VoxelPerChunk;
            int SizeZ = Information.VoxelPerChunk;

            TerrainChunk Chunk = Chunks[ChunkId];
            VoxelTempData Datas = GetTempData(Thread, Chunk.StartX, Chunk.StartY, Chunk.StartZ, SizeX, SizeY, SizeZ, 1);
            VoxelBlock.Generate(this, Info, Data, Datas, GetWorldVoxelX(Chunk.StartX), GetWorldVoxelY(Chunk.StartY), GetWorldVoxelZ(Chunk.StartZ), SizeX, SizeY, SizeZ, 1, TerrainManager.Instance.OptimizeBlockBorders);
            Datas.Close();
        }

        public override float InterpolateHeight(byte CurrentType, float TopVolume, float CurrentVolume)
        {
            return 0;
        }

        public override void GetPositionOffset(ref float x, ref float y, ref float z)
        {
            x -= 0.5f * Information.Scale;
            z -= 0.5f * Information.Scale;
        }

        public IEnumerable BlockLoopNoAir(TerrainChunk Chunk)
        {
            int x, y, z;
            for (x = 0; x < Information.VoxelPerChunk; x++)
                for (y = 0; y < Information.VoxelPerChunk; y++)
                    for (z = 0; z < Information.VoxelPerChunk; z++)
                    {
                        if (Voxels.GetType(x + Chunk.StartX, y + Chunk.StartY, z + Chunk.StartZ) == BlockManager.None)
                            continue;

                        yield return new VectorI3(x, y, z);
                    }
        }

        public IEnumerable BlockFaceLoop(VectorI3 index)
        {
            for (int f = 0; f < BlockHelper.facescount; f++)
            {
                if (!BlockFaceCheckBounds(index, f)) continue;

                yield return f;
            }
        }

        public bool BlockFaceCheckBounds(VectorI3 index, int face)
        {
            return GetNeighbor(index.x, index.y, index.z, face) == 0;
        }

        public byte GetNeighbor(int x, int y, int z, int directionIndex, bool Safe = false)
        {
            return GetByte(x + BlockHelper.direction[directionIndex].x,
                y + BlockHelper.direction[directionIndex].y,
                z + BlockHelper.direction[directionIndex].z, Safe);
        }

        public bool IsSafe(int px, int py, int pz)
        {
            return (px < Information.VoxelPerChunk - 1 && px >= 1)
                    && (py < Information.VoxelPerChunk - 1 && py >= 1)
                    && (pz < Information.VoxelPerChunk - 1 && pz >= 1);
        }

        public bool[] GetNeighbors(VectorI3 index)
        {
            bool[] neighbors = new bool[26];
            bool Safe = IsSafe(index.x, index.y, index.z);

            for (int i = 0; i < 26; i++)
            {
                if (GetNeighbor(index.x, index.y, index.z, i, Safe) != 0)
                    neighbors[i] = true;
            }

            return neighbors;
        }

        static public void Generate(TerrainBlock Block, TempMeshData Info, IMeshData IData, VoxelTempData Datas, int StartX, int StartY, int StartZ, int SizeX, int SizeY, int SizeZ, int LodSize, bool OptimizeBorder=true)
        {
            if (!Datas.IsFull && !Datas.IsEmpty)
            {
                VoxelTempInfo VoxelInfo = null;
                VoxelTempInfo AroundInfo = null;
                ColorUVMeshData Data = IData as ColorUVMeshData;

                byte BlockType = 0;
                bool Safe = false;
                BlockInformation BlockInfo = null;
                Color[] ambientColors = new Color[5];
                int i, g, f, foundNeighbors = 0;
                int px, py, pz;
                int x, y, z;
                BlockHelper.BlockFace Face;

                for (x = 0; x < (SizeZ / LodSize); x++)
                {
                    px = StartX + (x - 1) * LodSize;
                    for (y = 0; y < (SizeY / LodSize); y++)
                    {
                        py = StartY + (y - 1) * LodSize;
                        for (z = 0; z < (SizeX / LodSize); z++)
                        {
                            pz = StartZ + (z - 1) * LodSize;
                            VoxelInfo = Datas[x + 1, y + 1, z + 1];

                            BlockType = VoxelInfo.Type;

                            if ((BlockType = VoxelInfo.Type) == 0)
                                continue;

                            foundNeighbors = 0;
                            BlockInfo = VoxelInfo.Info;

                            if (BlockInfo == null || BlockInfo.VoxelType == VoxelTypes.FLUID)
                                continue;

                            for (i = 0; i < BlockHelper.facescount; ++i)
                            {
                                AroundInfo = VoxelInfo.Arounds[i];
                                if (AroundInfo.Type != BlockManager.None && AroundInfo.Info.VoxelType != VoxelTypes.FLUID)
                                {
                                    ++foundNeighbors;
                                    Datas.Neighbors[i] = true;
                                }
                                else
                                    Datas.Neighbors[i] = false;
                            }

                            if (foundNeighbors == 0) continue;

                            Safe = false;

                            for (f = BlockHelper.facescount-1; f >= 0; --f)
                            {
                                if (Datas.Neighbors[f] == true)
                                    continue;

                                if (OptimizeBorder && VoxelInfo.Arounds[f].Border)
                                    continue;

                                if (Safe == false)
                                {
                                    Safe = true;

                                    for (i = BlockHelper.facescount; i < 26; ++i)
                                    {
                                        AroundInfo = VoxelInfo.Arounds[i];
                                        if (AroundInfo.Type != 0 && AroundInfo.Info.VoxelType != VoxelTypes.FLUID)
                                            Datas.Neighbors[i] = true;
                                        else
                                            Datas.Neighbors[i] = false;
                                    }
                                }

                                VoxelAmbient.ChangeLight(ref ambientColors, f, Datas.Neighbors);
                                Face = BlockHelper.facesEnum[f];

                                for (g = BlockInfo.ColorCount-1; g >= 0; --g)
                                {
                                    if (BlockInfo.ColorGenerators[g].IsAllowedFace(Face))
                                    {
                                        ambientColors[0] = BlockInfo.ColorGenerators[g].GenerateColor(px, py, pz, f, ambientColors[0]);
                                        ambientColors[0].a = 1;

                                        ambientColors[1] = BlockInfo.ColorGenerators[g].GenerateColor(px, py, pz, f, ambientColors[1]);
                                        ambientColors[1].a = 1;

                                        ambientColors[2] = BlockInfo.ColorGenerators[g].GenerateColor(px, py, pz, f, ambientColors[2]);
                                        ambientColors[2].a = 1;

                                        ambientColors[3] = BlockInfo.ColorGenerators[g].GenerateColor(px, py, pz, f, ambientColors[3]);
                                        ambientColors[3].a = 1;

                                        ambientColors[4] = BlockInfo.ColorGenerators[g].GenerateColor(px, py, pz, f, ambientColors[4]);
                                        ambientColors[4].a = 1;
                                    }
                                }

                                AddFace(Block, BlockType, x, y, z, f,(int)Face, ref Info, (float)(Block.Information.Scale * LodSize) + 0.0001f);

                                if (ambientColors != null)
                                    Info.Colors.AddRange(ambientColors);
                            }
                        }
                    }
                }

                if (Info.Triangles.Indices.Count > 0)
                {
                    Info.CurrentIndices = new int[1][];
                    Info.CurrentIndices[0] = Info.Triangles.Indices.ToArray();
                    Info.GenerateNormals();

                    Data.CColors = Info.Colors.ToArray();
                    Data.CNormals = Info.Normals.ToArray();
                    Data.CIndices = Info.CurrentIndices;
                    Data.CUVs = Info.Uvs.ToArray();
                    Data.CVertices = Info.Vertices.ToArray();
                    Data.CMaterials = (TerrainManager.Instance as VoxelManager).WorldSharedMaterial;
                }
            }
        }

        static public void AddFace(TerrainBlock Block, byte BlockType, int x, int y, int z, int f,int Face, ref TempMeshData Info, float Scale, float Height = 0, float Offset=0)
        {
            Vector3 midvertex = new Vector3();
            int i = 0;
            Info.Vertices.CheckArray(4);
            Vector3 vertex;
            Height *= Scale;
            Offset *= Scale;
            Vector3[] vertices = BlockHelper.faceVertices[f];
            for (i = 0; i < BlockHelper.verticescount - 1; ++i)
            {
                vertex = vertices[i] * Scale;
                vertex.x += (x - 1) * Scale;
                vertex.y += (y - 1) * Scale;
                vertex.z += (z - 1) * Scale;

                if (Height != 0 && f != 3)
                {
                    if (i == 0 || i == 1)
                        vertex.y -= Height;
                    else if ((i == 2 || i == 3) && f == 2)
                        vertex.y -= Height;

                    if (i > 1 && f != 2)
                        vertex.y += Offset;
                }

                midvertex += vertex;
                Info.Vertices.AddSafe(vertex);
            }

            midvertex *= 0.25f;
            Info.Vertices.Add(midvertex);

            Info.Triangles.Indices.CheckArray(12);
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[0] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[1] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[2] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[3] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[4] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[5] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[6] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[7] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[8] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[9] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[10] + Info.TriangleIndex));
            Info.Triangles.Indices.AddSafe((BlockHelper.triangles[11] + Info.TriangleIndex));
            x = Face;

            Info.Uvs.CheckArray(5);
            Info.Uvs.AddSafe(TerrainManager.Instance.Palette.GetUv((int)BlockType, x, 0));
            Info.Uvs.AddSafe(TerrainManager.Instance.Palette.GetUv((int)BlockType, x, 1));
            Info.Uvs.AddSafe(TerrainManager.Instance.Palette.GetUv((int)BlockType, x, 2));
            Info.Uvs.AddSafe(TerrainManager.Instance.Palette.GetUv((int)BlockType, x, 3));
            Info.Uvs.AddSafe(TerrainManager.Instance.Palette.GetUv((int)BlockType, x, 4));

            Info.TriangleIndex += BlockHelper.verticescount;
        }
    }
}
