﻿using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    static public class VoxelAmbient
    {
        static public Color ambient = new Color(0.2f, 0.2f, 0.3f);
        static public Color nonAmbient = new Color(1, 1, 1);

        static public void GenerateAmbientLight(VoxelBlock Block, TerrainChunk chunk)
        {
            List<Color> colors = new List<Color>();

            Color[] cols = new Color[5];
            foreach (VectorI3 index in Block.BlockLoopNoAir(chunk))
            {
                VectorI3 blockIndex = index + new VectorI3(chunk.StartX, chunk.StartY, chunk.StartZ);

                bool[] neighbors = Block.GetNeighbors(blockIndex);

                foreach (int f in Block.BlockFaceLoop(blockIndex))
                {
                    colors.AddRange(ChangeLight(ref cols, f, neighbors));
                }
            }
        }

        static public Color[] ChangeLight(ref Color[] colors, int faceIndex, bool[] neighbors)
        {
            Color midColor = Color.black;
            Color color = Color.white;

            for (int i = 3; i >= 0; --i)
            {
                color = Color.white;

                //Light inside edge
                switch (faceIndex)
                {
                    case (int)BlockHelper.Direction.Left: color = GetAmbientLightLeft(i, neighbors); break;
                    case (int)BlockHelper.Direction.Right: color = GetAmbientLightRight(i, neighbors); break;
                    case (int)BlockHelper.Direction.Top: color = GetAmbientLightTop(i, neighbors); break;
                    case (int)BlockHelper.Direction.Bottom: color = GetAmbientLightBottom(i, neighbors); break;
                    case (int)BlockHelper.Direction.Front: color = GetAmbientLightFront(i, neighbors); break;
                    case (int)BlockHelper.Direction.Back: color = GetAmbientLightBack(i, neighbors); break;
                }

                color.a = 1;
                colors[i] = color;
                midColor.r += color.r;
                midColor.g += color.g;
                midColor.b += color.b;
            }

            midColor *= 0.25f;
            midColor.a = 1;

            if (BlockHelper.verticescount > 4)
            {
                colors[4] = midColor;
            }

            return colors;
        }

        static Color GetAmbientLightLeft(int index, bool[] neighbors)
        {
            switch (index)
            {
                case 0:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftFront] ||
                        neighbors[(int)BlockHelper.Direction.LeftTop] ||
                        neighbors[(int)BlockHelper.Direction.LeftTopFront]
                        ) { return ambient; } break;
                case 1:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftBack] ||
                        neighbors[(int)BlockHelper.Direction.LeftTop] ||
                        neighbors[(int)BlockHelper.Direction.LeftTopBack]
                        ) { return ambient; } break;
                case 2:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftFront] ||
                        neighbors[(int)BlockHelper.Direction.LeftBottom] ||
                        neighbors[(int)BlockHelper.Direction.LeftBottomFront]
                        ) { return ambient; } break;
                case 3:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftBack] ||
                        neighbors[(int)BlockHelper.Direction.LeftBottom] ||
                        neighbors[(int)BlockHelper.Direction.LeftBottomBack]
                        ) { return ambient; } break;
            }

            return nonAmbient;
        }

        static Color GetAmbientLightRight(int index, bool[] neighbors)
        {
            switch (index)
            {
                case 0:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightBack] ||
                        neighbors[(int)BlockHelper.Direction.RightTop] ||
                        neighbors[(int)BlockHelper.Direction.RightTopBack]
                        ) { return ambient; } break;
                case 1:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightFront] ||
                        neighbors[(int)BlockHelper.Direction.RightTop] ||
                        neighbors[(int)BlockHelper.Direction.RightTopFront]
                        ) { return ambient; } break;
                case 2:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightBack] ||
                        neighbors[(int)BlockHelper.Direction.RightBottom] ||
                        neighbors[(int)BlockHelper.Direction.RightBottomBack]
                        ) { return ambient; } break;

                case 3:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightFront] ||
                        neighbors[(int)BlockHelper.Direction.RightBottom] ||
                        neighbors[(int)BlockHelper.Direction.RightBottomFront]
                        ) { return ambient; } break;

            }

            return nonAmbient;
        }

        static Color GetAmbientLightTop(int index, bool[] neighbors)
        {
            switch (index)
            {
                case 0:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftTop] ||
                        neighbors[(int)BlockHelper.Direction.TopFront] ||
                        neighbors[(int)BlockHelper.Direction.LeftTopFront]
                        ) { return ambient; } break;
                case 1:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightTop] ||
                        neighbors[(int)BlockHelper.Direction.TopFront] ||
                        neighbors[(int)BlockHelper.Direction.RightTopFront]
                        ) { return ambient; } break;
                case 2:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftTop] ||
                        neighbors[(int)BlockHelper.Direction.TopBack] ||
                        neighbors[(int)BlockHelper.Direction.LeftTopBack]
                        ) { return ambient; } break;
                case 3:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightTop] ||
                        neighbors[(int)BlockHelper.Direction.TopBack] ||
                        neighbors[(int)BlockHelper.Direction.RightTopBack]
                        ) { return ambient; } break;
            }

            return nonAmbient;
        }

        static Color GetAmbientLightBottom(int index, bool[] neighbors)
        {
            switch (index)
            {
                case 0:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftBottom] ||
                        neighbors[(int)BlockHelper.Direction.BottomBack] ||
                        neighbors[(int)BlockHelper.Direction.LeftBottomBack]
                        ) { return ambient; } break;
                case 1:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightBottom] ||
                        neighbors[(int)BlockHelper.Direction.BottomBack] ||
                        neighbors[(int)BlockHelper.Direction.RightBottomBack]
                        ) { return ambient; } break;
                case 2:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftBottom] ||
                        neighbors[(int)BlockHelper.Direction.BottomFront] ||
                        neighbors[(int)BlockHelper.Direction.LeftBottomFront]
                        ) { return ambient; } break;
                case 3:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightBottom] ||
                        neighbors[(int)BlockHelper.Direction.BottomFront] ||
                        neighbors[(int)BlockHelper.Direction.RightBottomFront]
                        ) { return ambient; } break;
            }

            return nonAmbient;
        }

        static Color GetAmbientLightFront(int index, bool[] neighbors)
        {
            switch (index)
            {
                case 0:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightFront] ||
                        neighbors[(int)BlockHelper.Direction.TopFront] ||
                        neighbors[(int)BlockHelper.Direction.RightTopFront]
                        ) { return ambient; } break;
                case 1:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftFront] ||
                        neighbors[(int)BlockHelper.Direction.TopFront] ||
                        neighbors[(int)BlockHelper.Direction.LeftTopFront]
                        ) { return ambient; } break;
                case 2:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightFront] ||
                        neighbors[(int)BlockHelper.Direction.BottomFront] ||
                        neighbors[(int)BlockHelper.Direction.RightBottomFront]
                        ) { return ambient; } break;
                case 3:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftFront] ||
                        neighbors[(int)BlockHelper.Direction.BottomFront] ||
                        neighbors[(int)BlockHelper.Direction.LeftBottomFront]
                        ) { return ambient; } break;
            }

            return nonAmbient;
        }

        static Color GetAmbientLightBack(int index, bool[] neighbors)
        {
            switch (index)
            {
                case 0:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftBack] ||
                        neighbors[(int)BlockHelper.Direction.TopBack] ||
                        neighbors[(int)BlockHelper.Direction.LeftTopBack]
                        ) { return ambient; } break;
                case 1:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightBack] ||
                        neighbors[(int)BlockHelper.Direction.TopBack] ||
                        neighbors[(int)BlockHelper.Direction.RightTopBack]
                        ) { return ambient; } break;
                case 2:
                    if (
                        neighbors[(int)BlockHelper.Direction.LeftBack] ||
                        neighbors[(int)BlockHelper.Direction.BottomBack] ||
                        neighbors[(int)BlockHelper.Direction.LeftBottomBack]
                        ) { return ambient; } break;
                case 3:
                    if (
                        neighbors[(int)BlockHelper.Direction.RightBack] ||
                        neighbors[(int)BlockHelper.Direction.BottomBack] ||
                        neighbors[(int)BlockHelper.Direction.RightBottomBack]
                        ) { return ambient; } break;
            }

            return nonAmbient;
        }
    }
}