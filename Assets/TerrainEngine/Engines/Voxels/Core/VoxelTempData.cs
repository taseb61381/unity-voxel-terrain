﻿
using UnityEngine;

namespace TerrainEngine
{
    public class VoxelTempInfo
    {
        public byte Type;
        public Vector3 p;
        public bool Border;
        public BlockInformation Info;
        public VoxelTempInfo[] Arounds;

        public VoxelTempInfo(int x,int y,int z)
        {
            Arounds = new VoxelTempInfo[26];
        }
    }

    public class VoxelTempData : IPoolable
    {
        public int SizeX;
        public int SizeY;
        public int SizeZ;
        public Color[] _tempColor;
        public VoxelTempInfo[][][] Temp;
        public bool[] Neighbors;
        public bool IsFull = false;
        public bool IsEmpty = false;

        public void Init(int SizeX, int SizeY, int SizeZ)
        {
            if (Temp == null)
            {
                this.SizeX = SizeX;
                this.SizeY = SizeY;
                this.SizeZ = SizeZ;
                this.Neighbors = new bool[26];
                this._tempColor = new Color[5];
                InitArray();
            }

            IsFull = false;
            IsEmpty = false;
        }

        public void InitArray()
        {
            Temp = new VoxelTempInfo[SizeX][][];
            for (int x = 0; x < SizeX; ++x)
            {
                Temp[x] = new VoxelTempInfo[SizeY][];
                for (int y = 0; y < SizeY; ++y)
                {
                    Temp[x][y] = new VoxelTempInfo[SizeZ];
                    for (int z = 0; z < SizeZ; ++z)
                    {
                        Temp[x][y][z] = new VoxelTempInfo(x, y, z);
                    }
                }
            }

            int ax = 0;
            int ay = 0;
            int az = 0;

            VoxelTempInfo Info = null;
            for (int x = 0; x < SizeX; ++x)
                for (int y = 0; y < SizeY; ++y)
                    for (int z = 0; z < SizeZ; ++z)
                    {
                        Info = Temp[x][y][z];
                        for (int i = 0; i < 26; ++i)
                        {
                            ax = BlockHelper.direction[i].x;
                            if(ax + x < 0) continue;
                            else if(ax + x >= SizeX) continue;

                            ay = BlockHelper.direction[i].y;
                            if(ay + y < 0) continue;
                            else if(ay + y >= SizeY) continue;

                            az = BlockHelper.direction[i].z;
                            if(az + z < 0) continue;
                            else if(az + z >= SizeZ) continue;


                            Info.Arounds[i] = Temp[ax + x][ay + y][az + z];
                        }
                    }
        }

        public VoxelTempInfo this[int x, int y, int z]
        {
            get
            {
                return Temp[x][y][z];
            }
            set
            {
                Temp[x][y][z] = value;
            }
        }

        public override void Clear()
        {

        }
    }
}
