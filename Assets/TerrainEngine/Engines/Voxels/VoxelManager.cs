﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class VoxelManager : TerrainManager
{
    [HideInInspector]
    public Rect[] WorldTextureAtlasUvs;

    [HideInInspector]
    public Texture2D WorldTextureAtlas;

    public Shader WorldTextureShader;
    public Shader WorldTextureShaderAlpha;

    [HideInInspector]
    public Material WorldTextureMaterial;

    [HideInInspector]
    public Material WorldTextureMaterialAlpha;

    [HideInInspector]
    public Material[] WorldSharedMaterial;

    public GameObject ColorGameObject;

    public override bool InitTextures()
    {
        if (!base.InitTextures())
            return false;

        IColorGenerator[] Colors = null;

        if(ColorGameObject != null)
            Colors = ColorGameObject.GetComponentsInChildren<IColorGenerator>();

        (Palette as VoxelPalette).BlockUvCoordinates = new BlockUVCoordinates[Palette.GetDataCount()];
        List<Texture2D> Textures = new List<Texture2D>();
        int TextureID = 0;
        BlockInformation Data = null;

        for (int i = 0; i < Palette.GetDataCount(); ++i)
        {
            Data = Palette.GetVoxelData<BlockInformation>((byte)i);

            if (Data == null)
                continue;
            else
            {
                Data.Type = i;

                if (Data.VoxelType == VoxelTypes.FLUID)
                {
                    if (Data.FluidMaterial == null)
                    {
                        Data.FluidMaterial = new Material(WorldTextureShaderAlpha);
                        Data.FluidMaterial.mainTexture = Data.Top;
                    }
                    (Palette as VoxelPalette).BlockUvCoordinates[Data.Type] = new BlockUVCoordinates(new Rect(0, 0, 1, 1), new Rect(0, 0, 1, 1), new Rect(0, 0, 1, 1), 1f / Data.Top.width, 1f / Data.Top.height);
                    continue;
                }

                if (Colors != null && Data.ColorGenerators.Length <= 0)
                {
                    for (int c = 0; c < Colors.Length; ++c)
                    {
                        if (Data.Name.ToLower() == Colors[c].Name.ToLower())
                            Data.ColorGenerators = new IColorGenerator[1] { Colors[c] };
                    }
                }

                if (Data.Top != null)
                {
                    if (!Textures.Contains(Data.Top))
                    {
                        Textures.Add(Data.Top);
                        Data.TopID = TextureID;
                        ++TextureID;
                    }
                    else
                        Data.TopID = Textures.IndexOf(Data.Top);
                }

                if (Data.Bottom != null)
                {
                    if (!Textures.Contains(Data.Bottom))
                    {
                        Textures.Add(Data.Bottom);
                        Data.BottomID = TextureID;
                        ++TextureID;
                    }
                    else
                        Data.BottomID = Textures.IndexOf(Data.Bottom);
                }

                if (Data.Side != null)
                {
                    if (!Textures.Contains(Data.Side))
                    {
                        Textures.Add(Data.Side);
                        Data.SideID = TextureID;
                        ++TextureID;
                    }
                    else
                        Data.SideID = Textures.IndexOf(Data.Side);
                }
            }
        }

        WorldTextureAtlas = new Texture2D(2, 2);
        WorldTextureAtlasUvs = WorldTextureAtlas.PackTextures(Textures.ToArray(), 0);
        WorldTextureAtlas.filterMode = FilterMode.Trilinear;
        WorldTextureAtlas.anisoLevel = 1;
        WorldTextureAtlas.Apply();

        float EpsilonX = 0.02f;
        float EpsilonY = 0.02f;

        for (int i = 0; i < Palette.GetDataCount(); ++i)
        {
            Data = Palette.GetVoxelData<BlockInformation>((byte)i);
            if (Data == null)
                continue;
            else
            {
                Rect Top = WorldTextureAtlasUvs[Data.TopID];
                Rect Bottom = WorldTextureAtlasUvs[Data.BottomID];
                Rect Side = WorldTextureAtlasUvs[Data.SideID];

                (Palette as VoxelPalette).BlockUvCoordinates[Data.Type] = new BlockUVCoordinates(Top, Side, Bottom, EpsilonX, EpsilonY);
            }
        }

        if (WorldTextureShader != null)
        {
            WorldTextureMaterial = new Material(WorldTextureShader);
            WorldTextureMaterial.mainTexture = WorldTextureAtlas;
        }

        if (WorldTextureShaderAlpha != null)
        {
            WorldTextureMaterialAlpha = new Material(WorldTextureShaderAlpha);
            WorldTextureMaterialAlpha.mainTexture = WorldTextureAtlas;
        }

        WorldSharedMaterial = new Material[1] { WorldTextureMaterial };

        return true;
    }

    public override TerrainBlock CreateTerrain()
    {
        return new VoxelBlock(this);
    }
}
