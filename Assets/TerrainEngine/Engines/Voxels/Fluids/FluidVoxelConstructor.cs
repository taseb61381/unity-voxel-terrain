﻿
using TerrainEngine;
using UnityEngine;

public class FluidVoxelConstructor : FluidMeshConstructor
{
    public override IMeshData GenerateMesh(ActionThread Thread, TerrainBlock Terrain, int ChunkId, FluidDatas FData, FluidInterpolation FluidInterpolations, FluidInformation FInfo, byte Type)
    {
        TempMeshData Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
        IMeshData Data = PoolManager.UPool.GetSimpleMeshData();

        Data.Materials = new Material[1];
        Data.Indices = new int[1][];

        byte BlockId = Type;
        BlockInformation BlockInfo = TerrainManager.Instance.Palette.GetVoxelData<BlockInformation>(BlockId);
        if (BlockInfo == null)
            return null;

        if (Data.Materials == null)
            return null;

        Data.Materials[0] = BlockInfo.FluidMaterial;

        byte AroundType = 0;
        int AroundLevel = 0;
        int cellLevel=0;

        int SizeX = TerrainManager.Instance.Informations.VoxelPerChunk;
        int SizeY = TerrainManager.Instance.Informations.VoxelPerChunk;
        int SizeZ = TerrainManager.Instance.Informations.VoxelPerChunk;
        FluidTempBlock Temp,TempAround;
        int x, y, z, f;
        float Size, MaxVolume = (float)FluidData.MaxVolume, AroundSize = 0;

        for (y = 0; y < SizeY; ++y)
        {
            for (x = 0; x < SizeX; ++x)
            {
                for (z = 0; z < SizeZ; ++z)
                {
                    Temp = FluidInterpolations[x + 1,y + 1,z + 1];
                    if (Temp.Volume > 0 && Temp.Type == BlockId)
                    {
                        if (FluidInterpolations[x + 1,y + 2,z + 1].Type == BlockId)
                        {
                            //if (Temp.Bottom != null && (Temp.Bottom.Type == 0 || (Temp.Bottom.Type == Temp.Type && Temp.Bottom.Volume < FluidData.MaxVolume)))
                                Temp.Volume = FluidData.MaxVolume;
                        }

                    }
                }
            }
        }

        VectorI3 Dir;
        int Face;
        for (y = 0; y < SizeY; ++y)
        {
            for (x = 0; x < SizeX; ++x)
            {
                for (z = 0; z < SizeZ; ++z)
                {
                    Temp = FluidInterpolations[x + 1,y + 1,z + 1];
                    cellLevel = Temp.Volume;
                    if (cellLevel > 0 && Temp.Type == BlockId)
                    {
                        Size = (float)cellLevel / MaxVolume;
                        if (Size < 0.1f) Size = 0.1f;

                        for (f = 0; f < 6; ++f)
                        {
                            Dir = BlockHelper.direction[f];
                            TempAround = FluidInterpolations[x + 1 + Dir.x,y + 1 + Dir.y,z + 1 + Dir.z];
                            AroundType = TempAround.Type;
                            AroundLevel = TempAround.Volume;
                            Face = (int)BlockHelper.facesEnum[f];

                            if (AroundType == 0)
                                VoxelBlock.AddFace(Terrain, BlockId, x, y, z, f,Face, ref Info, Terrain.Information.Scale, 1f - Size);
                            else
                            {
                                if (f != 2 && AroundType != BlockId)
                                    continue;

                                if (f == 3) // Bottom
                                {
                                    if (AroundLevel >= FluidData.MaxVolume)
                                        continue;
                                    else
                                        VoxelBlock.AddFace(Terrain, BlockId, x, y, z, f, Face, ref Info, Terrain.Information.Scale, 1f - Size);
                                }
                                else if (f == 2)
                                {
                                    if(AroundType != Temp.Type)
                                        VoxelBlock.AddFace(Terrain, BlockId, x, y, z, f, Face, ref Info, Terrain.Information.Scale, 1f - Size);
                                }
                                else
                                {
                                    if (AroundLevel < cellLevel)
                                    {
                                        if (TempAround.Block == Terrain)
                                        {
                                            AroundSize = (float)AroundLevel / MaxVolume;
                                            if (AroundSize < 0.1f)
                                                AroundSize = 0.1f;
                                            VoxelBlock.AddFace(Terrain, BlockId, x, y, z, f, Face, ref Info, Terrain.Information.Scale, 1f - Size, AroundSize);
                                        }
                                        else
                                        {
                                            VoxelBlock.AddFace(Terrain, BlockId, x, y, z, f, Face, ref Info, Terrain.Information.Scale, 1f - Size);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        Data.Indices[0] = Info.Triangles.Indices.ToArray();
        Data.Vertices = Info.Vertices.ToArray();
        Data.UVs = Info.Uvs.ToArray();

        Info.Close();

        if (!Data.IsValid())
        {
            Data.Close();
            Data = null;
        }
        return Data;
    }

    /*public override void Build(TerrainBlock Block, ICustomTerrainData IData)
    {
        Mesh CMesh = Block.Script.GetComponent<MeshFilter>().sharedMesh;
        MeshRenderer CRenderer = Block.Script.GetComponent<MeshRenderer>();

        CMesh.Clear(false);

        MeshData Data = IData.CData;
        if (Data == null || Block.Closed)
            return;

        CMesh.vertices = Data.Vertices;
        CMesh.uv = Data.Uvs;
        CMesh.colors = Data.Colors;
        CMesh.subMeshCount = Data.Materials.Length;
        for (int i = 0; i < Data.Indices.Length; ++i)
            CMesh.SetTriangles(Data.Indices[i], i);
        CRenderer.sharedMaterials = Data.Materials;
        CMesh.RecalculateNormals();
    }

    public override MeshData Generate(TerrainBlock Block, ICustomTerrainData Data)
    {
        if (Block.Closed)
            return null;

        int cellLevel = 0;

        TempMeshData Info = new TempMeshData(true);
        MeshData MeshData = new MeshData();

        MeshData.Materials = new Material[Processor.BlocksId.Length];
        MeshData.Indices = new int[Processor.BlocksId.Length][];
        int MatId = 0;

        for (int i = 0; i < Processor.BlocksId.Length; ++i)
        {
            byte BlockId = (byte)Processor.BlocksId[i];
            VoxelInformation BlockInfo = Processor.Blocks[i];
            MeshData.Materials[MatId] = BlockInfo.FluidMaterial;
            TerrainBlock AroundBlock = null;
            ICustomTerrainData Around = null;
            int px = 0;
            int py = 0;
            int pz = 0;
            byte Type = 0;
            int AroundLevel = 0;
            Color col = Color.white;
            Color[] cols = new Color[5] { col, col, col, col, col };

            for (int y = 0; y < Block.Informations.VoxelCountY; ++y)
            {
                for (int x = 0; x < Block.Informations.VoxelCountX; ++x)
                {
                    for (int z = 0; z < Block.Informations.VoxelCountZ; ++z)
                    {
                        cellLevel = Data.GetInt(x, y, z);
                        if (cellLevel > 0 && Block.Voxels.GetByte(x, y, z) == BlockId)
                        {
                            float Size = (float)cellLevel / (float)EulerianFluidData.MaxLevel;
                            if (Size < 0.1f)
                                Size = 0.1f;

                            for (int f = 0; f < 6; ++f)
                            {
                                px = x + BlockHelper.direction[f].x;
                                py = y + BlockHelper.direction[f].y;
                                pz = z + BlockHelper.direction[f].z;

                                AroundBlock = Block.GetAroundBlock(ref px, ref py, ref pz);
                                if (AroundBlock == null)
                                    continue;

                                Type = AroundBlock.GetByte(px, py, pz, true);
                                if (Type == 0)
                                    VoxelChunk.AddFace(Block, BlockId, x, y, z, f, ref Info, cols, Block.Informations.Scale, 1f - Size);
                                else if (Type == BlockId)
                                {
                                    if (f == 2) // Top
                                        continue;
                                    else if (f == 3) // Bottom
                                    {
                                        Around = AroundBlock.Voxels.GetCustomData("Fluid");
                                        AroundLevel = Around.GetInt(px, py, pz);
                                        if (AroundLevel >= EulerianFluidData.MaxLevel)
                                            continue;
                                        else
                                            VoxelChunk.AddFace(Block, BlockId, x, y, z, f, ref Info, cols, Block.Informations.Scale, 1f - Size);
                                    }
                                    else
                                    {
                                        Around = AroundBlock.Voxels.GetCustomData("Fluid");
                                        AroundLevel = Around.GetInt(px, py, pz);
                                        if (AroundLevel < cellLevel)
                                            VoxelChunk.AddFace(Block, BlockId, x, y, z, f, ref Info, cols, Block.Informations.Scale, 1f - Size);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            MeshData.Indices[MatId] = Info.Indices.ToArray();
            Info.Indices.Clear();
            ++MatId;
        }

        MeshData.Colors = Info.Colors.ToArray();
        MeshData.Vertices = Info.Vertices.ToArray();
        MeshData.Uvs = Info.Uvs.ToArray();
        return MeshData;
    }*/
}