
using TerrainEngine;
using UnityEngine;

public class VoxelPalette : IVoxelPalette
{
    public BlockInformation[] VoxelBlocks;

    [HideInInspector]
    public BlockUVCoordinates[] BlockUvCoordinates;

    public override T GetVoxelData<T>(byte Type)
    {
        return VoxelBlocks[Type] as T;
    }

    public override int GetDataCount()
    {
        return VoxelBlocks.Length;
    }

    public override Vector2 GetUv(int Type, int Face, int Vertex)
    {
        return BlockUvCoordinates[Type].BlockFaces[Face, Vertex];
    }
}
