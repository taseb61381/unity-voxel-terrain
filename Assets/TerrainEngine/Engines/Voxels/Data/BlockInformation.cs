﻿using System;
using System.Xml.Serialization;

using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class BlockInformation : VoxelInformation
    {
        public Texture2D Top;
        public Texture2D Bottom;
        public Texture2D Side;

        [XmlIgnore]
        public Material FluidMaterial;

        [HideInInspector]
        public int TopID;

        [HideInInspector]
        public int BottomID;

        [HideInInspector]
        public int SideID;

        public IColorGenerator[] ColorGenerators;

        private int _ColorCount = -1;

        public int ColorCount
        {
            get
            {
                if (_ColorCount == -1)
                {
                    if (ColorGenerators != null)
                        _ColorCount = ColorGenerators.Length;
                    else
                        _ColorCount = 0;
                }

                return _ColorCount;
            }
        }
    }
}
