﻿
namespace TerrainEngine
{
    public struct VoxelGenerationInfo
    {
        public byte Type;
        public BlockInformation Block;

        public VoxelGenerationInfo(byte Type, BlockInformation Block)
        {
            this.Type = Type;
            this.Block = Block;
        }
    }
}
