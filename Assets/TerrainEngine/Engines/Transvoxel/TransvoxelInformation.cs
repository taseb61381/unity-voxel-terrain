﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class TransvoxelInformation
    {
        [HideInInspector]
        public int voxelLength = 0;

        [HideInInspector]
        public int voxelLenghtNormal = 0;

        [HideInInspector]
        public int vlenghtlod = 0;

        [HideInInspector]
        public int voxelCount = 0;

        [HideInInspector]
        public int vcountlod = 0;

        [HideInInspector]
        public int voxelCountLodAll = 0;

        [HideInInspector]
        public int voxelLengthSurface = 0;

        [HideInInspector]
        public int voxelCountSurface = 0;

        public VectorI3[][] toIterate;

        public VectorI3[][] toIterateLOD;

        [HideInInspector]
        public int MinimumLodLevel = 0;

        [HideInInspector]
        public int MaxLodLevel = 0;

        public NodeLevelInformation NodeLevels;

        /// <summary>
        /// For each Node Index, contains ChunkId to use for generate a mesh. Multiple Node can share the same ChunkId when they are parents
        /// </summary>
        [NonSerialized]
        public int[] ChunkNodes;

        /// <summary>
        /// Contains nodes info : Level, Id, Parent,etc. This nodes are shared to all TerrainBlock. Only Node State are personnal to each Block (Open/Close,Custom Data)
        /// Int = ChunkId of this node
        /// </summary>
        [NonSerialized]
        public OctreeChildsArray<int> NodesArray;

        private TerrainInformation Informations;

        public void InitOctree()
        {
            if (NodeLevels == null)
                NodeLevels = new NodeLevelInformation();

            int MaxLevel = NodeLevels.SetMaxLevel(Informations.VoxelsPerAxis, Informations.VoxelPerChunk);
            NodeLevels.GenerateDistancePowers(Informations.VoxelsPerAxis, Informations.VoxelPerChunk);

            ChunkNodes = new int[Informations.TotalChunksPerBlock];
            NodesArray = new OctreeChildsArray<int>(new OctreeNode<int>(0, 0, 0, -1), MaxLevel);
            NodesArray.Array[0].SubDivise(0, NodesArray, 0, 0, 0, 0, Informations.ChunkPerBlock, OnNodeSubDivise);
        }

        public void OnNodeSubDivise(int NodeIndex, int x, int y, int z, int Size)
        {
            int ChunkId = x * Informations.ChunkPerBlock * Informations.ChunkPerBlock + y * Informations.ChunkPerBlock + z;

            NodesArray.Array[NodeIndex].Data = ChunkId;

            if (NodesArray.Array[NodeIndex].m_level == NodesArray.MaxLevel)
            {
                if (ChunkNodes[ChunkId] != 0)
                    Debug.LogError(ChunkId + ",Already set:" + ChunkNodes[ChunkId] + ",New:" + NodeIndex);
                ChunkNodes[ChunkId] = NodeIndex;
            }
        }

        public void Init(TerrainInformation Information)
        {
            this.Informations = Information;

            voxelLength = Informations.VoxelPerChunk;
            voxelLenghtNormal = voxelLength + 3;
            vlenghtlod = (voxelLength + 1) * 2;
            vcountlod = vlenghtlod * vlenghtlod;
            voxelCountLodAll = 6 * vcountlod;
            voxelLengthSurface = voxelLength + 1;

            voxelCount = voxelLenghtNormal * voxelLenghtNormal * voxelLenghtNormal;
            voxelCountSurface = voxelLengthSurface * voxelLengthSurface * voxelLengthSurface;

            toIterate = new VectorI3[][] {
                                                new VectorI3[] {new VectorI3(0, 0, 0), new VectorI3(1, vlenghtlod, vlenghtlod)},
                                                new VectorI3[] {new VectorI3(vlenghtlod-2, 0, 0), new VectorI3(vlenghtlod-1, vlenghtlod, vlenghtlod)},
                                                new VectorI3[] {new VectorI3(0, 0, 0), new VectorI3(vlenghtlod, 1, vlenghtlod)},
                                                new VectorI3[] {new VectorI3(0, vlenghtlod-2, 0), new VectorI3(vlenghtlod, vlenghtlod-1, vlenghtlod)},
                                                new VectorI3[] {new VectorI3(0, 0, 0), new VectorI3(vlenghtlod, vlenghtlod, 1)},
                                                new VectorI3[] {new VectorI3(0, 0, vlenghtlod-2), new VectorI3(vlenghtlod, vlenghtlod, vlenghtlod-1)},
            };

            int voxelLengthLodStart = vlenghtlod - 2;
            int voxelLengthLodEnd = vlenghtlod - 1;

            toIterateLOD = new VectorI3[][]{
    new VectorI3[] {new VectorI3(0, 0, 0),                     new VectorI3(1, voxelLengthLodStart, voxelLengthLodStart)},
    new VectorI3[] {new VectorI3(voxelLengthLodStart, 0, 0),   new VectorI3(voxelLengthLodEnd, voxelLengthLodStart, voxelLengthLodStart)},
    new VectorI3[] {new VectorI3(0, 0, 0),                     new VectorI3(voxelLengthLodStart, 1, voxelLengthLodStart)},
    new VectorI3[] {new VectorI3(0, voxelLengthLodStart, 0),   new VectorI3(voxelLengthLodStart, voxelLengthLodEnd, voxelLengthLodStart)},
    new VectorI3[] {new VectorI3(0, 0, 0),                     new VectorI3(voxelLengthLodStart, voxelLengthLodStart, 1)},
    new VectorI3[] {new VectorI3(0, 0, voxelLengthLodStart),   new VectorI3(voxelLengthLodStart, voxelLengthLodStart, voxelLengthLodEnd)}
      };

            /*for (int i = 0; i < ActionProcessor.TManager.Threads.Length; ++i)
            {
                ExecuteAction Action = new ExecuteAction("UpdateLOD", UpdateLOD);
                ActionProcessor.TManager.Threads[i].AddAction(Action);
            }*/
            InitOctree();
        }
    }
}
