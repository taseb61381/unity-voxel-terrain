﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    public class TransvoxelTempData : ITransvoxelArray
    {
        public override void LoadVoxels(TerrainBlock Block, int m_lod, int StartX, int StartY, int StartZ)
        {
            if (Block.Voxels.UseHeight == 0)
                LoadVoxelsHeight(Block, m_lod, StartX, StartY, StartZ);
            else
                LoadVoxelsDefault(Block, m_lod, StartX, StartY, StartZ);
        }

        public void LoadVoxelsDefault(TerrainBlock Block, int m_lod, int StartX, int StartY, int StartZ)
        {
            int m_voxelSkip = (int)Math.Pow(2.0, (double)m_lod);
            int DataCount = TerrainManager.Instance.Palette.DataCount;

            if (ActiveSubMesh == null || ActiveSubMesh.Length != DataCount)
                ActiveSubMesh = new bool[DataCount];

            for (DataCount = DataCount - 1; DataCount >= 0; --DataCount)
                ActiveSubMesh[DataCount] = false;

            IsValidChunk = false;
            Count = 0;
            MaterialCount = 0;
            int indX, indY, indZ, Current = 0;
            int px, py, pz;
            int x, y, z;
            bool sx, sy, vx = false, vy = false, IsInChunk = false;
            byte LastType = 0;
            float Isolevel = TerrainInformation.Isolevel;

            MarchingInformation LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(0);
            int Index = 0;
            int End = TransvoxelInfo.voxelLength + 2;
            int InsideEnd = TransvoxelInfo.voxelLength + 1;
            int ChunkX, ChunkY, IndexX, IndexY;
            TerrainInformation Information = Block.Information;
            TerrainBlock AroundBlock = null;
            byte VType = 0;
            float Volume = 0;

            int VoxelPerChunk = Information.VoxelPerChunk;
            int ChunkPerBlock = Information.ChunkPerBlock;
            int VoxelsPerAxis = Information.VoxelsPerAxis;
            int vcountlod = TransvoxelInfo.vcountlod;
            int vlenghtlod = TransvoxelInfo.vlenghtlod;

            /*LoadHeightMap(Block, m_lod, StartX, StartY, StartZ);
            LoadVoxelArray(Block, m_lod, StartX, StartY, StartZ);
            IsValidChunk = Count != 0;*/

            for (indX = -1; indX < End; ++indX)
            {
                px = (StartX + indX * m_voxelSkip);
                sx = px >= 0 && px < VoxelsPerAxis; // Position is inside Block
                vx = indX >= 0 && indX < InsideEnd; // Position is inside current chunk
                ChunkX = (px / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock;
                IndexX = (px % VoxelPerChunk) * VoxelPerChunk * VoxelPerChunk;

                for (indY = -1; indY < End; ++indY)
                {
                    py = (StartY + indY * m_voxelSkip);
                    sy = py >= 0 && py < VoxelsPerAxis; // Position is inside Block
                    vy = indY >= 0 && indY < InsideEnd; // Position is inside current chunk
                    ChunkY = (py / VoxelPerChunk) * ChunkPerBlock;
                    IndexY = (py % VoxelPerChunk) * VoxelPerChunk;

                    for (indZ = -1; indZ < End; ++indZ, ++Index)
                    {
                        pz = (StartZ + indZ * m_voxelSkip);

                        IsInChunk = vx && vy && indZ >= 0 && indZ < InsideEnd;  // Position is inside current chunk

                        //Block.GetByteAndFloat(px, py, pz, ref VType, ref Volume);

                        if ((sx && sy && (pz >= 0 && pz < VoxelsPerAxis)))
                        {
                            Block.Voxels.GetTypeAndVolume(px, py, pz, ChunkX + ChunkY + (pz / VoxelPerChunk), IndexX + IndexY + (pz % VoxelPerChunk), ref VType, ref Volume);
                        }
                        else
                        {
                            x = px;
                            y = py;
                            z = pz;
                            AroundBlock = Block.GetAroundBlock(ref x, ref y, ref z) as TerrainBlock;
                            if (AroundBlock == null)
                            {
                                VType = 0;
                                Volume = 0;
                            }
                            else
                                AroundBlock.Voxels.GetTypeAndVolume(x, y, z, ref VType, ref Volume);
                        }

                        if (VType != 0)
                        {
                            if (VType != LastType)
                            {
                                LastType = VType;
                                LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(VType);
                            }

                            if (LastVoxelType.UseSplatMap)
                            {
                                voxels[Index].VOriginalType = VType;

                                if (LastVoxelType.MainVoxelId != 0)
                                    VType = LastVoxelType.MainVoxelId;
                            }
                            else
                                voxels[Index].VOriginalType = VType;

                            if (LastVoxelType.VoxelType == VoxelTypes.FLUID)
                            {
                                VType = 0;
                                Volume = 0;
                            }
                            else
                            {
                                if (IsInChunk && !IsValidChunk)
                                {
                                    if (Volume >= Isolevel)
                                    {
                                        IsValidChunk = LastVoxelType.VoxelType == VoxelTypes.TRANSLUCENT;
                                        ++Count;
                                    }
                                }

                                if (IsInChunk && ActiveSubMesh[VType] == false)
                                {
                                    ActiveSubMesh[VType] = true;
                                    ++MaterialCount;
                                }
                            }
                        }
                        else
                        {
                            Volume = 0;
                        }

                        voxels[Index].VType = VType;
                        if (VType != 0)
                            voxels[Index].Volume = Volume;


                        if (IsInChunk && !IsValidChunk)
                        {
                            ++Current;

                            if (Count != 0 && Count != Current)
                                IsValidChunk = true;
                        }
                    }
                }
            }

            if (IsValidChunk && m_lod > 0)
            {
                int Skip = (m_voxelSkip / 2);
                VectorI3 start, end;

                for (int lod = 0; lod < 6; ++lod)
                {
                    start = TransvoxelInfo.toIterate[lod][0];
                    end = TransvoxelInfo.toIterate[lod][1];
                    LastType = 0;
                    LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(0);

                    for (indX = start.x; indX < end.x; ++indX)
                    {
                        px = StartX + indX * Skip;
                        sx = px >= 0 && px < VoxelsPerAxis; // Position is inside Block
                        vx = indX >= 0 && indX < InsideEnd; // Position is inside current chunk
                        ChunkX = (px / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock;
                        IndexX = (px % VoxelPerChunk) * VoxelPerChunk * VoxelPerChunk;

                        for (indY = start.y; indY < end.y; ++indY)
                        {
                            py = StartY + indY * Skip;
                            sy = py >= 0 && py < VoxelsPerAxis; // Position is inside Block
                            vy = indY >= 0 && indY < InsideEnd; // Position is inside current chunk
                            ChunkY = (py / VoxelPerChunk) * ChunkPerBlock;
                            IndexY = (py % VoxelPerChunk) * VoxelPerChunk;

                            for (indZ = start.z; indZ < end.z; ++indZ)
                            {
                                pz = StartZ + indZ * Skip;
                                IsInChunk = vx && vy && indZ >= 0 && indZ < InsideEnd;  // Position is inside current chunk

                                Index = GetLodInterpolationIndex(indX - start.x, indY - start.y, indZ - start.z, lod,vcountlod,vlenghtlod);

                                if ((sx && sy && (pz >= 0 && pz < VoxelsPerAxis)))
                                {
                                    Block.Voxels.GetTypeAndVolume(px, py, pz, ChunkX + ChunkY + (pz / VoxelPerChunk), IndexX + IndexY + (pz % VoxelPerChunk), ref VType, ref Volume);
                                }
                                else
                                {
                                    x = px;
                                    y = py;
                                    z = pz;
                                    AroundBlock = Block.GetAroundBlock(ref x, ref y, ref z) as TerrainBlock;
                                    if (AroundBlock == null)
                                    {
                                        VType = 0;
                                        Volume = 0;
                                    }
                                    else
                                        AroundBlock.Voxels.GetTypeAndVolume(x, y, z, ref VType, ref Volume);
                                }

                                if (VType != 0)
                                {
                                    if (VType != LastType)
                                    {
                                        LastType = VType;
                                        LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(VType);
                                    }

                                    if (LastVoxelType.UseSplatMap)
                                    {
                                        voxelslod[Index].VOriginalType = VType;

                                        if (LastVoxelType.MainVoxelId != 0)
                                            VType = LastVoxelType.MainVoxelId;
                                    }
                                    else
                                        voxelslod[Index].VOriginalType = VType;

                                    if (LastVoxelType.VoxelType == VoxelTypes.FLUID)
                                    {
                                        Volume = 0;
                                        VType = 0;
                                    }
                                    else
                                    {
                                        if (IsInChunk && ActiveSubMesh[VType] == false)
                                        {
                                            ActiveSubMesh[VType] = true;
                                            ++MaterialCount;
                                        }
                                    }
                                }
                                else
                                {
                                    Volume = 0;
                                }

                                voxelslod[Index].VType = VType;
                                if (VType != 0)
                                    voxelslod[Index].Volume = Volume;
                            }
                        }
                    }
                }
            }

        }

        public void LoadVoxelsHeight(TerrainBlock Block, int m_lod, int StartX, int StartY, int StartZ)
        {
            int m_voxelSkip = (int)Math.Pow(2.0, (double)m_lod);
            int DataCount = TerrainManager.Instance.Palette.DataCount;

            if (ActiveSubMesh == null || ActiveSubMesh.Length != DataCount)
                ActiveSubMesh = new bool[DataCount];

            for (DataCount = DataCount - 1; DataCount >= 0; --DataCount)
                ActiveSubMesh[DataCount] = false;

            IsValidChunk = false;
            bool LocalIsValidChunk = false;
            int LocalCount = 0;

            Count = 0;
            MaterialCount = 0;
            int indX, indY, indZ, Current = 0;
            int px, py, pz;
            int x, y, z;
            bool sx,sz, vx = false,vz=false, IsInChunk = false;
            byte LastType = 0;
            byte LastRegisteredType = 0;
            float Isolevel = TerrainInformation.Isolevel;

            MarchingInformation LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(0);
            int Index = 0;
            int TotalEnd = TransvoxelInfo.voxelLength + 3;
            int End = TransvoxelInfo.voxelLength + 2;
            int InsideEnd = TransvoxelInfo.voxelLength + 1;
            int ChunkX, ChunkId, ChunkZ, IndexX, IndexZ;
            TerrainInformation Information = Block.Information;
            TerrainBlock AroundBlock = null;
            byte VType = 0;
            float Volume = 0;

            int VoxelPerChunk = Information.VoxelPerChunk;
            int ChunkPerBlock = Information.ChunkPerBlock;
            int VoxelsPerAxis = Information.VoxelsPerAxis;
            int vcountlod = TransvoxelInfo.vcountlod;
            int vlenghtlod = TransvoxelInfo.vlenghtlod;

            int  LastChunkId=-1;
            bool HasChunk=false;
            float LastHeight = 0;
            byte LastHeightType = 0;

            for (indX = -1; indX < End; ++indX)
            {
                px = (StartX + indX * m_voxelSkip);
                sx = px >= 0 && px < VoxelsPerAxis; // Position is inside Block
                vx = indX >= 0 && indX < InsideEnd; // Position is inside current chunk
                ChunkX = (px / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock;

                for (indZ = -1; indZ < End; ++indZ)
                {
                    pz = (StartZ + indZ * m_voxelSkip);
                    sz = pz >= 0 && pz < VoxelsPerAxis; // Position is inside Block
                    vz = indZ >= 0 && indZ < InsideEnd; // Position is inside current chunk
                    ChunkZ = (pz / VoxelPerChunk);

                    Index = (indX + 1) * TotalEnd * TotalEnd + (indZ + 1);
                    LastChunkId = -1;

                    for (indY = -1; indY < End; ++indY, Index += TotalEnd)
                    {
                        py = (StartY + indY * m_voxelSkip);
                        ChunkId = ChunkX + (py / VoxelPerChunk) * ChunkPerBlock + ChunkZ;

                        IsInChunk = vx && (indY >= 0 && indY < InsideEnd) && vz;  // Position is inside current chunk

                        if (sx && (py >= 0 && py < VoxelsPerAxis) && sz)
                        {
                            if (LastChunkId != ChunkId)
                            {
                                LastChunkId = ChunkId;

                                if (Block.Voxels.Voxels[ChunkId] == null)
                                {
                                    HasChunk = false;
                                    LastHeight = Block.Voxels.GetTypeAndHeightFromGrid(px, pz, ref LastHeightType);

                                    VType = LastHeightType;
                                    Block.Voxels.GetVolumeFromHeight(py, ref VType, ref Volume, LastHeight);
                                }
                                else
                                {
                                    HasChunk = true;
                                    IndexX = (px % VoxelPerChunk) * VoxelPerChunk * VoxelPerChunk;
                                    IndexZ = (pz % VoxelPerChunk);
                                    VType = Block.Voxels.GetTypeAndVolumeFromChunk(px, py, pz, ChunkId, IndexX + ((py % VoxelPerChunk) * VoxelPerChunk) + IndexZ, ref Volume);
                                }
                            }
                            else
                            {
                                if (!HasChunk)
                                {
                                    //LastHeight = Block.Voxels.GetTypeAndHeightFromGrid(px, pz, ref LastHeightType);
                                    VType = LastHeightType;
                                    Block.Voxels.GetVolumeFromHeight(py, ref VType, ref Volume, LastHeight);
                                }
                                else
                                {
                                    IndexX = (px % VoxelPerChunk) * VoxelPerChunk * VoxelPerChunk;
                                    IndexZ = (pz % VoxelPerChunk);
                                    VType = Block.Voxels.GetTypeAndVolumeFromChunk(px, py, pz, ChunkId, IndexX + ((py % VoxelPerChunk) * VoxelPerChunk) + IndexZ, ref Volume);
                                }
                            }
                        }
                        else
                        {
                            x = px;
                            y = py;
                            z = pz;
                            AroundBlock = Block.GetAroundBlock(ref x, ref y, ref z) as TerrainBlock;
                            if (AroundBlock == null)
                            {
                                VType = 0;
                                Volume = 0;
                            }
                            else
                                VType = AroundBlock.Voxels.GetTypeAndVolumeHeight(x, y, z, ref Volume);
                        }

                        if (VType != 0)
                        {
                            if (VType != LastType)
                            {
                                LastType = VType;
                                LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(VType);
                            }

                            if (LastVoxelType.VoxelType == VoxelTypes.FLUID)
                            {
                                VType = 0;
                                Volume = 0;
                                voxels[Index].VType = 0;
                            }
                            else
                            {
                                if (LastVoxelType.UseSplatMap && LastVoxelType.MainVoxelId != 0)
                                {
                                    voxels[Index].VOriginalType = VType;
                                    VType = LastVoxelType.MainVoxelId;
                                }
                                else
                                    voxels[Index].VOriginalType = VType;

                                if (IsInChunk)
                                {
                                    if (!LocalIsValidChunk && Volume >= Isolevel)
                                    {
                                        LocalIsValidChunk = LastVoxelType.VoxelType == VoxelTypes.TRANSLUCENT;
                                        ++LocalCount;
                                    }

                                    if (LastRegisteredType != VType && ActiveSubMesh[VType] == false)
                                    {
                                        LastRegisteredType = VType;
                                        ActiveSubMesh[VType] = true;
                                        ++MaterialCount;
                                    }
                                }

                                voxels[Index].VType = VType;
                                voxels[Index].Volume = Volume;
                            }
                        }
                        else
                        {
                            Volume = 0;
                            voxels[Index].VType = 0;
                        }


                        if (IsInChunk && !LocalIsValidChunk)
                        {
                            ++Current;

                            if (LocalCount != 0 && LocalCount != Current)
                                LocalIsValidChunk = true;
                        }
                    }
                }
            }

            IsValidChunk = LocalIsValidChunk;
            Count = LocalCount;

            if (LocalIsValidChunk && m_lod > 0)
            {
                int Skip = (m_voxelSkip / 2);
                int startx, endx,starty,endy,startz,endz;

                for (int lod = 0; lod < 6; ++lod)
                {
                    LastChunkId = -1;
                    HasChunk = false;
                    LastHeightType = 0;

                    startx = TransvoxelInfo.toIterate[lod][0].x;
                    starty = TransvoxelInfo.toIterate[lod][0].y;
                    startz = TransvoxelInfo.toIterate[lod][0].z;

                    endx = TransvoxelInfo.toIterate[lod][1].x;
                    endy = TransvoxelInfo.toIterate[lod][1].y;
                    endz = TransvoxelInfo.toIterate[lod][1].z;
                    LastType = 0;
                    LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(0);

                    for (indX = startx; indX < endx; ++indX)
                    {
                        px = StartX + indX * Skip;
                        sx = px >= 0 && px < VoxelsPerAxis; // Position is inside Block
                        vx = indX >= 0 && indX < InsideEnd; // Position is inside current chunk
                        ChunkX = (px / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock;

                        for (indZ = startz; indZ < endz; ++indZ)
                        {
                            pz = StartZ + indZ * Skip;
                            sz = pz >= 0 && pz < VoxelsPerAxis; // Position is inside Block
                            vz = indZ >= 0 && indZ < InsideEnd; // Position is inside current chunk
                            ChunkZ = (pz / VoxelPerChunk);

                            LastChunkId = -1;
                            LastHeight = -1;

                            for (indY = starty; indY < endy; ++indY)
                            {
                                py = StartY + indY * Skip;
                                ChunkId = ChunkX + (py / VoxelPerChunk) * ChunkPerBlock + ChunkZ;
                                //IndexY = (py % VoxelPerChunk) * VoxelPerChunk;

                                IsInChunk = vx && (indY >= 0 && indY < InsideEnd) && vz;  // Position is inside current chunk

                                Index = GetLodInterpolationIndex(indX - startx, indY - starty, indZ - startz, lod, vcountlod, vlenghtlod);

                                if (sx && (py >= 0 && py < VoxelsPerAxis) && sz)
                                {
                                    if (LastChunkId != ChunkId)
                                    {
                                        LastChunkId = ChunkId;

                                        if (Block.Voxels.Voxels[ChunkId] == null)
                                        {
                                            HasChunk = false;
                                            //if (LastHeight < 0)
                                                LastHeight = Block.Voxels.GetTypeAndHeightFromGrid(px, pz, ref LastHeightType);

                                            VType = LastHeightType;
                                            Block.Voxels.GetVolumeFromHeight(py, ref VType, ref Volume, LastHeight);
                                        }
                                        else
                                        {
                                            HasChunk = true;
                                            IndexX = (px % VoxelPerChunk) * VoxelPerChunk * VoxelPerChunk;
                                            IndexZ = (pz % VoxelPerChunk);
                                            VType = Block.Voxels.GetTypeAndVolumeFromChunk(px, py, pz, ChunkId, IndexX + ((py % VoxelPerChunk) * VoxelPerChunk) + IndexZ, ref Volume);
                                        }
                                    }
                                    else
                                    {
                                        if (!HasChunk)
                                        {
                                            //if (LastHeight < 0)
                                                LastHeight = Block.Voxels.GetTypeAndHeightFromGrid(px, pz, ref LastHeightType);

                                            VType = LastHeightType;
                                            Block.Voxels.GetVolumeFromHeight(py, ref VType, ref Volume, LastHeight);
                                        }
                                        else
                                        {
                                            IndexX = (px % VoxelPerChunk) * VoxelPerChunk * VoxelPerChunk;
                                            IndexZ = (pz % VoxelPerChunk);
                                            VType = Block.Voxels.GetTypeAndVolumeFromChunk(px, py, pz,ChunkId, IndexX + ((py % VoxelPerChunk) * VoxelPerChunk) + IndexZ, ref Volume);
                                        }
                                    }
                                }
                                else
                                {
                                    x = px;
                                    y = py;
                                    z = pz;
                                    AroundBlock = Block.GetAroundBlock(ref x, ref y, ref z) as TerrainBlock;
                                    if (AroundBlock == null)
                                    {
                                        VType = 0;
                                        Volume = 0;
                                    }
                                    else
                                        VType = AroundBlock.Voxels.GetTypeAndVolumeHeight(x, y, z, ref Volume);
                                }

                                if (VType != 0)
                                {
                                    if (VType != LastType)
                                    {
                                        LastType = VType;
                                        LastVoxelType = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(VType);

                                    }
                                    if (LastVoxelType.VoxelType == VoxelTypes.FLUID)
                                    {
                                        Volume = 0;
                                        VType = 0;
                                        voxelslod[Index].VType = 0;
                                    }
                                    else
                                    {
                                        if (LastVoxelType.UseSplatMap && LastVoxelType.MainVoxelId != 0)
                                        {
                                            voxelslod[Index].VOriginalType = VType;
                                            VType = LastVoxelType.MainVoxelId;
                                        }
                                        else
                                            voxelslod[Index].VOriginalType = VType;

                                        if (IsInChunk && LastRegisteredType != VType && ActiveSubMesh[VType] == false)
                                        {
                                            LastRegisteredType = VType;
                                            ActiveSubMesh[VType] = true;
                                            ++MaterialCount;
                                        }


                                        voxelslod[Index].VType = VType;
                                        voxelslod[Index].Volume = Volume;
                                    }
                                }
                                else
                                {
                                    Volume = 0;
                                    voxelslod[Index].VType = 0;
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
