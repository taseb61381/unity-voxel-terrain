﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TerrainEngine
{
    public struct VoxelInterpolationInfo
    {
        public byte VType;
        public float Volume;
        public byte VOriginalType;
        //public Color SplatColor;
    }

    public class ITransvoxelArray : IPoolable
    {
        public TransvoxelInformation TransvoxelInfo;
        public VoxelInterpolationInfo[] voxelslod; // 6 Side of the chunk
        public VoxelInterpolationInfo[] voxels;

        public bool GenerateUVs = false;
        public bool GenerateTangents = false;
        public bool GenerateColors = false;

        public int[] ids = new int[12];
        public uint[] idsLOD = new uint[12];

        public Vector3[] normalsForTransvoxel = new Vector3[4];
        public int[] vertexIndicesReuse;
        public List<int>[] m_indicesLod;
        public int[] vertexIndicesReuseLod;
        public bool[] ActiveSubMesh;
        public int MaterialCount;
        public int Count;
        public bool IsValidChunk = false;

        public virtual void Init(TransvoxelBlock Block, int lod, int StartX, int StartY, int StartZ, int voxelLenght)
        {
            if (voxels == null)
            {
                int Lenght = TransvoxelInfo.voxelLenghtNormal;

                this.voxelslod = new VoxelInterpolationInfo[6 * TransvoxelInfo.vcountlod];
                this.voxels = new VoxelInterpolationInfo[Lenght * Lenght * Lenght];

                int voxelEnd = voxelLenght + 2;

                int vertexIndicesReuseSize = (((voxelEnd + 1) * 3) * ((voxelEnd + 1) * 3) * ((voxelEnd + 1) * 3));

                if (vertexIndicesReuse == null || vertexIndicesReuse.Length != vertexIndicesReuseSize)
                    vertexIndicesReuse = new int[vertexIndicesReuseSize];

                int vertexIndicesReuseLodSize = ((voxelLenght + 1) * 4) * ((voxelLenght + 1) * 4);
                if (vertexIndicesReuseLod == null || vertexIndicesReuseLod.Length != vertexIndicesReuseLodSize)
                    vertexIndicesReuseLod = new int[vertexIndicesReuseLodSize];
            }

            LoadVoxels(Block, lod, StartX, StartY, StartZ);
        }

        public virtual void LoadVoxels(TerrainBlock Block, int m_lod, int StartX, int StartY, int StartZ)
        {

        }

        public void Reset()
        {

            int i;
            for (i = 11; i >= 0; --i)
            {
                ids[i] = -1;
                idsLOD[i] = 0;
            }

            for (i = 3; i >= 0; --i)
            {
                normalsForTransvoxel[i] = Vector3.zero;
            }

            for (i = vertexIndicesReuse.Length - 1; i >= 0; --i)
            {
                vertexIndicesReuse[i] = -1;
            }

            if (m_indicesLod == null)
            {
                m_indicesLod = new List<int>[6];
                m_indicesLod[0] = new List<int>(100);
                m_indicesLod[1] = new List<int>(100);
                m_indicesLod[2] = new List<int>(100);
                m_indicesLod[3] = new List<int>(100);
                m_indicesLod[4] = new List<int>(100);
                m_indicesLod[5] = new List<int>(100);
            }
            else
            {
                m_indicesLod[0].Clear();
                m_indicesLod[1].Clear();
                m_indicesLod[2].Clear();
                m_indicesLod[3].Clear();
                m_indicesLod[4].Clear();
                m_indicesLod[5].Clear();
            }
        }

        public override void Clear()
        {

        }

        public void GetInterpolation(int x, int y, int z, out VoxelInterpolationInfo Info)
        {
            Info = voxels[((x + 1) * TransvoxelInfo.voxelLenghtNormal * TransvoxelInfo.voxelLenghtNormal + (y + 1) * TransvoxelInfo.voxelLenghtNormal + z + 1)];
        }

        public void SetInterpolation(int x, int y, int z, ref VoxelInterpolationInfo Info)
        {
            voxels[((x + 1) * TransvoxelInfo.voxelLenghtNormal * TransvoxelInfo.voxelLenghtNormal + (y + 1) * TransvoxelInfo.voxelLenghtNormal + z + 1)] = Info;
        }

        static public int GetLodInterpolationIndex(int x, int y, int z, int lod, int vcountlod,int vlenghtlod)
        {
            if (lod < 2)
            {
                x = y;
                y = z;
            }
            else if (lod < 4)
            {
                y = z;
            }

            return lod * vcountlod + x * vlenghtlod + y;
        }

        public void getVoxelLod(int x, int y, int z, int lod, out VoxelInterpolationInfo Info)
        {
            if (lod < 2)
            {
                x = y;
                y = z;
            }
            else if (lod < 4)
            {
                y = z;
            }

            Info = voxelslod[lod * TransvoxelInfo.vcountlod + x * TransvoxelInfo.vlenghtlod + y];
        }

        public float getVoxelLodVolume(int x, int y, int z, int lod, int MaterialId)
        {
            if (lod < 2)
            {
                x = y;
                y = z;
            }
            else if (lod < 4)
            {
                y = z;
            }

            x = lod * TransvoxelInfo.vcountlod + x * TransvoxelInfo.vlenghtlod + y;
            y = voxelslod[x].VType;
            if (y != MaterialId)
                return TerrainInformation.Isofix;

            return voxelslod[x].Volume;
        }
    }
}
