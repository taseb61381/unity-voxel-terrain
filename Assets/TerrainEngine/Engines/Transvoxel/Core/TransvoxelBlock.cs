﻿using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    public class TransvoxelBlock : MarchingBlock
    {
        public TransvoxelInformation TransvoxelInfo;
        public OctreeChildsArray<int> NodesArray;
        public int[] ChunkNodes
        {
            get
            {
                return TransvoxelInfo.ChunkNodes;
            }
        }
        private int LastFrameCount = 0;
        private SList<int> BuildedNodes = new SList<int>(4);
        private bool CanUpdateLOD = false;

        public TransvoxelBlock(TransvoxelManager Manager)
            : base(Manager)
        {
            this.TransvoxelInfo = Manager.TransvoxelInfo;
            NodesArray = new OctreeChildsArray<int>(Manager.TransvoxelInfo.NodesArray);
        }

        public TransvoxelBlock(TerrainInformation Information, TransvoxelInformation TransvoxelInfo) : base(Information)
        {
            this.TransvoxelInfo = TransvoxelInfo;
        }

        public override void Init(TerrainInformation Information, VectorI3 Position, ushort UID)
        {
            base.Init(Information, Position, UID);

            for (int i = NodesArray.ClosedNodes.Length - 1; i >= 0; --i)
            {
                NodesArray.ClosedNodes[i] = false;
            }
        }

        public override void RelocateChunks()
        {
            base.RelocateChunks();
            UpdateOctree(null, false,null);
        }

        public override void LoadingDone()
        {
            CanUpdateLOD = true;
            base.LoadingDone();
        }

        public override void CloseThread()
        {
            CanUpdateLOD = false;
            base.CloseThread();
        }

        /// <summary>
        /// Used for octree. This will return the chunk parent
        /// </summary>
        /// <returns></returns>
        public override int GetChunkParent(int ChunkId)
        {
            return NodesArray.Array[NodesArray.Array[ChunkNodes[ChunkId]].GetOpenParent(NodesArray)].Data;
        }

        public void UpdateOctree(ActionThread Thread, bool Generate, List<KeyValuePair<int, bool>> UpdatedNodes)
        {
            if (Generate && !CanUpdateLOD)
            {
                Debug.LogError("TerrainBlock : Can not update LOD");
                return;
            }

            NodesArray.Array[0].SubDivise(NodesArray, 0, 
                WorldPosition.x, WorldPosition.y, WorldPosition.z, Information.BlockSizeX,
                TransvoxelInfo.MinimumLodLevel - 1,
                TransvoxelInfo.NodeLevels,
                ITerrainCameraHandler.Instance.Container,
                Generate ? UpdatedNodes : null);
        }

        public override void Generate(ActionThread Thread,ref TempMeshData Info,ref IMeshData MData, int ChunkId)
        {
            int Parent = NodesArray.Array[ChunkNodes[ChunkId]].GetOpenParent(NodesArray);

            if (NodesArray.Array[Parent].Data == ChunkId)
            {
                GenerateNode(Thread, ref Info, ref MData, Parent);
            }
        }

        public override void Build(IMeshData Data, int ChunkId, bool Original = true, bool Combined = false, bool CanDisableChilds = true)
        {
            int Parent = NodesArray.Array[ChunkNodes[ChunkId]].GetOpenParent(NodesArray);

            base.Build(Data, ChunkId, Original, Combined);

            if (CanDisableChilds)
            {
                if (LastFrameCount != Time.frameCount)
                {
                    LastFrameCount = Time.frameCount;
                    BuildedNodes.ClearFast();
                }

                for (int i = BuildedNodes.Count - 1; i >= 0; --i)
                {
                    if (BuildedNodes.array[i] == Parent)
                        return;
                }

                BuildedNodes.Add(Parent);
                DisableChilds(ChunkId, Parent);
            }
        }

        public void DisableChilds(int IgnoreChunk, int Index)
        {
            int ChunkId = NodesArray.Array[Index].Data;
            if (ChunkId != IgnoreChunk)
            {
                base.DisableMesh(ChunkId);
            }

            int Start = NodesArray.Array[Index].StartIndex;
            int End = NodesArray.Array[Index].EndIndex;

            if (Start == -1 || End == -1)
                return;

            for (Index = Start; Index < End; ++Index)
            {
                if (!NodesArray.ClosedNodes[Index])
                {
                    ChunkId = NodesArray.Array[Index].Data;
                    if (ChunkId != IgnoreChunk)
                    {
                        base.DisableMesh(ChunkId);
                    }
                }
            }

        }

        public void GenerateNode(ActionThread Thread,ref TempMeshData Info,ref IMeshData MData, int NodeIndex)
        {
            if (ThreadManager.EnableStats)
                Thread.RecordTime();

            int ChunkId = NodesArray.Array[NodeIndex].Data;
            int m_lod = NodesArray.MaxLevel - NodesArray.Array[NodeIndex].m_level;

            int LocalX = Chunks[ChunkId].LocalX;
            int LocalY = Chunks[ChunkId].LocalY;
            int LocalZ = Chunks[ChunkId].LocalZ;

            TransvoxelTempData VoxelArray = Thread.Pool.UnityPool.MarchingPool as TransvoxelTempData;
            if (VoxelArray == null)
            {
                VoxelArray = new TransvoxelTempData();
                Thread.Pool.UnityPool.MarchingPool = VoxelArray;
            }

            VoxelArray.TransvoxelInfo = TransvoxelInfo;
            VoxelArray.Init(this, m_lod, LocalX * Information.VoxelPerChunk, LocalY * Information.VoxelPerChunk, LocalZ * Information.VoxelPerChunk, TransvoxelInfo.voxelLength);

            if (ThreadManager.EnableStats)
                Thread.AddStats("Client:NodeInit", Thread.Time(), false);

            GenerateNode(Thread, ref Info, ref MData, m_lod,ChunkId, VoxelArray);
        }

        public void GenerateNode(ActionThread Thread,ref TempMeshData Info,ref IMeshData MData, int m_lod, int ChunkId, ITransvoxelArray VoxelArray)
        {
            // FULL or EMPTY Chunk
            if (!VoxelArray.IsValidChunk || VoxelArray.Count == 0 || VoxelArray.MaterialCount == 0)
            {
                VoxelArray.Close();
                return;
            }

            if (ThreadManager.EnableStats)
                Thread.RecordTime();

            Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();

            TransvoxelInformation TransvoxelInfo = VoxelArray.TransvoxelInfo;
            TransvoxelManager Data = (TerrainManager.Instance as TransvoxelManager);

            float Isolevel = TerrainInformation.Isolevel;
            float Scale = Information.Scale;
            float volumValue = 0;
            float voxelSize = 1 << m_lod;

            Vector3 ChunkWorldPosition = GetChunkWorldPosition(ChunkId);

            bool GenerateColor = VoxelArray.GenerateColors;
            bool GenerateUV = VoxelArray.GenerateUVs || VoxelArray.GenerateTangents;
            int voxelLength = TransvoxelInfo.voxelLength;
            int voxelEnd = voxelLength + 2;
            int vlwnc = TransvoxelInfo.voxelLenghtNormal;
            int vlwnc2 = vlwnc * vlwnc;
            long VertexCount = 0;
            int PaletteCount = Data.Palette.DataCount;
            VoxelArray.MaterialCount = 0;

            VoxelInterpolationInfo Voxel0 = new VoxelInterpolationInfo(), 
                Right1 = new VoxelInterpolationInfo(), 
                Forward2 = new VoxelInterpolationInfo(), 
                RightForward3 = new VoxelInterpolationInfo(), 
                Top4 = new VoxelInterpolationInfo(), 
                RightTop5 = new VoxelInterpolationInfo(), 
                TopForward6 = new VoxelInterpolationInfo(), 
                RightTopForward7 = new VoxelInterpolationInfo(), 
                Voxel8 = new VoxelInterpolationInfo(), 
                Corner0, 
                Corner1;
            MarchingInformation SplatInfo;
            for (int i = 0; i < PaletteCount; ++i)
            {
                if (VoxelArray.ActiveSubMesh[i] == false)
                    continue;

                SplatInfo = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>((byte)i);
                if (SplatInfo.UseSplatMap)
                {
                    GenerateColor = true;

                    if (SplatInfo.MainVoxelId != 0)
                    {
                        VoxelArray.ActiveSubMesh[SplatInfo.MainVoxelId] = true;
                        VoxelArray.ActiveSubMesh[i] = false;
                    }
                }
            }

            if (GenerateColor || GenerateUV)
                MData = PoolManager.UPool.GetColorUVMeshData();
            else
                MData = PoolManager.UPool.GetSimpleMeshData();

            MarchingInformation MarchingInfo = null;
            Color CurrentColor = new Color(1, 0, 0, 0);
            Color Black = Color.black;
            bool HasColor = false;
            MarchingPalette Palette = TerrainManager.Instance.Palette as MarchingPalette;
            Vector2 UV = new Vector2();

            if (ThreadManager.EnableStats)
                Thread.AddStats("Client:InitNodeValues", Thread.Time());

            for (int MaterialId = 0; MaterialId < PaletteCount; ++MaterialId)
            {
                if (VoxelArray.ActiveSubMesh[MaterialId] == false)
                    continue;

                VoxelArray.Reset();

                SplatInfo = Palette.VoxelBlocks[MaterialId];

                if (ThreadManager.EnableStats)
                    Thread.RecordTime();

                #region GenerateNode

                // isLevel describes at which interpolation-level a surface is generated around the voxel

                {
                    int x, y, z;
                    byte tableIndex;
                    bool calculateFaces;
                    TransvoxelTables.RegularCellData data;
                    int data2, corner0, corner1, id, ind, owner;
                    int vertexIndex0, vertexIndex1, vertexIndex2;
                    Vector3 vertex0, vertex1, vertex2, addNormal, point;
                    voxelEnd = voxelLength + 2;
                    --voxelEnd;
                    int IndexX, IndexY, Index;
                    float TotalColor = 0;
                    VectorI3 Corner0Position = new VectorI3();
                    VectorI3 Corner1Position = new VectorI3();
                    bool Inited = false;
                    bool CalcX, CalcY,CalcZ;

                    for (x = -1; x < voxelEnd; ++x)
                    {
                        IndexX = (x + 1) * vlwnc2;
                        CalcX = x >= 0 && x < voxelLength;

                        for (y = -1; y < voxelEnd; ++y)
                        {
                            IndexY = (y + 1) * vlwnc;
                            CalcY = y >= 0 && y < voxelLength;

                            Inited = false;

                            for (z = -1; z < voxelEnd; ++z)
                            {
                                CalcZ = z >= 0 && z < voxelLength;
                                // depending on the voxel-neighbour- the count and look, of the triangles gets calculated.
                                tableIndex = 0;

                                HasColor = false;
                                Index = IndexX + IndexY + (z + 1);

                                if (!Inited)
                                {
                                    Inited = true;
                                    Voxel0 = VoxelArray.voxels[Index];
                                    Right1 = VoxelArray.voxels[Index + vlwnc2];
                                    Forward2 = VoxelArray.voxels[Index + 1];
                                    RightForward3 = VoxelArray.voxels[Index + vlwnc2 + 1];
                                    Top4 = VoxelArray.voxels[Index + vlwnc];
                                    RightTop5 = VoxelArray.voxels[Index + vlwnc2 + vlwnc];
                                    TopForward6 = VoxelArray.voxels[Index + vlwnc + 1];
                                    RightTopForward7 = VoxelArray.voxels[Index + vlwnc2 + vlwnc + 1];

                                    if (Voxel0.VType != MaterialId) Voxel0.Volume = TerrainInformation.Isofix;
                                    if (Right1.VType != MaterialId) Right1.Volume = TerrainInformation.Isofix;
                                    if (Forward2.VType != MaterialId) Forward2.Volume = TerrainInformation.Isofix;
                                    if (RightForward3.VType != MaterialId) RightForward3.Volume = TerrainInformation.Isofix;
                                    if (Top4.VType != MaterialId) Top4.Volume = TerrainInformation.Isofix;
                                    if (RightTop5.VType != MaterialId) RightTop5.Volume = TerrainInformation.Isofix;
                                    if (TopForward6.VType != MaterialId) TopForward6.Volume = TerrainInformation.Isofix;
                                    if (RightTopForward7.VType != MaterialId) RightTopForward7.Volume = TerrainInformation.Isofix;
                                }
                                else
                                {
                                    Voxel0 = Forward2;
                                    Right1 = RightForward3;
                                    Top4 = TopForward6;
                                    RightTop5 = RightTopForward7;

                                    Forward2 = VoxelArray.voxels[Index + 1];
                                    RightForward3 = VoxelArray.voxels[Index + vlwnc2 + 1];
                                    TopForward6 = VoxelArray.voxels[Index + vlwnc + 1];
                                    RightTopForward7 = VoxelArray.voxels[Index + vlwnc2 + vlwnc + 1];

                                    if (Forward2.VType != MaterialId) Forward2.Volume = TerrainInformation.Isofix;
                                    if (RightForward3.VType != MaterialId) RightForward3.Volume = TerrainInformation.Isofix;
                                    if (TopForward6.VType != MaterialId) TopForward6.Volume = TerrainInformation.Isofix;
                                    if (RightTopForward7.VType != MaterialId) RightTopForward7.Volume = TerrainInformation.Isofix;
                                }

                                if (Voxel0.Volume < Isolevel) tableIndex |= 1;
                                if (Right1.Volume < Isolevel) tableIndex |= 2;
                                if (Forward2.Volume < Isolevel) tableIndex |= 4;
                                if (RightForward3.Volume < Isolevel) tableIndex |= 8;
                                if (Top4.Volume < Isolevel) tableIndex |= 16;
                                if (RightTop5.Volume < Isolevel) tableIndex |= 32;
                                if (TopForward6.Volume < Isolevel) tableIndex |= 64;
                                if (RightTopForward7.Volume < Isolevel) tableIndex |= 128;

                                // Now create a triangulation of the isosurface in this
                                // cell.
                                if (tableIndex != 0 && tableIndex != 255)
                                {
                                    if (GenerateColor)
                                    {
                                        if (Voxel0.VOriginalType != 0)
                                        {
                                            MarchingInfo = Palette.VoxelBlocks[Voxel0.VOriginalType];
                                            if (MarchingInfo.UseSplatMap)
                                            {
                                                CurrentColor = MarchingInfo.SplatColor;
                                                HasColor = true;
                                            }
                                        }

                                        if (!HasColor && TopForward6.VOriginalType != 0)
                                        {
                                            MarchingInfo = Palette.VoxelBlocks[TopForward6.VOriginalType];
                                            if (MarchingInfo.UseSplatMap)
                                            {
                                                CurrentColor = MarchingInfo.SplatColor;
                                                HasColor = true;
                                            }
                                        }

                                        if (!HasColor && RightTopForward7.VOriginalType != 0)
                                        {
                                            MarchingInfo = Palette.VoxelBlocks[RightTopForward7.VOriginalType];
                                            if (MarchingInfo.UseSplatMap)
                                            {
                                                CurrentColor = MarchingInfo.SplatColor;
                                                HasColor = true;
                                            }
                                        }

                                        if (!HasColor && RightForward3.VOriginalType != 0)
                                        {
                                            MarchingInfo = Palette.VoxelBlocks[RightForward3.VOriginalType];
                                            if (MarchingInfo.UseSplatMap)
                                            {
                                                CurrentColor = MarchingInfo.SplatColor;
                                                HasColor = true;
                                            }
                                        }


                                        if (!HasColor)
                                            CurrentColor = SplatInfo.SplatColor;

                                        CurrentColor = (CurrentColor + Palette.VoxelBlocks[Right1.VOriginalType].SplatColor + Palette.VoxelBlocks[Forward2.VOriginalType].SplatColor + Palette.VoxelBlocks[Top4.VOriginalType].SplatColor) * 0.25f;
                                        TotalColor = CurrentColor.a + CurrentColor.r + CurrentColor.g + CurrentColor.b;
                                        if (TotalColor > 1f)
                                        {
                                            TotalColor = (TotalColor - 1f) * 0.25f;
                                            CurrentColor.a -= TotalColor;
                                            CurrentColor.r -= TotalColor;
                                            CurrentColor.g -= TotalColor;
                                            CurrentColor.b -= TotalColor;
                                        }

                                    }

                                    // OPTIMISE: too many vertices get calculated because of normalcorrection; optimise!
                                    data = TransvoxelTables.regularCellData[TransvoxelTables.regularCellClass[tableIndex]];
                                    VertexCount = (data.geometryCounts >> 4);
                                    for (ind = 0; ind < VertexCount; ++ind)
                                    {
                                        data2 = TransvoxelTables.regularVertexData[tableIndex][ind];
                                        corner0 = data2 & 0x0F;
                                        corner1 = (data2 & 0xF0) >> 4;
                                        //id = calculateEdgeId(Data, x, y, z, data2 >> 8); // for reuse
                                        owner = (((data2 >> 8) & 0xF0) >> 4);
                                        id = (((x - (owner % 2) + 2) * 3) * ((vlwnc) * 3) * ((vlwnc) * 3) + ((y - ((owner >> 2) % 2) + 2) * 3) * ((vlwnc) * 3) + ((z - ((owner >> 1) % 2) + 2) * 3)) + (((data2 >> 8) & 0x0F) - 1);

                                        if (VoxelArray.vertexIndicesReuse[id] == -1)
                                        {
                                            switch (corner0)
                                            {
                                                case 0:
                                                case 8: Corner0 = Voxel0; Corner0Position.x = x; Corner0Position.y = y; Corner0Position.z = z; break;
                                                case 1: Corner0 = Right1; Corner0Position.x = x + 1; Corner0Position.y = y; Corner0Position.z = z; break;
                                                case 2: Corner0 = Forward2; Corner0Position.x = x; Corner0Position.y = y; Corner0Position.z = z + 1; break;
                                                case 3: Corner0 = RightForward3; Corner0Position.x = x + 1; Corner0Position.y = y; Corner0Position.z = z + 1; break;
                                                case 4: Corner0 = Top4; Corner0Position.x = x; Corner0Position.y = y + 1; Corner0Position.z = z; break;
                                                case 5: Corner0 = RightTop5; Corner0Position.x = x + 1; Corner0Position.y = y + 1; Corner0Position.z = z; break;
                                                case 6: Corner0 = TopForward6; Corner0Position.x = x; Corner0Position.y = y + 1; Corner0Position.z = z + 1; break;
                                                case 7: Corner0 = RightTopForward7; Corner0Position.x = x + 1; Corner0Position.y = y + 1; Corner0Position.z = z + 1; break;
                                                default: Corner0 = Voxel0; Corner0Position.x = x; Corner0Position.y = y; Corner0Position.z = z; break;
                                            }
                                            switch (corner1)
                                            {
                                                case 0:
                                                case 8: Corner1 = Voxel0; Corner1Position.x = x; Corner1Position.y = y; Corner1Position.z = z; break;
                                                case 1: Corner1 = Right1; Corner1Position.x = x + 1; Corner1Position.y = y; Corner1Position.z = z; break;
                                                case 2: Corner1 = Forward2; Corner1Position.x = x; Corner1Position.y = y; Corner1Position.z = z + 1; break;
                                                case 3: Corner1 = RightForward3; Corner1Position.x = x + 1; Corner1Position.y = y; Corner1Position.z = z + 1; break;
                                                case 4: Corner1 = Top4; Corner1Position.x = x; Corner1Position.y = y + 1; Corner1Position.z = z; break;
                                                case 5: Corner1 = RightTop5; Corner1Position.x = x + 1; Corner1Position.y = y + 1; Corner1Position.z = z; break;
                                                case 6: Corner1 = TopForward6; Corner1Position.x = x; Corner1Position.y = y + 1; Corner1Position.z = z + 1; break;
                                                case 7: Corner1 = RightTopForward7; Corner1Position.x = x + 1; Corner1Position.y = y + 1; Corner1Position.z = z + 1; break;
                                                default: Corner1 = Voxel0; Corner1Position.x = x; Corner1Position.y = y; Corner1Position.z = z; break;
                                            }

                                            volumValue = (Isolevel - Corner0.Volume) / (Corner1.Volume - Corner0.Volume);
                                            point.x = (((float)Corner0Position.x + volumValue * ((float)Corner1Position.x - (float)Corner0Position.x)) * voxelSize * Scale);
                                            point.y = (((float)Corner0Position.y + volumValue * ((float)Corner1Position.y - (float)Corner0Position.y)) * voxelSize * Scale);
                                            point.z = (((float)Corner0Position.z + volumValue * ((float)Corner1Position.z - (float)Corner0Position.z)) * voxelSize * Scale);


                                            Info.Triangles.Vertices.Add(point);
                                            Info.Triangles.Normals.Add(Vector3.zero);
                                            if (GenerateColor)
                                                Info.Colors.Add(HasColor ? CurrentColor : Black);

                                            if (GenerateUV)
                                            {
                                                point.x += ChunkWorldPosition.x;
                                                point.y += ChunkWorldPosition.y;
                                                point.z += ChunkWorldPosition.z;
                                                UV.x = point.x + point.y;
                                                UV.y = point.y + point.z;
                                                Info.Uvs.Add(UV);
                                            }

                                            //BASSERT(m_positions.size() == m_normals.size());
                                            VoxelArray.ids[ind] = VoxelArray.vertexIndicesReuse[id] = Info.Triangles.Vertices.Count - 1;
                                        }
                                        else
                                        {
                                            VoxelArray.ids[ind] = VoxelArray.vertexIndicesReuse[id];
                                        }
                                    }

                                    calculateFaces = CalcX && CalcY && CalcZ;

                                    VertexCount = (data.geometryCounts & 0x0F) * 3;
                                    for (ind = 0; ind < VertexCount; ind += 3)
                                    {
                                        // calc normal
                                        vertexIndex1 = VoxelArray.ids[data.vertexIndex[ind + 1]];
                                        vertex1 = Info.Triangles.Vertices.array[(vertexIndex1)];
                                        vertexIndex2 = VoxelArray.ids[data.vertexIndex[ind + 2]];
                                        vertex2 = Info.Triangles.Vertices.array[(vertexIndex2)];

                                        if (Vector3.Distance(vertex1, vertex2) < 0.0001f)
                                        {
                                            continue; // triangle with zero space
                                        }

                                        vertexIndex0 = VoxelArray.ids[data.vertexIndex[ind + 0]];
                                        vertex0 = Info.Triangles.Vertices.array[(vertexIndex0)];

                                        if (Vector3.Distance(vertex0, vertex1) < 0.0001f || Vector3.Distance(vertex0, vertex2) < 0.0001f)
                                        {
                                            continue; // triangle with zero space
                                        }

                                        addNormal = Vector3.Cross((vertex1 - vertex0), (vertex2 - vertex0));//.normalisedCopy();



                                        Info.Triangles.Normals.array[vertexIndex0] += addNormal;
                                        Info.Triangles.Normals.array[vertexIndex1] += addNormal;
                                        Info.Triangles.Normals.array[vertexIndex2] += addNormal;

                                        if (calculateFaces)
                                        {
                                            // insert new triangle
                                            Info.Triangles.Indices.CheckArray(3);
                                            Info.Triangles.Indices.array[Info.Triangles.Indices.Count++] = Info.TriangleIndex + vertexIndex0;
                                            Info.Triangles.Indices.array[Info.Triangles.Indices.Count++] = Info.TriangleIndex + vertexIndex1;
                                            Info.Triangles.Indices.array[Info.Triangles.Indices.Count++] = Info.TriangleIndex + vertexIndex2;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Debug.Log(Node.m_position + " Generate Mesh LOD : " + LodIndex + ",Indices:" + m_indices.Count);
                }

                #endregion

                if (ThreadManager.EnableStats)
                    Thread.AddStats("Client:GenerateNode", Thread.Time());

                if (ThreadManager.EnableStats)
                    Thread.RecordTime();

                #region GenerateLOD

                // transvoxel
                if (m_lod > 0)
                {
                    TransvoxelTables.TransitionCellData data;

                    bool invertTriangles, calculateVertexPosition;
                    ushort ind, invert = 1;
                    uint x, y, z, classIndex, tableIndex;
                    int coord, id, newEdge;
                    int data2, edge, edgeId, edgeBetween, owner, newEdgeId, newOwner;
                    int vertexIndex0, vertexIndex1, vertexIndex2;

                    Vector3 vertex0, vertex1, vertex2, point;
                    VectorI3[] ToIt, voxelLookups;
                    VectorI3 start, end, Reuse, Look0, Look1, Look2, Look3, Look4, Look5, Look6, Look7, Look8, corn0, corn1;

                    float InterpoFrom, InterpoTo;
                    int lookUpX, lookUpY, lookUpZ, lodvcount = 0, vlenghtlod = Data.TransvoxelInfo.vlenghtlod;

                    // the indexer for the vertices. *3 because gets saved with edge-id
                    List<int> IndicesLOD = null;

                    int ReuseLenght = VoxelArray.vertexIndicesReuseLod.Length - 1;
                    // 0:Left
                    // 1:Right
                    // 2:Bottom
                    // 3:Top
                    // 4:Backward
                    // 5:Forward
                    int i;
                    for (ushort lod = 0; lod < 6; ++lod)
                    {
                        coord = (lod / 2);
                        ToIt = TransvoxelInfo.toIterateLOD[lod];
                        start = ToIt[0];
                        end = ToIt[1];
                        invertTriangles = (toInvertTriangles[lod]);
                        IndicesLOD = VoxelArray.m_indicesLod[lod];
                        Reuse = reuseCorrection[lod];

                        // the indexer for the vertices. *3 because gets saved with edge-id
                        voxelLookups = Lookups[coord];
                        Look0 = voxelLookups[0];
                        Look1 = voxelLookups[1];
                        Look2 = voxelLookups[2];
                        Look3 = voxelLookups[3];
                        Look4 = voxelLookups[4];
                        Look5 = voxelLookups[5];
                        Look6 = voxelLookups[6];
                        Look7 = voxelLookups[7];
                        Look8 = voxelLookups[8];
                        lodvcount = lod * TransvoxelInfo.vcountlod;

                        for (i = ReuseLenght; i >= 0; --i)
                            VoxelArray.vertexIndicesReuseLod[i] = -1;

                        for (x = (uint)start.x; x < (uint)end.x; x += 2)
                        {
                            lookUpX = (int)x - start.x;

                            for (y = (uint)start.y; y < (uint)end.y; y += 2)
                            {
                                lookUpY = (int)y - start.y;

                                for (z = (uint)start.z; z < (uint)end.z; z += 2)
                                {
                                    lookUpZ = (int)z - start.z;

                                    HasColor = false;
                                    tableIndex = 0;

                                    if (lod < 2)
                                    {
                                        Voxel0 = VoxelArray.voxelslod[lodvcount + (lookUpY + Look0.y) * vlenghtlod + (lookUpZ + Look0.z)];
                                        Right1 = VoxelArray.voxelslod[lodvcount + (lookUpY + Look1.y) * vlenghtlod + (lookUpZ + Look1.z)];
                                        Forward2 = VoxelArray.voxelslod[lodvcount + (lookUpY + Look2.y) * vlenghtlod + (lookUpZ + Look2.z)];
                                        RightForward3 = VoxelArray.voxelslod[lodvcount + (lookUpY + Look3.y) * vlenghtlod + (lookUpZ + Look3.z)];
                                        Top4 = VoxelArray.voxelslod[lodvcount + (lookUpY + Look4.y) * vlenghtlod + (lookUpZ + Look4.z)];
                                        RightTop5 = VoxelArray.voxelslod[lodvcount + (lookUpY + Look5.y) * vlenghtlod + (lookUpZ + Look5.z)];
                                        TopForward6 = VoxelArray.voxelslod[lodvcount + (lookUpY + Look6.y) * vlenghtlod + (lookUpZ + Look6.z)];
                                        RightTopForward7 = VoxelArray.voxelslod[lodvcount + (lookUpY + Look7.y) * vlenghtlod + (lookUpZ + Look7.z)];
                                        Voxel8 = VoxelArray.voxelslod[lodvcount + (lookUpY + Look8.y) * vlenghtlod + (lookUpZ + Look8.z)];
                                    }
                                    else if (lod < 4)
                                    {
                                        Voxel0 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look0.x) * vlenghtlod + (lookUpZ + Look0.z)];
                                        Right1 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look1.x) * vlenghtlod + (lookUpZ + Look1.z)];
                                        Forward2 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look2.x) * vlenghtlod + (lookUpZ + Look2.z)];
                                        RightForward3 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look3.x) * vlenghtlod + (lookUpZ + Look3.z)];
                                        Top4 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look4.x) * vlenghtlod + (lookUpZ + Look4.z)];
                                        RightTop5 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look5.x) * vlenghtlod + (lookUpZ + Look5.z)];
                                        TopForward6 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look6.x) * vlenghtlod + (lookUpZ + Look6.z)];
                                        RightTopForward7 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look7.x) * vlenghtlod + (lookUpZ + Look7.z)];
                                        Voxel8 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look8.x) * vlenghtlod + (lookUpZ + Look8.z)];
                                    }
                                    else
                                    {
                                        Voxel0 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look0.x) * vlenghtlod + (lookUpY + Look0.y)];
                                        Right1 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look1.x) * vlenghtlod + (lookUpY + Look1.y)];
                                        Forward2 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look2.x) * vlenghtlod + (lookUpY + Look2.y)];
                                        RightForward3 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look3.x) * vlenghtlod + (lookUpY + Look3.y)];
                                        Top4 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look4.x) * vlenghtlod + (lookUpY + Look4.y)];
                                        RightTop5 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look5.x) * vlenghtlod + (lookUpY + Look5.y)];
                                        TopForward6 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look6.x) * vlenghtlod + (lookUpY + Look6.y)];
                                        RightTopForward7 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look7.x) * vlenghtlod + (lookUpY + Look7.y)];
                                        Voxel8 = VoxelArray.voxelslod[lodvcount + (lookUpX + Look8.x) * vlenghtlod + (lookUpY + Look8.y)];
                                    }

                                    if (Voxel0.VType != MaterialId) Voxel0.Volume = TerrainInformation.Isofix;
                                    if (Right1.VType != MaterialId) Right1.Volume = TerrainInformation.Isofix;
                                    if (Forward2.VType != MaterialId) Forward2.Volume = TerrainInformation.Isofix;
                                    if (RightForward3.VType != MaterialId) RightForward3.Volume = TerrainInformation.Isofix;
                                    if (Top4.VType != MaterialId) Top4.Volume = TerrainInformation.Isofix;
                                    if (RightTop5.VType != MaterialId) RightTop5.Volume = TerrainInformation.Isofix;
                                    if (TopForward6.VType != MaterialId) TopForward6.Volume = TerrainInformation.Isofix;
                                    if (RightTopForward7.VType != MaterialId) RightTopForward7.Volume = TerrainInformation.Isofix;
                                    if (Voxel8.VType != MaterialId) Voxel8.Volume = TerrainInformation.Isofix;

                                    if (Voxel0.Volume < Isolevel) tableIndex |= 1;
                                    if (Right1.Volume < Isolevel) tableIndex |= 2;
                                    if (Forward2.Volume < Isolevel) tableIndex |= 4;
                                    if (RightForward3.Volume < Isolevel) tableIndex |= 8;
                                    if (Top4.Volume < Isolevel) tableIndex |= 16;
                                    if (RightTop5.Volume < Isolevel) tableIndex |= 32;
                                    if (TopForward6.Volume < Isolevel) tableIndex |= 64;
                                    if (RightTopForward7.Volume < Isolevel) tableIndex |= 128;
                                    if (Voxel8.Volume < Isolevel) tableIndex |= 256;

                                    if (tableIndex == 0 || tableIndex == 511) // no triangles
                                    {
                                        continue;
                                    }


                                    if (GenerateColor)
                                    {
                                        if (Voxel0.VOriginalType != 0)
                                        {
                                            MarchingInfo = Palette.VoxelBlocks[Voxel0.VOriginalType];
                                            if (MarchingInfo.UseSplatMap)
                                            {
                                                CurrentColor = MarchingInfo.SplatColor;
                                                HasColor = true;
                                            }
                                        }

                                        if (!HasColor && TopForward6.VOriginalType != 0)
                                        {
                                            MarchingInfo = Palette.VoxelBlocks[TopForward6.VOriginalType];
                                            if (MarchingInfo.UseSplatMap)
                                            {
                                                CurrentColor = MarchingInfo.SplatColor;
                                                HasColor = true;
                                            }
                                        }

                                        if (!HasColor && RightTopForward7.VOriginalType != 0)
                                        {
                                            MarchingInfo = Palette.VoxelBlocks[RightTopForward7.VOriginalType];
                                            if (MarchingInfo.UseSplatMap)
                                            {
                                                CurrentColor = MarchingInfo.SplatColor;
                                                HasColor = true;
                                            }
                                        }

                                        if (!HasColor && RightForward3.VOriginalType != 0)
                                        {
                                            MarchingInfo = Palette.VoxelBlocks[RightForward3.VOriginalType];
                                            if (MarchingInfo.UseSplatMap)
                                            {
                                                CurrentColor = MarchingInfo.SplatColor;
                                                HasColor = true;
                                            }
                                        }


                                        if (!HasColor)
                                            CurrentColor = SplatInfo.SplatColor;
                                    }

                                    classIndex = TransvoxelTables.transitionCellClass[tableIndex];
                                    data = TransvoxelTables.transitionCellData[classIndex & 0x7F]; // only the last 7 bit count
                                    VertexCount = (data.geometryCounts >> 4);

                                    for (ind = 0; ind < VertexCount; ++ind)
                                    {
                                        data2 = TransvoxelTables.transitionVertexData[tableIndex][ind];
                                        edge = (data2 >> 8);
                                        edgeId = (edge & 0x0F);
                                        edgeBetween = (data2 & 0xFF);

                                        if (edgeId == 0x9 || edgeId == 0x8)
                                        {
                                            owner = ((edge & 0xF0) >> 4);
                                            newEdgeId = 0;
                                            newOwner = 0;

                                            if (coord == 0)
                                            {
                                                if (edgeId == 0x8)
                                                {
                                                    newEdgeId = 0x3;
                                                }
                                                else
                                                {
                                                    newEdgeId = 0x1;
                                                }
                                                if (owner == 0x1)
                                                {
                                                    newOwner = 0x4;
                                                }
                                                else if (owner == 0x2)
                                                {
                                                    newOwner = 0x2;
                                                }
                                            }
                                            else if (coord == 1)
                                            {
                                                if (edgeId == 0x8)
                                                {
                                                    newEdgeId = 0x2;
                                                }
                                                else
                                                {
                                                    newEdgeId = 0x1;
                                                }
                                                newOwner = owner;
                                            }
                                            else if (coord == 2)
                                            {
                                                newEdgeId = (ushort)(edgeId - 6);
                                                if (owner == 0x1)
                                                {
                                                    newOwner = 0x1;
                                                }
                                                else if (owner == 0x2)
                                                {
                                                    newOwner = 0x4;
                                                }
                                            }


                                            newEdge = ((newOwner << 4) | newEdgeId);
                                            id = calculateEdgeId(vlwnc, (int)(x * 0.5f) - Reuse.x, (int)(y * 0.5f) - Reuse.y, (int)(z * 0.5f) - Reuse.z, newEdge);

                                            if ((VoxelArray.idsLOD[ind] = (uint)VoxelArray.vertexIndicesReuse[id]) >= Info.Triangles.Normals.Count)
                                                VoxelArray.idsLOD[ind] = 0;


                                            switch (edgeBetween)
                                            {
                                                case 0x9A:
                                                    VoxelArray.normalsForTransvoxel[0] = Info.Triangles.Normals.array[(int)VoxelArray.idsLOD[ind]];
                                                    break;
                                                case 0xAC:
                                                    VoxelArray.normalsForTransvoxel[1] = Info.Triangles.Normals.array[(int)VoxelArray.idsLOD[ind]];
                                                    break;
                                                case 0xBC:
                                                    VoxelArray.normalsForTransvoxel[2] = Info.Triangles.Normals.array[(int)VoxelArray.idsLOD[ind]];
                                                    break;
                                                case 0x9B:
                                                    VoxelArray.normalsForTransvoxel[3] = Info.Triangles.Normals.array[(int)VoxelArray.idsLOD[ind]];
                                                    break;
                                                default:
                                                    break;
                                                //BASSERT(false);
                                            }
                                        }
                                    }

                                    for (ind = 0; ind < VertexCount; ++ind)
                                    {
                                        data2 = TransvoxelTables.transitionVertexData[tableIndex][ind];
                                        edge = (ushort)(data2 >> 8);
                                        edgeId = (ushort)(edge & 0x0F);
                                        edgeBetween = (ushort)(data2 & 0xFF);

                                        if (edgeId != 0x9 && edgeId != 0x8)
                                        {
                                            owner = (int)((edge & 0xF0) >> 4);

                                            if (owner == 4) // no reuse
                                            {
                                                id = -1;
                                            }
                                            else
                                            {
                                                switch (coord)
                                                {
                                                    case 0:
                                                        id = calculateVertexIdTransvoxel(voxelLength, (int)((int)y * 0.5f) - (int)(owner % 2), (int)((int)z * 0.5f) - (int)((owner >> 1) % 2)) + ((int)(edge & 0x0F) - 3);
                                                        break;
                                                    case 1:
                                                        id = calculateVertexIdTransvoxel(voxelLength, (int)((int)x * 0.5f) - (int)(owner % 2), (int)((int)z * 0.5f) - (int)((owner >> 1) % 2)) + ((int)(edge & 0x0F) - 3);
                                                        break;
                                                    case 2:
                                                        id = calculateVertexIdTransvoxel(voxelLength, (int)((int)x * 0.5f) - (int)(owner % 2), (int)((int)y * 0.5f) - (int)((owner >> 1) % 2)) + ((int)(edge & 0x0F) - 3);
                                                        break;
                                                    default:
                                                        id = -1;
                                                        break;
                                                }
                                            }

                                            calculateVertexPosition = id == -1;

                                            if (!calculateVertexPosition)
                                            {
                                                calculateVertexPosition = VoxelArray.vertexIndicesReuseLod[id] == -1;
                                            }

                                            if (calculateVertexPosition)
                                            {
                                                switch ((ushort)(data2 & 0x0F))
                                                {
                                                    case 0x0: corn0.x = 0; corn0.y = 0; corn0.z = 0; break;
                                                    case 0x1: corn0 = voxelLookups[1]; break;
                                                    case 0x2: corn0 = voxelLookups[2]; break;
                                                    case 0x3: corn0 = voxelLookups[7]; break;
                                                    case 0x4: corn0 = voxelLookups[8]; break;
                                                    case 0x5: corn0 = voxelLookups[3]; break;
                                                    case 0x6: corn0 = voxelLookups[6]; break;
                                                    case 0x7: corn0 = voxelLookups[5]; break;
                                                    case 0x8: corn0 = voxelLookups[4]; break;
                                                    case 0x9:
                                                    case 0xA:
                                                    case 0xB:
                                                    case 0xC:
                                                    default: corn0.x = 0; corn0.y = 0; corn0.z = 0; break;
                                                }

                                                switch ((ushort)((data2 & 0xF0) >> 4))
                                                {
                                                    case 0x0: corn1.x = 0; corn1.y = 0; corn1.z = 0; break;
                                                    case 0x1: corn1 = voxelLookups[1]; break;
                                                    case 0x2: corn1 = voxelLookups[2]; break;
                                                    case 0x3: corn1 = voxelLookups[7]; break;
                                                    case 0x4: corn1 = voxelLookups[8]; break;
                                                    case 0x5: corn1 = voxelLookups[3]; break;
                                                    case 0x6: corn1 = voxelLookups[6]; break;
                                                    case 0x7: corn1 = voxelLookups[5]; break;
                                                    case 0x8: corn1 = voxelLookups[4]; break;
                                                    case 0x9:
                                                    case 0xA:
                                                    case 0xB:
                                                    case 0xC:
                                                    default: corn1.x = 0; corn1.y = 0; corn1.z = 0; break;
                                                }

                                                InterpoFrom = VoxelArray.getVoxelLodVolume(corn0.x + (int)x, corn0.y + (int)y, corn0.z + (int)z, lod, MaterialId);
                                                InterpoTo = VoxelArray.getVoxelLodVolume(corn1.x + (int)x, corn1.y + (int)y, corn1.z + (int)z, lod, MaterialId) - InterpoFrom;

                                                point.x = corn0.x + ((Isolevel - InterpoFrom) / InterpoTo) * (corn1.x - corn0.x);
                                                point.y = corn0.y + ((Isolevel - InterpoFrom) / InterpoTo) * (corn1.y - corn0.y);
                                                point.z = corn0.z + ((Isolevel - InterpoFrom) / InterpoTo) * (corn1.z - corn0.z);

                                                point.x = (point.x + lookUpX + (float)start.x) * 0.5f * voxelSize * Scale;
                                                point.y = (point.y + lookUpY + (float)start.y) * 0.5f * voxelSize * Scale;
                                                point.z = (point.z + lookUpZ + (float)start.z) * 0.5f * voxelSize * Scale;

                                                Info.Triangles.Vertices.Add(point);

                                                if (GenerateColor)
                                                    Info.Colors.Add(HasColor ? CurrentColor : new Color(1, 0, 0, 0));

                                                if (GenerateUV)
                                                {
                                                    point.x += ChunkWorldPosition.x;
                                                    point.y += ChunkWorldPosition.y;
                                                    point.z += ChunkWorldPosition.z;
                                                    UV.x = point.x + point.y;
                                                    UV.y = point.y + point.z;
                                                    Info.Uvs.Add(UV);
                                                }

                                                switch (edgeBetween)
                                                {
                                                    case 0x01:
                                                    case 0x12:
                                                        Info.Triangles.Normals.Add(VoxelArray.normalsForTransvoxel[0]);
                                                        break;
                                                    case 0x03:
                                                    case 0x36:
                                                        Info.Triangles.Normals.Add(VoxelArray.normalsForTransvoxel[3]);
                                                        break;
                                                    case 0x25:
                                                    case 0x58:
                                                        Info.Triangles.Normals.Add(VoxelArray.normalsForTransvoxel[1]);
                                                        break;
                                                    case 0x67:
                                                    case 0x78:
                                                        Info.Triangles.Normals.Add(VoxelArray.normalsForTransvoxel[2]);
                                                        break;
                                                    case 0x34:
                                                    case 0x14:
                                                    case 0x45:
                                                    case 0x47:
                                                        Info.Triangles.Normals.Add(VoxelArray.normalsForTransvoxel[0] + VoxelArray.normalsForTransvoxel[1] + VoxelArray.normalsForTransvoxel[2] + VoxelArray.normalsForTransvoxel[3]);
                                                        break;
                                                    default:
                                                        break;
                                                    //BASSERT(false);
                                                }
                                                //BASSERT(m_positions.size() == m_normals.size());

                                                if (id == -1)
                                                {
                                                    VoxelArray.idsLOD[ind] = (uint)Info.Triangles.Vertices.Count - 1;
                                                }
                                                else
                                                {
                                                    VoxelArray.idsLOD[ind] = (uint)(Info.Triangles.Vertices.Count - 1);
                                                    VoxelArray.vertexIndicesReuseLod[id] = (Info.Triangles.Vertices.Count - 1);
                                                }
                                            }
                                            else
                                            {
                                                VoxelArray.idsLOD[ind] = (uint)VoxelArray.vertexIndicesReuseLod[id];
                                            }
                                        }
                                    }

                                    VertexCount = (data.geometryCounts & 0x0F) * 3;
                                    for (ind = 0; ind < VertexCount; ind += 3)
                                    {
                                        // calc normal
                                        vertexIndex0 = (int)VoxelArray.idsLOD[data.vertexIndex[ind + 0]];
                                        vertex0 = Info.Triangles.Vertices.array[vertexIndex0];

                                        vertexIndex2 = (int)VoxelArray.idsLOD[data.vertexIndex[ind + 2]];
                                        vertex2 = Info.Triangles.Vertices.array[vertexIndex2];


                                        if (Vector3.Distance(vertex0,vertex2) < 0.001f)
                                        {
                                            continue; // triangle with zero space
                                        }

                                        vertexIndex1 = (int)VoxelArray.idsLOD[data.vertexIndex[ind + 1]];
                                        vertex1 = Info.Triangles.Vertices.array[vertexIndex1];

                                        if (Vector3.Distance(vertex1,vertex2) < 0.001f || Vector3.Distance(vertex0,vertex1) < 0.001f)
                                        {
                                            continue; // triangle with zero space
                                        }

                                        // Vertex2 all time existing vertex

                                        // insert new triangle
                                        invert = 1;
                                        if (invertTriangles)
                                        {
                                            invert = 0;
                                        }
                                        IndicesLOD.Add(vertexIndex0);
                                        if ((classIndex >> 7) % 2 == invert)
                                        {
                                            IndicesLOD.Add(vertexIndex1);
                                            IndicesLOD.Add(vertexIndex2);
                                        }
                                        else
                                        {
                                            IndicesLOD.Add(vertexIndex2);
                                            IndicesLOD.Add(vertexIndex1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                #endregion

                if (ThreadManager.EnableStats)
                    Thread.AddStats("Client:GenerateNodeLOD", Thread.Time());

                if (Info.Triangles.Indices.Count > 0)
                {
                    List<int> TempIndices;
                    int Len = 0;
                    int i, j;

                    for (i = 0; i < 6; ++i)
                    {
                        TempIndices = VoxelArray.m_indicesLod[i];
                        Len = TempIndices.Count;
                        Info.Triangles.Indices.CheckArray(Len);
                        for (j = 0; j < Len; ++j)
                        {
                            Info.Triangles.Indices.AddSafe(Info.TriangleIndex + TempIndices[j]);
                        }

                    }

                    Info.Vertices.AddRange(Info.Triangles.Vertices);
                    Info.Normals.AddRange(Info.Triangles.Normals);
                    Info.TriangleIndex = (ushort)Info.Vertices.Count;
                    Info.TempMaterials.Add(TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>((byte)MaterialId).MaterialInfo);
                    Info.TempIndices.Add(Info.Triangles.Indices.ToArray());
                    Info.Triangles.Clear();
                    ++VoxelArray.MaterialCount;
                }
            }


            if (VoxelArray.MaterialCount != 0)
            {
                if (ThreadManager.EnableStats)
                    Thread.RecordTime();

                MData.Clear();
                MData.SetVertices(Info.Vertices.ToArray());
                MData.SetUVs(Info.Uvs.ToArray());
                MData.SetNormals(Info.Normals.ToArray());
                MData.SetColors(Info.Colors.ToArray());

                MData.SetIndices(PoolManager.UPool.GetIndicesArray(VoxelArray.MaterialCount));
                MData.SetMaterials(PoolManager.UPool.GetMaterials(Info.TempMaterials));

                Material[] Materials = MData.GetMaterials();
                int[][] Indices = MData.GetIndices();
                Vector3[] Normals = MData.GetNormals();

                int i;
                for (i = Materials.Length-1; i >= 0; --i)
                {
                    Indices[i] = Info.TempIndices.array[i];
                }

                if (Normals != null && Normals.Length != 0)
                {
                    for (i = Normals.Length - 1; i >= 0; --i)
                        Normals[i].Normalize();
                }


                if (ThreadManager.EnableStats)
                    Thread.AddStats("Client:ApplyNodeData", Thread.Time(), false);
            }

            VoxelArray.Close();
        }

        #region Transvoxel

        static public int calculateEdgeId(int voxelLenghtNormal, int x, int y, int z, int edgeInformation)
        {
            int owner = ((edgeInformation & 0xF0) >> 4);
            return (((x - (owner % 2) + 2) * 3) * ((voxelLenghtNormal) * 3) * ((voxelLenghtNormal) * 3) + ((y - ((owner >> 2) % 2) + 2) * 3) * ((voxelLenghtNormal) * 3) + ((z - ((owner >> 1) % 2) + 2) * 3)) + ((edgeInformation & 0x0F) - 1);
        }
        static public int calculateEdgeIdTransvoxel(int voxelLength, int x, int y, int z, int edgeInformation, int coord)
        {
            //int edge = (int)(edgeInformation & 0x0F);
            //int diffFst = (int)(owner % 2);
            //int diffSnd = (int)((owner >> 1) % 2);

            int owner = (int)((edgeInformation & 0xF0) >> 4);

            if (owner == 4) // no reuse
            {
                return -1;
            }

            switch (coord)
            {
                case 0:
                    return calculateVertexIdTransvoxel(voxelLength, y - (int)(owner % 2), z - (int)((owner >> 1) % 2)) + ((int)(edgeInformation & 0x0F) - 3);
                case 1:
                    return calculateVertexIdTransvoxel(voxelLength, x - (int)(owner % 2), z - (int)((owner >> 1) % 2)) + ((int)(edgeInformation & 0x0F) - 3);
                case 2:
                    return calculateVertexIdTransvoxel(voxelLength, x - (int)(owner % 2), y - (int)((owner >> 1) % 2)) + ((int)(edgeInformation & 0x0F) - 3);
                default:
                    break;
            }

            //BASSERT(false);
            return -1;
        }

        static public int calculateVertexIdTransvoxel(int voxelLength, int x, int y)
        {
            return (x + 1) * 4 * (voxelLength + 1) + (y + 1) * 4;
        }
        static public Vector3 calculateIntersectionPositionTransvoxel(TransvoxelTempData TempMesh, int x, int y, int z, int corner0, int corner1, VectorI3[] voxel, ushort lod, int MaterialId)
        {
            VectorI3 corn0, corn1;
            switch (corner0)
            {
                case 0x0: corn0.x = 0; corn0.y = 0; corn0.z = 0; break;
                case 0x1: corn0 = voxel[1]; break;
                case 0x2: corn0 = voxel[2]; break;
                case 0x3: corn0 = voxel[7]; break;
                case 0x4: corn0 = voxel[8]; break;
                case 0x5: corn0 = voxel[3]; break;
                case 0x6: corn0 = voxel[6]; break;
                case 0x7: corn0 = voxel[5]; break;
                case 0x8: corn0 = voxel[4]; break;
                case 0x9:
                case 0xA:
                case 0xB:
                case 0xC:
                default: corn0.x = 0; corn0.y = 0; corn0.z = 0; break;
            }

            switch (corner1)
            {
                case 0x0: corn1.x = 0; corn1.y = 0; corn1.z = 0; break;
                case 0x1: corn1 = voxel[1]; break;
                case 0x2: corn1 = voxel[2]; break;
                case 0x3: corn1 = voxel[7]; break;
                case 0x4: corn1 = voxel[8]; break;
                case 0x5: corn1 = voxel[3]; break;
                case 0x6: corn1 = voxel[6]; break;
                case 0x7: corn1 = voxel[5]; break;
                case 0x8: corn1 = voxel[4]; break;
                case 0x9:
                case 0xA:
                case 0xB:
                case 0xC:
                default: corn1.x = 0; corn1.y = 0; corn1.z = 0; break;
            }

            corn0 = getInterpolatedPosition(corn0, corn1,
                TempMesh.getVoxelLodVolume(corn0.x + x, corn0.y + y, corn0.z + z, lod, MaterialId),
                TempMesh.getVoxelLodVolume(corn1.x + x, corn1.y + y, corn1.z + z, lod, MaterialId));
            corn0.x += x;
            corn0.y += y;
            corn0.z += z;
            return corn0 * 0.5f;
        }

        static public VectorI3 calculateCornerTransvoxel(int corner, VectorI3[] voxel)
        {
            switch (corner)
            {
                case 0x0:
                    return new VectorI3(0, 0, 0);
                case 0x1:
                    return voxel[1];
                case 0x2:
                    return voxel[2];
                case 0x3:
                    return voxel[7];
                case 0x4:
                    return voxel[8];
                case 0x5:
                    return voxel[3];
                case 0x6:
                    return voxel[6];
                case 0x7:
                    return voxel[5];
                case 0x8:
                    return voxel[4];
                case 0x9:
                case 0xA:
                case 0xB:
                case 0xC:
                default:
                    break;
            }
            //BASSERT(false);
            return new VectorI3(0, 0, 0);
        }
        static public Vector3 getInterpolatedPosition(Vector3 positionFrom, Vector3 positionTo, float interpolationFrom, float interpolationTo)
        {
            return positionFrom + (((TerrainInformation.Isolevel - ((float)interpolationFrom)) / ((float)(interpolationTo - interpolationFrom)))) * (positionTo - positionFrom);
        }

        static public readonly VectorI3[] reuseCorrection = {
            new VectorI3(1, 0, 0),
            new VectorI3(1, 0, 0),
            new VectorI3(0, 1, 0),
            new VectorI3(0, 1, 0),
            new VectorI3(0, 0, 1),
            new VectorI3(0, 0, 1)
        };

            static public readonly bool[] toInvertTriangles = {
            false, true,
            true, false, // data from Eric Lengyel seems to have different axis-desc
            false, true
        };

            static public readonly VectorI3[][] Lookups = new VectorI3[][]
        {
            new VectorI3[] {new VectorI3(0, 0, 0),new VectorI3(0, 1, 0),new VectorI3(0, 2, 0),new VectorI3(0, 2, 1),new VectorI3(0, 2, 2),new VectorI3(0, 1, 2),new VectorI3(0, 0, 2),new VectorI3(0, 0, 1),new VectorI3(0, 1, 1)},
            new VectorI3[] {new VectorI3(0, 0, 0),new VectorI3(1, 0, 0),new VectorI3(2, 0, 0),new VectorI3(2, 0, 1),new VectorI3(2, 0, 2),new VectorI3(1, 0, 2),new VectorI3(0, 0, 2),new VectorI3(0, 0, 1),new VectorI3(1, 0, 1)},
            new VectorI3[]  {new VectorI3(0, 0, 0),new VectorI3(1, 0, 0),new VectorI3(2, 0, 0),new VectorI3(2, 1, 0),new VectorI3(2, 2, 0),new VectorI3(1, 2, 0),new VectorI3(0, 2, 0),new VectorI3(0, 1, 0),new VectorI3(1, 1, 0)},
        };

        #endregion
    }
}
