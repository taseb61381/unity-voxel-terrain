﻿using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;


public class TransvoxelManager : MarchingManager
{
    public TransvoxelInformation TransvoxelInfo;

    public ExecuteAction UpdateLODThread;
    public ExecuteAction UpdateLODUnity;

    public override TerrainBlock CreateTerrain()
    {
        return new TransvoxelBlock(this);
    }

    public override void OnState(LoadingStates State)
    {
        if (State == LoadingStates.STARTING)
        {
            UpdateLODThread = new ExecuteAction("Client:UpdateLOD", UpdateLOD);
            UpdateLODUnity = new ExecuteAction("Client:UpdateLOD", UpdateLOD);
            ActionProcessor.Add(UpdateLODThread, 0);
            ActionProcessor.AddToUnity(UpdateLODUnity);
        }
    }

    public override bool InitCustomData()
    {
        if (TransvoxelInfo == null)
            TransvoxelInfo = new TransvoxelInformation();

        TransvoxelInfo.Init(Informations);

        return base.InitCustomData();
    }

    #region LODUpdater

    public struct ToGenerateNodeInfo
    {
        public ushort BlockId; // BlockId
        public int ChunkId; // Chunk To Generate
        public byte Level; // Level of the node
        public bool Priority; // True, Must generate First
    }

    public ActionProgressCounter LODActions = new ActionProgressCounter();
    private List<ITerrainBlock> OrderedBlocks = new List<ITerrainBlock>();
    private List<KeyValuePair<int, bool>> UpdatedNodes = new List<KeyValuePair<int, bool>>(); // Updated Nodes, Index:Open.  0 = Closed, 1 = Subdivided
    private SList<ToGenerateNodeInfo> Infos = new SList<ToGenerateNodeInfo>(10);
    private SList<ChunkGenerateInfo> GenerateInfos = new SList<ChunkGenerateInfo>(10);
    public PoolInfoRef TerrainChunkGeneratePool;


    public bool UpdateLOD(ActionThread Thread)
    {
        if (!Thread.IsUnityThread())
        {
            if (LODActions.Count() == 0 || LODActions.IsDone())
            {
                ThreadManager.IsClientWorking = false;

                if (Builder.BlockBuilder.IsGenerating)
                    return false;

                if (ITerrainCameraHandler.Instance.Container.HasPositionChanged(4f))
                {
                    ThreadManager.IsClientWorking = true;

                    //Debug.Log("UdpateLOD : " + LODActions.Count() + ",Done:" + LODActions.IsDone());
                    LODActions.Clear();

                    Container.GetBlocks(OrderedBlocks);

                    OrderedBlocks.Sort(delegate(ITerrainBlock A, ITerrainBlock B)
                    {
                        Vector3 ClosestA = ITerrainCameraHandler.Instance.Container.GetClosest(A.CenterPosition);
                        Vector3 ClosestB = ITerrainCameraHandler.Instance.Container.GetClosest(B.CenterPosition);
                        return Vector3.Distance(A.CenterPosition, ClosestA) > Vector3.Distance(B.CenterPosition, ClosestB) ? 1 : -1;
                    });

                    Infos.Clear();
                    for (int i = 0; i < OrderedBlocks.Count; ++i)
                        UpdateBlock(Thread, OrderedBlocks[i]);
                    OrderedBlocks.Clear();
                    GenerateLODActions();
                    Infos.Clear();
                }
            }
        }
        else
        {
            if (LODActions.Count() != 0 && !LODActions.IsDone())
            {
                for (int i = LODActions.Count() - 1; i >= 0; --i)
                {
                    TerrainChunkGenerateAndBuildGroup Group = LODActions.Actions[i] as TerrainChunkGenerateAndBuildGroup;
                    if (Group != null && !Group.CanBuild)
                    {
                        if (Group.IsDone())
                            continue;

                        if (Group.BuildGroup == null)
                            return false;

                        if (Group.BuildGroup.Step < 1)
                            return false;
                    }
                }

                for (int i = LODActions.Count() - 1; i >= 0; --i)
                {
                    TerrainChunkGenerateAndBuildGroup Group = LODActions.Actions[i] as TerrainChunkGenerateAndBuildGroup;
                    if (Group == null || Group.CanBuild)
                        continue;

                    if (Group.IsDone())
                        continue;

                    if (Group.BuildGroup == null)
                        continue;

                    if (Group.BuildGroup.Step != 1)
                        continue;

                    Group.BuildGroup.Build(Thread);
                }

            }
        }

        return false;
    }

    public void UpdateBlock(ActionThread Thread, ITerrainBlock Block)
    {
        if (Block == null || Block.HasState(TerrainBlockStates.CLOSED) || !Block.HasState(TerrainBlockStates.LOADED))
            return;

        UpdatedNodes.Clear();
        (Block as TransvoxelBlock).UpdateOctree(Thread, true, UpdatedNodes);
        AddNodesToGenerate((Block as TransvoxelBlock));
        UpdatedNodes.Clear();
    }

    public bool ContainsGenerate(ushort BlockId, int ChunkId)
    {
        for (int i = Infos.Count - 1; i >= 0; --i)
        {
            if (Infos.array[i].ChunkId == ChunkId && Infos.array[i].BlockId == BlockId)
                return true;
        }

        return false;
    }

    public void AddNodesToGenerate(TransvoxelBlock Block)
    {
        if (UpdatedNodes.Count != 0)
        {
            ToGenerateNodeInfo Gen = new ToGenerateNodeInfo();
            Gen.BlockId = Block.BlockId;
            int NodeIndex, StartIndex;

            int j;
            for (int i = 0; i < UpdatedNodes.Count; ++i)
            {
                NodeIndex = UpdatedNodes[i].Key;

                if (UpdatedNodes[i].Value) // Node is opened
                {
                    StartIndex = Block.NodesArray.Array[NodeIndex].StartIndex;

                    // Add child chunks to generate
                    for (j = 0; j < 8; ++j)
                    {
                        if (Block.NodesArray.Array[StartIndex + j].HasChilds(Block.NodesArray, StartIndex + j))
                            continue;

                        Gen.ChunkId = Block.NodesArray.Array[StartIndex + j].Data;
                        Gen.Level = Block.NodesArray.Array[StartIndex + j].m_level;
                        //Gen.Priority = Block.HasNodeAroundDifferent(StartIndex + j, 2);

                        if (!ContainsGenerate(Gen.BlockId, Gen.ChunkId))
                            Infos.Add(Gen);
                    }
                }
                else // Node is closed
                {
                    Gen.ChunkId = Block.NodesArray.Array[NodeIndex].Data;
                    Gen.Level = Block.NodesArray.Array[NodeIndex].m_level;
                    //Gen.Priority = Block.HasNodeAroundDifferent(NodeIndex, 2);

                    if (!ContainsGenerate(Gen.BlockId, Gen.ChunkId))
                    {
                        Infos.Add(Gen);
                    }
                }
            }
        }
    }

    public void GenerateLODActions()
    {
        if (Infos.Count == 0)
            return;

        if (TerrainChunkGeneratePool.Index == 0)
            TerrainChunkGeneratePool = PoolManager.Pool.GetPool("TerrainChunkGenerateAction");

        GenerateInfos.Clear();

        TerrainChunkGenerateAndBuildGroup GenerateAction = null;
        ChunkGenerateInfo GenerateInfo = new ChunkGenerateInfo();

        for (int Level = 10; Level >= 0; --Level)
        {
            for (int i = Infos.Count - 1; i >= 0; --i)
            {
                if (Infos.array[i].Level == Level)
                {
                    GenerateInfo.ChunkRef.BlockId = Infos.array[i].BlockId;
                    GenerateInfo.ChunkRef.ChunkId = Infos.array[i].ChunkId;
                    GenerateInfo.Data = null;
                    GenerateInfos.Add(GenerateInfo);
                }
            }

            if (GenerateInfos.Count == 0)
                continue;

            GenerateAction = new TerrainChunkGenerateAndBuildGroup();
            GenerateAction.Init(GenerateInfos.ToArray());
            GenerateAction.CanBuild = Level >= TransvoxelInfo.MaxLodLevel-2;

            for (int i = 0; i < Processors.Count; ++i)
                Processors[i].OnTerrainLODUpdate(GenerateAction, Level);

            GenerateAction.Start();

            LODActions.Add(GenerateAction);
            ActionProcessor.AddToUnity(GenerateAction);
            GenerateInfos.Clear();
        }
    }


    #endregion
}
