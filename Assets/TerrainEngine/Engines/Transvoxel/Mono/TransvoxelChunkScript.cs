﻿
using UnityEngine;

public class TransvoxelChunkScript : TerrainChunkScript
{
    public override void Apply(IMeshData Data)
    {
        Mesh CMesh = GetMesh();
        CMesh.Clear(false);
        CMesh.vertices = Data.Vertices;
        CMesh.normals = Data.Normals;
        CMesh.SetIndices(Data.Indices[0], MeshTopology.Triangles, 0);
    }
}
