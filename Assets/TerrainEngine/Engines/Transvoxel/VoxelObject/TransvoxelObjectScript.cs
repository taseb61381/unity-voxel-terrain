﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TerrainEngine
{
    public class TransvoxelObjectScript
    {
        public TerrainInformation Information;
        public TransvoxelInformation TransvoxelInfo;
        public TerrainContainer Container;
        public TransvoxelBlock Block;

        public TypeVolumeTerrainData Data;
        public LibNoise.IModule Generator;

        public TerrainBuilder.TerrainBlockBuilder Builder;

        public void Start()
        {
            if (TransvoxelInfo == null)
                TransvoxelInfo = new TransvoxelInformation();

            Information.ForceHierarchyUpdate = true;
            Information.Init();
            TransvoxelInfo.Init(Information);
            Container = new TerrainContainer(TerrainManager.Instance, Information);

            Builder = new TerrainBuilder.TerrainBlockBuilder(Container,Information);
            Block = new TransvoxelBlock(Information, TransvoxelInfo);
            Data = new TypeVolumeTerrainData();


            Data.Init(Information,VectorI3.zero);
            Block.Init(Information, VectorI3.zero, 0);

            Block[1, 1, 1] = Block;
            Block.RelocateChunks();
        }
    }
}
