using UnityEngine;

namespace TerrainEngine
{
    public class SurfaceNetsBlock : MarchingBlock
    {
        static int[] cube_edges = new int[24];
        static int[] edge_table = new int[256];
        static bool EdgeGenerated = false;

        public SurfaceNetsBlock(TerrainManager Manager)
            : base(Manager)
        {
            lock (cube_edges)
            {
                if (!EdgeGenerated)
                {
                    EdgeGenerated = true;
                    //Initialize the cube_edges table
                    // This is just the vertex number of each cube
                    int k = 0;
                    for (int i = 0; i < 8; ++i)
                    {
                        for (int j = 1; j <= 4; j <<= 1)
                        {
                            int p = i ^ j;
                            if (i <= p)
                            {
                                SurfaceNetsBlock.cube_edges[k++] = i;
                                SurfaceNetsBlock.cube_edges[k++] = p;
                            }
                        }
                    }

                    //Initialize the intersection table.
                    //  This is a 2^(cube configuration) ->  2^(edge configuration) map
                    //  There is one entry for each possible cube configuration, and the output is a 12-bit vector enumerating all edges crossing the 0-level.
                    for (int i = 0; i < 256; ++i)
                    {
                        int em = 0;
                        for (int j = 0; j < 24; j += 2)
                        {
                            bool a = (i & (1 << SurfaceNetsBlock.cube_edges[j])) == 0 ? false : true;
                            bool b = (i & (1 << SurfaceNetsBlock.cube_edges[j + 1])) == 0 ? false : true;
                            em |= a != b ? (1 << (j >> 1)) : 0;
                        }
                        SurfaceNetsBlock.edge_table[i] = em;
                    }
                }
            }
        }

        public override float InterpolateHeight(byte CurrentType, float TopVolume, float CurrentVolume)
        {
            return 0;
        }

        public override MarchingVoxelArray GetInterpolations(ActionThread Thread, int StartX, int StartY, int StartZ,
    int SizeX, int SizeY, int SizeZ, int PaletteCount, ref bool IsValidChunk)
        {
            bool sx, sy, vx = false, vy = false, IsInChunk = false;
            byte ValType, LastType = 0;
            int px, py, pz, Count = 0, Current = 0, i = 0;
            MarchingInformation LastInfo = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(0);
            MarchingInformation Default = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(1);

            MarchingVoxelArray Interpolations = Thread.Pool.GetData<MarchingVoxelArray>("Marching");
            Interpolations.Init(SizeX, SizeY, SizeZ, 2, PaletteCount);
            MarchingInterpolationInfo Info = null;

            for (int x = 0; x < SizeX; ++x)
            {
                px = StartX + x;
                sx = Voxels.IsValid(px);

                if (!IsValidChunk)
                    vx = x < SizeX - 1;

                for (int y = 0; y < SizeY; ++y)
                {
                    py = StartY + y;
                    sy = Voxels.IsValid(py);

                    if (!IsValidChunk)
                        vy = y < SizeY - 1;

                    for (int z = 0; z < SizeZ; ++z)
                    {
                        pz = StartZ + z;

                        Info = Interpolations.Temp[i];
                        ++i;
                        Info.ClearIndices();
                        Info.State = MarchingInterpolationInfo.InterpolationState.NONE;
                        Info.Info = Default;

                        if (!IsValidChunk)
                            IsInChunk = vx && vy && z < SizeZ - 1;

                        if ((ValType = GetMarchingInterpolation(px, py, pz, ref Info, (sx && sy && Voxels.IsValid(pz)))) != BlockManager.None)
                        {
                            if (ValType != LastType)
                            {
                                LastType = ValType;
                                LastInfo = TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>(ValType);
                            }

                            Info.Info = LastInfo;

                            if ((Info.VoxelType = LastInfo.VoxelType) == VoxelTypes.FLUID)
                            {
                                Info.Volume = 0;
                                Info.VType = 0;
                            }
                            else
                            {
                                Info.Full = Info.Volume >= TerrainInformation.Isolevel;

                                if (IsInChunk && !IsValidChunk)
                                {
                                    if (Info.Full)
                                    {
                                        if (Info.VoxelType == VoxelTypes.TRANSLUCENT)
                                            IsValidChunk = true;

                                        Count += 1;
                                    }
                                }

                                if (IsInChunk)
                                {
                                    Interpolations.ActiveMaterial(ValType);
                                }
                            }
                        }
                        else
                        {
                            Info.VoxelType = VoxelTypes.OPAQUE;
                            Info.VType = 0;
                            Info.Volume = 0;
                        }

                        if (IsInChunk && !IsValidChunk)
                        {
                            ++Current;

                            if (Count != 0 && Count != Current)
                                IsValidChunk = true;
                        }
                    }
                }
            }

            return Interpolations;
        }

        public override void Generate(ActionThread Thread,ref TempMeshData Info,ref IMeshData Data, int ChunkId)
        {
            TerrainChunk Chunk = Chunks[ChunkId];
            int SizeX = Information.VoxelPerChunk + 2;
            int SizeY = Information.VoxelPerChunk + 2;
            int SizeZ = Information.VoxelPerChunk + 2;

            int StartX = Chunk.StartX;
            int StartY = Chunk.StartY;
            int StartZ = Chunk.StartZ;

            bool IsValidChunk = false;
            MarchingVoxelArray Interpolations = GetInterpolations(Thread, StartX, StartY, StartZ, SizeX, SizeY, SizeZ, TerrainManager.Instance.Palette.DataCount, ref IsValidChunk);
            if (!IsValidChunk)
            {
                Interpolations.Close();
                return;
            }

            Info = Thread.Pool.UnityPool.TempChunkPool.GetData<TempMeshData>();
            Data = PoolManager.UPool.GetSimpleMeshData();

        Vector3 R = new Vector3(1, (SizeX + 1), (SizeX + 1) * (SizeY + 1));
        Vector3 v = new Vector3();
        float[] grid = new float[8];
        int buf_no = 1, mask, m;
        int k, i,edge_mask,e_count;
        float p, s, g0, g1;
        int e0, e1;
        int a;
        int b;
        int VerticeCount = 0;

        int x, y, z;

        int[] buffer = new int[(int)R.z * 2];

        MarchingInterpolationInfo val0;
        MarchingInterpolationInfo Right;
        MarchingInterpolationInfo TopRight;
        MarchingInterpolationInfo Top;
        MarchingInterpolationInfo Forward;
        MarchingInterpolationInfo ForwardRight;
        MarchingInterpolationInfo ForwardTopRight;
        MarchingInterpolationInfo ForwardTop;

        bool CheckBorders = TerrainManager.Instance.OptimizeBlockBorders;
        bool BorderChunk = HasChunkState(ChunkId, ChunkStates.Border);

        for (z = 0; z < SizeZ - 1; ++z, buf_no ^= 1, R.z = -R.z)
        {
            m = 1 + (SizeX + 1) * (1 + buf_no * (SizeY + 1));

            for (y = 0; y < SizeY - 1; ++y, m += 2)
            {
                for (x = 0; x < SizeX - 1; ++x, ++m)
                {
                    mask = 0;
                    val0 = Interpolations[x, y, z];

                    Right = val0.Right;
                    TopRight = val0.TopRight;
                    Top = val0.Top;
                    Forward = val0.Forward;
                    ForwardRight = val0.ForwardRight;
                    ForwardTopRight = val0.ForwardTopRight;
                    ForwardTop = val0.ForwardTop;

                    if (CheckBorders && BorderChunk)
                    {
                        if (Right.Border
                            || TopRight.Border
                            || Top.Border
                            || Forward.Border
                            || ForwardRight.Border
                            || ForwardTopRight.Border
                            || ForwardTop.Border)
                            continue;
                    }

                    if (!val0.Full)
                        mask |= 1;

                    if (!Right.Full)
                        mask |= 2;

                    if (!Top.Full)
                        mask |= 4;

                    if (!TopRight.Full)
                        mask |= 8;

                    if (!Forward.Full)
                        mask |= 16;

                    if (!ForwardRight.Full)
                        mask |= 32;

                    if (!ForwardTop.Full)
                        mask |= 64;

                    if (!ForwardTopRight.Full)
                        mask |= 128;

                    if (mask == 0 || mask == 0xFF)
                        continue;

                    edge_mask = edge_table[mask];
                    v.y = 0;
                    v.x = v.z = 0f;

                    e_count = 0;
                    p = 0;

                    for (i = 0; i < 12; ++i)
                    {
                        //Use edge mask to check if it is crossed
                        if ((edge_mask & (1 << i)) == 0)
                            continue;

                        ++e_count;

                        e0 = cube_edges[i << 1];
                        e1 = cube_edges[(i << 1) + 1];
                        g0 = grid[e0];
                        g1 = grid[e1];
                        p = g0 - g1;

                        k = 1;
                        a = e0 & k;
                        b = e1 & k;
                        if (a != b) v.x += a != 0 ? 1.0f - p : p;
                        else v.x += a != 0 ? 1.0f : 0;
                        k <<= 1;

                        a = e0 & k;
                        b = e1 & k;
                        if (a != b) v.y += a != 0 ? 1.0f - p : p;
                        else v.y += a != 0 ? 1.0f : 0;
                        k <<= 1;

                        a = e0 & k;
                        b = e1 & k;
                        if (a != b) v.z += a != 0 ? 1.0f - p : p;
                        else v.z += a != 0 ? 1.0f : 0;
                    }

                    s = 1.0f / e_count;

                    v.x = (x + s * v.x);
                    v.y = (y + s * v.y);
                    v.z = (z + s * v.z);

                    buffer[m] = VerticeCount;
                    Info.Vertices.Add(v);
                    ++VerticeCount;

                    // Down Right or Left
                    if ((edge_mask & 1) != 0)
                    {
                        if (y != 0 && z != 0)
                        {
                            if ((mask & 1) == 1)
                            {
                                if (x != SizeX - 2)
                                {
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.y - (int)R.z]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.y]);

                                    Info.Triangles.Indices.Add(buffer[m - (int)R.y]);
                                    Info.Triangles.Indices.Add(buffer[m]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z]);
                                }

                            }
                            else
                            {
                                if (x != SizeX - 2)
                                {
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.y]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.y - (int)R.z]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z]);

                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z]);
                                    Info.Triangles.Indices.Add(buffer[m]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.y]);
                                }
                            }
                        }
                    }

                    if ((edge_mask & 2) != 0)
                    {
                        if (z != 0 && x != 0)
                        {
                            if ((mask & 1) == 1)
                            {
                                if (y != SizeY - 2)
                                {
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.x]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z - (int)R.x]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z]);

                                    Info.Triangles.Indices.Add(buffer[m - (int)R.x]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z]);
                                    Info.Triangles.Indices.Add(buffer[m]);
                                }
                            }
                            else
                            {
                                if (y != SizeY - 2)
                                {
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z - (int)R.x]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.x]);

                                    Info.Triangles.Indices.Add(buffer[m - (int)R.z]);
                                    Info.Triangles.Indices.Add(buffer[m - (int)R.x]);
                                    Info.Triangles.Indices.Add(buffer[m]);
                                }
                            }
                        }
                    }

                    // Down Forward or Backward
                    if ((edge_mask & 4) != 0)
                    {
                        if (x != 0 && y != 0)
                        {
                            if ((mask & 1) == 1)
                            {
                                if (z != SizeZ - 2)
                                {
                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.y)]);
                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.x - R.y)]);
                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.x)]);

                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.x)]);
                                    Info.Triangles.Indices.Add(buffer[m]);
                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.y)]);
                                }

                            }
                            else
                            {
                                if (z != SizeZ - 2)
                                {
                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.x)]);
                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.x - R.y)]);
                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.y)]);

                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.y)]);
                                    Info.Triangles.Indices.Add(buffer[m]);
                                    Info.Triangles.Indices.Add(buffer[(int)(m - R.x)]);
                                }
                            }
                        }
                    }
                }
            }
        }
            Info.TempMaterials.Add(TerrainManager.Instance.Palette.GetVoxelData<MarchingInformation>((byte)1).MaterialInfo);
            Interpolations.MaterialCount = 0;
            Data.Materials = PoolManager.UPool.GetMaterials(Info.TempMaterials);

            Data.Indices = new int[1][];
            Data.Vertices = Info.Vertices.ToArray();
            Data.Indices[0] = Info.Triangles.Indices.ToArray();
            Info.Triangles.Clear();
            Interpolations.Close();
        }
    }
}