
using TerrainEngine;

public class SurfaceNetsManager : MarchingManager 
{
    public override TerrainBlock CreateTerrain()
    {
        return new SurfaceNetsBlock(this);
    }
}
