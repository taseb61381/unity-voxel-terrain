using UnityEngine;

public class Wireframe : MonoBehaviour
{
    public KeyCode ActiveKey;
    public bool IsActive;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(ActiveKey))
            IsActive = !IsActive;

        if (IsActive)
            GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
        else
            GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
    }

    void OnPreRender()
    {
        GL.wireframe = IsActive;
    }

    void OnPostRender()
    {
        GL.wireframe = false;
    }
}
