﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public class ActiveDisableInfo
    {
        public bool Active;
        public bool Recursirve = true;
        public GameObject Obj;
        public MonoBehaviour Mono;
        public Camera Camera;
        public Rigidbody Gravity;

        public MonoBehaviour DestroyMono;
        public GameObject DestroyObject;
        public Rigidbody FreezeBody;
        public CreateGameObjectAction InstanciateObject;

        public MonoBehaviour[] RandomMono;

        public float DelayedTime = 0f;
        private float NextCall = 0f;

        public void Call()
        {
            if (DelayedTime != 0)
                NextCall = Time.realtimeSinceStartup + DelayedTime;
            else
                Execute();
        }

        public void Update()
        {
            if (NextCall != 0f && NextCall < Time.realtimeSinceStartup)
                Execute();
        }

        public void Execute()
        {
            NextCall = 0f;

            if (Obj != null)
            {
                if (Recursirve)
                    Obj.SetActiveRecursively(Active);
                else
                    Obj.SetActive(Active);
            }

            if (Mono != null)
                Mono.enabled = Active;

            if (Camera != null)
                Camera.enabled = Active;

            if (Gravity != null)
                Gravity.useGravity = Active;

            if (!Active && DestroyMono != null)
                GameObject.Destroy(DestroyMono);

            if (FreezeBody != null)
            {
                FreezeBody.constraints = RigidbodyConstraints.FreezeRotation;
            }

            if (DestroyObject != null)
                GameObject.Destroy(DestroyObject);

            if (InstanciateObject != null && InstanciateObject.ToInstanciate != null)
                ActionProcessor.AddToUnity(InstanciateObject);

            if (RandomMono != null && RandomMono.Length > 0)
            {
                MonoBehaviour M = RandomMono[UnityEngine.Random.Range(0, RandomMono.Length)];
                M.enabled = Active;
            }
        }
    }

    [Serializable]
    public class ActiveDisableContainer
    {
        public List<ActiveDisableInfo> Objects;
        public List<GameObject> GoToInvertState;
        public List<MonoBehaviour> MonoToInvertState;
        public List<Camera> CamToInvertState;
        public List<Light> LightToInvertState;
        public List<Rigidbody> GravityToIntertState;
        public List<Collider> ColliderToInvertState;

        public int Count
        {
            get
            {
                int c = 0;
                if (Objects != null)
                    c += Objects.Count;
                if (GoToInvertState != null)
                    c += GoToInvertState.Count;
                if (MonoToInvertState != null)
                    c += MonoToInvertState.Count;
                if (CamToInvertState != null)
                    c += CamToInvertState.Count;
                if (LightToInvertState != null)
                    c += LightToInvertState.Count;
                if (GravityToIntertState != null)
                    c += GravityToIntertState.Count;
                if (ColliderToInvertState != null)
                    c += ColliderToInvertState.Count;
                return c;
            }
        }

        public void AddMonoToCall(MonoBehaviour Mono, bool ToActive)
        {
            if (Objects == null)
                Objects = new List<ActiveDisableInfo>();

            ActiveDisableInfo Info = new ActiveDisableInfo();
            Info.Mono = Mono;
            Info.Active = ToActive;
            Objects.Add(Info);
        }

        public bool Contains(MonoBehaviour Mono)
        {
            if (Objects == null)
                return false;

            foreach (ActiveDisableInfo Info in Objects)
                if (Info.Mono == Mono)
                    return true;

            return false;
        }

        public void Update()
        {
            if(Objects != null)
                for (int i = 0; i < Objects.Count; ++i)
                    Objects[i].Update();
        }

        public void Call()
        {
            if (Objects != null)
            {
                for (int i = 0; i < Objects.Count; ++i)
                    Objects[i].Call();
            }

            if (GoToInvertState != null)
            {
                foreach (GameObject Go in GoToInvertState)
                    if (Go != null)
                        Go.SetActive(!Go.activeInHierarchy);
            }

            if (MonoToInvertState != null)
            {
                foreach (MonoBehaviour Mono in MonoToInvertState)
                {
                    if (Mono != null)
                        Mono.enabled = !Mono.enabled;
                }
            }

            if (CamToInvertState != null)
            {
                foreach (Camera Cam in CamToInvertState)
                    if (Cam != null)
                        Cam.enabled = !Cam.enabled;
            }

            if (LightToInvertState != null)
            {
                foreach (Light L in LightToInvertState)
                    if (L != null)
                        L.enabled = !L.enabled;
            }
            if (GravityToIntertState != null)
            {
                foreach (Rigidbody Rigid in GravityToIntertState)
                    if (Rigid != null)
                        Rigid.useGravity = !Rigid.useGravity;
            }

            if(ColliderToInvertState != null)
            {
                foreach (Collider Col in ColliderToInvertState)
                    if (Col != null)
                        Col.enabled = !Col.enabled;
            }
        }

        public void InverseStates()
        {
            for (int i = 0; i < Objects.Count; ++i)
                Objects[i].Active = !Objects[i].Active;
        }
    }
}
