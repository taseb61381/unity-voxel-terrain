using UnityEngine;

public class ScaleEffect : TimedEffect 
{
    public Vector3 MaxScale;
    public Vector3 MinScale;
    private Transform CTransform;
    private Vector3 OriginalScale;

    void Start()
    {
        CTransform = transform;
    }

    public override void OnUpdate()
    {
        if(IsEnable)
            CTransform.localScale = Vector3.Lerp(MinScale, MaxScale, Progress); 
        else
            CTransform.localScale = Vector3.Lerp(MaxScale, MinScale, Progress); 
    }
}
