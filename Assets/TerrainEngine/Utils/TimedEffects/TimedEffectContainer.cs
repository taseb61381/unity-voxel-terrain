﻿using System.Collections.Generic;

using UnityEngine;

public class TimedEffectContainer : MonoBehaviour
{
    public List<TimedEffect> Effects;
}