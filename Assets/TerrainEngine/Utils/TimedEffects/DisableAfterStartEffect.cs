
public class DisableAfterStartEffect : TimedEffect
{
    public override void OnDone()
    {
        TimedEffect[] Effects = gameObject.GetComponents<TimedEffect>();
        foreach (TimedEffect Effect in Effects)
            if (Effect != this)
                Effect.Disable();
    }
}
