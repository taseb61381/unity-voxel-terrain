using UnityEngine;

public class FadeEffect : TimedEffect 
{
    public Material Material;
    public string PropertyName = "_Color";

    private float EnableAlpha = 1f;
    private float DisableAlpha = 0;

    public override void OnUpdate()
    {
        if (Material == null)
            return;

        Color Col = Material.GetColor(PropertyName);

        if(IsEnable)
            Col.a = Mathf.Lerp(DisableAlpha, EnableAlpha, Progress);
        else
            Col.a = Mathf.Lerp(EnableAlpha, DisableAlpha, Progress);
    }
}
