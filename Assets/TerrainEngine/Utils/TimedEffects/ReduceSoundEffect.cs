using UnityEngine;

public class ReduceSoundEffect : TimedEffect 
{
    public AudioSource Source;
    public float MaxVolume = 1f;
    public float DisableVolume = 0;

    private float StartVolume = 0;

    public override void OnStartDisable()
    {
        if (Source == null)
            Source = GetComponent<AudioSource>();

        if (Source != null)
            StartVolume = Source.volume;
    }

    public override void OnStartEnable()
    {
        if (Source == null)
            Source = GetComponent<AudioSource>();

        if (Source != null)
            StartVolume = Source.volume;
    }

    public override void OnUpdate()
    {
        if (Source == null)
            Source = GetComponent<AudioSource>();

        if (Source == null)
            return;

        Source.volume = Mathf.Lerp(StartVolume, IsEnable ? MaxVolume : DisableVolume, Progress);
    }

    public override void OnDone()
    {
        if (Source == null)
            return;

        Source.volume = IsEnable ? MaxVolume : DisableVolume;
    }
}
