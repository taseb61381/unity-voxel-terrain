
public class ActiveDisableTimeEffect : TimedEffect
{
    public override void OnDone()
    {
        gameObject.SetActiveRecursively(IsEnable);
    }
}
