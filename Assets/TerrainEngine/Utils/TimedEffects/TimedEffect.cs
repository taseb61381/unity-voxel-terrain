using UnityEngine;

public abstract class TimedEffect : MonoBehaviour 
{
    public float TimeEffect;

    public float StartTime;
    public bool IsEnable;
    public bool Done;
    public float Progress;

    void Awake()
    {
        Done = true;
    }

    public void Disable()
    {
        StartTime = Time.realtimeSinceStartup;
        IsEnable = false;
        Done = false;
        OnStartDisable();
    }

    public void Enable()
    {
        StartTime = Time.realtimeSinceStartup;
        IsEnable = true;
        Done = false;
        OnStartEnable();
    }

    private float GetElapsedTime()
    {
        return Mathf.Clamp((Time.realtimeSinceStartup - StartTime) / TimeEffect, 0f, 1f);
    }

    void Update()
    {
        if (Done)
            return;

        Progress = GetElapsedTime();

        OnUpdate();

        if (StartTime + TimeEffect <= Time.realtimeSinceStartup)
        {
            OnDone();
            Done = true;
        }
    }

    public virtual void OnStartEnable()
    {

    }

    public virtual void OnStartDisable()
    {

    }

    public virtual void OnUpdate()
    {

    }

    public virtual void OnDone()
    {

    }
}
