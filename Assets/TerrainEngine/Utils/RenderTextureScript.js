private var mat : Material;

 

function OnPostRender() {

    if (!mat) {

        mat = new Material ("Shader \"Hidden/Alpha\" {" +

            "SubShader {" +

            "    Pass {" +

            "        ZTest Always Cull Off ZWrite Off" +

            "        ColorMask A" +

            "        Color (0,1,1,1)" +

            "    }" +

            "}" +

            "}"

        );

        mat.shader.hideFlags = HideFlags.HideAndDontSave;

        mat.hideFlags = HideFlags.HideAndDontSave;

    }

    GL.PushMatrix ();

    GL.LoadOrtho ();

    for (var i = 0; i < mat.passCount; ++i) {

        mat.SetPass (i);

        GL.Begin (GL.QUADS);

        GL.Vertex3 (0, 0, 0.1);

        GL.Vertex3 (1, 0, 0.1);

        GL.Vertex3 (1, 1, 0.1);

        GL.Vertex3 (0, 1, 0.1);

        GL.End ();

    }

    GL.PopMatrix ();

}