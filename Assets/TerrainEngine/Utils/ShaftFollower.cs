using UnityEngine;

public class ShaftFollower : MonoBehaviour
{
    public Transform Sun;
    public GameObject Target;
    public float Distance = 1000f;


    void Start()
    {
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (Target == null)
            return;

        if (Sun == null)
            Sun = gameObject.transform;

        Sun.position = Target.transform.position - Distance * Sun.forward;
	}
}
