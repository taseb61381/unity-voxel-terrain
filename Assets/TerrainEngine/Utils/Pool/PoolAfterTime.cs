using TerrainEngine;
using UnityEngine;

/// <summary>
/// Close current gameobject after x seconds. Add this gameobject to pooling system using gameobject.name. It can be reused by using GameObjectPool.GetGameObject();
/// </summary>
public class PoolAfterTime : MonoBehaviour 
{
    public float PoolTime;
    public float StartTime;

    void Awake()
    {
    }

    void OnEnable()
    {
        StartTime = Time.realtimeSinceStartup;
    }

    void OnDisable()
    {
        if(ActionProcessor.Instance != null)
            GameObjectPool.CloseGameObject(gameObject);

        StartTime = 0;
    }

    void Update()
    {
        if (StartTime + PoolTime <= Time.realtimeSinceStartup)
        {
            enabled = false;
        }
    }
}
