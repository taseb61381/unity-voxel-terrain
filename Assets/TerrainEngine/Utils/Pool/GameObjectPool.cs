﻿using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    /// <summary>
    /// Classes managing GameObject Pooling system.
    /// </summary>
    static public class GameObjectPool
    {
        #region DefaultObjects

        public class DefaultObjectInfo
        {
            public int PoolId;
            public GameObject DefaultObject;
        }

        static public SList<DefaultObjectInfo> DefaultObjects = new SList<DefaultObjectInfo>(10);

        static public bool GetDefaultObject(int PoolId, ref GameObject DefaultObject)
        {
            for (int i = DefaultObjects.Count - 1; i >= 0; --i)
            {
                if (DefaultObjects.array[i].PoolId == PoolId)
                {
                    DefaultObject = DefaultObjects.array[i].DefaultObject;
                    return true;
                }
            }

            return false;
        }

        static public void SetDefaultObject(int PoolId, GameObject DefaultObject)
        {
            for (int i = DefaultObjects.Count - 1; i >= 0; --i)
            {
                if (DefaultObjects.array[i].PoolId == PoolId)
                {
                    DefaultObjects.array[i].DefaultObject = DefaultObject;
                    return;
                }
            }

            DefaultObjectInfo Info = new DefaultObjectInfo();
            Info.PoolId = PoolId;
            Info.DefaultObject = DefaultObject;
            DefaultObjects.Add(Info);
        }

        static public void SetDefaultObject(string Name, GameObject DefaultObject)
        {
            int PoolId = GetPoolId(Name, true);

            for (int i = DefaultObjects.Count - 1; i >= 0; --i)
            {
                if (DefaultObjects.array[i].PoolId == PoolId)
                {
                    DefaultObjects.array[i].DefaultObject = DefaultObject;
                    return;
                }
            }

            DefaultObjectInfo Info = new DefaultObjectInfo();
            Info.PoolId = PoolId;
            Info.DefaultObject = DefaultObject;
            DefaultObjects.Add(Info);
        }

        #endregion

        #region Pools Info

        static public SList<GameObjectPoolInfo> Pools = new SList<GameObjectPoolInfo>(50);

        /// <summary>
        /// Return Pool Index from Pools. Access to pool : GameObjectPool.Pools.Array[Index];
        /// </summary>
        static public int GetPoolId(string Name, bool Create=true)
        {
            for (int i = Pools.Count-1; i >= 0; --i)
            {
                if (Pools.array[i].PoolName == Name)
                    return i;
            }

            if (Create)
            {
                Pools.Add(new GameObjectPoolInfo(Pools.Count, Name));
                return Pools.Count - 1;
            }

            return -1;
        }

        #endregion

        #region Objects Stacks

        static public FList<Stack<GameObject>> Stacks = new FList<Stack<GameObject>>(50);
        static public Queue<Stack<GameObject>> StacksPools = new Queue<Stack<GameObject>>(50);

        static public int GetStackId()
        {
            if (StacksPools.Count != 0)
            {
                Stacks.Add(StacksPools.Dequeue());
            }
            else
                Stacks.Add(new Stack<GameObject>(5));

            return Stacks.Count - 1;
        }

        static public void CloseStack(int StackId)
        {
            if (StackId == -1 || StackId >= Stacks.Count)
                return;

            Stack<GameObject> Stack = Stacks.array[StackId];
            Stacks.array[StackId] = null;
            StacksPools.Enqueue(Stack);
        }

        #endregion

        #region GameObjects

        static public Transform MainPoolObject;

        /// <summary>
        /// Return a GameObject by name. If there is no gameobjects already closed , a new one is instancied.
        /// </summary>
        static public GameObject GetGameObject(string Name)
        {
            for (int i = Pools.Count - 1; i >= 0; --i)
            {
                if (Pools.array[i].PoolName == Name)
                    return Pools.array[i].GetDefaultObject();
            }

            return null;
        }

        /// <summary>
        /// Close a gameobject. Disable it and add it to a Queue. Object can be reused by using : GetGameObject(name);
        /// </summary>
        static public void CloseGameObject(GameObject Obj)
        {
            string Name = Obj.name;
            for (int i = Pools.Count - 1; i >= 0; --i)
            {
                if (Pools.array[i].PoolName == Name)
                {
                    Pools.array[i].CloseObject(Obj);
                    return;
                }
            }
        }

        /// <summary>
        /// Return a GameObject by name. If no gameobjects already exist, a new GameObject will be instancied from Prefab object.
        /// </summary>
        static public GameObject GetGameObject(string Name, GameObject Prefab, bool Active = true)
        {
            for (int i = Pools.Count - 1; i >= 0; --i)
            {
                if (Pools.array[i].PoolName == Name)
                {
                    return Pools.array[i].GetObject(Prefab, false, Active);
                }
            }

            return null;
        }

        /// <summary>
        /// Close a gameobject. Disable it and add it to a Queue. Object can be reused by using : GetGameObject(name);
        /// </summary>
        static public void CloseGameObject(string Name, GameObject Obj)
        {
            for (int i = Pools.Count - 1; i >= 0; --i)
            {
                if (Pools.array[i].PoolName == Name)
                {
                    Pools.array[i].CloseObject(Obj);
                    return;
                }
            }
        }

        public delegate void PoolInfoCallDelegate(string Name, int Created, int Enqueue, int Dequeue, int Deleted);

        static public void GetStats(PoolInfoCallDelegate del)
        {
            for (int i = 0; i < Pools.Count; ++i)
            {
                del(Pools.array[i].PoolName, Pools.array[i].CreatedObject, Pools.array[i].EnqueueObject, Pools.array[i].DequeueObject, 0);
            }
        }

        #endregion
    }
}
