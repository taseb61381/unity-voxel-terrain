﻿using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    public struct GameObjectPoolInfo
    {
        public int PoolId;
        public int StackId;
        public string PoolName;

        public int CreatedObject;
        public int DequeueObject;
        public int EnqueueObject;
        public int QueueCount;
        public bool JustCreated;
        public bool EnablePooling;
        public float LastUpdate;

        public GameObjectPoolInfo(int PoolId, string Name)
        {
            this.PoolId = PoolId;
            this.StackId = -1;
            this.PoolName = Name;

            CreatedObject = 0;
            DequeueObject = 0;
            EnqueueObject = 0;
            QueueCount = 0;
            JustCreated = false;
            EnablePooling = true;
            LastUpdate = 0;
        }

        public void AddObjectToStack(GameObject Obj)
        {
            if (StackId == -1)
                StackId = GameObjectPool.GetStackId();

            GameObjectPool.Stacks.array[StackId].Push(Obj);
            ++EnqueueObject;
            ++QueueCount;
        }

        public bool GetObjectFromStack(ref GameObject Obj)
        {
            if (StackId == -1)
                return false;

            Stack<GameObject> Stack = GameObjectPool.Stacks.array[StackId];

            Obj = Stack.Pop();
            ++DequeueObject;
            --QueueCount;

            if (Stack.Count == 0)
            {
                GameObjectPool.CloseStack(StackId);
                StackId = -1;
            }
            return true;
        }

        public GameObject GetDefaultObject(bool ForceNew = false, bool Active = true)
        {
            GameObject DefaultObject = null;
            LastUpdate = Time.time;

            if(GameObjectPool.GetDefaultObject(PoolId, ref DefaultObject))
                return GetObject(DefaultObject, ForceNew, Active);

            return null;
        }

        public GameObject GetObject(GameObject Base, bool ForceNew = false, bool Active = true)
        {
            GameObject Obj = null;
            LastUpdate = Time.time;
            JustCreated = false;

            while (Obj == null)
            {
                if (ForceNew || !EnablePooling || !GetObjectFromStack(ref Obj))
                {
                    Obj = ActionProcessor.CreateGameObject(Base);
                    ++CreatedObject;
                    JustCreated = true;
                }
            }

            Obj.name = PoolName;

            if (Active)
                Obj.SetActive(true);

            return Obj;
        }

        public GameObject GetObject(GameObject Base, Vector3 Position, Quaternion Rotation, bool ForceNew = false, bool Active = true)
        {
            GameObject Obj = null;
            LastUpdate = Time.time;
            JustCreated = false;

            while (Obj == null)
            {
                if (ForceNew || !EnablePooling || !GetObjectFromStack(ref Obj))
                {
                    Obj = ActionProcessor.CreateGameObject(Base, Position, Rotation);
                    ++CreatedObject;
                    JustCreated = true;
                }

                if (!JustCreated && Obj != null)
                {
                    Transform TR = Obj.transform;
                    TR.position = Position;
                    TR.rotation = Rotation;
                }
            }

            Obj.name = PoolName;

            if (Active)
                Obj.SetActive(true);

            return Obj;
        }

        public void CloseObject(GameObject Obj)
        {
            LastUpdate = Time.time;
            if (EnablePooling)
            {
                Obj.SetActive(false);

                if (ThreadManager.IsEditor)
                {
                    if(GameObjectPool.MainPoolObject == null)
                    {
                        GameObjectPool.MainPoolObject = new GameObject().transform;
                        GameObjectPool.MainPoolObject.name = "(Pool)";
                    }

                    Obj.transform.parent = GameObjectPool.MainPoolObject;
                }
                else
                    Obj.transform.parent = null;

                AddObjectToStack(Obj);
            }
            else
                GameObject.Destroy(Obj);
        }

        public string GetStats()
        {
            return PoolName + " :\n" + "-Created= " + CreatedObject + "\n-Dequeue=" + DequeueObject + "\n-Enqueue=" + EnqueueObject + "\nLastUpdate:" + (int)(Time.time - LastUpdate);
        }
    }
}
