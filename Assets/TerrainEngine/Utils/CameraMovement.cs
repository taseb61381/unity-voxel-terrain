using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    public KeyCode ForwardKey = KeyCode.Z;
    public KeyCode LeftKey = KeyCode.Q;
    public KeyCode RightKey = KeyCode.D;
    public KeyCode BackwardKey = KeyCode.S;

    public float Speed = 10;
    public float SpeedPower = 2;
    private Transform CTransform;

	void Start () 
    {
        CTransform = transform;
	}
	
	void Update () 
    {
		
        if (!Input.anyKey)
            return;

        float Power = 1;
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            Power = SpeedPower;

        if (Input.GetKey(KeyCode.Q))
            CTransform.position = CTransform.position + Vector3.up * Speed * Time.deltaTime * Power;

        if (Input.GetKey(KeyCode.Z))
            CTransform.position = CTransform.position - Vector3.up * Speed * Time.deltaTime * Power;

        if (Input.GetKey(ForwardKey) || Input.GetAxis("Vertical") > 0)
            CTransform.position = CTransform.position + CTransform.forward * Speed * Time.deltaTime * Power;

        if (Input.GetKey(BackwardKey) || Input.GetAxis("Vertical") < 0)
            CTransform.position = CTransform.position - CTransform.forward * Speed * Time.deltaTime * Power;

        if (Input.GetKey(LeftKey) || Input.GetAxis("Horizontal") < 0)
            CTransform.position = CTransform.position - CTransform.right * Speed * Time.deltaTime * Power;

        if (Input.GetKey(RightKey) || Input.GetAxis("Horizontal") > 0)
            CTransform.position = CTransform.position + CTransform.right * Speed * Time.deltaTime * Power;
	}
}
