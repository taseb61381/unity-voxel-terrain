﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TerrainEngine
{
    public class MaterialIdInformation
    {
        public int InstanceId;
        public Material Mat;
    }

    public class MaterialIdInformationContainer
    {
        public int Count;
        public MaterialIdInformation[] Mats;
        public Material[] MatsArray;

        public MaterialIdInformationContainer(FList<MaterialIdInformation> L)
        {
            Mats = new MaterialIdInformation[L.Count];
            MatsArray = new Material[L.Count];
            Count = L.Count;

            for (int i = 0; i < L.Count; ++i)
            {
                Mats[i] = L.array[i];
                MatsArray[i] = L.array[i].Mat;
            }
        }

        public bool IsEqual(FList<MaterialIdInformation> L)
        {
            if (L.Count != Count)
                return false;

            for (int i = L.Count - 1; i >= 0; --i)
            {
                if (L.array[i].InstanceId != Mats[i].InstanceId)
                    return false;
            }

            return true;
        }
    }
}
