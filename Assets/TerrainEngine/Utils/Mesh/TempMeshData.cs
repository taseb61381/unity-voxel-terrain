﻿using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    public class TempMeshData : IPoolable
    {
        public int TriangleIndex;
        public TriangleData Triangles;
        public FList<Vector2> Uvs;
        public FList<Vector3> Vertices;
        public FList<Color> Colors;
        public FList<Vector3> Normals;
        public FList<Vector4> Tangents;
        public FList<MaterialIdInformation> TempMaterials;
        public int[][] CurrentIndices;
        public List<List<Vector3>> normal_buffer;
        public FList<int[]> TempIndices;

        public TempMeshData()
        {
            TriangleIndex = 0;
            Triangles = new TriangleData();
            Uvs = new FList<Vector2>(500);
            Vertices = new FList<Vector3>(500);
            Colors = new FList<Color>(10);
            Normals = new FList<Vector3>(500);
            Tangents = new FList<Vector4>(1);
            normal_buffer = new List<List<Vector3>>();
            TempMaterials = new FList<MaterialIdInformation>(5);
            TempIndices = new FList<int[]>(5);
            CurrentIndices = null;
        }

        public override void Clear()
        {
            TriangleIndex = 0;
            Triangles.Clear();
            Uvs.ClearFast();
            Vertices.ClearFast();
            Colors.ClearFast();
            Normals.ClearFast();
            Tangents.ClearFast();
            TempMaterials.Clear();
            TempIndices.Clear();
            CurrentIndices = null;
        }

        private int i,a,j;
        public void GenerateNormals()
        {
            if (Vertices.Count == 0)
                return;

            normal_buffer.Clear();

            for (i = 0; i < Vertices.Count; ++i)
            {
                if (normal_buffer.Count <= i)
                    normal_buffer.Add(new List<Vector3>(3));
                else
                    normal_buffer[i].Clear();
            }

            Normals.CheckArray(Vertices.Count);
            Vector3 vc0;
            Vector3 vc2;
            Vector3 vc1;

            Vector3 vt1;
            Vector3 vt2;
            Vector3 normal;
            for(a=0;a<CurrentIndices.Length;++a)
            {
                if (CurrentIndices[a] == null)
                    continue;

                for (i = 0; i < CurrentIndices[a].Length; i += 3)
                {
                    vc0 = Vertices.array[CurrentIndices[a][i]];
                    vc2 = Vertices.array[CurrentIndices[a][i + 2]];
                    vc1 = Vertices.array[CurrentIndices[a][i + 1]];

                    vt1 = vc1 - vc0;
                    vt2 = vc2 - vc0;
                    normal = Vector3.Cross(vt1, vt2);
                    normal.Normalize();

                    normal_buffer[CurrentIndices[a][i + 0]].Add(normal);
                    normal_buffer[CurrentIndices[a][i + 1]].Add(normal);
                    normal_buffer[CurrentIndices[a][i + 2]].Add(normal);
                }
            }

            Vector3 nm = new Vector3();
            for (i = 0; i < Vertices.Count; ++i)
            {
                nm.x = nm.y = nm.z = 0;
                for (j = 0; j < normal_buffer[i].Count; ++j)
                    nm += normal_buffer[i][j];

                Normals.array[i] = nm / normal_buffer[i].Count;
            }

            Normals.Count = Vertices.Count;
        }
    }
}
