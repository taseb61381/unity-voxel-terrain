using System.Collections.Generic;
using UnityEngine;


public class MeshUtils : MonoBehaviour
{
    public static List<Vector3> findAdjacentNeighbors(Vector3[] v, int[] t, Vector3 vertex)
    {
        List<Vector3> adjacentV = new List<Vector3>();
        List<int> facemarker = new List<int>();
        int facecount = 0;

        // Find matching vertices
        for (int i = 0; i < v.Length; i++)
            if (Mathf.Approximately(vertex.x, v[i].x) &&
                Mathf.Approximately(vertex.y, v[i].y) &&
                Mathf.Approximately(vertex.z, v[i].z))
            {
                int v1 = 0;
                int v2 = 0;
                bool marker = false;

                // Find vertex indices from the triangle array
                for (int k = 0; k < t.Length; k = k + 3)
                    if (facemarker.Contains(k) == false)
                    {
                        v1 = 0;
                        v2 = 0;
                        marker = false;

                        if (i == t[k])
                        {
                            v1 = t[k + 1];
                            v2 = t[k + 2];
                            marker = true;
                        }

                        if (i == t[k + 1])
                        {
                            v1 = t[k];
                            v2 = t[k + 2];
                            marker = true;
                        }

                        if (i == t[k + 2])
                        {
                            v1 = t[k];
                            v2 = t[k + 1];
                            marker = true;
                        }

                        facecount++;
                        if (marker)
                        {
                            // Once face has been used mark it so it does not get used again
                            facemarker.Add(k);

                            // Add non duplicate vertices to the list
                            if (isVertexExist(adjacentV, v[v1]) == false)
                            {
                                adjacentV.Add(v[v1]);
                                //TerrainManager.Log("Adjacent vertex index = " + v1);
                            }

                            if (isVertexExist(adjacentV, v[v2]) == false)
                            {
                                adjacentV.Add(v[v2]);
                                //TerrainManager.Log("Adjacent vertex index = " + v2);
                            }
                            marker = false;
                        }
                    }
            }

        //TerrainManager.Log("Faces Found = " + facecount);

        return adjacentV;
    }

    public static List<int> findAdjacentNeighborIndexes(Vector3[] v, int[] t, Vector3 vertex)
    {
        List<int> adjacentIndexes = new List<int>();
        List<Vector3> adjacentV = new List<Vector3>();
        List<int> facemarker = new List<int>();
        int facecount = 0;

        // Find matching vertices
        for (int i = 0; i < v.Length; i++)
            if (Mathf.Approximately(vertex.x, v[i].x) &&
                Mathf.Approximately(vertex.y, v[i].y) &&
                Mathf.Approximately(vertex.z, v[i].z))
            {
                int v1 = 0;
                int v2 = 0;
                bool marker = false;

                // Find vertex indices from the triangle array
                for (int k = 0; k < t.Length; k = k + 3)
                    if (facemarker.Contains(k) == false)
                    {
                        v1 = 0;
                        v2 = 0;
                        marker = false;

                        if (i == t[k])
                        {
                            v1 = t[k + 1];
                            v2 = t[k + 2];
                            marker = true;
                        }

                        if (i == t[k + 1])
                        {
                            v1 = t[k];
                            v2 = t[k + 2];
                            marker = true;
                        }

                        if (i == t[k + 2])
                        {
                            v1 = t[k];
                            v2 = t[k + 1];
                            marker = true;
                        }

                        facecount++;
                        if (marker)
                        {
                            // Once face has been used mark it so it does not get used again
                            facemarker.Add(k);

                            // Add non duplicate vertices to the list
                            if (isVertexExist(adjacentV, v[v1]) == false)
                            {
                                adjacentV.Add(v[v1]);
                                adjacentIndexes.Add(v1);
                                //TerrainManager.Log("Adjacent vertex index = " + v1);
                            }

                            if (isVertexExist(adjacentV, v[v2]) == false)
                            {
                                adjacentV.Add(v[v2]);
                                adjacentIndexes.Add(v2);
                                //TerrainManager.Log("Adjacent vertex index = " + v2);
                            }
                            marker = false;
                        }
                    }
            }

        //TerrainManager.Log("Faces Found = " + facecount);

        return adjacentIndexes;
    }

    static bool isVertexExist(List<Vector3> adjacentV, Vector3 v)
    {
        bool marker = false;
        foreach (Vector3 vec in adjacentV)
            if (Mathf.Approximately(vec.x, v.x) && Mathf.Approximately(vec.y, v.y) && Mathf.Approximately(vec.z, v.z))
            {
                marker = true;
                break;
            }

        return marker;
    }

    static public SList<Vector3> tan1, tan2;
    static public SList<int> triangles;

    static public void calculateMeshTangents(Mesh mesh, IMeshData Data, Vector3[] vertices, Vector3[] normals, Vector2[] uv)
    {
        //speed up math by copying the mesh arrays
        if (triangles.array == null)
            triangles = new SList<int>(500);

        if (normals == null || normals.Length == 0)
            normals = mesh.normals;

        //variable definitions
        triangles.ClearFast();
        int triangleCount = Data.GetIndicesCount(ref triangles);
        int vertexCount = vertices.Length;

        if (tan1.array == null)
        {
            tan1 = new SList<Vector3>(vertexCount);
            tan2 = new SList<Vector3>(vertexCount);
        }

        tan1.ClearFast();
        tan2.ClearFast();
        tan1.CheckArray(vertexCount);
        tan2.CheckArray(vertexCount);

        Vector4[] tangents = new Vector4[vertexCount];
        float sdirx, sdiry, sdirz, tdirx, tdiry, tdirz;
        Vector3 v1, v2,v3;
        Vector2 w1, w2, w3;
        float x1,x2,y1,y2,z1,z2,s1,s2,t1,t2;
        float div,r;
        int i1, i2, i3;

        for (long a = 0; a < triangleCount; a += 3)
        {
            i1 = triangles.array[a + 0];
            i2 = triangles.array[a + 1];
            i3 = triangles.array[a + 2];

            v1 = vertices[i1];
            v2 = vertices[i2];
            v3 = vertices[i3];

            w1 = uv[i1];
            w2 = uv[i2];
            w3 = uv[i3];

            x1 = v2.x - v1.x;
            x2 = v3.x - v1.x;
            y1 = v2.y - v1.y;
            y2 = v3.y - v1.y;
            z1 = v2.z - v1.z;
            z2 = v3.z - v1.z;

            s1 = w2.x - w1.x;
            s2 = w3.x - w1.x;
            t1 = w2.y - w1.y;
            t2 = w3.y - w1.y;

            div = s1 * t2 - s2 * t1;
            r = div == 0.0f ? 0.0f : 1.0f / div;

            sdirx = (t2 * x1 - t1 * x2) * r;
            sdiry = (t2 * y1 - t1 * y2) * r;
            sdirz = (t2 * z1 - t1 * z2) * r;

            tdirx = (s1 * x2 - s2 * x1) * r;
            tdiry = (s1 * y2 - s2 * y1) * r;
            tdirz = (s1 * z2 - s2 * z1) * r;

            tan1.array[i1].x += sdirx;
            tan1.array[i1].y += sdiry;
            tan1.array[i1].z += sdirz;

            tan1.array[i2].x += sdirx;
            tan1.array[i2].y += sdiry;
            tan1.array[i2].z += sdirz;

            tan1.array[i3].x += sdirx;
            tan1.array[i3].y += sdiry;
            tan1.array[i3].z += sdirz;

            tan2.array[i1].x += tdirx;
            tan2.array[i1].y += tdiry;
            tan2.array[i1].z += tdirz;

            tan2.array[i2].x += tdirx;
            tan2.array[i2].y += tdiry;
            tan2.array[i2].z += tdirz;

            tan2.array[i3].x += tdirx;
            tan2.array[i3].y += tdiry;
            tan2.array[i3].z += tdirz;
        }

        Vector3 n;
        Vector3 t;
        for (long a = 0; a < vertexCount; ++a)
        {
            n = normals[a];
            t = tan1.array[a];

            //Vector3 tmp = (t - n * Vector3.Dot(n, t)).normalized;
            //tangents[a] = new Vector4(tmp.x, tmp.y, tmp.z);
            Vector3.OrthoNormalize(ref n, ref t);
            tangents[a].x = t.x;
            tangents[a].y = t.y;
            tangents[a].z = t.z;

            tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2.array[a]) < 0.0f) ? -1.0f : 1.0f;
        }

        mesh.tangents = tangents;
    }

    static public void calculateMeshTangentsWithoutUV(Mesh mesh,IMeshData Data, Vector3[] vertices, Vector3[] normals)
    {
        //speed up math by copying the mesh arrays
        if (triangles.array == null)
            triangles = new SList<int>(500);

        if (normals == null || normals.Length == 0)
            normals = mesh.normals;

        //variable definitions
        triangles.ClearFast();
        int triangleCount = Data.GetIndicesCount(ref triangles);
        int vertexCount = vertices.Length;


        if (tan1.array == null)
        {
            tan1 = new SList<Vector3>(vertexCount);
            tan2 = new SList<Vector3>(vertexCount);
        }

        tan1.ClearFast();
        tan2.ClearFast();
        tan1.CheckArray(vertexCount);
        tan2.CheckArray(vertexCount);

        Vector4[] tangents = new Vector4[vertexCount];
        float sdirx,sdiry,sdirz, tdirx,tdiry,tdirz;
        Vector3 v1, v2, v3;
        Vector2 w1, w2, w3;
        float x1, x2, y1, y2, z1, z2, s1, s2, t1, t2;
        float div, r;
        int i1, i2, i3;

        for (long a = 0; a < triangleCount; a += 3)
        {
            i1 = triangles.array[a + 0];
            i2 = triangles.array[a + 1];
            i3 = triangles.array[a + 2];

            v1 = vertices[i1];
            v2 = vertices[i2];
            v3 = vertices[i3];

            w1.x = v1.x + v1.y;
            w1.y = v1.y + v1.z;

            w2.x = v2.x + v2.y;
            w2.y = v2.y + v2.z;

            w3.x = v3.x + v3.y;
            w3.y = v3.y + v3.z;

            x1 = v2.x - v1.x;
            x2 = v3.x - v1.x;
            y1 = v2.y - v1.y;
            y2 = v3.y - v1.y;
            z1 = v2.z - v1.z;
            z2 = v3.z - v1.z;

            s1 = w2.x - w1.x;
            s2 = w3.x - w1.x;
            t1 = w2.y - w1.y;
            t2 = w3.y - w1.y;

            div = s1 * t2 - s2 * t1;
            r = div == 0.0f ? 0.0f : 1.0f / div;

            sdirx = (t2 * x1 - t1 * x2) * r;
            sdiry = (t2 * y1 - t1 * y2) * r;
            sdirz = (t2 * z1 - t1 * z2) * r;

            tdirx = (s1 * x2 - s2 * x1) * r;
            tdiry = (s1 * y2 - s2 * y1) * r;
            tdirz = (s1 * z2 - s2 * z1) * r;

            tan1.array[i1].x += sdirx;
            tan1.array[i1].y += sdiry;
            tan1.array[i1].z += sdirz;

            tan1.array[i2].x += sdirx;
            tan1.array[i2].y += sdiry;
            tan1.array[i2].z += sdirz;

            tan1.array[i3].x += sdirx;
            tan1.array[i3].y += sdiry;
            tan1.array[i3].z += sdirz;

            tan2.array[i1].x += tdirx;
            tan2.array[i1].y += tdiry;
            tan2.array[i1].z += tdirz;

            tan2.array[i2].x += tdirx;
            tan2.array[i2].y += tdiry;
            tan2.array[i2].z += tdirz;

            tan2.array[i3].x += tdirx;
            tan2.array[i3].y += tdiry;
            tan2.array[i3].z += tdirz;
        }

        Vector3 n;
        Vector3 t;
        for (long a = 0; a < vertexCount; ++a)
        {
            n = normals[a];
            t = tan1.array[a];

            //Vector3 tmp = (t - n * Vector3.Dot(n, t)).normalized;
            //tangents[a] = new Vector4(tmp.x, tmp.y, tmp.z);
            Vector3.OrthoNormalize(ref n, ref t);
            tangents[a].x = t.x;
            tangents[a].y = t.y;
            tangents[a].z = t.z;

            tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2.array[a]) < 0.0f) ? -1.0f : 1.0f;
        }

        mesh.tangents = tangents;
    }

    static public Vector3 RotateVertice(Vector3 Vertice, float YAngle)
    {
        Vector3 Temp = Vertice;
        float t = Mathf.Deg2Rad * YAngle;
        Vertice.x = Temp.x * Mathf.Cos(t) - Temp.z * Mathf.Sin(t);
        Vertice.z = Temp.x * Mathf.Sin(t) + Temp.z * Mathf.Cos(t);
        return Vertice;
    }

    static public Vector3 RotateAxis(Vector3 Vertice, Vector3 Axis, float Angle)
    {
        return Quaternion.AngleAxis(Angle, Axis) * Vertice;
    }

    static public Vector3 RotateVertice(Vector3 Center, Vector3 Vertice, float Angle)
    {
        Vector3 Temp = new Vector3();
        float t = Mathf.Deg2Rad * Angle;
        Temp.x = Center.x + (Vertice.x - Center.x) * Mathf.Cos(t) - (Vertice.z - Center.z) * Mathf.Sin(t);
        Temp.z = Center.z + (Vertice.x - Center.x) * Mathf.Sin(t) + (Vertice.z - Center.z) * Mathf.Cos(t);
        return Temp;
    }

    static public Vector3[] RotateVertices(Vector3[] Vertices, float Angle)
    {
        Vector3 Vertice = new Vector3();
        Vector3 Temp = new Vector3();

        float t = Mathf.Deg2Rad * Angle;
        for (int i = 0; i < Vertices.Length; ++Angle)
        {
            Temp = Vertices[i];

            Vertice.x = Temp.x * Mathf.Cos(t) - Temp.z * Mathf.Sin(t);
            Vertice.z = Temp.x * Mathf.Sin(t) + Temp.z * Mathf.Cos(t);
            Vertice.y = Temp.y;

            Vertices[i] = Vertice;
        }

        return Vertices;
    }

    static public int[] AddOffsetIndice(int[] Indices, int Offset)
    {
        if (Offset == 0)
            return Indices;

        int Lenght = Indices.Length;
        int[] NewIndices = new int[Lenght];

        for (int i = 0; i < Lenght; ++i)
            NewIndices[i] = Indices[i] + Offset;

        return NewIndices;
    }

    static public Vector3[] AddOffsetVertices(Vector3[] Vertices, Vector3 Offset)
    {
        int Lenght = Vertices.Length;
        Vector3[] NewVertices = new Vector3[Lenght];

        for (int i = 0; i < Lenght; ++i)
            NewVertices[i] = Offset + Vertices[i];

        return NewVertices;
    }

    public enum Orientation
    {
        Horizontal,
        Vertical
    }

    public enum AnchorPoint
    {
        TopLeft,
        TopHalf,
        TopRight,
        RightHalf,
        BottomRight,
        BottomHalf,
        BottomLeft,
        LeftHalf,
        Center
    }

    static public void CreatePlane(Mesh m, int widthSegments, int lengthSegments, Orientation orientation = Orientation.Horizontal, AnchorPoint anchor = AnchorPoint.Center)
    {
        float width = widthSegments;
        float length = lengthSegments;

        Vector2 anchorOffset;
        switch (anchor)
        {
            case AnchorPoint.TopLeft:
                anchorOffset = new Vector2(-width / 2.0f, length / 2.0f);
                break;
            case AnchorPoint.TopHalf:
                anchorOffset = new Vector2(0.0f, length / 2.0f);
                break;
            case AnchorPoint.TopRight:
                anchorOffset = new Vector2(width / 2.0f, length / 2.0f);
                break;
            case AnchorPoint.RightHalf:
                anchorOffset = new Vector2(width / 2.0f, 0.0f);
                break;
            case AnchorPoint.BottomRight:
                anchorOffset = new Vector2(width / 2.0f, -length / 2.0f);
                break;
            case AnchorPoint.BottomHalf:
                anchorOffset = new Vector2(0.0f, -length / 2.0f);
                break;
            case AnchorPoint.BottomLeft:
                anchorOffset = new Vector2(-width / 2.0f, -length / 2.0f);
                break;
            case AnchorPoint.LeftHalf:
                anchorOffset = new Vector2(-width / 2.0f, 0.0f);
                break;
            case AnchorPoint.Center:
            default:
                anchorOffset = Vector2.zero;
                break;
        }

        int hCount2 = widthSegments + 1;
        int vCount2 = lengthSegments + 1;
        int numTriangles = widthSegments * lengthSegments * 6;
        int numVertices = hCount2 * vCount2;

        Vector3[] vertices = new Vector3[numVertices];
        Vector2[] uvs = new Vector2[numVertices];
        int[] triangles = new int[numTriangles];
        int index = 0;

        float uvFactorX = 1.0f / widthSegments;
        float uvFactorY = 1.0f / lengthSegments;
        float scaleX = width / widthSegments;
        float scaleY = length / lengthSegments;
        for (float y = 0.0f; y < vCount2; y++)
        {
            for (float x = 0.0f; x < hCount2; x++)
            {
                if (orientation == Orientation.Horizontal)
                {
                    vertices[index] = new Vector3(x * scaleX - width / 2f - anchorOffset.x, 0.0f, y * scaleY - length / 2f - anchorOffset.y);
                }
                else
                {
                    vertices[index] = new Vector3(x * scaleX - width / 2f - anchorOffset.x, y * scaleY - length / 2f - anchorOffset.y, 0.0f);
                }
                uvs[index++] = new Vector2(x * uvFactorX, y * uvFactorY);
            }
        }

        index = 0;
        for (int y = 0; y < lengthSegments; y++)
        {
            for (int x = 0; x < widthSegments; x++)
            {
                triangles[index] = (y * hCount2) + x;
                triangles[index + 1] = ((y + 1) * hCount2) + x;
                triangles[index + 2] = (y * hCount2) + x + 1;

                triangles[index + 3] = ((y + 1) * hCount2) + x;
                triangles[index + 4] = ((y + 1) * hCount2) + x + 1;
                triangles[index + 5] = (y * hCount2) + x + 1;
                index += 6;
            }
        }

        m.vertices = vertices;
        m.uv = uvs;
        m.triangles = triangles;
        m.RecalculateNormals();
    }
}