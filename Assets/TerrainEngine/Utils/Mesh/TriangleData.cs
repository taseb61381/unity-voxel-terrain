﻿using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    public class TriangleData
    {
        public FList<Vector3> Vertices;
        public FList<int> Indices;
        public FList<Vector3> Normals;
        private FList<Vector3> OptimizedVertice;

        public TriangleData()
        {
            Vertices = new FList<Vector3>(500);
            Indices = new FList<int>(500);
            OptimizedVertice = new FList<Vector3>(500);
            Normals = new FList<Vector3>(500);
        }

        public void Clear()
        {
            Vertices.ClearFast();
            Indices.ClearFast();
            OptimizedVertice.ClearFast();
            Normals.ClearFast();
        }

        public void Optimize(int OffsetTriangle = 0)
        {
            if (Vertices.Count == 0)
                return;

            OptimizedVertice.CheckArray(Vertices.Count);
            OptimizedVertice.Clear();

            Vector3 vert = Vector3.zero;
            Vector3 newVert = Vector3.zero;

            for (int i = 0; i < Vertices.Count; ++i)
            {
                vert = Vertices.array[i];
                for (int j = 0; j < OptimizedVertice.Count; ++j)
                {
                    newVert = OptimizedVertice.array[j];

                    if (newVert == vert)
                    {
                        goto skipToNext;
                    }
                }

                OptimizedVertice.Add(vert);

                skipToNext:
                ;
            }

            for (int i = 0; i < Indices.Count; ++i)
            {
                for (int j = 0; j < OptimizedVertice.Count; ++j)
                {
                    newVert = OptimizedVertice.array[j];
                    vert = Vertices.array[Indices.array[i] - OffsetTriangle];

                    if (newVert == vert)
                    {
                        Indices.array[i] = (OffsetTriangle + j);
                        break;
                    }
                }
            }

            Vertices.Set(OptimizedVertice.ToArray());
        }

        public void Simplify()
        {
            List<TempTriangle> Temp = new List<TempTriangle>();

            for(int i=0;i<Vertices.Count;++i)
                Temp.Add(new TempTriangle(Vertices.array[i], Normals.array[i]));

            for (int i = 0; i < Indices.Count; i+=3)
            {
                TempTriangle vc0 = Temp[Indices.array[i]];
                TempTriangle vc2 = Temp[Indices.array[i + 2]];
                TempTriangle vc1 = Temp[Indices.array[i + 1]];

                vc0.Link.Add(vc1);
                vc0.Link.Add(vc2);

                vc1.Link.Add(vc0);
                vc1.Link.Add(vc2);

                vc2.Link.Add(vc0);
                vc2.Link.Add(vc1);
            }

            for (int i = 0; i < Vertices.Count; ++i)
            {
                TempTriangle vc = Temp[i];

                foreach (TempTriangle a in vc.Link.ToArray())
                {
                    foreach (TempTriangle b in a.Link.ToArray())
                    {
                        if (b.Normal == a.Normal)
                        {
                            vc.Link.Remove(a);
                            vc.Link.Add(b);
                            a.Link.Remove(b);
                        }
                    }
                }
            }

            Vertices.ClearFast();
            Normals.ClearFast();
            Indices.ClearFast();
            int IndiceCount = 0;

            foreach (TempTriangle vc in Temp)
            {
                if (vc.Link.Count == 2)
                {
                    if (vc.Indice == 0)
                    {
                        Vertices.Add(vc.Vertex);
                        Normals.Add(vc.Normal);
                        vc.Indice = IndiceCount;
                        Indices.Add(IndiceCount++);
                    }
                    else
                        Indices.Add(vc.Indice);

                    if (vc.Link[0].Indice == 0)
                    {
                        Vertices.Add(vc.Link[0].Vertex);
                        Normals.Add(vc.Link[0].Normal);
                        vc.Link[0].Indice = IndiceCount;
                        Indices.Add(IndiceCount++);
                    }
                    else
                        Indices.Add(vc.Link[0].Indice);

                    if (vc.Link[1].Indice == 0)
                    {
                        Vertices.Add(vc.Link[1].Vertex);
                        Normals.Add(vc.Link[1].Normal);
                        vc.Link[1].Indice = IndiceCount;
                        Indices.Add(IndiceCount++);
                    }
                    else
                        Indices.Add(vc.Link[1].Indice);

                }
            }
        }
    }

    public class TempTriangle
    {
        public TempTriangle(Vector3 Vertex, Vector3 Normal)
        {
            this.Vertex = Vertex;
            this.Normal = Normal;
            this.Indice = 0;
        }

        public Vector3 Vertex;
        public Vector3 Normal;
        public int Indice;
        public List<TempTriangle> Link = new List<TempTriangle>();
    }
}
