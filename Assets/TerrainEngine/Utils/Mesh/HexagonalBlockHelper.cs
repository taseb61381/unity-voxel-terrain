﻿
using UnityEngine;

namespace TerrainEngine
{
    static public class HexagonalBlockHelper
    {
        static public readonly int Facecount = 8;
        static public readonly int TopBottomVertex = 14;

        static public readonly float h = Mathf.Sin(DegreesToRadians(30));
        static public readonly float r = Mathf.Cos(DegreesToRadians(30));

        public static float DegreesToRadians(float degrees)
        {
            return degrees * Mathf.PI / 180f;
        }

        public enum HexagonalFaces
        {
            TOP = 0,
            BOTTOM = 1,
            LEFT = 2,
            TOP_LEFT = 3,
            UP = 4,
            TOP_RIGHT = 5,
            RIGHT = 6,
            BOTTOM_RIGHT = 7,
        };

        public static readonly BlockHelper.BlockFace[] facesEnum = new BlockHelper.BlockFace[8]
        {
            BlockHelper.BlockFace.Top,
            BlockHelper.BlockFace.Bottom,
            BlockHelper.BlockFace.Side,
            BlockHelper.BlockFace.Side,
            BlockHelper.BlockFace.Side,
            BlockHelper.BlockFace.Side,
            BlockHelper.BlockFace.Side,
            BlockHelper.BlockFace.Side
        };

        static public readonly Vector3[] Vertexs = new Vector3[] 
        { 
            new Vector3(0, 1, 0), // 0
            new Vector3(1, 1, 0), // 1
            new Vector3(1, 1, 0), // 2
            new Vector3(1, 1, 0), // 3
            new Vector3(0, 1, 0), // 4
            new Vector3(0, 1, 0), // 5

            new Vector3(0, 0, 0), // 6
            new Vector3(1, 0, 0), // 7
            new Vector3(1, 0, 0), // 8
            new Vector3(1, 0, 0), // 9
            new Vector3(0, 0, 0), // 10
            new Vector3(0, 0, 0), // 11

            new Vector3(0, 1, 0), // 12
            new Vector3(0, 0, 0), // 13
        };

        static public readonly Vector3[] HOffset = new Vector3[] 
        { 
            new Vector3(0, 0, 0), // 0
            new Vector3(0, 0, 0), // 1
            new Vector3(1, 0, 0), // 2
            new Vector3(0, 0, 0), // 3
            new Vector3(0, 0, 0), // 4
            new Vector3(-1, 0, 0), // 5

            new Vector3(0, 0, 0), // 6
            new Vector3(0, 0, 0), // 7
            new Vector3(1, 0, 0), // 8
            new Vector3(0, 0, 0), // 9
            new Vector3(0, 0, 0), // 10
            new Vector3(-1, 0, 0), // 11

            new Vector3(1, 0, 0), // 12
            new Vector3(1, 0, 0) // 13
        };

        static public readonly Vector3[] ROffset = new Vector3[] 
        { 
            new Vector3(0, 0, 0), // 0
            new Vector3(0, 0, 0), // 1
            new Vector3(0, 0, 1), // 2
            new Vector3(0, 0, 2), // 3
            new Vector3(0, 0, 2), // 4
            new Vector3(0, 0, 1), // 5

            new Vector3(0, 0, 0), // 6
            new Vector3(0, 0, 0), // 7
            new Vector3(0, 0, 1), // 8
            new Vector3(0, 0, 2), // 9
            new Vector3(0, 0, 2), // 10
            new Vector3(0, 0, 1),  // 11 5

            new Vector3(0, 0, 1), // 12
            new Vector3(0, 0, 1) // 13
        };

        static public readonly int[][] VertexSide = new int[][]
        {
            new int[] { 0, 1, 2, 3, 4, 5 , 12 }, // TOP
            new int[] { 6, 7, 8, 9, 10, 11, 13 }, // BOTTOM

            new int[] { 0, 1, 6, 7 }, // LEFT |
            new int[] { 1, 2, 7, 8 }, // TOP-LEFT /
            new int[] { 2, 3, 8, 9 }, // UP _

            new int[] { 3, 4, 9, 10 }, // TOP RIGHT \
            new int[] { 4, 5, 10, 11 }, // RIGHT |
            new int[] { 5, 0, 11, 6 } // BOTTOM RIGHT
        };


        static public int[][] VertexOrder = new int[][]
        {
            new int[] { 12, 1, 0,  12,2,1  ,12,3,2, 12,4,3 ,12,5,4 ,0,5,12 }, // TOP
            new int[] { 6, 7, 13,  7,8,13  ,8,9,13, 9,10,13 ,10,11,13 ,13,11,6 }, // BOTTOM

            new int[] { 0, 1, 6,  7,6,1  }, // LEFT
            new int[] { 1, 2, 7,  8,7,2  }, // TOP - LEFT
            new int[] { 2, 3, 8,  9,8,3  }, // UP
            new int[] { 3, 4, 9,  10,9,4  }, // TOP RIGHT
            new int[] { 4, 5, 10,  11,10,5  }, // RIGHT
            new int[] { 5, 0, 11,  6,11,0  } // BOTTOM - RIGHT
        };

        static public Vector3 GetVertex(int side, int x, int y, int z, float Scale)
        {
            float offx = (x * h);
            float offz = (x % 2 != 0 ? -r : 0) + (z * 2 * (r - h));

            return new Vector3(
                Scale * x + Scale * (Vertexs[side].x + (h * HOffset[side].x) + (r * ROffset[side].x) + offx),
                Scale * y + Scale * (Vertexs[side].y + (h * HOffset[side].y) + (r * ROffset[side].y)),
                Scale * z + Scale * (Vertexs[side].z + (h * HOffset[side].z) + (r * ROffset[side].z) + offz)
                );
        }

        static public readonly VectorI3[][] AroundOffsets = new VectorI3[][]
        {
            new VectorI3[] // impair
            {
                new VectorI3(0,1,0), // TOP 
                new VectorI3(0,-1,0), // BOTTOM

                new VectorI3(0, 0, -1), // UP
                new VectorI3(1, 0, 0), // UP RIGHT

                new VectorI3(1,0,1), // DOWN RIGHT
                new VectorI3(0,0,1), // DOWN

                new VectorI3(-1,0,1), // DOWN LEFT
                new VectorI3(-1,0,0), // UP LEFT
            },

            new VectorI3[] // Pair
            {
                new VectorI3(0,1,0),     // TOP 
                new VectorI3(0,-1,0),    // BOTTOM
                new VectorI3(0, 0, -1),  // UP
                new VectorI3(1, 0, -1),  // UP RIGHT
                new VectorI3(1,0,0),     // DOWN RIGHT
                new VectorI3(0,0,1),     // DOWN
                new VectorI3(-1,0,0),    // DOWN LEFT
                new VectorI3(-1,0,-1),   // UP LEFT
            }
        };
    }
}
