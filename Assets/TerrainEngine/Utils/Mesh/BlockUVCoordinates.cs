﻿
using UnityEngine;

namespace TerrainEngine
{
    public class BlockUVCoordinates
    {
        private readonly Rect[] m_BlockFaceUvCoordinates = new Rect[3];
        private Vector2[,] m_BlockFaces = new Vector2[3, 8];

        public BlockUVCoordinates(Rect topUvCoordinates, Rect sideUvCoordinates, Rect bottomUvCoordinates, float EpsilonX, float EpsilonY)
        {
            BlockFaceUvCoordinates[(int)BlockHelper.BlockFace.Top] = topUvCoordinates;
            BlockFaceUvCoordinates[(int)BlockHelper.BlockFace.Side] = sideUvCoordinates;
            BlockFaceUvCoordinates[(int)BlockHelper.BlockFace.Bottom] = bottomUvCoordinates;

            AddUv(BlockHelper.BlockFace.Top, topUvCoordinates, EpsilonX, EpsilonY);
            AddUv(BlockHelper.BlockFace.Side, sideUvCoordinates, EpsilonX, EpsilonY);
            AddUv(BlockHelper.BlockFace.Bottom, bottomUvCoordinates, EpsilonX, EpsilonY);
        }

        public void AddUv(BlockHelper.BlockFace Face, Rect Uv, float EpsilonX, float EpsilonY)
        {
            m_BlockFaces[(int)Face, 0] = new Vector2(Uv.x + EpsilonX, Uv.y + Uv.height - EpsilonY); // Left Bottom
            m_BlockFaces[(int)Face, 1] = new Vector2(Uv.x + Uv.width - EpsilonX, Uv.y + Uv.height - EpsilonY); // Right Bottom
            m_BlockFaces[(int)Face, 2] = new Vector2(Uv.x + EpsilonX, Uv.y + EpsilonY); // LEFT
            m_BlockFaces[(int)Face, 3] = new Vector2(Uv.x + Uv.width - EpsilonX, Uv.y + EpsilonY); // Right
            m_BlockFaces[(int)Face, 4] = new Vector2(Uv.x + (Uv.width * 0.5f) , Uv.y + (Uv.height * 0.5f)); // Center

            m_BlockFaces[(int)Face, 5] = new Vector2(Uv.x + Uv.width - EpsilonX, Uv.y + EpsilonY);
            m_BlockFaces[(int)Face, 6] = new Vector2(Uv.x + Uv.width - EpsilonX, Uv.y + EpsilonY);
            m_BlockFaces[(int)Face, 7] = new Vector2(Uv.x + Uv.width - EpsilonX, Uv.y + EpsilonY);
        }

        public Vector2[,] BlockFaces
        {
            get { return m_BlockFaces; }
        }

        public Rect[] BlockFaceUvCoordinates
        {
            get { return m_BlockFaceUvCoordinates; }
        }
    }
}