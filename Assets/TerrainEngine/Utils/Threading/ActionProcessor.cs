using TerrainEngine;
using UnityEngine;

public class ActionProcessor : MonoBehaviour 
{
    static public ActionProcessor Instance;
    static public bool Started = false;
    static public int UnityFrameCount = 0;      // Thread-safe access
    static public ActionThread Unity
    {
        get
        {
            return Instance.UnityThread;
        }
    }


    static public ThreadManager TManager
    {
        get
        {
            return Instance.Manager;
        }
    }

    public ThreadManager Manager;

    public ActiveDisableContainer OnStart;
    public int MaxThreadCount = 0;
    public float TargetFPS = 60;
    public int OffsetFPS = 0;

    public int MaxActionTime = 0;

    public int ThreadSleepMSTime = 20;
    public int MaxThreadedActionTime = 50;
    public bool EnablePooling = true;
    public bool EnableStats = false;

    [HideInInspector]
    public bool IsRunning = true;

    public int RenderTime = 0;
    public int UpdateTime = 0;

    public ActionThread UnityThread;            // ActionThread running on UnityThread : Update()

    private ActionRenderCounter RenderCounter;
    private Camera LastCam;

	void Start() 
    {
        Debug.Log("ActionProcessor : Starting...");

        if (Instance != null)
        {
            Debug.LogError("Can not start multiple instance of ActionProcessor Class !");
            GameObject.Destroy(this);
            return;
        }

        Instance = this;

        ThreadManager.IsPlaying = Application.isPlaying;
        ThreadManager.IsWebPlayer = Application.isWebPlayer;
        ThreadManager.IsEditor = Application.isEditor;
        ThreadManager.EnableStats = EnableStats;

        if (EnableStats)
        {
            Debug.LogWarning("Warning : ActionProcessor EnableStats is true. All actions times will be recorded for analyzes. This can reduce performances");
        }

        Manager = new ThreadManager();
        Manager.EnablePooling = EnablePooling;
        Manager.MaxThreadCount = MaxThreadCount;
        Manager.MaxThreadedActionTime = MaxThreadedActionTime;
        Manager.ThreadSleepMSTime = ThreadSleepMSTime;
        Manager.Start();

        UnityThread = new ActionThread(Manager, EnablePooling, -1);

        if (OnStart != null)
            OnStart.Call();

        Started = true;
	}

    void OnDisable()
    {
        Instance = null;
        Started = false;
        IsRunning = false;

        Manager.Stop();
    }

    void Update()
    {
        ThreadManager.UnityTime = Time.realtimeSinceStartup;
        UnityFrameCount = Time.frameCount;

        Camera MainCam = Camera.main;
        if (MainCam != null)
        {
            if (LastCam == null || LastCam != MainCam)
            {
                LastCam = MainCam;
                RenderCounter = MainCam.GetComponent<ActionRenderCounter>();

                if (RenderCounter == null)
                    RenderCounter = MainCam.gameObject.AddComponent<ActionRenderCounter>();
            }
        }
    }

    void LateUpdate()
    {
        ThreadManager.UnityTime = Time.realtimeSinceStartup;

        if (RenderCounter != null)
        {
            UpdateTime = RenderCounter.GetUpdateTime();
            RenderTime = RenderCounter.GetRenderTime();
        }
        else
            UpdateTime = RenderTime = 0;

        MaxActionTime = (int)((1f / TargetFPS) * 1000f);
        MaxActionTime = MaxActionTime - UpdateTime;

        if (RenderTime > (MaxActionTime+OffsetFPS))
            OffsetFPS++;
        else if (OffsetFPS > 0)
            --OffsetFPS;

        if (RenderTime == 0)
            OffsetFPS = 0;

        UnityThread.ExtraTime = 0;

        MaxActionTime += OffsetFPS;
        if (MaxActionTime >= 0)
            UnityThread.Execute(MaxActionTime);
    }

    #region Functions

    static public GameObject CreateGameObject(GameObject Obj)
    {
        Obj = GameObject.Instantiate(Obj) as GameObject;
        return Obj;
    }

    static public GameObject CreateGameObject(GameObject Obj, Vector3 Position,Quaternion Rotation)
    {
        Obj = GameObject.Instantiate(Obj, Position,Rotation) as GameObject;
        return Obj;
    }

    static public void SetGameObjectState(GameObject Obj, bool Value, bool CheckEffects=false)
    {
        if (CheckEffects)
        {
            TimedEffectContainer Container = Obj.GetComponent<TimedEffectContainer>();
            if(Container != null && Container.Effects.Count > 0)
            {
                foreach (TimedEffect Effect in Container.Effects)
                {
                    if (Value) Effect.Enable();
                    else Effect.Disable();
                }
            }
            else
            {
                Obj.SetActiveRecursively(Value);
            }
        }
        else
        {
            Obj.SetActiveRecursively(Value);
        }
    }

    static public IAction Add(IAction Action, int ThreadID, bool UseUnityThread = true, int MaxThreadId = int.MaxValue)
    {
        if (Started)
            return Instance.AddAction(Action, ThreadID, UseUnityThread, MaxThreadId);
        else
            Debug.LogError("ActionProcessor not started !");

        return null;
    }

    static public IAction AddToUnity(IAction Action)
    {
        Instance.UnityThread.AddAction(Action);
        return Action;
    }

    static public void AddToUnity(IAction[] Actions)
    {
        Instance.UnityThread.AddAction(Actions);
    }

    static public IAction AddToThread(IAction Action)
    {
        return Add(Action, int.MaxValue, false, int.MaxValue);
    }

    static public void AddToThreadFast(IAction Action)
    {
        if (Started)
        {
            TManager.AddFast(Action);
        }
        else
            Debug.LogError("ActionProcessor not started !");
    }

    static public void AddFast(IAction Action)
    {
        TManager.FastThread.AddAction(Action);
    }

    /// <summary>
    /// Add an action to perform.
    /// </summary>
    /// <param name="Action">Action to perform</param>
    /// <param name="ThreadID">(-1 = UnityThread, 0 >= DesiredThread, if threadid > threadcount, less use thread will be selected)</param>
    private IAction AddAction(IAction Action, int ThreadID, bool UseUnityThread = true, int MaxThreadId = int.MaxValue) // -1 == Unity thread
    {
        if (ThreadID < 0)
        {
            if (ThreadID == -1)
                UnityThread.AddAction(Action);

            return Action;
        }

        Manager.AddAction(Action, ThreadID, MaxThreadId);
        return Action;
    }

    #endregion
}
