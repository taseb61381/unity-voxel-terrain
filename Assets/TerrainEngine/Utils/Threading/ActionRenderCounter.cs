using System.Diagnostics;
using UnityEngine;

public class ActionRenderCounter : MonoBehaviour 
{
    public int RenderTime;
    public int LateUpdateTime;
    private Stopwatch Watch = new Stopwatch();

    public int GetUpdateTime()
    {
        return (int)Watch.ElapsedMilliseconds - RenderTime;
    }

    public int GetRenderTime()
    {
        return RenderTime;
    }

    void OnPreRender()
    {
        Watch.Reset();
        Watch.Start();
    }

    void OnPostRender()
    {
        RenderTime = (int)Watch.ElapsedMilliseconds;

        Watch.Reset();
        Watch.Start();
    }
}
