using UnityEngine;

public class ActionProcessorCustomValues : MonoBehaviour {

    public ActionProcessor Processor;
    public string Description;

    public float TargetFPS;
    public int ThreadSleepTime;
    public int ThreadActionTime;

    private float BackTargetFPS;
    private int BackThreadSleepTime;
    private int BackThreadActionTime;

    void OnEnable()
    {
        BackTargetFPS = Processor.TargetFPS;
        BackThreadSleepTime = Processor.ThreadSleepMSTime;
        BackThreadActionTime = Processor.MaxThreadedActionTime;

        Processor.TargetFPS = TargetFPS;
        Processor.ThreadSleepMSTime = ThreadSleepTime;
        Processor.MaxThreadedActionTime = ThreadActionTime;
    }

    void OnDisable()
    {
        Processor.TargetFPS = BackTargetFPS;
        Processor.ThreadSleepMSTime = BackThreadSleepTime;
        Processor.MaxThreadedActionTime = BackThreadActionTime;
    }
}
