﻿
using TerrainEngine;
using UnityEngine;

public class DebugLogAction : IAction
{
    public string Message = "";

    public DebugLogAction(string Message)
    {
        this.Message = Message;
    }

    public override bool OnExecute(ActionThread Thread)
    {
        Debug.Log(Message);
        return true;
    }
}