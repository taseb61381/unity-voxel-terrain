﻿using System;
using TerrainEngine;
using UnityEngine;

[Serializable]
public class DeleteGameObjectAction : IAction
{
    public delegate void OnDeleteEnd(DeleteGameObjectAction Obj);

    public GameObject ToRemove;
    public OnDeleteEnd OnEnd;

    public DeleteGameObjectAction()
    {

    }

    public DeleteGameObjectAction(GameObject ToRemove)
    {
        this.ToRemove = ToRemove;
    }

    public override bool OnExecute(ActionThread Thread)
    {
        if (ToRemove != null)
                GameObject.Destroy(ToRemove);

        if (OnEnd != null)
            OnEnd(this);

        Close();
        return true;
    }
}