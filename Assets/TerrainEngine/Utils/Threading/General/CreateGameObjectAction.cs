﻿using System;
using TerrainEngine;
using UnityEngine;

[Serializable]
public class CreateGameObjectAction : IAction
{
    public delegate void CreateDoneDelegate(CreateGameObjectAction Obj);

    public GameObject ToInstanciate;
    public MeshFilter Filter;
    public Vector3 Position;
    public Quaternion Rotation;
    public Vector3 Scale;
    public GameObject CreatedObject;
    public Transform Parent;
    public float DespawnTime;
    public bool UsePool;
    public int PoolInfo;
    public CreateDoneDelegate OnEnd;
    public PrimitiveType PType = PrimitiveType.Sphere;
    public bool UsePrimitive = false;
    public string Name = null;

    public CreateGameObjectAction()
    {

    }

    public void Init(PrimitiveType PType, Vector3 Position, Quaternion Rotation, Vector3 Scale, float DespawnTime = 0, bool UsePool = false, int PoolInfo = -1)
    {
        this.UsePrimitive = true;
        this.PType = PType;
        this.Position = Position;
        this.Rotation = Rotation;
        this.Scale = Scale;
        this.DespawnTime = DespawnTime;
        this.UsePool = UsePool;
        this.PoolInfo = PoolInfo;
    }

    public void Init(GameObject ToInstanciate, Vector3 Position, Quaternion Rotation, Vector3 Scale, float DespawnTime = 0, bool UsePool = false, int PoolInfo = -1)
    {
        this.ToInstanciate = ToInstanciate;
        this.Position = Position;
        this.Rotation = Rotation;
        this.Scale = Scale;
        this.DespawnTime = DespawnTime;
        this.UsePool = UsePool;
        this.PoolInfo = PoolInfo;
    }

    public void Init(MeshFilter Filter, Vector3 Position, Quaternion Rotation, Vector3 Scale, float DespawnTime = 0, bool UsePool = false, int PoolInfo = -1)
    {
        this.Filter = Filter;
        this.Position = Position;
        this.Rotation = Rotation;
        this.Scale = Scale;
        this.DespawnTime = DespawnTime;
        this.UsePool = UsePool;
        this.PoolInfo = PoolInfo;
    }

    public override void Clear()
    {
        Name = null;
        ToInstanciate = null;
        DespawnTime = 0;
        CreatedObject = null;
        PoolInfo = -1;
        OnEnd = null;
        UsePrimitive = false;
    }

    public override bool OnExecute(ActionThread Thread)
    {
        if (Filter != null)
            ToInstanciate = Filter.gameObject;

        if (ToInstanciate == null)
        {
            if(UsePrimitive)
                CreatedObject = GameObject.CreatePrimitive(PType);
        }
        else
        {
            if (!UsePool)
                CreatedObject = ActionProcessor.CreateGameObject(ToInstanciate);
            else
            {
                if (PoolInfo == -1)
                    CreatedObject = GameObjectPool.GetGameObject(ToInstanciate.name, ToInstanciate, false);
                else
                    CreatedObject = GameObjectPool.Pools.array[PoolInfo].GetObject(ToInstanciate, false, false);
            }

            CreatedObject.name = ToInstanciate.name;
        }

        if (CreatedObject == null)
        {
            Close();
            return true;
        }

        if (Name != null)
            CreatedObject.name = Name;

        Transform transform = CreatedObject.transform;
        transform.rotation = Rotation;
        transform.position = Position;
        transform.localScale = Scale;

        if (DespawnTime > 0)
        {
            if (!UsePool)
                GameObject.Destroy(CreatedObject, DespawnTime);
            else
            {
                PoolAfterTime Script = CreatedObject.GetComponent<PoolAfterTime>();
                if (Script == null)
                    Script = CreatedObject.AddComponent<PoolAfterTime>();

                Script.PoolTime = DespawnTime;
                Script.enabled = true;
            }
        }

        if(Parent != null)
            transform.parent = Parent;

        if (CreatedObject.activeInHierarchy == false)
            CreatedObject.SetActiveRecursively(true);

        if (OnEnd != null)
            OnEnd(this);

        Close();
        return true;
    }
}