﻿using System.Collections.Generic;

namespace TerrainEngine
{
    public class WaitActionDone : IAction
    {
        public List<IAction> Actions = new List<IAction>();

        public override bool OnExecute(ActionThread Thread)
        {
            for (int i = 0; i < Actions.Count; ++i)
            {
                if (Actions[i] != null && !Actions[i].IsDone())
                    return false;
            }

            return true;
        }

        public void AddAction(IAction Action)
        {
            if (Action == null)
                return;

            Actions.Add(Action);
        }
    }
}
