﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerrainEngine
{
    [Serializable]
    public class NodeLevelInformation
    {
        public delegate int LevelDistanceDelegate(int Level,float Distance,int Power);

        public int MaxLODLevel = 999;
        public int MinLODLevel = 0;
        public int[] LODPowers;
        private float[] LODDistances;

        public int GetMaxLevel(int TreeSize,int NodeSize)
        {
            int MaxLevel = 0;
            while (TreeSize > NodeSize)
            {
                TreeSize /= 2;
                ++MaxLevel;
            }
            return MaxLevel;
        }

        public int SetMaxLevel(int TreeSize,int NodeSize)
        {
           return SetMaxLevel(GetMaxLevel(TreeSize, NodeSize));
        }

        public int SetMaxLevel(int Count)
        {
            if (LODPowers == null)
                LODPowers = new int[Count];
            else if(LODPowers.Length < Count)
            {
                int[] NewPowers = new int[Count];
                for (int i = 0; i < LODPowers.Length; ++i)
                    NewPowers[i] = LODPowers[i];
                LODPowers = NewPowers;
            }
            else if (LODPowers.Length > Count)
            {
                int[] NewPowers = new int[Count];
                for (int i = 0; i < Count; ++i)
                    NewPowers[i] = LODPowers[i];
                LODPowers = NewPowers;
            }

            return Count;
        }

        public float GetLODDistance(byte Level)
        {
            return LODDistances[Level];
        }

        public void GetDistances(int TreeSize,int NodeSize, LevelDistanceDelegate del)
        {
            SetMaxLevel(TreeSize, NodeSize);

            int Length = LODPowers.Length;
            float Distance = 0;
            float LastDistance = 0;
            int Power = 0;
            int DistancePower = 0;
            for (int i = 0; i < Length; ++i)
            {
                Power = LODPowers[Length - i - 1];

                DistancePower = Power + 1 + (i == 0 ? 2 : 0);
                Distance = DistancePower * (NodeSize * (i + 1));

                LODPowers[Length - i - 1] = del(i, Distance + LastDistance, Power);

                LastDistance += Distance;
            }
        }

        public void GenerateDistancePowers(int TreeSize, int NodeSize)
        {
            SetMaxLevel(TreeSize, NodeSize);

            LODDistances = new float[LODPowers.Length];
            float Value = 0;
            for (int i = LODPowers.Length - 1; i >= 0; --i)
            {
                LODDistances[i] += LODPowers[i];
                Value = LODPowers[i];

                for (int j = i - 1; j >= 0; --j)
                {
                    Value *= 0.5f;
                    LODDistances[j] += Value;
                }
            }
        }
    }
}
