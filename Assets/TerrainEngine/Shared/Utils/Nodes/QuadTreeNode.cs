﻿using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    public class QuadTreeChildsArray<T>
    {
        public QuadTreeNode<T>[] Array;
        public int Count = 0;

        public QuadTreeChildsArray(QuadTreeNode<T> MainNode, int MaxLevel)
        {
            Array = new QuadTreeNode<T>[QuadTreeNode<T>.GetMaxChilds(MaxLevel) + 1];
            Array[0] = MainNode;
            Count = 1;
        }

        public void GetIndex(ref int Index)
        {
            Index = Count;
            Count += 8;
        }
    }

    public struct QuadTreeNode<T>
    {
        public byte m_level, m_id;
        public Vector3 m_position;
        public float m_size;
        public bool Closed;
        public Vector3 m_center
        {
            get
            {
                return new Vector3(m_position.x + m_size * 0.5f, m_position.y + m_size * 0.5f, m_position.z + m_size * 0.5f);
            }
        }

        public int ParentIndex; // Parent Index in Array
        public int StartIndex;  // StartIndex for childs in Array
        public int Index; // Index of this node in Array
        public T Data; // Data that node contains

        public QuadTreeNode(byte id, byte ParentLevel, int ParentStartIndex, int ParentIndex, Vector3 Position, float Size)
        {
            this.StartIndex = -1;
            this.Data = default(T);

            this.Closed = false;
            this.m_id = id;
            this.m_position = Position;
            this.m_size = Size;
            if (ParentIndex != -1)
            {
                this.m_level = (byte)(ParentLevel + 1);
                this.Index = ParentStartIndex + id;
                this.ParentIndex = ParentIndex;
            }
            else
            {
                this.Index = 0;
                this.ParentIndex = -1;
                this.m_level = 0;
            }
        }

        public void Init(byte id, byte ParentLevel, int ParentStartIndex, int ParentIndex, Vector3 Position, float Size)
        {
            this.Closed = false;
            this.m_id = id;
            this.m_position = Position;
            this.m_size = Size;

            if (ParentIndex != -1)
            {
                this.m_level = (byte)(ParentLevel + 1);
                this.Index = ParentStartIndex + id;
                this.ParentIndex = ParentIndex;
            }
            else
            {
                this.Index = 0;
                this.ParentIndex = -1;
                this.m_level = 0;
            }
        }

        public void SubDivise(QuadTreeChildsArray<T> Array, int Level)
        {
            if (m_level >= Level)
            {
                CloseChilds(Array);
                return;
            }

            if (!HasChilds())
            {
                Closed = false;

                float MidSize = m_size * 0.5f;

                if (!HasChildArray())
                {
                    Array.GetIndex(ref StartIndex);
                    Array.Array[StartIndex] = new QuadTreeNode<T>(0, m_level, StartIndex, Index, m_position, MidSize);
                    Array.Array[StartIndex + 1] = new QuadTreeNode<T>(1, m_level, StartIndex, Index, m_position + new Vector3(0, 0, MidSize), MidSize);
                    Array.Array[StartIndex + 2] = new QuadTreeNode<T>(2, m_level, StartIndex, Index, m_position + new Vector3(MidSize, 0, MidSize), MidSize);
                    Array.Array[StartIndex + 3] = new QuadTreeNode<T>(3, m_level, StartIndex, Index, m_position + new Vector3(MidSize, 0, 0), MidSize);
                }
                else
                {
                    Array.Array[StartIndex].Init(0, m_level, StartIndex, Index, m_position, MidSize);
                    Array.Array[StartIndex + 1].Init(1, m_level, StartIndex, Index, m_position + new Vector3(0, 0, MidSize), MidSize);
                    Array.Array[StartIndex + 2].Init(2, m_level, StartIndex, Index, m_position + new Vector3(MidSize, 0, MidSize), MidSize);
                    Array.Array[StartIndex + 3].Init(3, m_level, StartIndex, Index, m_position + new Vector3(MidSize, 0, 0), MidSize);
                }
            }

            Array.Array[StartIndex].SubDivise(Array, Level);
            Array.Array[StartIndex + 1].SubDivise(Array, Level);
            Array.Array[StartIndex + 2].SubDivise(Array, Level);
            Array.Array[StartIndex + 3].SubDivise(Array, Level);
        }

        public void Close(QuadTreeChildsArray<T> Array)
        {
            CloseChilds(Array);
        }

        public void Reset(QuadTreeChildsArray<T> Array)
        {
            Closed = true;
            if (HasChildArray())
            {
                float MidSize = m_size * 0.5f;
                Array.Array[StartIndex].Init(0, m_level, StartIndex, Index, m_position, MidSize);
                Array.Array[StartIndex + 1].Init(1, m_level, StartIndex, Index, m_position + new Vector3(0, 0, MidSize), MidSize);
                Array.Array[StartIndex + 2].Init(2, m_level, StartIndex, Index, m_position + new Vector3(MidSize, 0, MidSize), MidSize);
                Array.Array[StartIndex + 3].Init(3, m_level, StartIndex, Index, m_position + new Vector3(MidSize, 0, 0), MidSize);

                Array.Array[StartIndex].Reset(Array);
                Array.Array[StartIndex + 1].Reset(Array);
                Array.Array[StartIndex + 2].Reset(Array);
                Array.Array[StartIndex + 3].Reset(Array);
            }
        }

        public int GetOpenParent(QuadTreeChildsArray<T> Array)
        {
            if (ParentIndex == -1 || Array.Array[ParentIndex].HasChilds())
                return Index;

            return Array.Array[ParentIndex].GetOpenParent(Array);
        }

        public void GetArrayCount(QuadTreeChildsArray<T> Array, ref int Count)
        {
            if (HasChilds())
            {
                Count += 4;
                for (int i = 0; i < 4; ++i)
                    Array.Array[StartIndex + i].GetArrayCount(Array, ref Count);
            }
        }

        static public int GetMaxChilds(int MaxLevel)
        {
            int Count = 4;
            int Childs = 4;
            for (int i = 0; i < MaxLevel - 1; ++i)
            {
                Count += Childs * 4;
                Childs = Childs * 4;
            }

            return Count;
        }

        #region Childs

        public bool HasChilds()
        {
            if (!Closed && HasChildArray())
                return true;

            return false;
        }

        public bool HasChildArray()
        {
            return StartIndex != -1;
        }

        public void GetChilds(QuadTreeChildsArray<T> Array, List<int> L)
        {
            if (HasChilds())
            {
                Array.Array[StartIndex].GetChilds(Array, L);
                Array.Array[StartIndex + 1].GetChilds(Array, L);
                Array.Array[StartIndex + 2].GetChilds(Array, L);
                Array.Array[StartIndex + 3].GetChilds(Array, L);
            }
            else
                L.Add(Index);
        }

        public void GetAllChilds(QuadTreeChildsArray<T> Array, List<int> L)
        {
            L.Add(Index);
            if (StartIndex != -1)
            {
                Array.Array[StartIndex].GetAllChilds(Array, L);
                Array.Array[StartIndex + 1].GetAllChilds(Array, L);
                Array.Array[StartIndex + 2].GetAllChilds(Array, L);
                Array.Array[StartIndex + 3].GetAllChilds(Array, L);
            }
        }

        public void CloseChilds(QuadTreeChildsArray<T> Array)
        {
            if (HasChilds())
            {
                Closed = true;

                Array.Array[StartIndex].Close(Array);
                Array.Array[StartIndex + 1].Close(Array);
                Array.Array[StartIndex + 2].Close(Array);
                Array.Array[StartIndex + 3].Close(Array);
            }
        }

        public bool GetChild(QuadTreeChildsArray<T> Array, Vector3 Position, ref int NodeIndex)
        {
            if (Contains(Position))
            {
                if (HasChilds())
                {
                    if (Array.Array[StartIndex].GetChild(Array, Position, ref NodeIndex))
                        return true;
                    if (Array.Array[StartIndex + 1].GetChild(Array, Position, ref NodeIndex))
                        return true;
                    if (Array.Array[StartIndex + 2].GetChild(Array, Position, ref NodeIndex))
                        return true;
                    if (Array.Array[StartIndex + 3].GetChild(Array, Position, ref NodeIndex))
                        return true;
                }
                else
                {
                    NodeIndex = Index;
                    return true;
                }
            }

            return false;
        }

        public bool Contains(Vector3 Position)
        {
            if (Position.x >= m_position.x
                && Position.z >= m_position.z)
            {
                if (Position.x < m_position.x + m_size
                    && Position.z < m_position.z + m_size)
                {
                    return true;
                }
            }

            return false;
        }

        public bool ContainsAround(Vector3 Position, float Power)
        {
            float offset = 1f + (0.125f * m_level) * Power;
            if (Position.x >= m_position.x - m_size * offset
                && Position.z >= m_position.z - m_size * offset)
            {
                if (Position.x < m_position.x + m_size * (1f + offset)
                    && Position.z < m_position.z + m_size * (1f + offset))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Positions

        public Vector3 Right
        {
            get
            {
                return m_position + new Vector3(m_size, 0, 0);
            }
        }

        public Vector3 Forward
        {
            get
            {
                return m_position + new Vector3(0, 0, m_size);
            }
        }

        public Vector3 RightForward
        {
            get
            {
                return m_position + new Vector3(m_size, 0, m_size);
            }
        }

        #endregion

        public override string ToString()
        {
            return "Level:" + m_level + ",Index:" + Index + ",ParentIndex:" + ParentIndex;
        }
    }
}