﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace TerrainEngine
{
    public class OctreeChildsArray<T> where T : struct
    {
        public OctreeNode<T>[] Array;
        public BitArray ClosedNodes;

        public int Count = 0;
        public int MaxLevel;

        public OctreeChildsArray(OctreeNode<T> MainNode, int MaxLevel)
        {
            this.MaxLevel = MaxLevel;

            Array = new OctreeNode<T>[OctreeNode<T>.GetMaxChilds(MaxLevel) + 1];
            Array[0] = MainNode;
            Count = 1;

            ClosedNodes = new BitArray(Array.Length);
            for (int i = Array.Length - 1; i >= 0; --i)
                ClosedNodes[i] = true;
        }

        public OctreeChildsArray(OctreeChildsArray<T> Parent)
        {
            Array = Parent.Array;
            MaxLevel = Parent.MaxLevel;
            ClosedNodes = new BitArray(Array.Length);
        }

        static public int GetMaxLevel(int Size,int Count)
        {
            int MaxLevel = 0;
            while (Size > Count)
            {
                Size /= 2;
                ++MaxLevel;
            }
            return MaxLevel;
        }

        public void GetIndex(ref int Index)
        {
            Index = Count;
            Count += 8;
        }

        public override string ToString()
        {
            int C = 0;
            for (int i = Array.Length - 1; i >= 0; --i)
            {
                if (ClosedNodes[i])
                {
                    ++C;
                }
            }

            return "(Close:" + C + ",Open:" + (Array.Length - C) + ")";
        }
    }

    public struct OctreeNode<T> where T : struct
    {
        public delegate void OnSubDiviseDelegate(int Index, int x, int y, int z, int Size);

        public enum NodesId : byte
        {
            Left = 0,
            Forward = 1,
            ForwardRight = 2,
            Right = 3,
            Top = 4,
            TopForward = 5,
            TopForwardRight = 6,
            TopRight = 7,
        }

        public byte m_id;
        public byte m_level;
        public int Index;
        public int ParentIndex; // Parent Index in Array
        public int StartIndex;  // StartIndex for childs in Array
        public int EndIndex;    // StartIndex to EndIndex of all childs
        public T Data;          // Custom data stored in Octree

        public OctreeNode(byte id, byte ParentLevel, int ParentStartIndex, int ParentIndex)
        {
            this.Index = ParentStartIndex + id;
            this.StartIndex = -1;
            this.EndIndex = -1;
            this.m_id = id;
            this.Data = default(T);

            if (ParentIndex != -1)
            {
                this.m_level = (byte)(ParentLevel + 1);
                this.ParentIndex = ParentIndex;
            }
            else
            {
                this.ParentIndex = -1;
                this.m_level = 0;
            }
        }

        public void Init(OctreeChildsArray<T> Array, byte id, byte ParentLevel, int ParentStartIndex, int ParentIndex)
        {
            this.Index = ParentStartIndex + id;
            this.m_id = id;
            Array.ClosedNodes[Index] = false;

            if (ParentIndex != -1)
            {
                this.m_level = (byte)(ParentLevel + 1);
                this.ParentIndex = ParentIndex;
            }
            else
            {
                this.ParentIndex = -1;
                this.m_level = 0;
            }

            if (Array != null && HasChildArray())
            {
                Array.ClosedNodes[Index] = true;
                Array.Array[StartIndex].Init(Array, 0, m_level, StartIndex, Index);
                Array.Array[StartIndex + 1].Init(Array, 1, m_level, StartIndex, Index);
                Array.Array[StartIndex + 2].Init(Array, 2, m_level, StartIndex, Index);
                Array.Array[StartIndex + 3].Init(Array, 3, m_level, StartIndex, Index);
                Array.Array[StartIndex + 4].Init(Array, 4, m_level, StartIndex, Index);
                Array.Array[StartIndex + 5].Init(Array, 5, m_level, StartIndex, Index);
                Array.Array[StartIndex + 6].Init(Array, 6, m_level, StartIndex, Index);
                Array.Array[StartIndex + 7].Init(Array, 7, m_level, StartIndex, Index);
            }
        }

        public void SubDivise(OctreeChildsArray<T> Array, int Level, int Index)
        {
            if (m_level >= Level)
            {
                CloseChilds(Array,Index);
                return;
            }

            if (!HasChilds(Array, Index))
            {
                Array.ClosedNodes[Index] = false;
                if (!HasChildArray())
                {
                    Array.GetIndex(ref StartIndex);
                    Array.Array[StartIndex] = new OctreeNode<T>(0, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 1] = new OctreeNode<T>(1, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 2] = new OctreeNode<T>(2, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 3] = new OctreeNode<T>(3, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 4] = new OctreeNode<T>(4, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 5] = new OctreeNode<T>(5, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 6] = new OctreeNode<T>(6, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 7] = new OctreeNode<T>(7, m_level, StartIndex, Index);
                }
                else
                {
                    Array.Array[StartIndex].Init(Array, 0, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 1].Init(Array, 1, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 2].Init(Array, 2, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 3].Init(Array, 3, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 4].Init(Array, 4, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 5].Init(Array, 5, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 6].Init(Array, 6, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 7].Init(Array, 7, m_level, StartIndex, Index);
                }
            }

            Array.Array[StartIndex].SubDivise(Array, Level, StartIndex);
            Array.Array[StartIndex + 1].SubDivise(Array, Level, StartIndex+1);
            Array.Array[StartIndex + 2].SubDivise(Array, Level, StartIndex+2);
            Array.Array[StartIndex + 3].SubDivise(Array, Level, StartIndex+3);
            Array.Array[StartIndex + 4].SubDivise(Array, Level, StartIndex+4);
            Array.Array[StartIndex + 5].SubDivise(Array, Level, StartIndex+5);
            Array.Array[StartIndex + 6].SubDivise(Array, Level, StartIndex+6);
            Array.Array[StartIndex + 7].SubDivise(Array, Level, StartIndex + 7);

            if(EndIndex == -1)
                EndIndex = Array.Count;
        }

        public void SubDivise(byte Id, OctreeChildsArray<T> Array, int Index, int x, int y, int z, int Size, OnSubDiviseDelegate OnSubDivise)
        {
            OnSubDivise(Index, x, y, z, Size);

            if (m_level >= Array.MaxLevel)
            {
                CloseChilds(Array,Index);
                return;
            }

            if (!HasChilds(Array, Index))
            {
                Array.ClosedNodes[Index] = false;

                if (!HasChildArray())
                {
                    Array.GetIndex(ref StartIndex);
                    Array.Array[StartIndex] = new OctreeNode<T>(0, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 1] = new OctreeNode<T>(1, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 2] = new OctreeNode<T>(2, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 3] = new OctreeNode<T>(3, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 4] = new OctreeNode<T>(4, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 5] = new OctreeNode<T>(5, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 6] = new OctreeNode<T>(6, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 7] = new OctreeNode<T>(7, m_level, StartIndex, Index);
                }
                else
                {
                    Array.Array[StartIndex].Init(null, 0, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 1].Init(null, 1, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 2].Init(null, 2, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 3].Init(null, 3, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 4].Init(null, 4, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 5].Init(null, 5, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 6].Init(null, 6, m_level, StartIndex, Index);
                    Array.Array[StartIndex + 7].Init(null, 7, m_level, StartIndex, Index);
                }
            }

            Size /= 2;

            Array.Array[StartIndex].SubDivise(0, Array, StartIndex, x, y, z, Size, OnSubDivise);
            Array.Array[StartIndex + 1].SubDivise(1, Array, StartIndex + 1, x, y, z + Size, Size, OnSubDivise);
            Array.Array[StartIndex + 2].SubDivise(2, Array, StartIndex + 2, x + Size, y, z + Size, Size, OnSubDivise);
            Array.Array[StartIndex + 3].SubDivise(3, Array, StartIndex + 3, x + Size, y, z, Size, OnSubDivise);
            Array.Array[StartIndex + 4].SubDivise(4, Array, StartIndex + 4, x, y + Size, z, Size, OnSubDivise);
            Array.Array[StartIndex + 5].SubDivise(5, Array, StartIndex + 5, x, y + Size, z + Size, Size, OnSubDivise);
            Array.Array[StartIndex + 6].SubDivise(6, Array, StartIndex + 6, x + Size, y + Size, z + Size, Size, OnSubDivise);
            Array.Array[StartIndex + 7].SubDivise(7, Array, StartIndex + 7, x + Size, y + Size, z, Size, OnSubDivise);
            if (EndIndex == -1)
                EndIndex = Array.Count;
        }

        public void SubDivise(OctreeChildsArray<T> NodesArray, int Index, float x, float y, float z, float Size,int MinimumLodLevel, NodeLevelInformation Levels, TransformsContainer Transforms, List<KeyValuePair<int, bool>> Result)
        {
            byte m_level = NodesArray.Array[Index].m_level;
            if (m_level >= NodesArray.MaxLevel)
            {
                return;
            }

            if (m_level <= MinimumLodLevel
                || Transforms.GetAllPositions((int CameraId, Vector3 Position) =>
                { return NodesArray.Array[Index].ContainsAround(x, y, z, Size, Position.x, Position.y, Position.z, Levels.GetLODDistance(m_level)); }))
            {
                if (!NodesArray.Array[Index].HasChilds(NodesArray, Index))
                {
                    NodesArray.Array[Index].SubDivise(NodesArray, m_level + 1, Index);

                    if (Result != null)
                        Result.Add(new KeyValuePair<int, bool>(Index, true));
                }

                int StartIndex = NodesArray.Array[Index].StartIndex;

                float MidSize = Size * 0.5f;

                SubDivise(NodesArray, StartIndex, x, y, z, MidSize, MinimumLodLevel,Levels,Transforms, Result);
                SubDivise(NodesArray, StartIndex + 1, x, y, z + MidSize, MidSize, MinimumLodLevel, Levels, Transforms, Result);
                SubDivise(NodesArray, StartIndex + 2, x + MidSize, y, z + MidSize, MidSize, MinimumLodLevel, Levels, Transforms, Result);
                SubDivise(NodesArray, StartIndex + 3, x + MidSize, y, z, MidSize, MinimumLodLevel, Levels, Transforms, Result);
                SubDivise(NodesArray, StartIndex + 4, x, y + MidSize, z, MidSize, MinimumLodLevel, Levels, Transforms, Result);
                SubDivise(NodesArray, StartIndex + 5, x, y + MidSize, z + MidSize, MidSize, MinimumLodLevel, Levels, Transforms, Result);
                SubDivise(NodesArray, StartIndex + 6, x + MidSize, y + MidSize, z + MidSize, MidSize, MinimumLodLevel, Levels, Transforms, Result);
                SubDivise(NodesArray, StartIndex + 7, x + MidSize, y + MidSize, z, MidSize, MinimumLodLevel, Levels, Transforms, Result);
            }
            else if (NodesArray.Array[Index].HasChilds(NodesArray, Index))
            {
                if (Result != null)
                    Result.Add(new KeyValuePair<int, bool>(Index, false));

                NodesArray.Array[Index].CloseChilds(NodesArray, Index);
            }
        }

        public void Close(OctreeChildsArray<T> Array, int Index)
        {
            CloseChilds(Array, Index);
        }

        public int GetOpenParent(OctreeChildsArray<T> Array)
        {
            if (ParentIndex == -1)
            {
                return 0;
            }

            if (Array.Array[ParentIndex].HasChilds(Array, ParentIndex))
            {
                return Index;
            }

            return Array.Array[ParentIndex].GetOpenParent(Array);
        }

        public bool HasDifferentLevelAround(OctreeChildsArray<T> Array)
        {
            int Around = GetLeft(Array);
            if (Around == -1 || Array.Array[Around].HasChilds(Array, Around))
                return true;

            Around = GetRight(Array);
            if (Around == -1 || Array.Array[Around].HasChilds(Array, Around))
                return true;

            Around = GetTop(Array);
            if (Around == -1 || Array.Array[Around].HasChilds(Array, Around))
                return true;

            Around = GetBottom(Array);
            if (Around == -1 || Array.Array[Around].HasChilds(Array, Around))
                return true;

            return false;
        }

        //0:BottomLeft
        //1:BottomForward
        //2:BottomForwardRight
        //3:BottomRight
        //4:Top
        //5:TopForward
        //6:TopForwardRight
        //7:TopRight

        public int GetLeft(OctreeChildsArray<T> Array)
        {
            if (ParentIndex == -1)
                return -1;

            if (m_id == (byte)NodesId.Left || m_id == (byte)NodesId.Forward || m_id == (byte)NodesId.Top || m_id == (byte)NodesId.TopForward) // Special cases, need to get parent left node
            {
                int Parent = Array.Array[ParentIndex].GetLeft(Array);
                if (Parent != -1 && Array.Array[Parent].HasChilds(Array, Parent))
                {
                    if (m_id == (byte)NodesId.Left) return Array.Array[Parent].StartIndex + (byte)NodesId.Right;
                    if (m_id == (byte)NodesId.Forward) return Array.Array[Parent].StartIndex + (byte)NodesId.ForwardRight;
                    if (m_id == (byte)NodesId.Top) return Array.Array[Parent].StartIndex + (byte)NodesId.TopRight;
                    if (m_id == (byte)NodesId.TopForward) return Array.Array[Parent].StartIndex + (byte)NodesId.TopForwardRight;
                }
            }
            else
            {
                if (m_id == (byte)NodesId.Right) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Left;
                if (m_id == (byte)NodesId.ForwardRight) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Forward;
                if (m_id == (byte)NodesId.TopRight) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Top;
                if (m_id == (byte)NodesId.TopForwardRight) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.TopForward;
            }

            return -1;
        }

        public int GetRight(OctreeChildsArray<T> Array)
        {
            if (ParentIndex == -1)
                return -1;

            if (m_id == (byte)NodesId.Right || m_id == (byte)NodesId.ForwardRight || m_id == (byte)NodesId.TopRight || m_id == (byte)NodesId.TopForwardRight) // Special cases, need to get parent left node
            {
                int Parent = Array.Array[ParentIndex].GetRight(Array);
                if (Parent != -1 && Array.Array[Parent].HasChilds(Array, Parent))
                {
                    if (m_id == (byte)NodesId.Right) return Array.Array[Parent].StartIndex + (byte)NodesId.Left;
                    if (m_id == (byte)NodesId.ForwardRight) return Array.Array[Parent].StartIndex + (byte)NodesId.Forward;
                    if (m_id == (byte)NodesId.TopRight) return Array.Array[Parent].StartIndex + (byte)NodesId.Top;
                    if (m_id == (byte)NodesId.TopForwardRight) return Array.Array[Parent].StartIndex + (byte)NodesId.TopForward;
                }
            }
            else
            {
                if (m_id == (byte)NodesId.Left) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Right;
                if (m_id == (byte)NodesId.Forward) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.ForwardRight;
                if (m_id == (byte)NodesId.Top) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.TopRight;
                if (m_id == (byte)NodesId.TopForward) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.TopForwardRight;
            }

            return -1;
        }

        public int GetForward(OctreeChildsArray<T> Array)
        {
            if (ParentIndex == -1)
                return -1;

            if (m_id == (byte)NodesId.Forward || m_id == (byte)NodesId.ForwardRight || m_id == (byte)NodesId.TopForward || m_id == (byte)NodesId.TopForwardRight) // Special cases, need to get parent left node
            {
                int Parent = Array.Array[ParentIndex].GetForward(Array);
                if (Parent != -1 && Array.Array[Parent].HasChilds(Array, Parent))
                {
                    if (m_id == (byte)NodesId.Forward) return Array.Array[Parent].StartIndex + (byte)NodesId.Left;
                    if (m_id == (byte)NodesId.ForwardRight) return Array.Array[Parent].StartIndex + (byte)NodesId.Right;
                    if (m_id == (byte)NodesId.TopForward) return Array.Array[Parent].StartIndex + (byte)NodesId.Top;
                    if (m_id == (byte)NodesId.TopForwardRight) return Array.Array[Parent].StartIndex + (byte)NodesId.TopRight;
                }
            }
            else
            {
                if (m_id == (byte)NodesId.Left) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Forward;
                if (m_id == (byte)NodesId.Right) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.ForwardRight;
                if (m_id == (byte)NodesId.Top) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.TopForward;
                if (m_id == (byte)NodesId.TopRight) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.TopForwardRight;
            }

            return -1;
        }

        public int GetBackward(OctreeChildsArray<T> Array)
        {
            if (ParentIndex == -1)
                return -1;

            if (m_id == (byte)NodesId.Left || m_id == (byte)NodesId.Right || m_id == (byte)NodesId.Top || m_id == (byte)NodesId.TopRight) // Special cases, need to get parent left node
            {
                int Parent = Array.Array[ParentIndex].GetBackward(Array);
                if (Parent != -1 && Array.Array[Parent].HasChilds(Array, Parent))
                {
                    if (m_id == (byte)NodesId.Left) return Array.Array[Parent].StartIndex + (byte)NodesId.Forward;
                    if (m_id == (byte)NodesId.Right) return Array.Array[Parent].StartIndex + (byte)NodesId.ForwardRight;
                    if (m_id == (byte)NodesId.Top) return Array.Array[Parent].StartIndex + (byte)NodesId.TopForward;
                    if (m_id == (byte)NodesId.TopRight) return Array.Array[Parent].StartIndex + (byte)NodesId.TopForwardRight;
                }
            }
            else
            {
                if (m_id == (byte)NodesId.Forward) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Left;
                if (m_id == (byte)NodesId.ForwardRight) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Right;
                if (m_id == (byte)NodesId.TopForward) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Top;
                if (m_id == (byte)NodesId.TopForwardRight) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.TopRight;
            }

            return -1;
        }

        public int GetTop(OctreeChildsArray<T> Array)
        {
            if (ParentIndex == -1)
                return -1;

            if (m_id == (byte)NodesId.Top || m_id == (byte)NodesId.TopForward || m_id == (byte)NodesId.TopForwardRight || m_id == (byte)NodesId.TopRight) // Special cases, need to get parent left node
            {
                int Parent = Array.Array[ParentIndex].GetTop(Array);
                if (Parent != -1 && Array.Array[Parent].HasChilds(Array, Parent))
                {
                    if (m_id == (byte)NodesId.Top) return Array.Array[Parent].StartIndex + (byte)NodesId.Left;
                    if (m_id == (byte)NodesId.TopForward) return Array.Array[Parent].StartIndex + (byte)NodesId.Forward;
                    if (m_id == (byte)NodesId.TopForwardRight) return Array.Array[Parent].StartIndex + (byte)NodesId.ForwardRight;
                    if (m_id == (byte)NodesId.TopRight) return Array.Array[Parent].StartIndex + (byte)NodesId.Right;
                }
            }
            else
            {
                if (m_id == (byte)NodesId.Left) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Top;
                if (m_id == (byte)NodesId.Forward) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.TopForward;
                if (m_id == (byte)NodesId.Right) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.TopRight;
                if (m_id == (byte)NodesId.ForwardRight) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.TopForwardRight;
            }

            return -1;
        }

        public int GetBottom(OctreeChildsArray<T> Array)
        {
            if (ParentIndex == -1)
                return -1;

            if (m_id == (byte)NodesId.Left || m_id == (byte)NodesId.Right || m_id == (byte)NodesId.Forward || m_id == (byte)NodesId.ForwardRight) // Special cases, need to get parent left node
            {
                int Parent = Array.Array[ParentIndex].GetBottom(Array);
                if (Parent != -1 && Array.Array[Parent].HasChilds(Array, Parent))
                {
                    if (m_id == (byte)NodesId.Left) return Array.Array[Parent].StartIndex + (byte)NodesId.Top;
                    if (m_id == (byte)NodesId.Right) return Array.Array[Parent].StartIndex + (byte)NodesId.TopRight;
                    if (m_id == (byte)NodesId.Forward) return Array.Array[Parent].StartIndex + (byte)NodesId.TopForward;
                    if (m_id == (byte)NodesId.ForwardRight) return Array.Array[Parent].StartIndex + (byte)NodesId.TopForwardRight;
                }
            }
            else
            {
                if (m_id == (byte)NodesId.Top) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Left;
                if (m_id == (byte)NodesId.TopForward) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Forward;
                if (m_id == (byte)NodesId.TopRight) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.Right;
                if (m_id == (byte)NodesId.TopForwardRight) return Array.Array[ParentIndex].StartIndex + (byte)NodesId.ForwardRight;
            }

            return -1;
        }

        static public int GetMaxChilds(int MaxLevel)
        {
            int Count = 8;
            int Childs = 8;
            for (int i = 0; i < MaxLevel - 1; ++i)
            {
                Count += Childs * 8;
                Childs = Childs * 8;
            }

            return Count;
        }

        #region Childs

        public bool HasChilds(OctreeChildsArray<T> Array, int Index)
        {
            if (!Array.ClosedNodes[Index] && HasChildArray())
                return true;

            return false;
        }

        public bool HasChilds(OctreeChildsArray<T> Array)
        {
            if (!Array.ClosedNodes[Index] && HasChildArray())
                return true;

            return false;
        }

        public bool HasChildArray()
        {
            return StartIndex != -1;
        }

        public void GetChilds(OctreeChildsArray<T> Array, List<int> L)
        {
            if (HasChilds(Array))
            {
                Array.Array[StartIndex].GetChilds(Array, L);
                Array.Array[StartIndex + 1].GetChilds(Array, L);
                Array.Array[StartIndex + 2].GetChilds(Array, L);
                Array.Array[StartIndex + 3].GetChilds(Array, L);
                Array.Array[StartIndex + 4].GetChilds(Array, L);
                Array.Array[StartIndex + 5].GetChilds(Array, L);
                Array.Array[StartIndex + 6].GetChilds(Array, L);
                Array.Array[StartIndex + 7].GetChilds(Array, L);
            }
            else
                L.Add(Index);
        }

        public void GetAllChilds(OctreeChildsArray<T> Array, List<int> L)
        {
            L.Add(Index);

            if (StartIndex != -1)
            {
                Array.Array[StartIndex].GetAllChilds(Array, L);
                Array.Array[StartIndex + 1].GetAllChilds(Array, L);
                Array.Array[StartIndex + 2].GetAllChilds(Array, L);
                Array.Array[StartIndex + 3].GetAllChilds(Array, L);
                Array.Array[StartIndex + 4].GetAllChilds(Array, L);
                Array.Array[StartIndex + 5].GetAllChilds(Array, L);
                Array.Array[StartIndex + 6].GetAllChilds(Array, L);
                Array.Array[StartIndex + 7].GetAllChilds(Array, L);
            }
        }

        public void CloseChilds(OctreeChildsArray<T> Array, int Index)
        {
            if (HasChilds(Array, Index))
            {
                Array.ClosedNodes[Index] = true;

                Array.Array[StartIndex].Close(Array, StartIndex);
                Array.Array[StartIndex + 1].Close(Array, StartIndex + 1);
                Array.Array[StartIndex + 2].Close(Array, StartIndex + 2);
                Array.Array[StartIndex + 3].Close(Array, StartIndex + 3);
                Array.Array[StartIndex + 4].Close(Array, StartIndex + 4);
                Array.Array[StartIndex + 5].Close(Array, StartIndex + 5);
                Array.Array[StartIndex + 6].Close(Array, StartIndex + 6);
                Array.Array[StartIndex + 7].Close(Array, StartIndex + 7);
            }
        }

        public bool ContainsAround(float m_x,float m_y,float m_z, float m_size, float x,float y,float z, float Power)
        {
            float offset = 1f + Power;

            if (x >= m_x - m_size * offset
                && y >= m_y - m_size * offset
                && z >= m_z - m_size * offset)
            {
                if (x < m_x + m_size * (1f + offset)
                    && y < m_y + m_size * (1f + offset)
                    && z < m_z + m_size * (1f + offset))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        public override string ToString()
        {
            return "Level:" + m_level + ",ParentIndex:" + ParentIndex;
        }
    }
}