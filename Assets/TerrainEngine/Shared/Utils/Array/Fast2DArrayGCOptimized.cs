﻿
namespace TerrainEngine
{
    public class Fast2DArrayGCOptimized<T>
    {
        public int SizeX, SizeY, MaxSize;
        public T[][] Array;

        public Fast2DArrayGCOptimized(int SizeX, int SizeY, int MaxSize)
        {
            this.SizeX = SizeX;
            this.SizeY = SizeY;

            int TotalSize = SizeX * SizeY;
            if (MaxSize > TotalSize) MaxSize = TotalSize;

            int Count = (TotalSize / MaxSize);
            if (TotalSize % MaxSize != 0) ++Count;

            MaxSize = (TotalSize / Count) + 1;

            this.MaxSize = MaxSize;
            this.Array = new T[Count][];
        }

        public T this[int x, int y]
        {
            get
            {
                int Index = y + x * SizeY;
                T[] SmallArray = Array[Index / MaxSize];

                if (SmallArray == null)
                    return default(T);

                return SmallArray[Index % MaxSize];
            }
            set
            {
                int Index = y + x * SizeY;
                T[] SmallArray = Array[Index / MaxSize];

                if (SmallArray == null)
                    SmallArray = Array[Index / MaxSize] = new T[MaxSize];

                SmallArray[Index % MaxSize] = value;
            }
        }

        public T this[int Index]
        {
            get
            {
                T[] SmallArray = Array[Index / MaxSize];

                if (SmallArray == null)
                    return default(T);

                return SmallArray[Index % MaxSize];
            }
            set
            {
                T[] SmallArray = Array[Index / MaxSize];

                if (SmallArray == null)
                    SmallArray = Array[Index / MaxSize] = new T[MaxSize];

                SmallArray[Index % MaxSize] = value;
            }
        }
    }
}
