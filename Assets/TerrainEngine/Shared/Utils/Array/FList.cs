﻿using System;

[Serializable]
public class FList<T>
{
    public T[] array;
    public int Count;
    public int Lenght;
    public int DoubleSize;
	
	public FList() : this(10)
    {
		
    }

    public FList(int Capacity)
    {
        this.Count = 0;
        this.array = new T[Capacity];
        this.Lenght = Capacity;
        this.DoubleSize = 0;
    }

    public FList(int Capacity,int DoubleSize)
    {
        this.Count = 0;
        this.array = new T[Capacity];
        this.Lenght = Capacity;
        this.DoubleSize = DoubleSize;
    }

    public void Add(T value)
    {
        if (Lenght == Count)
        {
            Lenght = DoubleSize == 0 ? Count * 2 : Count + DoubleSize;
            T[] newArray = new T[Lenght];
            Array.Copy(array, 0, newArray, 0, Count);
            array = newArray;
        }

        array[Count++] = value;
    }

    public void AddSafe(T value)
    {
        array[Count++] = value;
    }

    public void AddSafe(T valueA, T valueB, T valueC, T valueD)
    {
        CheckArray(4);
        array[Count++] = valueA;
        array[Count++] = valueB;
        array[Count++] = valueC;
        array[Count++] = valueD;
    }

    public void AddRange(T[] values)
    {
        int Size = values.Length;
        if (Lenght - (Count + Size) <= 0)
        {
            T[] newArray = new T[Count * 2 + Size];
            Array.Copy(array, 0, newArray, 0, Count);
            array = newArray;
            Lenght = Count * 2 + Size;
        }

        Array.Copy(values, 0, array, Count, Size);
        Count += Size;
    }

    public void AddRange(SList<T> values)
    {
        int Size = values.Length;
        if (Lenght - (Count + Size) <= 0)
        {
            Lenght = Count * 2 + Size;
            T[] newArray = new T[Lenght];
            Array.Copy(array, 0, newArray, 0, Count);
            array = newArray;
        }

        Array.Copy(values.array, 0, array, Count, Size);
        Count += Size;
    }

    public void AddRange(FList<T> values)
    {
        int Size = values.Length;
        if (Lenght - (Count + Size) <= 0)
        {
            Lenght = Count * 2 + Size;
            T[] newArray = new T[Lenght];
            Array.Copy(array, 0, newArray, 0, Count);
            array = newArray;
        }

        Array.Copy(values.array, 0, array, Count, Size);
        Count += Size;
    }

    public bool Remove(T obj)
    {
        int Index = Array.IndexOf(array, obj, 0, Count);
        if (Index >= 0)
        {
            RemoveAt(Index);
            return true;
        }

        return false;
    }

    public void RemoveAt(int index)
    {
        if ((uint)index >= (uint)Count)
        {
            return;
        }

        --Count;
        if (index < Count)
        {
            Array.Copy(array, index + 1, array, index, Count - index);
        }

        array[Count] = default(T);
    }

    public bool Contains(T obj)
    {
        if (Array.IndexOf(array, obj, 0, Count) >= 0)
        {
            return true;
        }

        return false;
    }

    public void Set(T[] values)
    {
        array = values;
        Lenght = values.Length;
        Count = Lenght;
    }

    public void Clear()
    {
        if (Count > 0)
        {
            Array.Clear(array, 0, array.Length);
        }
        Count = 0;
    }

    /// <summary>
    /// Do not reset array, only to use if list do not contains any reference for GC
    /// </summary>
    public void ClearFast()
    {
        Count = 0;
    }

    public T[] ToArray()
    {
        if (Count == 0)
            return null;

        T[] New = new T[Count];
        Array.Copy(array, New, Count);
        return New;
    }

    public void CheckArray(int Size)
    {
        if (Lenght - (Count + Size) <= 0)
        {
            Lenght = DoubleSize == 0 ? (Count * 2 + Size) : (Count + DoubleSize + Size);
            T[] newArray = new T[Lenght];
            Array.Copy(array, 0, newArray, 0, Count);
            array = newArray;
        }
    }


    public int Length
    {
        get
        {
            return Count;
        }
    }

}