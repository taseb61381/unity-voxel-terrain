﻿using System.Collections.Generic;

namespace TerrainEngine
{
    /// <summary>
    /// Fast Threaded List. Objects are added in waiting list. All elements are pushed to the working list when Process() is call.
    /// </summary>
    public class TransitionQueue<T>
    {
        public List<T> Interdata;
        public Queue<T> List;
        public volatile int WaitingCount;
        public int Count
        {
            get
            {
                return List.Count;
            }
        }

        public TransitionQueue()
        {
            Interdata = new List<T>();
            List = new Queue<T>();
            WaitingCount = 0;
        }

        public void Process()
        {
            if (WaitingCount == 0)
                return;

            lock (Interdata)
            {
                for (int i = 0; i < Interdata.Count; ++i)
                {
                    List.Enqueue(Interdata[i]);
                }

                Interdata.Clear();
                WaitingCount = 0;
            }
        }

        public void AddUnsafe(T obj)
        {
            lock (Interdata)
            {
                Interdata.Add(obj);
                WaitingCount = Interdata.Count;
            }
        }

        public void AddRangeUnsafe(T[] objs)
        {
            lock (Interdata)
            {
                Interdata.AddRange(objs);
                WaitingCount = Interdata.Count;
            }
        }

        public void Clear()
        {
            List.Clear();
            Interdata.Clear();
            WaitingCount = 0;
        }
    }
}
