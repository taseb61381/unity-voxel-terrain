﻿
namespace TerrainEngine
{
    public class Fast2DArray<T>
    {
        public int SizeX, SizeY;
        public T[] array;

        public Fast2DArray(int SizeX, int SizeY)
        {
            this.SizeX = SizeX;
            this.SizeY = SizeY;
            this.array = new T[SizeX * SizeY];
        }

        public T this[int x, int y]
        {
            get
            {
                return array[y + x * SizeY];
            }
            set
            {
                array[y + x * SizeY] = value;
            }
        }
    }
}
