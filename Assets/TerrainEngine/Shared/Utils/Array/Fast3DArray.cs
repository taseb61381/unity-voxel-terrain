﻿
namespace TerrainEngine
{
    public class Fast3DArray<T>
    {
        public int SizeX, SizeY, SizeZ;
        public T[] Array;

        public Fast3DArray(int SizeX, int SizeY, int SizeZ)
        {
            this.SizeX = SizeX;
            this.SizeY = SizeY;
            this.SizeZ = SizeZ;
            this.Array = new T[SizeX * SizeY * SizeZ];
        }

        public T this[int x, int y, int z]
        {
            get
            {
                return Array[x * SizeY * SizeZ + y * SizeZ + z];
            }
            set
            {
                Array[x * SizeY * SizeZ + y * SizeZ + z] = value;
            }
        }
    }
}
