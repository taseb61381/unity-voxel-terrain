﻿using System.Collections.Generic;

namespace TerrainEngine
{
    public struct MemoryAllocatorData<T>
    {
        public int Position;
        public T[] Array;

        public T this[int Index]
        {
            get
            {
                return Array[Index+Position];
            }
            set
            {
                Array[Index + Position] = value;
            }
        }
    }

    public abstract class MemoryAllocator
    {
        static public List<MemoryAllocator> Alocators = new List<MemoryAllocator>();

        static public void CleanMemory()
        {
            foreach (MemoryAllocator Data in Alocators)
                Data.Clean();

            Alocators.Clear();
        }

        public MemoryAllocator()
        {
            lock (Alocators)
                Alocators.Add(this);
        }

        public virtual void Clean()
        {

        }
    }

    public class OptmizedMemoryAllocator<T> : MemoryAllocator
    {
        public PoolInfoRef PoolCount;
        public List<T[]> LargeArrays;
        public int Size;
        public int TotalSize;
        public int LastArrayId = 0;
        public int LastPosition = 0;
        public Stack<MemoryAllocatorData<T>> Pool;

        public OptmizedMemoryAllocator()
            : base()
        {
            LargeArrays = new List<T[]>(50);
            LargeArrays.Add(null);
            Pool = new Stack<MemoryAllocatorData<T>>();
        }

        public void SetSize(int Size, int GlobalArrayMultiplycator = 50)
        {
            if (GlobalArrayMultiplycator < 2)
                GlobalArrayMultiplycator = 2;

            this.Size = Size;
            this.TotalSize = Size * GlobalArrayMultiplycator;
        }

        public void GetData(ref MemoryAllocatorData<T> Data)
        {
            lock (Pool)
            {
                if (Pool.Count != 0)
                {
                    ++PoolCount.DequeueData;
                    Data = Pool.Pop();
                    return;
                }
            }

            lock (LargeArrays)
            {
                T[] array = null;

                if (LastArrayId <= LargeArrays.Count)
                    LargeArrays.Add(null);

                array = LargeArrays[LastArrayId];

                if (array == null)
                {
                    if (PoolCount.Index == 0)
                        PoolCount = PoolManager.Pool.GetPool(typeof(T).Name);

                    ++PoolCount.CreatedObject;
                    LargeArrays[LastArrayId] = array = new T[TotalSize];
                    LargeArrays.Add(null);
                }

                Data.Array = array;
                Data.Position = LastPosition;

                LastPosition += Size;
                if (LastPosition >= TotalSize)
                {
                    LastPosition = 0;
                    ++LastArrayId;
                }
            }
        }

        public void CloseData(MemoryAllocatorData<T> Data)
        {
            if (Data.Array == null)
                return;

            ++PoolCount.EnqueueData;

            lock (Pool)
            {
                Pool.Push(Data);
            }
        }

        public override void Clean()
        {
            LargeArrays.Clear();
            Pool.Clear();
        }
    }

}
