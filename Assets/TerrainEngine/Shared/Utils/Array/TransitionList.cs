﻿using System.Collections.Generic;

namespace TerrainEngine
{
    /// <summary>
    /// Fast Threaded List. Objects are added in waiting list. All elements are pushed to the working list when Process() is call.
    /// </summary>
    public class TransitionList<T>
    {
        public List<T> Interdata;
        public List<T> List;
        public volatile int WaitingCount;
        public int Count
        {
            get
            {
                return List.Count;
            }
        }

        public TransitionList()
        {
            Interdata = new List<T>();
            List = new List<T>();
            WaitingCount = 0;
        }

        public void Process()
        {
            if (WaitingCount == 0)
                return;

            lock (Interdata)
            {
                List.AddRange(Interdata);
                Interdata.Clear();
                WaitingCount = 0;
            }
        }

        public void AddUnsafe(T obj)
        {
            lock (Interdata)
            {
                Interdata.Add(obj);
                WaitingCount = Interdata.Count;
            }
        }

        public void AddRangeUnsafe(T[] objs)
        {
            lock (Interdata)
            {
                Interdata.AddRange(objs);
                WaitingCount = Interdata.Count;
            }
        }

        #region Safe

        public void AddSafe(T obj)
        {
            List.Add(obj);
        }

        public void AddRangeSafe(T[] obj)
        {
            List.AddRange(obj);
        }

        public bool Remove(T obj)
        {
            return List.Remove(obj);
        }

        public void Clear()
        {
            List.Clear();
            Interdata.Clear();
            WaitingCount = 0;
        }

        public T[] ToArray()
        {
            return List.ToArray();
        }

        #endregion
    }
}
