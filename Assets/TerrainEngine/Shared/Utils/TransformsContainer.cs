﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TerrainEngine
{
    /// <summary>
    /// This class is used to track multiple positions with index. Usefull for look over multiple cameras/objects
    /// Used for TerrainLOD/Grass/Details/GameObjects etc
    /// </summary>
    public class TransformsContainer
    {
        public delegate void GetPositionDelegate(int UID, Vector3 Position, Quaternion Rotation, float Distance);
        public delegate bool CheckPositionDelegate(int UID,Vector3 Position);

        [Serializable]
        public struct TransformIndex
        {
            /// <summary>
            /// Unique ID of this object
            /// </summary>
            public int UID;
            public Vector3 Position;
            public Quaternion Rotation;

            public Vector2 PositionXZ
            {
                get
                {
                    return new Vector2(Position.x, Position.z);
                }
            }

            public Vector3 LastPositionCheck;
        }

        /// <summary>
        /// Unique ID per object, this number is incremented after every new object
        /// </summary>
        public int LastUID = 0;
        public SList<TransformIndex> Transforms = new SList<TransformIndex>(1);

        /// <summary>
        /// Add a new Transform to the list with an Unique ID
        /// </summary>
        public int AddTransform(Vector3 Position,Quaternion Rotation)
        {
            int UID = ++LastUID;
            Transforms.Add(new TransformIndex() { UID = UID, Position = Position, Rotation = Rotation });
            return UID;
        }

        public bool IsEmpty()
        {
            return Transforms.Count == 0;
        }

        /// <summary>
        /// Update a transform with UID
        /// </summary>
        public void UpdateTransform(int UID, Vector3 Position, Quaternion Rotation)
        {
            for (int i = Transforms.Count - 1; i >= 0; --i)
            {
                if (Transforms.array[i].UID == UID)
                {
                    Transforms.array[i].Position = Position;
                    Transforms.array[i].Rotation = Rotation;
                    return;
                }
            }
        }

        /// <summary>
        /// Remove a transform from this container
        /// </summary>
        /// <param name="UID"></param>
        /// <returns></returns>
        public bool RemoveTransform(int UID)
        {
            for (int i = Transforms.Count - 1; i >= 0; --i)
            {
                if (Transforms.array[i].UID == UID)
                {
                    Transforms.RemoveAt(i);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Return array index from UID of an object
        /// </summary>
        public int GetTransformIndex(int UID)
        {
            for (int i = Transforms.Count - 1; i >= 0; --i)
                if (Transforms.array[i].UID == UID)
                    return i;

            return -1;
        }


        #region GetFunctions

        /// <summary>
        /// Return all transforms Added to this container
        /// </summary>
        /// <param name="del"></param>
        public void GetPositions(GetPositionDelegate del)
        {
            for (int i = Transforms.Count - 1; i >= 0; --i)
                del(Transforms.array[i].UID, Transforms.array[i].Position, Transforms.array[i].Rotation, 0);
        }

        /// <summary>
        /// Return closed transform from Position. UseXZ if distance must be calculated with Vector2 (X,Z) coords
        /// </summary>
        public void GetClosest(GetPositionDelegate del, Vector3 Position,bool UseXZ=false)
        {
            float MaxDistance = float.MaxValue;
            float Distance = 0;
            int Index = -1;

            if (!UseXZ)
            {
                for (int i = Transforms.Count - 1; i >= 0; --i)
                {
                    if ((Distance = Vector3.Distance(Transforms.array[i].Position, Position)) < MaxDistance)
                    {
                        Index = i;
                        MaxDistance = Distance;
                    }
                }

                if (Index >= 0)
                {
                    del(Transforms.array[Index].UID, Transforms.array[Index].Position, Transforms.array[Index].Rotation, MaxDistance);
                }
            }
            else
            {
                Vector2 PositionXZ = new Vector2(Position.x, Position.z);
                for (int i = Transforms.Count - 1; i >= 0; --i)
                {
                    if ((Distance = Vector2.Distance(Transforms.array[i].PositionXZ, PositionXZ)) < MaxDistance)
                    {
                        Index = i;
                        MaxDistance = Distance;
                    }
                }

                if (Index >= 0)
                {
                    del(Transforms.array[Index].UID, Transforms.array[Index].PositionXZ, Transforms.array[Index].Rotation, MaxDistance);
                }
            }

        }

        /// <summary>
        /// Return closest position
        /// </summary>
        public Vector3 GetClosest(Vector3 Position,bool UseXZ=false)
        {
            float MaxDistance = float.MaxValue;
            float Distance = 0;
            int Index = -1;

            if (!UseXZ)
            {
                for (int i = Transforms.Count - 1; i >= 0; --i)
                {
                    if ((Distance = Vector3.Distance(Transforms.array[i].Position, Position)) < MaxDistance)
                    {
                        Index = i;
                        MaxDistance = Distance;
                    }
                }

                if (Index >= 0)
                {
                    return Transforms.array[Index].Position;
                }

                return Vector3.zero;
            }
            else
            {
                Vector2 PositionXZ = new Vector2(Position.x, Position.z);
                for (int i = Transforms.Count - 1; i >= 0; --i)
                {
                    if ((Distance = Vector2.Distance(Transforms.array[i].PositionXZ, PositionXZ)) < MaxDistance)
                    {
                        Index = i;
                        MaxDistance = Distance;
                    }
                }

                if (Index >= 0)
                {
                    return Transforms.array[Index].PositionXZ;
                }

                return Vector3.zero;
            }
        }

        public bool GetAllPositions(CheckPositionDelegate del)
        {
            for(int i=0;i<Transforms.Count;++i)
            {
                if (del(Transforms.array[i].UID, Transforms.array[i].Position))
                    return true;
            }

            return false;
        }

        #endregion

        #region DistanceFunctions

        /// <summary>
        /// Calculate the distance from the last position update, and return true if > MaxDistance
        /// Used to call an event when objects move after every XX meters
        /// </summary>
        public bool HasPositionChanged(int UID, float MaxDistance)
        {
            int Index = GetTransformIndex(UID);
            if (Index == -1)
                return false;

            if (Vector3.Distance(Transforms.array[Index].Position, Transforms.array[Index].LastPositionCheck) > MaxDistance)
            {
                Transforms.array[Index].LastPositionCheck = Transforms.array[Index].Position;
                return true;
            }

            return false;
        }

        public bool HasPositionChanged(float MaxDistance)
        {
            for(int Index=Transforms.Count-1;Index>=0;--Index)
            {
                if (Vector3.Distance(Transforms.array[Index].Position, Transforms.array[Index].LastPositionCheck) > MaxDistance)
                {
                    Transforms.array[Index].LastPositionCheck = Transforms.array[Index].Position;
                    return true;
                }
            }

            return false;
        }

        #endregion

    }
}
