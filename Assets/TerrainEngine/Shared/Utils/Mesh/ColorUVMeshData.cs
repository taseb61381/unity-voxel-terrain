﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;


public class ColorUVMeshData : SimpleMeshData
{
    public Vector2[] CUVs;
    public Color[] CColors;

    public override int GetColorsCount(ref Color[] Colors)
    {
        Colors = this.CColors;

        if (Colors != null)
            return Colors.Length;

        return 0;
    }

    public override void SetColors(Color[] Colors)
    {
        this.CColors = Colors;
    }

    public override int GetUVCount(ref Vector2[] UVs)
    {
        UVs = this.CUVs;

        if (UVs != null)
            return UVs.Length;

        return 0;
    }

    public override void SetUVs(Vector2[] UVs)
    {
        this.CUVs = UVs;
    }

    public override Color[] GetColors()
    {
        return CColors;
    }

    public override Vector2[] GetUV()
    {
        return CUVs;
    }

    public override void Clear()
    {
        CUVs = null;
        CColors = null;
        base.Clear();
    }
}
