﻿using UnityEngine;

/*public class IMeshData : IPoolable
{
    public bool Combined;
    public bool EraseDatas;
    public int VerticesCount;
    public int IndiceCount;    
    public Vector2[] Uvs;
    public Vector3[] Vertices;
    public Vector3[] Normals;
    public Vector4[] Tangents;
    public Material[] Materials;
    public byte[] Properties;
    public Color[] Colors;
    public int[][] Indices;

    public IMeshData()
    {
        EraseDatas = true;
    }

    public int GetIndicesCount(ref SList<int> TotalIndices)
    {
        int Count = 0;
        if (Indices != null)
        {
            for (int i = 0; i < Indices.Length; ++i)
            {
                if (Indices[i] != null)
                {
                    Count += Indices[i].Length;
                    TotalIndices.AddRange(Indices[i]);
                }
            }
        }
        return Count;
    }

    public bool IsValid()
    {
        return Vertices != null && Vertices.Length > 0 && Indices != null && Indices.Length > 0;
    }

    public int GetVerticesCount()
    {
        if (Vertices != null)
            return Vertices.Length;

        return 0;
    }

    public override void Clear()
    {
        Uvs = null;
        Combined = false;

        if (EraseDatas)
        {
            Indices = null;
        }
        else if (Indices != null)
        {
            for (int i = 0; i < Indices.GetLength(0); ++i)
                Indices[i] = null;
        }

        Normals = null;
        Tangents = null;
        Colors = null;
        Materials = null;
        Vertices = null;
        VerticesCount = 0;
        IndiceCount = 0;
    }

    public override string ToString()
    {
        return "Mesh Data Vertices:" + (Vertices != null ? Vertices.Length : 0);
    }
}*/