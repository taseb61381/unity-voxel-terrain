﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PropertiesMeshDatas : CompleteMeshData
{
    public byte[] Properties;
    public bool EraseDatas = false;

    public override bool IsValid()
    {
        return base.IsValid();
    }

    public override void Clear()
    {
        Combined = false;
        EraseDatas = false;
        VerticesCount = 0;
        IndiceCount = 0;
        base.Clear();
    }
}
