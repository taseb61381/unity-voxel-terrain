﻿using UnityEngine;
using System.Collections;

public class CompleteMeshData : ColorUVMeshData
{
    public new Vector4[] CTangents;
    public new int VerticesCount;

    public override int GetTangentsCount(ref Vector4[] Tangents)
    {
        Tangents = this.CTangents;

        if (Tangents != null)
            return Tangents.Length;

        return 0;
    }


    public override void SetTangents(Vector4[] Tangents)
    {
        this.CTangents = Tangents;
    }


    public override Vector4[] GetTangents()
    {
        return CTangents;
    }

    public override void Clear()
    {
        VerticesCount = 0;
        CTangents = null;
        base.Clear();
    }
}
