﻿using UnityEngine;
using System.Collections;

public abstract class IMeshData : IPoolable
{
    public Vector3[] Vertices
    {
        get
        {
            return GetVertices();
        }
        set
        {
            SetVertices(value);
        }
    }
    public int[][] Indices
    {
        get
        {
            return GetIndices();
        }
        set
        {
            SetIndices(value);
        }
    }
    public Vector3[] Normals
    {
        get
        {
            return GetNormals();
        }
        set
        {
            SetNormals(value);
        }
    }
    public Vector2[] UVs
    {
        get
        {
            return GetUV();
        }
        set
        {
            SetUVs(value);
        }
    }
    public Vector4[] Tangents
    {
        get
        {
            return GetTangents();
        }
        set
        {
            SetTangents(value);
        }
    }
    public Color[] Colors
    {
        get
        {
            return GetColors();
        }
        set
        {
            SetColors(value);
        }
    }
    public Material[] Materials
    {
        get
        {
            return GetMaterials();
        }
        set
        {
            SetMaterials(value);
        }
    }

    public UnityEngine.Rendering.ShadowCastingMode CastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
    public bool ReceiveShadows = false;
    public bool GenerateUVS = false;
    public bool GenerateTangents = false;
    public bool Combined = false;
    public int VerticesCount = 0;
    public int IndiceCount = 0;

    public virtual int GetIndicesCount(ref SList<int> TotalIndices)
    {
        TotalIndices.Clear();
        return 0;
    }

    public abstract bool IsValid();

    public virtual int GetIndicesCount(ref int[][] Indices)
    {
        return 0;
    }

    public virtual int[][] GetIndices()
    {
        return null;
    }

    public virtual int GetVerticesCount(ref Vector3[] Vertices)
    {
        return 0;
    }

    public virtual Vector3[] GetVertices()
    {
        return null;
    }

    public virtual int GetVerticesCount()
    {
        return 0;
    }

    public virtual int GetNormalsCount(ref Vector3[] Normals)
    {
        return 0;
    }

    public virtual Vector3[] GetNormals()
    {
        return null;
    }

    public virtual int GetUVCount(ref Vector2[] UVs)
    {
        return 0;
    }

    public virtual Vector2[] GetUV()
    {
        return null;
    }

    public virtual int GetColorsCount(ref Color[] Colors)
    {
        return 0;
    }

    public virtual Color[] GetColors()
    {
        return null;
    }

    public virtual int GetTangentsCount(ref Vector4[] Tangents)
    {
        return 0;
    }

    public virtual Vector4[] GetTangents()
    {
        return null;
    }

    public virtual int GetMaterialsCount(ref Material[] Materials)
    {
        return 0;
    }

    public virtual Material[] GetMaterials()
    {
        return null;
    }

    public virtual int GetPropertiesCount(ref int[][] Properties)
    {
        return 0;
    }

    public virtual byte[] GetProperies()
    {
        return null;
    }

    public virtual void SetVertices(Vector3[] Vertices)
    {

    }

    public virtual void SetIndices(int[][] Indices)
    {

    }

    public virtual void SetNormals(Vector3[] Normals)
    {

    }

    public virtual void SetUVs(Vector2[] UVs)
    {

    }

    public virtual void SetTangents(Vector4[] Tangents)
    {

    }

    public virtual void SetColors(Color[] Colors)
    {

    }

    public virtual void SetMaterials(Material[] Materials)
    {

    }

    public virtual void Apply(Mesh CMesh)
    {
        CMesh.Clear(false);

        if (!IsValid())
            return;

        Vector3[] Vertices = null;
        Vector2[] UVs = null;
        Vector3[] Normals = null;
        Vector4[] Tangents = null;
        Material[] Materials = null;
        Color[] Colors = null;
        int[][] Indices = null;

        GetVerticesCount(ref Vertices);
        GetUVCount(ref UVs);
        GetNormalsCount(ref Normals);
        GetTangentsCount(ref Tangents);
        GetMaterialsCount(ref Materials);
        GetColorsCount(ref Colors);
        GetIndicesCount(ref Indices);

        CMesh.vertices = Vertices;
        CMesh.subMeshCount = Indices.Length;
        for (int i = 0; i < Indices.Length; ++i)
        {
            CMesh.SetIndices(Indices[i], MeshTopology.Triangles, i);
        }

        if (Normals != null && Normals.Length > 0)
        {
            CMesh.normals = Normals;
        }
        else
            CMesh.RecalculateNormals();

        if (Colors != null)
            CMesh.colors = Colors;

        if (UVs != null && UVs.Length > 0)
        {
            CMesh.uv = UVs;
        }
        else if (GenerateUVS)
        {
            UVs = new Vector2[Vertices.Length];
            Vector2 uv = new Vector2();
            Vector3 v = new Vector3();

            for (int i = Vertices.Length - 1; i >= 0; --i)
            {
                v = Vertices[i];
                uv.x = v.x + v.y;
                uv.y = v.y + v.z;
                UVs[i] = uv;
            }

            CMesh.uv = UVs;
        }

        if (Tangents != null)
        {
            CMesh.tangents = Tangents;
        }
        else if (GenerateTangents)
        {
            if (UVs != null)
                MeshUtils.calculateMeshTangents(CMesh, this, Vertices, Normals, UVs);
            else
                MeshUtils.calculateMeshTangentsWithoutUV(CMesh, this, Vertices, Normals);
        }

    }

    public override void Clear()
    {
        CastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        ReceiveShadows = false;
        GenerateUVS = false;
        GenerateTangents = false;
        Combined = false;
        VerticesCount = IndiceCount = 0;
    }

    static public IMeshData Create(bool UseUVS,bool UseTangents,bool UseColors)
    {
        if (UseUVS || UseTangents || UseColors)
            return new CompleteMeshData();

        return new SimpleMeshData();
    }
}
