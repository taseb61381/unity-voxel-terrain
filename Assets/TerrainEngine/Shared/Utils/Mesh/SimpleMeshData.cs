﻿using UnityEngine;
using System.Collections;

public class SimpleMeshData : IMeshData
{
    public int[][] CIndices;
    public Vector3[] CVertices;
    public Vector3[] CNormals;
    public Material[] CMaterials;

    public override int GetIndicesCount(ref SList<int> TotalIndices)
    {
        int Count = 0;
        if (CIndices != null)
        {
            for (int i = 0; i < CIndices.Length; ++i)
            {
                if (CIndices[i] != null)
                {
                    Count += CIndices[i].Length;
                    TotalIndices.AddRange(CIndices[i]);
                }
            }
        }
        return Count;
    }

    public override int GetIndicesCount(ref int[][] Indices)
    {
        Indices = this.CIndices;

        if (Indices != null)
            return Indices.Length;

        return 0;
    }

    public override int[][] GetIndices()
    {
        return CIndices;
    }

    public override void SetIndices(int[][] Indices)
    {
        this.CIndices = Indices;
    }


    public override int GetVerticesCount(ref Vector3[] Vertices)
    {
        Vertices = this.CVertices;

        if (Vertices != null)
            return Vertices.Length;

        return 0;
    }

    public override int GetVerticesCount()
    {
        return CVertices != null ? CVertices.Length : 0;
    }

    public override Vector3[] GetVertices()
    {
        return CVertices;
    }

    public override void SetVertices(Vector3[] Vertices)
    {
        this.CVertices = Vertices;
    }

    public override int GetMaterialsCount(ref Material[] Materials)
    {
        Materials = this.CMaterials;

        if (Materials != null)
            return Materials.Length;

        return 0;
    }

    public override Material[] GetMaterials()
    {
        return CMaterials;
    }

    public override void SetMaterials(Material[] Materials)
    {
        this.CMaterials = Materials;
    }


    public override int GetNormalsCount(ref Vector3[] Normals)
    {
        Normals = this.CNormals;

        if (Normals != null)
            return Normals.Length;

        return 0;
    }

    public override Vector3[] GetNormals()
    {
        return CNormals;
    }

    public override void SetNormals(Vector3[] Normals)
    {
        this.CNormals = Normals;
    }


    public override bool IsValid()
    {
        return CVertices != null && CVertices.Length > 0 && CIndices != null && CIndices.Length > 0;
    }

    public override void Clear()
    {
        CIndices = null;
        CNormals = null;
        CVertices = null;
        CMaterials = null;
        base.Clear();
    }

    public override string ToString()
    {
        return "Vertices:" + (CVertices != null ? CVertices.Length : 0) + ",Valid:" + IsValid() + ",Materials:" + (Materials != null ? Materials.Length : 0) + "CIndices:" + (CIndices != null ? CIndices.Length : 0);
    }
}
