﻿using System;

using System.IO;
using System.Xml.Serialization;

namespace TerrainEngine
{
    static public class XmlMgr
    {
        static public bool CheckFolders(string Path)
        {
            if (Path == null || Path.Length <= 0)
                return true;

            if (!Directory.Exists(Path))
            {
                Console.WriteLine("[Loading]", "Creating Directory : " + Path);
                Directory.CreateDirectory(Path);
                return true;
            }

            return false;
        }

        static public bool ExistFile(string Path)
        {
            return File.Exists(Path);
        }

        static public T GetXMLFile<T>(string path)
        {
            XmlSerializer Serialized = new XmlSerializer(typeof(T));
            object o = null;

            try
            {
                using (FileStream Stream = File.OpenRead(path))
                {
                    if (GeneralLog.DrawLogs)
                        GeneralLog.Log("[Loading] : Loaded XML : " + path);
                    o = Serialized.Deserialize(Stream);
                    Stream.Close();
                }
            }
            catch (Exception e)
            {
                if (GeneralLog.DrawLogs)
                    GeneralLog.Log("XML Not Found : " + path + "\n" + e.ToString());

                return (T)Convert.ChangeType(null, typeof(T));
            }

            return (T)o;
        }

        static public T GetXMLFile<T>(Type type, string path)
        {
            XmlSerializer Serialized = new XmlSerializer(type);
            object o = null;

            try
            {
                using (FileStream Stream = File.OpenRead(path))
                {
                    if (GeneralLog.DrawLogs)
                        GeneralLog.Log("[Loading] : Loaded XML : " + path);

                    o = Serialized.Deserialize(Stream);
                    Stream.Close();
                }
            }
            catch (Exception e)
            {
                if (GeneralLog.DrawLogs)
                    GeneralLog.Log("XML Not Found : " + path + "\n" + e.ToString());

                return (T)Convert.ChangeType(null, type);
            }

            return (T)o;
        }

        static public bool SaveXMLFile(string path, object obj)
        {
            if (GeneralLog.DrawLogs)
                GeneralLog.Log("[XmlMgr] SaveXML : " + path);

            try
            {
                CheckFolders(Path.GetDirectoryName(path));

                using (FileStream Stream = File.Create(path))
                {
                    SaveXMLStream(Stream, obj);

                    if (GeneralLog.DrawLogs)
                        GeneralLog.Log("[XmlMgr] Created XML : " + path);
                }
            }
            catch (Exception e)
            {
                GeneralLog.Log("[XmlMgr] Error : " + path + "\n" + e.ToString());
                return false;
            }

            return true;
        }

        static public void SaveXMLStream(Stream stream, object obj)
        {
            XmlSerializer Serialized = new XmlSerializer(obj.GetType());
            Serialized.Serialize(stream, obj);
            stream.Close();
        }

        static public T GetXML<T>(Stream stream)
        {
            try
            {
                XmlSerializer Serialized = new XmlSerializer(typeof(T));
                object o = null;

                o = Serialized.Deserialize(stream);
                stream.Close();
                return (T)o;
            }
            catch (Exception e)
            {
                if (GeneralLog.DrawLogs)
                    GeneralLog.Log("XML Error :" + e.ToString());

                return (T)Convert.ChangeType(null, typeof(T));
            }
        }
    }
}
