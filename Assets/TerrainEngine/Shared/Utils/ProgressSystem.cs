﻿using UnityEngine;

namespace TerrainEngine
{
    public struct ProgressSystem
    {
        static public ProgressSystem Instance = new ProgressSystem();

        public struct ProgressInfo
        {
            public ProgressInfo(string Name, float Pct,float StartTime)
            {
                this.Name = Name;
                this.Pct = Pct;
                this.StartTime = StartTime;
            }

            public string Name;
            public float Pct;
            public float StartTime;
        }

        public string Name;
        public SList<ProgressInfo> ProgressObjects;
        private float LastProgress;
        private bool Dirty;

        public void SetProgress(string Name, float Pct,float Time=0)
        {
            if (Name == null)
                return;

            if (ProgressObjects.array == null)
                ProgressObjects = new SList<ProgressInfo>(50);

            lock (ProgressObjects.array)
            {
                for (int i = ProgressObjects.Count - 1; i >= 0; --i)
                {
                    if (ProgressObjects.array[i].Name == Name)
                    {
                        ProgressObjects.array[i].Pct = Pct;
                        Dirty = true;
                        return;
                    }
                }

                ProgressObjects.Add(new ProgressInfo(Name, Pct, Time));
            }

            Dirty = true;
        }

        /// <summary>
        /// Return total progress, if 100% clear the list
        /// </summary>
        /// <returns></returns>
        public float GetProgress(float Time=0)
        {
            if (ProgressObjects.array == null || ProgressObjects.Count == 0)
                return 100f;

            float TotalProgress = 0;

            if (Dirty)
            {
                Dirty = false;

                lock (ProgressObjects.array)
                {
                    for (int i = ProgressObjects.Count - 1; i >= 0; --i)
                    {
                        if (Time - ProgressObjects.array[i].StartTime > 60)
                            ProgressObjects.array[i].Pct = 100f;

                        TotalProgress += ProgressObjects.array[i].Pct;
                    }

                    TotalProgress /= ProgressObjects.Count;

                    if (TotalProgress >= 99.9f)
                        ProgressObjects.ClearFast();

                    LastProgress = TotalProgress;
                }
            }

            return LastProgress;
        }

        /// <summary>
        /// Return total progress, if 100% clear the list
        /// </summary>
        /// <returns></returns>
        public void GetProgress(ref float progress, ref int objects)
        {
            if (ProgressObjects.array == null || ProgressObjects.Count == 0)
            {
                progress = 100f;
                objects = 0;
                return;
            }

            GetProgress();

            progress = LastProgress;
            objects = ProgressObjects.Count;
        }

        public void Clear()
        {
            if (ProgressObjects.array != null && ProgressObjects.Count != 0)
            {
                lock (ProgressObjects.array)
                    ProgressObjects.ClearFast();
            }
        }

        public void DrawCircle(Rect Position, Texture2D Circle, float Speed)
        {
            float Progress = GetProgress();

            if (Progress != 100f)
            {
                Vector2 Pivot = new Vector2(Position.xMin + Position.width * 0.5f, Position.yMin + Position.height * 0.5f);

                Matrix4x4 matrixBackup = GUI.matrix;
                GUIUtility.RotateAroundPivot(Time.time * Speed, Pivot);
                GUI.DrawTexture(Position, Circle);
                GUI.matrix = matrixBackup;
            }
        }

        Vector2 Scroll;
        public void DrawList(Rect Position)
        {
            if (ProgressObjects.array == null)
                return;

            lock (ProgressObjects.array)
            {
                int Count = 0;
                int i;
                for (i = 0; i < ProgressObjects.Count; ++i)
                {
                    if (ProgressObjects.array[i].Pct < 100)
                    {
                        ++Count;
                    }
                }

                float TotalHeight = Count * 20;
                if (TotalHeight < Position.height)
                {
                    Position.y = Position.y + (Position.height - TotalHeight);
                    Position.height = TotalHeight;
                }

                Rect P = new Rect(5, Position.y + 5, Position.width - 10, 20);
                GUI.Box(Position, "");

                for (i = 0; i < ProgressObjects.Count; ++i)
                {
                    if (ProgressObjects.array[i].Pct < 100)
                    {
                        GUI.Label(P, ProgressObjects.array[i].Name + " " + ProgressObjects.array[i].Pct.ToString("000") + "%");
                        P.y += 20;
                    }
                }
            }
        }
    }
}
