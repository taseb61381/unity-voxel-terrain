﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class bezierCurveStatic
{
    #region initialize

    // return list of all point in bezierCurve
    // you have to refresh when you modifie a pts
    public static void newCurve(int precision, Vector3 pts0, Vector3 pts1, Vector3 pts2, List<Vector3> baryList, List<Vector3> ptsPosition)
    {
        baryList.Clear();
        ptsPosition.Clear();
        ptsPosition.Add(pts0);
        ptsPosition.Add(pts1);
        ptsPosition.Add(pts2);
        barycenter2(ptsPosition, baryList, precision);
    }

    public static void newCurve(int precision, Vector3 pts0, Vector3 pts1, Vector3 pts2, Vector3 pts3, List<Vector3> baryList, List<Vector3> ptsPosition)
    {
        baryList.Clear();
        ptsPosition.Clear();
        ptsPosition.Add(pts0);
        ptsPosition.Add(pts1);
        ptsPosition.Add(pts2);
        ptsPosition.Add(pts3);
        barycenter3(ptsPosition, baryList, precision);
    }

    public static void newCurve(int precision, List<Vector3> ptsPosition, List<Vector3> baryList)
    {
        baryList.Clear();
        if (ptsPosition.Count == 3)
            barycenter2(ptsPosition, baryList, precision);
        else if (ptsPosition.Count == 4)
            barycenter3(ptsPosition, baryList, precision);
        else
            baryList.Clear();
    }

    #endregion

    #region barycenters

    private static void barycenter2(List<Vector3> ptsPosition, List<Vector3> baryList, int precision)
    {
        float t = 1f / precision;
        baryList.Add(ptsPosition[0]);
        Vector3 p = new Vector3();
        for (int i = 1; i < precision; i++)
        {
            p.x = ((1 - t) * (1 - t) * ptsPosition[0].x) + (2 * t * (1 - t) * ptsPosition[1].x) + (t * t * ptsPosition[2].x);
            p.y = ((1 - t) * (1 - t) * ptsPosition[0].y) + (2 * t * (1 - t) * ptsPosition[1].y) + (t * t * ptsPosition[2].y);
            p.z = ((1 - t) * (1 - t) * ptsPosition[0].z) + (2 * t * (1 - t) * ptsPosition[1].z) + (t * t * ptsPosition[2].z);
            baryList.Add(p);
            t += 1f / precision;
        }
        baryList.Add(ptsPosition[2]);
    }

    public static void barycenter2(Vector3 A,Vector3 B,Vector3 C, List<Vector3> baryList, int precision)
    {
        float t = 1f / precision;
        baryList.Add(A);
        Vector3 p = new Vector3();
        for (int i = 1; i < precision; i++)
        {
            p.x = ((1 - t) * (1 - t) * A.x) + (2 * t * (1 - t) * B.x) + (t * t * C.x);
            p.y = ((1 - t) * (1 - t) * A.y) + (2 * t * (1 - t) * B.y) + (t * t * C.y);
            p.z = ((1 - t) * (1 - t) * A.z) + (2 * t * (1 - t) * B.z) + (t * t * C.z);
            baryList.Add(p);
            t += 1f / precision;
        }
        baryList.Add(C);
    }

    private static void barycenter3(List<Vector3> ptsPosition, List<Vector3> baryList, float precision)
    {
        if (precision <= 0)
            precision = 1;

        float t = 1f / precision;
        Vector3 p = new Vector3();
        baryList.Add(ptsPosition[0]);

        while(t < 1f)
        {
            p.x = (1 - t) * (1 - t) * (1 - t) * ptsPosition[0].x +
                ((1 - t) * (1 - t)) * 3 * t * ptsPosition[1].x +
                (3 * t * t * (1 - t) * ptsPosition[2].x) +
                (t * t * t * ptsPosition[3].x);

            p.y = (1 - t) * (1 - t) * (1 - t) * ptsPosition[0].y +
                ((1 - t) * (1 - t)) * 3 * t * ptsPosition[1].y +
                (3 * t * t * (1 - t) * ptsPosition[2].y) +
                (t * t * t * ptsPosition[3].y);

            p.z = (1 - t) * (1 - t) * (1 - t) * ptsPosition[0].z +
                ((1 - t) * (1 - t)) * 3 * t * ptsPosition[1].z +
                (3 * t * t * (1 - t) * ptsPosition[2].z) +
                (t * t * t * ptsPosition[3].z);

            baryList.Add(p);
            t += 1f / precision;
        }
        baryList.Add(ptsPosition[3]);
    }

    public static void barycenter3(Vector3 A,Vector3 B,Vector3 C,Vector3 D, List<Vector3> baryList, float precision)
    {
        if (precision <= 0)
            precision = 1;

        float t = 1f / precision;
        Vector3 p = new Vector3();
        baryList.Add(A);

        while (t < 1f)
        {
            p.x = (1 - t) * (1 - t) * (1 - t) * A.x +
                ((1 - t) * (1 - t)) * 3 * t * B.x +
                (3 * t * t * (1 - t) * C.x) +
                (t * t * t * D.x);

            p.y = (1 - t) * (1 - t) * (1 - t) * A.y +
                ((1 - t) * (1 - t)) * 3 * t * B.y +
                (3 * t * t * (1 - t) * C.y) +
                (t * t * t * D.y);

            p.z = (1 - t) * (1 - t) * (1 - t) * A.z +
                ((1 - t) * (1 - t)) * 3 * t * B.z +
                (3 * t * t * (1 - t) * C.z) +
                (t * t * t * D.z);

            baryList.Add(p);
            t += 1f / precision;
        }
        baryList.Add(D);
    }

    #endregion

    #region Public potential_utilities

    //return a float curve's lenght
    public static float curveLength(List<Vector3> baryList, int precision)
    {
        float res = 0.0f;
        for (int i = 0; i < precision; i++)
        {
            res += segmentLength(baryList[i], baryList[i + 1]);
        }
        return res;
    }

    //return distance between 2 pts
    public static float segmentLength(Vector3 A, Vector3 B)
    {
        return Mathf.Sqrt((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y) + (B.z - A.z) * (B.z - A.z));
    }

    //replace thing in context by the thing you want to create
    public static void doSomethingAllDistance(float distance, List<Vector3> baryList, int precision)
    {
        float res = 0.0f;
        float segProportion, X, Y, Z;
        for (int i = 0; i < precision; i++)
        {
            res += segmentLength(baryList[i], baryList[i + 1]);
            if (res >= distance)
            {
                res -= distance;
                segProportion = 1f - res / segmentLength(baryList[i], baryList[i + 1]);
                X = baryList[i].x + (segProportion * (baryList[i + 1].x - baryList[i].x));
                Y = baryList[i].y + (segProportion * (baryList[i + 1].y - baryList[i].y));
                Z = baryList[i].z + (segProportion * (baryList[i + 1].z - baryList[i].z));

                // do something on coor X,Y and Z
                //createPoint(X, Y, Z);

            }
        }
    }

    //create a cube to test others functions
    public static void createPoint(float X, float Y, float Z)
    {
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
        go.name = "point";
        go.transform.position = new Vector3(X, Y, Z);
        go.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

    }

    //boolean function to know if a curve cross an other curve
    public static bool curveCrossed(List<Vector3> curve1, List<Vector3> curve2, float maxDistance)
    {
        int i, j;
        for(i=0; i < curve1.Count; i++)
        {
            for(j=0; j < curve2.Count; j++)
            {
                if (maxDistance <= segmentLength(curve1[i], curve2[j]))
                {
                    return true;
                }
            }
        }
        return false;
    }

    #endregion

    public static void thomas(List<Vector3> ptsPosition, List<Vector3> barylist, int distance)
    {
        barylist.Clear();
        float res, precRes;
        int precision = 30;
        bool done = false;
        bool avancer = true;
        precRes = 10000000f;
        barycenter3(ptsPosition, barylist, precision);
        res = distance - segmentLength(barylist[0], barylist[1]);
        while(!done)
        {
            if (res > 0 ){
                barycenter3(ptsPosition, barylist, precision-1);
                res = distance - segmentLength(barylist[0], barylist[1]);
                avancer = true;
            }
            else if(res < 0){
                barycenter3(ptsPosition, barylist, precision+1);
                res = distance - segmentLength(barylist[0], barylist[1]);
                avancer = false;
            }
            if (precRes < res && avancer)
            {
                barycenter3(ptsPosition, barylist, precision + 1);
                done = true;
            }
            else if (precRes < res && !avancer)
            {
                barycenter3(ptsPosition, barylist, precision - 1);
                done = true;
            }
            precRes = res;
        }

    }

    public static void newCurveSplit(List<Vector3> ptsPosition, List<Vector3> barylist, float Distance)
    {
        if (Distance < 0.1f)
            Distance = 0.1f;

        float step = 0.1f;
        float t = step;
        float Lastt = step;
        Vector3 p = new Vector3();
        barylist.Add(ptsPosition[0]);
        float CurrentDistance = 0;
        Vector3 LastPosition = ptsPosition[0];
        int Count = 5;

        while (t < 1f)
        {
            p.x = (1 - t) * (1 - t) * (1 - t) * ptsPosition[0].x +
                ((1 - t) * (1 - t)) * 3 * t * ptsPosition[1].x +
                (3 * t * t * (1 - t) * ptsPosition[2].x) +
                (t * t * t * ptsPosition[3].x);

            p.y = (1 - t) * (1 - t) * (1 - t) * ptsPosition[0].y +
                ((1 - t) * (1 - t)) * 3 * t * ptsPosition[1].y +
                (3 * t * t * (1 - t) * ptsPosition[2].y) +
                (t * t * t * ptsPosition[3].y);

            p.z = (1 - t) * (1 - t) * (1 - t) * ptsPosition[0].z +
                ((1 - t) * (1 - t)) * 3 * t * ptsPosition[1].z +
                (3 * t * t * (1 - t) * ptsPosition[2].z) +
                (t * t * t * ptsPosition[3].z);

            CurrentDistance = Vector3.Distance(p, LastPosition);

            if (Count != 0 && (CurrentDistance > Distance + Distance * 0.7f || CurrentDistance < Distance - Distance * 0.7f))
            {
                --Count;
                step /= CurrentDistance / Distance;
                t = Lastt + step;
                continue;
            }
            else
                step = 0.05f;

            Count = 5;
            LastPosition = p;
            barylist.Add(p);
            Lastt = t;
            t += step;
        }

        if (Vector3.Distance(ptsPosition[3], barylist[barylist.Count - 1]) < Distance)
            barylist[barylist.Count - 1] = ptsPosition[3];
        else
            barylist.Add(ptsPosition[3]);
    }

    public static void newCurveSplit(Vector3 A,Vector3 B,Vector3 C,Vector3 D, List<Vector3> barylist, float Distance)
    {
        if (Distance < 0.1f)
            Distance = 0.1f;

        float step = 0.1f;
        float t = step;
        float Lastt = step;
        Vector3 p = new Vector3();
        barylist.Add(A);
        float CurrentDistance = 0;
        Vector3 LastPosition = A;
        int Count = 5;

        while (t < 1f)
        {
            p.x = (1 - t) * (1 - t) * (1 - t) * A.x +
                ((1 - t) * (1 - t)) * 3 * t * B.x +
                (3 * t * t * (1 - t) * C.x) +
                (t * t * t * D.x);

            p.y = (1 - t) * (1 - t) * (1 - t) * A.y +
                ((1 - t) * (1 - t)) * 3 * t * B.y +
                (3 * t * t * (1 - t) * C.y) +
                (t * t * t * D.y);

            p.z = (1 - t) * (1 - t) * (1 - t) * A.z +
                ((1 - t) * (1 - t)) * 3 * t * B.z +
                (3 * t * t * (1 - t) * C.z) +
                (t * t * t * D.z);

            CurrentDistance = Vector3.Distance(p, LastPosition);

            if (Count != 0 && (CurrentDistance > Distance + Distance * 0.7f || CurrentDistance < Distance - Distance * 0.7f))
            {
                --Count;
                step /= CurrentDistance / Distance;
                t = Lastt + step;
                continue;
            }
            else
                step = 0.05f;

            Count = 5;
            LastPosition = p;
            barylist.Add(p);
            Lastt = t;
            t += step;
        }

        if (Vector3.Distance(D, barylist[barylist.Count - 1]) < Distance)
            barylist[barylist.Count - 1] = D;
        else
            barylist.Add(D);
    }
}
