using System;
using System.Xml.Serialization;
using UnityEngine;

/// <summary>
/// Random class. Generate Random (Scale/Positon/Etc..)
/// </summary>
[Serializable]
public class RandomInfo
{
    public bool Enabled;
    public bool UseXOnly = false;
    public Vector3 Offset;
    public Vector3 Min;
    public Vector3 Max;

    [XmlIgnore,HideInInspector]
    public Vector3 Range;

    public void Init()
    {
        Range = Max - Min;
    }

    public void CopyFrom(RandomInfo Other)
    {
        this.Enabled = Other.Enabled;
        this.UseXOnly = Other.UseXOnly;
        this.Min = Other.Min;
        this.Max = Other.Max;
    }

    public float RandomF(FastRandom Random)
    {
        if (!Enabled)
            return Min.x;

        return Range.x * Random.randomPosFloat() + Min.x + Offset.x;
    }

    public float RandomNoise(double x, double z)
    {
        if (!Enabled)
            return Min.x;

        x = TerrainEngine.SimplexNoise.Noise2(x * 0.01, z * 0.01); // -1 to 1
        x += 1f; // 0 to 2
        x *= 0.5f; // 0 to 1

        return Range.x * (float)x + Min.x + Offset.x;
    }

    public Vector3 RandomNoise3(double x, double z)
    {
        if (!Enabled)
            return Min;

        x = TerrainEngine.SimplexNoise.Noise2(x * 0.01, z * 0.01); // -1 to 1
        x += 1f; // 0 to 2
        x *= 0.5f; // 0 to 1

        return Range * (float)x + Min + Offset;
    }

    public float RandomF(ref SFastRandom Random)
    {
        if (!Enabled)
            return Min.x;

        return Range.x * Random.randomPosFloat() + Min.x + Offset.x;
    }

    public float RandomF(float Random)
    {
        if (!Enabled)
            return Min.x;

        return Range.x * Random + Min.x + Offset.x;
    }

    public Vector3 Random(FastRandom Random)
    {
        if (!Enabled)
            return Min;

        float x = Range.x * Random.randomPosFloat();
        if (!UseXOnly)
        {
            return new Vector3(x + Min.x, (Range.y != 0 ? Range.y * Random.randomPosFloat() : 0) + Min.y, (Range.z != 0 ? Range.z * Random.randomPosFloat() : 0) + Min.z);
        }
        else
            return new Vector3(x + Min.x, x + Min.x, x + Min.x);
    }

    public Vector3 Random(ref SFastRandom Random)
    {
        if (!Enabled)
            return Min;

        float x = Range.x * Random.randomPosFloat();
        if (!UseXOnly)
        {
            return new Vector3(x + Min.x, (Range.y != 0 ? Range.y * Random.randomPosFloat() : 0) + Min.y, (Range.z != 0 ? Range.z * Random.randomPosFloat() : 0) + Min.z);
        }
        else
            return new Vector3(x + Min.x, x + Min.x, x + Min.x);
    }

    public Vector3 Random(float Random)
    {
        if (!Enabled)
            return Min;

        float x = Range.x * Random;
        if (!UseXOnly)
        {
            return new Vector3(x + Min.x, (Range.y != 0 ? Range.y * Random : 0) + Min.y, (Range.z != 0 ? Range.z * Random : 0) + Min.z);
        }
        else
            return new Vector3(x + Min.x, x + Min.x, x + Min.x);
    }

    public float RandomY(float Random)
    {
        if (!Enabled)
            return Min.y;

        return Range.y * Random + Min.y;
    }

    public void Random(ref SFastRandom Random, ref Vector3 Vector)
    {
        Vector.x = Min.x;
        Vector.y = Min.y;
        Vector.z = Min.z;

        if (!Enabled)
        {
            return;
        }

        float x = Range.x * Random.randomPosFloat();
        if (!UseXOnly)
        {
            Vector.x += x;
            Vector.y += Range.y * Random.randomPosFloat();
            Vector.z +=  Range.z * Random.randomPosFloat();
        }
        else
        {
            Vector.x += x;
            Vector.y += x;
            Vector.z += x;
        }
    }
}