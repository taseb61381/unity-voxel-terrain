﻿using System;
using UnityEngine;

public static class MathHelper
{
    public static float RAD_TO_DEG = (float)(180.0f / Mathf.PI);
    public static float DEG_TO_RAD = (float)(Mathf.PI / 180.0f);

    public static int fastAbs(int i)
    {
        return (i >= 0) ? i : -i;
    }

    public static float fastAbs(float d)
    {
        return (d >= 0) ? d : -d;
    }

    public static double fastAbs(double d)
    {
        return (d >= 0) ? d : -d;
    }

    public static double fastFloor(double d)
    {
        int i = (int)d;
        return (d < 0 && d != i) ? i - 1 : i;
    }

    public static float fastFloor(float d)
    {
        int i = (int)d;
        return (d < 0 && d != i) ? i - 1 : i;
    }

    public static float clamp(float value)
    {
        if (value > 1.0f)
            return 1.0f;
        if (value < 0.0f)
            return 0.0f;
        return value;
    }

    public static double clamp(double value, double min, double max)
    {
        if (value > max)
            return max;
        if (value < min)
            return min;
        return value;
    }

    public static float clamp(float value, float min, float max)
    {
        if (value > max)
            return max;
        if (value < min)
            return min;
        return value;
    }

    public static int clamp(int value, int min, int max)
    {
        if (value > max)
            return max;
        if (value < min)
            return min;
        return value;
    }

    public static double biLerp(double x, double y, double q11, double q12, double q21, double q22, double x1, double x2, double y1, double y2)
    {
        double r1 = lerp(x, x1, x2, q11, q21);
        double r2 = lerp(x, x1, x2, q12, q22);
        return lerp(y, y1, y2, r1, r2);
    }

    public static double lerp(double x, double x1, double x2, double q00, double q01)
    {
        return ((x2 - x) / (x2 - x1)) * q00 + ((x - x1) / (x2 - x1)) * q01;
    }

    public static float lerpf(float x, float x1, float x2, float q00, float q01)
    {
        return ((x2 - x) / (x2 - x1)) * q00 + ((x - x1) / (x2 - x1)) * q01;
    }

    public static double lerp(double x1, double x2, double p)
    {
        return x1 * (1.0 - p) + x2 * p;
    }

    public static float lerpf(float x1, float x2, float p)
    {
        return x1 * (1.0f - p) + x2 * p;
    }

    public static double triLerp(double x, double y, double z, double q000, double q001, double q010, double q011, double q100, double q101, double q110, double q111, double x1, double x2, double y1, double y2, double z1, double z2)
    {
        double x00 = lerp(x, x1, x2, q000, q100);
        double x10 = lerp(x, x1, x2, q010, q110);
        double x01 = lerp(x, x1, x2, q001, q101);
        double x11 = lerp(x, x1, x2, q011, q111);
        double r0 = lerp(y, y1, y2, x00, x01);
        double r1 = lerp(y, y1, y2, x10, x11);
        return lerp(z, z1, z2, r0, r1);
    }

    public static float triLerpf(float x, float y, float z, float q000, float q001, float q010, float q011, float q100, float q101, float q110, float q111, float x1, float x2, float y1, float y2, float z1, float z2)
    {
        float x00 = lerpf(x, x1, x2, q000, q100);
        float x10 = lerpf(x, x1, x2, q010, q110);
        float x01 = lerpf(x, x1, x2, q001, q101);
        float x11 = lerpf(x, x1, x2, q011, q111);
        float r0 = lerpf(y, y1, y2, x00, x01);
        float r1 = lerpf(y, y1, y2, x10, x11);
        return lerpf(z, z1, z2, r0, r1);
    }

    public static int mapToPositive(int x)
    {
        if (x >= 0)
            return x * 2;

        return -x * 2 - 1;
    }

    public static int redoMapToPositive(int x)
    {
        if (x % 2 == 0)
        {
            return x / 2;
        }

        return -(x / 2) - 1;
    }

    public static int cantorize(int k1, int k2)
    {
        return ((k1 + k2) * (k1 + k2 + 1) / 2) + k2;
    }

    public static int cantorX(int c)
    {
        int j = (int)(Mathf.Sqrt(0.25f + 2 * c) - 0.5);
        return j - cantorY(c);
    }

    public static int cantorY(int c)
    {
        int j = (int)(Mathf.Sqrt(0.25f + 2 * c) - 0.5);
        return c - j * (j + 1) / 2;
    }

    public static int ceilPowerOfTwo(int val)
    {
        val--;
        val = (val >> 1) | val;
        val = (val >> 2) | val;
        val = (val >> 4) | val;
        val = (val >> 8) | val;
        val = (val >> 16) | val;
        val++;
        return val;
    }

    public static int sizeOfPower(int val)
    {
        int power = 0;
        while (val > 1)
        {
            val = val >> 1;
            power++;
        }
        return power;
    }

    public static int floorToInt(float val)
    {
        int i = (int)val;
        return (val < 0 && val != i) ? i - 1 : i;
    }

    public static int ceilToInt(float val)
    {
        int i = (int)val;
        return (val >= 0 && val != i) ? i + 1 : i;
    }

    public static VectorI3 ArrayToDirection(this Vector3 arrayPosition, Vector3 arraySize)
    {
        if (arrayPosition.x == 0) { arrayPosition.x = -1; }
        else if (arrayPosition.x == arraySize.x - 1) { arrayPosition.x = 1; }
        else { arrayPosition.x = 0; }

        if (arrayPosition.y == 0) { arrayPosition.y = -1; }
        else if (arrayPosition.y == arraySize.y - 1) { arrayPosition.y = 1; }
        else { arrayPosition.y = 0; }

        if (arrayPosition.z == 0) { arrayPosition.z = -1; }
        else if (arrayPosition.z == arraySize.z - 1) { arrayPosition.z = 1; }
        else { arrayPosition.z = 0; }

        return arrayPosition;
    }

    public static bool IsEdgeOfArray(this VectorI3 arrayPosition, VectorI3 arraySize)
    {
        arraySize -= VectorI3.one;
        return (arrayPosition.x == 0 || arrayPosition.x == arraySize.x || arrayPosition.y == 0 || arrayPosition.y == arraySize.y || arrayPosition.z == 0 || arrayPosition.z == arraySize.z);
    }
    public static bool IsCornerEdgeOfArray(this VectorI3 arrayPosition, VectorI3 arraySize)
    {
        if (arrayPosition.x == 0 || arrayPosition.x == arraySize.x - 1)
        {
            if (arrayPosition.y == 0 || arrayPosition.y == arraySize.y - 1) { return true; }
            else if (arrayPosition.z == 0 || arrayPosition.z == arraySize.z - 1) { return true; }
        }

        if (arrayPosition.y == 0 || arrayPosition.y == arraySize.y - 1)
        {
            if (arrayPosition.x == 0 || arrayPosition.x == arraySize.x - 1) { return true; }
            else if (arrayPosition.z == 0 || arrayPosition.z == arraySize.z - 1) { return true; }
        }

        if (arrayPosition.z == 0 || arrayPosition.z == arraySize.z - 1)
        {
            if (arrayPosition.x == 0 || arrayPosition.x == arraySize.x - 1) { return true; }
            else if (arrayPosition.y == 0 || arrayPosition.y == arraySize.y - 1) { return true; }
        }

        return false;
    }
    public static bool IsCornerOfArray(this VectorI3 arrayPosition, VectorI3 arraySize)
    {
        return
            ((arrayPosition.x == 0) || (arrayPosition.x == arraySize.x - 1)) &&
                ((arrayPosition.y == 0) || (arrayPosition.y == arraySize.y - 1)) &&
                ((arrayPosition.z == 0) || (arrayPosition.z == arraySize.z - 1));
    }

    public static bool ArrayOutOfBounds(this VectorI3 index, VectorI3 arraySize)
    {
        //Debug.LogWarning("ArrayOutOfBounds("+index+", "+arraySize+") x = " + (index.x > arraySize.x - 1 || index.x < 0) + " y = " + (index.y > arraySize.y - 1 || index.y < 0) + " z = " + (index.z > arraySize.z - 1 || index.z < 0));
        if ((int)index.x > ((int)arraySize.x - 1) || (int)index.x < 0) { return true; }
        if ((int)index.y > ((int)arraySize.y - 1) || (int)index.y < 0) { return true; }
        if ((int)index.z > ((int)arraySize.z - 1) || (int)index.z < 0) { return true; }

        return false;
    }

    public static VectorI3 Wrap3DIndex(this VectorI3 positionIndex, VectorI3 arraySize)
    {
        VectorI3 newDirection = new VectorI3(
            positionIndex.x % arraySize.x,
            positionIndex.y % arraySize.y,
            positionIndex.z % arraySize.z
            );

        if (newDirection.x < 0) { newDirection.x = arraySize.x + newDirection.x; }
        if (newDirection.y < 0) { newDirection.y = arraySize.y + newDirection.y; }
        if (newDirection.z < 0) { newDirection.z = arraySize.z + newDirection.z; }

        return newDirection;
    }

    public static VectorI3 Wrap3DIndex(this VectorI3 positionIndex, VectorI3 direction, VectorI3 arraySize)
    {
        VectorI3 newDirection = new VectorI3(
            ((positionIndex.x + direction.x) % (arraySize.x)),
            ((positionIndex.y + direction.y) % (arraySize.y)),
            ((positionIndex.z + direction.z) % (arraySize.z))
            );

        if (newDirection.x < 0) { newDirection.x = arraySize.x + newDirection.x; }
        if (newDirection.y < 0) { newDirection.y = arraySize.y + newDirection.y; }
        if (newDirection.z < 0) { newDirection.z = arraySize.z + newDirection.z; }

        return newDirection;
    }

    public static int WrapIndexMod(this int index, int endIndex, int maxSize)
    {
        return (endIndex + index) % maxSize;
    }


    public static int FlatIndex(this VectorI3 index, VectorI3 size)
    {
        return index.x + index.y * size.x + index.z * size.x * size.y;
    }

    public static VectorI3 FlatTo3DIndex(this int index, VectorI3 size)
    {
        return new VectorI3(
            index % size.x,
            index / size.x % size.y,
            index / (size.x * size.y) % size.z
        );
    }

    public static readonly int[] opposite = new int[]{
		1, 0,
		3, 2,
		5, 4,

		7, 6,
		9, 8,
		11, 10,
		13, 12,

		15, 14,
		17, 16,
		19, 18,
		21, 20,
		23, 22,
		25, 24
	};

    public static readonly VectorI3[] direction = new VectorI3[]{
		VectorI3.left,
		VectorI3.right,
		VectorI3.up,
		VectorI3.down,
		VectorI3.forward,
		VectorI3.back,
		
		//Corner
		new VectorI3(-1,1,1),
		new VectorI3(1,-1,-1),

		new VectorI3(-1,-1,1),
		new VectorI3(1,1,-1),

		new VectorI3(-1,1,-1),
		new VectorI3(1,-1,1),

		new VectorI3(-1,-1,-1),
		new VectorI3(1,1,1),

		//Edge Corner
		new VectorI3(-1,0,1), //Left Front
		new VectorI3(1,0,-1), //Right Back
		new VectorI3(-1,0,-1), //Left Back
		new VectorI3(1,0,1), //Right Front

		new VectorI3(-1,1,0), //Left Top
		new VectorI3(1,-1,0), //Right Bottom
		new VectorI3(-1,-1,0), //Left Bottom
		new VectorI3(1,1,0), //Right Top

		new VectorI3(0,1,1), //Top Front
		new VectorI3(0,-1,-1), //Bottom Back
		new VectorI3(0,1,-1), //Top Back
		new VectorI3(0,-1,1), //Bottom Front
	};

    public static VectorI3 Direction(this BlockHelper.Direction direction)
    {
        return MathHelper.direction[(int)direction];
    }

    public static BlockHelper.Direction Direction(int direction)
    {
        return (BlockHelper.Direction)Enum.Parse(typeof(BlockHelper.Direction), direction.ToString());
    }

    public static VectorI3 IntToDirection(this int index)
    {
        return direction[index];
    }

    public static int DirectionToIndex(this Vector3 dir)
    {
        for (int i = 0; i < direction.Length; i++)
        {
            if (direction[i] == dir)
            {
                return i;
            }
        }

        return 0;
    }

    public static Vector3 Vector3Clamp(this Vector3 value, Vector3 min, Vector3 max)
    {
        value.x = value.x > max.x ? value.x = max.x : (value.x < min.x ? value.x = min.x : value.x);
        value.y = value.y > max.y ? value.y = max.y : (value.y < min.y ? value.y = min.y : value.y);
        value.z = value.z > max.z ? value.z = max.z : (value.z < min.z ? value.z = min.z : value.z);

        return value;
    }

    public static int VectorSize(this Vector3 vector)
    {
        return Mathf.RoundToInt(vector.x * vector.y * vector.z);
    }

    public static Vector3[] OffsetVector3(int count, Vector3[] vectors, float x, float y, float z, float Scale)
    {
        Vector3[] offsets = new Vector3[count];

        for (int i = 0; i < count; i++)
        {
            offsets[i] = vectors[i] * Scale;
            offsets[i].x += x * Scale;
            offsets[i].y += y * Scale;
            offsets[i].z += z * Scale;
        }

        return offsets;
    }
    public static Vector3[] OffsetVector3(int count, Vector3[] vectors, Vector3[] offset)
    {
        Vector3[] offsets = new Vector3[count];

        for (int i = 0; i < offset.Length; i++)
        {
            offsets[i] = vectors[i] + offset[i];
        }

        return offsets;
    }
    public static Vector3[] OffsetVector3(int count, Vector3[] vectors, Vector3[] offset, Vector3 offset2)
    {
        Vector3[] offsets = new Vector3[count];

        for (int i = 0; i < count; i++)
        {
            offsets[i] = vectors[i] + offset2 + offset[i];
        }

        return offsets;
    }

    public static Vector2[] OffsetVector2(int count, Vector2[] vectors, Vector2 offset)
    {
        Vector2[] offsets = new Vector2[count];

        for (int i = 0; i < count; i++)
        {
            offsets[i] = vectors[i] + offset;
        }

        return offsets;
    }

    public static ushort[] OffsetInt(ushort count, ushort[] ints, ushort offset)
    {
        ushort[] offsets = new ushort[count];

        for (int i = 0; i < count; i++)
        {
            offsets[i] = (ushort)(ints[i] + offset);
        }

        return offsets;
    }

    public static Vector3 ParseVector3(string sourceString)
    {
        string outString;
        Vector3 outVector3;
        string[] splitString = new string[3];

        // Trim extranious parenthesis
        outString = sourceString.Substring(1, sourceString.Length - 2);

        // Split delimted values into an array
        splitString = outString.Split(","[0]);

        // Build new Vector3 from array elements
        outVector3.x = float.Parse(splitString[0]);
        outVector3.y = float.Parse(splitString[1]);
        outVector3.z = float.Parse(splitString[2]);

        return outVector3;
    }
}
