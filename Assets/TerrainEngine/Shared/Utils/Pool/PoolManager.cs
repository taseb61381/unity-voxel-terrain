﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    public class UnityPoolManager
    {
        public PoolManager Pool;
        public object MarchingPool;
        public PoolInfoRef TempChunkPool;
        public PoolInfoRef SimpleMeshData;
        public PoolInfoRef ColorUVMeshData;
        public PoolInfoRef CompleteMeshData;
        public PoolInfoRef MarchingFluidPool;
        public PoolInfoRef FluidPool;
        public PoolInfoRef ViewablePool;
        public PoolInfoRef NoiseBuilderPool;
        public PoolInfoRef GameObjectCreateActionPool;
        public PoolInfoRef GameObjectPoolRemoveActionPool;
        public PoolInfoRef GameObjectPoolRemoveActionListPool;
        public PoolInfoRef UniqueMaterials;
        public PoolInfoRef MeshArrayCount;

        public UnityPoolManager(PoolManager Pool)
        {
            this.Pool = Pool;
            this.TempChunkPool = Pool.GetPool("TempMeshData");
            this.SimpleMeshData = Pool.GetPool("SimpleMeshData");
            this.ColorUVMeshData = Pool.GetPool("ColorUVMeshData");
            this.CompleteMeshData = Pool.GetPool("CompleteMeshData");
            this.MarchingFluidPool = Pool.GetPool("MarchingFluidData");
            this.FluidPool = Pool.GetPool("FluidInterpolation");
            this.ViewablePool = Pool.GetPool("RegenerateViewableChunkAction");
            this.NoiseBuilderPool = Pool.GetPool("NoiseBuilder");
            this.GameObjectCreateActionPool = Pool.GetPool("CreateGameObjectAction");
            this.GameObjectPoolRemoveActionPool = Pool.GetPool("GameObjectPoolRemoveAction");
            this.GameObjectPoolRemoveActionListPool = Pool.GetPool("GameObjectPoolRemoveListAction");
            this.UniqueMaterials = Pool.GetPool("UniqueMaterialsArray");
            this.MeshArrayCount = Pool.GetPool("MeshArray");

            PoolManager.Pools.array[SimpleMeshData.Index].CanFreeMemory = false;

            PoolManager.Pools.array[MarchingFluidPool.Index].EnablePooling = true;
            PoolManager.Pools.array[TempChunkPool.Index].EnablePooling = true;
            PoolManager.Pools.array[TempChunkPool.Index].CanFreeMemory = false;
            PoolManager.Pools.array[FluidPool.Index].EnablePooling = true;
            PoolManager.Pools.array[ViewablePool.Index].EnablePooling = true;
            PoolManager.Pools.array[ViewablePool.Index].MaxPool = 100;
        }

        public IMeshData GetColorUVMeshData()
        {
            return PoolManager.Pools.array[ColorUVMeshData.Index].GetData<ColorUVMeshData>();
        }

        public IMeshData GetSimpleMeshData()
        {
            return PoolManager.Pools.array[SimpleMeshData.Index].GetData<SimpleMeshData>();
        }

        public IMeshData GetCompleteMeshData()
        {
            return PoolManager.Pools.array[CompleteMeshData.Index].GetData<CompleteMeshData>();
        }

        #region Materials

        public FList<MaterialIdInformationContainer> MaterialsContainers = new FList<MaterialIdInformationContainer>(1);

        public Material[] GetMaterials(FList<MaterialIdInformation> L)
        {
            if(L.Count == 0)
                return null;

            lock(MaterialsContainers.array)
            {
                for(int i=MaterialsContainers.Count-1;i>=0;--i)
                {
                    if(MaterialsContainers.array[i].IsEqual(L))
                        return MaterialsContainers.array[i].MatsArray;
                }

                ++UniqueMaterials.CreatedObject;
                MaterialIdInformationContainer Container = new MaterialIdInformationContainer(L);
                MaterialsContainers.Add(Container);
                return Container.MatsArray;
            }
        }

        // Only Array (Details,Grass)
        public FastDictionary<int, Stack<Material[]>> MaterialsPool = new FastDictionary<int, Stack<Material[]>>(50);
        public int MaterialEnqueue = 0;
        public int MaterialDequeue = 0;
        public int MaterialCreated = 0;

        public Material[] GetMaterialArray(int Size)
        {
            Stack<Material[]> Queue = null;

            lock (MaterialsPool)
            {
                if (MaterialsPool.TryGetValue(Size, out Queue))
                {
                    if (Queue.Count != 0)
                    {
                        ++MaterialDequeue;
                        return Queue.Pop();
                    }
                }
            }

            ++MaterialCreated;
            return new Material[Size];
        }

        public void CloseMaterialArray(Material[] Mats)
        {
            if (Mats == null)
                return;

            int Size = Mats.Length;
            Stack<Material[]> Queue = null;
            lock (MaterialsPool)
            {
                if (!MaterialsPool.TryGetValue(Size, out Queue))
                {
                    Queue = new Stack<Material[]>();
                    MaterialsPool.Add(Size, Queue);
                }

                if (Queue.Count > 30)
                    return;

                MaterialEnqueue++;
                Queue.Push(Mats);
            }
        }

        #endregion

        #region IndiceArray

        public FastDictionary<int, Stack<int[][]>> IndicesPool = new FastDictionary<int, Stack<int[][]>>();
        public int IndicesEnqueue = 0;
        public int IndicesDequeue = 0;
        public int IndicesCreated = 0;

        public int[][] GetIndicesArray(int Size)
        {
            Stack<int[][]> Queue = null;

            lock (IndicesPool)
            {
                if (IndicesPool.TryGetValue(Size, out Queue))
                {
                    if (Queue.Count != 0)
                    {
                        ++IndicesDequeue;
                        return Queue.Pop();
                    }
                }
            }

            ++IndicesCreated;
            return new int[Size][];
        }

        public void CloseIndicesArray(int[][] Indices)
        {
            if (Indices == null)
                return;

            //return;

            int Size = Indices.Length;
            Stack<int[][]> Queue = null;
            lock (IndicesPool)
            {
                if (!IndicesPool.TryGetValue(Size, out Queue))
                {
                    Queue = new Stack<int[][]>();
                    IndicesPool.Add(Size, Queue);
                }

                if (Queue.Count > 50)
                    return;

                for (--Size; Size >= 0; --Size)
                    Indices[Size] = null;

                IndicesEnqueue++;
                Queue.Push(Indices);
            }
        }

        #endregion

        #region List

        static public Stack<List<int>> ListPool = new Stack<List<int>>();

        public List<int> GetList()
        {
            lock (ListPool)
                if (ListPool.Count != 0)
                    return ListPool.Pop();

            return new List<int>();
        }

        public void CloseList(List<int> L)
        {
            if (L == null)
                return;

            L.Clear();

            lock (ListPool)
                ListPool.Push(L);
        }

        #endregion
    }

    public struct PoolInfoRef
    {
        public int Index;

        public PoolInfoRef(int Index)
        {
            this.Index = Index;
        }

        public T GetData<T>() where T : IPoolable
        {
            return PoolManager.Pools.array[Index].GetData<T>();
        }

        public void CloseData(IPoolable Obj)
        {
            PoolManager.Pools.array[Index].CloseData(Obj);
        }

        public void AddCreatedData(int Value)
        {
            PoolManager.Pools.array[Index].CreatedObject += Value;
        }

        public void AddDequeueData(int Value)
        {
            PoolManager.Pools.array[Index].DequeueObject += Value;
        }

        public void AddEnqueueData(int Value)
        {
            PoolManager.Pools.array[Index].EnqueueObject += Value;
        }

        public int AddPool()
        {
            return PoolManager.Pools.array[Index].AddPool();
        }

        public void AddCreated()
        {
            PoolManager.Pools.array[Index].AddCreated();
        }

        public void RemovePool()
        {
            PoolManager.Pools.array[Index].RemovePool();
        }

        public int CreatedObject
        {
            get
            {
                return PoolManager.Pools.array[Index].CreatedObject;
            }
            set
            {
                PoolManager.Pools.array[Index].CreatedObject = value;
            }
        }

        public int EnqueueData
        {
            get
            {
                return PoolManager.Pools.array[Index].EnqueueObject;
            }
            set
            {
                PoolManager.Pools.array[Index].EnqueueObject = value;
            }
        }

        public int DequeueData
        {
            get
            {
                return PoolManager.Pools.array[Index].DequeueObject;
            }
            set
            {
                PoolManager.Pools.array[Index].DequeueObject = value;
            }
        }

        public int QueueCount
        {
            get
            {
                return PoolManager.Pools.array[Index].QueueCount;
            }
            set
            {
                PoolManager.Pools.array[Index].QueueCount = value;
            }
        }
    }

    public class PoolManager
    {
        static public FList<PoolInfo> Pools = new FList<PoolInfo>(500);
        static public FList<Stack<IPoolable>> Queues = new FList<Stack<IPoolable>>(500);

        static public int CreatePool(string Name, bool EnablePooling)
        {
            int Id = 0;
            lock (Pools.array)
            {
                Id = Pools.Count;
                Pools.Add(new PoolInfo(Id, Name, EnablePooling));
                Queues.Add(null);
            }
            return Id;
        }

        static private PoolManager _Pool;
        static public PoolManager Pool
        {
            get
            {
                if (_Pool == null)
                    _Pool = new PoolManager(true,0);

                return _Pool;
            }
        }

        /// <summary>
        /// UnityPool : Contains Materials,Mesh,TempData,etc...
        /// </summary>
        public UnityPoolManager UnityPool;
        static public UnityPoolManager UPool
        {
            get
            {
                return Pool.UnityPool;
            }
        }

        public int ThreadId;
        public bool EnablePooling = true;

        public PoolManager(bool EnablePooling, int ThreadId)
        {
            this.EnablePooling = EnablePooling;
            this.ThreadId = ThreadId;

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
            UnityPool = new UnityPoolManager(this);
#endif
        }

        public FList<int> ObjectPool = new FList<int>(50);

        public PoolInfoRef GetPool(string Name)
        {
            int I = 0;

            lock (ObjectPool.array)
            {
                for (int i = ObjectPool.Count - 1; i >= 0; --i)
                    if (PoolManager.Pools.array[ObjectPool.array[i]].Name == Name)
                        return new PoolInfoRef(ObjectPool.array[i]);

                I = CreatePool(Name, EnablePooling);
                ObjectPool.Add(I);
            }

            return new PoolInfoRef(I);
        }

        public int GetPoolId(string Name)
        {
            int I = 0;

            lock (ObjectPool.array)
            {
                for (int i = ObjectPool.Count - 1; i >= 0; --i)
                    if (PoolManager.Pools.array[ObjectPool.array[i]].Name == Name)
                        return ObjectPool.array[i];

                I = CreatePool(Name, EnablePooling);
                ObjectPool.Add(I);
            }

            return I;
        }

        public T GetData<T>(string Name, bool Force = false) where T : IPoolable
        {
            if (!EnablePooling && !Force)
                return (T)Activator.CreateInstance(typeof(T));

            int I = GetPoolId(Name);
            T obj = PoolManager.Pools.array[I].GetData<T>();
            return obj;
        }

        public IPoolable GetData(string Name)
        {
            if (!EnablePooling)
                return null;

            int I = GetPoolId(Name);
            IPoolable obj = PoolManager.Pools.array[I].GetData();
            return obj;
        }

        public void SetData(string Name, IPoolable Obj)
        {
            if (!EnablePooling)
                return;

            int I = GetPoolId(Name);
            PoolManager.Pools.array[I].SetData(Obj);
        }

        public void CloseData(string Name, IPoolable Data, bool Force = false)
        {
            if (!EnablePooling && !Force)
                return;

            int I = GetPoolId(Name);
            PoolManager.Pools.array[I].CloseData(Data);
        }

        public delegate void PoolInfoCallDelegate(string Name, int Created, int Enqueue, int Dequeue, int Deleted);

        public void GetStats(PoolInfoCallDelegate del)
        {
            int Created = 0;
            int Enqueue = 0;
            int Dequeue = 0;
            int Deleted = 0;

            for (int i = 0; i < ObjectPool.Count; ++i)
            {
                int Index = ObjectPool.array[i];
                Created += PoolManager.Pools.array[Index].CreatedObject;
                Deleted += PoolManager.Pools.array[Index].DeletedData;
                Enqueue += PoolManager.Pools.array[Index].EnqueueObject;
            }

            del("Total", Created, Enqueue, Dequeue, Deleted);

            for (int i = 0; i < ObjectPool.Count; ++i)
            {
                int Index = ObjectPool.array[i];

                del(PoolManager.Pools.array[Index].Name, PoolManager.Pools.array[Index].CreatedObject, PoolManager.Pools.array[Index].EnqueueObject, PoolManager.Pools.array[Index].DequeueObject, PoolManager.Pools.array[Index].DeletedData);
            }

            if (UnityPool != null)
            {
                del("Materials Array", UnityPool.MaterialCreated, UnityPool.MaterialEnqueue, UnityPool.MaterialDequeue, 0);
                del("Indices Array", UnityPool.IndicesCreated, UnityPool.IndicesEnqueue, UnityPool.IndicesDequeue, 0);
            }
        }

        public void GetStats(List<string> Str)
        {
            if(Str == null)
                Str = new List<string>(ObjectPool.Count+3);
            else
                Str.Clear();

            int TotalObjects = 0;

            Str.Add("");
            Str.Add("");
            Str.Add("");

            for (int i=0;i<ObjectPool.Count;++i)
            {
                int Index = ObjectPool.array[i];

                TotalObjects += PoolManager.Pools.array[Index].CreatedObject - PoolManager.Pools.array[Index].DeletedData;

                if (PoolManager.Pools.array[Index].CreatedObject != 0 || PoolManager.Pools.array[Index].DequeueObject != 0)
                {
                    Str.Add(PoolManager.Pools.array[Index].GetStats(PoolManager.Pools.array[Index].Name));
                }
            }

            Str[0] = "Classes in Memory :" + TotalObjects;

            if (UnityPool != null)
            {
                Str[1] = "Materials Array :\n" + "-Created= " + UnityPool.MaterialCreated + "\n-Dequeue=" + UnityPool.MaterialDequeue + "\n-Enqueue=" + UnityPool.MaterialEnqueue;
                Str[2] = "Indices Array :\n" + "-Created= " + UnityPool.IndicesCreated + "\n-Dequeue=" + UnityPool.IndicesDequeue + "\n-Enqueue=" + UnityPool.IndicesEnqueue;
            }
        }
    }
}
