﻿using System;
using System.Collections.Generic;
using System.Linq;
using TerrainEngine;

public struct PoolInfo
{
    public int PoolId;
    public string Name;
    public bool EnablePooling;

    public int CreatedObject;
    public int DequeueObject;
    public int EnqueueObject;
    public int QueueCount;
    public int DeletedData;
    public int MaxPool;
    public int LowerNumberInPool;
    public float LastMemoryFree;
    public bool CanFreeMemory;

    public Stack<IPoolable> CachedPool
    {
        get
        {
            return PoolManager.Queues.array[PoolId];
        }
        set
        {
            PoolManager.Queues.array[PoolId] = value;
        }
    }

    public PoolInfo(int PoolId, string Name, bool EnablePooling)
    {
        this.PoolId = PoolId;
        this.Name = Name;
        this.EnablePooling = EnablePooling;

        CreatedObject = 0;
        DequeueObject = 0;
        EnqueueObject = 0;
        QueueCount = 0;
        DeletedData = 0;
        MaxPool = 0;
        LowerNumberInPool = 0;
        LastMemoryFree = 0;
        CanFreeMemory = true;
    }

    public T GetData<T>() where T : IPoolable
    {
        IPoolable obj = null;
        Stack<IPoolable> Pool = CachedPool;

        if (EnablePooling && Pool != null)
        {
            lock (Pool)
            {
                if (Pool.Count != 0)
                {
                    ++DequeueObject;
                    obj = Pool.Pop();
                }
            }
        }

        if (obj == null)
        {
            ++CreatedObject;
            obj = (T)Activator.CreateInstance(typeof(T));
        }

        obj.SetPool(PoolId);
        return obj as T;
    }

    public IPoolable GetData()
    {
        if (!EnablePooling)
            return null;

        Stack<IPoolable> Pool = CachedPool;
        if (Pool == null)
        {
            return null;
        }
        else
        {
            lock (Pool)
            {
                IPoolable obj = null;

                if (Pool != null && Pool.Count != 0)
                {
                    obj = Pool.Pop();
                    obj.SetPool(PoolId);
                    QueueCount = Pool.Count();
                    RemovePool();
                }

                return obj;
            }
        }
    }

    public void SetData(IPoolable Obj)
    {
        if (!EnablePooling)
            return;


        Obj.SetPool(PoolId);
        AddCreated();
    }

    public void CloseData(IPoolable Obj)
    {
        Obj.Clear();
        Obj.SetPool(int.MinValue);

        if (!EnablePooling)
            return;

        Stack<IPoolable> Pool = CachedPool;

        if (Pool == null)
            CachedPool = Pool = new Stack<IPoolable>(2);

        lock (Pool)
        {
            if (MaxPool != 0 && Pool.Count >= MaxPool)
                return;

            Pool.Push(Obj);
            QueueCount = Pool.Count;
            AddPool();
        }
    }

    public string GetStats(string Name)
    {
        return Name + " :\n" + "-Created:" + CreatedObject + "\nDeleted:" + DeletedData + "\n-Dequeue=" + DequeueObject + "\n-Enqueue:" + EnqueueObject + "\n-InPool:" + QueueCount;
    }

    public void AddCreated()
    {
        ++CreatedObject;
    }

    public void RemovePool()
    {
        ++DequeueObject;
        if (CanFreeMemory && QueueCount < LowerNumberInPool)
        {
            LastMemoryFree = ThreadManager.UnityTime + 30;
            LowerNumberInPool = QueueCount;
        }
    }

    public int AddPool()
    {
        ++EnqueueObject;
        if (CanFreeMemory && LastMemoryFree < ThreadManager.UnityTime)
        {
            Stack<IPoolable> Pool = CachedPool;

            int ToFree = 0;
            if (LowerNumberInPool != 0 && LowerNumberInPool != QueueCount)
            {
                ToFree = LowerNumberInPool;
                DeletedData += ToFree;
                UnityEngine.Debug.Log("Freeing Memory :" + Name + ",Count:" + ToFree);
                while (LowerNumberInPool > 0 && (Pool != null && Pool.Count != 0))
                    Pool.Pop().Delete();
            }

            LowerNumberInPool = QueueCount;
            LastMemoryFree = ThreadManager.UnityTime + 120;
            return ToFree;
        }

        return 0;
    }
}