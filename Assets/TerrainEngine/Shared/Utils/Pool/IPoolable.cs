﻿using TerrainEngine;

public abstract class IPoolable
{
    internal int PoolId = int.MinValue;

    public void SetPool(int PoolId)
    {
        this.PoolId = PoolId;
    }

    public abstract void Clear();

    public void ForceClose()
    {
        if (PoolId >= 0)
        {
            PoolManager.Pools.array[PoolId].CloseData(this);
        }
        else
        {
            Clear();
        }
    }

    public virtual void Close()
    {
        if (PoolId >= 0)
        {
            PoolManager.Pools.array[PoolId].CloseData(this);
        }
        else
        {
            Clear();
        }
    }

    public virtual void Delete()
    {
        this.PoolId = int.MinValue;
    }
}