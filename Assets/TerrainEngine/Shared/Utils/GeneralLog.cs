﻿using System;
using System.Text;
using UnityEngine;

/// <summary>
/// Unified Logger , usable in Unity and Server.
/// Use Init to register delegate function for draw message. Unity : Debug.Log,Debug.LogWarning,etc...
/// </summary>
static public class GeneralLog
{
    public delegate void OnLogDelegate(object message);
    public delegate void OnExceptionDelgate(Exception e);

    static public OnLogDelegate OnLog;
    static public OnLogDelegate OnLogWarning;
    static public OnLogDelegate OnLogError;
    static public OnLogDelegate OnLogDebug;
    static public OnLogDelegate OnLogSuccess;
    static public OnExceptionDelgate OnLogException;
    static public OnLogDelegate OnLogArray;
    static public bool Inited = false;
    static public bool DrawLogs = true;

    static public void Init(OnLogDelegate Log, OnLogDelegate Warning, OnLogDelegate Error, OnExceptionDelgate Ex)
    {
        if (!Inited)
        {
            Inited = true;
            OnLog += Log;
            OnLogWarning += Warning;
            OnLogError += Error;
            OnLogException += Ex;
            OnLogSuccess += Log;
        }
    }

    static public void Log(object message)
    {
        if (!Inited)
        {
            Init(Debug.Log, Debug.LogWarning, Debug.LogError, Debug.LogException);
        }

        if (OnLog != null)
            OnLog(message);
    }

    static public void Log(object title, object message)
    {
        if (OnLog != null)
            OnLog(title + " : " + message);
    }

    static public void LogWarning(object message)
    {
        if (!Inited)
        {
            Init(Debug.Log, Debug.LogWarning, Debug.LogError, Debug.LogException);
        }

        if (OnLogWarning != null)
            OnLogWarning(message);
    }

    static public void LogError(object message)
    {
        if (!Inited)
        {
            Init(Debug.Log, Debug.LogWarning, Debug.LogError, Debug.LogException);
        }

        if (OnLogError != null)
            OnLogError(message);
    }

    static public void LogDebug(object message)
    {
        if (!Inited)
        {
            Init(Debug.Log, Debug.LogWarning, Debug.LogError, Debug.LogException);
        }

        if (OnLogDebug != null)
            OnLogDebug(message);
    }

    static public void LogSuccess(object message)
    {
        if (OnLogSuccess != null)
            OnLogSuccess(message);
    }

    static public void LogWarning(object title, object message)
    {
        if (OnLogWarning != null)
            OnLogWarning(title + " : " + message);
    }

    static public void LogDebug(object title, object message)
    {
        if (OnLogDebug != null)
            OnLogDebug(title + " : " + message);
    }

    static public void LogError(object title, object message)
    {
        if (OnLogError != null)
            OnLogError(title + " : " + message);
    }

    static public void LogSuccess(object title, object message)
    {
        if (OnLogSuccess != null)
            OnLogSuccess(title + " : " + message);
    }

    static public void LogException(Exception e)
    {
        if (OnLogException != null)
            OnLogException(e);
    }

    static public void LogArray(string name, byte[] dump, int start, int len, bool Force = false)
    {
        if (OnLogArray != null)
            OnLogArray("P " + name + " : " + Hex(dump, start, len));
    }

    static public string Hex(byte[] dump, int start, int len)
    {
        StringBuilder hexDump = new StringBuilder();

        try
        {
            int end = start + len;
            for (int i = start; i < end; i += 16)
            {
                StringBuilder text = new StringBuilder();
                StringBuilder hex = new StringBuilder();
                hex.Append("\n");

                for (int j = 0; j < 16; j++)
                {
                    if (j + i < end)
                    {
                        byte val = dump[j + i];
                        hex.Append(" ");
                        hex.Append(dump[j + i].ToString("X2"));
                        if (j == 3 || j == 7 || j == 11)
                            hex.Append(" ");
                        if (val >= 32 && val <= 127)
                        {
                            text.Append((char)val);
                        }
                        else
                        {
                            text.Append(".");
                        }
                    }
                    else
                    {
                        hex.Append("   ");
                        text.Append("  ");
                    }
                }
                hex.Append("  ");
                hex.Append("//" + text.ToString());
                hexDump.Append(hex.ToString());
            }
        }
        catch (Exception e)
        {
            LogError("HexDump", e.ToString());
        }

        return hexDump.ToString();
    }
}