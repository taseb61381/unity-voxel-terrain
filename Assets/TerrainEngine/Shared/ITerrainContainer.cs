﻿using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    public class ITerrainContainer
    {
        public ITerrainBlock[] BlocksArray = new ITerrainBlock[65536];
        public List<ITerrainBlock> Blocks = new List<ITerrainBlock>(10);
        private ushort LastBlockId;  // Each TerrainBlock have an unique ID.
        internal ushort MaxId = 0; // Max Block id created

        /// <summary>
        /// Generate an unique ID for each Terrain Block.Shortest Terain modification packet uid,x,y,z,type,volume,modtype
        /// Terrain uid is sent (ushort), no terrain position (4 * int)
        /// </summary>
        /// <returns></returns>
        public ushort GetNewBlockId()
        {
            while (BlocksArray[LastBlockId] != null)
            {
                ++LastBlockId;

                if (LastBlockId >= 65535)
                    LastBlockId = 0;
            }

            return LastBlockId;
        }

        /// <summary>
        /// Return an existing block from this Position
        /// </summary>
        public ITerrainBlock GetBlock(VectorI3 Position)
        {
            for (int i = Blocks.Count - 1; i >= 0; --i)
            {
                if (Blocks[i].LocalPosition == Position)
                {
                    return Blocks[i];
                }
            }

            return null;
        }

        /// <summary>
        /// Return an existing block from this Position
        /// </summary>
        public bool GetBlock(VectorI3 Position, out ITerrainBlock Block)
        {
            for (int i = Blocks.Count - 1; i >= 0; --i)
            {
                if (Blocks[i].LocalPosition == Position)
                {
                    Block = Blocks[i];
                    return true;
                }
            }

            Block = null;
            return false;
        }

        public bool GetBlockXY(int x,int y,out ITerrainBlock Block)
        {
            for (int i = Blocks.Count - 1; i >= 0; --i)
            {
                if (Blocks[i].LocalPosition.x == x && Blocks[i].LocalPosition.y == y)
                {
                    Block = Blocks[i];
                    return true;
                }
            }

            Block = null;
            return false;
        }

        /// <summary>
        /// Return a terrain block by ID. More faster than position, and cost lest bandwitch
        /// </summary>
        public ITerrainBlock GetBlock(ushort ID)
        {
            if (ID >= ushort.MaxValue)
                return null;

            return BlocksArray[ID];
        }

        public void GetBlocks(List<ITerrainBlock> L)
        {
            L.Clear();
            for (int i = 0; i <= MaxId; ++i)
            {
                if (BlocksArray[i] != null)
                {
                    L.Add(BlocksArray[i]);
                }
            }
        }

        /// <summary>
        /// Return all blocks sorted by distance from position
        /// </summary>
        public void GetBlocksByDistance(List<ITerrainBlock> L, Vector3 Position)
        {
            GetBlocks(L);

            L.Sort(delegate(ITerrainBlock A, ITerrainBlock B)
            {
                return Vector3.Distance(A.CenterPosition, Position) > Vector3.Distance(B.CenterPosition, Position) ? 1 : -1;
            });
        }

        /// <summary>
        /// Add a new Block to list and Array
        /// </summary>
        public void AddBlock(ITerrainBlock Block, ushort ID)
        {
            lock (Blocks)
            {
                BlocksArray[Block.BlockId] = Block;
                Blocks.Add(Block);

                if (ID > MaxId)
                    MaxId = ID;
            }
        }

        /// <summary>
        /// Remove a Block from list and Array
        /// </summary>
        public bool RemoveBlock(ITerrainBlock Block, ushort ID)
        {
            lock (Blocks)
            {
                if (Blocks.Remove(Block))
                {
                    BlocksArray[Block.BlockId] = null;
                    return true;
                }
            }

            return false;
        }
    }
}
