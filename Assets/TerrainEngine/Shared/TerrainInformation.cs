﻿using FrameWork;
using System;
using System.Xml.Serialization;
using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public enum CompressionModes : int
    {
        NO_COMPRESSION = zlibConst.Z_NO_COMPRESSION,
        BEST_SPEED = zlibConst.Z_BEST_SPEED,
        BEST_COMPRESSION = zlibConst.Z_BEST_COMPRESSION,
    }

    [Serializable]
    public enum TerrainDataType : byte
    {
        TYPE_DATA = 1,
        TYPE_VOLUME_DATA = 3,
    }

    [Serializable]
    public enum VoxelSizes : byte
    {
        SIZE_4 = 4,
        SIZE_8 = 8,
        SIZE_16 = 16,
        SIZE_32 = 32,
        SIZE_64 = 64,
        SIZE_128 = 128,
    }

    [Serializable]
    public class TerrainInformation
    {
        public const float Isolevel = 0.5f;
        public const float Isofix = 0.499999f;

        public string Name = "New Terrain";
        public string FolderName = "New Folder";
        public DateTime CreatedTime = DateTime.Now;
        public TerrainDataType DataType = TerrainDataType.TYPE_VOLUME_DATA;
        public CompressionModes CompressionMode = CompressionModes.BEST_COMPRESSION;
        public bool LoadEnabled = false;
        public bool SaveEnabled = false;

        public string Seed = "0";
        private int _Seed = -1;
        public int IntSeed
        {
            get
            {
                if (_Seed <= 0)
                    _Seed = Math.Abs(Seed.GetHashCode()) + 1;

                return _Seed;
            }
        }
        public float Scale = 1;
        public int WorldGroundLevel = 0; // Unity position of ground level.
        public int LocalGroundLevel = 0; // Ground level in voxels. WorldGroundLevel / Scale.
        public Vector3 MinRange = new Vector3(-1, 0, -1);
        public Vector3 MaxRange = new Vector3(1, 0, 1);
        public Vector3 MinLimit = new Vector3(0, 0, 0);
        public Vector3 MaxLimit = new Vector3(0, 0, 0);
        public Vector3 DemiScale
        {
            get
            {
                return new Vector3(Scale * 0.5f, Scale * 0.5f, Scale * 0.5f);
            }
        }

        public VoxelSizes VoxelPerChunkXYZ = VoxelSizes.SIZE_8;
        public VoxelSizes ChunkPerBlockXYZ = VoxelSizes.SIZE_8;

        [HideInInspector]
        public bool ForceHierarchyUpdate = false;

        public void Init()
        {
            LocalGroundLevel = (int)(WorldGroundLevel / Scale);

            VoxelPerChunk = (int)VoxelPerChunkXYZ;
            ChunkPerBlock = (int)ChunkPerBlockXYZ;
            VoxelsPerAxis = VoxelPerChunk * ChunkPerBlock;
            TotalChunksPerBlock = ChunkPerBlock * ChunkPerBlock * ChunkPerBlock;
            TotalVoxelsPerBlock = (long)VoxelsPerAxis * (long)VoxelsPerAxis * (long)VoxelsPerAxis;

            ChunkSizeX = ((float)VoxelPerChunk + ChunkSizeOffset.x) * Scale;
            ChunkSizeY = ((float)VoxelPerChunk + ChunkSizeOffset.y) * Scale;
            ChunkSizeZ = ((float)VoxelPerChunk + ChunkSizeOffset.z) * Scale;
            BlockSizeX = ChunkSizeX * (float)ChunkPerBlock;
            BlockSizeY = ChunkSizeY * (float)ChunkPerBlock;
            BlockSizeZ = ChunkSizeZ * (float)ChunkPerBlock;

            WorldChunkSize = new Vector3(ChunkSizeX, ChunkSizeY, ChunkSizeZ);
            TotalVoxelsPerChunk = VoxelPerChunk * VoxelPerChunk * VoxelPerChunk;
        }

        [HideInInspector]
        public int VoxelPerChunk = 16; // Number of voxels per chunks (x*y*z)

        [HideInInspector]
        public int ChunkPerBlock = 16; // Number of chunk per (x*y*z)

        [HideInInspector]
        public int TotalVoxelsPerChunk;

        [NonSerialized, XmlIgnore]
        public int TotalChunksPerBlock;

        [NonSerialized, XmlIgnore]
        public long TotalVoxelsPerBlock;

        [NonSerialized, XmlIgnore]
        public int VoxelsPerAxis;

        [NonSerialized, XmlIgnore]
        public float ChunkSizeX;

        [NonSerialized, XmlIgnore]
        public float ChunkSizeY;

        [NonSerialized, XmlIgnore]
        public float ChunkSizeZ;

        [NonSerialized, XmlIgnore]
        public float BlockSizeX;

        [NonSerialized, XmlIgnore]
        public float BlockSizeY;

        [NonSerialized, XmlIgnore]
        public float BlockSizeZ;

        public Vector3 WorldBlockSize
        {
            get
            {
                return new Vector3(BlockSizeX, BlockSizeY, BlockSizeZ);
            }
        }

        [NonSerialized, XmlIgnore]
        public Vector3 ChunkSizeOffset = new Vector3(0, 0, 0);

        [NonSerialized]
        public Vector3 WorldChunkSize;

        public Bounds ChunkBounds
        {
            get
            {
                return new Bounds(WorldChunkSize * 0.5f, WorldChunkSize * 0.5f);
            }
        }

        public float MaxBlockSize
        {
            get
            {
                return Mathf.Max(Mathf.Max(BlockSizeX, BlockSizeY), BlockSizeZ);
            }
        }

        public float MaxChunkSize
        {
            get
            {
                return Mathf.Max(Mathf.Max(ChunkSizeX, ChunkSizeY), ChunkSizeZ);
            }
        }

        public float MinChunkSize
        {
            get
            {
                return Mathf.Min(Mathf.Min(ChunkSizeX, ChunkSizeY), ChunkSizeZ);
            }
        }

        public Vector3 GetStrictVoxelPosition(Vector3 WorldPosition)
        {
            float diffx = (WorldPosition.x % Scale);
            float diffy = (WorldPosition.y % Scale);
            float diffz = (WorldPosition.z % Scale);
            WorldPosition.x = WorldPosition.x - diffx;
            WorldPosition.y = WorldPosition.y - diffy;
            WorldPosition.z = WorldPosition.z - diffz;
            return WorldPosition;
        }

        public Vector3 GetBlockWorldPosition(VectorI3 LocalPosition)
        {
            return new Vector3((float)LocalPosition.x * BlockSizeX, (float)LocalPosition.y * BlockSizeY, (float)LocalPosition.z * BlockSizeZ);
        }

        public VectorI3 GetBlockFromWorldPosition(float WorldX,float WorldY,float WorldZ)
        {
            VectorI3 BlockPosition = new Vector3(WorldX, WorldY, WorldZ);
            BlockPosition.x = (int)(BlockPosition.x / BlockSizeX);
            BlockPosition.y = (int)(BlockPosition.y / BlockSizeY);
            BlockPosition.z = (int)(BlockPosition.z / BlockSizeZ);
            BlockPosition.x -= WorldX < 0 ? 1 : 0;
            BlockPosition.y -= WorldY < 0 ? 1 : 0;
            BlockPosition.z -= WorldZ < 0 ? 1 : 0;
            return BlockPosition;
        }

        public VectorI3 GetBlockFromWorldPosition(Vector3 WorldPosition)
        {
            VectorI3 BlockPosition = WorldPosition;
            BlockPosition.x = (int)(BlockPosition.x / BlockSizeX);
            BlockPosition.y = (int)(BlockPosition.y / BlockSizeY);
            BlockPosition.z = (int)(BlockPosition.z / BlockSizeZ);
            BlockPosition.x -= WorldPosition.x < 0 ? 1 : 0;
            BlockPosition.y -= WorldPosition.y < 0 ? 1 : 0;
            BlockPosition.z -= WorldPosition.z < 0 ? 1 : 0;
            return BlockPosition;
        }

        public VectorI3 GetBlockFromVoxelPosition(VectorI3 VoxelPositon)
        {
            VoxelPositon.x = (int)(VoxelPositon.x / VoxelsPerAxis);
            VoxelPositon.y = (int)(VoxelPositon.y / VoxelsPerAxis);
            VoxelPositon.z = (int)(VoxelPositon.z / VoxelsPerAxis);
            VoxelPositon.x -= VoxelPositon.x < 0 ? 1 : 0;
            VoxelPositon.y -= VoxelPositon.y < 0 ? 1 : 0;
            VoxelPositon.z -= VoxelPositon.z < 0 ? 1 : 0;
            return VoxelPositon;
        }

        public Vector3 GetBlockMiddlePosition(VectorI3 LocalPosition)
        {
            return
                new Vector3(LocalPosition.x * BlockSizeX, LocalPosition.y * BlockSizeY, LocalPosition.z * BlockSizeZ)
                + new Vector3(BlockSizeX / 2, BlockSizeY / 2, BlockSizeZ / 2);
        }

        public Vector3 GetVoxelWorldPosition(VectorI3 BlockPostion, int vx, int vy, int vz)
        {
            return GetBlockWorldPosition(BlockPostion) + GetVoxelWorldPosition(vx, vy, vz);
        }

        public Vector3 GetVoxelWorldPosition(int x, int y, int z)
        {
            return new Vector3(BlockSizeX * x * Scale / (VoxelsPerAxis * Scale), BlockSizeY * y * Scale / (VoxelsPerAxis * Scale), BlockSizeZ * z * Scale / (VoxelsPerAxis * Scale));
        }

        /// <summary>
        /// Return voxel position in world. Example : BlockPosition x = 1 and vx = 30 (inside block). Return BlockPosition*VoxelCountX+vx;
        /// </summary>
        public VectorI3 GetWorldVoxel(VectorI3 BlockPosition, int vx, int vy, int vz)
        {
            BlockPosition.x = BlockPosition.x * VoxelsPerAxis + vx;
            BlockPosition.y = BlockPosition.y * VoxelsPerAxis + vy;
            BlockPosition.z = BlockPosition.z * VoxelsPerAxis + vz;
            return BlockPosition;
        }

        public bool IsValidChunkPositon(VectorI3 LocalPosition)
        {
            if (LocalPosition.x < 0 || LocalPosition.x >= ChunkPerBlock)
                return false;

            if (LocalPosition.y < 0 || LocalPosition.y >= ChunkPerBlock)
                return false;

            if (LocalPosition.z < 0 || LocalPosition.z >= ChunkPerBlock)
                return false;

            return true;
        }

        public VectorI3 GetChunkGlobalPosition(Vector3 WorldPosition)
        {
            VectorI3 Result = new VectorI3(WorldPosition.x / ChunkSizeX, WorldPosition.y / ChunkSizeY, WorldPosition.z / ChunkSizeZ);
            if (WorldPosition.x < 0) Result.x -= 1;
            if (WorldPosition.y < 0) Result.y -= 1;
            if (WorldPosition.z < 0) Result.z -= 1;
            return Result; 
        }

        public Vector3 GetWorldVoxelPosition(Vector3 WorldPosition)
        {
            WorldPosition /= Scale;
            WorldPosition.x = (int)WorldPosition.x;
            WorldPosition.y = (int)WorldPosition.y;
            WorldPosition.z = (int)WorldPosition.z;
            WorldPosition *= Scale;
            return WorldPosition;
        }

        public override string ToString()
        {
            string Info = "TerrainInformations : \n";
            Info += "Name : " + Name + "\n";
            Info += "Seed : " + Seed + "\n";
            Info += "Scale : " + Scale + "\n";
            Info += "MinRange : " + MinRange + "\n";
            Info += "MaxRange : " + MaxRange + "\n";
            Info += "VoxelPerChunk : " + VoxelPerChunk + "," + VoxelPerChunk + "," + VoxelPerChunk + "\n";
            Info += "ChunkCount : " + ChunkPerBlock + "," + ChunkPerBlock + "," + ChunkPerBlock;
            return Info;
        }

        #region Datas

        [NonSerialized, XmlIgnore]
        public TerrainChunk[] Chunks;

        public void InitChunks()
        {
            if (Chunks == null)
            {
                Chunks = new TerrainChunk[TotalChunksPerBlock];

                int x, y, z, Index = 0;

                for (x = 0; x < ChunkPerBlock; ++x)
                {
                    for (y = 0; y < ChunkPerBlock; ++y)
                    {
                        for (z = 0; z < ChunkPerBlock; ++z)
                        {
                            Chunks[Index].LocalX = (byte)x;
                            Chunks[Index].LocalY = (byte)y;
                            Chunks[Index].LocalZ = (byte)z;
                            ++Index;
                        }
                    }
                }
            }
        }

        public delegate void OnDataCreateDelegate(TerrainDatas Data);

        [XmlIgnore]
        public OnDataCreateDelegate OnDataCreate;
        public PoolInfoRef TerrainDatasPool;
        public TerrainDatas GetData(VectorI3 Position)
        {
            if (TerrainDatasPool.Index == 0)
            {
                TerrainDatasPool = PoolManager.Pool.GetPool("TerrainDatas");
                PoolManager.Pools.array[TerrainDatasPool.Index].CanFreeMemory = false;
            }

            TerrainDatas Data = null;

            lock (this)
            {
                switch (DataType)
                {
                    case TerrainDataType.TYPE_DATA:
                        Data = TerrainDatasPool.GetData<TypeTerrainData>();
                        break;
                    case TerrainDataType.TYPE_VOLUME_DATA:
                        Data = TerrainDatasPool.GetData<TypeVolumeTerrainData>();
                        break;
                    default:
                        Debug.LogError("Invalid Data Type : Please set a Data Type in TerrainManager.Information");
                        Data = TerrainDatasPool.GetData<TypeVolumeTerrainData>();
                        break;
                }

                Data.Init(this, Position);

                if (OnDataCreate != null)
                    OnDataCreate(Data);
            }

            return Data;
        }

        public int GetChunkId(int ChunkX, int ChunkY, int ChunkZ)
        {
            return ChunkZ + ChunkY * ChunkPerBlock + ChunkX * ChunkPerBlock * ChunkPerBlock;
        }


        public int GetChunkIdFromVoxels(int x, int y, int z)
        {
            return (x / VoxelPerChunk) * ChunkPerBlock * ChunkPerBlock + (y / VoxelPerChunk) * ChunkPerBlock + (z / VoxelPerChunk);
        }

        public void GetChunkPositionFromChunkId(int ChunkId, ref int ChunkX, ref int ChunkY, ref int ChunkZ)
        {
            ChunkZ = ChunkPerBlock * ChunkPerBlock;

            ChunkX = (ChunkId / ChunkZ);
            ChunkId -= ChunkX * ChunkZ;

            ChunkY = (ChunkId / ChunkPerBlock);
            ChunkId -= ChunkY * ChunkPerBlock;
            ChunkZ = ChunkId;
        }

        public int GetVoxelFlatten(int x, int y, int z)
        {
            return (x % VoxelPerChunk) * VoxelPerChunk * VoxelPerChunk + (y % VoxelPerChunk) * VoxelPerChunk + (z % VoxelPerChunk);
        }

        #endregion
    }
}