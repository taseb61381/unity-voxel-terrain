﻿using System;
using System.IO;

namespace FrameWork
{
    static public class ZlibMgr
    {
        static public byte[] GetResult(byte[] Input, bool compress)
        {
            return compress ? Compress(Input,zlibConst.Z_BEST_COMPRESSION, zlibConst.Z_NO_FLUSH) : Decompress(Input);
        }

        static public byte[] Compress(byte[] Input,int Compression,int Flush)
        {
            byte[] Result = null;

            using (MemoryStream OutPut = new MemoryStream())
            {
                using(ZOutputStream ZStream = new ZOutputStream(OutPut, Compression))
                {
                    ZStream.FlushMode = Flush;
                    Process(ZStream, Input);
                    Result = OutPut.ToArray();
                }

                OutPut.Close();
            }

            return Result;
        }

        static public byte[] Decompress(byte[] Input)
        {
            byte[] Result = null;

            using (MemoryStream OutPut = new MemoryStream())
            {
                using(ZOutputStream ZStream = new ZOutputStream(OutPut, true))
                {
                    Process(ZStream, Input);
                    Result = OutPut.ToArray();
                }
                OutPut.Close();
            }

            return Result;
        }

        static private void Process(ZOutputStream ZStream,byte[] Input)
        {
            try
            {
                ZStream.Write(Input, 0, Input.Length);
                ZStream.Flush();
                ZStream.Close();
            }
            catch (Exception e)
            {
                GeneralLog.LogError("Zlib - Process Error : " + e.ToString());
            }
        }
    }
}
