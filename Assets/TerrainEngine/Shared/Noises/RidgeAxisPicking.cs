using System.Collections.Generic;
using UnityEngine;

public class RidgeAxisPicking
{
    //--Member varibles----------------------------------------------------------------------------------------------------------------------------------------------------------

    public class HeightMap
    {
        public float[][] Heights;
        public RidgeMap m_isRidge;
        public DeadEndMap m_isDeadEnd;
        public ConnectionMap m_connectionTable;

        public int Width;
        public int Height;
        public bool Flip;
        public float WaterLevel;

        public void Init(int Width, int Height, bool Flip)
        {
            this.Width = Width;
            this.Height = Height;
            this.Flip = Flip;

            Heights = new float[Width][];
            for (int i = 0; i < Width; ++i)
                Heights[i] = new float[Height];

            m_isRidge = new RidgeMap(Width, Height);
            m_isDeadEnd = new DeadEndMap(Width, Height);
            m_connectionTable = new ConnectionMap(Width, Height, (int)RidgeAxisPicking.CONNECTION.END);
        }

        public void Load(Texture2D Texture, bool Flip)
        {
            Init(Texture.width, Texture.height, Flip);
            int x, y;
            float ht;

            //The RidgeAxisPicking calculate function needs the heights as a 2d array of floats
            for (x = 0; x < Width; x++)
            {
                for (y = 0; y < Height; y++)
                {
                    ht = Texture.GetPixel(x, y).r;

                    if (Flip) ht *= -1.0f; //If you want to extract vallys instead of ridges just flip heights

                    this[x, y] = ht;
                }
            }
        }

        public void Load(LibNoise.IModule Noise,double WorldX,double WorldZ, bool Flip, int Width,int Height, double Scale)
        {
            Init(Width, Height, Flip);
            float MaxValue = int.MinValue;
            float MinValue = int.MaxValue;
            float Dens = 0;
            int x, z;

            for (x = 0; x < Width; ++x)
            {
                for (z = 0; z < Height; ++z)
                {
                    Dens = (float)Noise.GetValue(WorldX + ((double)x * Scale), WorldZ + ((double)z * Scale));
                    Heights[x][z] = Dens;

                    if (Dens < MinValue) MinValue = Dens;
                    else if (Dens > MaxValue) MaxValue = Dens;
                }
            }

            WaterLevel = (1f / Mathf.Abs(MinValue - MaxValue)) * (-MinValue);
            MinValue = Mathf.Abs(MinValue);
            MaxValue = Mathf.Abs(MaxValue);
            for (x = 0; x < Width; ++x)
            {
                for (z = 0; z < Height; ++z)
                {
                    Dens = (Heights[x][z] + MinValue) / (MaxValue + MinValue);
                    if (Flip) Dens *= -1.0f;

                    Heights[x][z] = Dens;
                }
            }
            
        }

        public float this[int x, int y]
        {
            get
            {
                return Heights[x][y];
            }
            set
            {
                Heights[x][y] = value;
            }
        }
    }

    public class RidgeMap
    {
        bool[][] m_isRidge;

        public RidgeMap(int Width, int Height)
        {
            m_isRidge = new bool[Width][];
            for (int i = 0; i < Width; ++i)
                m_isRidge[i] = new bool[Height];
        }

        public bool this[int x, int y]
        {
            get
            {
                return m_isRidge[x][y];
            }
            set
            {
                m_isRidge[x][y] = value;
            }
        }
    }

    public class DeadEndMap
    {
        bool[][] m_isDeadEnd;

        public DeadEndMap(int Width, int Height)
        {
            m_isDeadEnd = new bool[Width][];
            for (int i = 0; i < Width; ++i)
                m_isDeadEnd[i] = new bool[Height];
        }

        public bool this[int x, int y]
        {
            get
            {
                return m_isDeadEnd[x][y];
            }
            set
            {
                m_isDeadEnd[x][y] = value;
            }
        }
    }

    public class ConnectionMap
    {
        bool[][][] m_connectionTable;

        public ConnectionMap(int Width, int Height, int Depth)
        {
            m_connectionTable = new bool[Width][][];
            for (int i = 0; i < Width; ++i)
            {
                m_connectionTable[i] = new bool[Height][];
                for (int j = 0; j < Height; ++j)
                    m_connectionTable[i][j] = new bool[Depth];
            }
        }

        public bool this[int x, int y, int z]
        {
            get
            {
                return m_connectionTable[x][y][z];
            }
            set
            {
                m_connectionTable[x][y][z] = value;
            }
        }
    }

    int m_width, m_height, m_checkCount;
    public int m_profileLength = 1, m_branchReduction = 2;
    public float MaxHeight = float.MaxValue; // Upper, Ridge are not created ( height[x,y] )
    public float MinHeight = float.MinValue;
    public bool BreakPolygon =true;
    public void SetProfileLength(int profileLength) { m_profileLength = profileLength; }
    public void SetBranchReduction(int branchReduction) { m_branchReduction = branchReduction; }

    static int[,] m_offsets = new int[,] { { 1, 0 }, { 1, 1 }, { 0, 1 }, { -1, 1 }, { -1, 0 }, { -1, -1 }, { 0, -1 }, { 1, -1 } };

    public enum CONNECTION { RIGHT = 0, RIGHT_TOP = 1, TOP = 2, LEFT_TOP = 3, LEFT = 4, LEFT_BOTTOM = 5, BOTTOM = 6, RIGHT_BOTTOM = 7, END = 8 };

    public enum HALF_CONNECTION { RIGHT = 0, RIGHT_TOP = 1, TOP = 2, LEFT_TOP = 3, END = 4 };

    static int[] OPP_CONNECTION = new int[] { 4, 5, 6, 7, 0, 1, 2, 3 };

    public HeightMap m_htmap;
    float[,] heights;
    int[,] beenChecked;

    public List<Segment> sortedSegments = new List<Segment>();
    public List<RidgeAxisPicking.Segment> segments = new List<Segment>();

    //--Helper class for segments---------------------------------------------------------------------------------------------------------------------------------------------------

    public class Segment : System.IComparable
    {
        public float ht;
        public bool dead, reliable;
        public int type, aX, aY, bX, bY;

        public Segment(int _type, int _ax, int _ay, int _bx, int _by, float _ht)
        {
            ht = _ht;
            type = _type;
            aX = _ax;
            aY = _ay;
            bX = _bx;
            bY = _by;
        }

        //Used to sort a array of segements by height
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Segment seg = obj as Segment;
            if (seg != null)
            {
                if (ht == seg.ht)
                {
                    //If both segements have the same height and one is marked as reliable than it should be ranked higher
                    if (reliable && !seg.reliable) return 1;
                    else if (!reliable && seg.reliable) return -1;
                }

                return ht.CompareTo(seg.ht);
            }
            else
                throw new System.ArgumentException("RidgeAxisPicking::Segment::CompareTo - Object is not a Segment");
        }
    };

    //--Utility Functions----------------------------------------------------------------------------------------------------------------------------------------------------------
    #region Utility

    bool InBounds(int x, int y)
    {
        if (x < 0 || x >= m_width || y < 0 || y >= m_height) return false;
        return true;
    }

    void ClampToBounds(ref int x, ref int y)
    {
        if (x < 0) x = 0;
        if (x >= m_width) x = m_width - 1;
        if (y < 0) y = 0;
        if (y >= m_height) y = m_height - 1;
    }

    bool IsConnection(int x, int y, int i)
    {
        if (m_htmap.m_connectionTable[x, y, i]) return true;
        if (m_htmap.m_connectionTable[x + m_offsets[i, 0], y + m_offsets[i, 1], OPP_CONNECTION[i]]) return true;
        return false;
    }

    bool IsConnectionWithBoundsCheck(int x, int y, int i)
    {
        if (!InBounds(x, y) || !InBounds(x + m_offsets[i, 0], y + m_offsets[i, 1])) return false;
        if (m_htmap.m_connectionTable[x, y, i]) return true;
        if (m_htmap.m_connectionTable[x + m_offsets[i, 0], y + m_offsets[i, 1], OPP_CONNECTION[i]]) return true;
        return false;
    }

    float GetAverageHeight(int x, int y, int i)
    {
        return m_htmap[x, y] + m_htmap[x + m_offsets[i, 0], y + m_offsets[i, 1]];
    }

    bool GetAverageHeightWithBoundsCheck(int x, int y, int i, ref float ht)
    {
        if (!InBounds(x + m_offsets[i, 0], y + m_offsets[i, 1]) || !InBounds(x, y)) return false;

        ht = m_htmap[x, y] + m_htmap[x + m_offsets[i, 0], y + m_offsets[i, 1]];

        return true;
    }

    void BreakConnection(int x, int y, int i)
    {
        m_htmap.m_connectionTable[x, y, i] = false;
        m_htmap.m_connectionTable[x + m_offsets[i, 0], y + m_offsets[i, 1], OPP_CONNECTION[i]] = false;
    }

    void BreakConnectionWithBoundsCheck(int x, int y, int i)
    {
        if (!InBounds(x, y) || !InBounds(x + m_offsets[i, 0], y + m_offsets[i, 1])) return;
        m_htmap.m_connectionTable[x, y, i] = false;
        m_htmap.m_connectionTable[x + m_offsets[i, 0], y + m_offsets[i, 1], OPP_CONNECTION[i]] = false;
    }

    #endregion

    //--Profile recognition--------------------------------------------------------------------------------------------------------------------------------------------------------
    #region Profile

    void SampleHeights(float[,] heights, int x, int y)
    {
        int i, j, idxX, idxY;

        //This will sample the heights around a pixel need to identify if its a ridge candidate
        //For example if m_profileLength is 2 the array is structured like this

        //+2 +1 -1 -2 (2 pos to the right, 1 pos to the right, 1 pos to the left, 2 pos to the left)
        //+2 +1 -1 -2 (Same as above but from right top to left bottom)
        //+2 +1 -1 -2 (Same as above but from top to bottom)
        //+2 +1 -1 -2 (Same as above but from left top to right bottom)

        for (i = 0; i < (int)HALF_CONNECTION.END; i++) //only need to loop from RIGHT to LEFT_BOTTOM
        {
            for (j = 0; j < m_profileLength; j++)
            {
                idxX = x + (j - m_profileLength) * m_offsets[i, 0];
                idxY = y + (j - m_profileLength) * m_offsets[i, 1];

                ClampToBounds(ref idxX, ref idxY); //could go out of bounds at edge of htmap so clamp

                heights[i, j] = m_htmap[idxX, idxY];

                idxX = x + (j + 1) * m_offsets[i, 0];
                idxY = y + (j + 1) * m_offsets[i, 1];

                ClampToBounds(ref idxX, ref idxY);

                heights[i, j + m_profileLength] = m_htmap[idxX, idxY];

            }
        }
    }

    bool IsRidge(float[,] heights, float ht)
    {
        if (ht > MaxHeight || ht < MinHeight)
            return false;

        int i, j;
        float ht0, ht1;
        bool b0, b1;

        //This will identify if a pixel should be regarded as a potental ridge
        //A pixel is a ridge candidate if there are lower heights on any axis
        //See the SampleHeights function for more info on how the heights array is structured

        for (i = 0; i < (int)HALF_CONNECTION.END; i++) //only need to loop from RIGHT to LEFT_BOTTOM
        {
            b0 = false;
            b1 = false;

            for (j = 0; j < m_profileLength; j++)
            {
                ht0 = heights[i, j]; //height on one side of pixel
                ht1 = heights[i, j + m_profileLength]; //height on other side of pixel

                if (ht0 < ht) b0 = true;
                if (ht1 < ht) b1 = true;

                if (b0 && b1) return true; //If both lower that ht of pixel this is a ridge candidate
            }
        }

        return false;
    }

    void MakeConnections(int x, int y)
    {
        //If a neigbour pixel and this pixel are both ridge candidates make a connection between them
        //The connection table will be used to make the segments later
        //The x,y positions passed to this function is the position of a pixel thats known to be a ridge 
        //so all we need to do is check if a neighbour is also a ridge

        for (int i = 0; i < (int)CONNECTION.END; i++)
        {
            int idxX = x + m_offsets[i, 0];
            int idxY = y + m_offsets[i, 1];

            if (!InBounds(idxX, idxY))
                m_htmap.m_connectionTable[x, y, i] = false;
            else
            {
                //if the neighbour pixel is a ridge and it is not already connected to this pixel make a connection
                if (m_htmap.m_isRidge[idxX, idxY] && !m_htmap.m_connectionTable[idxX, idxY, OPP_CONNECTION[i]])
                {
                    m_htmap.m_connectionTable[x, y, i] = true;
                }
            }
        }
    }

    void ProfileRecognition()
    {
        //This function will identify all possible ridge candidates and connect them together

        int x, y;


        for (x = 0; x < m_width; x++)
        {
            for (y = 0; y < m_height; y++)
            {
                //Find the heights needed from neigbour pixels
                SampleHeights(heights, x, y);
                //Determine from the heights if this is a ridge candidate
                m_htmap.m_isRidge[x, y] = IsRidge(heights, m_htmap[x, y]);
            }
        }

        for (x = 0; x < m_width; x++)
        {
            for (y = 0; y < m_height; y++)
            {
                if (m_htmap.m_isRidge[x, y])
                {
                    //If this is a ridge make a connection with any neighbours that are also ridges
                    MakeConnections(x, y);
                }
            }
        }
    }

    //--Profile optimisation--------------------------------------------------------------------------------------------------------------------------------------------------------

    void RemoveCrosses(int x, int y)
    {
        //After the profile regconition stage its possible to have a connection between four neighbour pixels that makes a cross. Remove this
        if (IsConnection(x, y, (int)CONNECTION.RIGHT_TOP) && IsConnection(x + 1, y, (int)CONNECTION.LEFT_TOP))
        {
            float ht0 = GetAverageHeight(x, y, (int)CONNECTION.RIGHT_TOP);
            float ht1 = GetAverageHeight(x + 1, y, (int)CONNECTION.LEFT_TOP);
            //Remove the segment with the lowest height
            if (ht0 < ht1)
                BreakConnection(x, y, (int)CONNECTION.RIGHT_TOP);
            else
                BreakConnection(x + 1, y, (int)CONNECTION.LEFT_TOP);
        }
    }

    void RemoveStraightParallel(int x, int y)
    {
        //This step tries to reduce the number of connections that are unlikley to be valid ridges by checking
        //if there is a connection between this and the pixel to the right (which will make a segment) and also the same for the pixel above and below
        //If this is the case you will end up will three parrallel segments and if the middle one has a higher height the other two should be removed
        if (IsConnection(x, y, (int)CONNECTION.RIGHT) && IsConnection(x, y + 1, (int)CONNECTION.RIGHT) && IsConnection(x, y - 1, (int)CONNECTION.RIGHT))
        {
            float ht0 = GetAverageHeight(x, y, (int)CONNECTION.RIGHT); //The height of this segment
            float ht1 = GetAverageHeight(x, y + 1, (int)CONNECTION.RIGHT); //The height of the segment above
            float ht2 = GetAverageHeight(x, y - 1, (int)CONNECTION.RIGHT); //The height of the segment below
            //If the middle segment is higher than both remove the connections
            if (ht0 > ht1 && ht0 > ht2)
            {
                BreakConnection(x, y + 1, (int)CONNECTION.RIGHT);
                BreakConnection(x, y - 1, (int)CONNECTION.RIGHT);
            }
        }

        //Same as above but on vertcial connections

        if (IsConnection(x, y, (int)CONNECTION.TOP) && IsConnection(x + 1, y, (int)CONNECTION.TOP) && IsConnection(x - 1, y, (int)CONNECTION.TOP))
        {
            float ht0 = GetAverageHeight(x, y, (int)CONNECTION.TOP);
            float ht1 = GetAverageHeight(x + 1, y, (int)CONNECTION.TOP);
            float ht2 = GetAverageHeight(x - 1, y, (int)CONNECTION.TOP);

            if (ht0 > ht1 && ht0 > ht2)
            {
                BreakConnection(x + 1, y, (int)CONNECTION.TOP);
                BreakConnection(x - 1, y, (int)CONNECTION.TOP);
            }
        }
    }

    void RemoveDiagonalParallel(int x, int y)
    {
        //Works the same as RemoveStraightParallel() function but on diagonal segments

        if (IsConnection(x, y, (int)CONNECTION.RIGHT_TOP) && IsConnection(x - 1, y, (int)CONNECTION.RIGHT_TOP) && IsConnection(x, y - 1, (int)CONNECTION.RIGHT_TOP))
        {
            float ht0 = GetAverageHeight(x, y, (int)CONNECTION.RIGHT_TOP);
            float ht1 = GetAverageHeight(x - 1, y, (int)CONNECTION.RIGHT_TOP);
            float ht2 = GetAverageHeight(x, y - 1, (int)CONNECTION.RIGHT_TOP);

            if (ht0 > ht1 && ht0 > ht2)
            {
                BreakConnection(x - 1, y, (int)CONNECTION.RIGHT_TOP);
                BreakConnection(x, y - 1, (int)CONNECTION.RIGHT_TOP);
            }
        }

        if (IsConnection(x, y, (int)CONNECTION.LEFT_TOP) && IsConnection(x + 1, y, (int)CONNECTION.LEFT_TOP) && IsConnection(x, y - 1, (int)CONNECTION.LEFT_TOP))
        {
            float ht0 = GetAverageHeight(x, y, (int)CONNECTION.LEFT_TOP);
            float ht1 = GetAverageHeight(x + 1, y, (int)CONNECTION.LEFT_TOP);
            float ht2 = GetAverageHeight(x, y - 1, (int)CONNECTION.LEFT_TOP);

            if (ht0 > ht1 && ht0 > ht2)
            {
                BreakConnection(x + 1, y, (int)CONNECTION.LEFT_TOP);
                BreakConnection(x, y - 1, (int)CONNECTION.LEFT_TOP);
            }
        }

    }

    void ProfileOptimisation()
    {
        //This will try and reduve the number of connections made that are unlikely to produce valid segments

        int x, y;

        for (x = 0; x < m_width - 1; x++)
        {
            for (y = 0; y < m_height - 1; y++)
            {
                if (m_htmap.m_isRidge[x, y])
                {
                    RemoveCrosses(x, y);
                }
            }
        }

        for (x = 1; x < m_width - 1; x++)
        {
            for (y = 1; y < m_height - 1; y++)
            {
                if (m_htmap.m_isRidge[x, y])
                {
                    RemoveStraightParallel(x, y);
                }
            }
        }

        for (x = 2; x < m_width - 2; x++)
        {
            for (y = 2; y < m_height - 2; y++)
            {
                if (m_htmap.m_isRidge[x, y])
                {
                    RemoveDiagonalParallel(x, y);
                }
            }
        }



    }

    #endregion
    //--Polygon breaking-------------------------------------------------------------------------------------------------------------------------------------------------------------

    #region Op
    bool IsClosedPolygon(int dontCheck, int tarX, int tarY, int x, int y, int[,] beenChecked)
    {
        //This function will continue to trace along each connected pixel untill the starting point (tarX,tarY) is reached or there are no more 
        //connections to check. If a the function can trace back to the starting point then the segment at position tarX,tarY must be part of a closed polygon and should be culled

        //As this function is recursive (it calls its self) a stack over flow can occur if you try and process a large height map or set m_profileLength to high
        //This is this best way I can think of at the moment

        if (!InBounds(x, y)) return false;
        //If the position has already been check for the current round (checkCount) then exit
        if (beenChecked[x, y] == m_checkCount) return false;
        //If hit a dead end return
        if (m_htmap.m_isDeadEnd[x, y]) return false;

        beenChecked[x, y] = m_checkCount;
        //Found target, return true
        if (tarX == x && tarY == y) return true;

        int idxX, idxY;
        //If there are no connections at this position a dead end must have been reached
        //Mark it as a dead end and it will not be traced again
        bool deadEnd = true;

        for (int i = 0; i < (int)CONNECTION.END; i++)
        {
            idxX = x + m_offsets[i, 0];
            idxY = y + m_offsets[i, 1];

            if (InBounds(idxX, idxY))
            {
                //If there is a connection at neighbour and its not a dead end and its not the neigbour that we came from check it
                if (IsConnection(x, y, i) && i != dontCheck && !m_htmap.m_isDeadEnd[idxX, idxY])
                {
                    deadEnd = false; //If were in here this position cant be a dead end

                    if (IsClosedPolygon(OPP_CONNECTION[i], tarX, tarY, idxX, idxY, beenChecked)) return true;
                }
            }
        }

        m_htmap.m_isDeadEnd[x, y] = deadEnd;

        return false;
    }

    void PolygonBreaking(List<Segment> sortedSegments)
    {
        //This function will remove all closed polygons (A series of segments that loops back on its self) and 
        //reduce the segments to a series of lines. The segments have been sorted from lowest to highest so the segments less likely to be valid are remove first

        m_checkCount = 1;

        Segment seg;
        for (int i = 0; i < sortedSegments.Count; i++)
        {
            seg = sortedSegments[i];

            if (IsClosedPolygon(OPP_CONNECTION[seg.type], seg.aX, seg.aY, seg.bX, seg.bY, beenChecked))
            {
                BreakConnection(seg.aX, seg.aY, seg.type); // remove from connection table
                seg.dead = true; //mark as dead, will be rmoved later
            }

            m_checkCount++;
        }


    }

    //--Sort segments-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    void CheckIfReliable(Segment seg)
    {
        //This function will find if a segment is reliable. A reliable segment is defined as a segment that is higher than the heights parrallel to it
        //A reliable segment is more likely to be a valid ridge candiate.
        float ht0, ht1 = 0.0f, ht2 = 0.0f;

        if (seg.type == (int)CONNECTION.RIGHT || seg.type == (int)CONNECTION.LEFT)
        {
            ht0 = seg.ht;
            if (!GetAverageHeightWithBoundsCheck(seg.aX, seg.aY + 1, seg.type, ref ht1)) ht1 = ht0;
            if (!GetAverageHeightWithBoundsCheck(seg.aX, seg.aY - 1, seg.type, ref ht2)) ht2 = ht0;
            if (ht0 > ht1 && ht0 > ht2) seg.reliable = true;
        }
        else if (seg.type == (int)CONNECTION.TOP || seg.type == (int)CONNECTION.BOTTOM)
        {
            ht0 = seg.ht;
            if (!GetAverageHeightWithBoundsCheck(seg.aX + 1, seg.aY, seg.type, ref ht1)) ht1 = ht0;
            if (!GetAverageHeightWithBoundsCheck(seg.aX - 1, seg.aY, seg.type, ref ht2)) ht2 = ht0;
            if (ht0 > ht1 && ht0 > ht2) seg.reliable = true;
        }
        else if (seg.type == (int)CONNECTION.RIGHT_TOP || seg.type == (int)CONNECTION.LEFT_BOTTOM)
        {
            ht0 = seg.ht;
            if (!GetAverageHeightWithBoundsCheck(seg.aX, seg.aY + 1, seg.type, ref ht1)) ht1 = ht0;
            if (!GetAverageHeightWithBoundsCheck(seg.aX + 1, seg.aY, seg.type, ref ht2)) ht2 = ht0;
            if (ht0 > ht1 && ht0 > ht2) seg.reliable = true;
        }
        else if (seg.type == (int)CONNECTION.LEFT_TOP || seg.type == (int)CONNECTION.RIGHT_BOTTOM)
        {
            ht0 = seg.ht;
            if (!GetAverageHeightWithBoundsCheck(seg.aX, seg.aY + 1, seg.type, ref ht1)) ht1 = ht0;
            if (!GetAverageHeightWithBoundsCheck(seg.aX - 1, seg.aY, seg.type, ref ht2)) ht2 = ht0;
            if (ht0 > ht1 && ht0 > ht2) seg.reliable = true;
        }

    }

    void SortSegements(List<Segment> sortedSegments)
    {
        //This function converts the connections in the connection table to segments
        int i, x, y, idxX, idxY;
        float ht;
        Segment seg;
        for (x = 0; x < m_width; x++)
        {
            for (y = 0; y < m_height; y++)
            {
                if (m_htmap.m_isRidge[x, y])
                {
                    for (i = 0; i < 8; i++)
                    {
                        idxX = x + m_offsets[i, 0];
                        idxY = y + m_offsets[i, 1];

                        if (m_htmap.m_connectionTable[x, y, i])
                        {
                            ht = m_htmap[x, y] + m_htmap[idxX, idxY]; //a segemnts height is the height at booth ends

                            seg = new Segment(i, x, y, idxX, idxY, ht);

                            CheckIfReliable(seg);

                            sortedSegments.Add(seg);
                        }
                    }
                }
            }
        }

        //Sorts the segments by height (lowest to highest) This is needed for the polygon breaking stage
        sortedSegments.Sort();
    }

    //--Branch reduction-----------------------------------------------------------------------------------------------------------------------------------------------------------

    void BranchReduction(List<Segment> segments)
    {
        //This function will reduce the number of end segments to produce a cleaner result
        //It is broken into to two parts. The first will just trim of a predefined number of end segments (m_branchReduction)
        //The second will continue to trim of end segments untill a reliable segment is hit


        int i, j, k, countA, countB;

        for (k = 0; k < m_branchReduction; k++)
        {
            for (i = 0; i < segments.Count; i++)
            {
                Segment seg = segments[i];

                if (!seg.dead)
                {
                    countA = 0; countB = 0;

                    for (j = 0; j < (int)CONNECTION.END; j++)
                    {
                        if (IsConnectionWithBoundsCheck(seg.aX, seg.aY, j)) countA++;
                        if (IsConnectionWithBoundsCheck(seg.bX, seg.bY, j)) countB++;
                    }

                    //If there is only one connection at either end of this segment it must be a end segment, mark as dead
                    if (countA == 1 || countB == 1) seg.dead = true;
                }
            }

            //Break all the connections at dead segments
            for (i = 0; i < segments.Count; i++)
            {
                Segment seg = segments[i];
                if (seg.dead) BreakConnectionWithBoundsCheck(seg.aX, seg.aY, seg.type);
            }
        }

        bool stop = false;
        int limit = 0;

        //while there are end segments that dont end in a reliable segment keep trimming unless the limit is hit to stop the while loop going on to long
        while (!stop && limit < 1000)
        {
            int count = 0;
            limit++;

            for (i = 0; i < segments.Count; i++)
            {
                Segment seg = segments[i];

                if (!seg.dead && !seg.reliable) //if this segemnt is not dead and its not reliable check if its a end segment
                {
                    countA = 0; countB = 0;

                    for (j = 0; j < (int)CONNECTION.END; j++)
                    {
                        if (IsConnectionWithBoundsCheck(seg.aX, seg.aY, j)) countA++;
                        if (IsConnectionWithBoundsCheck(seg.bX, seg.bY, j)) countB++;
                    }

                    //If there is only one connection at either end of this segment it must be a end segment, mark as dead
                    if (countA == 1 || countB == 1)
                    {
                        count++;
                        seg.dead = true;
                    }
                }
            }

            //Break all the connections at dead segments
            for (i = 0; i < segments.Count; i++)
            {
                Segment seg = segments[i];
                if (seg.dead) BreakConnectionWithBoundsCheck(seg.aX, seg.aY, seg.type);
            }

            //Didnt find anything to trim on this run, exit
            if (count == 0) stop = true;
        }



    }

    void RemoveShortSegments(List<Segment> segments)
    {
        //This function will remove any single segments not connected to anything else

        int i, j, count;
        Segment seg;
        for (i = 0; i < segments.Count; i++)
        {
            seg = segments[i];

            if (!seg.dead)
            {
                count = 0; ;

                for (j = 0; j < (int)CONNECTION.END; j++)
                {
                    //if this is a single segment there should only be a single connection at each end
                    if (IsConnectionWithBoundsCheck(seg.aX, seg.aY, j) || IsConnectionWithBoundsCheck(seg.bX, seg.bY, OPP_CONNECTION[j])) count++;
                }

                if (count == 1) seg.dead = true;
            }
        }

        for (i = 0; i < segments.Count; i++)
        {
            seg = segments[i];
            if (seg.dead) BreakConnectionWithBoundsCheck(seg.aX, seg.aY, seg.type);
        }

    }

    #endregion
    //--Entry------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void Calculate()
    {
        Clean();

        ProfileRecognition();

        ProfileOptimisation();

        SortSegements(sortedSegments);

        if(BreakPolygon)
            PolygonBreaking(sortedSegments);

        BranchReduction(sortedSegments);

        RemoveShortSegments(sortedSegments);

        //A large number of sorted segment will have been marked as dead and should not be returned
        for (int i = 0; i < sortedSegments.Count; i++)
        {
            Segment seg = sortedSegments[i];
            if (!seg.dead) segments.Add(seg);
        }
    }

    public void DrawToArray(float[,] Array, int SizeX,int SizeY, int Scale)
    {
        int i, idxX, idxY;
        int x, y;

        RidgeAxisPicking.Segment seg;
        //Draw the lines
        int Mid = Scale / 2;
        for (i = 0; i < segments.Count; i++)
        {
            seg = segments[i];

            for (x = -Mid; x <= Mid; ++x)
            {
                for (y = -Mid; y <= Mid; ++y)
                {
                    idxX = (seg.aX * Scale) + x;
                    idxY = (seg.aY * Scale) + y;
                    if (idxX < SizeX && idxY < SizeY && idxX > -1 && idxY > -1)
                    {
                        Array[idxX, idxY] = -1f;
                    }
                }
            }
        }
    }

    public void DrawToTexture(Texture2D Texture, Color Col, int Scale)
    {
        int i, j, idxX, idxY;

        RidgeAxisPicking.Segment seg;
        //Draw the lines
        for (i = 0; i < segments.Count; i++)
        {
            seg = segments[i];

            for (j = 0; j < Scale; j++)
            {
                idxX = seg.aX * Scale + Scale / 2 + (seg.bX - seg.aX) * j;
                idxY = seg.aY * Scale + Scale / 2 + (seg.bY - seg.aY) * j;

                Texture.SetPixel(idxX, idxY, Col);
            }
        }
    }

    public void DrawHeightMap(ref Texture2D Texture, LibNoise.IModule Noise, int SizeX,int SizeY, int WorldX,int WorldZ, double Scale)
    {
        if (Texture == null)
            Texture = new Texture2D(SizeX, SizeY);

        float MaxValue = int.MinValue;
        float MinValue = int.MaxValue;
        float[,] Density = new float[SizeX, SizeY];
        float Dens = 0;

        BiomeData Modules = null;
        int x, z;
        for (x = 0; x < SizeX; ++x)
        {
            for (z = 0; z < SizeY; ++z)
            {
                Dens = (float)Noise.GetValue(x * Scale + WorldX, z * Scale + WorldZ, Modules);
                Density[x, z] = Dens;
                if (Dens < MinValue) MinValue = Dens;
                else if (Dens > MaxValue) MaxValue = Dens;
            }
        }

        MinValue = Mathf.Abs(MinValue);
        MaxValue = Mathf.Abs(MaxValue);
        Color col = new Color();
        col.a = 1;
        for (x = 0; x < SizeX; ++x)
        {
            for (z = 0; z < SizeY; ++z)
            {
                Dens = (Density[x, z] + MinValue) / (MaxValue + MinValue);
                col.r = col.g = col.b = Dens;
                if (Density[x, z] < 0)
                    col.b = 1f;

                Texture.SetPixel(x, z, col);
            }
        }
    }

    public void Clean()
    {
        m_width = m_htmap.Width;
        m_height = m_htmap.Height;
        segments.Clear();
        sortedSegments.Clear();
        int x, y;
        if (heights == null)
        {
            heights = new float[(int)HALF_CONNECTION.END, m_profileLength * 2];
            beenChecked = new int[m_width, m_height];
        }
        else
        {
            for (x = 0; x < (int)HALF_CONNECTION.END; ++x)
            {
                for (y = 0; y < m_profileLength * 2; ++y)
                {
                    heights[x, y] = 0;
                }
            }

            for (x = 0; x < m_width; ++x)
            {
                for (y = 0; y < m_height; ++y)
                {
                    beenChecked[x, y] = 0;
                }
            }
        }
    }
}
