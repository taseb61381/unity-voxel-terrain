﻿
using UnityEngine;

namespace TerrainEngine
{
    public class PerlinNoise
    {
        private static float LACUNARITY = 2.1379201f;
        private static float H = 0.836281f;

        private float[] _spectralWeights;

        private int[] _noisePermutations;
        private bool _recomputeSpectralWeights = true;
        private int _octaves = 9;

        public PerlinNoise(int seed)
        {
            FastRandom rand = new FastRandom(seed);

            _noisePermutations = new int[512];
            int[] _noiseTable = new int[256];

            for (int i = 0; i < 256; i++)
                _noiseTable[i] = i;

            for (int i = 0; i < 256; i++)
            {
                int j = rand.randomInt() % 256;
                j = (j < 0) ? -j : j;

                int swap = _noiseTable[i];
                _noiseTable[i] = _noiseTable[j];
                _noiseTable[j] = swap;
            }

            for (int i = 0; i < 256; i++)
                _noisePermutations[i] = _noisePermutations[i + 256] = _noiseTable[i];

        }

        private int fastfloor(float x)
        {
            return x > 0 ? (int)x : (int)x - 1;
        } 

        public float noise(float x, float y, float z)
        {
            int X = fastfloor(x) & 255, Y = fastfloor(y) & 255, Z = fastfloor(z) & 255;

            x -= fastfloor(x);
            y -= fastfloor(y);
            z -= fastfloor(z);

            float u = fade(x), v = fade(y), w = fade(z);
            int A = _noisePermutations[X] + Y, AA = _noisePermutations[A] + Z, AB = _noisePermutations[(A + 1)] + Z,
                    B = _noisePermutations[(X + 1)] + Y, BA = _noisePermutations[B] + Z, BB = _noisePermutations[(B + 1)] + Z;

            return lerp(w, lerp(v, lerp(u, grad(_noisePermutations[AA], x, y, z),
                    grad(_noisePermutations[BA], x - 1, y, z)),
                    lerp(u, grad(_noisePermutations[AB], x, y - 1, z),
                            grad(_noisePermutations[BB], x - 1, y - 1, z))),
                    lerp(v, lerp(u, grad(_noisePermutations[(AA + 1)], x, y, z - 1),
                            grad(_noisePermutations[(BA + 1)], x - 1, y, z - 1)),
                            lerp(u, grad(_noisePermutations[(AB + 1)], x, y - 1, z - 1),
                                    grad(_noisePermutations[(BB + 1)], x - 1, y - 1, z - 1))));
        }

        public float fBm(float x, float y, float z)
        {
            float result = 0.0f;

            if (_recomputeSpectralWeights)
            {
                _spectralWeights = new float[_octaves];

                for (int i = 0; i < _octaves; i++)
                    _spectralWeights[i] = Mathf.Pow(LACUNARITY, -H * i);

                _recomputeSpectralWeights = false;
            }

            for (int i = 0; i < _octaves; i++)
            {
                result += noise(x, y, z) * _spectralWeights[i];

                x *= LACUNARITY;
                y *= LACUNARITY;
                z *= LACUNARITY;
            }

            return result;
        }

        private static float fade(float t)
        {
            return t * t * t * (t * (t * 6 - 15) + 10);
        }

        public static float lerp(float t, float a, float b)
        {
            return a + t * (b - a);
        }

        public static float grad(int hash, float x, float y, float z)
        {
            int h = hash & 15;
            float u = h < 8 ? x : y, v = h < 4 ? y : h == 12 || h == 14 ? x : z;
            return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
        }

        public void setOctaves(int octaves)
        {
            _octaves = octaves;
            _recomputeSpectralWeights = true;
        }

        public int getOctaves()
        {
            return _octaves;
        }
    }
}