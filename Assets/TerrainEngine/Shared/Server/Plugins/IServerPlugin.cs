﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using FrameWork;

namespace TerrainEngine
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ServerPluginAttribute : Attribute
    {
        public int Id;  // Unique ID, used to fast access in Plugin Container
        public string Name; // Unique Name, used to Load/search/add/remove plugin to a plugin Container
        public int Priority; // Order in plugin container. Lower value = functions called first
        public bool OverrideExisting = false; // Set to true to override a default plugin. Used to create custom plugins
        public Type ObjectType = typeof(TerrainWorldServer); // Type receiving this plugin, one instance per object. Can be used for network/processors/AI/events/quests/pathfinding/etc.
    }

    public class ServerPluginInfo
    {
        public ServerPluginAttribute Attribute;
        public Type ClassType;
    }

    /// <summary>
    /// Inherit from this class to handle plugins on your object. Used for TerrainServer,TerrainObject,etc. Set ObjectType=typeof(YourClass) in ServerPluginAttribute
    /// </summary>
    public class ServerPluginsContainer
    {
        public SList<IServerPlugin> Plugins = new SList<IServerPlugin>(1);

        public bool AddPlugin(IServerPlugin Plugin)
        {
            for(int i=Plugins.Count-1;i>=0;--i)
            {
                if (Plugins.array[i].Attribute.Id == Plugin.Attribute.Id || Plugins.array[i].Attribute.Name == Plugin.Attribute.Name)
                {
                    if (Plugins.array[i] == Plugin || !Plugin.Attribute.OverrideExisting)
                    {
                        GeneralLog.LogError("Plugin Already registered :" + Plugin);
                        return false;
                    }
                    else
                        RemovePluginIndex(i);
                }
            }

            Plugins.Add(Plugin);
            return true;
        }

        public IServerPlugin GetPlugin(int Id)
        {
            for (int i = Plugins.Count - 1; i >= 0; --i)
                if (Plugins.array[i].Attribute.Id == Id)
                    return Plugins.array[i];
            return null;
        }

        public IServerPlugin GetPlugin(string Name)
        {
            for (int i = Plugins.Count - 1; i >= 0; --i)
                if (Plugins.array[i].Attribute.Name == Name)
                    return Plugins.array[i];
            return null;
        }

        public int GetPluginIndex(int Id)
        {
            for (int i = Plugins.Count - 1; i >= 0; --i)
                if (Plugins.array[i].Attribute.Id == Id)
                    return i;
            return -1;
        }

        public int GetPluginIndex(string Name)
        {
            for (int i = Plugins.Count - 1; i >= 0; --i)
                if (Plugins.array[i].Attribute.Name == Name)
                    return i;
            return -1;
        }

        public int GetPluginIndex(IServerPlugin Plugin)
        {
            for (int i = Plugins.Count - 1; i >= 0; --i)
                if (Plugins.array[i] == Plugin)
                    return i;
            return -1;
        }

        public bool RemovePluginIndex(int Index)
        {
            if (Index < Plugins.Count && Index >= 0)
            {
                IServerPlugin Plugin = Plugins.array[Index];
                Plugin.OnRemovePlugin();
                Plugins.RemoveAt(Index);
                return true;
            }

            return false;
        }

        public bool RemovePlugin(IServerPlugin Plugin)
        {
            if (Plugin == null)
                return false;

            return RemovePluginIndex(GetPluginIndex(Plugin));
        }

        public IServerPlugin RemovePlugin(int Id)
        {
            IServerPlugin Plugin = GetPlugin(Id);
            RemovePlugin(Plugin);
            return Plugin;
        }

        public IServerPlugin RemovePlugin(string Name)
        {
            IServerPlugin Plugin = GetPlugin(Name);
            RemovePlugin(Plugin);
            return Plugin;
        }

        public void Clear()
        {
            for (int i = Plugins.Count - 1; i >= 0; --i)
                RemovePluginIndex(i);
        }
    }

    /// <summary>
    /// Inherit from this class to handle server events. Override functions to customize your server
    /// </summary>
    public abstract class IServerPlugin
    {
        public ServerPluginAttribute Attribute;

        public override string ToString()
        {
            return base.ToString();
        }

        public virtual void OnAddPlugin()
        {

        }

        public virtual void OnRemovePlugin()
        {

        }

        #region Network

        public virtual void OnClientConnect(TCPClient Client)
        {

        }

        public virtual void OnClientDisconnect(TCPClient Client)
        {

        }


        #endregion

        #region Server

        public virtual void OnServerStart(TerrainWorldServer Server)
        {

        }
        
        public virtual void OnServerAddPlugin(TerrainWorldServer Server,IServerPlugin Plugin)
        {

        }

        public virtual void OnServerRemovePlugin(TerrainWorldServer Server, IServerPlugin Plugin)
        {

        }

        public virtual void OnServerUpdate(ActionThread Thread, TerrainWorldServer Server)
        {

        }

        public virtual void OnServerStop(TerrainWorldServer Server)
        {

        }


        #endregion

        #region Objects

        public virtual void OnObjectEnterWorld(TerrainObjectServer Object)
        {

        }

        public virtual void OnObjectLeaveWorld(TerrainObjectServer Object)
        {

        }

        public virtual void OnObjectMoveChunk(TerrainObjectServer Object, VectorI3 Chunk)
        {

        }

        public virtual void OnObjectEnterBlock(TerrainBlockServer Block,TerrainObjectServer Obj)
        {

        }

        public virtual void OnObjectLeaveBlock(TerrainBlockServer Block, TerrainObjectServer Obj)
        {

        }

        #endregion

        #region Terrain

        public virtual void OnGeneratorLoad(IWorldGenerator Generator)
        {

        }

        public virtual void OnBlockStartGenerate(ActionThread Thread, TerrainBlockServer Block)
        {

        }

        public virtual bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            return true;
        }

        public virtual void OnBlockGenerated(ActionThread Thread, TerrainBlockServer Block)
        {

        }


        public virtual void OnTerrainDataCreate(TerrainDatas Data)
        {

        }

        public virtual void OnTerrainDataClose(TerrainDatas Data)
        {

        }

        public virtual void OnTerrainBlockCreate(TerrainBlockServer Block)
        {

        }

        public virtual void OnTerrainBlockClose(TerrainBlockServer Block)
        {

        }


        #endregion

        #region WorldEvents

        public virtual bool OnVoxelStartModification(ITerrainBlock Block, uint TObject, ref VoxelFlush Flush)
        {
            return true;
        }

        public virtual void OnVoxelApplyModification(ITerrainBlock Block, uint TObject, ref VoxelFlush Flush)
        {

        }

        public virtual void OnTerrainOperator(TerrainWorldServer Server, ITerrainOperator Operator)
        {

        }

        public virtual void OnReceiveCustomPacket(PacketIn Packet, uint TObject, string CustomName)
        {

        }


        #endregion
    }
}
