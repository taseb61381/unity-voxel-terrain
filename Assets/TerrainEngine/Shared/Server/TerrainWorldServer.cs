﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Reflection;
using UnityEngine;

namespace TerrainEngine
{
    /// <summary>
    /// Server managing objects and terrains. Contains all terrains loaded and all objects positions
    /// </summary>
    public class TerrainWorldServer : ITerrainContainer
    {
        static public PoolInfoRef TerrainBlockServerPool;

        public uint CurrentID = 0;          // Unique TerrainObject ID
        public string BiomesFolder = "";    // Folder containing Biomes XML Files exported from Client scene. Menu : [TerrainEngine/Biomes/Export To XML]
        public WorldGenerator Generator;    // This Class contains all function that will generate the world
        public bool IsServer = false;       // True if server is using the network
        public ITerrainSave TerrainSave;    // Interface for Save/Load Terrains. Create a class that inherit from ITerrainSave if you need custom system with different folder
        public ThreadManager Manager;       // Contains all threads.
        private float NextUpdate = 0;       // TerrainObject And World is updated every X seconds. This value contains the next time when the world can be updated
        public TerrainInformation Information { get { return Generator.Information; } }
        public float LoadingProgress
        {
            get
            {
                return ProgressSystem.Instance.GetProgress(ThreadManager.UnityTime);
            }
        } // World Generation LoadingProgress
        public ITerrainOperatorNetworkManager OperatorManager = new ITerrainOperatorNetworkManager();
        public TerrainOperatorContainer OperatorsContainer;

        public List<ServerPluginInfo> PlugingsInfo = new List<ServerPluginInfo>();
        public ServerPluginsContainer Plugins = new ServerPluginsContainer();

        #region Main

        public void Start(ThreadManager Manager, WorldGenerator Generator, ITerrainSave TerrainSave = null)
        {
            if (GeneralLog.DrawLogs)
                GeneralLog.Log("TerrainWorldServer : Client server starting");

            this.Generator = Generator;
            this.Manager = Manager;
            this.IsServer = false;
            this.BiomesFolder = Information.FolderName;

            LoadPlugins();

            if (TerrainSave == null)
                TerrainSave = new ITerrainSave();

            this.TerrainSave = TerrainSave;
            this.TerrainSave.Init(this);
            Information.InitChunks();

            InitThreads();

            OperatorManager.LoadOperators();

            InitWorldGenerator();

            Generator.OnServerStart(this);
        }

        public void Start(string BiomesFolder, ITerrainSave TerrainSave = null, ThreadManager Manager = null)
        {
            if (GeneralLog.DrawLogs)
                GeneralLog.Log("Server Starting on path :" + BiomesFolder);

            this.BiomesFolder = BiomesFolder;
            this.IsServer = true;

            this.Generator = XmlMgr.GetXMLFile<WorldGenerator>(Path.Combine(BiomesFolder,"WorldGenerator.xml"));
            if (this.Generator == null)
            {
                GeneralLog.LogError("Could not find WorldGenerator.xml");
                return;
            }
            this.Generator.Information.Init();

            if (Manager == null)
            {
                this.Manager = new ThreadManager();
            }

            this.Manager.Start();

            LoadPlugins();

            if (TerrainSave == null)
                TerrainSave = new ITerrainSave();

            this.TerrainSave = TerrainSave;
            this.TerrainSave.Init(this);

            TerrainSave.LoadBiomes(Generator);
            InitThreads();

            OperatorsContainer = new TerrainOperatorContainer();
            OperatorManager.LoadOperators();

            InitWorldGenerator();
            Generator.OnServerStart(this);
        }

        private void InitThreads()
        {
            for (int i = 0; i < this.Manager.Threads.Length; ++i)
            {
                if (this.Manager.Threads[i].ThreadId >= 0)
                {
                    this.Manager.Threads[i].AddAction(new ExecuteAction("Server:Update", UpdateThread));
                }
            }
        }

        public void Stop(bool SaveWorld)
        {
            if (SaveWorld)
            {
                GeneralLog.Log("TerrainWorldServer : Saving World...");
                SaveAll();

                while (IsSaving())
                {
                    Thread.Sleep(10);
                }

                Thread.Sleep(100);
            }

            this.Manager.Stop();
            Generator.OnServerStop(this);
        }

        public bool UpdateThread(ActionThread Thread)
        {
            WorldUpdate(Thread);
            return false;
        }

        public void WorldUpdate(ActionThread Thread)
        {
            if (Thread.ThreadId == 0)
            {
                ThreadManager.UnityTime = (float)(DateTime.UtcNow - Manager.StartTime).TotalMilliseconds * 0.001f;

                FlushVoxels();

                if (NextUpdate < ThreadManager.UnityTime)
                {
                    if (ThreadManager.EnableStats)
                        Thread.RecordTime();

                    lock (Objects.array)
                    {
                        for (int i = Objects.Count - 1; i >= 0; --i)
                        {
                            Objects.array[i].Update();
                        }
                    }

                    UpdateBlocks();

                    if (ThreadManager.EnableStats)
                        Thread.AddStats("Server:UpdateObjects", Thread.Time());
                }

                if (UpdateSavingBlocks(Thread))
                    return;

                UpdateClosingVoxels();
            }

            UdpateWorldGeneration(Thread);
        }

        #endregion

        #region Plugins

        public virtual void LoadPlugins()
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type[] Types = assembly.GetTypes();
                for (int i = 0; i < Types.Length; ++i)
                {
                    // Pick up a class
                    if (Types[i].IsClass != true || !Types[i].IsSubclassOf(typeof(IServerPlugin)))
                        continue;

                    foreach (object at in Types[i].GetCustomAttributes(typeof(ServerPluginAttribute), false))
                    {
                        ServerPluginAttribute Attr = at as ServerPluginAttribute;

                        GeneralLog.Log("Init Server Script:" + Attr.Name + ",Class:" + Types[i] + ",ObjectType:" + Attr.ObjectType);

                        ServerPluginInfo Info = new ServerPluginInfo();
                        Info.Attribute = Attr;
                        Info.ClassType = Types[i];
                        PlugingsInfo.Add(Info);
                    }
                }
            }

            PlugingsInfo.Sort(delegate(ServerPluginInfo A, ServerPluginInfo B)
                {
                    if (A.Attribute.Priority < B.Attribute.Priority)
                        return -1;
                    else
                        return 1;
                });

            OnLoadObject(this, Plugins);
        }

        public virtual void OnLoadObject(object Obj,ServerPluginsContainer Container)
        {
            Type T = Obj.GetType();
            for (int i = 0; i < PlugingsInfo.Count; ++i)
            {
                if (PlugingsInfo[i].Attribute.ObjectType == T)
                {
                    IServerPlugin Plugin = Activator.CreateInstance(PlugingsInfo[i].ClassType) as IServerPlugin;
                    Plugin.Attribute = PlugingsInfo[i].Attribute;
                    Container.AddPlugin(Plugin);
                }
            }
        }

        #endregion

        #region Terrains

        public delegate void OnServerSendCreateBlockDelegate(TerrainObjectServer Object, TerrainBlockServer Block);
        public delegate void OnServerSendAllowedBlocksDelegate(TerrainObjectServer Object, List<VectorI3> Positions);

        public OnServerSendCreateBlockDelegate OnServerSendCreateBlock;
        public OnServerSendAllowedBlocksDelegate OnServerSendAllowedBlocks;

        public bool GetBlock(VectorI3 Position, out TerrainBlockServer Block)
        {
            Block = GetBlock(Position) as TerrainBlockServer;
            if (Block == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Return a Terrain Block. A block is a part of terrain that contain chunks and all objects/voxels
        /// </summary>
        public TerrainBlockServer GetBlock(VectorI3 Position, bool Create)
        {
            TerrainBlockServer Block = null;
            lock (Blocks)
            {
                if (!GetBlock(Position, out Block) && Create)
                {
                    if (TerrainBlockServerPool.Index == 0)
                    {
                        TerrainBlockServerPool = PoolManager.Pool.GetPool("TerrainBlockServer");
                        PoolManager.Pools.array[TerrainBlockServerPool.Index].CanFreeMemory = false;
                    }

                    Block = TerrainBlockServerPool.GetData<TerrainBlockServer>();
                    Block.Server = this;
                    Block.Init(Information, Position, GetNewBlockId());
                    AddBlock(Block);
                }
            }

            return Block;
        }

        public void AddBlock(TerrainBlockServer Block)
        {
            //GeneralLog.Log("Server", Block.Position + " Add Block");

            base.AddBlock(Block, Block.BlockId);
            TerrainBlockServer AroundBlock = null;

            int x, y, z;
            VectorI3 direction = new VectorI3();
            for (x = -1; x < 2; ++x)
            {
                direction.x = x;
                for (y = -1; y < 2; ++y)
                {
                    direction.y = y;
                    for (z = -1; z < 2; ++z)
                    {
                        direction.z = z;
                        if (GetBlock(Block.LocalPosition + direction, out AroundBlock))
                        {
                            Block[direction.x + 1, direction.y + 1, direction.z + 1] = AroundBlock;
                            AroundBlock[2 - (direction.x + 1), 2 - (direction.y + 1), 2 - (direction.z + 1)] = Block;
                        }
                    }
                }
            }

            WorldGenerator.AddBlockToGenerate(Block, Block.OnGenerationDone);
        }

        public void RemoveBlock(TerrainBlockServer Block)
        {
            if (GeneralLog.DrawLogs)
                GeneralLog.Log("Server", Block.LocalPosition + " Remove Block");

            lock (Blocks)
            {
                if (base.RemoveBlock(Block, Block.BlockId))
                {
                    ITerrainBlock AroundBlock = null;
                    int x, y, z;
                    for (x = -1; x < 2; ++x)
                    {
                        for (y = -1; y < 2; ++y)
                        {
                            for (z = -1; z < 2; ++z)
                            {
                                AroundBlock = Block[x + 1, y + 1, z + 1];
                                if (AroundBlock != null && AroundBlock != Block)
                                {
                                    AroundBlock[2 - (x + 1), 2 - (y + 1), 2 - (z + 1)] = null;
                                    Block[x + 1, y + 1, z + 1] = null;
                                }
                            }
                        }
                    }
                }
            }

            Block.Close();
        }

        private void UpdateBlocks()
        {
            NextUpdate = ThreadManager.UnityTime + 0.5f;
            lock (Blocks)
            {
                for (int i = Blocks.Count - 1; i >= 0; --i)
                    (Blocks[i] as TerrainBlockServer).Update();
            }
        }

        #endregion

        #region Objects

        public delegate void OnServerObjectDelegate(TerrainObjectServer Obj);

        public OnServerObjectDelegate OnAddObject;
        public OnServerObjectDelegate OnRemoveObject;

        /// <summary>
        /// All Objects on Server. (Players) Object around TerrainBlock will be loaded when object's position change. TerrainObjectServer.Handlers will handle send/receive network function
        /// </summary>
        public SList<TerrainObjectServer> Objects = new SList<TerrainObjectServer>(2);

        public void AddObject(TerrainObjectServer Object)
        {
            if (Object.Server == this)
                return;

            lock (Objects.array)
            {
                if (Object.Server != null)
                    Object.Server.RemoveObject(Object);

                Object.Server = this;
                Objects.Add(Object);
                Object.Id = ++CurrentID;
            }

            OnLoadObject(Object, Object.Plugings);

            if (OnAddObject != null)
                OnAddObject(Object);
        }

        public void RemoveObject(TerrainObjectServer Object)
        {
            if (Object.Server != this)
                return;

            if (OnRemoveObject != null)
                OnRemoveObject(Object);

            lock (Objects.array)
            {
                Object.Server = null;
                Objects.Remove(Object);
            }

            Object.Clear();
        }

        #endregion

        #region Generation

        public delegate void OnBlockEventDelegate(TerrainBlockServer Block);

        public struct TempTerrainEvent
        {
            public TempTerrainEvent(TerrainBlockServer Block, OnBlockEventDelegate Event)
            {
                this.Block = Block;
                this.Event = Event;
                this.StartTime = 0;
            }

            public float StartTime;
            public TerrainBlockServer Block;    // Current Block Generating
            public OnBlockEventDelegate Event; // Function To call when Block Generation is done
        }

        private bool IsWorldGeneratorInited = false;
        public ITerrainWorldGenerator WorldGenerator = null;

        public void UdpateWorldGeneration(ActionThread Thread)
        {
            if (WorldGenerator != null)
                WorldGenerator.Udpate(Thread);
        }

        public virtual void InitWorldGenerator()
        {
            if (IsWorldGeneratorInited)
                return;

            IsWorldGeneratorInited = true;

            if (WorldGenerator == null)
            {
                WorldGenerator = new ITerrainWorldGenerator();
                WorldGenerator.Init(this);
            }
            else
                WorldGenerator.Init(this);
        }

        #endregion

        #region Save

        private Queue<TempTerrainEvent> SavingBlocks = new Queue<TempTerrainEvent>();
        private List<TerrainDatas> ClosingVoxels = new List<TerrainDatas>();

        public bool IsSaving()
        {
            return SavingBlocks.Count != 0;
        }

        public void AddBlockToSave(TerrainBlockServer Block, OnBlockEventDelegate OnSaved)
        {
            lock (SavingBlocks)
            {
                ProgressSystem.Instance.SetProgress("Save Terrain : " + Block.LocalPosition, 0, ThreadManager.UnityTime);
                SavingBlocks.Enqueue(new TempTerrainEvent(Block, OnSaved));
            }
        }

        public void AddVoxelsToClose(TerrainDatas Data)
        {
            //GeneralLog.Log("TerrainBlockServer", Data.Position + " AddVoxelsToClose");
            lock (ClosingVoxels)
            {
                Data.IsUsedByServer = false;
                ClosingVoxels.Add(Data);
            }
        }

        public void SaveAll()
        {
            lock (Blocks)
            {
                GeneralLog.Log("Saving all terrains : " + Blocks.Count);

                for (int i = Blocks.Count - 1; i >= 0; --i)
                    AddBlockToSave(Blocks[i] as TerrainBlockServer, null);
            }
        }

        private bool GetBlockToSave(ref TempTerrainEvent Info)
        {
            lock (SavingBlocks)
            {
                if (SavingBlocks.Count != 0)
                {
                    Info = SavingBlocks.Dequeue();
                    return true;
                }
            }

            return false;
        }

        private void UpdateClosingVoxels()
        {
            lock (ClosingVoxels)
            {
                for (int i = ClosingVoxels.Count - 1; i >= 0; --i)
                {
                    if (ClosingVoxels[i].IsUsedByClient == false && ClosingVoxels[i].IsUsedByServer == false)
                    {
                        if(GeneralLog.DrawLogs)
                            GeneralLog.Log("TerrainBlockServer", ClosingVoxels[i].Position + " Closing Voxels");
                        Generator.OnTerrainDataClose(ClosingVoxels[i]);
                        ClosingVoxels[i].Close();
                        ClosingVoxels.RemoveAt(i);
                    }
                }
            }
        }

        private bool UpdateSavingBlocks(ActionThread Thread)
        {
            TempTerrainEvent Info = new TempTerrainEvent();
            if (GetBlockToSave(ref Info) && Info.Block != null)
            {
                if (ThreadManager.EnableStats)
                    Thread.RecordTime();

                TerrainSave.Save(Info.Block);
                ProgressSystem.Instance.SetProgress("Save Terrain : " + Info.Block.LocalPosition, 100f, ThreadManager.UnityTime);

                if (Info.Event != null)
                    Info.Event(Info.Block);

                if (ThreadManager.EnableStats)
                    Thread.AddStats("Server:SaveBlock", Thread.Time());

                return true;
            }

            return false;
        }

        #endregion

        #region Modification

        public bool CanFlushVoxels = true;

        public void AddOperator(ITerrainOperator Operator,uint ObjectId)
        {
            Operator.ObjectId = ObjectId;
            OperatorsContainer.AddOperator(Operator);
        }

        public void FlushVoxels()
        {
            if (!CanFlushVoxels || OperatorsContainer == null || OperatorsContainer.Count == 0)
                return;

            OperatorsContainer.Flush(this, CanModifyVoxel, OnModifyVoxel, null);
        }

        public bool CanModifyVoxel(ITerrainOperator Operator, ref VoxelFlush Flush)
        {
            bool CanModify = true;

            for (int i = 0; i < Generator.Generators.Count; ++i)
            {
                if (!Generator.Generators[i].OnVoxelStartModification(Flush.Block, Operator.ObjectId, ref Flush))
                {
                    CanModify = false;
                    break;
                }
            }

            return CanModify;
        }

        public bool OnModifyVoxel(ITerrainOperator Operator, ref VoxelFlush Flush) 
        {
            TerrainBlockServer BlockServer = null;
            if (Flush.Block is TerrainBlockServer)
            {
                BlockServer = Flush.Block as TerrainBlockServer;
            }
            else
            {
                BlockServer = BlocksArray[Flush.Block.BlockId] as TerrainBlockServer;
            }

            TerrainObjectServer TObject = null;
            for (int j = BlockServer.Objects.Count - 1; j >= 0; --j)
            {
                TObject = BlockServer.Objects.array[j];
                if (TObject != null && TObject.Id != Operator.ObjectId && (TObject.Handlers is TCPTerrainClientHandler))
                {
                    TObject.AddOperator(Operator);
                }
            }

            return true;
        }

        #endregion
    }
}