﻿using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    public struct ServerVoxelModification
    {
        public ServerVoxelModification(ITerrainOperator Operator, uint ObjectId)
        {
            this.Operator = Operator;
            this.ObjectId = ObjectId;
        }

        public ITerrainOperator Operator;
        /// <summary>
        /// Server Object Id. Client Id of this modification
        /// </summary>
        public uint ObjectId;
    }

    public struct ServerCustomDataModification
    {
        public byte Type, ProcessorID, ModType;
        public ushort x, y, z, UID;
        public uint TObject;
        public int Int;
        public float Float;
    }

    public class TerrainBlockServer : ITerrainBlock
    {
        static public float BlockRemoveAfterTime = 10f;

        public TerrainWorldServer Server;
        public float LastObjectsTime = 0;
        public SList<TerrainObjectServer> Objects = new SList<TerrainObjectServer>(2);
        public SList<TerrainObjectServer> ToSendObjects = new SList<TerrainObjectServer>(2);

        public override void Init(TerrainInformation Information, VectorI3 Position, ushort UID)
        {
            base.Init(Information, Position, UID);
            SetState(TerrainBlockStates.LOADED, false);
            this.LastObjectsTime = 0;
        }

        public override VoxelTypes GetVoxelType(byte Id)
        {
            return Server.Generator.Palette[Id].VoxelType;
        }

        public void Update()
        {
            if (Voxels == null || !HasState(TerrainBlockStates.GENERATED))
                return;

            if (LastObjectsTime != 0 && (LastObjectsTime < ThreadManager.UnityTime + BlockRemoveAfterTime || !Server.IsServer))
            {
                if (!HasState(TerrainBlockStates.CLOSED))
                {
                    //GeneralLog.Log("Server", "Block closing, no objects :" + Position);
                    Server.AddBlockToSave(this, Server.RemoveBlock);
                }

                SetState(TerrainBlockStates.CLOSED, true);
            }
            else
                SetState(TerrainBlockStates.CLOSED, false);
        }

        public void OnGenerationDone(TerrainBlockServer Block)
        {
            //GeneralLog.Log("TerrainBlockServer", LocalPosition + " Generated : Sending To Waiting List " + ToSendObjects.Count);

            lock (Objects.array)
            {
                if (HasState(TerrainBlockStates.GENERATED) && ToSendObjects.Count != 0)
                {
                    for(int i=0;i<ToSendObjects.Count;++i)
                    {
                        if (Objects.Contains(ToSendObjects.array[i]))
                            SendMeTo(ToSendObjects.array[i]);
                    }
                }
            }
        }

        #region Objects

        public bool AddObject(TerrainObjectServer Object)
        {
            lock (Objects.array)
            {
                if (!Objects.Contains(Object))
                {
                    //GeneralLog.Log("TerrainBlockServer", LocalPosition + " Adding Object : " + Object);
                    Objects.Add(Object);
                    return true;
                }
            }

            return false;
        }

        public bool RemoveObject(TerrainObjectServer Object)
        {
            lock (Objects.array)
            {
                if (Objects.Remove(Object))
                {
                    ToSendObjects.Remove(Object);
                    if (!HasState(TerrainBlockStates.GENERATED))
                        Object.Handlers.SendGeneratingAbord(LocalPosition);

                    if (Objects.Count == 0)
                    {
                        //GeneralLog.Log("TerrainBlockServer", LocalPosition + " Remove Object : " + Object + ".No more objects,closing in few seconds");
                        LastObjectsTime = ThreadManager.UnityTime;
                    }
                    else
                    {
                        //GeneralLog.Log("TerrainBlockServer", LocalPosition + " Remove Object : " + Object);
                        LastObjectsTime = 0;
                    }

                    return true;
                }
            }

            return false;
        }

        public bool HasObject(TerrainObjectServer Object)
        {
            lock (Objects.array)
                return Objects.Contains(Object);
        }

        #endregion

        #region Sending Datas

        public void SendMeTo(TerrainObjectServer Object)
        {
            //GeneralLog.Log("TerrainBlockServer", LocalPosition + " Send Me To : " + Object);


            if (!HasState(TerrainBlockStates.GENERATED))
            {
                lock (Objects.array)
                {
                    if (!ToSendObjects.Contains(Object))
                    {
                        //GeneralLog.Log("TerrainBlockServer", LocalPosition + " Add to sending list : " + Object);

                        Object.Handlers.SendGeneratingBlock(LocalPosition, 0);
                        ToSendObjects.Add(Object);
                    }
                }
            }
            else
            {
                //GeneralLog.Log("TerrainBlockServer", LocalPosition + " Sending Create Block : " + Object);
                Object.Handlers.CreateBlock(this);
                if (Object.Server.OnServerSendCreateBlock != null)
                    Object.Server.OnServerSendCreateBlock(Object, this);
            }
        }

        #endregion

        public override void Clear()
        {
            SetState(TerrainBlockStates.LOADED, false);
            SetState(TerrainBlockStates.GENERATED, false);

            if (Voxels != null)
            {
                Server.AddVoxelsToClose(Voxels);
                Voxels = null;
                Objects.Clear();
                ToSendObjects.Clear();
            }
        }
    }

}