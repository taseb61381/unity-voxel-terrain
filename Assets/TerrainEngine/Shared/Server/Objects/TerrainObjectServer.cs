﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    /// <summary>
    /// Terrain Object on world. This class manage send/receive world functions. Current TerrainBlock and TerrainChunk. Other around object etc.
    /// </summary>
    public class TerrainObjectServer
    {
        public TerrainWorldServer Server;

        public uint Id;                         // Unique TerrainObject ID
        public Vector3 Position;                // WordPosition (UnityPosition)
        public VectorI3 LoadBlockPosition;     // Force this object to load blocks only around this blockposition
        public VectorI3 CurrentBlockPosition;   // BlockPosition, used to GetBlock on TerrainWorldServer.GetBlock(BlockPosition);
        public VectorI3 CurrentChunkPosition;
        public VectorI3 CurrentVoxel;
        public ITerrainObjectHandlers Handlers; // This class will manage send/receive network function. Create a new class inherit from ITerrainObjectHandlers and override functions
        public bool IsDirty = false;            // Position has changed. Around Block/Objects must be updated

        public List<ushort> CurrentBlocks = new List<ushort>(); // Current Blocks around this object.
        public List<ushort> ToSendList = new List<ushort>();    // Current Blocks that are generating. When generation will be done, this blocks will be sent to this object
        public List<VectorI3> VisibleBlocks = new List<VectorI3>();                     // List of BlockPosition visible around this object
        public ServerPluginsContainer Plugings = new ServerPluginsContainer();

        private Vector3 LastPosition = new Vector3(float.MinValue,float.MinValue,float.MinValue);

        public virtual void Clear()
        {
            Handlers = null;
            IsDirty = false;
            Plugings.Clear();
            VisibleBlocks.Clear();
            ToSendList.Clear();
            CurrentBlocks.Clear();
            RangedObjects.Clear();
            Voxels.Clear();
        }

        public void SetHandler(ITerrainObjectHandlers Handlers)
        {
            this.Handlers = Handlers;
            Handlers.Owner = this;
            Handlers.Init(this);
        }

        public void SetPosition(float x, float y, float z)
        {
            Position.x = x;
            Position.y = y;
            Position.z = z;
            CurrentChunkPosition = Server.Information.GetChunkGlobalPosition(Position);
            CurrentBlockPosition = Server.Information.GetBlockFromWorldPosition(Position);
            LoadBlockPosition = CurrentBlockPosition;
        }

        public void SetLoadPosition(float x,float y,float z)
        {
            LoadBlockPosition = Server.Information.GetBlockFromWorldPosition(x, y, z);

            if (IsDirty)
                return;

            if ((LastPosition - Position).sqrMagnitude > 25)
            {
                LastPosition = Position;
                IsDirty = true;
            }
        }

        public bool IsInBlock(VectorI3 Position)
        {
            ITerrainBlock Block = null;
            lock (CurrentBlocks)
            {
                for (int i = CurrentBlocks.Count - 1; i >= 0; --i)
                {
                    Block = Server.GetBlock(CurrentBlocks[i]);
                    if (Block != null && Block.LocalPosition == Position)
                        return true;
                }
            }
            return false;
        }

        public bool IsOnBlockRange(ushort BlockUID)
        {
            ITerrainBlock Block = Server.GetBlock(BlockUID);
            if (Block == null)
                return false;

            return IsOnBlockRange(Block);
        }

        public bool IsOnBlockRange(ITerrainBlock Block)
        {
            int Rx = Block.LocalPosition.x - LoadBlockPosition.x;
            int Ry = Block.LocalPosition.y - LoadBlockPosition.y;
            int Rz = Block.LocalPosition.z - LoadBlockPosition.z;

            if (Rx < Server.Information.MinRange.x || Rx > Server.Information.MaxRange.x)
                return false;

            if (Ry < Server.Information.MinRange.y || Ry > Server.Information.MaxRange.y)
                return false;

            if (Rz < Server.Information.MinRange.z || Rz > Server.Information.MaxRange.z)
                return false;

            return true;
        }

        public void Update()
        {
            if (IsDirty)
                UpdateDirty();

            UpdateVoxels();
        }

        public void UpdateBlocks()
        {
            TerrainBlockServer Block = null;
            VectorI3 BlockPosition = new VectorI3();
            int x, y, z;

            lock (CurrentBlocks)
            {
                for (x = 0; x < CurrentBlocks.Count; )
                {
                    Block = Server.GetBlock(CurrentBlocks[x]) as TerrainBlockServer;

                    if (Block != null && !IsOnBlockRange(Block))
                    {
                        Block.RemoveObject(this);
                        CurrentBlocks.RemoveAt(x);
                        continue;
                    }

                    ++x;
                }

                ToSendList.Clear();
                VisibleBlocks.Clear();
            }


            for (x = (int)Server.Information.MinRange.x; x <= (int)Server.Information.MaxRange.x; ++x)
            {
                BlockPosition.x = LoadBlockPosition.x + x;
                for (y = (int)Server.Information.MinRange.y; y <= (int)Server.Information.MaxRange.y; ++y)
                {
                    BlockPosition.y = LoadBlockPosition.y + y;
                    for (z = (int)Server.Information.MinRange.z; z <= (int)Server.Information.MaxRange.z; ++z)
                    {
                        BlockPosition.z = LoadBlockPosition.z + z;

                        if (Server.Information.MinLimit != Server.Information.MaxLimit)
                        {
                            if (x < (int)Server.Information.MinLimit.x || x >= (int)Server.Information.MaxLimit.x)
                                continue;

                            if (y < (int)Server.Information.MinLimit.y || y >= (int)Server.Information.MaxLimit.y)
                                continue;

                            if (z < (int)Server.Information.MinLimit.z || z >= (int)Server.Information.MaxLimit.z)
                                continue;
                        }
                        VisibleBlocks.Add(BlockPosition);
                    }
                }
            }

            VisibleBlocks.Sort(
                (VectorI3 BlockX, VectorI3 BlockY) =>
                {
                    float DistanceX = Vector3.Distance(Position, Server.Information.GetBlockMiddlePosition(BlockX));
                    float DistanceY = Vector3.Distance(Position, Server.Information.GetBlockMiddlePosition(BlockY));

                    if (DistanceX == DistanceY) return 0;
                    else if (DistanceX > DistanceY) return 1;

                    return -1;
                });

            lock (CurrentBlocks)
            {
                for (int i = 0; i < VisibleBlocks.Count; ++i)
                {
                    if (!IsInBlock(VisibleBlocks[i]))
                    {
                        Block = Server.GetBlock(VisibleBlocks[i], true);
                        if (Block.AddObject(this))
                        {
                            CurrentBlocks.Add(Block.BlockId);
                            ToSendList.Add(Block.BlockId);
                        }
                    }
                }
            }

            if (ToSendList.Count > 0)
            {
                Handlers.SendAllowedBlocks(VisibleBlocks);
                if (Server.OnServerSendAllowedBlocks != null)
                    Server.OnServerSendAllowedBlocks(this, VisibleBlocks);

                for (x = 0; x < ToSendList.Count; ++x)
                {
                    Block = Server.GetBlock(ToSendList[x]) as TerrainBlockServer;
                    if (Block != null)
                        Block.SendMeTo(this);
                }
            }
        }

        public void UpdateDirty()
        {
            UpdateBlocks();
            UpdateInRange();
            IsDirty = false;
        }

        public void RemoveFromWorld()
        {
            TerrainBlockServer Block = null;
            lock (CurrentBlocks)
            {
                for (int i = CurrentBlocks.Count - 1; i >= 0; --i)
                {
                    Block = Server.GetBlock(CurrentBlocks[i]) as TerrainBlockServer;
                    if (Block != null)
                        Block.RemoveObject(this);
                }
                RangedObjects.Clear();
                CurrentBlocks.Clear();
            }

            Server.RemoveObject(this);
        }

        #region Ranged

        public List<TerrainObjectServer> RangedObjects = new List<TerrainObjectServer>();

        public void AddInRange(TerrainObjectServer Object)
        {
            lock (RangedObjects)
            {
                if (!RangedObjects.Contains(Object))
                {
                    RangedObjects.Add(Object);
                    Object.AddInRange(this);
                    Handlers.OnAddObjectInRange(Object);
                }
            }
        }

        public void RemoveInRange(TerrainObjectServer Object)
        {
            lock (RangedObjects)
            {
                if (!RangedObjects.Remove(Object))
                {
                    Object.RemoveInRange(this);
                    Handlers.OnRemoveObjectInRange(Object);
                }
            }
        }

        public void UpdateInRange()
        {
            TerrainBlockServer Block = null;
            int i, j;

            lock (CurrentBlocks)
            {
                for (i = CurrentBlocks.Count - 1; i >= 0; --i)
                {
                    Block = Server.GetBlock(CurrentBlocks[i]) as TerrainBlockServer;
                    if (Block != null)
                    {
                        lock (Block.Objects.array)
                        {
                            for (j = Block.Objects.Count - 1; j >= 0; --j)
                            {
                                if (Vector3.Distance(Block.Objects.array[j].Position, Position) < 200 && Block.Objects.array[j] != this)
                                {
                                    AddInRange(Block.Objects.array[j]);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Voxels

        public SList<ITerrainOperator> Voxels = new SList<ITerrainOperator>(2);

        public void AddOperator(ITerrainOperator Operator)
        {
            for (int i = Voxels.Count-1; i >= 0; --i)
            {
                if (Voxels.array[i] == Operator)
                {
                    return;
                }
            }

            Voxels.Add(Operator);
        }

        public void UpdateVoxels()
        {
            if (Voxels.Count != 0)
            {
                Handlers.SendOperators(Voxels);
                Voxels.Clear();
            }
        }

        #endregion
    }
}