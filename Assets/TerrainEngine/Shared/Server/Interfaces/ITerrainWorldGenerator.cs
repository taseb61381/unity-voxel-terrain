﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TerrainEngine
{
    public class ITerrainWorldGenerator
    {
        public class GenerateFunctionInfo
        {
            public string Name;
            public IWorldGenerator.GenerateDelegate Function;
            public IWorldGenerator Parent;
        }

        public TerrainWorldServer Server;
        public TerrainInformation Information
        {
            get
            {
                return Server.Information;
            }
        }

        public bool IsGenerating = false;
        public TerrainWorldServer.TempTerrainEvent GeneratingBlock;
        public SList<TerrainWorldServer.TempTerrainEvent> GeneratingBlocks = new SList<TerrainWorldServer.TempTerrainEvent>(10);

        
        private SList<VectorI2>[] ChunksToGenerate;
        private BiomeData[] ThreadsBiomes;

        public List<GenerateFunctionInfo> GenerateFunctions = new List<GenerateFunctionInfo>();

        public override string ToString()
        {
            string Str = "Threads:" + ThreadsBiomes.Length + "\n";

            for (int i = 0; i < ThreadsBiomes.Length; ++i)
            {
                Str += "Id:" + i + "," + GetBiomeState(ThreadsBiomes[i],i) + "\n";
            }

            return Str;
        }

        public string GetBiomeState(BiomeData Data, int Index)
        {
            try
            {
                if (Data.GeneratorId >= GenerateFunctions.Count || Data.ChunkId == -1)
                    return "Done:" + Data.ChunkIndex + "/" + ChunksToGenerate[Index].Count + "," + ThreadManager.Instance.Threads[Index].LastAction;
                else
                    return "ChunkId:" + Data.ChunkId + "Gen:" + Data.GeneratorId + ",Index:" + Data.ChunkIndex + "/" + ChunksToGenerate[Index].Count + ",Name:" + GenerateFunctions[Data.GeneratorId].Name + ",\n" + ThreadManager.Instance.Threads[Index].LastAction;
            }
            catch(Exception e)
            {
                GeneralLog.LogError(e.ToString());
                return "Error";
            }
        }

        public virtual void Init(TerrainWorldServer Server)
        {
            this.Server = Server;
            WorldGenerator Generator = Server.Generator;
            IWorldGenerator[][] WorldGenerators = Generator.WorldGenerators;

            int i, j;
            IWorldGenerator Gen;
            for (i = 0; i < Generator.GetGeneratorsCount(IWorldGenerator.GeneratorTypes.TERRAIN);++i)
            {
                Gen = WorldGenerators[(int)IWorldGenerator.GeneratorTypes.TERRAIN][i];
                AddGenerateFunction(null, Gen.OnGenerate, Gen.FileName);
                if (Gen.IsCallingCustomGenerators())
                {
                    for (j = 0; j < Generator.GetGeneratorsCount(IWorldGenerator.GeneratorTypes.CUSTOM); ++j)
                    {
                        AddGenerateFunction(Gen, WorldGenerators[(int)IWorldGenerator.GeneratorTypes.CUSTOM][j].OnGenerate, WorldGenerators[(int)IWorldGenerator.GeneratorTypes.CUSTOM][j].FileName);
                    }
                }
            }

            j = Server.Generator.GetGeneratorsCount(IWorldGenerator.GeneratorTypes.GROUND);

            if (j > 0)
            {
                for (i = 0; i < j; ++i)
                {
                    if (WorldGenerators[(int)IWorldGenerator.GeneratorTypes.GROUND][i].IsUsingGroundPoints())
                    {
                        AddGenerateFunction(null, GenerateGroundPoints, "GenerateGroundPoints");
                        break;
                    }
                }

                for (i = 0; i < j; ++i)
                {
                    Gen = WorldGenerators[(int)IWorldGenerator.GeneratorTypes.GROUND][i];
                    AddGenerateFunction(null, Gen.OnGenerate, Gen.FileName);
                }
            }

            InitThreads(Server.Manager.Threads.Length);
        }

        public virtual void InitThreads(int Count)
        {
            ChunksToGenerate = new SList<VectorI2>[Count];
            ThreadsBiomes = new BiomeData[Count];

            int ChunksPerThread = (Server.Information.ChunkPerBlock * Server.Information.ChunkPerBlock) / Count;
            int ThreadId = -1, Index = 0;

            for (int x = 0; x < Server.Information.ChunkPerBlock; ++x)
            {
                for (int z = 0; z < Server.Information.ChunkPerBlock; ++z, ++Index)
                {
                    if ((Index == 0 || Index % ChunksPerThread == 0) && ThreadId < Count - 1)
                    {
                        ++ThreadId;

                        ChunksToGenerate[ThreadId] = new SList<VectorI2>(ChunksPerThread);
                        ThreadsBiomes[ThreadId] = new BiomeData();
                        ThreadsBiomes[ThreadId].ChunkId = -1;
                        ThreadsBiomes[ThreadId].ChunkIndex = -1;
                    }

                    ChunksToGenerate[ThreadId].Add(new VectorI2(x, z));
                }
            }
        }

        public virtual void AddGenerateFunction(IWorldGenerator Parent, IWorldGenerator.GenerateDelegate Func, string Name)
        {
            GeneralLog.Log("[Generator] Function:" + Name);
            GenerateFunctions.Add(new GenerateFunctionInfo() { Parent = Parent, Function = Func, Name = Name });
        }

        public virtual void AddBlockToGenerate(TerrainBlockServer Block, TerrainWorldServer.OnBlockEventDelegate OnGenerate)
        {
            lock (GeneratingBlocks.array)
            {
                GeneratingBlocks.Add(new TerrainWorldServer.TempTerrainEvent() { Block = Block, Event = OnGenerate });
                ProgressSystem.Instance.SetProgress("Generate Terrain : " + Block.LocalPosition.ToString(), 0, ThreadManager.UnityTime);
            }
        }

        public virtual void GetNextBlockToGenerate(ActionThread Thread)
        {
            TerrainDatas Voxels = null;

            lock (GeneratingBlocks.array)
            {
                if (GeneratingBlocks.Count == 0)
                {
                    return;
                }

                for (int i = 0; i < ThreadsBiomes.Length; ++i)
                {
                    ThreadsBiomes[i].Clear();
                    ThreadsBiomes[i].ChunkId = -1;
                    ThreadsBiomes[i].ChunkIndex = -1;
                }

                GeneratingBlock = GeneratingBlocks.array[0];
                GeneratingBlock.StartTime = ThreadManager.UnityTime;
                GeneratingBlocks.RemoveAt(0);

                Voxels = Server.Information.GetData(GeneratingBlock.Block.LocalPosition);
                Voxels.IsUsedByServer = true;

                GeneratingBlock.Block.Voxels = Voxels;
                GeneratingBlock.Block.Voxels.BlockId = GeneratingBlock.Block.BlockId;

                Server.Generator.OnTerrainDataCreate(GeneratingBlock.Block.Voxels);
                Server.TerrainSave.Load(GeneratingBlock.Block);
            }

            if (GeneratingBlock.Block.HasState(TerrainBlockStates.GENERATED))
            {
                GeneralLog.Log("Block Loaded :" + GeneratingBlock.Block + ",Voxels:" + Voxels);

                OnTerrainBlockGenerated(Thread);
            }
            else
            {
                GeneralLog.Log("Generation Start, Block:" + GeneratingBlock.Block + ",Voxels:" + Voxels);

                IsGenerating = true;
            }
        }

        public virtual bool IsChunksGenerating()
        {
            for (int i = ChunksToGenerate.Length - 1; i >= 0; --i)
            {
                if (ThreadsBiomes[i].ChunkIndex < ChunksToGenerate[i].Count)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Update Called by TerrainWorldServer, all threads
        /// </summary>
        /// <param name="Thread"></param>
        public void Udpate(ActionThread Thread)
        {
            if (ThreadManager.IsClientWorking)
                return;

            if(Thread.ThreadId == 0)
            {
                if (!IsGenerating)
                    GetNextBlockToGenerate(Thread);
            }

            ThreadManager.IsServerWorking = IsGenerating;

            if (IsGenerating)
            {
                while (Generate(Thread) && Thread.HasTime())
                {

                }
            }

            if(Thread.ThreadId == 0 && IsGenerating)
            {
                if (IsChunksGenerating())
                    return;

                OnTerrainBlockGenerated(Thread);
            }
        }

        public virtual bool Generate(ActionThread Thread)
        {
            BiomeData Data = ThreadsBiomes[Thread.ThreadId];

            if (Data.ChunkId == -1)
            {
                ++Data.ChunkIndex;
                int Index = Data.ChunkIndex;
                if (Index >= ChunksToGenerate[Thread.ThreadId].Count)
                    return false;

                Data.Clear();
                Data.ChunkX = ChunksToGenerate[Thread.ThreadId].array[Index].x;
                Data.ChunkZ = ChunksToGenerate[Thread.ThreadId].array[Index].y;
                Data.ChunkId = Data.ChunkX * Server.Information.ChunkPerBlock * Server.Information.ChunkPerBlock + Data.ChunkZ;
            }

            if (Data.ChunkId != -1)
            {
                try
                {
                    if (GenerateChunk(Thread, Data, GeneratingBlock.Block, Data.ChunkX, Data.ChunkZ))
                    {
                        Data.ChunkId = -1;
                    }
                }
                catch (Exception e)
                {
                    Data.ChunkId = -1;
                    GeneralLog.LogError(e.ToString());
                }
            }

            return true;
        }

        public virtual void OnTerrainBlockGenerated(ActionThread Thread)
        {
            ProgressSystem.Instance.SetProgress("Generate Terrain : " + GeneratingBlock.Block.LocalPosition.ToString(), 100f, ThreadManager.UnityTime);

            if (GeneratingBlock.Block.Objects.Count == 0)
            {
                //GeneralLog.Log("TerrainWorldServer : " + Data.GeneratingBlock.Block.Position + " Generation Stopped " + ThreadId);
                Server.AddBlockToSave(GeneratingBlock.Block, Server.RemoveBlock);
            }
            else
            {
                GeneratingBlock.Block.SetState(TerrainBlockStates.GENERATED, true);

                for (int i = 0; i < Server.Generator.Generators.Count; ++i)
                    Server.Generator.Generators[i].OnBlockGenerated(Thread, GeneratingBlock.Block);

                if (GeneralLog.DrawLogs)
                    GeneralLog.Log("TerrainWorldServer : " + GeneratingBlock.Block.LocalPosition + " Generated Block on Thread " + Thread.ThreadId + " in " + (ThreadManager.UnityTime - GeneratingBlock.StartTime) + "s");

                if (ThreadManager.EnableStats)
                    Thread.AddStats("TotalBlockGeneration", (long)((ThreadManager.UnityTime - GeneratingBlock.StartTime) * 1000f));


                if (GeneratingBlock.Event != null)
                    GeneratingBlock.Event(GeneratingBlock.Block);
            }

            GeneratingBlock = new TerrainWorldServer.TempTerrainEvent();
            IsGenerating = false;
        }

        public bool GenerateChunk(ActionThread Thread, BiomeData Data, TerrainBlockServer Block, int ChunkX, int ChunkZ)
        {
            for (; Data.GeneratorId < GenerateFunctions.Count; )
            {
                if (ThreadManager.EnableStats)
                    Thread.RecordTime();

                GenerateFunctionInfo Info = GenerateFunctions[Data.GeneratorId];

                if (Info.Function(Thread, Info.Parent,Block, Block.Voxels, Data, Data.ChunkId, Data.ChunkX, Data.ChunkZ))
                {
                    Data.LoopStep.Clear();
                    Data.InternalState = 0;
                    ++Data.GeneratorId;

                    if (ThreadManager.EnableStats)
                        Thread.AddStats("Server:" + Info.Name, Thread.Time());

                    continue;
                }

                if (ThreadManager.EnableStats)
                    Thread.AddStats("Server:" + Info.Name, Thread.Time());
                return false;
            }

            Data.GeneratorId = 0;
            Data.InternalState = 0;

            Data.LoopStep.Clear();
            Data.GroundPoints.ClearFast();
            if (Data != null)
                Data.Close();
            return true;
        }

        public bool GenerateGroundPoints(ActionThread Thread,IWorldGenerator Parent,TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            Block.GenerateGroundPoints(Data, ChunkX, ChunkZ);

            return true;
        }

        public virtual int GetGeneratingBlocksCount()
        {
            return GeneratingBlocks.Count;
        }

        public virtual int GetGeneratedChunks()
        {
            int Count = 0;
            for (int i = ThreadsBiomes.Length - 1; i >= 0; --i)
                Count += ThreadsBiomes[i].ChunkIndex;

            return Count;
        }

        public virtual int GetTotalChunksToGenerate()
        {
            int Count = 0;
            for (int i = ChunksToGenerate.Length - 1; i >= 0; --i)
                Count += ChunksToGenerate[i].Count;

            return Count;
        }
    }
}
