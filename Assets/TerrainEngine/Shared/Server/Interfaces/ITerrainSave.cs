﻿using FrameWork;
using System;
using System.IO;
using System.Reflection;

namespace TerrainEngine
{
    /// <summary>
    /// Load / Save functions for terrain and Biomes. Override to customize your save folders and loading system
    /// </summary>
    
    [Serializable]
    public class ITerrainSave
    {
        public TerrainInformation Information;
        public string BiomesFolder = "";
        public string SaveDirectory = "";

        public virtual void Init(TerrainWorldServer Server)
        {
            this.Information = Server.Information;

            SaveDirectory = Path.Combine(Server.BiomesFolder, "Terrains");
            BiomesFolder = Server.BiomesFolder;

            if (!Directory.Exists(SaveDirectory))
                Directory.CreateDirectory(SaveDirectory);

            if (GeneralLog.DrawLogs)
            {
                GeneralLog.Log("Biomes Directory :" + BiomesFolder);
                GeneralLog.Log("Save Directory :" + SaveDirectory);
            }
        }

        #region Terrain

        public void Save(TerrainBlockServer Block)
        {
            if (!Information.SaveEnabled)
                return;

            if (!Block.HasState(TerrainBlockStates.GENERATED) || Block.Voxels == null || !Block.Voxels.IsDirty())
                return;

            if (SaveTerrain(Block))
            {

            }
        }

        public void Load(TerrainBlockServer Block)
        {
            if (!Information.LoadEnabled || Block.HasState(TerrainBlockStates.LOADED))
                return;

            if (LoadTerrain(Block))
            {
                Block.SetState(TerrainBlockStates.LOADED, true);
                Block.SetState(TerrainBlockStates.GENERATED, true);
            }
        }

        /// <summary>
        /// Save TerrainBlock Voxels. Override this function for custom save folders
        /// </summary>
        public virtual bool SaveTerrain(TerrainBlockServer Block)
        {
            string FilePath = SaveDirectory;
            Directory.CreateDirectory(FilePath);
            FilePath = Path.Combine(FilePath, Block.ToString());

            if(GeneralLog.DrawLogs)
                GeneralLog.Log("TerrainBlockServer", Block.LocalPosition + " Saving Block : " + FilePath);

            using (FileStream Stream = new FileStream(FilePath, FileMode.Create))
            {
                using (ZOutputStream ZStream = new ZOutputStream(Stream, (int)Information.CompressionMode))
                {
                    ZStream.FlushMode = zlibConst.Z_FULL_FLUSH;

                    using (MemoryStream MStream = new MemoryStream())
                    {
                        using (BinaryWriter Writer = new BinaryWriter(MStream))
                        {
                            lock (Block.Voxels)
                                SaveData(Block.Voxels, Writer);

                            MStream.Position = 0;
                            byte[] Uncompresssed = new byte[MStream.Length];
                            MStream.Read(Uncompresssed, 0, Uncompresssed.Length);
                            ZStream.Write(Uncompresssed, 0, Uncompresssed.Length);
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Load Voxels from a Terrain. Override this function for custom save folders
        /// </summary>
        public virtual bool LoadTerrain(TerrainBlockServer Block)
        {
            //WorldFolder.GetTerrainFolder(Information) + "\\Terrains";

            string FilePath = SaveDirectory;
            Directory.CreateDirectory(FilePath);
            FilePath = Path.Combine(FilePath, Block.ToString());
            if (!File.Exists(FilePath))
            {
                return false;
            }

            if(GeneralLog.DrawLogs)
                GeneralLog.Log("TerrainBlockServer", Block.LocalPosition + " Loading Block " + FilePath);

            using (FileStream Stream = new FileStream(FilePath, FileMode.Open))
            {
                byte[] FileArray = new byte[Stream.Length];
                Stream.Read(FileArray, 0, (int)Stream.Length);

                using (MemoryStream TempStream = new MemoryStream())
                {
                    using (ZOutputStream ZStream = new ZOutputStream(TempStream, true))
                    {
                        ZStream.Write(FileArray, 0, (int)Stream.Length);
                        TempStream.Position = 0;

                        using (BinaryReader Reader = new BinaryReader(TempStream))
                        {
                            lock(Block.Voxels)
                                ReadData(Block.Voxels, Reader);
                        }
                    }
                }
            }

            Block.SetState(TerrainBlockStates.LOADED, true);
            Block.SetState(TerrainBlockStates.GENERATED, true);
            return true;
        }


        public virtual void SaveData(TerrainDatas TData, BinaryWriter Stream)
        {
            Stream.Write((byte)1); // SaveVersion. Used for add future data, and retro-compatibility
            TData.Save(Stream);
            //GeneralLog.Log("Terrain Size:" + Stream.BaseStream.Length);
            Stream.Write((byte)TData.Customs.Count);

            long Size = 0, i = 0;
            using (MemoryStream TempStream = new MemoryStream())
            {
                using (BinaryWriter TempWriter = new BinaryWriter(TempStream))
                {
                    for (int j = 0; j < TData.Customs.Count; ++j)
                    {
                        TempStream.Position = 0;
                        TData.Customs.array[j].Save(TempWriter);

                        Size = TempStream.Position;
                        //GeneralLog.Log("Customs " + Customs.array[j] + ",Size:" + Size);
                        Stream.Write(TData.Customs.array[j].Name);
                        Stream.Write((int)Size);
                        Stream.Write(TData.Customs.array[j].UID);

                        TempStream.Position = 0;
                        for (i = 0; i < Size; ++i)
                            Stream.Write((byte)TempStream.ReadByte());
                    }
                }
            }

            TData.Dirty = false;
        }

        public virtual void ReadData(TerrainDatas TData, BinaryReader Stream)
        {
            byte SaveVersion = Stream.ReadByte();
            //GeneralLog.Log("Read Terrain Size:" + Stream.BaseStream.Length);
            TData.Read(Stream);

            int CustomCount = Stream.ReadByte();
            long Size = 0;
            byte ProcessorID = 0;
            string Name;
            ICustomTerrainData Data;

            for (int i = 0; i < CustomCount; ++i)
            {
                Name = Stream.ReadString();
                Size = Stream.ReadInt32();
                ProcessorID = Stream.ReadByte();

                //GeneralLog.Log(Position + " Reading:" + Name + ",Size:" + Size + ",Remain:" + (Stream.BaseStream.Length - Stream.BaseStream.Position) + ",ProcessorID:" + ProcessorID);
                Data = TData.GetCustomData<ICustomTerrainData>(Name);
                if (Data != null)
                {
                    Data.UID = ProcessorID;
                    Data.Read(Stream);
                }
                else
                {
                    //GeneralLog.LogError(Position + " i :" + i + ", Invalid Custom Data : " + Name + ",Skipping bytes:" + Size + ",Position:" + Stream.BaseStream.Position);
                    Stream.BaseStream.Position = Stream.BaseStream.Position + Size;
                }
            }

            TData.Dirty = false;
        }

        #endregion

        #region Biomes XML

        /// <summary>
        /// Load all biomes generators
        /// </summary>
        public void LoadBiomes(WorldGenerator Generator)
        {
            LoadGeneratorsTypes(Generator);
            LoadGenerators(Generator);
            Generator.SortGenerators();
        }

        /// <summary>
        /// Load all classes inheriting from IWorldGenerator
        /// </summary>
        public void LoadGeneratorsTypes(WorldGenerator Generator)
        {
            string Name = "";
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.IsClass != true)
                        continue;

                    if (!type.IsSubclassOf(typeof(IWorldGenerator)))
                        continue;

                    Name = type.Name;

                    if (GeneralLog.DrawLogs)
                        GeneralLog.Log("[Registering] Generator class : " + Name);

                    if (!Generator.GeneratorsTypes.ContainsKey(Name))
                        Generator.GeneratorsTypes.Add(Name, type);
                }
            }
        }

        /// <summary>
        /// Instanciate Generators
        /// </summary>
        public void LoadGenerators(WorldGenerator Generator)
        {
            foreach (WorldGenerator.GeneratorInformation Info in Generator.GeneratorsFiles)
            {
                Type T = Generator.GeneratorsTypes[Info.ClassName];
                Generator.AddGenerator(XmlMgr.GetXMLFile<IWorldGenerator>(T, BiomesFolder + "\\" + Info.FileName + ".xml"), false);
            }
        }

        #endregion
    }
}
