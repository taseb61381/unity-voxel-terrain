﻿using System.Collections.Generic;

namespace TerrainEngine
{
    public enum TerrainOpcodes : byte
    {
        SEND_GENERATING = 0,
        SEND_ABORD = 1,
        SEND_CREATE = 2,
        SEND_ALLOWED = 3,
        SEND_OPERATOR = 4,
        SEND_SCRIPT = 5,
    }

    /// <summary>
    /// This interface will manage client/server code. LocalTerrainObjectHandler will directly draw blocks and remove block client side. NetworkTerrainObjectHandler will send packets to server and wait result for creating blocks etc
    /// </summary>
    public abstract class ITerrainObjectHandlers
    {
        public TerrainObjectServer Owner;

        public abstract void Init(TerrainObjectServer Owner);

        public abstract void SendGeneratingBlock(VectorI3 Position, float Pct); // Server inform client that block is generating. Can be called multiple time for update % is generation time is longer or delayed

        public abstract void SendGeneratingAbord(VectorI3 Position); // Server inform client that block generation is stopped, no more players on it or error

        public abstract void CreateBlock(TerrainBlockServer Block); // Server send block data to client

        public abstract void SendAllowedBlocks(List<VectorI3> Positions); // Server send allowed blocks, client check and remove blocks who are too far

        public virtual void SendOperators(SList<ITerrainOperator> Voxels) { } // Send to server all flush voxels from other clients

        public virtual void SendModifiedCustomData(List<ServerCustomDataModification> Datas) { } // Send to server all flush from other clients

        public virtual string GetName()
        {
            return "Local-" + Owner.Id;
        }

        #region Processors

        public virtual void SendProcessorData(byte ProcessorID, ushort x, ushort y, ushort z, byte Type) { } // Add or Remove a processor data (Usefull for gameobjects, removing trees for example)

        #endregion


        #region Distant Objects

        public abstract void SendMeTo(TerrainObjectServer Object);

        public abstract void OnAddObjectInRange(TerrainObjectServer Object);

        public abstract void OnRemoveObjectInRange(TerrainObjectServer Object);

        #endregion
    }
}
