﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace TerrainEngine
{
    /// <summary>
    /// This class manage all ActionThread
    /// </summary>
    public class ThreadManager
    {
        static public bool IsEditor = false;        // Thread-safe access
        static public bool IsWebPlayer = false;     // Thread-safe access
        static public bool IsPlaying = false;       // Thread-safe access
        static public float UnityTime = 0f;         // Thread-safe access
        static public bool EnableStats = false;     // True if generation time and actions times must be recorded. Can slow down the generation
        static public bool IsClientWorking = false; // True if client is working on something important (Like LOD), Terrain Generation will be stopped until client finished
        static public bool IsServerWorking = false; // True if server is generating a terrain
        static public ThreadManager Instance;

        public delegate void OnActionDelegate(int ThreadId, IAction Action);
        public delegate void OnAddStatDelegate(int ThreadId, string Name, long MSTime, bool IsMain);

        public ActionThread[] Threads;
        public ActionThread FastThread;
        public OnActionDelegate OnAction; // Called when action is Executed
        public OnAddStatDelegate OnStat; // This function is called by threads when stat is added (stats are time used by an action)

        public int MaxThreadCount = 0;              // Number of threads to create. 0 = Environement.CoreCount.
        public int ThreadSleepMSTime = 50;
        public int MaxThreadedActionTime = 50;
        public bool EnablePooling = true;
        public DateTime StartTime;

        public int FastCount = 0;
        public TQueue<IAction> FastActions = new TQueue<IAction>();
        public void AddFast(IAction Action)
        {
            lock (Action)
            {
                ++Action.WorkingCount;
                Action.Progress = 0.0f;

            }

            FastActions.Enqueue(Action);
            FastCount = FastActions.Count;
        }
        public bool GetFast(ref IAction Action)
        {
            if (FastCount == 0)
                return false;

            Action = FastActions.Dequeue();
            FastCount = FastActions.Count;

            return true;
        }

        /// <summary>
        /// Start all threads (Environement.CoreCount-2)
        /// </summary>
        public void Start()
        {
            if (Threads == null || Threads.Length == 0)
            {
                GeneralLog.Log("ThreadManager : Start...");

                Instance = this;

                StartTime = DateTime.UtcNow;
                int CoreCount = Environment.ProcessorCount;
                if (MaxThreadCount != 0)
                    CoreCount = MaxThreadCount;
                else
                    CoreCount = Math.Max(CoreCount - 1, 1);

                Threads = new ActionThread[CoreCount];
                ActionThread Thread;

                for (int i = 0; i < CoreCount; ++i)
                {
                    GeneralLog.Log("ThreadManager : Creating thread " + i);
                    Thread = Threads[i] = new ActionThread(this, EnablePooling, i);
                    Thread.Pool.EnablePooling = EnablePooling;
                    Thread.StartThread();
                }

                FastThread = new ActionThread(this, true, -2);
                FastThread.StartThread();
            }
        }

        /// <summary>
        /// Stop all threads
        /// </summary>
        public void Stop()
        {
            GeneralLog.Log("ThreadManager : Stop...");

            if (Threads == null || Threads.Length == 0)
            {
                throw new Exception("ThreadManager : No thread to stop");
            }

            for (int i = 0; i < Threads.Length; ++i)
            {
                Threads[i].isRunning = false;
            }

            for (int i = 0; i < Threads.Length; ++i)
            {
                Threads[i].thread.Join();
            }
        }

        /// <summary>
        /// Add a new action to execute to selected thread
        /// </summary>
        /// <param name="Action">Action</param>
        /// <param name="ThreadId">ThreadId : 0 -> CoreCount. int.max for use the fastest thread</param>
        public void AddAction(IAction Action, int ThreadId, int MaxThreadId = int.MaxValue)
        {
            if (ThreadId < 0 || ThreadId >= Threads.Length)
            {
                int MaxActionCount = int.MaxValue;
                ActionThread Thread = null;

                for (int i = 0; i < Threads.Length; ++i)
                {
                    if (i < MaxThreadId && Threads[i].TotalAction < MaxActionCount)
                    {
                        Thread = Threads[i];
                        MaxActionCount = Thread.TotalAction;
                    }
                }

                if(Thread != null)
                    Thread.AddAction(Action);
            }
            else
                Threads[ThreadId].AddAction(Action);
        }

        /// <summary>
        /// Add a new action to execute on available thread (less used)
        /// </summary>
        /// <param name="Action"></param>
        public void AddAction(IAction Action)
        {
            AddAction(Action, int.MaxValue);
        }
    }
}
