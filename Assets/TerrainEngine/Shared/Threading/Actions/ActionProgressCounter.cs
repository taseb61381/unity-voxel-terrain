﻿using System;
using System.Collections.Generic;

using TerrainEngine;

/// <summary>
/// This class contains an action list and return Progress or IsDone if all actions are done
/// </summary>
public class ActionProgressCounter : IAction
{
    public List<IAction> Actions = new List<IAction>();

    public void Add(IAction Action)
    {
        Actions.Add(Action);
    }

    public void Remove(IAction Action)
    {
        Actions.Remove(Action);
    }

    public int Count()
    {
        return Actions.Count;
    }

    public override void Clear()
    {
        Actions.Clear();
    }

    public float TotalProgress
    {
        get
        {
            float Count = Actions.Count;
            if (Count == 0)
                return 100.0f;

            float TotalProgress = 0.0f;
            foreach (IAction Action in Actions.ToArray())
                TotalProgress += Action.Progress;
            return TotalProgress / Count;
        }
    }

    public int DoneCount
    {
        get
        {
            int Count = Actions.Count-1;
            int Done = 0;
            for (; Count >= 0; --Count)
            {
                if (Actions[Count].IsDone())
                    ++Done;
            }

            return Done;
        }
    }

    public override bool IsDone()
    {
        return DoneCount == Actions.Count;
    }

    public float DoneProgress
    {
        get
        {
            int Count = Actions.Count;
            if (Count > 0)
                return (float)DoneCount / (float)Count * 100.0f;
            else
                return 100f;
        }
    }

    public override string ToString()
    {
        string Str = "";
        Str+="Count:" + DoneCount + ",Total:" + Actions.Count;
        foreach (IAction Action in Actions)
            Str += "(" + Action + ")";
        return Str;
    }
}
