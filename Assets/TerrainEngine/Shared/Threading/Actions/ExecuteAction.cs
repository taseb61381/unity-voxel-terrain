﻿

using TerrainEngine;

public class ExecuteAction : IAction
{
    public delegate bool ExecuteActionDelegate(ActionThread Thread);
    public delegate void ExecuteActionDelegateVoid();

    public ExecuteActionDelegate action;
    public ExecuteActionDelegateVoid actionvoid;
    public string Name = "";

    public ExecuteAction(string Name, ExecuteActionDelegate action)
    {
        this.Name = Name;
        this.action = action;
    }

    public ExecuteAction(string Name, ExecuteActionDelegateVoid actionvoid)
    {
        this.Name = Name;
        this.actionvoid = actionvoid;
    }

    public override bool OnExecute(ActionThread Thread)
    {
        if (action != null)
            return action.Invoke(Thread);

        if (actionvoid != null)
            actionvoid.Invoke();
        return true;
    }

    public override string ToString()
    {
        return Name;
    }
}