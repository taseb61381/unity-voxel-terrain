﻿using System;


namespace TerrainEngine
{
    public abstract class IAction : IPoolable
    {
        public delegate void ActionDoneCallback(ActionThread Thread, IAction Action);

        [NonSerialized]
        public float Progress = 0.0f;

        public bool MustClose = false;

        public byte WorkingCount = 0; // Number of threads working on this Action 

        public virtual bool IsDone()
        {
            return WorkingCount == 0;
        }

        public override void Close()
        {
            MustClose = true;
        }

        /// <summary>
        /// Main function to be called from a class action thread.
        /// </summary>
        /// <param name="Thread">Thread that executes the current action.</param>
        /// <returns>Returns true if the action may be removed from the list.</returns>
        public virtual bool Execute(ActionThread Thread)
        {
            return OnExecute(Thread);
        }

        /// <summary>
        /// Virtual function to implement in classes inheriting IAction
        /// </summary>
        /// <param name="isthread"></param>
        /// <returns>Returns true if the action may be removed from the list.</returns>
        public virtual bool OnExecute(ActionThread Thread)
        {
            if (Thread.ThreadId == -1)
                return OnUpdate(Thread);
            else
                return OnThreadUpdate(Thread);
        }

        /// <summary>
        /// Unity Thead Update
        /// </summary>
        /// <returns>Returns true if the action may be removed from the list.</returns>
        public virtual bool OnUpdate(ActionThread Thread) { return true; }

        /// <summary>
        /// Separate Thread Update
        /// </summary>
        /// <returns>Returns true if the action may be removed from the list.</returns>
        public virtual bool OnThreadUpdate(ActionThread Thread) { return true; }

        public override void Clear()
        {
            MustClose = false;
            WorkingCount = 0;
            Progress = 0f;
        }
    }
}