using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using TerrainEngine;
using UnityEngine;

public class ActionThread 
{
    public ThreadManager Manager;
    public int ThreadId;
    public Thread thread;
    public bool isRunning = true;

    public long ExecutionTime = 0;
    public int ActionCount = 0;
    public int MaxActionTime = 0;
    public IAction LastAction = null;
    public TransitionQueue<IAction> Actions = new TransitionQueue<IAction>(); // Actions to execute in list
    public PoolManager Pool;
    public long ExtraTime = 0; // GameObject Creation ExtraTime;

    private long StartTime;

    public bool IsUnityThread()
    {
        return ThreadId == -1;
    }

    public Stopwatch TimeCounter = new Stopwatch();

    public ActionThread(ThreadManager Manager, bool EnablePooling, int ThreadId)
    {
        this.Manager = Manager;
        this.ThreadId = ThreadId;
        this.Pool = new PoolManager(EnablePooling,  ThreadId);
        this.Pool.ThreadId = ThreadId;
        this.Pool.EnablePooling = EnablePooling;
    }

    /// <summary>
    /// Returns the total number of action required or being performed on this thread
    /// </summary>
    public int TotalAction
    {
        get
        {
            return Actions.WaitingCount + ActionCount;
        }
    }

    public void StartThread()
    {
        ThreadStart TS = new ThreadStart(ThreadUpdate);
        thread = new Thread(TS);
        thread.IsBackground = true;
        thread.Start();
    }

    public void ThreadUpdate()
    {
        while (isRunning)
        {
            try
            {
                if (ThreadId != -2)
                {
                    Execute(Manager.MaxThreadedActionTime);
                    Thread.Sleep(Mathf.Max(10, (int)(Manager.ThreadSleepMSTime - ExecutionTime)));
                }
                else
                {
                    Execute(10);
                    Thread.Sleep(10);
                }
            }
            catch (Exception e)
            {
                GeneralLog.LogError(e.ToString());
            }
        }

        GeneralLog.Log("ActionThread : Stop ID:" + ThreadId);
    }

    /// <summary>
    /// Performs a maximum action in a given time.
    /// </summary>
    /// <param name="MaxActionTime">Maximum time allowed.</param>
    public void Execute(int MaxActionTime)
    {
        ///// START //////

        this.MaxActionTime = MaxActionTime;
        Times.Clear();
        Actions.Process();

        ActionCount = Actions.Count;
        int Count = ActionCount;

        TimeCounter.Reset();
        TimeCounter.Start();

        if (ThreadId != -1 && ThreadId != -2)
        {
            try
            {
                ExecuteFastActions();
            }
            catch(Exception e)
            {
                GeneralLog.LogError(e.ToString());
            }
        }
        

        if (!HasTime())
            return;

        IAction Action = null;
        while (Actions.List.Count != 0 && Count > 0 && isRunning)
        {
            LastAction = Action;
            Action = Actions.List.Dequeue();
            --Count;

            if (Action != null)
            {
                StartTime = TimeCounter.ElapsedMilliseconds;

                if (Manager.OnAction != null)
                    Manager.OnAction(ThreadId, Action);

                if (ThreadId == -1)
                {
                    if (Action.Execute(this))
                    {
                        lock (Action)
                        {
                            if (Action.WorkingCount > 0)
                                --Action.WorkingCount;
                            else
                                GeneralLog.LogError("Invalid WorkingCount:" + Action);
                            Action.Progress = 100.0f;

                            if (Action.WorkingCount == 0 && Action.MustClose)
                            {
                                Action.ForceClose();
                            }
                        }
                    }
                    else
                        Actions.List.Enqueue(Action);
                }
                else
                {

                    try
                    {
                        if (Action.Execute(this))
                        {
                            lock (Action)
                            {
                                if (Action.WorkingCount > 0)
                                    --Action.WorkingCount;
                                else
                                    GeneralLog.LogError("Invalid WorkingCount:" + Action);
                                Action.Progress = 100.0f;

                                if (Action.WorkingCount == 0 && Action.MustClose)
                                {
                                    Action.ForceClose();
                                }
                            }
                        }
                        else
                            Actions.List.Enqueue(Action);
                    }
                    catch (Exception e)
                    {
                        GeneralLog.LogError(e.ToString());
                    }
                }

                if (ThreadManager.EnableStats)
                {
                    AddStats(Action.ToString(), TimeCounter.ElapsedMilliseconds - StartTime, true);
                }
                LastAction = null;
            }

            if (!HasTime())
                break;
        }

        TimeCounter.Stop();

        ////// END ///////

        ExecutionTime = TimeCounter.ElapsedMilliseconds;
    }

    public void ExecuteFastActions()
    {
        IAction Action = null;
        while (Manager.GetFast(ref Action))
        {
            LastAction = Action;
            Action.Execute(this);

            lock (Action)
            {
                if (Action.WorkingCount > 0)
                    --Action.WorkingCount;
                else
                    GeneralLog.LogError("Invalid WorkingCount:" + Action);

                Action.Progress = 100.0f;
            }

            if (Action.MustClose)
            {
                Action.ForceClose();
            }

            LastAction = null;

            if (!HasTime())
                break;
        }
    }

    public bool HasTime()
    {
        if (TimeCounter.ElapsedMilliseconds + ExtraTime >= MaxActionTime || !isRunning)
            return false;

        return true;
    }

    public bool HasTime(int Offset)
    {
        if (TimeCounter.ElapsedMilliseconds + ExtraTime >= MaxActionTime + Offset)
            return false;

        if (!isRunning)
            return false;

        return true;
    }

    private Stack<long> Times = new Stack<long>();

    /// <summary>
    /// Use this function for record the current time. Used for add Stats
    /// Thread.RecordTime();
    /// Thread.AddStats("YourName",Thread.Time());
    /// </summary>
    public void RecordTime()
    {
        Times.Push(TimeCounter.ElapsedMilliseconds);
    }

    /// <summary>
    /// Use this function for record the current time. Used for add Stats
    /// Thread.RecordTime();
    /// Thread.AddStats("YourName",Thread.Time());
    /// </summary>
    public long Time()
    {
        if (Times.Count != 0)
            return TimeCounter.ElapsedMilliseconds - Times.Pop();
        return 0;
    }

    /// <summary>
    /// Add an action to the waiting list of actions to execute.
    /// </summary>
    /// <param name="Action"></param>
    public void AddAction(IAction Action)
    {
        lock (Action)
        {
            ++Action.WorkingCount;
            Action.Progress = 0.0f;

        }
        Actions.AddUnsafe(Action);
    }

    public void AddAction(IAction[] Action)
    {
        for (int i = Action.Length - 1; i >= 0; --i)
        {
            lock (Action)
            {
                ++Action[i].WorkingCount;
                Action[i].Progress = 0.0f;

            }
        }
        Actions.AddRangeUnsafe(Action);
    }

    #region Stats

    public SList<ActionStats> Stats = new SList<ActionStats>(10);

    /// <summary>
    /// Saves the execution times of an action.
    /// </summary>
    /// <param name="Name">Action name</param>
    /// <param name="MSTime">Execution time in ms</param>
    public void AddStats(string Name, long MSTime,bool IsMain=false)
    {
        if (Name == null || Name == "")
            return;

        if (Manager.OnStat != null)
            Manager.OnStat(ThreadId, Name, MSTime, IsMain);

        if (MSTime == 0)
            return;

        lock (Stats.array)
        {
            for (int i = Stats.Count - 1; i >= 0; --i)
            {
                if (Stats.array[i].ActionName == Name)
                {
                    ++Stats.array[i].ExecutionCount;

                    Stats.array[i].LastMSTime = MSTime;
                    Stats.array[i].TotalMSTime += MSTime;

                    if (MSTime > Stats.array[i].MaxTime)
                        Stats.array[i].MaxTime = MSTime;

                    if (Stats.array[i].ExecutionCount >= int.MaxValue)
                    {
                        Stats.array[i].ExecutionCount = 0;
                        Stats.array[i].TotalMSTime = 0;
                    }

                    return;
                }
            }


            ActionStats Stat = new ActionStats(Name);
            Stat.ThreadID = ThreadId;
            ++Stat.ExecutionCount;
            Stat.LastMSTime = MSTime;
            Stat.TotalMSTime += MSTime;
            Stat.IsMain = IsMain;
            if (MSTime > Stat.MaxTime)
                Stat.MaxTime = MSTime;

            if (Stat.ExecutionCount >= int.MaxValue)
            {
                Stat.ExecutionCount = 0;
                Stat.TotalMSTime = 0;
            }
            Stats.Add(Stat);
        }
    }

    /// <summary>
    /// Returns a list of execution times
    /// </summary>
    /// <returns></returns>
    public delegate void CallGUI(ActionStats Stat);
    public void GetStats(CallGUI del)
    {
        lock (Stats.array)
        {
            long ExecutionCount = 0;
            long TotalMSTime = 0;
            long MaxTime = 0;

            for (int j = 0; j < Stats.Count; ++j)
            {
                if (Stats.array[j].IsMain)
                {
                    ExecutionCount += Stats.array[j].ExecutionCount;
                    TotalMSTime += Stats.array[j].TotalMSTime;
                    if (Stats.array[j].MaxTime > MaxTime)
                        MaxTime = Stats.array[j].MaxTime;
                }
            }
            ActionStats Stat = new ActionStats();
            Stat.ActionName = "Global";
            Stat.ExecutionCount = ExecutionCount;
            Stat.TotalMSTime = TotalMSTime;
            Stat.MaxTime = MaxTime;
            del(Stat);

            for (int j = 0; j < Stats.Count; ++j)
            {
                del(Stats.array[j]);
            }
        }

    }

    #endregion
}
