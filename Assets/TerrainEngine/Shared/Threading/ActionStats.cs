﻿
public struct ActionStats
{
    public ActionStats(string Name)
    {
        ActionName = Name;
        ExecutionCount = 0;
        TotalMSTime = 0;
        LastMSTime = 0;
        MaxTime = 0;
        ThreadID = 0;
        IsMain = false;
    }
    public string ActionName;
    public long ExecutionCount;
    public long TotalMSTime;
    public long LastMSTime;
    public long MaxTime;
    public int ThreadID;
    public bool IsMain;

    public override string ToString()
    {
        return ActionName + " : \nCount=" + ExecutionCount + "\nTime=" + TotalMSTime + "\nLastTime=" + LastMSTime + "\nMaxTime=" + MaxTime + "\nAvg=" + (float)((float)TotalMSTime / (float)ExecutionCount);
    }
}