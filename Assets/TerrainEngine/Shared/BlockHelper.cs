﻿using System.Collections.Generic;

using UnityEngine;


public class Grid : MonoBehaviour 
{
        public GameObject plane;
        public int width = 1;
        public int height = 1;
 
        private GameObject [,] grid;
 
        void Awake()
        {
            grid = new GameObject[50, 50];
            Vector3 Position = new UnityEngine.Vector3(); // Declare your struct and reuse it multiple time is fastest than creating a new Vector3() on a for loop
            int x,z; // Declare variables that will be use multiple time. Here , you don"t need for(int z;;), it create z forearch x, so it's slower

            for (x = 0; x < width; x++)
            {
                Position.x= x; // Fastest to assign X only when x change than on all z change

                for (z = 0; z < height; z++)
                {
                    Position.z = z;
                    grid[x,z]= GameObject.Instantiate(plane,Position,plane.transform.rotation) as GameObject; // Creating a new object and changing as GameObject 
                    grid[x,z].name = Position.ToString(); // Converting Vector3 to a string and set it as name of gameobject;
                }
            }
        }
 
        void Start () 
        {
       
        }
 
        void Update () 
        {
       
        }
}

public static class BlockHelper
{
    public const int facescount = 6; //6
    public const int verticescount = 5; //5
    public const int trianglescount = 4; //4

    public enum BlockFace : byte
    {
        Top = 0,
        Side = 1,
        Bottom = 2
    }


    public enum Direction
    {
        Left = 0,
        Right = 1,
        Top = 2, Up = 2,
        Bottom = 3, Down = 3,
        Forward = 4, Front = 4,
        Backward = 5, Back = 5,

        LeftTopFront = 6,
        RightBottomBack = 7,
        LeftBottomFront = 8,
        RightTopBack = 9,
        LeftTopBack = 10,
        RightBottomFront = 11,
        LeftBottomBack = 12,
        RightTopFront = 13,

        LeftFront = 14,
        RightBack = 15,
        LeftBack = 16,
        RightFront = 17,
        LeftTop = 18,
        RightBottom = 19,
        LeftBottom = 20,
        RightTop = 21,
        TopFront = 22,
        BottomBack = 23,
        TopBack = 24,
        BottomFront = 25
    }

    public enum CVIToDirection
    {
        LeftTopFront = 6,
        RightTopFront = 13,
        LeftTopBack = 10,
        RightTopBack = 9,
        LeftBottomFront = 8,
        RightBottomFront = 11,
        LeftBottomBack = 12,
        RightBottomBack = 7
    }

    public enum CubeVertexIndex
    {
        LeftTopFront = 0,
        RightTopFront = 1,
        LeftTopBack = 2,
        RightTopBack = 3,
        LeftBottomFront = 4,
        RightBottomFront = 5,
        LeftBottomBack = 6,
        RightBottomBack = 7
    }

    public static readonly Vector3[] leftFaceVertices = new Vector3[] {
		new Vector3(0f, 1f, 1f), // 
        new Vector3(0f, 1f, 0f),
		new Vector3(0f, 0f, 1f), 
        new Vector3(0f, 0f, 0f), 
        new Vector3(0f, 0.5f, 0.5f)
	};
    public static readonly Vector3[] rightFaceVertices = new Vector3[] {
		new Vector3(1f, 1f, 0f), new Vector3(1f, 1f, 1f),
		new Vector3(1f, 0f, 0f), new Vector3(1f, 0f, 1f)
		, new Vector3(1f, 0.5f, 0.5f)
	};
    public static readonly Vector3[] topFaceVertices = new Vector3[] {
		new Vector3(0f, 1f, 1f), // Left
        new Vector3(1f, 1f, 1f), // Right
		new Vector3(0f, 1f, 0f), // Left BOTTOM
        new Vector3(1f, 1f, 0f), // Righ BOTTOM
        new Vector3(0.5f, 1f, 0.5f) // Center
	};
    public static readonly Vector3[] bottomFaceVertices = new Vector3[] {
		new Vector3(0f, 0f, 0f), new Vector3(1f, 0f, 0f),
		new Vector3(0f, 0f, 1f), new Vector3(1f, 0f, 1f)
		, new Vector3(0.5f, 0f, 0.5f)
	};
    public static readonly Vector3[] frontFaceVertices = new Vector3[] {
		new Vector3(1f, 1f, 1f), new Vector3(0f, 1f, 1f),
		new Vector3(1f, 0f, 1f), new Vector3(0f, 0f, 1f)
		, new Vector3(0.5f, 0.5f, 1f)
	};
    public static readonly Vector3[] backFaceVertices = new Vector3[] {
		new Vector3(0f, 1f, 0f), new Vector3(1f, 1f, 0f),
		new Vector3(0f, 0f, 0f), new Vector3(1f, 0f, 0f)
		, new Vector3(0.5f, 0.5f, 0f)
	};

    public static readonly Vector3[][] faceVertices = new Vector3[BlockHelper.facescount][] {
		leftFaceVertices,
		rightFaceVertices,
		topFaceVertices,
		bottomFaceVertices,
		frontFaceVertices,
		backFaceVertices
	};

    public static readonly BlockFace[] facesEnum = new BlockFace[6]
    {
        BlockFace.Side,
        BlockFace.Side,
        BlockFace.Top,
        BlockFace.Bottom,
        BlockFace.Side,
        BlockFace.Side
    };

    public static readonly Vector2[] uvs = new Vector2[]{
		Vector2.up / 16f, 
        Vector2.one / 16f,
		Vector2.zero,
        Vector2.right / 16f,
		Vector2.one / 32f
	};

    public static readonly ushort[] triangles = new ushort[]{
		/*0,1,2,
		1,3,2*/
		0,1,4,
		1,3,4,
		3,2,4,
		2,0,4
		
	};

    public static readonly VectorI3[] direction = new VectorI3[]
    {
        new VectorI3(-1, 0, 0),  // Left 0
	    new VectorI3(1, 0, 0),   // Right 1
	    new VectorI3(0, 1, 0),   // Up 2
        new VectorI3(0, -1, 0),  // Down 3
	    new VectorI3(0, 0, 1),   // forward 4
	    new VectorI3(0, 0, -1),  //  back 5
		
		//Corner
		new VectorI3(-1,1,1), // 6
		new VectorI3(1,-1,-1), // 7

		new VectorI3(-1,-1,1), // 8
		new VectorI3(1,1,-1), // 9

		new VectorI3(-1,1,-1), // 10
		new VectorI3(1,-1,1), // 11

		new VectorI3(-1,-1,-1), // 12
		new VectorI3(1,1,1), // 13

		//Edge Corner
		new VectorI3(-1,0,1), //Left Front 14
		new VectorI3(1,0,-1), //Right Back 15
		new VectorI3(-1,0,-1), //Left Back 16
		new VectorI3(1,0,1), //Right Front 17

		new VectorI3(-1,1,0), //Left Top 18
		new VectorI3(1,-1,0), //Right Bottom 19
		new VectorI3(-1,-1,0), //Left Bottom 20
		new VectorI3(1,1,0), //Right Top 21

		new VectorI3(0,1,1), //Top Front 22
		new VectorI3(0,-1,-1), //Bottom Back 23
		new VectorI3(0,1,-1), //Top Back 24
		new VectorI3(0,-1,1), //Bottom Front 25
	};

    //Get which vertices are closer to a certain neighbor or side of the block
    public static readonly byte[][][] vertexPositionIndex = new byte[][][]{
		//Max Vertices 4
		new byte[][]{//** Left Neighbor **
			new byte[] { 0,1,2,3 }, //Left face
			new byte[0],	//Right face
			new byte[] { 0,2 }, //Top face
			new byte[] { 1,3 }, //Bottom face
			new byte[] { 1,3 }, //Front face
			new byte[] { 0,2 } //Back face
		},
		
		new byte[][]{//** Right Neighbor **
			new byte[0], //Left face
			new byte[] { 0,1,2,3 }, //Right face
			new byte[] { 1,3 }, //Top face
			new byte[] { 0,2 }, //Bottom face
			new byte[] { 0,2 }, //Front face
			new byte[] { 1,3 } //Back face
		},

		new byte[][]{//** Top Neighbor **
			new byte[] { 0,1 }, //Left face
			new byte[] { 0,1 },	//Right face
			new byte[] { 0,1,2,3 }, //Top face
			new byte[0], //Bottom face
			new byte[] { 0,1 }, //Front face
			new byte[] { 0,1 } //Back face
		},
		
		new byte[][]{//** Bottom Neighbor **
			new byte[] { 2,3 }, //Left face
			new byte[] { 2,3 }, //Right face
			new byte[0], //Top face
			new byte[] { 0,1,2,3 }, //Bottom face
			new byte[] { 2,3 }, //Front face
			new byte[] { 2,3 } //Back face
		},

		new byte[][]{//** Front Neighbor **
			new byte[] { 0,2 }, //Left face
			new byte[] { 1,3 },	//Right face
			new byte[] { 0,1 }, //Top face
			new byte[] { 2,3 }, //Bottom face
			new byte[] { 0,1,2,3 }, //Front face
			new byte[0] //Back face
		},
		
		new byte[][]{//** Back Neighbor **
			new byte[] { 1,3 }, //Left face
			new byte[] { 0,2 }, //Right face
			new byte[] { 2,3 }, //Top face
			new byte[] { 0,1 }, //Bottom face
			new byte[0], //Front face
			new byte[] { 0,1,2,3 } //Back face
		},
		
		//Corners - Max Vertices 1
		//LTF - Left Top Front
		//RBB - Right Bottom Back
		//LBF - Left Bottom Front
		//RTB - Right Top Back
		//LTB - Left Top Back
		//RBF - Right Bottom Front
		//LBB - Left Bottom Back
		//RTF - Right Top Front
		
		new byte[][]{//** Left Top Front Neighbor **
			new byte[] { 0 }, //Left face
			new byte[0], //Right face
			new byte[] { 0 }, //Top face
			new byte[0], //Bottom face
			new byte[] { 1 }, //Front face
			new byte[0] //Back face
		},

		new byte[][]{//** Right Bottom Back Neighbor **
			new byte[0], //Left face
			new byte[] { 2 }, //Right face
			new byte[0], //Top face
			new byte[] { 1 }, //Bottom face
			new byte[0], //Front face
			new byte[] { 3 }  //Back face
		},

		new byte[][]{//** Left Bottom Front Neighbor **
			new byte[] { 2 }, //Left face
			new byte[0], //Right face
			new byte[0], //Top face
			new byte[] { 0 }, //Bottom face
			new byte[] { 3 }, //Front face
			new byte[0] //Back face
		},

		new byte[][]{//** Right Top Back **
			new byte[0], //Left face
			new byte[] { 0 }, //Right face
			new byte[] { 3 }, //Top face
			new byte[0], //Bottom face
			new byte[0], //Front face
			new byte[] { 1 }  //Back face
		},

		new byte[][]{//** Left Top Back **
			new byte[] { 1 }, //Left face
			new byte[0], //Right face
			new byte[] { 0 }, //Top face
			new byte[0], //Bottom face
			new byte[0], //Front face
			new byte[] { 0 }  //Back face
		},
		
		new byte[][]{//** Right Bottom Front Neighbor **
			new byte[0], //Left face
			new byte[] { 3 }, //Right face
			new byte[0], //Top face
			new byte[] { 2 }, //Bottom face
			new byte[] { 2 }, //Front face
			new byte[0] //Back face
		},

		new byte[][]{//** Left Bottom Back Neighbor **
			new byte[] { 3 }, //Left face
			new byte[0], //Right face
			new byte[] { 3 }, //Top face
			new byte[0], //Bottom face
			new byte[0], //Front face
			new byte[] { 2 }  //Back face
		},

		new byte[][]{//** Right Top Front Neighbor **
			new byte[0], //Left face
			new byte[] { 1 }, //Right face
			new byte[] { 1 }, //Top face
			new byte[0], //Bottom face
			new byte[] { 0 }, //Front face
			new byte[0] //Back face
		},

		//Edge Corners - Max Vertices 2
		new byte[][]{//** Left Front **
			new byte[] { 0,2 }, //Left face
			new byte[0], //Right face
			new byte[] { 0 }, //Top face
			new byte[] { 2 }, //Bottom face
			new byte[] { 1,3 }, //Front face
			new byte[0] //Back face
		},
		new byte[][]{//** Right Back **
			new byte[0], //Left face
			new byte[] { 0,2 }, //Right face
			new byte[] { 3 }, //Top face
			new byte[] { 1 }, //Bottom face
			new byte[0], //Front face
			new byte[] { 1,3 } //Back face
		},
		new byte[][]{//** Left Back **
			new byte[] { 1,3 }, //Left face
			new byte[0], //Right face
			new byte[] { 2 }, //Top face
			new byte[] { 0 }, //Bottom face
			new byte[0], //Front face
			new byte[] { 0,2 } //Back face
		},
		new byte[][]{//** Right Front **
			new byte[0], //Left face
			new byte[] { 1,3 }, //Right face
			new byte[] { 1 }, //Top face
			new byte[] { 3 }, //Bottom face
			new byte[] { 0,2 }, //Front face
			new byte[0] //Back face
		},

		new byte[][]{//** Left Top **
			new byte[] { 0,1 }, //Left face
			new byte[0], //Right face
			new byte[] { 0,2 }, //Top face
			new byte[0], //Bottom face
			new byte[] { 1 }, //Front face
			new byte[] { 0 } //Back face
		},
		new byte[][]{//** Right Bottom **
			new byte[0], //Left face
			new byte[] { 2,3 }, //Right face
			new byte[0], //Top face
			new byte[] { 1,3 }, //Bottom face
			new byte[0], //Front face
			new byte[] { 3 } //Back face
		},
		new byte[][]{//** Left Bottom **
			new byte[] { 1,3 }, //Left face
			new byte[0], //Right face
			new byte[0], //Top face
			new byte[] { 0,2 }, //Bottom face
			new byte[] { 3 }, //Front face
			new byte[] { 2 } //Back face
		},
		new byte[][]{//** Right Top **
			new byte[0], //Left face
			new byte[] { 0,1 }, //Right face
			new byte[] { 1,3 }, //Top face
			new byte[0], //Bottom face
			new byte[] { 0 }, //Front face
			new byte[] { 1 } //Back face
		},

		new byte[][]{//** Top Front **
			new byte[] { 0 }, //Left face
			new byte[] { 1 }, //Right face
			new byte[] { 0,1 }, //Top face
			new byte[0], //Bottom face
			new byte[] { 0,1 }, //Front face
			new byte[0] //Back face
		},
		new byte[][]{//** Bottom Back **
			new byte[] { 3 }, //Left face
			new byte[] { 2 }, //Right face
			new byte[0], //Top face
			new byte[] { 0,1 }, //Bottom face
			new byte[0], //Front face
			new byte[] { 2,3 } //Back face
		},
		new byte[][]{//** Top Back **
			new byte[] { 1 }, //Left face
			new byte[] { 0 }, //Right face
			new byte[] { 2,3 }, //Top face
			new byte[0], //Bottom face
			new byte[0], //Front face
			new byte[] { 0,1 } //Back face
		},
		new byte[][]{//** Bottom Front **
			new byte[] { 2 }, //Left face
			new byte[] { 3 }, //Right face
			new byte[0], //Top face
			new byte[] { 2,3 }, //Bottom face
			new byte[] { 2,3 }, //Front face
			new byte[0] //Back face
		}
	};

    public static readonly byte[][] cornerToVertexArray = new byte[][]{
		//Vertice Indexs
		new byte[]{ 0, 0, 1 },//LTF - Left Top Front
		new byte[]{ 1, 1, 0 },//RTF - Right Top Front
		new byte[]{ 1, 2, 0 },//LTB - Left Top Back
		new byte[]{ 0, 3, 1 },//RTB - Right Top Back

		new byte[]{ 2, 2, 3 },//LBF - Left Bottom Front
		new byte[]{ 3, 3, 2 },//RBF - Right Bottom Front
		new byte[]{ 3, 0, 2 },//LBB - Left Bottom Back
		new byte[]{ 2, 1, 3 } //RBB - Right Bottom Back
	};

    public static readonly byte[][] cornerToFaceArray = new byte[][]{
		new byte[]{ 0,2,4 },//LTF
		new byte[]{ 1,2,4 },//RTF
		new byte[]{ 0,2,5 },//LTB
		new byte[]{ 1,2,5 },//RTB
		new byte[]{ 1,3,4 },//LBF
		new byte[]{ 0,3,4 },//RBF
		new byte[]{ 1,3,5 },//LBB
		new byte[]{ 0,3,5 },//RBB
	};

    public static int GetCornerIndex(Direction face, CubeVertexIndex corner)
    {
        int index = 0;

        byte[] verts = cornerToVertexArray[(byte)corner];

        switch (corner)
        {
            case CubeVertexIndex.LeftTopFront:
                switch (face)
                {
                    case Direction.Left: index = verts[0]; break;
                    case Direction.Top: index = verts[1]; break;
                    case Direction.Front: index = verts[2]; break;
                }
                break;
            case CubeVertexIndex.RightTopFront:
                switch (face)
                {
                    case Direction.Right: index = verts[0]; break;
                    case Direction.Top: index = verts[1]; break;
                    case Direction.Front: index = verts[2]; break;
                }
                break;
            case CubeVertexIndex.LeftTopBack:
                switch (face)
                {
                    case Direction.Left: index = verts[0]; break;
                    case Direction.Top: index = verts[1]; break;
                    case Direction.Back: index = verts[2]; break;
                }
                break;
            case CubeVertexIndex.RightTopBack:
                switch (face)
                {
                    case Direction.Right: index = verts[0]; break;
                    case Direction.Top: index = verts[1]; break;
                    case Direction.Back: index = verts[2]; break;
                }
                break;

            case CubeVertexIndex.LeftBottomFront:
                switch (face)
                {
                    case Direction.Left: index = verts[0]; break;
                    case Direction.Bottom: index = verts[1]; break;
                    case Direction.Front: index = verts[2]; break;
                }
                break;
            case CubeVertexIndex.RightBottomFront:
                switch (face)
                {
                    case Direction.Right: index = verts[0]; break;
                    case Direction.Bottom: index = verts[1]; break;
                    case Direction.Front: index = verts[2]; break;
                }
                break;
            case CubeVertexIndex.LeftBottomBack:
                switch (face)
                {
                    case Direction.Left: index = verts[0]; break;
                    case Direction.Bottom: index = verts[1]; break;
                    case Direction.Back: index = verts[2]; break;
                }
                break;
            case CubeVertexIndex.RightBottomBack:
                switch (face)
                {
                    case Direction.Right: index = verts[0]; break;
                    case Direction.Bottom: index = verts[1]; break;
                    case Direction.Back: index = verts[2]; break;
                }
                break;
        }

        return index;
    }

    //Modifies the vertex on each face
    public static Vector3[][] OffsetCorner(int cornerIndex, Vector3 offset)
    {
        Vector3[][] vector = new Vector3[3][]{//Faces
			new Vector3[4],//Vertices
			new Vector3[4],//Vertices
			new Vector3[4] //Vertices
		};

        int[] verts = new int[3];
        BlockHelper.cornerToVertexArray[cornerIndex].CopyTo(verts, 0);

        for (int i = 0; i < BlockHelper.cornerToFaceArray[cornerIndex].Length; i++)
        {
            vector[i][verts[i]] += offset;
        }

        return vector;
    }

    /*static public void RenderWithVAOGreedy(TerrainDatas data)
    {
        List<Vector3> vertices = new List<Vector3>();
        List<int> elements = new List<int>();

        for (int d = 0; d < 3; d++)
        {
            int i, j, k, l, w, h, u = (d + 1) % 3, v = (d + 2) % 3;
            int[] x = new int[3];
            int[] q = new int[3];
            bool[] mask = new bool[32 * 32];

            q[d] = 1;

            for (x[d] = -1; x[d] < 32; )
            {
                // Compute the mask
                int n = 0;
                for (x[v] = 0; x[v] < 32; ++x[v])
                {
                    for (x[u] = 0; x[u] < 32; ++x[u])
                    {
                        mask[n++] = (0 <= x[d] ? data.HasVolume(x[0], x[1], x[2],0) : false) !=
                            (x[d] < 32 - 1 ? data.HasVolume(x[0] + q[0], x[1] + q[1], x[2] + q[2],0) : false);
                    }
                }

                // Increment x[d]
                ++x[d];

                // Generate mesh for mask using lexicographic ordering
                n = 0;
                for (j = 0; j < 32; ++j)
                {
                    for (i = 0; i < 32; )
                    {
                        if (mask[n])
                        {
                            // Compute width
                            for (w = 1; i + w < 32 && mask[n + w]; ++w) ;

                            // Compute height (this is slightly awkward
                            var done = false;
                            for (h = 1; j + h < 32; ++h)
                            {
                                for (k = 0; k < w; ++k)
                                {
                                    if (!mask[n + k + h * 32])
                                    {
                                        done = true;
                                        break;
                                    }
                                }
                                if (done) break;
                            }

                            // Add quad
                            x[u] = i; x[v] = j;
                            int[] du = new int[3];
                            int[] dv = new int[3];
                            du[u] = w;
                            dv[v] = h;

                            AddFace(new Vector3(x[0], x[1], x[2]),
                                    new Vector3(x[0] + du[0], x[1] + du[1], x[2] + du[2]),
                                    new Vector3(x[0] + du[0] + dv[0], x[1] + du[1] + dv[1], x[2] + du[2] + dv[2]),
                                    new Vector3(x[0] + dv[0], x[1] + dv[1], x[2] + dv[2]), vertices, elements);

                            // Zero-out mask
                            for (l = 0; l < h; ++l)
                            {
                                for (k = 0; k < w; ++k)
                                {
                                    mask[n + k + l * 32] = false;
                                }
                            }

                            // Increment counters and continue
                            i += w; n += w;
                        }
                        else
                        {
                            ++i; ++n;
                        }
                    }
                }
            }
        }

        Vector3[] vertex = vertices.ToArray();
        int[] element = elements.ToArray();
        //Vector3[] normals = OpenGL.Geometry.CalculateNormals(vertex, element);
    }*/

    static public void AddFace(Vector3 a, Vector3 b, Vector3 c, Vector3 d, List<Vector3> vertices, List<int> indices)
    {
        int Count = indices.Count;
        vertices.Add(a);
        vertices.Add(b);
        vertices.Add(c);
        vertices.Add(d);
        indices.Add(0 + Count);
        indices.Add(1 + Count);
        indices.Add(2 + Count);

        indices.Add(2 + Count);
        indices.Add(3 + Count);
        indices.Add(0 + Count);
    }
}