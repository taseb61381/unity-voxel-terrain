using System;

[Serializable]
public enum VoxelTypes : byte
{
    OPAQUE = 0,
    TRANSLUCENT = 1,
    FLUID = 2,
};