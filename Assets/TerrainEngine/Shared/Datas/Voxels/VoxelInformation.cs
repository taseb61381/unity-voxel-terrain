﻿using System;
using System.Xml.Serialization;


namespace TerrainEngine
{
    [Serializable]
    public class VoxelInformation
    {
        public string Name;

        public VoxelTypes VoxelType = VoxelTypes.OPAQUE;

        [XmlIgnore, NonSerialized]
        public int Type;
    }
}
