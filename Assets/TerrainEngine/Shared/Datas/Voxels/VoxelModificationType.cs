﻿public enum VoxelModificationType : byte
{
    SET_TYPE = 1,
    SET_VOLUME = 2,
    ADD_VOLUME = 3,
    REMOVE_VOLUME = 4,
    SET_TYPE_ADD_VOLUME = 6,
    SET_TYPE_REMOVE_VOLUME = 7,
    SET_TYPE_SET_VOLUME = 8,
    SEND_EVENTS = 9, // Used to inform all processors that a voxel was modified
    NETWORK = 10,
    REMOVE_VOLUME_IF_TYPE = 11,
    ADD_VOLUME_IF_TYPE = 12,
    SET_TYPE_ADD_VOLUME_IF_EMPTY = 13,
    SET_TYPE_ADD_VOLUME_IF_EMPTY_OR_EQUAL = 14,
}