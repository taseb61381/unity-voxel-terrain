﻿using System.IO;

using UnityEngine;

namespace TerrainEngine
{
    public abstract class ICustomTerrainData : ITerrainDataSave
    {
        public int CustomIndex;
        public TerrainDatas Terrain = null;
        public TerrainInformation Information
        {
            get
            {
                return Terrain.Information;
            }
        }

        public virtual void Init(TerrainDatas Terrain)
        {
            this.Terrain = Terrain;
        }

        #region Datas

        public virtual byte GetByte(int x, int y, int z)
        {
            return 0;
        }

        public virtual int GetInt(int x, int y, int z)
        {
            return 0;
        }

        public virtual float GetFloat(int x, int y, int z)
        {
            return 0;
        }

        public virtual bool GetBool(int x, int y, int z)
        {
            return false;
        }

        public virtual void SetByte(int x, int y, int z, byte Value)
        {

        }

        public virtual void SetInt(int x, int y, int z, int Value)
        {

        }

        public virtual void SetFloat(int x, int y, int z, float Value)
        {

        }

        public virtual void SetBool(int x, int y, int z, bool Value)
        {

        }

        public virtual void SetByteIntFloat(int x, int y, int z, byte Byte, int Int, float Float)
        {
            SetByte(x, y, z, Byte);
            SetFloat(x, y, z, Float);
        }

        public virtual void GetByteAndFloat(int x, int y, int z, ref byte Byte, ref float Float)
        {
            Float = GetFloat(x, y, z);
            Byte = GetByte(x, y, z);
        }

        public virtual bool IsValid(int x, int y, int z)
        {
            return Terrain.IsValid(x, y, z);
        }

        public virtual void GenerateGroundPoints(BiomeData Data, int ChunkX, int ChunkZ)
        {

        }

        #endregion

        #region UnSafe Values

        public virtual byte GetByteU(int x, int y, int z)
        {
            if (!IsValid(x, y, z))
                return 0;

            return GetByte(x, y, z);
        }

        public virtual int GetIntU(int x, int y, int z)
        {
            if (!IsValid(x, y, z))
                return 0;

            return GetInt(x, y, z);
        }

        public virtual float GetFloatU(int x, int y, int z)
        {
            if (!IsValid(x, y, z))
                return 0;

            return GetFloat(x, y, z);
        }

        public virtual void SetByteU(int x, int y, int z, byte Value)
        {
            if (IsValid(x, y, z))
                SetByte(x, y, z, Value);
        }

        public virtual void SetIntU(int x, int y, int z, int Value)
        {
            if (IsValid(x, y, z))
                SetInt(x, y, z, Value);

        }

        public virtual void SetFloatU(int x, int y, int z, float Value)
        {
            if (IsValid(x, y, z))
                SetFloat(x, y, z, Value);
        }

        public virtual void SetByteIntFloatU(int x, int y, int z, byte Byte, int Int, float Float)
        {
            SetByte(x, y, z, Byte);
            SetInt(x, y, z, Int);
            SetFloat(x, y, z, Float);
        }

        #endregion

        #region Virtual functions

        /// <summary>
        /// Generic function for get scale. Ovveride it for be able to use Generic Scripting system
        /// </summary>
        public float GetScale(int x, int y, int z)
        {
            return 0;
        }

        /// <summary>
        /// Generic function for get orientation.Ovveride it for be able to use Generic Scripting system
        /// </summary>
        public virtual float GetOrientation(int x, int y, int z)
        {
            return 0;
        }

        #endregion
    }
}

