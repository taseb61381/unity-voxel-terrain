﻿

namespace TerrainEngine
{
    public struct SimpleVoxelPoint
    {
        public int x, y, z;
        public byte Type;

        public SimpleVoxelPoint(int x, int y, int z, byte Type)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.Type = Type;
        }
    }

    public struct ComplexVoxelPoint
    {
        public ushort BlockId;
        public int x, y, z;
        public byte Type;

        public ComplexVoxelPoint(ushort BlockId, int x, int y, int z, byte Type)
        {
            this.BlockId = BlockId;
            this.x = x;
            this.y = y;
            this.z = z;
            this.Type = Type;
        }
    }

    public struct VoxelPoint
    {
        public VoxelModificationType ModType;
        public byte Type;
        public ushort BlockId;
        public int x, y, z;
        public float Volume;
        public bool IsNetwork;

        public VectorI3 Position
        {
            get
            {
                return new VectorI3(x, y, z);
            }
        }

        public VoxelPoint(ushort BlockId, int x, int y, int z, byte Type)
            : this(BlockId, x, y, z, Type, 0)
        {
            ModType = VoxelModificationType.SET_TYPE_SET_VOLUME;
            IsNetwork = false;
        }

        public VoxelPoint(ComplexVoxelPoint Point)
        {
            this.BlockId = Point.BlockId;
            this.x = Point.x;
            this.y = Point.y;
            this.z = Point.z;
            this.Type = Point.Type;
            this.Volume = 0;
            ModType = VoxelModificationType.SET_TYPE;
            this.IsNetwork = false;
        }

        public VoxelPoint(ushort BlockId, int x, int y, int z, byte Type, float Volume)
        {
            this.BlockId = BlockId;
            this.x = x;
            this.y = y;
            this.z = z;
            this.Type = Type;
            this.Volume = Volume;
            ModType = VoxelModificationType.SET_TYPE_SET_VOLUME;
            this.IsNetwork = false;
        }

        public VoxelPoint(ushort BlockId, int x, int y, int z, byte Type, float Volume, VoxelModificationType ModType, bool IsNetwork = false)
        {
            this.BlockId = BlockId;
            this.x = x;
            this.y = y;
            this.z = z;
            this.Type = Type;
            this.Volume = Volume;
            this.ModType = ModType;
            this.IsNetwork = IsNetwork;
        }

        public static bool operator !=(VoxelPoint lhs, VoxelPoint rhs)
        {
            return lhs.x != rhs.x || lhs.y != rhs.y || lhs.z != rhs.z;
        }

        public static bool operator ==(VoxelPoint lhs, VoxelPoint rhs)
        {
            return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(VoxelPoint))
            {
                VoxelPoint v = (VoxelPoint)obj;
                return this.x == v.x && this.y == v.y && this.z == v.z;
            }

            return false;
        }

        public override string ToString()
        {
            return x + "," + y + "," + z + "," + Type;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}