﻿using System;
using System.IO;

using UnityEngine;

namespace TerrainEngine
{
    public class TerrainDatas : ITerrainDataSave
    {
        public delegate float HeightFunctionDelegate(int x, int z, ref byte Type);
        public delegate bool LoopOverVoxelsDelegate(int x,int y,int z,int ChunkId,int Index, ref byte Type, ref float Volume); // Return true if voxel has been changed. Step is used to restart loop where it was stopped.
        public delegate void LoopOverVoxelsFastDelegate(int x,int y,int z,int ChunkId,int StartX,int StartZ, int Index, byte Type, float Volume); // Return true if voxel has been changed. Step is used to restart loop where it was stopped.
        public delegate bool CanContinueLoop(); // Function to set for pause loop. Return true if can continue.

        /// <summary>
        /// Max voxel volume allowed per voxel
        /// </summary>
        public const float HeightScale = 0.05f;
        public const float MinVolume = -128f;
        public const float MaxVolume = 128f;

        /// <summary>
        /// Values to convert ushort to float -> float to ushort
        /// </summary>
        public const float RangeVolume = MaxVolume - MinVolume;
        public const float UshortPerValue = (float)ushort.MaxValue / RangeVolume;
        public const float MinUshort = UshortPerValue * MinVolume;
        public const float InvertUshortPerValue = RangeVolume / (float)ushort.MaxValue;

        /// <summary>
        /// Class containing current LoopInformation. Used for restart loop where it was stopped
        /// </summary>
        public class LoopStepInformation
        {
            public bool IsDone = false;
            public int ChunkX, ChunkY, ChunkZ, ChunkId;
            public int ForceChunkX, ForceChunkY, ForceChunkZ;
            public bool CallEmptyChunks = true;

            public bool IsEmpty()
            {
                return ChunkX == 0 && ChunkY == 0 && ChunkZ == 0 && ChunkId == 0;
            }

            public void Clear()
            {
                ForceChunkX = ForceChunkY = ForceChunkZ = -1;
                ChunkX = ChunkY = ChunkZ = ChunkId;
                IsDone = CallEmptyChunks = true;
            }

            public override string ToString()
            {
                return "Loop: Cx:" + ChunkX + ",Cy:" + ChunkY + ",Cz:" + ChunkZ + ",Ci:" + ChunkId + ",Done:" + IsDone;
            }
        }

        /// <summary>
        /// Used for optimize access to local variable in function, is more fast than class ref
        /// </summary>
        public struct TempLoopStepInformation
        {
            public int ChunkX, ChunkY, ChunkZ, ChunkId;
            public int ForceChunkX, ForceChunkY, ForceChunkZ;

            public TempLoopStepInformation(LoopStepInformation Info)
            {
                if (Info != null)
                {
                    this.ChunkX = Info.ChunkX;
                    this.ChunkY = Info.ChunkY;
                    this.ChunkZ = Info.ChunkZ;
                    this.ChunkId = Info.ChunkId;
                    this.ForceChunkX = Info.ForceChunkX;
                    this.ForceChunkY = Info.ForceChunkY;
                    this.ForceChunkZ = Info.ForceChunkZ;
                }
                else
                {
                    this.ChunkX = 0;
                    this.ChunkY = 0;
                    this.ChunkZ = 0;
                    this.ChunkId = 0;
                    ForceChunkX = ForceChunkY = ForceChunkZ = -1;
                }
            }

            public void Set(LoopStepInformation Info)
            {
                if (Info == null)
                    return;

                Info.ChunkX = this.ChunkX;
                Info.ChunkY = this.ChunkY;
                Info.ChunkZ = this.ChunkZ;
                Info.ChunkId = this.ChunkId;
            }
        }


        public ushort BlockId;
        public TerrainInformation Information;
        public VectorI3 Position;
        public bool IsUsedByClient = false;
        public bool IsUsedByServer = false;

        public int WorldX;
        public int WorldY;
        public int WorldZ;

        public int VoxelsPerAxis
        {
            get
            {
                return Information.VoxelsPerAxis;
            }
        }

        public virtual void Init(TerrainInformation Informations, VectorI3 Position)
        {
            this.Information = Informations;
            this.Position = Position;
            this.Dirty = false;

            WorldX = Position.x * Informations.VoxelsPerAxis;
            WorldY = Position.y * Informations.VoxelsPerAxis;
            WorldZ = Position.z * Informations.VoxelsPerAxis;
        }

        public bool IsValid(int x, int y, int z)
        {
            if (x >= VoxelsPerAxis || x < 0 || y >= VoxelsPerAxis || y < 0 || z >= VoxelsPerAxis || z < 0)
                return false;

            return true;
        }

        public bool IsValid(int p)
        {
            if (p > -1 && p < VoxelsPerAxis)
                return true;

            return false;
        }

        public virtual void LoopOverAllVoxels(LoopStepInformation StepInfo,CanContinueLoop CanContinue, LoopOverVoxelsDelegate Function)
        {
            GeneralLog.LogError("Loop function is not overrided");

            //if (Function == null)
            //    return;

            //StepInfo.IsDone = false;
            //TempLoopStepInformation Step = new TempLoopStepInformation(StepInfo);
            //byte Type = 0;
            //float Volume = 0;
            //for (; Step.x < VoxelCountX; ++Step.x)
            //{
            //    for (; Step.y < VoxelCountX; ++Step.y)
            //    {
            //        for (; Step.z < VoxelCountX; ++Step.z)
            //        {
            //            GetTypeAndVolume(Step.x, Step.y, Step.z, ref Type, ref Volume);
            //            if (Function(Step.x, Step.y, Step.z, 0, ref Type, ref Volume))
            //                SetTypeAndVolume(Step.x, Step.y, Step.z, Type, Volume);
            //        }

            //        Step.z = 0;

            //        if (!CanContinue())
            //        {
            //            ++Step.y;
            //            Step.Set(StepInfo);
            //            return;
            //        }
            //    }

            //    Step.y = 0;
            //}

            //StepInfo.Clear();
        }

        public virtual void LoopOverYVoxels(LoopStepInformation StepInfo, CanContinueLoop CanContinue, LoopOverVoxelsDelegate Function)
        {
        }

        public virtual void LoopOverYVoxelsFast(LoopStepInformation StepInfo, CanContinueLoop CanContinue, LoopOverVoxelsFastDelegate Function)
        {

        }

        public virtual void GenerateGroundPoints(BiomeData Data, int ChunkX, int ChunkZ)
        {
            for (int i = Customs.Count - 1; i >= 0; --i)
                Customs.array[i].GenerateGroundPoints(Data, ChunkX, ChunkZ);
        }

        #region HeightMap

        public int UseHeight = 2; // 0 = HeightGrid,1 = HeightFunction, 2 = None

        /// <summary>
        /// Function that will return an Height at given X/Z position
        /// </summary>
        public HeightFunctionDelegate HeightFunction;

        /// <summary>
        /// Array that contains Height at given X/Z position
        /// </summary>
        public ushort[] HeightGrid;

        /// <summary>
        /// Array that contains Voxel Type at given X/Z position
        /// </summary>
        public byte[] HeightTypes;

        /// <summary>
        /// Return height at local X/Z position. (Height > 0);
        /// </summary>
        public virtual float GetHeight(int x, int z)
        {
            if (UseHeight == 0)
            {
                x = x * Information.VoxelsPerAxis + z;
                return (float)HeightGrid[x] * HeightScale + WorldY;
            }

            if (UseHeight == 1)
            {
                byte OldType = 0;
                return HeightFunction(WorldX + x, WorldZ + z, ref OldType);
            }

            return float.MinValue;
        }

        public virtual float GetHeight(int x, int z, ref byte Type)
        {
            if (UseHeight == 0)
            {
                x = x * Information.VoxelsPerAxis + z;
                Type = HeightTypes[x];
                return (float)HeightGrid[x] * HeightScale + WorldY;
            }

            if (UseHeight == 1)
            {
                return HeightFunction(WorldX + x, WorldZ + z, ref Type);
            }

            Type = 0;
            return float.MinValue;
        }

        /// <summary>
        /// Return Volume and Type at given X/Y/Z position. This function will use HeightGrid or HeightFunction if possible or return 0,0
        /// </summary>
        public virtual void GetHeight(int x, int y, int z, ref byte Type, ref float Volume)
        {
            if (UseHeight == 0)
            {
                x = x * Information.VoxelsPerAxis + z;
                Volume = (float)HeightGrid[x] * HeightScale - y;
                if (Volume < MinVolume)
                {
                    Volume = MinVolume;
                    Type = 0;
                    return;
                }
                else if (Volume > MaxVolume) Volume = MaxVolume;
                Type = HeightTypes[x];
                Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                return;
            }

            if (UseHeight == 1)
            {
                Volume = HeightFunction(WorldX + x, WorldZ + z, ref Type) - y - WorldY;
                if (Volume < MinVolume) Volume = MinVolume;
                else if (Volume > MaxVolume) Volume = MaxVolume;
                Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                return;
            }

            Type = 0;
            Volume = 0;
            return;
        }

        public virtual void InitHeight()
        {
            if (UseHeight == 2)
            {
                lock (this)
                {
                    if (HeightGrid == null)
                    {
                        int Size = Information.VoxelsPerAxis * Information.VoxelsPerAxis;
                        HeightGrid = new ushort[Size];
                        HeightTypes = new byte[Size];
                        UseHeight = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Set height at given x/z position. If HeightGrid is null, it will be created using Information.VoxelCountX
        /// </summary>
        public virtual void SetHeight(int x, int z, float Height, byte Type)
        {
            Height -= WorldY;
            if (Height < 0)
            {
                if (UseHeight == 2)
                    return;

                Height = 0;
            }

            if (UseHeight == 2)
            {
                lock (this)
                {
                    if (HeightGrid == null)
                    {
                        int Size = Information.VoxelsPerAxis * Information.VoxelsPerAxis;
                        HeightGrid = new ushort[Size];
                        HeightTypes = new byte[Size];
                        UseHeight = 0;
                    }
                }
            }

            x = x * Information.VoxelsPerAxis + z;
            HeightGrid[x] = (ushort)(Height / HeightScale);
            HeightTypes[x] = Type;
            Dirty = true;
        }

        /// <summary>
        /// Set height at given x/z position. If HeightGrid is null, it will be created using Information.VoxelCountX
        /// </summary>
        public virtual void SetHeightFast(int x, int z, float Height, byte Type)
        {
            Height -= WorldY;
            if (Height < 0)
            {
                return;
            }

            x = x * Information.VoxelsPerAxis + z;
            HeightGrid[x] = (ushort)(Height / HeightScale);
            HeightTypes[x] = Type;
        }

        /// <summary>
        /// Set height at given x/z position. If HeightGrid is null, it will be created using Information.VoxelCountX
        /// </summary>
        public virtual void SetHeightFast(int x, int z, float Height)
        {
            Height -= WorldY;
            if (Height < 0)
            {
                return;
            }

            x = x * Information.VoxelsPerAxis + z;
            HeightGrid[x] = (ushort)(Height / HeightScale);
        }

        /// <summary>
        /// Set an HeightFunction. This function will return an height at given X/Z position
        /// </summary>
        public virtual void SetHeight(HeightFunctionDelegate HeightFunction)
        {
            this.HeightFunction = HeightFunction;
            this.UseHeight = 1;
        }

        public virtual void GetHeightFromGrid(int x, int y, int z, ref byte Type, ref float Volume)
        {
            x = x * Information.VoxelsPerAxis + z;
            Volume = (float)HeightGrid[x] * HeightScale - y;
            if (Volume < MinVolume)
            {
                Volume = MinVolume;
                Type = 0;
                return;
            }
            else if (Volume > MaxVolume) Volume = MaxVolume;

            Type = HeightTypes[x];
            Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
        }

        public virtual void GetHeightFromFunction(int x, int y, int z, ref byte Type, ref float Volume)
        {
            Volume = HeightFunction(WorldX + x, WorldZ + z, ref Type) - y - WorldY;
            if (Volume < MinVolume) Volume = MinVolume;
            else if (Volume > MaxVolume) Volume = MaxVolume;
            Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
        }

        public virtual bool IsUnderGround(int x, int y, int z)
        {
            if (Voxels[(x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk)] != null)
                return false;

            if (UseHeight == 2)
                return false;

            byte Type = 0;
            return (GetHeight(x, z, ref Type) - (y + WorldY)) >= MaxVolume/8f && Type != 0;
        }

        #endregion

        #region Chunks

        /// <summary>
        /// Contains voxels for each chunk. Voxels[ChunkId][Index]. Index = VoxelPerChunkX*VoxelPerchunkY*VoxelPerChunkZ
        /// </summary>
        public byte[][] Voxels;

        #endregion

        public virtual byte GetTypeAndVolume(int x, int y, int z, ref byte Type, ref float Volume)
        {
            Type = GetType(x, y, z);
            Volume = GetVolume(x, y, z);
            return Type;
        }

        public virtual byte GetTypeAndVolumeHeight(int x, int y, int z, ref float Volume)
        {
            Volume = GetVolume(x, y, z);
            return GetType(x, y, z);
        }

        public virtual byte GetTypeAndVolume(int x, int y, int z, int ChunkId, ref byte Type, ref float Volume)
        {
            Type = GetType(x, y, z);
            Volume = GetVolume(x, y, z);
            return Type;
        }

        public virtual byte GetTypeAndVolume(int x, int y, int z, int ChunkId, int Index, ref byte Type, ref float Volume)
        {
            Type = GetType(x, y, z);
            Volume = GetVolume(x, y, z);
            return Type;
        }

        /// <summary>
        /// Return Type and Volume of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public virtual byte GetTypeAndVolumeHeight(int x, int y, int z, int ChunkId, int Index, ref float Volume)
        {
            Volume = GetVolume(x, y, z);
            return GetType(x, y, z);
        }

        public virtual byte GetTypeAndVolumeFromChunk(int x, int y, int z, int ChunkId, int Index, ref float Volume)
        {
            return 0;
        }

        public virtual float GetTypeAndHeightFromGrid(int x, int z, ref byte Type)
        {
            return 0;
        }

        public void GetVolumeFromHeight(int y,ref byte Type, ref float Volume, float Height)
        {
            Volume = Height - y;
            if (Volume < MinVolume)
            {
                Volume = MinVolume;
                Type = 0;
                return;
            }
            if (Volume > MaxVolume) Volume = MaxVolume;
            Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
        }

        public virtual void Reset(byte DefaultType)
        {
            float Volume = DefaultType == 0 ? 0 : 1;

            for (int x = 0; x < VoxelsPerAxis; ++x)
            {
                for (int y = 0; y < VoxelsPerAxis; ++y)
                {
                    for (int z = 0; z < VoxelsPerAxis; ++z)
                    {
                        SetTypeAndVolume(x, y, z, DefaultType, Volume);
                    }
                }
            }
        }

        public override void Clear()
        {
            CompressedData = null;
            IsUsedByClient = IsUsedByServer = false;
            Dirty = false;

            if (Customs.Count != 0)
            {
                for (int i = Customs.Count - 1; i >= 0; --i)
                    Customs.array[i].Clear();
            }
        }

        #region Values

        public virtual void Set(int x, int y, int z, byte[, ,] Data)
        {
            for (int px = 0; px < Data.GetLength(0); ++px)
                for (int py = 0; py < Data.GetLength(1); ++py)
                    for (int pz = 0; pz < Data.GetLength(2); ++pz)
                    {
                        if (Data[px, py, pz] != BlockManager.None)
                        {
                            SetTypeAndVolume(px + x, py + y, pz + z, Data[px, py, pz], 1f);
                        }
                    }
        }

        #endregion

        #region Safe Values

        public virtual byte GetType(int x, int y, int z)
        {
            return 0;
        }

        public virtual float GetVolume(int x, int y, int z)
        {
            byte Type = 0;
            float Volume = 0;
            GetTypeAndVolume(x, y, z, ref Type, ref Volume);
            return Volume;
        }

        public virtual float GetVolume(int x, int y, int z, int ChunkId)
        {
            byte Type = 0;
            float Volume = 0;
            GetTypeAndVolume(x, y, z,ChunkId, ref Type, ref Volume);
            return Volume;
        }

        public virtual float GetVolume(int x, int y, int z, int ChunkId,ref bool IsSafe)
        {
            byte Type = 0;
            float Volume = 0;
            GetTypeAndVolume(x, y, z, ChunkId, ref Type, ref Volume);
            return Volume;
        }

        public virtual void SetType(int x, int y, int z, byte Type)
        {

        }

        public virtual void SetType(int x, int y, int z, byte Type, int ChunkId)
        {

        }

        public virtual void SetVolume(int x, int y, int z, float Volume)
        {

        }

        public virtual void SetVolume(int x, int y, int z, float Volume, int ChunkId)
        {
            SetVolume(x, y, z, Volume);
        }

        public virtual void SetVolume(int x, int y, int z, float Volume, int ChunkId,ref bool IsSafe)
        {
            SetVolume(x, y, z, Volume);
        }

        public virtual void SetTypeAndVolume(int x, int y, int z, byte Type, float Volume)
        {
            SetType(x, y, z, Type);
            SetVolume(x, y, z, Volume);
        }

        public virtual void SetTypeAndVolume(int x, int y, int z, int ChunkId, byte Type, float Value)
        {
            SetTypeAndVolume(x, y, z, Type, Value);
        }

        public virtual void SetTypeAndVolume(int x, int y, int z, int ChunkId, byte Type, float Value,ref bool IsSafe)
        {
            SetTypeAndVolume(x, y, z, Type, Value);
        }

        public virtual void SetTypeAndVolume(int x, int y, int z, int ChunkId, int Index, byte Type, float Value)
        {
            SetTypeAndVolume(x, y, z, Type, Value);
        }

        public virtual bool HasVolume(int x, int y, int z, float MinVolume)
        {
            return false;
        }

        #endregion

        #region UnSafe Values

        public virtual byte GetTypeU(int x, int y, int z)
        {
            if (!IsValid(x, y, z))
                return 0;

            return GetType(x, y, z);
        }

        public virtual float GetVolumeU(int x, int y, int z)
        {
            if (!IsValid(x, y, z))
                return 0;

            return GetVolume(x, y, z);
        }

        public virtual void GetTypeAndVolumeU(int x, int y, int z, ref byte Type, ref float Volume)
        {
            if (IsValid(x, y, z))
                GetTypeAndVolume(x, y, z, ref Type, ref Volume);
            else
            {
                Type = 0;
                Volume = 0;
            }
        }

        public virtual void SetTypeU(int x, int y, int z, byte Value)
        {
            if (IsValid(x, y, z))
                SetType(x, y, z, Value);
        }

        public virtual void SetVolumeU(int x, int y, int z, float Value)
        {
            if (IsValid(x, y, z))
                SetVolume(x, y, z, Value);
        }

        #endregion

        #region Positions

        public virtual Vector3 GetPosition()
        {
            return Position;
        }

        public virtual Vector3 GetWorldPosition()
        {
            return new Vector3(WorldX, WorldY, WorldZ);
        }

        public virtual TerrainInformation GetInformation()
        {
            return Information;
        }

        public virtual int GetWorldX(int x)
        {
            return WorldX + x;
        }

        public virtual int GetWorldY(int y)
        {
            return WorldY + y;
        }

        public virtual int GetWorldZ(int z)
        {
            return WorldZ + z;
        }

        #endregion

        #region Save

        public virtual bool IsDirty()
        {
            if (Dirty)
                return true;


            for (int j = 0; j < Customs.Count; ++j)
                if (Customs.array[j].Dirty)
                    return true;

            return false;
        }

        public override void Save(BinaryWriter Stream)
        {
            if (HeightGrid == null)
                Stream.Write(false);
            else
            {
                Stream.Write(true);

                byte Value = 0;
                int Len = HeightGrid.Length;

                Stream.Write(Len);

                byte LastValue = 0;
                byte LastCount = 0;

                ////// TYPE /////
                // RLE Compression
                for (int i = 0; i < Len; ++i)
                {
                    Value = HeightTypes[i];
                    if (Value != LastValue || LastCount >= 255)
                    {
                        if (LastCount != 0)
                        {
                            Stream.Write(LastCount);
                            Stream.Write(LastValue);
                        }

                        LastCount = 1;
                        LastValue = Value;
                    }
                    else
                        ++LastCount;
                }

                if (LastCount != 0)
                {
                    Stream.Write(LastCount);
                    Stream.Write(LastValue);
                }

                ////// Height /////
                int Last = HeightGrid[0];
                int Current = 0;
                int Diff = 0;

                Stream.Write((ushort)Last);
                for(int i=1;i<HeightGrid.Length-1;++i)
                {
                    Current = HeightGrid[i];
                    Diff = Current-Last;
                    if(Diff >= 0)
                    {
                        while (Diff >= 0)
                        {
                            Stream.Write((byte)(128 + Mathf.Min(127, Diff)));
                            Diff -= 127;
                        }
                    }
                    else
                    {
                        Diff = -Diff;
                        while (Diff >= 0)
                        {
                            Stream.Write((byte)(128 - Math.Min(128, Diff)));
                            Diff -= 128;
                        }
                    }

                    Last = Current;
                }
            }
        }

        public override void Read(BinaryReader Stream)
        {
            if (Stream.ReadBoolean())
            {
                int Len = Stream.ReadInt32();

                HeightTypes = new byte[Len];
                HeightGrid = new ushort[Len];
                UseHeight = 0;

                byte type = 0;
                byte Count = 0;
                int j = 0;
                for (int i = 0; i < Len;)
                {
                    Count = Stream.ReadByte();
                    type = Stream.ReadByte();
                    for (j = 0; j < Count; ++j)
                    {
                        HeightTypes[i + j] = type;
                    }
                    i += Count;
                }

                int Last = Stream.ReadUInt16();
                byte Value = 0;
                for (int i = 1; i < Len - 1; ++i)
                {
                    Value = Stream.ReadByte();

                    if (Value < 128)
                    {
                        Last -= 128 - Value;
                        while (Value == 0)
                        {
                            Value = Stream.ReadByte();
                            Last -= 128 - Value;
                        }
                    }
                    else
                    {
                        Last += Value - 128;
                        while (Value == 255)
                        {
                            Value = Stream.ReadByte();
                            Last += Value - 128;
                        }
                    }

                    HeightGrid[i] = (ushort)(Last);
                }
            }

        }

        #endregion

        #region CustomData

        public SList<ICustomTerrainData> Customs;

        public virtual bool RegisterCustomData(string Name, ICustomTerrainData Data)
        {
            Data.Init(this);
            Data.Name = Name;

            if (Customs.array == null)
                Customs = new SList<ICustomTerrainData>(2);
            else
            {
                for (int i = Customs.Count - 1; i >= 0; --i)
                {
                    if (Customs.array[i].Name == Name)
                    {
                        return false;
                    }
                }
            }

            Data.CustomIndex = Customs.Count;
            Customs.Add(Data);
            return true;
        }

        public virtual int GetCustomDataIndex(string Name)
        {
            for (int i = Customs.Count - 1; i >= 0; --i)
            {
                if (Customs.array[i].Name == Name)
                    return i;
            }

            return -1;
        }

        public virtual T GetCustomData<T>(string Name) where T : ICustomTerrainData
        {
            for (int i = Customs.Count - 1; i >= 0; --i)
            {
                if (Customs.array[i].Name == Name)
                    return Customs.array[i] as T;
            }

            return null;
        }

        public virtual T GetCustomData<T>() where T : ICustomTerrainData
        {
            for (int i = Customs.Count - 1; i >= 0; --i)
            {
                if (Customs.array[i] is T)
                    return Customs.array[i] as T;
            }

            return null;
        }

        public virtual T GetCustomDataByID<T>(byte ProcessorID) where T : ICustomTerrainData
        {
            for (int i = Customs.Count - 1; i >= 0; --i)
            {
                if (Customs.array[i].UID == ProcessorID)
                    return Customs.array[i] as T;
            }

            return null;
        }

        #endregion
    }
}