﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TerrainEngine
{
    public abstract class ITerrainDataSave : IPoolable
    {
        public byte UID = 0;
        public string Name = "TerrainData";
        public byte[] CompressedData;
        public bool Dirty=false;

        public virtual void Save(BinaryWriter Writer)
        {

        }

        public virtual void Read(BinaryReader Reader)
        {

        }

        public override void Clear()
        {
            CompressedData = null;
            Dirty = false;
        }
    }
}
