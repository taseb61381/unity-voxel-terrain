﻿using System.Collections.Generic;
using System.IO;

using UnityEngine;

namespace TerrainEngine
{
    /// <summary>
    /// Default Voxels Data Storage. Can store each voxel information or an 2DHeightmap or an HeightMap function
    /// Store 2 Bytes . VoxelType, VoxelVolume. Can be used for Fluids and Terrain data
    /// </summary>
    public class TypeVolumeTerrainData : TerrainDatas
    {
        static public PoolInfoRef VoxelsPool = new PoolInfoRef();
        static public Stack<byte[]> Pool = new Stack<byte[]>();

        static public PoolInfoRef VoxelsVolumePool = new PoolInfoRef();
        static public Stack<ushort[]> VolumePool = new Stack<ushort[]>();

        public override void Init(TerrainInformation Informations, VectorI3 Position)
        {
            if (VoxelsPool.Index == 0)
                VoxelsPool = PoolManager.Pool.GetPool("VoxelsArray");

            base.Init(Informations, Position);

            if (Voxels == null)
            {
                Voxels = new byte[Information.TotalChunksPerBlock][];
                VoxelsVolume = new ushort[Information.TotalChunksPerBlock][];
            }
        }

        /// <summary>
        /// Remove all voxels array and add them to the pool
        /// </summary>
        public override void Clear()
        {
            lock (Pool)
            {
                for (int ChunkId = Information.TotalChunksPerBlock - 1; ChunkId >= 0; --ChunkId)
                {
                    if (Voxels[ChunkId] != null)
                    {
                        Pool.Push(Voxels[ChunkId]);
                        Voxels[ChunkId] = null;
                        ++VoxelsPool.EnqueueData;
                    }
                }
            }

            lock (VolumePool)
            {
                for (int ChunkId = Information.TotalChunksPerBlock - 1; ChunkId >= 0; --ChunkId)
                {
                    if (VoxelsVolume[ChunkId] != null)
                    {
                        VolumePool.Push(VoxelsVolume[ChunkId]);
                        VoxelsVolume[ChunkId] = null;
                        ++VoxelsVolumePool.EnqueueData;
                    }
                }
            }
            base.Clear();
        }

        #region Chunks

        public ushort[][] VoxelsVolume;

        /// <summary>
        /// Return an new byte[] or use Pool is exist
        /// </summary>
        private byte[] GetArray(int Size)
        {
            byte[] Array = null;
            lock (Pool)
            {
                if (Pool.Count != 0)
                {
                    ++VoxelsPool.DequeueData;
                    Array = Pool.Pop();
                }
            }

            if (Array == null)
            {
                ++VoxelsPool.CreatedObject;
                return new byte[Size];
            }
            else
            {
                System.Array.Clear(Array, 0, Size);
            }

            return Array;
        }

        /// <summary>
        /// Return an new byte[] or use Pool is exist
        /// </summary>
        private ushort[] GetArrayVolume(int Size)
        {
            ushort[] Array = null;
            lock (VolumePool)
            {
                if (VolumePool.Count != 0)
                {
                    ++VoxelsPool.DequeueData;
                    Array = VolumePool.Pop();
                }
            }

            if (Array == null)
            {
                ++VoxelsVolumePool.CreatedObject;
                return new ushort[Size];
            }
            else
            {
                System.Array.Clear(Array, 0, Size);
            }

            return Array;
        }

        /// <summary>
        /// Init a new chunk voxel array. This function will look at HeightMap system to get voxel data when creating new voxels array
        /// </summary>
        private void InitChunk(int x, int y, int z, int ChunkId)
        {
            int VoxelCountPerChunk = Information.TotalVoxelsPerChunk;
            byte[] Chunk = Voxels[ChunkId] = GetArray(VoxelCountPerChunk);
            ushort[] Volumes = VoxelsVolume[ChunkId] = GetArrayVolume(VoxelCountPerChunk);
            int i = 0;

            // If an height system is valid we set the chunk voxels to values extracted from Height
            if (UseHeight != 2)
            {
                byte Type = 0;
                float Volume = 0;
                int sx = (x / Information.VoxelPerChunk) * Information.VoxelPerChunk;
                int sy = (y / Information.VoxelPerChunk) * Information.VoxelPerChunk;
                int sz = (z / Information.VoxelPerChunk) * Information.VoxelPerChunk;

                int ex = sx + Information.VoxelPerChunk, ey = sy + Information.VoxelPerChunk, ez = sz + Information.VoxelPerChunk;
                int px, py, pz;

                if (UseHeight == 1)
                {
                    for (px = sx; px < ex; ++px)
                        for (py = sy; py < ey; ++py)
                            for (pz = sz; pz < ez; ++pz)
                            {
                                GetHeightFromFunction(px, py, pz, ref Type, ref Volume);
                                Chunk[i] = Type;
                                if (Volume < MinVolume) Volume = MinVolume;
                                if (Volume > MaxVolume) Volume = MaxVolume;
                                Volumes[i] = (ushort)((Volume - MinVolume) * UshortPerValue);
                                ++i;
                            }
                }
                else
                {
                        /*i = 0;
                        for (px = sx; px < ex; ++px)
                            for (py = sy; py < ey; ++py)
                                for (pz = sz; pz < ez; ++pz)
                                {
                                    GetHeightFromGrid(px, py, pz, ref Type, ref Volume);
                                    Chunk[i] = Type;
                                    Chunk[i + VoxelCountPerChunk] = (byte)(Volume / MaxVolume * (float)ushort.MaxValue);
                                    ++i;
                                }*/

                        i = 0;
                        int Index = 0;
                        float Height = 0;
                        for (px = sx; px < ex; ++px)
                        {
                            Index = px * Information.VoxelsPerAxis + sz;
                            for (pz = sz; pz < ez; ++pz,++Index)
                            {
                                Type = HeightTypes[Index];
                                Height = (float)HeightGrid[Index] * HeightScale;

                                i = (px - sx) * Information.VoxelPerChunk * Information.VoxelPerChunk + (pz - sz);
                                for (py = sy; py < ey; ++py, i += Information.VoxelPerChunk)
                                {
                                    Volume = Height - py;
                                    if (Volume < MinVolume) Volume = MinVolume;
                                    if (Volume > MaxVolume) Volume = MaxVolume;

                                    Chunk[i] = Type;
                                    Volumes[i] = (ushort)((Volume - MinVolume) * UshortPerValue);
                                }
                            }
                        }
                }
            }

        }

        #endregion

        #region ReadWrite

        public override float GetVolume(int x, int y, int z)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                //GetHeight(x, y, z, ref Type, ref Volume);
                if (UseHeight == 0)
                {
                    x = x * Information.VoxelsPerAxis + z;
                    float Volume = (float)HeightGrid[x] * HeightScale - y;
                    if (Volume < MinVolume)
                    {
                        Volume = MinVolume;
                        return Volume;
                    }
                    else if (Volume > MaxVolume) Volume = MaxVolume;

                    return (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                }

                if (UseHeight == 1)
                {
                    byte Type = 0;
                    float Volume = HeightFunction(WorldX + x, WorldZ + z, ref Type) - y - WorldY;
                    if (Volume < MinVolume) Volume = MinVolume;
                    else if (Volume > MaxVolume) Volume = MaxVolume;
                    return (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                }

                return 0f;
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            return ((float)VoxelsVolume[ChunkId][x] + MinUshort) * InvertUshortPerValue;
        }

        public override float GetVolume(int x, int y, int z, int ChunkId)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                //GetHeight(x, y, z, ref Type, ref Volume);
                if (UseHeight == 0)
                {
                    x = x * Information.VoxelsPerAxis + z;
                    float Volume = (float)HeightGrid[x] * HeightScale - y;
                    if (Volume < MinVolume)
                    {
                        Volume = MinVolume;
                        return Volume;
                    }
                    else if (Volume > MaxVolume) Volume = MaxVolume;

                    return (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                }

                if (UseHeight == 1)
                {
                    byte Type = 0;
                    float Volume = HeightFunction(WorldX + x, WorldZ + z, ref Type) - y - WorldY;
                    if (Volume < MinVolume) Volume = MinVolume;
                    else if (Volume > MaxVolume) Volume = MaxVolume;
                    return (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                }

                return 0f;
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            return ((float)VoxelsVolume[ChunkId][x] + MinUshort) * InvertUshortPerValue;
        }

        public override float GetVolume(int x, int y, int z, int ChunkId, ref bool IsSafe)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (!IsSafe && Voxels[ChunkId] == null)
            {
                //GetHeight(x, y, z, ref Type, ref Volume);
                if (UseHeight == 0)
                {
                    x = x * Information.VoxelsPerAxis + z;
                    float Volume = (float)HeightGrid[x] * HeightScale - y;
                    if (Volume < MinVolume)
                    {
                        Volume = MinVolume;
                        return Volume;
                    }
                    else if (Volume > MaxVolume) Volume = MaxVolume;

                    return (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                }

                if (UseHeight == 1)
                {
                    byte Type = 0;
                    float Volume = HeightFunction(WorldX + x, WorldZ + z, ref Type) - y - WorldY;
                    if (Volume < MinVolume) Volume = MinVolume;
                    else if (Volume > MaxVolume) Volume = MaxVolume;
                    return (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                }

                return 0f;
            }

            IsSafe = true;
            return ((float)VoxelsVolume[ChunkId][(x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk)] + MinUshort) * InvertUshortPerValue;
        }

        /// <summary>
        /// Return Type and Volume of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public override byte GetTypeAndVolume(int x, int y, int z, ref byte Type, ref float Volume)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                //GetHeight(x, y, z, ref Type, ref Volume);
                if (UseHeight == 0)
                {
                    x = x * Information.VoxelsPerAxis + z;
                    Volume = (float)HeightGrid[x] * HeightScale - y;
                    if (Volume < MinVolume)
                    {
                        Volume = MinVolume;
                        Type = 0;
                        return 0;
                    }
                    if (Volume > MaxVolume) Volume = MaxVolume;
                    Type = HeightTypes[x];
                    Volume = (((ushort)((Volume - MinVolume) * UshortPerValue))+ MinUshort) * InvertUshortPerValue;
                    return Type;
                }

                if (UseHeight == 1)
                {
                    Volume = HeightFunction(WorldX + x, WorldZ + z, ref Type) - y - WorldY;
                    if (Volume < MinVolume) Volume = MinVolume;
                    if (Volume > MaxVolume) Volume = MaxVolume;

                    Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                    return Type;
                }

                Type = 0;
                Volume = 0;
                return Type;
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Type = Voxels[ChunkId][x];
            Volume = ((float)VoxelsVolume[ChunkId][x] + MinUshort) * InvertUshortPerValue;
            return Type;
        }

        /// <summary>
        /// Return Type and Volume of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public override byte GetTypeAndVolumeHeight(int x, int y, int z, ref float Volume)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                x = x * Information.VoxelsPerAxis + z;
                Volume = (float)HeightGrid[x] * HeightScale - y;
                if (Volume < MinVolume)
                {
                    Volume = MinVolume;
                    return 0;
                }
                if (Volume > MaxVolume) Volume = MaxVolume;
                Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                return HeightTypes[x];
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Volume = ((float)VoxelsVolume[ChunkId][x] + MinUshort) * InvertUshortPerValue;
            return Voxels[ChunkId][x];
        }

        /// <summary>
        /// Return Type and Volume of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public override byte GetTypeAndVolume(int x, int y, int z, int ChunkId, ref byte Type, ref float Volume)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                //GetHeight(x, y, z, ref Type, ref Volume);
                if (UseHeight == 0)
                {
                    x = x * Information.VoxelsPerAxis + z;
                    Volume = (float)HeightGrid[x] * HeightScale - y;
                    if (Volume < MinVolume)
                    {
                        Volume = MinVolume;
                        Type = 0;
                        return 0;
                    }
                    if (Volume > MaxVolume) Volume = MaxVolume;
                    Type = HeightTypes[x];
                    Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                    return Type;
                }

                if (UseHeight == 1)
                {
                    Volume = HeightFunction(WorldX + x, WorldZ + z, ref Type) - y - WorldY;
                    return Type;
                }

                Type = 0;
                Volume = 0;
                return Type;
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Type = Voxels[ChunkId][x];
            Volume = ((float)VoxelsVolume[ChunkId][x] + MinUshort) * InvertUshortPerValue;
            return Type;
        }

        /// <summary>
        /// Return Type and Volume of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public override byte GetTypeAndVolume(int x, int y, int z, int ChunkId,int Index, ref byte Type, ref float Volume)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                //GetHeight(x, y, z, ref Type, ref Volume);
                if (UseHeight == 0)
                {
                    x = x * Information.VoxelsPerAxis + z;
                    Volume = (float)HeightGrid[x] * HeightScale - y;
                    if (Volume < MinVolume)
                    {
                        Volume = MinVolume;
                        Type = 0;
                        return 0;
                    }
                    if (Volume > MaxVolume) Volume = MaxVolume;

                    Type = HeightTypes[x];
                    Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                    return Type;
                }

                if (UseHeight == 1)
                {
                    return Type;
                }

                Type = 0;
                Volume = 0;
                return Type;
            }

            Type = Voxels[ChunkId][Index];
            Volume = ((float)VoxelsVolume[ChunkId][Index] + MinUshort) * InvertUshortPerValue;
            return Type;
        }

        /// <summary>
        /// Return Type and Volume of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public override byte GetTypeAndVolumeHeight(int x, int y, int z, int ChunkId, int Index, ref float Volume)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                x = x * Information.VoxelsPerAxis + z;
                Volume = (float)HeightGrid[x] * HeightScale - y;
                if (Volume < MinVolume)
                {
                    Volume = MinVolume;
                    return 0;
                }
                if (Volume > MaxVolume) Volume = MaxVolume;

                Volume = (((ushort)((Volume - MinVolume) * UshortPerValue)) + MinUshort) * InvertUshortPerValue;
                return HeightTypes[x];
            }

            Volume = ((float)VoxelsVolume[ChunkId][Index] + MinUshort) * InvertUshortPerValue;
            return Voxels[ChunkId][Index];
        }

        public override float GetTypeAndHeightFromGrid(int x, int z, ref byte Type)
        {
            x = x * Information.VoxelsPerAxis + z;
            Type = HeightTypes[x];
            return (float)HeightGrid[x] * HeightScale;
        }

        public override byte GetTypeAndVolumeFromChunk(int x, int y, int z, int ChunkId, int Index, ref float Volume)
        {
            Volume = ((float)VoxelsVolume[ChunkId][Index] + MinUshort) * InvertUshortPerValue;
            return Voxels[ChunkId][Index];
        }

        public override void SetVolume(int x, int y, int z, float Volume)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            if (Voxels[ChunkId] == null)
            {
                if (Volume < 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            if (Volume > MaxVolume) Volume = MaxVolume;
            if (Volume < 0) Volume = 0;

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            VoxelsVolume[ChunkId][x] = (ushort)((Volume - MinVolume) * UshortPerValue);
            Dirty = true;
        }

        public override void SetVolume(int x, int y, int z, float Volume, int ChunkId)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                if (Volume <= 0f)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            if (Volume > MaxVolume) Volume = MaxVolume;
            if (Volume < MinVolume) Volume = MinVolume;

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            VoxelsVolume[ChunkId][x] = (ushort)((Volume - MinVolume) * UshortPerValue);
            Dirty = true;
        }

        public override void SetVolume(int x, int y, int z, float Volume, int ChunkId, ref bool IsSafe)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (!IsSafe && Voxels[ChunkId] == null)
            {
                if (Volume <= 0f)
                    return;

                InitChunk(x, y, z, ChunkId);
                IsSafe = true;
            }

            if (Volume > MaxVolume) Volume = MaxVolume;
            if (Volume < MinVolume) Volume = MinVolume;

            VoxelsVolume[ChunkId][(x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk)] = (ushort)((Volume - MinVolume) * UshortPerValue);
            Dirty = true;
        }

        /// <summary>
        /// Set Type and Volume to X,Y,Z voxels
        /// </summary>
        public override void SetTypeAndVolume(int x, int y, int z, byte Type, float Volume)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                if (Type == 0 && Volume < 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            if (Volume > MaxVolume) Volume = MaxVolume;
            if (Volume < MinVolume) Volume = MinVolume;

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Voxels[ChunkId][x] = Type;
            VoxelsVolume[ChunkId][x] = (ushort)((Volume - MinVolume) * UshortPerValue);
            Dirty = true;
        }

        /// <summary>
        /// Set Type and Volume to X,Y,Z voxels
        /// </summary>
        public override void SetTypeAndVolume(int x, int y, int z,int ChunkId, byte Type, float Volume)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                if (Type == 0 && Volume < 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            if (Volume > MaxVolume) Volume = MaxVolume;
            if (Volume < MinVolume) Volume = MinVolume;

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Voxels[ChunkId][x] = Type;
            VoxelsVolume[ChunkId][x] = (ushort)((Volume - MinVolume) * UshortPerValue);
            Dirty = true;
        }

        /// <summary>
        /// Set Type and Volume to X,Y,Z voxels
        /// </summary>
        public override void SetTypeAndVolume(int x, int y, int z, int ChunkId, byte Type, float Volume,ref bool IsSafe)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (!IsSafe && Voxels[ChunkId] == null)
            {
                if (Type == 0 && Volume < 0)
                    return;

                InitChunk(x, y, z, ChunkId);
                IsSafe = true;
            }

            if (Volume > MaxVolume) Volume = MaxVolume;
            if (Volume < MinVolume) Volume = MinVolume;

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Voxels[ChunkId][x] = Type;
            VoxelsVolume[ChunkId][x] = (ushort)((Volume - MinVolume) * UshortPerValue);
            Dirty = true;
        }

        /// <summary>
        /// Set Type and Volume to X,Y,Z voxels
        /// </summary>
        public override void SetTypeAndVolume(int x, int y, int z, int ChunkId, int Index, byte Type, float Volume)
        {
            if (Voxels[ChunkId] == null)
            {
                if (Type == 0 && Volume < 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            if (Volume > MaxVolume) Volume = MaxVolume;
            if (Volume < MinVolume) Volume = MinVolume;

            Voxels[ChunkId][Index] = Type;
            VoxelsVolume[ChunkId][Index] = (ushort)((Volume - MinVolume) * UshortPerValue);
            Dirty = true;
        }

        public override void SetType(int x, int y, int z, byte Type)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            if (Voxels[ChunkId] == null)
            {
                if (Type == 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Voxels[ChunkId][x] = Type;
            Dirty = true;
        }

        public override void SetType(int x, int y, int z, byte Type, int ChunkId)
        {
            if (Voxels[ChunkId] == null)
            {
                if (Type == 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Dirty = true;
        }

        public override byte GetType(int x, int y, int z)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            byte Type = 0;
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                float Volume = 0;
                GetHeight(x, y, z, ref Type, ref Volume);
                return Type;
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Type = Voxels[ChunkId][x];
            return Type;
        }

        public override bool HasVolume(int x, int y, int z, float MinVolume)
        {
            byte Type = 0;
            float Volume = 0;
            GetTypeAndVolume(x, y, z, ref Type, ref Volume);
            if (Type == 0 || Volume < MinVolume)
                return false;

            return true;
        }

        /// <summary>
        /// Fast Method to get/set all voxels. Can be stopped and restarted
        /// </summary>
        public override void LoopOverAllVoxels(LoopStepInformation StepInfo, CanContinueLoop CanContinue, LoopOverVoxelsDelegate Function)
        {
            LoopOverYVoxels(StepInfo, CanContinue, Function);
        }

        public override void LoopOverYVoxels(LoopStepInformation StepInfo, CanContinueLoop CanContinue, LoopOverVoxelsDelegate Function)
        {
            int ChunkX = 0,ChunkY = 0,ChunkZ = 0, ChunkId;
            int StartX = 0, StartZ = 0;
            int EndX = Information.ChunkPerBlock, EndZ = Information.ChunkPerBlock;

            int px, py, pz, f, x, y, z;
            int VoxelPerChunk = Information.VoxelPerChunk;
            bool CallEmptyChunks = StepInfo.CallEmptyChunks;

            byte Type = 0,HeightType=0;
            float Volume = 0, Height = 0;
            byte[] Array;
            ushort[] Volumes;
            StepInfo.IsDone = false;

            if(StepInfo.ForceChunkX != -1)
            {
                StartX = StepInfo.ForceChunkX;
                EndX = Mathf.Min(StepInfo.ForceChunkX + 1,Information.ChunkPerBlock);
            }
            if (StepInfo.ForceChunkY == -1)
                StepInfo.ForceChunkY = 0;

            if (StepInfo.ForceChunkZ != -1)
            {
                StartZ = StepInfo.ForceChunkZ;
                EndZ = Mathf.Min(StepInfo.ForceChunkZ + 1,Information.ChunkPerBlock);
            }

            for (ChunkX = StartX; ChunkX < EndX; ++ChunkX)
            {
                for (ChunkZ = StartZ; ChunkZ < EndZ; ++ChunkZ)
                {
                    for (x = 0; x < VoxelPerChunk; ++x)
                    {
                        px = x + ChunkX * VoxelPerChunk;

                        for (z = 0; z < VoxelPerChunk; ++z)
                        {
                            pz = z + ChunkZ * VoxelPerChunk;

                            if (UseHeight != 2)
                            {
                                HeightType = 0;
                                Height = GetHeight(px, pz, ref HeightType);
                                Height -= WorldY;
                            }

                            for (ChunkY = 0; ChunkY < Information.ChunkPerBlock; ++ChunkY)
                            {
                                ChunkId = ChunkX * Information.ChunkPerBlock * Information.ChunkPerBlock + ChunkY * Information.ChunkPerBlock + ChunkZ;
                                py = ChunkY * VoxelPerChunk;
                                f = x * VoxelPerChunk * VoxelPerChunk + z;

                                Array = Voxels[ChunkId];

                                if (Array == null)
                                {
                                    if (UseHeight != 2)
                                    {
                                        if (CallEmptyChunks)
                                        {
                                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                            {
                                                Type = HeightType;
                                                Volume = Height - (float)py;

                                                if (Function(px, py, pz, ChunkId, f, ref Type, ref Volume))
                                                {
                                                    SetTypeAndVolume(px, py, pz, ChunkId, f, Type, Volume);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                            {
                                                Type = HeightType;
                                                Volume = Height - (float)py;

                                                if (Function(px, py, pz, ChunkId, f, ref Type, ref Volume))
                                                {
                                                    SetTypeAndVolume(px, py, pz, ChunkId, f, Type, Volume);
                                                }

                                                if (Volume < 0f)
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (CallEmptyChunks)
                                        {
                                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                            {
                                                Volume = 0;
                                                Type = 0;

                                                if (Function(px, py, pz, ChunkId, f, ref Type, ref Volume))
                                                {
                                                    SetTypeAndVolume(px, py, pz, ChunkId, f, Type, Volume);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Volumes = VoxelsVolume[ChunkId];

                                    for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                    {
                                        Type = Array[f];
                                        Volume = ((float)Volumes[f] + MinUshort) * InvertUshortPerValue;

                                        if (Function(px, py, pz, ChunkId, f, ref Type, ref Volume))
                                        {

                                            Array[f] = Type;
                                            Volumes[f] = (ushort)((Volume - MinVolume) * UshortPerValue);
                                            Dirty = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            StepInfo.Clear();
        }

        public override void LoopOverYVoxelsFast(LoopStepInformation StepInfo, CanContinueLoop CanContinue, LoopOverVoxelsFastDelegate Function)
        {
            int ChunkPerBlock = Information.ChunkPerBlock;
            int EndX = ChunkPerBlock, EndZ = ChunkPerBlock;

            int px, py, pz, f, x, y, z;
            int VoxelPerChunk = Information.VoxelPerChunk;
            bool CallEmptyChunks = StepInfo.CallEmptyChunks;

            byte HeightType = 0;
            float Height = 0;
            byte[] Array;
            ushort[] Volumes;
            StepInfo.IsDone = false;

            int ChunkX = StepInfo.ForceChunkX;
            int ChunkZ = StepInfo.ForceChunkZ;

            int StartX = ChunkX * VoxelPerChunk;
            int StartZ = ChunkZ * VoxelPerChunk;
            int ChunkY, ChunkId;

            for (x = 0; x < VoxelPerChunk; ++x)
            {
                px = x + ChunkX * VoxelPerChunk;

                for (z = 0; z < VoxelPerChunk; ++z)
                {
                    pz = z + ChunkZ * VoxelPerChunk;

                    if (UseHeight != 2)
                    {
                        HeightType = 0;
                        Height = GetHeight(px, pz, ref HeightType);
                        Height -= WorldY;
                    }

                    ChunkId = ChunkX * ChunkPerBlock * ChunkPerBlock + ChunkZ;

                    for (ChunkY = 0; ChunkY < ChunkPerBlock; ++ChunkY, ChunkId+=ChunkPerBlock)
                    {
                        py = ChunkY * VoxelPerChunk;
                        f = x * VoxelPerChunk * VoxelPerChunk + z;

                        if (Voxels[ChunkId] == null)
                        {
                            if (UseHeight != 2)
                            {
                                if (CallEmptyChunks)
                                {
                                    for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                    {
                                        Function(px, py, pz, ChunkId, StartX, StartZ, f, HeightType, Height - (float)py);
                                    }
                                }
                                else
                                {
                                    for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                    {
                                        Function(px, py, pz, ChunkId, StartX, StartZ, f, HeightType, Height - (float)py);
                                    }
                                }
                            }
                            else
                            {
                                if (CallEmptyChunks)
                                {
                                    for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                    {
                                        Function(px, py, pz, ChunkId, StartX, StartZ, f, 0, 0);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Array = Voxels[ChunkId];
                            Volumes = VoxelsVolume[ChunkId];

                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                            {
                                Function(px, py, pz, ChunkId, StartX, StartZ, f, Array[f], ((float)Volumes[f] + MinUshort) * InvertUshortPerValue);
                            }
                        }
                    }
                }
            }
                
            

            StepInfo.Clear();
        }

        public override void GenerateGroundPoints(BiomeData Data, int ChunkX, int ChunkZ)
        {
            Fast2DArray<byte> TempDatas = Data.GetCustomData("TempDatas") as Fast2DArray<byte>;
            bool LastIsFull = false;

            int ChunkY = 0, ChunkId;
            int ChunkPerBlock = Information.ChunkPerBlock;
            int EndX = ChunkPerBlock, EndZ = ChunkPerBlock;

            int px, py, pz, f, x, y, z;
            int VoxelPerChunk = Information.VoxelPerChunk;

            byte HeightType = 0;
            float Height = 0;
            byte[] Array;
            ushort[] Volumes;
            byte LastType = 0;

            int StartX, StartZ,StartY;

            StartX = ChunkX * VoxelPerChunk;
            StartZ = ChunkZ * VoxelPerChunk;

            for (x = 0; x < VoxelPerChunk; ++x)
            {
                px = x + ChunkX * VoxelPerChunk;

                for (z = 0; z < VoxelPerChunk; ++z)
                {
                    pz = z + ChunkZ * VoxelPerChunk;

                    if (UseHeight != 2)
                    {
                        HeightType = 0;
                        Height = GetHeight(px, pz, ref HeightType);
                        Height -= WorldY;
                    }

                    for (ChunkY = 0; ChunkY < ChunkPerBlock; ++ChunkY)
                    {
                        ChunkId = ChunkX * ChunkPerBlock * ChunkPerBlock + ChunkY * ChunkPerBlock + ChunkZ;

                        if (Voxels[ChunkId] == null)
                        {
                            if (UseHeight != 2)
                            {
                                StartY = ChunkY * VoxelPerChunk;

                                py = (int)Height;
                                AddPoint(Data, (byte)x, (ushort)0, (byte)z, HeightType, Height, ref LastIsFull,ref LastType, TempDatas);

                                if (py >= StartY && py < StartY + VoxelPerChunk)
                                {
                                    AddPoint(Data, (byte)x, (ushort)py, (byte)z, HeightType, Height - (float)py, ref LastIsFull,ref LastType, TempDatas);
                                    AddPoint(Data, (byte)x, (ushort)(py + 1), (byte)z, HeightType, Height - (float)(py+1), ref LastIsFull,ref LastType, TempDatas);
                                }
                            }
                        }
                        else
                        {
                            py = ChunkY * VoxelPerChunk;
                            f = x * VoxelPerChunk * VoxelPerChunk + z;

                            Array = Voxels[ChunkId];
                            Volumes = VoxelsVolume[ChunkId];

                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                            {
                                AddPoint(Data, (byte)x, (ushort)py, (byte)z, Array[f], ((float)Volumes[f] + MinUshort) * InvertUshortPerValue, ref LastIsFull,ref LastType, TempDatas);
                            }
                        }
                    }
                }
            }

            base.GenerateGroundPoints(Data, ChunkX, ChunkZ);
        }

        public void AddPoint(BiomeData Data, byte x, ushort y, byte z,byte Type,float Volume, ref bool LastIsFull,ref byte LastType, Fast2DArray<byte> TempDatas)
        {
            if (y == 0)
            {
                LastIsFull = Type != 0 && Volume >= TerrainInformation.Isolevel;
                LastType = Type;
                return;
            }

            //VoxelTypes VType = Server.Generator.Palette[Type].VoxelType;

            // If current voxel is full and last voxel is empty. Add TopGroud
            if (Type != 0 && Volume >= TerrainInformation.Isolevel)
            {
                if (!LastIsFull)
                {
                    GroundPoint Point = new GroundPoint();
                    Point.x = (byte)(x);
                    Point.y = (ushort)y;
                    Point.z = (byte)(z);
                    Point.Type = Type;
                    Point.Side = 1;
                    Data.GroundPoints.Add(Point);
                }

                LastIsFull = true;
            }
            else
            {
                if (LastIsFull)
                {
                    GroundPoint Point = new GroundPoint();
                    Point.x = (byte)(x);
                    Point.y = (ushort)(y - 1);
                    Point.z = (byte)(z);
                    Point.Type = LastType;
                    Point.Side = 0;
                    Data.GroundPoints.Add(Point);
                }

                LastIsFull = false;
            }

            if (y == Information.VoxelsPerAxis - 1 && TempDatas != null)
            {
                if (Type != 0 && Volume >= TerrainInformation.Isolevel)
                {
                    if (TempDatas.array[(x) * Information.VoxelPerChunk + (z)] == 0)
                    {
                        GroundPoint Point = new GroundPoint();
                        Point.x = (byte)(x);
                        Point.y = (ushort)y;
                        Point.z = (byte)(z);
                        Point.Type = LastType;
                        Point.Side = 0;
                        Data.GroundPoints.Add(Point);
                    }
                }
                else
                {
                    if (TempDatas.array[(x) * Information.VoxelPerChunk + (z)] != 0)
                    {
                        GroundPoint Point = new GroundPoint();
                        Point.x = (byte)(x);
                        Point.y = (ushort)y;
                        Point.z = (byte)(z);
                        Point.Type = Type;
                        Point.Side = 1;
                        Data.GroundPoints.Add(Point);
                    }

                }
            }

            LastType = Type;
        }

        #endregion

        #region SaveLoad

        /// <summary>
        /// Save current terrain block to a file
        /// </summary>
        public override void Save(BinaryWriter Stream)
        {
            base.Save(Stream);

            int VoxelCount = Information.TotalVoxelsPerChunk;

            int j = 0;
            byte[] Data;
            ushort[] Volumes;

            byte LastValue = 0;
            byte LastCount = 0;
            byte Value = 0;
            ushort Volume = 0;
            ushort LastVolume = 0;

            for (int i = 0; i < Information.TotalChunksPerBlock; ++i)
            {
                Data = Voxels[i];
                Volumes = VoxelsVolume[i];
                if (Data != null)
                {
                    Stream.Write(true);

                    // RLE Compression

                    ///////////// VOXEL TYPE //////////////////
                    LastValue = 0;
                    LastCount = 0;
                    for (j = 0; j < VoxelCount; ++j)
                    {
                        Value = Data[j];
                        if (Value != LastValue || LastCount >= 255)
                        {
                            if (LastCount != 0)
                            {
                                Stream.Write(LastCount);
                                Stream.Write(LastValue);
                            }

                            LastCount = 1;
                            LastValue = Value;
                        }
                        else
                            ++LastCount;
                    }

                    if (LastCount != 0)
                    {
                        Stream.Write(LastCount);
                        Stream.Write(LastValue);
                    }

                    //////////////// VOXEL VOLUME /////////////////////
                    LastVolume = 0;
                    LastCount = 0;

                    for (j = 0; j < VoxelCount; ++j)
                    {
                        Volume = Volumes[j];
                        if (Volume != LastVolume || LastCount >= 255)
                        {
                            if (LastCount != 0)
                            {
                                Stream.Write(LastCount);
                                Stream.Write(LastVolume);
                            }

                            LastCount = 1;
                            LastVolume = Volume;
                        }
                        else
                            ++LastCount;
                    }

                    if (LastCount != 0)
                    {
                        Stream.Write(LastCount);
                        Stream.Write(LastVolume);
                    }
                }
                else
                    Stream.Write(false);
            }
        }

        /// <summary>
        /// Read current terrain block data from file
        /// </summary>
        public override void Read(BinaryReader Stream)
        {
            base.Read(Stream);

            int VoxelCount = Information.TotalVoxelsPerChunk;

            int j = 0;
            byte[] Data;
            ushort[] Volumes;
            byte Count = 0;
            byte Value = 0;
            ushort Volume = 0;
            int Index = 0;
            for (int i = 0; i < Information.TotalChunksPerBlock; ++i)
            {
                if (Stream.ReadBoolean())
                {
                    Data = Voxels[i] = GetArray(VoxelCount);
                    Volumes = VoxelsVolume[i] = GetArrayVolume(VoxelCount);

                    for (j = 0; j < VoxelCount;)
                    {
                        Count = Stream.ReadByte();
                        Value = Stream.ReadByte();
                        for (Index = 0; Index < Count; ++Index)
                        {
                            Data[j + Index] = Value;
                        }
                        j += Count;
                    }

                    for (j = 0; j < VoxelCount;)
                    {
                        Count = Stream.ReadByte();
                        Volume = Stream.ReadUInt16();
                        for (Index = 0; Index < Count; ++Index)
                        {
                            Volumes[j + Index] = Volume;
                        }
                        j += Count;
                    }
                }
            }
        }

        #endregion
    }
}
