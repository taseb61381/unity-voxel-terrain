﻿using System.Collections.Generic;
using System.IO;

using UnityEngine;

namespace TerrainEngine
{
    /// <summary>
    /// Default Voxels Data Storage. Can store each voxel information or an 2DHeightmap or an HeightMap function
    /// Store 1 Byte . VoxelType
    /// </summary>
    public class TypeTerrainData : TerrainDatas
    {
        static public PoolInfoRef VoxelsPool = new PoolInfoRef();
        static public Stack<byte[]> Pool = new Stack<byte[]>();

        public override void Init(TerrainInformation Informations, VectorI3 Position)
        {
            if (VoxelsPool.Index == 0)
                VoxelsPool = PoolManager.Pool.GetPool("VoxelsArray");

            base.Init(Informations, Position);

            if (Voxels == null)
            {
                Voxels = new byte[Information.TotalChunksPerBlock][];
            }
        }

        /// <summary>
        /// Remove all voxels array and add them to the pool
        /// </summary>
        public override void Clear()
        {
            lock (Pool)
            {
                for (int ChunkId = Information.TotalChunksPerBlock - 1; ChunkId >= 0; --ChunkId)
                {
                    if (Voxels[ChunkId] != null)
                    {
                        Pool.Push(Voxels[ChunkId]);
                        Voxels[ChunkId] = null;
                        ++VoxelsPool.EnqueueData;
                    }
                }
            }
            base.Clear();
        }

        #region Chunks

        /// <summary>
        /// Return an new byte[] or use Pool is exist
        /// </summary>
        private byte[] GetArray(int Size)
        {
            lock (Pool)
            {
                if (Pool.Count != 0)
                {
                    ++VoxelsPool.DequeueData;
                    return Pool.Pop();
                }
            }

            ++VoxelsPool.CreatedObject;
            return new byte[Size];
        }

        /// <summary>
        /// Init a new chunk voxel array. This function will look at HeightMap system to get voxel data when creating new voxels array
        /// </summary>
        private void InitChunk(int x, int y, int z, int ChunkId)
        {
            int VoxelCountPerChunk = Information.TotalVoxelsPerChunk;
            byte[] Chunk = Voxels[ChunkId] = GetArray(VoxelCountPerChunk);
            int i = 0;

            // If an height system is valid we set the chunk voxels to values extracted from Height
            if (HeightFunction != null || HeightGrid != null)
            {
                byte Type = 0;
                float Volume = 0;
                int sx = (x / Information.VoxelPerChunk) * Information.VoxelPerChunk;
                int sy = (y / Information.VoxelPerChunk) * Information.VoxelPerChunk;
                int sz = (z / Information.VoxelPerChunk) * Information.VoxelPerChunk;

                int ex = sx + Information.VoxelPerChunk, ey = sy + Information.VoxelPerChunk, ez = sz + Information.VoxelPerChunk;
                int px, py, pz;

                if (HeightFunction != null)
                {
                    for (px = sx; px < ex; ++px)
                        for (py = sy; py < ey; ++py)
                            for (pz = sz; pz < ez; ++pz)
                            {
                                GetHeightFromFunction(px, py, pz, ref Type, ref Volume);
                                Chunk[i] = Type;
                                ++i;
                            }
                }
                else
                {
                    for (px = sx; px < ex; ++px)
                        for (py = sy; py < ey; ++py)
                            for (pz = sz; pz < ez; ++pz)
                            {
                                GetHeightFromGrid(px, py, pz, ref Type, ref Volume);
                                Chunk[i] = Type;
                                ++i;
                            }
                }
            }
            else
            {
                for (i = VoxelCountPerChunk - 1; i >= 0; --i)
                {
                    Chunk[i] = 0;
                }
            }

        }

        #endregion

        #region ReadWrite

        /// <summary>
        /// Return Type of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public override byte GetType(int x, int y, int z)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                byte Type = 0;
                float Volume = 0;
                GetHeight(x, y, z, ref Type, ref Volume);
                return Type;
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            return Voxels[ChunkId][x];
        }

        /// <summary>
        /// Return Type and Volume of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public override byte GetTypeAndVolume(int x, int y, int z, ref byte Type, ref float Volume)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                GetHeight(x, y, z, ref Type, ref Volume);
                return Type;
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Type = Voxels[ChunkId][x];
            Volume = Type != 0 ? 1f : 0;
            return Type;
        }

        /// <summary>
        /// Return Type and Volume of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public override byte GetTypeAndVolume(int x, int y, int z, int ChunkId, ref byte Type, ref float Volume)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                GetHeight(x, y, z, ref Type, ref Volume);
                return Type;
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Type = Voxels[ChunkId][x];
            Volume = Type != 0 ? 1f : 0;
            return Type;
        }

        /// <summary>
        /// Return Type and Volume of voxel at given X/Y/Z position. Use HeightMap if exist
        /// </summary>
        public override byte GetTypeAndVolume(int x, int y, int z, int ChunkId, int Index, ref byte Type, ref float Volume)
        {
            // Chunk do not contains voxels. Use HeightMap if exist
            if (Voxels[ChunkId] == null)
            {
                GetHeight(x, y, z, ref Type, ref Volume);
                return Type;
            }

            Type = Voxels[ChunkId][Index];
            Volume = Type == 0 ? 0f : 1f;
            return Type;
        }

        /// <summary>
        /// Set Type and Volume to X,Y,Z voxels
        /// </summary>
        public override void SetTypeAndVolume(int x, int y, int z, byte Type, float Volume)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);

            if (Volume < TerrainInformation.Isolevel)
                Type = 0;

            if (Voxels[ChunkId] == null)
            {
                if (Type == 0 && Volume < 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Voxels[ChunkId][x] = Type;
        }

        /// <summary>
        /// Set Type and Volume to X,Y,Z voxels
        /// </summary>
        public override void SetTypeAndVolume(int x, int y, int z, int ChunkId, int Index, byte Type, float Volume)
        {
            if (Voxels[ChunkId] == null)
            {
                if (Type == 0 && Volume < 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            Voxels[ChunkId][Index] = Type;
        }

        public override void SetType(int x, int y, int z, byte Type)
        {
            int ChunkId = (x / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock + (y / Information.VoxelPerChunk) * Information.ChunkPerBlock + (z / Information.VoxelPerChunk);
            if (Voxels[ChunkId] == null)
            {
                if (Type == 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Voxels[ChunkId][x] = Type;
        }

        public override void SetType(int x, int y, int z, byte Type, int ChunkId)
        {
            if (Voxels[ChunkId] == null)
            {
                if (Type == 0)
                    return;

                InitChunk(x, y, z, ChunkId);
            }

            x = (x % Information.VoxelPerChunk) * Information.VoxelPerChunk * Information.VoxelPerChunk + (y % Information.VoxelPerChunk) * Information.VoxelPerChunk + (z % Information.VoxelPerChunk);
            Voxels[ChunkId][x] = Type;
        }

        public override bool HasVolume(int x, int y, int z, float MinVolume)
        {
            byte Type = 0;
            float Volume = 0;
            GetTypeAndVolume(x, y, z, ref Type, ref Volume);
            if (Type == 0 || Volume < MinVolume)
                return false;

            return true;
        }

        public override void LoopOverYVoxels(LoopStepInformation StepInfo, CanContinueLoop CanContinue, LoopOverVoxelsDelegate Function)
        {
            int ChunkX = 0, ChunkY = 0, ChunkZ = 0, ChunkId;
            int StartX = 0, StartZ = 0;
            int EndX = Information.ChunkPerBlock, EndZ = Information.ChunkPerBlock;

            int px, py, pz, f, x, y, z;
            int VoxelPerChunk = Information.VoxelPerChunk;
            bool CallEmptyChunks = StepInfo.CallEmptyChunks;

            byte Type = 0, HeightType = 0;
            float Volume = 0, Height = 0;
            byte[] Array;
            StepInfo.IsDone = false;

            if (StepInfo.ForceChunkX != -1)
            {
                StartX = StepInfo.ForceChunkX;
                EndX = Mathf.Min(StepInfo.ForceChunkX + 1, Information.ChunkPerBlock);
            }
            if (StepInfo.ForceChunkY == -1)
                StepInfo.ForceChunkY = 0;

            if (StepInfo.ForceChunkZ != -1)
            {
                StartZ = StepInfo.ForceChunkZ;
                EndZ = Mathf.Min(StepInfo.ForceChunkZ + 1, Information.ChunkPerBlock);
            }

            for (ChunkX = StartX; ChunkX < EndX; ++ChunkX)
            {
                for (ChunkZ = StartZ; ChunkZ < EndZ; ++ChunkZ)
                {
                    for (x = 0; x < VoxelPerChunk; ++x)
                    {
                        px = x + ChunkX * VoxelPerChunk;

                        for (z = 0; z < VoxelPerChunk; ++z)
                        {
                            pz = z + ChunkZ * VoxelPerChunk;

                            for (ChunkY = 0; ChunkY < Information.ChunkPerBlock; ++ChunkY)
                            {
                                ChunkId = ChunkX * Information.ChunkPerBlock * Information.ChunkPerBlock + ChunkY * Information.ChunkPerBlock + ChunkZ;
                                py = ChunkY * VoxelPerChunk;
                                f = x * VoxelPerChunk * VoxelPerChunk + z;

                                Array = Voxels[ChunkId];

                                if (Array == null)
                                {
                                    if (UseHeight != 2)
                                    {
                                        HeightType = 0;
                                        Height = GetHeight(px, pz, ref HeightType);

                                        if (CallEmptyChunks)
                                        {
                                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                            {
                                                Type = HeightType;
                                                Volume = Height - (float)py - (float)WorldY;

                                                if (Function(px, py, pz, ChunkId, f, ref Type, ref Volume))
                                                {
                                                    SetTypeAndVolume(px, py, pz, ChunkId, f, Type, Volume);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                            {
                                                Type = HeightType;
                                                Volume = Height - (float)py - (float)WorldY;

                                                if (Function(px, py, pz, ChunkId, f, ref Type, ref Volume))
                                                {
                                                    SetTypeAndVolume(px, py, pz, ChunkId, f, Type, Volume);
                                                }

                                                if (Type == 0 || Volume <= 0f)
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (CallEmptyChunks)
                                        {
                                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                            {
                                                Volume = 0;
                                                Type = 0;

                                                if (Function(px, py, pz, ChunkId, f, ref Type, ref Volume))
                                                {
                                                    SetTypeAndVolume(px, py, pz, ChunkId, f, Type, Volume);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                    {
                                        Type = Array[f];
                                        Volume = Type != 0 ? 1f : 0f;
                                        if (Function(px, py, pz, ChunkId, f, ref Type, ref Volume))
                                        {
                                            Array[f] = Type;
                                            Dirty = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            StepInfo.Clear();
        }

        public override void LoopOverYVoxelsFast(LoopStepInformation StepInfo, CanContinueLoop CanContinue, LoopOverVoxelsFastDelegate Function)
        {
            int ChunkX = 0, ChunkY = 0, ChunkZ = 0, ChunkId;
            int SChunkX = 0, SChunkZ = 0;
            int EndX = Information.ChunkPerBlock, EndZ = Information.ChunkPerBlock;

            int px, py, pz, f, x, y, z;
            int VoxelPerChunk = Information.VoxelPerChunk;
            bool CallEmptyChunks = StepInfo.CallEmptyChunks;

            byte Type = 0, HeightType = 0;
            float Volume = 0, Height = 0;
            byte[] Array;
            StepInfo.IsDone = false;

            if (StepInfo.ForceChunkX != -1)
            {
                SChunkX = StepInfo.ForceChunkX;
                EndX = Mathf.Min(StepInfo.ForceChunkX + 1, Information.ChunkPerBlock);
            }
            if (StepInfo.ForceChunkY == -1)
                StepInfo.ForceChunkY = 0;

            if (StepInfo.ForceChunkZ != -1)
            {
                SChunkZ = StepInfo.ForceChunkZ;
                EndZ = Mathf.Min(StepInfo.ForceChunkZ + 1, Information.ChunkPerBlock);
            }

            int StartX, StartZ;

            for (ChunkX = SChunkX; ChunkX < EndX; ++ChunkX)
            {
                StartX = ChunkX * VoxelPerChunk;
                for (ChunkZ = SChunkZ; ChunkZ < EndZ; ++ChunkZ)
                {
                    StartZ = ChunkZ * VoxelPerChunk;

                    for (x = 0; x < VoxelPerChunk; ++x)
                    {
                        px = x + ChunkX * VoxelPerChunk;

                        for (z = 0; z < VoxelPerChunk; ++z)
                        {
                            pz = z + ChunkZ * VoxelPerChunk;

                            for (ChunkY = 0; ChunkY < Information.ChunkPerBlock; ++ChunkY)
                            {
                                ChunkId = ChunkX * Information.ChunkPerBlock * Information.ChunkPerBlock + ChunkY * Information.ChunkPerBlock + ChunkZ;
                                py = ChunkY * VoxelPerChunk;
                                f = x * VoxelPerChunk * VoxelPerChunk + z;

                                Array = Voxels[ChunkId];

                                if (Array == null)
                                {
                                    if (UseHeight != 2)
                                    {
                                        HeightType = 0;
                                        Height = GetHeight(px, pz, ref HeightType);

                                        if (CallEmptyChunks)
                                        {
                                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                            {
                                                Function(px, py, pz, ChunkId,StartX,StartZ, f, HeightType, Height - (float)py - (float)WorldY);
                                            }
                                        }
                                        else
                                        {
                                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                            {
                                                Type = HeightType;
                                                Volume = Height - (float)py - (float)WorldY;

                                                Function(px, py, pz, ChunkId,StartX,StartZ, f, Type, Volume);

                                                if (Type == 0 || Volume <= 0f)
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (CallEmptyChunks)
                                        {
                                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                            {
                                                Volume = 0;
                                                Type = 0;

                                                Function(px, py, pz, ChunkId,StartX,StartZ, f, Type, Volume);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                                    {
                                        Type = Array[f];
                                        Function(px, py, pz, ChunkId,StartX,StartZ, f, Type, Type != 0 ? 1f : 0f);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            StepInfo.Clear();
        }

        public override void GenerateGroundPoints(BiomeData Data, int ChunkX, int ChunkZ)
        {
            Fast2DArray<byte> TempDatas = Data.GetCustomData("TempDatas") as Fast2DArray<byte>;
            bool LastIsFull = false;

            int ChunkY = 0, ChunkId;
            int ChunkPerBlock = Information.ChunkPerBlock;
            int EndX = ChunkPerBlock, EndZ = ChunkPerBlock;

            int px, py, pz, f, x, y, z;
            int VoxelPerChunk = Information.VoxelPerChunk;

            byte HeightType = 0;
            float Height = 0;
            byte[] Array;
            byte LastType = 0;

            int StartX, StartZ, StartY;

            StartX = ChunkX * VoxelPerChunk;
            StartZ = ChunkZ * VoxelPerChunk;

            for (x = 0; x < VoxelPerChunk; ++x)
            {
                px = x + ChunkX * VoxelPerChunk;

                for (z = 0; z < VoxelPerChunk; ++z)
                {
                    pz = z + ChunkZ * VoxelPerChunk;

                    if (UseHeight != 2)
                    {
                        HeightType = 0;
                        Height = GetHeight(px, pz, ref HeightType);
                        Height -= WorldY;
                    }

                    for (ChunkY = 0; ChunkY < ChunkPerBlock; ++ChunkY)
                    {
                        ChunkId = ChunkX * ChunkPerBlock * ChunkPerBlock + ChunkY * ChunkPerBlock + ChunkZ;

                        if (Voxels[ChunkId] == null)
                        {
                            if (UseHeight != 2)
                            {
                                StartY = ChunkY * VoxelPerChunk;

                                py = (int)Height;
                                AddPoint(Data, (byte)x, (ushort)0, (byte)z, HeightType, Height, ref LastIsFull, ref LastType, TempDatas);

                                if (py >= StartY && py < StartY + VoxelPerChunk)
                                {
                                    AddPoint(Data, (byte)x, (ushort)py, (byte)z, HeightType, Height - (float)py, ref LastIsFull, ref LastType, TempDatas);
                                    AddPoint(Data, (byte)x, (ushort)(py + 1), (byte)z, HeightType, Height - (float)(py + 1), ref LastIsFull, ref LastType, TempDatas);
                                }
                            }
                        }
                        else
                        {
                            py = ChunkY * VoxelPerChunk;
                            f = x * VoxelPerChunk * VoxelPerChunk + z;

                            Array = Voxels[ChunkId];

                            for (y = 0; y < VoxelPerChunk; ++y, ++py, f += VoxelPerChunk)
                            {
                                AddPoint(Data, (byte)x, (ushort)py, (byte)z, Array[f], Array[f] != 0 ? 1f : 0f, ref LastIsFull, ref LastType, TempDatas);
                            }
                        }
                    }
                }
            }

            base.GenerateGroundPoints(Data, ChunkX, ChunkZ);
        }

        public void AddPoint(BiomeData Data, byte x, ushort y, byte z, byte Type, float Volume, ref bool LastIsFull, ref byte LastType, Fast2DArray<byte> TempDatas)
        {
            if (y == 0)
            {
                LastIsFull = Type != 0 && Volume >= TerrainInformation.Isolevel;
                LastType = Type;
                return;
            }

            //VoxelTypes VType = Server.Generator.Palette[Type].VoxelType;

            // If current voxel is full and last voxel is empty. Add TopGroud
            if (Type != 0 && Volume >= TerrainInformation.Isolevel)
            {
                if (!LastIsFull)
                {
                    GroundPoint Point = new GroundPoint();
                    Point.x = (byte)(x);
                    Point.y = (ushort)y;
                    Point.z = (byte)(z);
                    Point.Type = Type;
                    Point.Side = 1;
                    Data.GroundPoints.Add(Point);
                }

                LastIsFull = true;
            }
            else
            {
                if (LastIsFull)
                {
                    GroundPoint Point = new GroundPoint();
                    Point.x = (byte)(x);
                    Point.y = (ushort)(y - 1);
                    Point.z = (byte)(z);
                    Point.Type = LastType;
                    Point.Side = 0;
                    Data.GroundPoints.Add(Point);
                }

                LastIsFull = false;
            }

            if (y == Information.VoxelsPerAxis - 1 && TempDatas != null)
            {
                if (Type != 0 && Volume >= TerrainInformation.Isolevel)
                {
                    if (TempDatas.array[(x) * Information.VoxelPerChunk + (z)] == 0)
                    {
                        GroundPoint Point = new GroundPoint();
                        Point.x = (byte)(x);
                        Point.y = (ushort)y;
                        Point.z = (byte)(z);
                        Point.Type = LastType;
                        Point.Side = 0;
                        Data.GroundPoints.Add(Point);
                    }
                }
                else
                {
                    if (TempDatas.array[(x) * Information.VoxelPerChunk + (z)] != 0)
                    {
                        GroundPoint Point = new GroundPoint();
                        Point.x = (byte)(x);
                        Point.y = (ushort)y;
                        Point.z = (byte)(z);
                        Point.Type = Type;
                        Point.Side = 1;
                        Data.GroundPoints.Add(Point);
                    }

                }
            }

            LastType = Type;
        }


        #endregion

        #region SaveLoad

        /// <summary>
        /// Save current terrain block to a file
        /// </summary>
        public override void Save(BinaryWriter Stream)
        {
            base.Save(Stream);

            int VoxelCount = Information.TotalVoxelsPerChunk;

            int j = 0;
            byte[] Data;
            for (int i = 0; i < Information.TotalChunksPerBlock; ++i)
            {
                Data = Voxels[i];
                if (Data != null)
                {
                    Stream.Write(true);

                    for (j = 0; j < VoxelCount; ++j)
                    {
                        Stream.Write(Data[j]);
                    }
                }
                else
                    Stream.Write(false);
            }
        }

        /// <summary>
        /// Read current terrain block data from file
        /// </summary>
        public override void Read(BinaryReader Stream)
        {
            base.Read(Stream);

            int VoxelCount = Information.TotalVoxelsPerChunk;

            int j = 0;
            byte[] Data;
            for (int i = 0; i < Information.TotalChunksPerBlock; ++i)
            {
                if (Stream.ReadBoolean())
                {
                    Data = Voxels[i] = GetArray(VoxelCount);

                    for (j = 0; j < VoxelCount; ++j)
                    {
                        Data[j] = Stream.ReadByte();
                    }
                }
            }
        }

        #endregion
    }
}
