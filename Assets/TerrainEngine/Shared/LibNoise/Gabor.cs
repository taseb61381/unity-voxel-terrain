﻿using System;
using System.Collections;

using UnityEngine;


namespace LibNoise
{
    public class Gabor : IModule
    {
        static readonly double COS_OMEGA = System.Math.Cos(PI / 4.0f);
        static readonly double SIN_OMEGA = System.Math.Sin(PI / 4.0f);

        private const int period = 256;
        public int Seed = 0;
        public double Frequency = 1;
        public double K = 1;
        public double A = 0.05;
        public double F0 = 0.0625;

        //public bool Anisotropic { get; set; }

        public int Impluses = 32;

        double PI_A_A;

        double F0_2_PI;

        double m_kernelRadius;

        double m_impulseDensity;

        double m_impulsesPeCell;

        double m_varianceSqrt;

        public override void Init()
        {
            UpdateVariance(Impluses, K, A, F0);
        }

        public void UpdateVariance(int impulses, double K, double A, double F0)
        {

            this.K = K;
            this.A = A;
            this.F0 = F0;
            F0_2_PI = 2.0f * PI * this.F0;
            PI_A_A = PI * this.A * this.A;

            m_kernelRadius = System.Math.Sqrt(-System.Math.Log(0.05) / PI) / A;

            m_impulseDensity = impulses / (PI * m_kernelRadius * m_kernelRadius);

            m_impulsesPeCell = System.Math.Exp(-m_impulseDensity * m_kernelRadius * m_kernelRadius);

            double s = ((K * K) / (4.0 * A * A)) * (1.0 + System.Math.Exp(-(2.0 * PI * F0 * F0) / (A * A)));

            double variance = m_impulseDensity * (1.0 / 3.0) * s;

            m_varianceSqrt = System.Math.Sqrt(variance);
        }

        int Morton(int x, int y)
        {
            int z = 0;
            int num = sizeof(int) * 8;

            for (int i = 0; i < num; ++i)
            {
                z |= ((x & (1 << i)) << i) | ((y & (1 << i)) << (i + 1));
            }

            return z;
        }

        double AnisotropicGabor(double lx, double ly)
        {

            double gaussian_envelop = K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly));

            double sinusoidal_carrier = System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA));

            return gaussian_envelop * sinusoidal_carrier;

        }

        double IsotropicGabor(double omega_0, double x, double y)
        {

            double gaussian_envelop = K * System.Math.Exp(-PI_A_A * (x * x + y * y));

            double sinusoidal_carrier = System.Math.Cos(F0_2_PI * (x * System.Math.Cos(omega_0) + y * System.Math.Sin(omega_0)));

            return gaussian_envelop * sinusoidal_carrier;

        }

        public int Poisson(System.Random rnd)
        {
            double g = m_impulsesPeCell;
            int em = 0;
            double t = rnd.NextDouble();

            while (t > g)
            {
                ++em;
                t *= rnd.NextDouble();
            }

            return em;
        }


        public double Cell(int pi, int pj, double px, double py)
        {
            //System.Random rnd = new System.Random(s);
            //NetFastRandom rnd = new NetFastRandom(s);

            SFastRandom rnd = new SFastRandom();
            int number_of_impulses = 0;
            int s = 0;
            double g = m_impulsesPeCell;
            double t = 0;
            double noise = 0.0f;
            double x_i;
            double y_i;
            double w_i;
            double x_i_x;
            double y_i_y;
            double lx, ly;
            int k;

            number_of_impulses = 0;
            s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
            if (s == 0) s = 1;
            rnd.InitSeed(s);
            t = rnd.NextDouble();
            while (t > g)
            {
                ++number_of_impulses;
                t *= rnd.NextDouble();
            }

            for (k = 0; k < number_of_impulses; ++k)
            {
                x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                {
                    lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                    noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                }
            }

            return noise;

        }

        /// <summary>
        /// Sample the noise in 2 dimensions.
        /// </summary>
        /// <param name="x">The position in the x axis.</param>
        /// <param name="y">The position in the y axis.</param>
        /// <returns>A noise value between -1 and 1.</returns>
        public override double GetValue(double x, double z)
        {

            x = x * Frequency;
            z = z * Frequency;

            x /= m_kernelRadius;
            z /= m_kernelRadius;

            double int_x = System.Math.Floor(x);
            double int_y = System.Math.Floor(z);

            double frac_x = x - int_x;
            double frac_y = z - int_y;

            int i = (int)int_x;
            int j = (int)int_y;

            double noise = 0.0;

            int pi, pj;
            double px, py;
            SFastRandom rnd = new SFastRandom();
            int number_of_impulses = 0;
            int s = 0;
            double g = m_impulsesPeCell;
            double t = 0;
            double x_i;
            double y_i;
            double w_i;
            double x_i_x;
            double y_i_y;
            double lx, ly;
            int k;

            pi = i - 1; pj = j - 1; px = frac_x + 1; py = frac_y + 1;
            {
                number_of_impulses = 0;
                s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
                if (s == 0) s = 1;
                rnd.InitSeed(s);
                t = rnd.NextDouble();
                while (t > g)
                {
                    ++number_of_impulses;
                    t *= rnd.NextDouble();
                }

                for (k = 0; k < number_of_impulses; ++k)
                {
                    x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                    if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                    {
                        lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                        noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                    }
                }
            }

            pi = i - 1; pj = j; px = frac_x + 1; py = frac_y;
            {
                number_of_impulses = 0;
                s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
                if (s == 0) s = 1;
                rnd.InitSeed(s);
                t = rnd.NextDouble();
                while (t > g)
                {
                    ++number_of_impulses;
                    t *= rnd.NextDouble();
                }

                for (k = 0; k < number_of_impulses; ++k)
                {
                    x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                    if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                    {
                        lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                        noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                    }
                }
            }

            pi = i - 1; pj = j + 1; px = frac_x + 1; py = frac_y - 1;
            {
                number_of_impulses = 0;
                s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
                if (s == 0) s = 1;
                rnd.InitSeed(s);
                t = rnd.NextDouble();
                while (t > g)
                {
                    ++number_of_impulses;
                    t *= rnd.NextDouble();
                }

                for (k = 0; k < number_of_impulses; ++k)
                {
                    x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                    if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                    {
                        lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                        noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                    }
                }
            }

            pi = i; pj = j - 1; px = frac_x; py = frac_y + 1;
            {
                number_of_impulses = 0;
                s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
                if (s == 0) s = 1;
                rnd.InitSeed(s);
                t = rnd.NextDouble();
                while (t > g)
                {
                    ++number_of_impulses;
                    t *= rnd.NextDouble();
                }

                for (k = 0; k < number_of_impulses; ++k)
                {
                    x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                    if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                    {
                        lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                        noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                    }
                }
            }

            pi = i; pj = j; px = frac_x; py = frac_y;
            {
                number_of_impulses = 0;
                s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
                if (s == 0) s = 1;
                rnd.InitSeed(s);
                t = rnd.NextDouble();
                while (t > g)
                {
                    ++number_of_impulses;
                    t *= rnd.NextDouble();
                }

                for (k = 0; k < number_of_impulses; ++k)
                {
                    x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                    if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                    {
                        lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                        noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                    }
                }
            }

            pi = i; pj = j + 1; px = frac_x; py = frac_y - 1;
            {
                number_of_impulses = 0;
                s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
                if (s == 0) s = 1;
                rnd.InitSeed(s);
                t = rnd.NextDouble();
                while (t > g)
                {
                    ++number_of_impulses;
                    t *= rnd.NextDouble();
                }

                for (k = 0; k < number_of_impulses; ++k)
                {
                    x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                    if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                    {
                        lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                        noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                    }
                }
            }

            pi = i + 1; pj = j - 1; px = frac_x - 1; py = frac_y + 1;
            {
                number_of_impulses = 0;
                s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
                if (s == 0) s = 1;
                rnd.InitSeed(s);
                t = rnd.NextDouble();
                while (t > g)
                {
                    ++number_of_impulses;
                    t *= rnd.NextDouble();
                }

                for (k = 0; k < number_of_impulses; ++k)
                {
                    x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                    if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                    {
                        lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                        noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                    }
                }
            }

            pi = i + 1; pj = j; px = frac_x - 1; py = frac_y;
            {
                number_of_impulses = 0;
                s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
                if (s == 0) s = 1;
                rnd.InitSeed(s);
                t = rnd.NextDouble();
                while (t > g)
                {
                    ++number_of_impulses;
                    t *= rnd.NextDouble();
                }

                for (k = 0; k < number_of_impulses; ++k)
                {
                    x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                    if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                    {
                        lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                        noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                    }
                }
            }

            pi = i + 1; pj = j + 1; px = frac_x - 1; py = frac_y - 1;
            {
                number_of_impulses = 0;
                s = (((pj % period) * period) + (pi % period)) + Seed; // periodic noise
                if (s == 0) s = 1;
                rnd.InitSeed(s);
                t = rnd.NextDouble();
                while (t > g)
                {
                    ++number_of_impulses;
                    t *= rnd.NextDouble();
                }

                for (k = 0; k < number_of_impulses; ++k)
                {
                    x_i = rnd.NextDouble(); y_i = rnd.NextDouble(); w_i = rnd.NextDouble() * 2.0f - 1.0f; x_i_x = px - x_i; y_i_y = py - y_i;
                    if (x_i_x * x_i_x + y_i_y * y_i_y < 1.0f)
                    {
                        lx = x_i_x * m_kernelRadius; ly = y_i_y * m_kernelRadius;
                        noise += w_i * (K * System.Math.Exp(-PI_A_A * (lx * lx + ly * ly)) * System.Math.Cos(F0_2_PI * (lx * COS_OMEGA + ly * SIN_OMEGA)));
                    }
                }
            }


            /*or (int di = -1; di <= +1; ++di)
            {
                for (int dj = -1; dj <= +1; ++dj)
                {
                    noise += Cell(i + di, j + dj, frac_x - di, frac_y - dj);
                }
            }*/

            return noise / (3.0 * m_varianceSqrt);
        }

        public override double GetValue(double x, double y, double z)
        {
            return GetValue(x, z);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return GetValue(x, z);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return GetValue(x, z);
        }
    }
}
