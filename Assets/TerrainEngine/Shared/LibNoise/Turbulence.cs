﻿using System;

namespace LibNoise
{
    public class Turbulence
        : IModule
    {
        public IModule SourceModule { get; set; }

        public double Power { get; set; }

        private Perlin XDistort;
        private Perlin YDistort;
        private Perlin ZDistort;

        public Turbulence()
        {
            XDistort = new Perlin();
            YDistort = new Perlin();
            ZDistort = new Perlin();

            Frequency = 1.0;
            Power = 1.0;
            Roughness = 3;
            Seed = 0;
        }

        public Turbulence(IModule sourceModule)
        {
            if (sourceModule == null)
                throw new ArgumentNullException();

            SourceModule = sourceModule;

            XDistort = new Perlin();
            YDistort = new Perlin();
            ZDistort = new Perlin();

            Frequency = 1.0;
            Power = 1.0;
            Roughness = 3;
            Seed = 0;
        }

        public double Frequency
        {
            get { return XDistort.Frequency; }
            set
            {
                XDistort.Frequency = YDistort.Frequency = ZDistort.Frequency = value;
            }
        }

        public override double GetValue(double x, double y, double z)
        {
            // Get the values from the three noise::module::Perlin noise modules and
            // add each value to each coordinate of the input value.  There are also
            // some offsets added to the coordinates of the input values.  This prevents
            // the distortion modules from returning zero if the (x, y, z) coordinates,
            // when multiplied by the frequency, are near an integer boundary.  This is
            // due to a property of gradient coherent noise, which returns zero at
            // integer boundaries.

            // Retrieve the output value at the offsetted input value instead of the
            // original input value.
            return SourceModule.GetValue(
                x + (XDistort.GetValue(x + (12414.0 / 65536.0), y + (65124.0 / 65536.0), z + (31337.0 / 65536.0)) * Power),
                y + (YDistort.GetValue(x + (26519.0 / 65536.0), y + (18128.0 / 65536.0), z + (60493.0 / 65536.0)) * Power),
                z + (ZDistort.GetValue(x + (53820.0 / 65536.0), y + (11213.0 / 65536.0), z + (44845.0 / 65536.0)) * Power));
        }

        public override double GetValue(double x, double z)
        {
            // Get the values from the three noise::module::Perlin noise modules and
            // add each value to each coordinate of the input value.  There are also
            // some offsets added to the coordinates of the input values.  This prevents
            // the distortion modules from returning zero if the (x, y, z) coordinates,
            // when multiplied by the frequency, are near an integer boundary.  This is
            // due to a property of gradient coherent noise, which returns zero at
            // integer boundaries.

            // Retrieve the output value at the offsetted input value instead of the
            // original input value.
            return SourceModule.GetValue(
                x + (XDistort.GetValue(x + (12414.0 / 65536.0), z + (31337.0 / 65536.0)) * Power),
                z + (ZDistort.GetValue(x + (53820.0 / 65536.0), z + (44845.0 / 65536.0)) * Power));
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            // Get the values from the three noise::module::Perlin noise modules and
            // add each value to each coordinate of the input value.  There are also
            // some offsets added to the coordinates of the input values.  This prevents
            // the distortion modules from returning zero if the (x, y, z) coordinates,
            // when multiplied by the frequency, are near an integer boundary.  This is
            // due to a property of gradient coherent noise, which returns zero at
            // integer boundaries.

            // Retrieve the output value at the offsetted input value instead of the
            // original input value.
            return SourceModule.GetValue(
                x + (XDistort.GetValue(x + (12414.0 / 65536.0), y + (65124.0 / 65536.0), z + (31337.0 / 65536.0)) * Power),
                y + (YDistort.GetValue(x + (26519.0 / 65536.0), y + (18128.0 / 65536.0), z + (60493.0 / 65536.0)) * Power),
                z + (ZDistort.GetValue(x + (53820.0 / 65536.0), y + (11213.0 / 65536.0), z + (44845.0 / 65536.0)) * Power));
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            // Get the values from the three noise::module::Perlin noise modules and
            // add each value to each coordinate of the input value.  There are also
            // some offsets added to the coordinates of the input values.  This prevents
            // the distortion modules from returning zero if the (x, y, z) coordinates,
            // when multiplied by the frequency, are near an integer boundary.  This is
            // due to a property of gradient coherent noise, which returns zero at
            // integer boundaries.

            // Retrieve the output value at the offsetted input value instead of the
            // original input value.
            return SourceModule.GetValue(
                x + (XDistort.GetValue(x + (12414.0 / 65536.0), z + (31337.0 / 65536.0)) * Power),
                z + (ZDistort.GetValue(x + (53820.0 / 65536.0), z + (44845.0 / 65536.0)) * Power));
        }

        public int Roughness
        {
            get { return XDistort.OctaveCount; }
            set
            {
                XDistort.OctaveCount = YDistort.OctaveCount = ZDistort.OctaveCount = value;
            }
        }

        public int Seed
        {
            get { return XDistort.Seed; }
            set
            {
                XDistort.Seed = value;
                YDistort.Seed = value + 1;
                ZDistort.Seed = value + 2;
            }
        }

        public void SetSeed(int Seed)
        {
            this.Seed = Seed;
        }

        public void SetFrequency(double Frequency)
        {
            this.Frequency = Frequency;
        }

        public void SetRoughness(int Roughness)
        {
            this.Roughness = Roughness;
        }

        public void SetPower(double Power)
        {
            this.Power = Power;
        }
    }
}
