﻿using System;

namespace LibNoise
{
    public class FastNoise
        : FastNoiseBasis
    {
        public double Frequency { get; set; }
        public double Persistence { get; set; }
        public NoiseQuality NoiseQuality { get; set; }
        public int OctaveCount = 1;
        public double Lacunarity { get; set; }

        private const int MaxOctaves = 30;

        public FastNoise()
            : this(0)
        {

        }

        public FastNoise(int seed)
            : base(seed)
        {
            Frequency = 1.0;
            Lacunarity = 2.0;
            OctaveCount = 6;
            Persistence = 0.5;
            NoiseQuality = NoiseQuality.Standard;
        }

        public override double GetValue(double x, double y, double z)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;
            long seed;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                seed = (Seed + currentOctave) & 0xffffffff;
                signal = GradientCoherentNoise(x, y, z, (int)seed, NoiseQuality);
                value += signal * curPersistence;

                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            return value;
        }

        public override double GetValue(double x, double z)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;
            long seed;

            x *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                seed = (Seed + currentOctave) & 0xffffffff;
                signal = GradientCoherentNoise(x, z, (int)seed, NoiseQuality);
                value += signal * curPersistence;

                x *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            return value;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;
            long seed;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                seed = (Seed + currentOctave) & 0xffffffff;
                signal = GradientCoherentNoise(x, y, z, (int)seed, NoiseQuality);
                value += signal * curPersistence;

                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            return value;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;
            long seed;

            x *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                seed = (Seed + currentOctave) & 0xffffffff;
                signal = GradientCoherentNoise(x, z, (int)seed, NoiseQuality);
                value += signal * curPersistence;

                x *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            return value;
        }
    }
}
