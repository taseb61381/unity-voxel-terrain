﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LibNoise
{
    public class NoiseSelecterName : IModule
    {
        [NonSerialized]
        public IModule SourceModule;

        [NonSerialized]
        public SList<NoiseNameValues> NameNoises = new SList<NoiseNameValues>(1);

        public NoiseSelecterName()
        {
            
        }

        public NoiseSelecterName(IModule SourceModule, List<NoiseNameValues> Names)
        {
            Names = new List<NoiseNameValues>(Names.Count);
            for (int i = 0; i < Names.Count; ++i)
                this.NameNoises.Add(Names[i]);

            this.SourceModule = SourceModule;
        }

        public override void Init()
        {
            for (int i = 0; i < NameNoises.Count; ++i)
            {
                if (!string.IsNullOrEmpty(NameNoises.array[i].Name))
                    NameNoises.array[i]._NameHashCode = NameNoises.array[i].Name.GetHashCode();
                else
                    NameNoises.array[i]._NameHashCode = 0;
            }
        }

        public override double GetValue(double x, double y, double z)
        {
            return 0;
        }

        public override double GetValue(double x, double z)
        {
            return 0;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            if (NameNoises.Count > 0)
            {
                double v = SourceModule.GetValue(x, y, z, Modules);
                for (int i = 0; i < NameNoises.Count; ++i)
                {
                    if (v >= NameNoises.array[i].MinNoise && v < NameNoises.array[i].MaxNoise)
                    {
                        Modules.Noises.Add(NameNoises.array[i]._NameHashCode);
                    }
                }
            }

            return 0;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            if (NameNoises.Count > 0)
            {
                double v = SourceModule.GetValue(x, z, Modules);
                for (int i = 0; i < NameNoises.Count; ++i)
                {
                    if (v >= NameNoises.array[i].MinNoise && v < NameNoises.array[i].MaxNoise)
                    {
                        Modules.Noises.Add(NameNoises.array[i]._NameHashCode);
                    }
                }
            }

            return 0;
        }
    }
}
