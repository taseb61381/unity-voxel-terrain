﻿
namespace LibNoise
{
    public enum NoiseQuality
    {
        Low,
        Standard,
        High
    }

    public abstract class IModule : Math
    {

        public IModule()
        {

        }

        public virtual void Init()
        {

        }

        public abstract double GetValue(double x, double y, double z);

        public abstract double GetValue(double x, double z);

        public abstract double GetValue(double x, double y, double z, BiomeData Modules);

        public abstract double GetValue(double x, double z, BiomeData Modules);
    }
}
