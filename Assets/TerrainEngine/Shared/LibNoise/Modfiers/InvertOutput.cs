﻿
namespace LibNoise.Modifiers
{
    public class InvertOutput
        : IModule
    {
        public IModule SourceModule { get; set; }

        public InvertOutput()
        {

        }

        public InvertOutput(IModule sourceModule)
        {
            SourceModule = sourceModule;
        }



        public override double GetValue(double x, double y, double z)
        {
            return -SourceModule.GetValue(x, y, z);
        }

        public override double GetValue(double x, double z)
        {
            return -SourceModule.GetValue(x, z);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return -SourceModule.GetValue(x, y, z, Modules);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return -SourceModule.GetValue(x, z, Modules);
        }
    }
}
