﻿using System;

namespace LibNoise.Modifiers
{
    [Serializable]
    public struct CurveControlPoint
    {
        public CurveControlPoint(double Input, double Output)
        {
            this.Input = Input;
            this.Output = Output;
        }

        public double Input;
        public double Output;
    }

    public class CurveOutput
        : IModule
    {
        public IModule SourceModule { get; set; }
        public SList<CurveControlPoint> ControlPoints = new SList<CurveControlPoint>(4);

        public CurveOutput()
        {

        }

        public CurveOutput(IModule sourceModule)
        {
            if (sourceModule == null)
                throw new ArgumentNullException("A source module must be provided.");

            SourceModule = sourceModule;
        }

        public override double GetValue(double x, double y, double z)
        {
            if (ControlPoints.Count < 4)
                throw new Exception("Four or more control points must be specified.");

            // Get the output value from the source module.
            x = SourceModule.GetValue(x, y, z);

            int controlPointCount = ControlPoints.Count;

            // Find the first element in the control point array that has an input value
            // larger than the output value from the source module.
            int indexPos;
            for (indexPos = 0; indexPos < controlPointCount; ++indexPos)
            {
                if (x < ControlPoints.array[indexPos].Input)
                {
                    break;
                }
            }

            // Find the four nearest control points so that we can perform cubic
            // interpolation.
            int index1 = Math.ClampValue(indexPos - 1, 0, controlPointCount - 1);
            int index2 = Math.ClampValue(indexPos, 0, controlPointCount - 1);

            // If some control points are missing (which occurs if the value from the
            // source module is greater than the largest input value or less than the
            // smallest input value of the control point array), get the corresponding
            // output value of the nearest control point and exit now.
            if (index1 == index2)
            {
                return ControlPoints.array[index1].Output;
            }

            // Compute the alpha value used for cubic interpolation.
            y = ControlPoints.array[index1].Input;

            // Now perform the cubic interpolation given the alpha value.
            return CubicInterpolate(
              ControlPoints.array[Math.ClampValue(indexPos - 2, 0, controlPointCount - 1)].Output,
              ControlPoints.array[index1].Output,
              ControlPoints.array[index2].Output,
              ControlPoints.array[Math.ClampValue(indexPos + 1, 0, controlPointCount - 1)].Output,
              (x - y) / (ControlPoints.array[index2].Input - y));
        }

        public override double GetValue(double x, double z)
        {
            if (ControlPoints.Count < 4)
                throw new Exception("Four or more control points must be specified.");

            // Get the output value from the source module.
            x = SourceModule.GetValue(x, z);

            int controlPointCount = ControlPoints.Count;

            // Find the first element in the control point array that has an input value
            // larger than the output value from the source module.
            int indexPos;
            for (indexPos = 0; indexPos < controlPointCount; ++indexPos)
            {
                if (x < ControlPoints.array[indexPos].Input)
                {
                    break;
                }
            }

            // Find the four nearest control points so that we can perform cubic
            // interpolation.
            int index1 = Math.ClampValue(indexPos - 1, 0, controlPointCount - 1);
            int index2 = Math.ClampValue(indexPos, 0, controlPointCount - 1);

            // If some control points are missing (which occurs if the value from the
            // source module is greater than the largest input value or less than the
            // smallest input value of the control point array), get the corresponding
            // output value of the nearest control point and exit now.
            if (index1 == index2)
            {
                return ControlPoints.array[index1].Output;
            }

            // Compute the alpha value used for cubic interpolation.
            z = ControlPoints.array[index1].Input;

            // Now perform the cubic interpolation given the alpha value.
            return CubicInterpolate(
              ControlPoints.array[Math.ClampValue(indexPos - 2, 0, controlPointCount - 1)].Output,
              ControlPoints.array[index1].Output,
              ControlPoints.array[index2].Output,
              ControlPoints.array[Math.ClampValue(indexPos + 1, 0, controlPointCount - 1)].Output,
              (x - z) / (ControlPoints.array[index2].Input - z));
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            if (ControlPoints.Count < 4)
                throw new Exception("Four or more control points must be specified.");

            // Get the output value from the source module.
            x = SourceModule.GetValue(x, y, z);

            int controlPointCount = ControlPoints.Count;

            // Find the first element in the control point array that has an input value
            // larger than the output value from the source module.
            int indexPos;
            for (indexPos = 0; indexPos < controlPointCount; ++indexPos)
            {
                if (x < ControlPoints.array[indexPos].Input)
                {
                    break;
                }
            }

            // Find the four nearest control points so that we can perform cubic
            // interpolation.
            int index1 = Math.ClampValue(indexPos - 1, 0, controlPointCount - 1);
            int index2 = Math.ClampValue(indexPos, 0, controlPointCount - 1);

            // If some control points are missing (which occurs if the value from the
            // source module is greater than the largest input value or less than the
            // smallest input value of the control point array), get the corresponding
            // output value of the nearest control point and exit now.
            if (index1 == index2)
            {
                return ControlPoints.array[index1].Output;
            }

            // Compute the alpha value used for cubic interpolation.
            y = ControlPoints.array[index1].Input;

            // Now perform the cubic interpolation given the alpha value.
            return CubicInterpolate(
              ControlPoints.array[Math.ClampValue(indexPos - 2, 0, controlPointCount - 1)].Output,
              ControlPoints.array[index1].Output,
              ControlPoints.array[index2].Output,
              ControlPoints.array[Math.ClampValue(indexPos + 1, 0, controlPointCount - 1)].Output,
              (x - y) / (ControlPoints.array[index2].Input - y));
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            if (ControlPoints.Count < 4)
                throw new Exception("Four or more control points must be specified.");

            // Get the output value from the source module.
            x = SourceModule.GetValue(x, z);

            int controlPointCount = ControlPoints.Count;

            // Find the first element in the control point array that has an input value
            // larger than the output value from the source module.
            int indexPos;
            for (indexPos = 0; indexPos < controlPointCount; ++indexPos)
            {
                if (x < ControlPoints.array[indexPos].Input)
                {
                    break;
                }
            }

            // Find the four nearest control points so that we can perform cubic
            // interpolation.
            int index1 = Math.ClampValue(indexPos - 1, 0, controlPointCount - 1);
            int index2 = Math.ClampValue(indexPos, 0, controlPointCount - 1);

            // If some control points are missing (which occurs if the value from the
            // source module is greater than the largest input value or less than the
            // smallest input value of the control point array), get the corresponding
            // output value of the nearest control point and exit now.
            if (index1 == index2)
            {
                return ControlPoints.array[index1].Output;
            }

            // Compute the alpha value used for cubic interpolation.
            z = ControlPoints.array[index1].Input;

            // Now perform the cubic interpolation given the alpha value.
            return CubicInterpolate(
              ControlPoints.array[Math.ClampValue(indexPos - 2, 0, controlPointCount - 1)].Output,
              ControlPoints.array[index1].Output,
              ControlPoints.array[index2].Output,
              ControlPoints.array[Math.ClampValue(indexPos + 1, 0, controlPointCount - 1)].Output,
              (x - z) / (ControlPoints.array[index2].Input - z));
        }

        public void AddControlPoint(double Input, double Output)
        {
            ControlPoints.Add(new CurveControlPoint(Input, Output));
        }
    }
}
