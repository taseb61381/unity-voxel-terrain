﻿using System;

namespace LibNoise.Modifiers
{
    public class Select
        : IModule
    {
        public IModule ControlModule { get; set; }
        public IModule SourceModule1 { get; set; }
        public IModule SourceModule2 { get; set; }

        internal double mEdgeFalloff;
        public double UpperBound { get; set; }
        public double LowerBound { get; set; }

        public Select()
        {
            EdgeFalloff = 0.5;
            LowerBound = -1.0;
            UpperBound = 1.0;
        }

        public Select(IModule control, IModule source1, IModule source2)
        {
            ControlModule = control;
            SourceModule1 = source1;
            SourceModule2 = source2;

            EdgeFalloff = 0.0;
            LowerBound = -1.0;
            UpperBound = 1.0;
        }

        public override double GetValue(double x, double y, double z)
        {
            return GetValue(SourceModule1, SourceModule2,
                ControlModule.GetValue(x, y, z), x, y, z, LowerBound, UpperBound, mEdgeFalloff);
        }

        public override double GetValue(double x, double z)
        {
            return GetValue(SourceModule1, SourceModule2,
                ControlModule.GetValue(x, z), x, z, LowerBound, UpperBound, mEdgeFalloff);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return GetValue(SourceModule1,SourceModule2,
                ControlModule.GetValue(x, y, z), x, y, z, LowerBound,UpperBound,mEdgeFalloff, Modules);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return GetValue(SourceModule1, SourceModule2,
                ControlModule.GetValue(x, z), x, z, LowerBound, UpperBound, mEdgeFalloff, Modules);
        }

        public double GetValue(IModule SourceModule1, IModule SourceModule2,
            double controlValue, double x, double y, double z, double LowerBound, double UpperBound, double EdgeFalloff, BiomeData Modules,float Alpha1=0.5f,float Alpha2=0.5f)
        {
            double alpha;

            if (EdgeFalloff > 0.0)
            {
                if (controlValue < (LowerBound - EdgeFalloff))
                {
                    // The output value from the control module is below the selector
                    // threshold; return the output value from the first source module.
                    return SourceModule1.GetValue(x, y, z, Modules);
                }
                else if (controlValue < (LowerBound + EdgeFalloff))
                {
                    // The output value from the control module is near the lower end of the
                    // selector threshold and within the smooth curve. Interpolate between
                    // the output values from the first and second source modules.
                    double lowerCurve = (LowerBound - EdgeFalloff);
                    double upperCurve = (LowerBound + EdgeFalloff);
                    alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));

                    int First = Modules.Noises.Count;
                    lowerCurve = SourceModule1.GetValue(x, y, z, Modules);
                    int Start = Modules.Noises.Count;

                    upperCurve = SourceModule2.GetValue(x, y, z, Modules);

                    if (alpha < Alpha1)
                        Modules.SetIndex(Start);
                    else
                        Modules.RemoveIndex(First, Start - First);

                    return LinearInterpolate(lowerCurve, upperCurve, alpha);
                }
                else if (controlValue < (UpperBound - EdgeFalloff))
                {
                    // The output value from the control module is within the selector
                    // threshold; return the output value from the second source module.
                    return SourceModule2.GetValue(x, y, z, Modules);
                }
                else if (controlValue < (UpperBound + EdgeFalloff))
                {
                    // The output value from the control module is near the upper end of the
                    // selector threshold and within the smooth curve. Interpolate between
                    // the output values from the first and second source modules.
                    double lowerCurve = (UpperBound - EdgeFalloff);
                    double upperCurve = (UpperBound + EdgeFalloff);
                    alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));

                    int First = Modules.Noises.Count;
                    lowerCurve = SourceModule1.GetValue(x, y, z, Modules);
                    int Start = Modules.Noises.Count;

                    upperCurve = SourceModule2.GetValue(x, y, z, Modules);

                    if (alpha < Alpha2)
                        Modules.SetIndex(Start);
                    else
                        Modules.RemoveIndex(First, Start - First);

                    return LinearInterpolate(upperCurve, lowerCurve, alpha);
                }
                else
                {
                    // Output value from the control module is above the selector threshold;
                    // return the output value from the first source module.
                    return SourceModule1.GetValue(x, y, z, Modules);
                }
            }
            else
            {
                if (controlValue < LowerBound || controlValue > UpperBound)
                {
                    return SourceModule1.GetValue(x, y, z, Modules);
                }
                else
                {
                    return SourceModule2.GetValue(x, y, z, Modules);
                }
            }
        }

        public double GetValue(IModule SourceModule1, IModule SourceModule2,
    double controlValue, double x, double z, double LowerBound, double UpperBound, double EdgeFalloff, BiomeData Modules, float Alpha1 = 0.5f, float Alpha2 = 0.5f)
        {
            double alpha;

            if (EdgeFalloff > 0.0)
            {
                if (controlValue < (LowerBound - EdgeFalloff))
                {
                    // The output value from the control module is below the selector
                    // threshold; return the output value from the first source module.
                    return SourceModule1.GetValue(x, z, Modules);
                }
                else if (controlValue < (LowerBound + EdgeFalloff))
                {
                    // The output value from the control module is near the lower end of the
                    // selector threshold and within the smooth curve. Interpolate between
                    // the output values from the first and second source modules.
                    double lowerCurve = (LowerBound - EdgeFalloff);
                    double upperCurve = (LowerBound + EdgeFalloff);
                    alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));

                    int First = Modules.Noises.Count;
                    lowerCurve = SourceModule1.GetValue(x, z, Modules);
                    int Start = Modules.Noises.Count;

                    upperCurve = SourceModule2.GetValue(x, z, Modules);

                    if (alpha < Alpha1)
                        Modules.SetIndex(Start);
                    else
                        Modules.RemoveIndex(First, Start - First);

                    return LinearInterpolate(lowerCurve, upperCurve, alpha);
                }
                else if (controlValue < (UpperBound - EdgeFalloff))
                {
                    // The output value from the control module is within the selector
                    // threshold; return the output value from the second source module.
                    return SourceModule2.GetValue(x, z, Modules);
                }
                else if (controlValue < (UpperBound + EdgeFalloff))
                {
                    // The output value from the control module is near the upper end of the
                    // selector threshold and within the smooth curve. Interpolate between
                    // the output values from the first and second source modules.
                    double lowerCurve = (UpperBound - EdgeFalloff);
                    double upperCurve = (UpperBound + EdgeFalloff);
                    alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));

                    int First = Modules.Noises.Count;
                    lowerCurve = SourceModule1.GetValue(x, z, Modules);
                    int Start = Modules.Noises.Count;

                    upperCurve = SourceModule2.GetValue(x, z, Modules);

                    if (alpha > Alpha2)
                        Modules.SetIndex(Start);
                    else
                        Modules.RemoveIndex(First, Start - First);

                    return LinearInterpolate(upperCurve, lowerCurve, alpha);
                }
                else
                {
                    // Output value from the control module is above the selector threshold;
                    // return the output value from the first source module.
                    return SourceModule1.GetValue(x, z, Modules);
                }
            }
            else
            {
                if (controlValue < LowerBound || controlValue > UpperBound)
                {
                    return SourceModule1.GetValue(x, z, Modules);
                }
                else
                {
                    return SourceModule2.GetValue(x, z, Modules);
                }
            }
        }

        public double GetValue(IModule SourceModule1, IModule SourceModule2,
    double controlValue, double x, double y, double z, double LowerBound, double UpperBound, double EdgeFalloff)
        {
            double alpha;

            if (EdgeFalloff > 0.0)
            {
                if (controlValue < (LowerBound - EdgeFalloff))
                {
                    // The output value from the control module is below the selector
                    // threshold; return the output value from the first source module.
                    return SourceModule1.GetValue(x, y, z);
                }
                else if (controlValue < (LowerBound + EdgeFalloff))
                {
                    // The output value from the control module is near the lower end of the
                    // selector threshold and within the smooth curve. Interpolate between
                    // the output values from the first and second source modules.
                    double lowerCurve = (LowerBound - EdgeFalloff);
                    double upperCurve = (LowerBound + EdgeFalloff);
                    alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));

                    return LinearInterpolate(SourceModule1.GetValue(x, y, z), SourceModule2.GetValue(x, y, z), alpha);
                }
                else if (controlValue < (UpperBound - EdgeFalloff))
                {
                    // The output value from the control module is within the selector
                    // threshold; return the output value from the second source module.
                    return SourceModule2.GetValue(x, y, z);
                }
                else if (controlValue < (UpperBound + EdgeFalloff))
                {
                    // The output value from the control module is near the upper end of the
                    // selector threshold and within the smooth curve. Interpolate between
                    // the output values from the first and second source modules.
                    double lowerCurve = (UpperBound - EdgeFalloff);
                    double upperCurve = (UpperBound + EdgeFalloff);
                    alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));

                    return LinearInterpolate(SourceModule2.GetValue(x, y, z), SourceModule1.GetValue(x, y, z), alpha);
                }
                else
                {
                    // Output value from the control module is above the selector threshold;
                    // return the output value from the first source module.
                    return SourceModule1.GetValue(x, y, z);
                }
            }
            else
            {
                if (controlValue < LowerBound || controlValue > UpperBound)
                {
                    return SourceModule1.GetValue(x, y, z);
                }
                else
                {
                    return SourceModule2.GetValue(x, y, z);
                }
            }
        }

        public double GetValue(IModule SourceModule1, IModule SourceModule2,
    double controlValue, double x, double z, double LowerBound, double UpperBound, double EdgeFalloff)
        {
            double alpha;

            if (EdgeFalloff > 0.0)
            {
                if (controlValue < (LowerBound - EdgeFalloff))
                {
                    // The output value from the control module is below the selector
                    // threshold; return the output value from the first source module.
                    return SourceModule1.GetValue(x, z);
                }
                else if (controlValue < (LowerBound + EdgeFalloff))
                {
                    // The output value from the control module is near the lower end of the
                    // selector threshold and within the smooth curve. Interpolate between
                    // the output values from the first and second source modules.
                    double lowerCurve = (LowerBound - EdgeFalloff);
                    double upperCurve = (LowerBound + EdgeFalloff);
                    alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));

                    return LinearInterpolate(SourceModule1.GetValue(x, z), SourceModule2.GetValue(x, z), alpha);
                }
                else if (controlValue < (UpperBound - EdgeFalloff))
                {
                    // The output value from the control module is within the selector
                    // threshold; return the output value from the second source module.
                    return SourceModule2.GetValue(x, z);
                }
                else if (controlValue < (UpperBound + EdgeFalloff))
                {
                    // The output value from the control module is near the upper end of the
                    // selector threshold and within the smooth curve. Interpolate between
                    // the output values from the first and second source modules.
                    double lowerCurve = (UpperBound - EdgeFalloff);
                    double upperCurve = (UpperBound + EdgeFalloff);
                    alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));

                    return LinearInterpolate(SourceModule2.GetValue(x, z), SourceModule1.GetValue(x, z), alpha);
                }
                else
                {
                    // Output value from the control module is above the selector threshold;
                    // return the output value from the first source module.
                    return SourceModule1.GetValue(x, z);
                }
            }
            else
            {
                if (controlValue < LowerBound || controlValue > UpperBound)
                {
                    return SourceModule1.GetValue(x, z);
                }
                else
                {
                    return SourceModule2.GetValue(x, z);
                }
            }
        }


        public void SetBounds(double lower, double upper)
        {
            if (lower > upper)
                throw new ArgumentException("The lower bounds must be lower than the upper bounds.");

            LowerBound = lower;
            UpperBound = upper;

            // Make sure that the edge falloff curves do not overlap.
            EdgeFalloff = mEdgeFalloff;
        }

        public void SetEdgeFalloff(double EdgeFalloff)
        {
            this.EdgeFalloff = EdgeFalloff;
        }

        public double EdgeFalloff
        {
            get { return mEdgeFalloff; }
            set
            {
                double boundSize = UpperBound - LowerBound;
                mEdgeFalloff = (value > boundSize / 2) ? boundSize / 2 : value;
            }
        }
    }
}
