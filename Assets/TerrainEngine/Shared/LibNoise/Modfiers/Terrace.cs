﻿
namespace LibNoise.Modifiers
{
    public class Terrace
        : IModule
    {
        public IModule SourceModule { get; set; }
        public bool InvertTerraces { get; set; }
        public SList<double> ControlPoints;

        public Terrace()
        {
            InvertTerraces = false;
        }

        public Terrace(IModule sourceModule)
        {
            SourceModule = sourceModule;
            InvertTerraces = false;

            if (SourceModule == null)
                GeneralLog.LogError(ToString() + " Invalid SourceModule");

            if (ControlPoints.Count < 2)
            {
                GeneralLog.LogError("Invalid Terrace points : Count < 2");
            }
        }

        public override double GetValue(double x, double y, double z)
        {
            // Get the output value from the source module.
            double sourceModuleValue = SourceModule.GetValue(x, y, z);

            int controlPointCount = ControlPoints.Count;

            // Find the first element in the control point array that has a value
            // larger than the output value from the source module.
            int indexPos;
            for (indexPos = 0; indexPos < controlPointCount; ++indexPos)
            {
                if (sourceModuleValue < ControlPoints.array[indexPos])
                {
                    break;
                }
            }

            // Find the two nearest control points so that we can map their values
            // onto a quadratic curve.
            int index0 = Math.ClampValue(indexPos - 1, 0, controlPointCount - 1);
            int index1 = Math.ClampValue(indexPos, 0, controlPointCount - 1);

            // If some control points are missing (which occurs if the output value from
            // the source module is greater than the largest value or less than the
            // smallest value of the control point array), get the value of the nearest
            // control point and exit now.
            if (index0 == index1)
            {
                return ControlPoints.array[index1];
            }

            // Compute the alpha value used for linear interpolation.
            double value0 = ControlPoints.array[index0];
            double value1 = ControlPoints.array[index1];
            double alpha = (sourceModuleValue - value0) / (value1 - value0);

            // Squaring the alpha produces the terrace effect.
            alpha *= alpha;

            // Now perform the linear interpolation given the alpha value.
            //return LinearInterpolate(value0, value1, alpha);
            return ((1.0 - alpha) * value0) + (alpha * value1);
        }

        public override double GetValue(double x, double z)
        {
            // Get the output value from the source module.
            double sourceModuleValue = SourceModule.GetValue(x, z);

            int controlPointCount = ControlPoints.Count;

            // Find the first element in the control point array that has a value
            // larger than the output value from the source module.
            int indexPos;
            for (indexPos = 0; indexPos < controlPointCount; indexPos++)
            {
                if (sourceModuleValue < ControlPoints.array[indexPos])
                {
                    break;
                }
            }

            // Find the two nearest control points so that we can map their values
            // onto a quadratic curve.
            int index0 = Math.ClampValue(indexPos - 1, 0, controlPointCount - 1);
            int index1 = Math.ClampValue(indexPos, 0, controlPointCount - 1);

            // If some control points are missing (which occurs if the output value from
            // the source module is greater than the largest value or less than the
            // smallest value of the control point array), get the value of the nearest
            // control point and exit now.
            if (index0 == index1)
            {
                return ControlPoints.array[index1];
            }

            // Compute the alpha value used for linear interpolation.
            double value0 = ControlPoints.array[index0];
            double value1 = ControlPoints.array[index1];
            double alpha = (sourceModuleValue - value0) / (value1 - value0);

            // Squaring the alpha produces the terrace effect.
            alpha *= alpha;

            // Now perform the linear interpolation given the alpha value.
            //return LinearInterpolate(value0, value1, alpha);
            return ((1.0 - alpha) * value0) + (alpha * value1);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            // Get the output value from the source module.
            double sourceModuleValue = SourceModule.GetValue(x, y, z, Modules);

            int controlPointCount = ControlPoints.Count;

            // Find the first element in the control point array that has a value
            // larger than the output value from the source module.
            int indexPos;
            for (indexPos = 0; indexPos < controlPointCount; indexPos++)
            {
                if (sourceModuleValue < ControlPoints.array[indexPos])
                {
                    break;
                }
            }

            // Find the two nearest control points so that we can map their values
            // onto a quadratic curve.
            int index0 = Math.ClampValue(indexPos - 1, 0, controlPointCount - 1);
            int index1 = Math.ClampValue(indexPos, 0, controlPointCount - 1);

            // If some control points are missing (which occurs if the output value from
            // the source module is greater than the largest value or less than the
            // smallest value of the control point array), get the value of the nearest
            // control point and exit now.
            if (index0 == index1)
            {
                return ControlPoints.array[index1];
            }

            // Compute the alpha value used for linear interpolation.
            double value0 = ControlPoints.array[index0];
            double value1 = ControlPoints.array[index1];
            double alpha = (sourceModuleValue - value0) / (value1 - value0);

            // Squaring the alpha produces the terrace effect.
            alpha *= alpha;

            // Now perform the linear interpolation given the alpha value.
            //return LinearInterpolate(value0, value1, alpha);
            return ((1.0 - alpha) * value0) + (alpha * value1);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            // Get the output value from the source module.
            double sourceModuleValue = SourceModule.GetValue(x, z, Modules);

            int controlPointCount = ControlPoints.Count;

            // Find the first element in the control point array that has a value
            // larger than the output value from the source module.
            int indexPos;
            for (indexPos = 0; indexPos < controlPointCount; indexPos++)
            {
                if (sourceModuleValue < ControlPoints.array[indexPos])
                {
                    break;
                }
            }

            // Find the two nearest control points so that we can map their values
            // onto a quadratic curve.
            int index0 = Math.ClampValue(indexPos - 1, 0, controlPointCount - 1);
            int index1 = Math.ClampValue(indexPos, 0, controlPointCount - 1);

            // If some control points are missing (which occurs if the output value from
            // the source module is greater than the largest value or less than the
            // smallest value of the control point array), get the value of the nearest
            // control point and exit now.
            if (index0 == index1)
            {
                return ControlPoints.array[index1];
            }

            // Compute the alpha value used for linear interpolation.
            double value0 = ControlPoints.array[index0];
            double value1 = ControlPoints.array[index1];
            double alpha = (sourceModuleValue - value0) / (value1 - value0);

            // Squaring the alpha produces the terrace effect.
            alpha *= alpha;

            // Now perform the linear interpolation given the alpha value.
            //return LinearInterpolate(value0, value1, alpha);
            return ((1.0 - alpha) * value0) + (alpha * value1);
        }

        public void AddControlPoint(double Point)
        {
            if (ControlPoints.array == null)
                ControlPoints = new SList<double>(1);

            ControlPoints.Add(Point);
        }
    }
}
