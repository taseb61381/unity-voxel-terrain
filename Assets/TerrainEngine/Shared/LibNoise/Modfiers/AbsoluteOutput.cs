﻿using System;

namespace LibNoise.Modifiers
{
    /// <summary>
    /// Module that returns the absolute value of the output of a source module.    
    /// </summary>
    public class AbsoluteOutput
        : IModule
    {
        /// <summary>
        /// The module from which to retrieve noise.
        /// </summary>
        public IModule SourceModule { get; set; }

        public AbsoluteOutput()
        {

        }

        /// <summary>
        /// Initialises a new instance of the AbsoluteOutput class.
        /// </summary>
        /// <param name="sourceModule">The module from which to retrieve noise.</param>
        public AbsoluteOutput(IModule sourceModule)
        {
            if (sourceModule == null)
                throw new ArgumentNullException("A source module must be provided.");

            SourceModule = sourceModule;
        }

        /// <summary>
        /// Returns the absolute value of noise from the source module at the given coordinates.
        /// </summary>
        public override double GetValue(double x, double y, double z)
        {
            return System.Math.Abs(SourceModule.GetValue(x, y, z));
        }

        /// <summary>
        /// Returns the absolute value of noise from the source module at the given coordinates.
        /// </summary>
        public override double GetValue(double x, double z)
        {
            return System.Math.Abs(SourceModule.GetValue(x, z));
        }

        /// <summary>
        /// Returns the absolute value of noise from the source module at the given coordinates.
        /// </summary>
        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return System.Math.Abs(SourceModule.GetValue(x, y, z, Modules));
        }

        /// <summary>
        /// Returns the absolute value of noise from the source module at the given coordinates.
        /// </summary>
        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return System.Math.Abs(SourceModule.GetValue(x, z, Modules));
        }
    }
}
