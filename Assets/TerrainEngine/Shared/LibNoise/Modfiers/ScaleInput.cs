﻿using System;

namespace LibNoise.Modifiers
{
    public class ScaleInput
        : IModule
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public IModule SourceModule { get; set; }

        public ScaleInput()
        {

        }

        public ScaleInput(IModule sourceModule, double x, double y, double z)
        {
            if (sourceModule == null)
                throw new ArgumentNullException("A source module must be provided.");

            SourceModule = sourceModule;
            X = x;
            Y = y;
            Z = z;
        }

        public override double GetValue(double x, double y, double z)
        {
            return SourceModule.GetValue(x * X, y * Y, z * Z);
        }

        public override double GetValue(double x, double z)
        {
            return SourceModule.GetValue(x * X, z * Z);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return SourceModule.GetValue(x * X, y * Y, z * Z,Modules);
        }

        public override double GetValue(double x,  double z, BiomeData Modules)
        {
            return SourceModule.GetValue(x * X, z * Z, Modules);
        }
    }
}
