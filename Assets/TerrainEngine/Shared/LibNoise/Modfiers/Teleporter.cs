﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibNoise
{
    public class Teleporter : IModule
    {
        public string Title = "";
        public IModule Input;
        public int TeleporterId;
        public int DestinationId;

        public override double GetValue(double x, double y, double z)
        {
            return Input.GetValue(x, y, z);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return Input.GetValue(x, y, z,Modules);
        }

        public override double GetValue(double x, double z)
        {
            return Input.GetValue(x, z);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return Input.GetValue(x, z, Modules);
        }
    }
}
