﻿using System;

namespace LibNoise.Modifiers
{
    public class InvertInput
        : IModule
    {
        public IModule SourceModule { get; set; }

        public InvertInput()
        {

        }

        public InvertInput(IModule sourceModule)
        {
            if (sourceModule == null)
                throw new ArgumentNullException("A source module must be provided.");

            SourceModule = sourceModule;
        }


        public override double GetValue(double x, double y, double z)
        {
            return SourceModule.GetValue(-x, -y, -z);
        }

        public override double GetValue(double x, double z)
        {
            return SourceModule.GetValue(-x, -z);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return SourceModule.GetValue(-x, -y, -z,Modules);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return SourceModule.GetValue(-x, -z, Modules);
        }
    }
}
