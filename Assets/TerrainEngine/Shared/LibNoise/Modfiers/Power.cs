﻿using System;

namespace LibNoise.Modifiers
{
    public class Power
        : IModule
    {
        public IModule BaseModule { get; set; }
        public IModule PowerModule { get; set; }

        public Power()
        {

        }

        public Power(IModule baseModule, IModule powerModule)
        {
            if (baseModule == null || powerModule == null)
                throw new ArgumentNullException("Base and power modules must be provided.");

            BaseModule = baseModule;
            PowerModule = powerModule;
        }

        public override double GetValue(double x, double y, double z)
        {
            return System.Math.Pow(BaseModule.GetValue(x, y, z), PowerModule.GetValue(x, y, z));
        }

        public override double GetValue(double x, double z)
        {
            return System.Math.Pow(BaseModule.GetValue(x, z), PowerModule.GetValue(x, z));
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return System.Math.Pow(BaseModule.GetValue(x, y, z, Modules), PowerModule.GetValue(x, y, z, Modules));
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return System.Math.Pow(BaseModule.GetValue(x, z, Modules), PowerModule.GetValue(x, z, Modules));
        }
    }
}
