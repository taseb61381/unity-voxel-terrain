﻿using System;

namespace LibNoise.Modifiers
{
    public class DisplaceInput
        : IModule
    {
        public IModule SourceModule { get; set; }
        public IModule XDisplaceModule { get; set; }
        public IModule YDisplaceModule { get; set; }
        public IModule ZDisplaceModule { get; set; }

        public DisplaceInput()
        {

        }

        public DisplaceInput(IModule sourceModule, IModule xDisplaceModule, IModule yDisplaceModule, IModule zDisplaceModule)
        {
            if (sourceModule == null || xDisplaceModule == null || yDisplaceModule == null || zDisplaceModule == null)
                throw new ArgumentNullException("Source and X, Y, and Z displacement modules must be provided.");

            SourceModule = sourceModule;
            XDisplaceModule = xDisplaceModule;
            YDisplaceModule = yDisplaceModule;
            ZDisplaceModule = zDisplaceModule;
        }

        public override double GetValue(double x, double y, double z)
        {
            x += XDisplaceModule.GetValue(x, y, z);
            y += YDisplaceModule.GetValue(x, y, z);
            z += ZDisplaceModule.GetValue(x, y, z);

            return SourceModule.GetValue(x, y, z);
        }

        public override double GetValue(double x, double z)
        {
            x += XDisplaceModule.GetValue(x, z);
            z += ZDisplaceModule.GetValue(x, z);

            return SourceModule.GetValue(x, z);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            x += XDisplaceModule.GetValue(x, y, z, Modules);
            y += YDisplaceModule.GetValue(x, y, z, Modules);
            z += ZDisplaceModule.GetValue(x, y, z, Modules);

            return SourceModule.GetValue(x, y, z, Modules);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            x += XDisplaceModule.GetValue(x, z, Modules);
            z += ZDisplaceModule.GetValue(x, z, Modules);

            return SourceModule.GetValue(x, z, Modules);
        }
    }
}
