﻿
namespace LibNoise.Modifiers
{
    public class BiasScaleOutput
        : IModule
    {
        public double Scale { get; set; }
        public double Bias { get; set; }

        public IModule SourceModule { get; set; }

        public BiasScaleOutput()
        {
            Bias = 0.0;
            Scale = 1.0;
        }

        public BiasScaleOutput(IModule sourceModule)
        {
            SourceModule = sourceModule;

            Bias = 0.0;
            Scale = 1.0;
        }

        public override double GetValue(double x, double y, double z)
        {
            return (SourceModule.GetValue(x, y, z) + Bias) * Scale;
        }

        public override double GetValue(double x, double z)
        {
            return (SourceModule.GetValue(x, z) + Bias) * Scale;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return (SourceModule.GetValue(x, y, z, Modules) + Bias) * Scale;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return (SourceModule.GetValue(x, z, Modules) + Bias) * Scale;
        }
    }
}
