﻿using System.Collections.Generic;

namespace LibNoise.Modifiers
{
    /// <summary>
    /// Module that returns a value from a selected noise depending on a ControlModule.
    /// Falloff and Bounds are automaticly calculated
    /// -1 to 0.25 : Forest
    /// -0.25 to 1 : Desert
    /// Between -0.25 to 0.25, it will be a smooth transition
    /// </summary>
    public class NoiseSelecter
        : IModule
    {
        /// <summary>
        /// The module from which to retrieve noise.
        /// </summary>
        public IModule SourceModule { get; set; }
        public SList<NoiseNameValues> Noises;
        private Select TempSelect;

        public NoiseSelecter()
        {

        }

        public override void Init()
        {
            this.TempSelect = new Select(null, null, null);

            for (int i = 0; i < Noises.Count; ++i)
            {
                if (!string.IsNullOrEmpty(Noises.array[i].Name))
                    Noises.array[i]._NameHashCode = Noises.array[i].Name.GetHashCode();
                else
                    Noises.array[i]._NameHashCode = 0;
            }
        }

        /// <summary>
        /// Initialises a new instance of the AbsoluteOutput class.
        /// </summary>
        /// <param name="sourceModule">The module from which to retrieve noise.</param>
        public NoiseSelecter(IModule sourceModule, List<NoiseNameValues> Names)
        {
            Noises = new SList<NoiseNameValues>(Names.Count);

            for (int i = 0; i < Names.Count; ++i)
            {
                Noises.Add(Names[i]);
            }

            SourceModule = sourceModule;
            this.TempSelect = new Select(null, null, null);
        }

        /// <summary>
        /// Returns the absolute value of noise from the source module at the given coordinates.
        /// </summary>
        public override double GetValue(double x, double y, double z)
        {
            if (Noises.Count > 0)
            {
                int ModA = -1, ModB = -1;

                double v = SourceModule.GetValue(x, y, z);
                for (int i = 0; i < Noises.Count; ++i)
                {
                    if (ModA == -1)
                    {
                        if (v >= Noises.array[i].MinNoise && v < Noises.array[i].MaxNoise)
                            ModA = i;
                        else
                            continue;
                    }
                    else if (ModB == -1)
                    {
                        if (v >= Noises.array[i].MinNoise && v < Noises.array[i].MaxNoise)
                        {
                            ModB = i;
                            break;
                        }
                        else
                            continue;
                    }
                }

                if (ModA != -1)
                {
                    if (ModB == -1)
                    {
                        return Noises.array[ModA].NoiseModule.GetValue(x, y, z);
                    }
                    else
                    {
                        double Edge = (Noises.array[ModA].MaxNoise - Noises.array[ModB].MinNoise) * 0.5;
                        return TempSelect.GetValue(Noises.array[ModA].NoiseModule, Noises.array[ModB].NoiseModule, v, x, y, z, Noises.array[ModA].MaxNoise - Edge, Noises.array[ModB].MaxNoise + Edge, Edge);
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// Returns the absolute value of noise from the source module at the given coordinates.
        /// </summary>
        public override double GetValue(double x, double z)
        {
            if (Noises.Count > 0)
            {
                int ModA = -1, ModB = -1;

                double v = SourceModule.GetValue(x, z);
                for (int i = 0; i < Noises.Count; ++i)
                {
                    if (ModA == -1)
                    {
                        if (v >= Noises.array[i].MinNoise && v < Noises.array[i].MaxNoise)
                            ModA = i;
                        else
                            continue;
                    }
                    else if (ModB == -1)
                    {
                        if (v >= Noises.array[i].MinNoise && v < Noises.array[i].MaxNoise)
                        {
                            ModB = i;
                            break;
                        }
                        else
                            continue;
                    }
                }


                if (ModA != -1)
                {
                    if (ModB == -1)
                    {
                        return Noises.array[ModA].NoiseModule.GetValue(x, z);
                    }
                    else
                    {
                        double Edge = (Noises.array[ModA].MaxNoise - Noises.array[ModB].MinNoise) * 0.5;
                        return TempSelect.GetValue(Noises.array[ModA].NoiseModule, Noises.array[ModB].NoiseModule, v, x, z, Noises.array[ModA].MaxNoise - Edge, Noises.array[ModB].MaxNoise + Edge, Edge);
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// Returns the absolute value of noise from the source module at the given coordinates.
        /// </summary>
        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            if (Noises.Count > 0)
            {
                int ModA = -1, ModB = -1;

                double v = SourceModule.GetValue(x, y, z);
                for (int i = 0; i < Noises.Count; ++i)
                {
                    if (ModA == -1)
                    {
                        if (v >= Noises.array[i].MinNoise && v < Noises.array[i].MaxNoise)
                            ModA = i;
                        else
                            continue;
                    }
                    else if (ModB == -1)
                    {
                        if (v >= Noises.array[i].MinNoise && v < Noises.array[i].MaxNoise)
                        {
                            ModB = i;
                            break;
                        }
                        else
                            continue;
                    }
                }


                if (ModA != -1)
                {
                    if (ModB == -1)
                    {
                        return Noises.array[ModA].NoiseModule.GetValue(x, y, z, Modules);
                    }
                    else
                    {
                        double Edge = (Noises.array[ModA].MaxNoise - Noises.array[ModB].MinNoise) * 0.5;
                        return TempSelect.GetValue(Noises.array[ModA].NoiseModule, Noises.array[ModB].NoiseModule, v, x, y, z, Noises.array[ModA].MaxNoise - Edge, Noises.array[ModB].MaxNoise + Edge, Edge, Modules, Noises.array[ModA].Alpha, Noises.array[ModB].Alpha);
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// Returns the absolute value of noise from the source module at the given coordinates.
        /// </summary>
        public override double GetValue(double x, double z, BiomeData Modules)
        {
            if (Noises.Count > 0)
            {
                int ModA = -1, ModB = -1;

                double v = SourceModule.GetValue(x, z);
                for (int i = 0; i < Noises.Count; ++i)
                {
                    if (ModA == -1)
                    {
                        if (v >= Noises.array[i].MinNoise && v < Noises.array[i].MaxNoise)
                            ModA = i;
                        else
                            continue;
                    }
                    else if (ModB == -1)
                    {
                        if (v >= Noises.array[i].MinNoise && v < Noises.array[i].MaxNoise)
                        {
                            ModB = i;
                            break;
                        }
                        else
                            continue;
                    }
                }

                if (ModA != -1)
                {
                    if (ModB == -1)
                    {
                        return Noises.array[ModA].NoiseModule.GetValue(x, z, Modules);
                    }
                    else
                    {
                        double Edge = (Noises.array[ModA].MaxNoise - Noises.array[ModB].MinNoise) * 0.5;
                        return TempSelect.GetValue(Noises.array[ModA].NoiseModule, Noises.array[ModB].NoiseModule, v, x, z, Noises.array[ModA].MaxNoise - Edge, Noises.array[ModB].MaxNoise + Edge, Edge, Modules, Noises.array[ModA].Alpha, Noises.array[ModB].Alpha);
                    }
                }
            }

            return 0;
        }
    }
}
