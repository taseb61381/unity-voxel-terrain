﻿using System;

namespace LibNoise.Modifiers
{
    public class ClampOutput
        : IModule
    {
        public double LowerBound { get; private set; }
        public double UpperBound { get; private set; }

        public IModule SourceModule { get; set; }

        public ClampOutput()
        {
            LowerBound = -1;
            UpperBound = 1;
        }

        public ClampOutput(IModule sourceModule)
        {
            if (sourceModule == null)
                throw new ArgumentNullException("A source module must be provided.");

            SourceModule = sourceModule;

            LowerBound = -1;
            UpperBound = 1;
        }

        public override double GetValue(double x, double y, double z)
        {
            x = SourceModule.GetValue(x, y, z);
            if (x < LowerBound)
            {
                return LowerBound;
            }
            else if (x > UpperBound)
            {
                return UpperBound;
            }
            else
            {
                return x;
            }
        }

        public override double GetValue(double x, double z)
        {
            x = SourceModule.GetValue(x, z);
            if (x < LowerBound)
            {
                return LowerBound;
            }
            else if (x > UpperBound)
            {
                return UpperBound;
            }
            else
            {
                return x;
            }
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            x = SourceModule.GetValue(x, y, z, Modules);
            if (x < LowerBound)
            {
                return LowerBound;
            }
            else if (x > UpperBound)
            {
                return UpperBound;
            }
            else
            {
                return x;
            }
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            x = SourceModule.GetValue(x, z, Modules);
            if (x < LowerBound)
            {
                return LowerBound;
            }
            else if (x > UpperBound)
            {
                return UpperBound;
            }
            else
            {
                return x;
            }
        }

        public void SetBounds(double lowerBound, double upperBound)
        {
            if (LowerBound >= upperBound)
                throw new Exception("Lower bound must be lower than upper bound.");

            LowerBound = lowerBound;
            UpperBound = upperBound;
        }
    }
}
