﻿using System;

namespace LibNoise.Modifiers
{
    public class ExponentialOutput
        : IModule
    {
        public IModule SourceModule { get; set; }

        public double Exponent { get; set; }

        public ExponentialOutput()
        {

        }

        public ExponentialOutput(IModule sourceModule, double exponent)
        {
            if (sourceModule == null)
                throw new ArgumentNullException("A source module must be provided.");

            SourceModule = sourceModule;
            Exponent = exponent;
        }

        public override double GetValue(double x, double y, double z)
        {
            return (System.Math.Pow(System.Math.Abs((SourceModule.GetValue(x, y, z) + 1.0) / 2.0), Exponent) * 2.0 - 1.0);
        }

        public override double GetValue(double x, double z)
        {
            return (System.Math.Pow(System.Math.Abs((SourceModule.GetValue(x, z) + 1.0) / 2.0), Exponent) * 2.0 - 1.0);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return (System.Math.Pow(System.Math.Abs((SourceModule.GetValue(x, y, z, Modules) + 1.0) / 2.0), Exponent) * 2.0 - 1.0);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return (System.Math.Pow(System.Math.Abs((SourceModule.GetValue(x, z, Modules) + 1.0) / 2.0), Exponent) * 2.0 - 1.0);
        }
    }
}
