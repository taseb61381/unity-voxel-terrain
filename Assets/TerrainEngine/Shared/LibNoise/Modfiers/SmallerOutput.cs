﻿using System;

namespace LibNoise.Modifiers
{
    public class SmallerOutput
        : IModule
    {
        public IModule SourceModule1 { get; set; }
        public IModule SourceModule2 { get; set; }

        public SmallerOutput()
        {

        }

        public SmallerOutput(IModule sourceModule1, IModule sourceModule2)
        {
            if (sourceModule1 == null || sourceModule2 == null)
                throw new ArgumentNullException("Source modules must be provided.");

            SourceModule1 = sourceModule1;
            SourceModule2 = sourceModule2;
        }

        public override double GetValue(double x, double y, double z)
        {
            double a = SourceModule1.GetValue(x, y, z);
            x = SourceModule2.GetValue(x, y, z);

            if (a < x)
            {
                return a;
            }
            else
            {
                return x;
            }
        }

        public override double GetValue(double x, double z)
        {
            double a = SourceModule1.GetValue(x, z);
            x = SourceModule2.GetValue(x, z);

            if (a < x)
            {
                return a;
            }
            else
            {
                return x;
            }
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            double a = SourceModule1.GetValue(x, y, z);
            x = SourceModule2.GetValue(x, y, z);

            if (a < x)
            {
                return a;
            }
            else
            {
                return x;
            }
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            double a = SourceModule1.GetValue(x, z);
            x = SourceModule2.GetValue(x, z);

            if (a < x)
            {
                return a;
            }
            else
            {
                return x;
            }
        }
    }
}
