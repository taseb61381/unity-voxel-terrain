﻿
namespace LibNoise.Modifiers
{
    public class ZeroToOneOutput
        : IModule
    {
        public IModule SourceModule { get; set; }

        public ZeroToOneOutput()
        {

        }

        public ZeroToOneOutput(IModule sourceModule)
        {
            SourceModule = sourceModule;
        }

        public override double GetValue(double x, double y, double z)
        {
            return (SourceModule.GetValue(x, y, z) + 1.0) * 0.5;
        }

        public override double GetValue(double x, double z)
        {
            return (SourceModule.GetValue(x, z) + 1.0) * 0.5;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return (SourceModule.GetValue(x, y, z, Modules) + 1.0) * 0.5;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return (SourceModule.GetValue(x, z, Modules) + 1.0) * 0.5;
        }
    }
}
