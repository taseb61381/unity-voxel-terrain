﻿
namespace LibNoise.Modifiers
{
    public class Multiply
        : IModule
    {
        public IModule SourceModule1 { get; set; }
        public IModule SourceModule2 { get; set; }

        public Multiply()
        {


        }

        public Multiply(IModule sourceModule1, IModule sourceModule2)
        {
            if (sourceModule1 == null || sourceModule2 == null)
            {
                UnityEngine.Debug.LogError("Source modules must be provided.");
                return;
            }

            SourceModule1 = sourceModule1;
            SourceModule2 = sourceModule2;
        }

        public override double GetValue(double x, double y, double z)
        {
            return SourceModule1.GetValue(x, y, z) * SourceModule2.GetValue(x, y, z);
        }

        public override double GetValue(double x, double z)
        {
            return SourceModule1.GetValue(x, z) * SourceModule2.GetValue(x, z);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return SourceModule1.GetValue(x, y, z, Modules) * SourceModule2.GetValue(x, y, z, Modules);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return SourceModule1.GetValue(x, z, Modules) * SourceModule2.GetValue(x, z, Modules);
        }
    }
}
