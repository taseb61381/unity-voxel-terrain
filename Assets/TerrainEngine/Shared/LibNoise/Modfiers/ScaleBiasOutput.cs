﻿using System;

namespace LibNoise.Modifiers
{
    public class ScaleBiasOutput
        : IModule
    {
        public double Scale { get; set; }
        public double Bias { get; set; }

        public IModule SourceModule { get; set; }

        public ScaleBiasOutput()
        {
            Bias = 0.0;
            Scale = 1.0;
        }

        public ScaleBiasOutput(IModule sourceModule)
        {
            if (sourceModule == null)
                throw new ArgumentNullException("A source module must be provided.");

            SourceModule = sourceModule;

            Bias = 0.0;
            Scale = 1.0;
        }

        public override double GetValue(double x, double y, double z)
        {
            return SourceModule.GetValue(x, y, z) * Scale + Bias;
        }

        public override double GetValue(double x, double z)
        {
            return SourceModule.GetValue(x, z) * Scale + Bias;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return SourceModule.GetValue(x, y, z, Modules) * Scale + Bias;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return SourceModule.GetValue(x, z, Modules) * Scale + Bias;
        }

        public void SetScale(double Scale)
        {
            this.Scale = Scale;
        }

        public void SetBias(double Bias)
        {
            this.Bias = Bias;
        }
    }
}
