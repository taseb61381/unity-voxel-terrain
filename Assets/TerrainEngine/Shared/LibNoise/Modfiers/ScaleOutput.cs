﻿using System;

namespace LibNoise.Modifiers
{
    public class ScaleOutput
        : IModule
    {
        public IModule SourceModule { get; set; }

        public double Scale { get; set; }

        public ScaleOutput()
        {

        }

        public ScaleOutput(IModule sourceModule, double scale)
        {
            if (sourceModule == null)
                throw new ArgumentNullException("A source module must be provided.");

            SourceModule = sourceModule;
            Scale = scale;
        }

        public override double GetValue(double x, double y, double z)
        {
            return SourceModule.GetValue(x, y, z) * Scale;
        }

        public override double GetValue(double x, double z)
        {
            return SourceModule.GetValue(x, z) * Scale;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return SourceModule.GetValue(x, y, z, Modules) * Scale;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return SourceModule.GetValue(x,  z, Modules) * Scale;
        }
    }
}
