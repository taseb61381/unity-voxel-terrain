﻿
namespace LibNoise
{
    public class Perlin
        : GradientNoiseBasis
    {
        public double Frequency { get; set; }
        public double Persistence { get; set; }
        public NoiseQuality NoiseQuality { get; set; }
        public int Seed { get; set; }
        public int OctaveCount = 1;
        public double Lacunarity { get; set; }
        private const int MaxOctaves = 30;

        public void SetSeed(int Seed)
        {
            this.Seed = Seed;
        }

        public void SetFrequency(double Frequency)
        {
            this.Frequency = Frequency;
        }

        public void SetPersistence(double Persistence)
        {
            this.Persistence = Persistence;
        }

        public void SetLacunarity(double Lacunarity)
        {
            this.Lacunarity = Lacunarity;
        }

        public void SetOctaveCount(int OctaveCount)
        {
            this.OctaveCount = OctaveCount;
        }

        public Perlin()
        {
            Frequency = 1.0;
            Lacunarity = 2.0;
            OctaveCount = 6;
            Persistence = 0.5;
            NoiseQuality = NoiseQuality.Standard;
            Seed = 0;
        }


        public override double GetValue(double x, double y, double z)
        {
            double value = 0.0;
            double curPersistence = 1.0;
            double nx, ny, nz;
            int seed = 0;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            int x0 = 0;
            int x1 = 0;
            int y0 = 0;
            int y1 = 0;
            int z0 = 0;
            int z1 = 0;
            double xs = 0, ys = 0, zs = 0;
            double n0, n1, ix0, ix1, iy0, iy1;
            long vectorIndex;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                if (x >= 1073741824.0) { nx = (2.0 * System.Math.IEEERemainder(x, 1073741824.0)) - 1073741824.0; }
                else if (x <= -1073741824.0) { nx = (2.0 * System.Math.IEEERemainder(x, 1073741824.0)) + 1073741824.0; }
                else { nx = x; }

                if (y >= 1073741824.0) { ny = (2.0 * System.Math.IEEERemainder(y, 1073741824.0)) - 1073741824.0; }
                else if (y <= -1073741824.0) { ny = (2.0 * System.Math.IEEERemainder(y, 1073741824.0)) + 1073741824.0; }
                else { ny = y; }

                if (z >= 1073741824.0) { nz = (2.0 * System.Math.IEEERemainder(z, 1073741824.0)) - 1073741824.0; }
                else if (z <= -1073741824.0) { nz = (2.0 * System.Math.IEEERemainder(z, 1073741824.0)) + 1073741824.0; }
                else { nz = z; }

                seed = (int)((Seed + currentOctave) & 0xffffffff);

                x0 = (nx > 0.0 ? (int)nx : (int)nx - 1);
                x1 = x0 + 1;
                y0 = (ny > 0.0 ? (int)ny : (int)ny - 1);
                y1 = y0 + 1;
                z0 = (nz > 0.0 ? (int)nz : (int)nz - 1);
                z1 = z0 + 1;

                switch (NoiseQuality)
                {
                    case NoiseQuality.Low:
                        xs = (nx - x0);
                        ys = (ny - y0);
                        zs = (nz - z0);
                        break;
                    case NoiseQuality.Standard:
                        xs = SCurve3(nx - x0);
                        ys = SCurve3(ny - y0);
                        zs = SCurve3(nz - z0);
                        break;
                    case NoiseQuality.High:
                        xs = SCurve5(nx - x0);
                        ys = SCurve5(ny - y0);
                        zs = SCurve5(nz - z0);
                        break;
                }

                //n0 = GradientNoise(x, y, z, x0, y0, z0, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y0, z0, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //ix0 = LinearInterpolate(n0, n1, xs);
                ix0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, y, z, x0, y1, z0, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //n1 = GradientNoise(x, y, z, x1, y1, z0, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //iy0 = LinearInterpolate(ix0, ix1, ys);
                iy0 = ((1.0 - ys) * ix0) + (ys * ix1);

                //n0 = GradientNoise(x, y, z, x0, y0, z1, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y0, z1, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //ix0 = LinearInterpolate(n0, n1, xs);
                ix0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, y, z, x0, y1, z1, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y1, z1, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //iy1 = LinearInterpolate(ix0, ix1, ys);
                iy1 = ((1.0 - ys) * ix0) + (ys * ix1);

                //return LinearInterpolate(iy0, iy1, zs);
                value += (((1.0 - zs) * iy0) + (zs * iy1)) * curPersistence;

                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            return value;
        }

        public override double GetValue(double x, double z)
        {
            double value = 0.0;
            double curPersistence = 1.0;
            double nx, nz;
            int seed = 0;

            x *= Frequency;
            z *= Frequency;

            int x0 = 0;
            int x1 = 0;
            int z0 = 0;
            int z1 = 0;

            double xs = 0, zs = 0;
            double ix1, iy0, n0, n1;
            long vectorIndex;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                if (x >= 1073741824.0) { nx = (2.0 * System.Math.IEEERemainder(x, 1073741824.0)) - 1073741824.0; }
                else if (x <= -1073741824.0) { nx = (2.0 * System.Math.IEEERemainder(x, 1073741824.0)) + 1073741824.0; }
                else { nx = x; }

                if (z >= 1073741824.0) { nz = (2.0 * System.Math.IEEERemainder(z, 1073741824.0)) - 1073741824.0; }
                else if (z <= -1073741824.0) { nz = (2.0 * System.Math.IEEERemainder(z, 1073741824.0)) + 1073741824.0; }
                else { nz = z; }

                seed = (int)((Seed + currentOctave) & 0xffffffff);

                x0 = (nx > 0.0 ? (int)nx : (int)nx - 1);
                x1 = x0 + 1;
                z0 = (nz > 0.0 ? (int)nz : (int)nz - 1);
                z1 = z0 + 1;

                switch (NoiseQuality)
                {
                    case NoiseQuality.Low:
                        xs = (nx - x0);
                        zs = (nz - z0);
                        break;
                    case NoiseQuality.Standard:
                        xs = SCurve3(nx - x0);
                        zs = SCurve3(nz - z0);
                        break;
                    case NoiseQuality.High:
                        xs = SCurve5(nx - x0);
                        zs = SCurve5(nz - z0);
                        break;
                }

                //n0 = GradientNoise(x, z, x0, z0, seed);
                vectorIndex = (XNoiseGen * x0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //n1 = GradientNoise(x, z, x1, z0, seed);
                vectorIndex = (XNoiseGen * x1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //iy0 = LinearInterpolate(n0, n1, xs);
                iy0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, z, x0, z1, seed);
                vectorIndex = (XNoiseGen * x0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;

                //n1 = GradientNoise(x, z, x1, z1, seed);
                vectorIndex = (XNoiseGen * x1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;

                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //return LinearInterpolate(iy0, ix1, zs);
                value += (((1.0 - zs) * iy0) + (zs * ix1)) * curPersistence;

                x *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            return value;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            double value = 0.0;
            double curPersistence = 1.0;
            double nx, ny, nz;
            int seed = 0;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            int x0 = 0;
            int x1 = 0;
            int y0 = 0;
            int y1 = 0;
            int z0 = 0;
            int z1 = 0;
            double xs = 0, ys = 0, zs = 0;
            double n0, n1, ix0, ix1, iy0, iy1;
            long vectorIndex;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                if (x >= 1073741824.0) { nx = (2.0 * System.Math.IEEERemainder(x, 1073741824.0)) - 1073741824.0; }
                else if (x <= -1073741824.0) { nx = (2.0 * System.Math.IEEERemainder(x, 1073741824.0)) + 1073741824.0; }
                else { nx = x; }

                if (y >= 1073741824.0) { ny = (2.0 * System.Math.IEEERemainder(y, 1073741824.0)) - 1073741824.0; }
                else if (y <= -1073741824.0) { ny = (2.0 * System.Math.IEEERemainder(y, 1073741824.0)) + 1073741824.0; }
                else { ny = y; }

                if (z >= 1073741824.0) { nz = (2.0 * System.Math.IEEERemainder(z, 1073741824.0)) - 1073741824.0; }
                else if (z <= -1073741824.0) { nz = (2.0 * System.Math.IEEERemainder(z, 1073741824.0)) + 1073741824.0; }
                else { nz = z; }

                seed = (int)((Seed + currentOctave) & 0xffffffff);

                x0 = (nx > 0.0 ? (int)nx : (int)nx - 1);
                x1 = x0 + 1;
                y0 = (ny > 0.0 ? (int)ny : (int)ny - 1);
                y1 = y0 + 1;
                z0 = (nz > 0.0 ? (int)nz : (int)nz - 1);
                z1 = z0 + 1;

                switch (NoiseQuality)
                {
                    case NoiseQuality.Low:
                        xs = (nx - x0);
                        ys = (ny - y0);
                        zs = (nz - z0);
                        break;
                    case NoiseQuality.Standard:
                        xs = SCurve3(nx - x0);
                        ys = SCurve3(ny - y0);
                        zs = SCurve3(nz - z0);
                        break;
                    case NoiseQuality.High:
                        xs = SCurve5(nx - x0);
                        ys = SCurve5(ny - y0);
                        zs = SCurve5(nz - z0);
                        break;
                }

                //n0 = GradientNoise(x, y, z, x0, y0, z0, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y0, z0, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //ix0 = LinearInterpolate(n0, n1, xs);
                ix0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, y, z, x0, y1, z0, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //n1 = GradientNoise(x, y, z, x1, y1, z0, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //iy0 = LinearInterpolate(ix0, ix1, ys);
                iy0 = ((1.0 - ys) * ix0) + (ys * ix1);

                //n0 = GradientNoise(x, y, z, x0, y0, z1, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y0, z1, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //ix0 = LinearInterpolate(n0, n1, xs);
                ix0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, y, z, x0, y1, z1, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y1, z1, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //iy1 = LinearInterpolate(ix0, ix1, ys);
                iy1 = ((1.0 - ys) * ix0) + (ys * ix1);

                //return LinearInterpolate(iy0, iy1, zs);
                value += (((1.0 - zs) * iy0) + (zs * iy1)) * curPersistence;

                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }
            return value;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            double value = 0.0;
            double curPersistence = 1.0;
            double nx, nz;
            int seed = 0;

            x *= Frequency;
            z *= Frequency;

            int x0 = 0;
            int x1 = 0;
            int z0 = 0;
            int z1 = 0;

            double xs = 0, zs = 0;
            double ix1, iy0, n0, n1;
            long vectorIndex;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                if (x >= 1073741824.0) { nx = (2.0 * System.Math.IEEERemainder(x, 1073741824.0)) - 1073741824.0; }
                else if (x <= -1073741824.0) { nx = (2.0 * System.Math.IEEERemainder(x, 1073741824.0)) + 1073741824.0; }
                else { nx = x; }

                if (z >= 1073741824.0) { nz = (2.0 * System.Math.IEEERemainder(z, 1073741824.0)) - 1073741824.0; }
                else if (z <= -1073741824.0) { nz = (2.0 * System.Math.IEEERemainder(z, 1073741824.0)) + 1073741824.0; }
                else { nz = z; }

                seed = (int)((Seed + currentOctave) & 0xffffffff);

                x0 = (nx > 0.0 ? (int)nx : (int)nx - 1);
                x1 = x0 + 1;
                z0 = (nz > 0.0 ? (int)nz : (int)nz - 1);
                z1 = z0 + 1;

                switch (NoiseQuality)
                {
                    case NoiseQuality.Low:
                        xs = (nx - x0);
                        zs = (nz - z0);
                        break;
                    case NoiseQuality.Standard:
                        xs = SCurve3(nx - x0);
                        zs = SCurve3(nz - z0);
                        break;
                    case NoiseQuality.High:
                        xs = SCurve5(nx - x0);
                        zs = SCurve5(nz - z0);
                        break;
                }

                //n0 = GradientNoise(x, z, x0, z0, seed);
                vectorIndex = (XNoiseGen * x0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //n1 = GradientNoise(x, z, x1, z0, seed);
                vectorIndex = (XNoiseGen * x1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //iy0 = LinearInterpolate(n0, n1, xs);
                iy0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, z, x0, z1, seed);
                vectorIndex = (XNoiseGen * x0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;

                //n1 = GradientNoise(x, z, x1, z1, seed);
                vectorIndex = (XNoiseGen * x1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;

                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //return LinearInterpolate(iy0, ix1, zs);
                value += (((1.0 - zs) * iy0) + (zs * ix1)) * curPersistence;

                x *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }
            return value;
        }
    }
}
