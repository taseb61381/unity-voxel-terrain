﻿
namespace LibNoise.Modifiers
{
    /// <summary>
    /// Module that returns the output of two source modules added together.
    /// </summary>
    public class AddThree
        : IModule
    {
        /// <summary>
        /// The first module from which to retrieve noise.
        /// </summary>
        public IModule SourceModule1 { get; set; }
        /// <summary>
        /// The second module from which to retrieve noise.
        /// </summary>
        public IModule SourceModule2 { get; set; }

        public IModule SourceModule3 { get; set; }

        public AddThree()
        {

        }

        /// <summary>
        /// Initialises a new instance of the Add class.
        /// </summary>
        /// <param name="sourceModule1">The first module from which to retrieve noise.</param>
        /// <param name="sourceModule2">The second module from which to retrieve noise.</param>
        public AddThree(IModule sourceModule1, IModule sourceModule2, IModule sourceModule3)
        {
            if (sourceModule1 == null || sourceModule2 == null || sourceModule3 == null)
            {
                UnityEngine.Debug.LogError("Source modules must be provided.");
                return;
            }

            SourceModule1 = sourceModule1;
            SourceModule2 = sourceModule2;
            SourceModule3 = sourceModule3;
        }

        /// <summary>
        /// Returns the output of the two source modules added together.
        /// </summary>
        public override double GetValue(double x, double y, double z)
        {
            return SourceModule1.GetValue(x, y, z) + SourceModule2.GetValue(x, y, z) + SourceModule3.GetValue(x, y, z);
        }

        /// <summary>
        /// Returns the output of the two source modules added together.
        /// </summary>
        public override double GetValue(double x, double z)
        {
            return SourceModule1.GetValue(x, z) + SourceModule2.GetValue(x, z) + SourceModule3.GetValue(x, z);
        }

        /// <summary>
        /// Returns the output of the two source modules added together.
        /// </summary>
        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return SourceModule1.GetValue(x, y, z, Modules) + SourceModule2.GetValue(x, y, z, Modules) + SourceModule3.GetValue(x, y, z, Modules);
        }

        /// <summary>
        /// Returns the output of the two source modules added together.
        /// </summary>
        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return SourceModule1.GetValue(x, z, Modules) + SourceModule2.GetValue(x, z, Modules) + SourceModule3.GetValue(x, z, Modules);
        }
    }
}
