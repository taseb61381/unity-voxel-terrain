﻿
namespace LibNoise
{
    public class Voronoi
        : ValueNoiseBasis
    {
        public double Frequency { get; set; }
        public double Displacement { get; set; }
        public bool DistanceEnabled { get; set; }
        public int Seed { get; set; }

        public void SetSeed(int Seed)
        {
            this.Seed = Seed;
        }

        public void SetFrequency(double Frequency)
        {
            this.Frequency = Frequency;
        }

        public void SetDisplacement(double Displacement)
        {
            this.Displacement = Displacement;
        }


        public Voronoi()
        {
            Frequency = 1.0;
            Displacement = 1.0;
            Seed = 0;
            DistanceEnabled = true;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            int xInt = (x > 0.0 ? (int)x : (int)x - 1);
            int yInt = (y > 0.0 ? (int)y : (int)y - 1);
            int zInt = (z > 0.0 ? (int)z : (int)z - 1);

            double minDist = 2147483647.0;
            double xCandidate = 0;
            double yCandidate = 0;
            double zCandidate = 0;

            double xPos = 0;
            double yPos = 0;
            double zPos = 0;
            double xDist = 0;
            double yDist = 0;
            double zDist = 0;
            double dist = 0;

            // Inside each unit cube, there is a seed point at a random position.  Go
            // through each of the nearby cubes until we find a cube with a seed point
            // that is closest to the specified position.
            int zCur, yCur, xCur;

            for (zCur = zInt - 2; zCur <= zInt + 2; zCur++)
            {
                for (yCur = yInt - 2; yCur <= yInt + 2; yCur++)
                {
                    for (xCur = xInt - 2; xCur <= xInt + 2; xCur++)
                    {

                        // Calculate the position and distance to the seed point inside of
                        // this unit cube.
                        xPos = xCur + ValueNoise(xCur, yCur, zCur, Seed);
                        yPos = yCur + ValueNoise(xCur, yCur, zCur, Seed + 1);
                        zPos = zCur + ValueNoise(xCur, yCur, zCur, Seed + 2);
                        xDist = xPos - x;
                        yDist = yPos - y;
                        zDist = zPos - z;
                        dist = xDist * xDist + yDist * yDist + zDist * zDist;

                        if (dist < minDist)
                        {
                            // This seed point is closer to any others found so far, so record
                            // this seed point.
                            minDist = dist;
                            xCandidate = xPos;
                            yCandidate = yPos;
                            zCandidate = zPos;
                        }
                    }
                }
            }

            double value;
            if (DistanceEnabled)
            {
                // Determine the distance to the nearest seed point.
                xDist = xCandidate - x;
                yDist = yCandidate - y;
                zDist = zCandidate - z;
                value = (System.Math.Sqrt(xDist * xDist + yDist * yDist + zDist * zDist)
                  ) * Math.Sqrt3 - 1.0;
            }
            else
            {
                value = 0.0;
            }

            int x0 = (xCandidate > 0.0 ? (int)xCandidate : (int)xCandidate - 1);
            int y0 = (yCandidate > 0.0 ? (int)yCandidate : (int)yCandidate - 1);
            int z0 = (zCandidate > 0.0 ? (int)zCandidate : (int)zCandidate - 1);

            // Return the calculated distance with the displacement value applied.
            return value + (Displacement * (double)ValueNoise(x0, y0, z0, 0));
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            x *= Frequency;
            z *= Frequency;

            int xInt = (x > 0.0 ? (int)x : (int)x - 1);
            int zInt = (z > 0.0 ? (int)z : (int)z - 1);

            double minDist = 2147483647.0;
            double xCandidate = 0;
            double zCandidate = 0;

            double xPos = 0;
            double zPos = 0;
            double xDist = 0;
            double zDist = 0;
            double dist = 0;

            // Inside each unit cube, there is a seed point at a random position.  Go
            // through each of the nearby cubes until we find a cube with a seed point
            // that is closest to the specified position.
            int zCur, xCur;

            for (zCur = zInt - 2; zCur <= zInt + 2; zCur++)
            {
                for (xCur = xInt - 2; xCur <= xInt + 2; xCur++)
                {

                    // Calculate the position and distance to the seed point inside of
                    // this unit cube.
                    xPos = xCur + ValueNoise2D(xCur, zCur, Seed);
                    zPos = zCur + ValueNoise2D(xCur, zCur, Seed + 2);
                    xDist = xPos - x;
                    zDist = zPos - z;
                    dist = xDist * xDist + zDist * zDist;

                    if (dist < minDist)
                    {
                        // This seed point is closer to any others found so far, so record
                        // this seed point.
                        minDist = dist;
                        xCandidate = xPos;
                        zCandidate = zPos;
                    }
                }
            }

            if (DistanceEnabled)
            {
                // Determine the distance to the nearest seed point.
                xDist = xCandidate - x;
                zDist = zCandidate - z;
                dist = (System.Math.Sqrt(xDist * xDist + zDist * zDist)) * Math.Sqrt3 - 1.0;
            }
            else
            {
                dist = 0.0;
            }

            // Return the calculated distance with the displacement value applied.
            return dist + (Displacement * (double)ValueNoise2D(
                (xCandidate > 0.0 ? (int)xCandidate : (int)xCandidate - 1),
                (zCandidate > 0.0 ? (int)zCandidate : (int)zCandidate - 1), 0));
        }

        public override double GetValue(double x, double y, double z)
        {
            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            int xInt = (x > 0.0 ? (int)x : (int)x - 1);
            int yInt = (y > 0.0 ? (int)y : (int)y - 1);
            int zInt = (z > 0.0 ? (int)z : (int)z - 1);

            double minDist = 2147483647.0;
            double xCandidate = 0;
            double yCandidate = 0;
            double zCandidate = 0;

            double xPos = 0;
            double yPos = 0;
            double zPos = 0;
            double xDist = 0;
            double yDist = 0;
            double zDist = 0;
            double dist = 0;

            // Inside each unit cube, there is a seed point at a random position.  Go
            // through each of the nearby cubes until we find a cube with a seed point
            // that is closest to the specified position.
            int zCur, yCur, xCur;

            for (zCur = zInt - 2; zCur <= zInt + 2; zCur++)
            {
                for (yCur = yInt - 2; yCur <= yInt + 2; yCur++)
                {
                    for (xCur = xInt - 2; xCur <= xInt + 2; xCur++)
                    {

                        // Calculate the position and distance to the seed point inside of
                        // this unit cube.
                        xPos = xCur + ValueNoise(xCur, yCur, zCur, Seed);
                        yPos = yCur + ValueNoise(xCur, yCur, zCur, Seed + 1);
                        zPos = zCur + ValueNoise(xCur, yCur, zCur, Seed + 2);
                        xDist = xPos - x;
                        yDist = yPos - y;
                        zDist = zPos - z;
                        dist = xDist * xDist + yDist * yDist + zDist * zDist;

                        if (dist < minDist)
                        {
                            // This seed point is closer to any others found so far, so record
                            // this seed point.
                            minDist = dist;
                            xCandidate = xPos;
                            yCandidate = yPos;
                            zCandidate = zPos;
                        }
                    }
                }
            }

            double value;
            if (DistanceEnabled)
            {
                // Determine the distance to the nearest seed point.
                xDist = xCandidate - x;
                yDist = yCandidate - y;
                zDist = zCandidate - z;
                value = (System.Math.Sqrt(xDist * xDist + yDist * yDist + zDist * zDist)
                  ) * Math.Sqrt3 - 1.0;
            }
            else
            {
                value = 0.0;
            }

            int x0 = (xCandidate > 0.0 ? (int)xCandidate : (int)xCandidate - 1);
            int y0 = (yCandidate > 0.0 ? (int)yCandidate : (int)yCandidate - 1);
            int z0 = (zCandidate > 0.0 ? (int)zCandidate : (int)zCandidate - 1);

            // Return the calculated distance with the displacement value applied.
            return value + (Displacement * (double)ValueNoise(x0, y0, z0, 0));
        }

        public override double GetValue(double x, double z)
        {
            x *= Frequency;
            z *= Frequency;

            int xInt = (x > 0.0 ? (int)x : (int)x - 1);
            int zInt = (z > 0.0 ? (int)z : (int)z - 1);

            double minDist = 2147483647.0;
            double xCandidate = 0;
            double zCandidate = 0;

            double xPos = 0;
            double zPos = 0;
            double xDist = 0;
            double zDist = 0;
            double dist = 0;

            // Inside each unit cube, there is a seed point at a random position.  Go
            // through each of the nearby cubes until we find a cube with a seed point
            // that is closest to the specified position.
            int zCur, xCur;

            for (zCur = zInt - 2; zCur <= zInt + 2; zCur++)
            {
                    for (xCur = xInt - 2; xCur <= xInt + 2; xCur++)
                    {

                        // Calculate the position and distance to the seed point inside of
                        // this unit cube.
                        xPos = xCur + ValueNoise2D(xCur, zCur, Seed);
                        zPos = zCur + ValueNoise2D(xCur, zCur, Seed + 2);
                        xDist = xPos - x;
                        zDist = zPos - z;
                        dist = xDist * xDist + zDist * zDist;

                        if (dist < minDist)
                        {
                            // This seed point is closer to any others found so far, so record
                            // this seed point.
                            minDist = dist;
                            xCandidate = xPos;
                            zCandidate = zPos;
                        }
                    }
            }

            if (DistanceEnabled)
            {
                // Determine the distance to the nearest seed point.
                xDist = xCandidate - x;
                zDist = zCandidate - z;
                dist = (System.Math.Sqrt(xDist * xDist + zDist * zDist)) * Math.Sqrt3 - 1.0;
            }
            else
            {
                dist = 0.0;
            }

            // Return the calculated distance with the displacement value applied.
            return dist + (Displacement * (double)ValueNoise2D(
                (xCandidate > 0.0 ? (int)xCandidate : (int)xCandidate - 1),
                (zCandidate > 0.0 ? (int)zCandidate : (int)zCandidate - 1),0));
        }
    }
}
