﻿using UnityEngine;
using System.Collections;

namespace LibNoise
{
    public class HeightMapModuleOuput : IModule
    {
        public LibNoise.IModule Input;

        public HeightMapModuleOuput()
        {

        }

        public override double GetValue(double x, double y, double z)
        {
            return Input.GetValue(x, y, z);
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return Input.GetValue(x, y, z, Modules);
        }

        public override double GetValue(double x, double z)
        {
            return Input.GetValue(x, z);
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return Input.GetValue(x, z, Modules);
        }
    }
}