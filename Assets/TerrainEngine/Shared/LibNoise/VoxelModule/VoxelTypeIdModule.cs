﻿using UnityEngine;
using System.Collections;

public class VoxelTypeIdModule : IVoxelModule 
{
    public string VoxelName = "";

    private int VoxelId = 0;

    public override void InitPalette(IVoxelModule.GetVoxelIdFromName Function)
    {
        VoxelId = Function(VoxelName);
    }

    public override byte GetVoxelId(BiomeData Biomes, double WorldX, double WorldY, double WorldZ, int Altitude)
    {
        return (byte)VoxelId;
    }
}
