﻿using UnityEngine;
using System.Collections;

public abstract class IVoxelModule 
{
    public delegate byte GetVoxelIdFromName(string Name);

    public virtual void Init()
    {

    }

    public virtual void InitPalette(GetVoxelIdFromName Function)
    {

    }


    public virtual byte GetVoxelId(BiomeData Biomes, double WorldX,double WorldY,double WorldZ, int Altitude)
    {
        return (byte)0;
    }
}
