﻿using UnityEngine;
using System.Collections;

public class VoxelTypeNoiseModule : IVoxelModule 
{
    public LibNoise.IModule Noise;
    public IVoxelModule TrueOuput;
    public IVoxelModule FalseOutput;

    public double Min = -1f;
    public double Max = 1f;

    public override byte GetVoxelId(BiomeData Biomes, double WorldX, double WorldY, double WorldZ, int Altitude)
    {
        if(Noise != null)
        {
            double Value = Noise.GetValue(WorldX,WorldY,WorldZ,Biomes);
            if(Value >= Min && Value < Max)
                return TrueOuput.GetVoxelId(Biomes,WorldX,WorldY,WorldZ,Altitude);
            else
                return FalseOutput.GetVoxelId(Biomes,WorldX,WorldY,WorldZ,Altitude);
        }
        else
            return FalseOutput.GetVoxelId(Biomes,WorldX,WorldY,WorldZ,Altitude);
    }
}
