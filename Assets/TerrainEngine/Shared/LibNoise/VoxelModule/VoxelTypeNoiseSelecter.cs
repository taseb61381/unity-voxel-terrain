﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using UnityEngine;
using TerrainEngine;
using LibNoise;

public class VoxelTypeNoiseSelecter : IVoxelModule
{
    public IModule Noise;
    public SList<NoiseNameValues> Modules = new SList<NoiseNameValues>(1);

    public override void Init()
    {
        for (int i = 0; i < Modules.Count; ++i)
            Modules.array[i]._NameHashCode = Modules.array[i].Name.GetHashCode();
    }

    public override byte GetVoxelId(BiomeData Biomes, double WorldX, double WorldY, double WorldZ, int Altitude)
    {
        double val = Noise.GetValue(WorldX, WorldZ);

        for (int i = Modules.Count - 1; i >= 0; --i)
        {
            if (val >= Modules.array[i].MinNoise && val < Modules.array[i].MaxNoise)
                return Modules.array[i].VoxelModule.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);
        }


        return 0;
    }
}
