﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TerrainEngine;
using LibNoise;

public class VoxelTypeBiomeSelecter : IVoxelModule
{
    public SList<NoiseNameValues> Names = new SList<NoiseNameValues>(1);
    public IVoxelModule FalseOutput;

    public override void Init()
    {
        for (int i = 0; i < Names.Count; ++i)
        {
            if (!string.IsNullOrEmpty(Names.array[i].Name))
                Names.array[i]._NameHashCode = Names.array[i].Name.GetHashCode();
            else
                Names.array[i]._NameHashCode = 0;
        }
    }

    public override byte GetVoxelId(BiomeData Biomes, double WorldX, double WorldY, double WorldZ, int Altitude)
    {
        for (int i = Names.Count - 1; i >= 0; --i)
        {
            if (Biomes.Contains(Names.array[i]._NameHashCode))
                return Names.array[i].VoxelModule.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);
        }

        if (FalseOutput != null)
            return FalseOutput.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);

        return 0;
    }
}
