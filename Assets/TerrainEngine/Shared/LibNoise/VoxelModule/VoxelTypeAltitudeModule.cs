﻿using UnityEngine;
using System.Collections;

public class VoxelTypeAltitudeModule : IVoxelModule
{
    public LibNoise.IModule Perturbation;

    public IVoxelModule TrueOuput;
    public IVoxelModule FalseOutput;

    public float NoiseScale = 1f;
    public int Min = 0;
    public int Max = 0;

    public override byte GetVoxelId(BiomeData Biomes, double WorldX, double WorldY, double WorldZ, int Altitude)
    {
        if (Perturbation != null)
        {
            double Value = Perturbation.GetValue(WorldX, WorldZ) * NoiseScale;

            if (Altitude >= Min + Value && Altitude < Max + Value)
            {
                return TrueOuput.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);
            }
            else
                return FalseOutput.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);

        }
        else if (Altitude >= Min && Altitude < Max)
        {
            return TrueOuput.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);
        }
        else
            return FalseOutput.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);
    }
}
