﻿using UnityEngine;
using System.Collections;

public class VoxelTypeBiomeModule : IVoxelModule 
{
    public IVoxelModule TrueOuput;
    public IVoxelModule FalseOutput;

    public string BiomeName;

    private int BiomeHascode = 0;

    public override void Init()
    {
        if (!string.IsNullOrEmpty(BiomeName))
        {
            BiomeHascode = BiomeName.GetHashCode();
        }
        else
            BiomeHascode = 0;
    }

    public override byte GetVoxelId(BiomeData Biomes, double WorldX, double WorldY, double WorldZ, int Altitude)
    {
        if (Biomes == null)
        {
            return FalseOutput.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);
        }
        else if (Biomes.Contains(BiomeHascode))
        {
            return TrueOuput.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);
        }
        else
            return FalseOutput.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);
    }
}
