﻿using UnityEngine;
using System.Collections;

public class VoxelTypeOuput : IVoxelModule
{
    public IVoxelModule Input;

    public override byte GetVoxelId(BiomeData Biomes, double WorldX, double WorldY, double WorldZ, int Altitude)
    {
        return Input.GetVoxelId(Biomes, WorldX, WorldY, WorldZ, Altitude);
    }
}
