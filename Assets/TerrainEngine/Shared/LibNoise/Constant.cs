﻿
namespace LibNoise.Modifiers
{
    public class Constant
        : IModule
    {   
        public double Value { get; set; }

        public Constant()
        {
            Value = 0;
        }

        public Constant(double value)
        {
            Value = value;
        }

        public override double GetValue(double x, double y, double z)
        {
            return Value;
        }

        public override double GetValue(double x, double z)
        {
            return Value;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            return Value;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            return Value;
        }
    }
}
