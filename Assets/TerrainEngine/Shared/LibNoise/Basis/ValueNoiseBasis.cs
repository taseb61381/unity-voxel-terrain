﻿using System;

namespace LibNoise
{
    public class ValueNoiseBasis : IModule     
    {
        private const int XNoiseGen = 1619;
        private const int YNoiseGen = 31337;
        private const int ZNoiseGen = 6971;
        private const int SeedNoiseGen = 1013;
        private const int ShiftNoiseGen = 8;

        static public int IntValueNoise(int x, int y, int z, int seed)
        {
            // All constants are primes and must remain prime in order for this noise
            // function to work correctly.
            x = (XNoiseGen * x + YNoiseGen * y + ZNoiseGen * z + SeedNoiseGen * seed) & 0x7fffffff;
            x = (x >> 13) ^ x;
            return (x * (x * x * 60493 + 19990303) + 1376312589) & 0x7fffffff;
        }

        static public int IntValueNoise(int x, int z, int seed)
        {
            // All constants are primes and must remain prime in order for this noise
            // function to work correctly.
            x = (XNoiseGen * x + ZNoiseGen * z + SeedNoiseGen * seed) & 0x7fffffff;
            x = (x >> 13) ^ x;
            return (x * (x * x * 60493 + 19990303) + 1376312589) & 0x7fffffff;
        }

        public override double GetValue(double x, double y, double z)
        {
            throw new NotImplementedException();
        }

        public override double GetValue(double x, double z)
        {
            throw new NotImplementedException();
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            throw new NotImplementedException();
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            throw new NotImplementedException();
        }

        static public double ValueNoise(int x, int y, int z, int seed)
        {
            return 1.0 - ((double)IntValueNoise(x, y, z, seed) / 1073741824.0);
        }

        static public double ValueNoise2D(int x, int z, int seed)
        {
            return 1.0 - ((double)IntValueNoise(x, z, seed) / 1073741824.0);
        }  
    }
}
