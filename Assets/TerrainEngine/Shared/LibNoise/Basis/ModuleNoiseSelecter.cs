﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using UnityEngine;

namespace LibNoise
{
    [Serializable]
    public struct NoiseNameValues
    {
        public string Name;

        [HideInInspector,XmlIgnore]
        public int _NameHashCode;

        public float MinNoise,MaxNoise;
        public float Alpha;

        public int ModuleId;

        [NonSerialized, XmlIgnore]
        public IModule NoiseModule;

        [NonSerialized, XmlIgnore]
        public IVoxelModule VoxelModule;
    }
}
