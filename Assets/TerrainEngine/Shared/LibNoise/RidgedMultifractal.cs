﻿
namespace LibNoise
{
    public class RidgedMultifractal
        : GradientNoiseBasis
    {
        public double Frequency { get; set; }
        public NoiseQuality NoiseQuality { get; set; }
        public int Seed { get; set; }
        public int OctaveCount = 1;
        private double mLacunarity;

        private const int MaxOctaves = 30;

        private double[] SpectralWeights = new double[MaxOctaves];

        public RidgedMultifractal()
        {
            Frequency = 1.0;
            Lacunarity = 2.0;
            OctaveCount = 6;
            NoiseQuality = NoiseQuality.Standard;
            Seed = 0;
        }

        public override double GetValue(double x, double y, double z)
        {
            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            double signal = 0.0;
            double value = 0.0;
            double weight = 1.0;

            double nx, ny, nz;

            // These parameters should be user-defined; they may be exposed in a
            // future version of libnoise.
            double offset = 1.0;
            double gain = 2.0;
            long seed;
            int x0 = 0;
            int x1 = 0;
            int y0 = 0;
            int y1 = 0;
            int z0 = 0;
            int z1 = 0;

            double n0, n1, ix0, ix1, iy0, iy1;
            long vectorIndex;
            double xs = 0, ys = 0, zs = 0;
            
            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                //double nx, ny, nz;

                /* nx = Math.MakeInt32Range(x);
                 ny = Math.MakeInt32Range(y);
                 nz = Math.MakeInt32Range(z);*/

                nx = x;
                ny = y;
                nz = z;
                seed = (Seed + currentOctave) & 0x7fffffff;
                x0 = (nx > 0.0 ? (int)nx : (int)nx - 1);
                x1 = x0 + 1;
                y0 = (ny > 0.0 ? (int)ny : (int)ny - 1);
                y1 = y0 + 1;
                z0 = (nz > 0.0 ? (int)nz : (int)nz - 1);
                z1 = z0 + 1;

                switch (NoiseQuality)
                {
                    case NoiseQuality.Low:
                        xs = (nx - x0);
                        ys = (ny - y0);
                        zs = (nz - z0);
                        break;
                    case NoiseQuality.Standard:
                        xs = SCurve3(nx - x0);
                        ys = SCurve3(ny - y0);
                        zs = SCurve3(nz - z0);
                        break;
                    case NoiseQuality.High:
                        xs = SCurve5(nx - x0);
                        ys = SCurve5(ny - y0);
                        zs = SCurve5(nz - z0);
                        break;
                }

                //n0 = GradientNoise(x, y, z, x0, y0, z0, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y0, z0, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //ix0 = LinearInterpolate(n0, n1, xs);
                ix0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, y, z, x0, y1, z0, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //n1 = GradientNoise(x, y, z, x1, y1, z0, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //iy0 = LinearInterpolate(ix0, ix1, ys);
                iy0 = ((1.0 - ys) * ix0) + (ys * ix1);

                //n0 = GradientNoise(x, y, z, x0, y0, z1, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y0, z1, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //ix0 = LinearInterpolate(n0, n1, xs);
                ix0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, y, z, x0, y1, z1, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y1, z1, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //iy1 = LinearInterpolate(ix0, ix1, ys);
                iy1 = ((1.0 - ys) * ix0) + (ys * ix1);

                //return LinearInterpolate(iy0, iy1, zs);
                signal = ((1.0 - zs) * iy0) + (zs * iy1);

                // Make the ridges.
                signal = (signal >= 0) ? signal : -signal;
                signal = offset - signal;

                // Square the signal to increase the sharpness of the ridges.
                signal *= signal;

                // The weighting from the previous octave is applied to the signal.
                // Larger values have higher weights, producing sharp points along the
                // ridges.
                signal *= weight;

                // Weight successive contributions by the previous signal.
                weight = signal * gain;
                if (weight > 1.0)
                {
                    weight = 1.0;
                }
                if (weight < 0.0)
                {
                    weight = 0.0;
                }

                // Add the signal to the output value.
                value += (signal * SpectralWeights[currentOctave]);

                // Go to the next octave.
                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
            }

            return (value * 1.25) - 1.0;
        }

        public override double GetValue(double x, double z)
        {
            x *= Frequency;
            z *= Frequency;

            double signal = 0.0;
            double value = 0.0;
            double weight = 1.0;

            // These parameters should be user-defined; they may be exposed in a
            // future version of libnoise.
            double offset = 1.0;
            double gain = 2.0;
            long seed;
            double nx, nz;
            int x0 = 0;
            int x1 = 0;
            int z0 = 0;
            int z1 = 0;

            double xs = 0, zs = 0;
            double ix1, iy0, n0, n1;
            long vectorIndex;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                nx = x;
                nz = z;
                seed = (Seed + currentOctave) & 0x7fffffff;
                x0 = (nx > 0.0 ? (int)nx : (int)nx - 1);
                x1 = x0 + 1;
                z0 = (nz > 0.0 ? (int)nz : (int)nz - 1);
                z1 = z0 + 1;

                switch (NoiseQuality)
                {
                    case NoiseQuality.Low:
                        xs = (nx - x0);
                        zs = (nz - z0);
                        break;
                    case NoiseQuality.Standard:
                        xs = SCurve3(nx - x0);
                        zs = SCurve3(nz - z0);
                        break;
                    case NoiseQuality.High:
                        xs = SCurve5(nx - x0);
                        zs = SCurve5(nz - z0);
                        break;
                }

                //n0 = GradientNoise(x, z, x0, z0, seed);
                vectorIndex = (XNoiseGen * x0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //n1 = GradientNoise(x, z, x1, z0, seed);
                vectorIndex = (XNoiseGen * x1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //iy0 = LinearInterpolate(n0, n1, xs);
                iy0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, z, x0, z1, seed);
                vectorIndex = (XNoiseGen * x0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;

                //n1 = GradientNoise(x, z, x1, z1, seed);
                vectorIndex = (XNoiseGen * x1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;

                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //return LinearInterpolate(iy0, ix1, zs);
                signal = ((1.0 - zs) * iy0) + (zs * ix1);

                // Make the ridges.
                signal = (signal >= 0) ? signal : -signal;
                signal = offset - signal;

                // Square the signal to increase the sharpness of the ridges.
                signal *= signal;

                // The weighting from the previous octave is applied to the signal.
                // Larger values have higher weights, producing sharp points along the
                // ridges.
                signal *= weight;

                // Weight successive contributions by the previous signal.
                weight = signal * gain;
                if (weight > 1.0)
                {
                    weight = 1.0;
                }
                else if (weight < 0.0)
                {
                    weight = 0.0;
                }

                // Add the signal to the output value.
                value += (signal * SpectralWeights[currentOctave]);

                // Go to the next octave.
                x *= Lacunarity;
                z *= Lacunarity;
            }

            return (value * 1.25) - 1.0;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            double signal = 0.0;
            double value = 0.0;
            double weight = 1.0;

            double nx, ny, nz;

            // These parameters should be user-defined; they may be exposed in a
            // future version of libnoise.
            double offset = 1.0;
            double gain = 2.0;
            long seed;
            int x0 = 0;
            int x1 = 0;
            int y0 = 0;
            int y1 = 0;
            int z0 = 0;
            int z1 = 0;

            double n0, n1, ix0, ix1, iy0, iy1;
            long vectorIndex;
            double xs = 0, ys = 0, zs = 0;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                //double nx, ny, nz;

                /* nx = Math.MakeInt32Range(x);
                 ny = Math.MakeInt32Range(y);
                 nz = Math.MakeInt32Range(z);*/

                nx = x;
                ny = y;
                nz = z;
                seed = (Seed + currentOctave) & 0x7fffffff;
                x0 = (nx > 0.0 ? (int)nx : (int)nx - 1);
                x1 = x0 + 1;
                y0 = (ny > 0.0 ? (int)ny : (int)ny - 1);
                y1 = y0 + 1;
                z0 = (nz > 0.0 ? (int)nz : (int)nz - 1);
                z1 = z0 + 1;

                switch (NoiseQuality)
                {
                    case NoiseQuality.Low:
                        xs = (nx - x0);
                        ys = (ny - y0);
                        zs = (nz - z0);
                        break;
                    case NoiseQuality.Standard:
                        xs = SCurve3(nx - x0);
                        ys = SCurve3(ny - y0);
                        zs = SCurve3(nz - z0);
                        break;
                    case NoiseQuality.High:
                        xs = SCurve5(nx - x0);
                        ys = SCurve5(ny - y0);
                        zs = SCurve5(nz - z0);
                        break;
                }

                //n0 = GradientNoise(x, y, z, x0, y0, z0, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y0, z0, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //ix0 = LinearInterpolate(n0, n1, xs);
                ix0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, y, z, x0, y1, z0, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //n1 = GradientNoise(x, y, z, x1, y1, z0, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;


                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //iy0 = LinearInterpolate(ix0, ix1, ys);
                iy0 = ((1.0 - ys) * ix0) + (ys * ix1);

                //n0 = GradientNoise(x, y, z, x0, y0, z1, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y0, z1, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //ix0 = LinearInterpolate(n0, n1, xs);
                ix0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, y, z, x0, y1, z1, seed);
                vectorIndex = (XNoiseGen * x0 + YNoiseGen * y1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //n1 = GradientNoise(x, y, z, x1, y1, z1, seed);
                vectorIndex = (XNoiseGen * x1 + YNoiseGen * y1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 1] * (ny - y1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;


                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //iy1 = LinearInterpolate(ix0, ix1, ys);
                iy1 = ((1.0 - ys) * ix0) + (ys * ix1);

                //return LinearInterpolate(iy0, iy1, zs);
                signal = ((1.0 - zs) * iy0) + (zs * iy1);

                // Make the ridges.
                signal = (signal >= 0) ? signal : -signal;
                signal = offset - signal;

                // Square the signal to increase the sharpness of the ridges.
                signal *= signal;

                // The weighting from the previous octave is applied to the signal.
                // Larger values have higher weights, producing sharp points along the
                // ridges.
                signal *= weight;

                // Weight successive contributions by the previous signal.
                weight = signal * gain;
                if (weight > 1.0)
                {
                    weight = 1.0;
                }
                if (weight < 0.0)
                {
                    weight = 0.0;
                }

                // Add the signal to the output value.
                value += (signal * SpectralWeights[currentOctave]);

                // Go to the next octave.
                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
            }
            return (value * 1.25) - 1.0;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            x *= Frequency;
            z *= Frequency;

            double signal = 0.0;
            double value = 0.0;
            double weight = 1.0;

            // These parameters should be user-defined; they may be exposed in a
            // future version of libnoise.
            double offset = 1.0;
            double gain = 2.0;
            long seed;
            double nx, nz;
            int x0 = 0;
            int x1 = 0;
            int z0 = 0;
            int z1 = 0;

            double xs = 0, zs = 0;
            double ix1, iy0, n0, n1;
            long vectorIndex;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                nx = x;
                nz = z;
                seed = (Seed + currentOctave) & 0x7fffffff;
                x0 = (nx > 0.0 ? (int)nx : (int)nx - 1);
                x1 = x0 + 1;
                z0 = (nz > 0.0 ? (int)nz : (int)nz - 1);
                z1 = z0 + 1;

                switch (NoiseQuality)
                {
                    case NoiseQuality.Low:
                        xs = (nx - x0);
                        zs = (nz - z0);
                        break;
                    case NoiseQuality.Standard:
                        xs = SCurve3(nx - x0);
                        zs = SCurve3(nz - z0);
                        break;
                    case NoiseQuality.High:
                        xs = SCurve5(nx - x0);
                        zs = SCurve5(nz - z0);
                        break;
                }

                //n0 = GradientNoise(x, z, x0, z0, seed);
                vectorIndex = (XNoiseGen * x0 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //n1 = GradientNoise(x, z, x1, z0, seed);
                vectorIndex = (XNoiseGen * x1 + ZNoiseGen * z0 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z0))) * 2.12;

                //iy0 = LinearInterpolate(n0, n1, xs);
                iy0 = ((1.0 - xs) * n0) + (xs * n1);

                //n0 = GradientNoise(x, z, x0, z1, seed);
                vectorIndex = (XNoiseGen * x0 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n0 = ((RandomVectors[(vectorIndex << 2)] * (nx - x0)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;

                //n1 = GradientNoise(x, z, x1, z1, seed);
                vectorIndex = (XNoiseGen * x1 + ZNoiseGen * z1 + SeedNoiseGen * seed) & 0xffffffff;
                vectorIndex ^= (vectorIndex >> ShiftNoiseGen);
                vectorIndex &= 0xff;
                n1 = ((RandomVectors[(vectorIndex << 2)] * (nx - x1)) + (RandomVectors[(vectorIndex << 2) + 2] * (nz - z1))) * 2.12;

                //ix1 = LinearInterpolate(n0, n1, xs);
                ix1 = ((1.0 - xs) * n0) + (xs * n1);

                //return LinearInterpolate(iy0, ix1, zs);
                signal = ((1.0 - zs) * iy0) + (zs * ix1);

                // Make the ridges.
                signal = (signal >= 0) ? signal : -signal;
                signal = offset - signal;

                // Square the signal to increase the sharpness of the ridges.
                signal *= signal;

                // The weighting from the previous octave is applied to the signal.
                // Larger values have higher weights, producing sharp points along the
                // ridges.
                signal *= weight;

                // Weight successive contributions by the previous signal.
                weight = signal * gain;
                if (weight > 1.0)
                {
                    weight = 1.0;
                }
                else if (weight < 0.0)
                {
                    weight = 0.0;
                }

                // Add the signal to the output value.
                value += (signal * SpectralWeights[currentOctave]);

                // Go to the next octave.
                x *= Lacunarity;
                z *= Lacunarity;
            }
            return (value * 1.25) - 1.0;
        }

        public double Lacunarity
        {
            get { return mLacunarity; }
            set
            {
                mLacunarity = value;
                CalculateSpectralWeights();
            }
        }

        private void CalculateSpectralWeights()
        {
            double h = 1.0;

            double frequency = 1.0;
            for (int i = 0; i < MaxOctaves; i++)
            {
                // Compute weight for each frequency.
                SpectralWeights[i] = System.Math.Pow(frequency, -h);
                frequency *= mLacunarity;
            }
        }

        public void SetSeed(int Seed)
        {
            this.Seed = Seed;
        }

        public void SetFrequency(double Frequency)
        {
            this.Frequency = Frequency;
        }

        public void SetLacunarity(double Lacunarity)
        {
            this.Lacunarity = Lacunarity;
        }

        public void SetOctaveCount(int OctaveCount)
        {
            this.OctaveCount = OctaveCount;
        }
    }
}
