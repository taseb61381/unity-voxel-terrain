﻿using System;

namespace LibNoise
{
    public class FastBillow
        : FastNoiseBasis
    {
        public double Frequency { get; set; }
        public double Persistence { get; set; }
        public NoiseQuality NoiseQuality { get; set; }
        public int OctaveCount = 1;
        public double Lacunarity { get; set; }

        private const int MaxOctaves = 30;

        public FastBillow()
            : this(0)
        {

        }

        public FastBillow(int seed)
            : base(seed)
        {
            Frequency = 1.0;
            Lacunarity = 2.0;
            OctaveCount = 6;
            Persistence = 0.5;
            NoiseQuality = NoiseQuality.Standard;
        }

        public override double GetValue(double x, double y, double z)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                signal = GradientCoherentNoise(x, y, z, (int)((mSeed + currentOctave) & 0xffffffff), NoiseQuality);
                signal = 2.0 * System.Math.Abs(signal) - 1.0;
                value += signal * curPersistence;

                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            value += 0.5;
            return value;
        }

        public override double GetValue(double x, double z)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;

            x *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                signal = GradientCoherentNoise(x, z, (int)((mSeed + currentOctave) & 0xffffffff), NoiseQuality);
                signal = 2.0 * System.Math.Abs(signal) - 1.0;
                value += signal * curPersistence;

                x *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            value += 0.5;
            return value;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                signal = GradientCoherentNoise(x, y, z, (int)((mSeed + currentOctave) & 0xffffffff), NoiseQuality);
                signal = 2.0 * System.Math.Abs(signal) - 1.0;
                value += signal * curPersistence;

                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            value += 0.5;
            return value;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;

            x *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                signal = GradientCoherentNoise(x, z, (int)((mSeed + currentOctave) & 0xffffffff), NoiseQuality);
                signal = 2.0 * System.Math.Abs(signal) - 1.0;
                value += signal * curPersistence;

                x *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            value += 0.5;
            return value;
        }
    }
}
