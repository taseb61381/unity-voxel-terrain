﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibNoise
{
    public class CustomNoiseData : IModule
    {
        public string Name;
        public IModule Value;
        public bool SelectUpper = false;
        public bool SelectLower = false;
        private double LastValue;

        private int NameHashCode;

        public override void Init()
        {
            if (String.IsNullOrEmpty(Name))
                NameHashCode = 0;
            else
                NameHashCode = Name.GetHashCode();
        }

        public override double GetValue(double x, double y, double z)
        {
            return 0;
        }

        public override double GetValue(double x, double z)
        {
            return 0;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            double NewValue = Value.GetValue(x, y, z, Modules);
            if (SelectUpper && NewValue > LastValue)
            {
                LastValue = NewValue;
                Modules.SetCustomNoiseData(NameHashCode,NewValue);
                return 0;
            }
            if (SelectLower && NewValue < LastValue)
            {
                LastValue = NewValue;
                Modules.SetCustomNoiseData(NameHashCode, NewValue);
                return 0;
            }

            LastValue = NewValue;
            Modules.SetCustomNoiseData(NameHashCode, NewValue);
            return 0;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            double NewValue = Value.GetValue(x, z, Modules);
            if (SelectUpper && NewValue > LastValue)
            {
                LastValue = NewValue;
                Modules.SetCustomNoiseData(NameHashCode, NewValue);
                return 0;
            }
            if (SelectLower && NewValue < LastValue)
            {
                LastValue = NewValue;
                Modules.SetCustomNoiseData(NameHashCode, NewValue);
                return 0;
            }

            LastValue = NewValue;
            Modules.SetCustomNoiseData(NameHashCode, NewValue);
            return 0;
        }
    }
}
