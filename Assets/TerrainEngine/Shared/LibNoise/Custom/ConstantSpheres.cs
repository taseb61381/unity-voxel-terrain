﻿
namespace LibNoise
{
    public class ConstantSpheres
        : IModule
    {
        public double Frequency = 0.1f;
        public double SphereSize = 2; // Sphere Size and space between spheres
        public double MinNoise = -1; // Min noise from perlin noise to draw a sphere.
        public double SphereNoise = 0.1; // Frequency of perlin noise.
        private Perlin Perlin;

        public ConstantSpheres()
        {
            Frequency = 0.1;
            Perlin = new Perlin();
        }

        internal int NameHashCode = 0;
        public string Name;

        public override void Init()
        {
            if (!string.IsNullOrEmpty(Name))
                NameHashCode = Name.GetHashCode();
            else
                NameHashCode = 0;
        }

        public override double GetValue(double x, double z)
        {
            x *= Frequency;
            z *= Frequency;
            Perlin.Frequency = SphereNoise;

            double targetx = SphereSize * (int)((x + SphereSize * 0.5) / SphereSize);
            double targetz = SphereSize * (int)((z + SphereSize * 0.5) / SphereSize);

            if (MinNoise < -1 || Perlin.GetValue(targetx, targetz) > MinNoise)
            {
                targetx = (targetx - x) * (targetx - x);
                targetz = (targetz - z) * (targetz - z);
                return 1.0 - (System.Math.Sqrt(targetx + targetz) * 4.0);
            }
            return -1;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            x *= Frequency;
            z *= Frequency;
            Perlin.Frequency = SphereNoise;

            double targetx = SphereSize * (int)((x + SphereSize * 0.5) / SphereSize);
            double targetz = SphereSize * (int)((z + SphereSize * 0.5) / SphereSize);

            if (MinNoise < -1 || Perlin.GetValue(targetx, targetz) > MinNoise)
            {
                targetx = (targetx - x) * (targetx - x);
                targetz = (targetz - z) * (targetz - z);

                if (NameHashCode != 0)
                    Modules.Noises.Add(NameHashCode);

                return 1.0 - (System.Math.Sqrt(targetx + targetz) * 4.0);
            }
            return -1;
        }


        public override double GetValue(double x, double y, double z)
        {
            x *= Frequency;
            z *= Frequency;
            Perlin.Frequency = SphereNoise;

            double targetx = SphereSize * (int)((x + SphereSize * 0.5) / SphereSize);
            double targetz = SphereSize * (int)((z + SphereSize * 0.5) / SphereSize);

            if (MinNoise < -1 || Perlin.GetValue(targetx, targetz) > MinNoise)
            {
                targetx = (targetx - x) * (targetx - x);
                targetz = (targetz - z) * (targetz - z);
                return 1.0 - (System.Math.Sqrt(targetx + targetz) * 4.0);
            }
            return -1;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            x *= Frequency;
            z *= Frequency;
            Perlin.Frequency = SphereNoise;

            double targetx = SphereSize * (int)((x + SphereSize * 0.5) / SphereSize);
            double targetz = SphereSize * (int)((z + SphereSize * 0.5) / SphereSize);

            if (MinNoise < -1 || Perlin.GetValue(targetx, targetz) > MinNoise)
            {
                targetx = (targetx - x) * (targetx - x);
                targetz = (targetz - z) * (targetz - z);

                if (NameHashCode != 0)
                    Modules.Noises.Add(NameHashCode);

                return 1.0 - (System.Math.Sqrt(targetx + targetz) * 4.0);
            }
            return -1;
        }
    }
}
