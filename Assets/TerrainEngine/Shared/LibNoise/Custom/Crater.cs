﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibNoise
{
    public class Crater : IModule
    {
        public double Frequency = 1;
        public double Size = 10;
        public int Seed = 0;
        public double radius = 1f;
        public double rimheight = 1f;
        public double craterdepth = 1f;
        public double falloff = 6f;
        public bool CubicForm = false;
        public int Chance = 100;
        public double ChanceFrequency = 1;
        public float OffsetScale = 0f;
        public double OffsetFrequency = 1f;
        public int OffsetSeed = 0;

        public IModule InCrater;
        public IModule InCenter;
        public IModule InFalloff;
        public IModule OutCrater;

        private FastNoise Noise;
        private FastNoise OffsetNoise;

        public Crater()
        {
            
        }

        public override void Init()
        {
            Noise = new FastNoise();
            Noise.Frequency = ChanceFrequency;
            Noise.NoiseQuality = NoiseQuality.Low;
            Noise.OctaveCount = 1;
            Noise.Seed = Seed;

            OffsetNoise = new FastNoise();
            OffsetNoise.Frequency = OffsetFrequency;
            OffsetNoise.Seed = OffsetSeed;
            OffsetNoise.OctaveCount = 1;
        }

        public override double GetValue(double x, double z)
        {
            double Ox = x;
            double Oz = z;

            if (x < 0) x = -x;
            if (z < 0) z = -z;

            x *= Frequency;
            z *= Frequency;

            double dx = Size * (int)((x + Size * 0.5) / Size);
            double dz = Size * (int)((z + Size * 0.5) / Size);

            double Offset = OffsetNoise.GetValue(dx, dz) * OffsetScale;

            x += Offset;
            z += Offset;

            dx = Size * (int)((x + Size * 0.5) / Size);
            dz = Size * (int)((z + Size * 0.5) / Size);

            if (Chance >= 100 || ((1.0 + Noise.GetValue(dx, dz)) * 0.5 * 100.0) < Chance)
            {
                dx = ((dx - x) * (dx - x));
                dz = ((dz - z) * (dz - z));

                double dist = CubicForm ? System.Math.Sqrt(dx * dx + dz * dz) : System.Math.Sqrt(dx + dz);
                double height = 0.0d;
                if (dist < radius) // Inside Crater Center
                {
                    if (CubicForm)
                        height = 1.0d - ((dx * dx + dz * dz) / (radius * radius));
                    else
                        height = 1.0d - ((dx + dz) / (radius));

                    height = rimheight - (height * craterdepth);

                    if ((object)InCenter != null)
                        InCenter.GetValue(Ox, Oz);
                }
                else if ((dist - radius) < falloff) // InsideFalloff
                {
                    height = (1.0d - ((dist - radius) / falloff)) * rimheight;

                    if ((object)InFalloff != null)
                        InFalloff.GetValue(Ox, Oz);
                }
                else // Outside Crater
                {
                    height = 0.0d;
                }

                if ((object)InCrater != null)
                    InCrater.GetValue(Ox, Oz);

                return height;
            }

            if ((object)OutCrater != null)
                OutCrater.GetValue(Ox, Oz);
            return 0.0;
        }
        public override double GetValue(double x, double z, BiomeData Modules)
        {
            double Ox = x;
            double Oz = z;

            if (x < 0) x = -x;
            if (z < 0) z = -z;

            x *= Frequency;
            z *= Frequency;

            double dx = Size * (int)((x + Size * 0.5) / Size);
            double dz = Size * (int)((z + Size * 0.5) / Size);

            double Offset = OffsetNoise.GetValue(dx, dz) * OffsetScale;

            x += Offset;
            z += Offset;

            dx = Size * (int)((x + Size * 0.5) / Size);
            dz = Size * (int)((z + Size * 0.5) / Size);

            if (Chance >= 100 || ((1.0 + Noise.GetValue(dx, dz)) * 0.5 * 100.0) < Chance)
            {
                dx = (dx - x) * (dx - x);
                dz = (dz - z) * (dz - z);

                double dist = CubicForm ? System.Math.Sqrt(dx * dx + dz * dz) : System.Math.Sqrt(dx + dz);
                double height = 0.0d;
                if (dist < radius) // Inside Crater Center
                {
                    if (CubicForm)
                        height = 1.0d - ((dx * dx + dz * dz) / (radius * radius));
                    else
                        height = 1.0d - ((dx + dz) / (radius));

                    height = rimheight - (height * craterdepth);

                    if ((object)InCenter != null)
                        InCenter.GetValue(Ox, Oz,Modules);
                }
                else if ((dist - radius) < falloff) // InsideFalloff
                {
                    height = (1.0d - ((dist - radius) / falloff)) * rimheight;

                    if ((object)InFalloff != null)
                        InFalloff.GetValue(Ox, Oz,Modules);
                }
                else // Outside Crater
                {
                    height = 0.0d;
                }

                if ((object)InCrater != null)
                    InCrater.GetValue(Ox, Oz,Modules);

                return height;
            }

            if ((object)OutCrater != null)
                OutCrater.GetValue(Ox, Oz,Modules);
            return 0.0;
        }

        public override double GetValue(double x, double y, double z)
        {
            double Ox = x;
            double Oy = y;
            double Oz = z;

            if (x < 0) x = -x;
            if (z < 0) z = -z;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            double dx = Size * (int)((x + Size * 0.5) / Size);
            double dz = Size * (int)((z + Size * 0.5) / Size);

            double Offset = OffsetNoise.GetValue(dx, dz) * OffsetScale;

            x += Offset;
            z += Offset;

            dx = Size * (int)((x + Size * 0.5) / Size);
            dz = Size * (int)((z + Size * 0.5) / Size);

            if (Chance >= 100 || ((1.0 + Noise.GetValue(dx, dz)) * 0.5 * 100.0) < Chance)
            {
                dx = (dx - x) * (dx - x);
                dz = (dz - z) * (dz - z);

                double dist = CubicForm ? System.Math.Sqrt(dx * dx + dz * dz) : System.Math.Sqrt(dx + dz);
                double height = 0.0d;
                if (dist < radius) // Inside Crater Center
                {
                    if (CubicForm)
                        height = 1.0d - ((dx * dx + dz * dz) / (radius * radius));
                    else
                        height = 1.0d - ((dx + dz) / (radius));

                    height = rimheight - (height * craterdepth);

                    if ((object)InCenter != null)
                        InCenter.GetValue(Ox,Oy, Oz);
                }
                else if ((dist - radius) < falloff) // InsideFalloff
                {
                    height = (1.0d - ((dist - radius) / falloff)) * rimheight;

                    if ((object)InFalloff != null)
                        InFalloff.GetValue(Ox,Oy, Oz);
                }
                else // Outside Crater
                {
                    height = 0.0d;
                }

                if ((object)InCrater != null)
                    InCrater.GetValue(Ox,Oy, Oz);

                return height;
            }

            if ((object)OutCrater != null)
                OutCrater.GetValue(Ox,Oy, Oz);
            return 0.0;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            double Ox = x;
            double Oy = y;
            double Oz = z;

            if (x < 0) x = -x;
            if (z < 0) z = -z;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            double dx = Size * (int)((x + Size * 0.5) / Size);
            double dz = Size * (int)((z + Size * 0.5) / Size);

            double Offset = OffsetNoise.GetValue(dx, dz) * OffsetScale;

            x += Offset;
            z += Offset;

            dx = Size * (int)((x + Size * 0.5) / Size);
            dz = Size * (int)((z + Size * 0.5) / Size);

            if (Chance >= 100 || ((1.0 + Noise.GetValue(dx, dz)) * 0.5 * 100.0) < Chance)
            {
                dx = (dx - x) * (dx - x);
                dz = (dz - z) * (dz - z);

                double dist = CubicForm ? System.Math.Sqrt(dx * dx + dz * dz) : System.Math.Sqrt(dx + dz);
                double height = 0.0d;
                if (dist < radius) // Inside Crater Center
                {
                    if (CubicForm)
                        height = 1.0d - ((dx * dx + dz * dz) / (radius * radius));
                    else
                        height = 1.0d - ((dx + dz) / (radius));

                    height = rimheight - (height * craterdepth);

                    if ((object)InCenter != null)
                        InCenter.GetValue(Ox,Oy, Oz,Modules);
                }
                else if ((dist - radius) < falloff) // InsideFalloff
                {
                    height = (1.0d - ((dist - radius) / falloff)) * rimheight;

                    if ((object)InFalloff != null)
                        InFalloff.GetValue(Ox,Oy, Oz, Modules);
                }
                else // Outside Crater
                {
                    height = 0.0d;
                }

                if ((object)InCrater != null)
                    InCrater.GetValue(Ox, Oy, Oz, Modules);

                return height;
            }

            if ((object)OutCrater != null)
                OutCrater.GetValue(Ox, Oy, Oz, Modules);
            return 0.0;
        }

        //Compute the dot product AB . AC
        static private double DotProduct(double pointAx, double pointAy, double pointBx, double pointBy, double pointCx, double pointCy)
        {
            return (pointBx - pointAx) * (pointCx - pointBx) + (pointBy - pointAy) * (pointCy - pointBy);
        }

        //Compute the cross product AB x AC
        static private double CrossProduct(double pointAx, double pointAy, double pointBx, double pointBy, double pointCx, double pointCy)
        {
            return (pointBx - pointAx) * (pointCy - pointAy) - (pointBy - pointAy) * (pointCx - pointAx);
        }

        //Compute the distance from A to B
        static double Distance(double pointAx, double pointAy, double pointBx, double pointBy)
        {
            double d1 = pointAx - pointBx;
            double d2 = pointAy - pointBy;

            return System.Math.Sqrt(d1 * d1 + d2 * d2);
        }

        //Compute the distance from AB to C
        //if isSegment is true, AB is a segment, not a line.
        static double LineToPointDistance2D(double pointAx, double pointAy, double pointBx, double pointBy, double pointCx, double pointCy,
            bool isSegment)
        {
            double dist = CrossProduct(pointAx, pointAy, pointBx, pointBy, pointCx, pointCy) / Distance(pointAx, pointAy, pointBx, pointBy);
            if (isSegment)
            {
                double dot1 = DotProduct(pointAx, pointAy, pointBx, pointBy, pointCx, pointCy);
                if (dot1 > 0)
                    return Distance(pointBx, pointBy, pointCx, pointCy);

                double dot2 = DotProduct(pointAx, pointAy, pointBx, pointBy, pointCx, pointCy);
                if (dot2 > 0)
                    return Distance(pointAx, pointAy, pointCx, pointCy);
            }
            return System.Math.Abs(dist);
        } 
    }
}
