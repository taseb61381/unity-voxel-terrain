﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibNoise
{
    public class ClampNoise : IModule
    {
        public double Size = 0.1;
        public double Middle = 0;
        public bool Squared = true;
        public double Bias = 0;
        public IModule Input;

        public IModule Inside;
        public IModule Outside;

        public ClampNoise()
        {

        }

        public override double GetValue(double x, double z)
        {
            double value = Input.GetValue(x, z);
            if (value < Middle - Size || value > Middle + Size)
            {
                if ((object)Outside != null)
                    Outside.GetValue(x, z);

                return 1;
            }

            value -= Middle;
            if (value < 0) value = -value;

            if ((object)Inside != null)
                Inside.GetValue(x, z);

            x = (Squared ? System.Math.Sqrt((value / Size)) : (value / Size)) + Bias;
            if (x > 1) x = 1;
            if (x < -1) x = -1;
            return x;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            double value = Input.GetValue(x, z,Modules);
            if (value < Middle - Size || value > Middle + Size)
            {
                if ((object)Outside != null)
                    Outside.GetValue(x, z, Modules);

                return 1;
            }

            value -= Middle;
            if (value < 0) value = -value;

            if ((object)Inside != null)
                Inside.GetValue(x, z, Modules);

            x = (Squared ? System.Math.Sqrt((value / Size)) : (value / Size)) + Bias;
            if (x > 1) x = 1;
            if (x < -1) x = -1;
            return x;
        }

        public override double GetValue(double x, double y, double z)
        {
            double value = Input.GetValue(x,y, z);
            if (value < Middle - Size || value > Middle + Size)
            {
                if ((object)Outside != null)
                    Outside.GetValue(x,y, z);

                return 1;
            }

            value -= Middle;
            if (value < 0) value = -value;

            if ((object)Inside != null)
                Inside.GetValue(x,y, z);

            x = (Squared ? System.Math.Sqrt((value / Size)) : (value / Size)) + Bias;
            if (x > 1) x = 1;
            if (x < -1) x = -1;
            return x;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            double value = Input.GetValue(x,y,z,Modules);
            if (value < Middle - Size || value > Middle + Size)
            {
                if ((object)Outside != null)
                    Outside.GetValue(x, y, z, Modules);

                return 1;
            }

            value -= Middle;
            if (value < 0) value = -value;

            if ((object)Inside != null)
                Inside.GetValue(x, y, z, Modules);

            x = (Squared ? System.Math.Sqrt((value / Size)) : (value / Size)) + Bias;
            if (x > 1) x = 1;
            if (x < -1) x = -1;
            return x;
        }
    }
}
