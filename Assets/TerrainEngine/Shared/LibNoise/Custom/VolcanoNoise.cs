﻿using UnityEngine;

namespace LibNoise
{
    public class VolcanoNoise
        : IModule
    {
        public double Frequency = 1f;
        public double VolcanoSize = 100; // Volcano Size and space between them
        public double MinNoise = -2; // Min noise from perlin noise to draw a sphere.
        public double PerlinFrequency = 1; // Frequency of perlin noise.
        public double CraterSize = 5; // Size of the volcano cone
        public double CraterHeight = 0.5; // Crater center height
        public bool GenerateRivers = true;
        public bool Expodend = false;
        private Perlin Perlin; // Used for noise placement.

        public IModule InsideCrater;
        public IModule InsideRiver;
        public IModule InsideVolcanoe;
        public IModule OutsideVolcanoe;

        public VolcanoNoise()
        {
            Frequency = 0.1;
            Perlin = new Perlin();
            Perlin.NoiseQuality = NoiseQuality.Low;
        }

        public double GetResult(double result,double distance,double Mid,double x,double z,double tx,double tz)
        {
            Mid = VolcanoSize * CraterSize;

            if (distance < Mid)
            {
                if (distance < Mid / 2)
                {
                    distance = Mid / 2;
                }

                result = Mid + (Mid - distance) * CraterHeight;
            }

            if (Expodend)
            {
                result = (System.Math.Sqrt(VolcanoSize) / result);
            }
            else
            {
                result = (((VolcanoSize)) / result);
            }
        
            return result;
        }

        public override double GetValue(double x, double y, double z)
        {
            x *= Frequency;
            z *= Frequency;
            Perlin.Frequency = PerlinFrequency;

            x += Perlin.GetValue(x*0.01, x*0.01) * 100;
            z += Perlin.GetValue(z*0.01, z*0.01) * 100;

            if (x < 0) x = -x;
            if (z < 0) z = -z;

            double targetx = VolcanoSize * (int)((x + VolcanoSize * 0.5) / VolcanoSize);
            double targetz = VolcanoSize * (int)((z + VolcanoSize * 0.5) / VolcanoSize);

            if (MinNoise < -1 || Perlin.GetValue(targetx, targetz) > MinNoise)
            {
                if (GenerateRivers)
                {
                    float d = (float)Perlin.GetValue(targetx * 10, targetz * 10) * 360f;
                    Vector2 p = Quaternion.Euler(0, 0, d) * new Vector2(0, 1);

                    double dist = GenerateRivers ? LineToPointDistance2D(targetx + p.x, targetz + p.y, targetx - p.x, targetz - p.y, x, z, false) : 0;
                }

                targetx = (targetx - x) * (targetx - x);
                targetz = (targetz - z) * (targetz - z);

                double result = 0;
                double distance = result = Expodend ? System.Math.Sqrt(targetx + targetz) : targetx+targetz;
                double Mid = (Expodend ? System.Math.Sqrt((VolcanoSize * 0.5)) * CraterSize : CraterSize * ((VolcanoSize * 0.5) + (VolcanoSize * 0.5)));

                return GetResult(result, distance, Mid, x, z,targetx,targetz);
            }
            return 0;
        }

        public override double GetValue(double x, double z)
        {
            x *= Frequency;
            z *= Frequency;
            Perlin.Frequency = PerlinFrequency;

            x += Perlin.GetValue(x * 0.001, x * 0.001) * VolcanoSize;
            z += Perlin.GetValue(z * 0.001, z * 0.001) * VolcanoSize;

            if (x < 0) x = -x;
            if (z < 0) z = -z;

            double targetx = VolcanoSize * (int)((x + VolcanoSize * 0.5) / VolcanoSize);
            double targetz = VolcanoSize * (int)((z + VolcanoSize * 0.5) / VolcanoSize);

            if (MinNoise < -1 || Perlin.GetValue(targetx, targetz) > MinNoise)
            {
                targetx = (targetx - x) * (targetx - x);
                targetz = (targetz - z) * (targetz - z);

                double result = 0;
                double distance = result = System.Math.Sqrt(targetx + targetz);
                double Mid = VolcanoSize * CraterSize;

                if (distance < Mid / 2)
                {
                    distance = Mid / 2;

                    return (1 - result) * Mid/distance;
                }

                return (1 - result);
            }
            return 0;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            double ox = x;
            double oy = y;
            double oz = z;


            x *= Frequency;
            z *= Frequency;
            Perlin.Frequency = PerlinFrequency;

            if (x < 0) x = -x;
            if (z < 0) z = -z;

            double targetx = VolcanoSize * (int)((x + VolcanoSize * 0.5) / VolcanoSize);
            double targetz = VolcanoSize * (int)((z + VolcanoSize * 0.5) / VolcanoSize);

            if (MinNoise < -1 || Perlin.GetValue(targetx, targetz) > MinNoise)
            {
                float d = (float)Perlin.GetValue(targetx*10, targetz*10) * 360f;
                Vector2 p = Quaternion.Euler(0, 0, d) * new Vector2(0, 1);

                if (InsideVolcanoe != null)
                    InsideVolcanoe.GetValue(ox, oy, oz, Modules);

                double dist = GenerateRivers ? LineToPointDistance2D(targetx + p.x, targetz + p.y, targetx - p.x, targetz - p.y, x, z, false) : 0;

                targetx = (targetx - x) * (targetx - x);
                targetz = (targetz - z) * (targetz - z);

                double result = 0;
                double distance = result = Expodend ? System.Math.Sqrt(targetx + targetz) : targetx + targetz;
                double Mid = (Expodend ? System.Math.Sqrt((VolcanoSize * 0.5)) * CraterSize : CraterSize * ((VolcanoSize * 0.5) + (VolcanoSize * 0.5)));

                if (distance < Mid / 2)
                {
                    if (InsideCrater != null)
                        InsideCrater.GetValue(ox,oy,oz, Modules);
                }

                if (GenerateRivers && distance > Mid / 2)
                {
                    if (dist < 0.02 + (Perlin.GetValue(x * 20, z * 20) * 0.015))
                    {
                        if (InsideRiver != null)
                            InsideRiver.GetValue(ox,oy,oz, Modules);
                    }
                }

                return GetResult(result, distance, Mid, x, z, targetx, targetz);
            }

            if (OutsideVolcanoe != null)
                return OutsideVolcanoe.GetValue(ox,oy,oz, Modules);
            return 0;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            double ox = x;
            double oz = z;


            x *= Frequency;
            z *= Frequency;
            Perlin.Frequency = PerlinFrequency;

            if (x < 0) x = -x;
            if (z < 0) z = -z;

            double targetx = VolcanoSize * (int)((x + VolcanoSize * 0.5) / VolcanoSize);
            double targetz = VolcanoSize * (int)((z + VolcanoSize * 0.5) / VolcanoSize);

            if (MinNoise < -1 || Perlin.GetValue(targetx, targetz) > MinNoise)
            {
                float d = (float)Perlin.GetValue(targetx * 10, targetz * 10) * 360f;
                Vector2 p = Quaternion.Euler(0, 0, d) * new Vector2(0, 1);

                double dist = GenerateRivers ? LineToPointDistance2D(targetx + p.x, targetz + p.y, targetx - p.x, targetz - p.y, x, z, false) : 0;

                targetx = (targetx - x) * (targetx - x);
                targetz = (targetz - z) * (targetz - z);

                double result = 0;
                double distance = result = Expodend ? System.Math.Sqrt(targetx + targetz) : targetx + targetz;
                double Mid = (Expodend ? System.Math.Sqrt((VolcanoSize * 0.5)) * CraterSize : CraterSize * ((VolcanoSize * 0.5) + (VolcanoSize * 0.5)));

                if (distance < Mid / 2)
                {
                    if (InsideCrater != null)
                        InsideCrater.GetValue(ox, oz, Modules);
                }

                if (GenerateRivers && distance > Mid / 2)
                {
                    if (dist < 0.02 + (Perlin.GetValue(x * 20, z * 20) * 0.015))
                    {
                        if (InsideRiver != null)
                            InsideRiver.GetValue(ox, oz, Modules);
                    }
                }

                if (InsideVolcanoe != null)
                    InsideVolcanoe.GetValue(ox, oz, Modules);

                return GetResult(result, distance, Mid, x, z, targetx, targetz);
            }

            if (OutsideVolcanoe != null)
                return OutsideVolcanoe.GetValue(ox, oz, Modules);

            return 0;
        }

        //Compute the dot product AB . AC
        static private double DotProduct(double pointAx,double pointAy,double pointBx,double pointBy,double pointCx,double pointCy)
        {
            return (pointBx - pointAx) * (pointCx - pointBx) + (pointBy - pointAy) * (pointCy - pointBy);
        }

        //Compute the cross product AB x AC
        static private double CrossProduct(double pointAx, double pointAy, double pointBx, double pointBy, double pointCx, double pointCy)
        {
            return (pointBx - pointAx) * (pointCy - pointAy) - (pointBy - pointAy) * (pointCx - pointAx);
        }

        //Compute the distance from A to B
        static double Distance(double pointAx, double pointAy, double pointBx, double pointBy)
        {
            double d1 = pointAx - pointBx;
            double d2 = pointAy - pointBy;

            return System.Math.Sqrt(d1 * d1 + d2 * d2);
        }

        //Compute the distance from AB to C
        //if isSegment is true, AB is a segment, not a line.
        static double LineToPointDistance2D(double pointAx,double pointAy,double pointBx,double pointBy,double pointCx,double pointCy,
            bool isSegment)
        {
            double dist = CrossProduct(pointAx, pointAy, pointBx, pointBy, pointCx, pointCy) / Distance(pointAx, pointAy, pointBx, pointBy);
            if (isSegment)
            {
                double dot1 = DotProduct(pointAx, pointAy, pointBx, pointBy, pointCx, pointCy);
                if (dot1 > 0)
                    return Distance(pointBx, pointBy, pointCx, pointCy);

                double dot2 = DotProduct(pointAx, pointAy, pointBx, pointBy, pointCx, pointCy);
                if (dot2 > 0)
                    return Distance(pointAx, pointAy, pointCx, pointCy);
            }
            return System.Math.Abs(dist);
        } 
    }
}
