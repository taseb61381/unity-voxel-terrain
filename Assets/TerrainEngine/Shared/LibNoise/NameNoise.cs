﻿
namespace LibNoise
{
    public class NameNoise : IModule
    {
        public IModule Input;

        public NameNoise()
        {

        }

        internal int NameHashCode = 0;
        public string Name;

        public override void Init()
        {
            if (!string.IsNullOrEmpty(Name))
                NameHashCode = Name.GetHashCode();
            else
                NameHashCode = 0;
        }

        public override double GetValue(double x,  double z)
        {
            if ((object)Input != null)
                return Input.GetValue(x, z);
            return 0;
        }

        public override double GetValue(double x,  double z, BiomeData Modules)
        {
            if (NameHashCode != 0)
                Modules.Noises.Add(NameHashCode);

            if ((object)Input != null)
                return Input.GetValue(x, z, Modules);

            return 0;
        }

        public override double GetValue(double x, double y, double z)
        {
            if ((object)Input != null)
                return Input.GetValue(x, y, z);

            return 0;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            if (NameHashCode != 0)
                Modules.Noises.Add(NameHashCode);

            if ((object)Input != null)
                return Input.GetValue(x, y, z, Modules);
            return 0;
        }
    }
}
