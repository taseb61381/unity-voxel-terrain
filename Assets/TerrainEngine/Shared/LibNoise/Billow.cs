﻿using System;

namespace LibNoise
{
    public class Billow
        : GradientNoiseBasis
    {
        public double Frequency { get; set; }
        public double Persistence { get; set; }
        public NoiseQuality NoiseQuality { get; set; }
        public int Seed { get; set; }
        public int OctaveCount = 1;
        public double Lacunarity { get; set; }

        public void SetSeed(int Seed)
        {
            this.Seed = Seed;
        }

        public void SetFrequency(double Frequency)
        {
            this.Frequency = Frequency;
        }

        public void SetPersistence(double Persistence)
        {
            this.Persistence = Persistence;
        }

        public void SetLacunarity(double Lacunarity)
        {
            this.Lacunarity = Lacunarity;
        }

        public void SetOctaveCount(int OctaveCount)
        {
            this.OctaveCount = OctaveCount;
        }

        public Billow()
        {
            Frequency = 1.0;
            Lacunarity = 2.0;
            OctaveCount = 6;
            Persistence = 0.5;
            NoiseQuality = NoiseQuality.Standard;
            Seed = 0;
        }

        public override double GetValue(double x, double y, double z)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;
            //double nx, ny, nz;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                signal = GradientCoherentNoise(x, y, z, (int)(long)((Seed + currentOctave) & 0xffffffff), NoiseQuality);
                signal = 2.0 * System.Math.Abs(signal) - 1.0;
                value += signal * curPersistence;

                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            value += 0.5;
            return value;
        }

        public override double GetValue(double x, double z)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;
            //double nx, ny, nz;

            x *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                signal = GradientCoherentNoise(x, z, (int)(long)((Seed + currentOctave) & 0xffffffff), NoiseQuality);
                signal = 2.0 * System.Math.Abs(signal) - 1.0;
                value += signal * curPersistence;

                x *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            value += 0.5;
            return value;
        }

        public override double GetValue(double x, double y, double z, BiomeData Modules)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;
            //double nx, ny, nz;

            x *= Frequency;
            y *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                signal = GradientCoherentNoise(x, y, z, (int)(long)((Seed + currentOctave) & 0xffffffff), NoiseQuality);
                signal = 2.0 * System.Math.Abs(signal) - 1.0;
                value += signal * curPersistence;

                x *= Lacunarity;
                y *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            value += 0.5;
            return value;
        }

        public override double GetValue(double x, double z, BiomeData Modules)
        {
            double value = 0.0;
            double signal = 0.0;
            double curPersistence = 1.0;
            //double nx, ny, nz;
            long seed;

            x *= Frequency;
            z *= Frequency;

            for (int currentOctave = 0; currentOctave < OctaveCount; currentOctave++)
            {
                seed = (Seed + currentOctave) & 0xffffffff;
                signal = GradientCoherentNoise(x, z, (int)seed, NoiseQuality);
                signal = 2.0 * System.Math.Abs(signal) - 1.0;
                value += signal * curPersistence;

                x *= Lacunarity;
                z *= Lacunarity;
                curPersistence *= Persistence;
            }

            value += 0.5;
            return value;
        }
    }
}
