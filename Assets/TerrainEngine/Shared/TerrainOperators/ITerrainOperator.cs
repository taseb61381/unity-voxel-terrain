﻿
namespace TerrainEngine
{
    /// <summary>
    ///  Class that will handle terrain operations. (Voxel modification, Custom processor data modification etc)
    /// </summary>
    public abstract class ITerrainOperator
    {
        public delegate bool CanModifyVoxel(ITerrainOperator Operator, ref VoxelFlush Flush); // Return True if voxel can me modified

        public ushort BlockId;
        public uint ObjectId;
        public int x, y, z;
        public byte Type;
        public float Volume;
        public VoxelModificationType ModType;
        public bool IsNetwork;

        public bool IsRemoving()
        {
            switch (ModType)
            {
                case VoxelModificationType.SET_TYPE:
                    return Type == 0;
                case VoxelModificationType.SET_VOLUME:
                    return Volume < TerrainInformation.Isolevel;
                case VoxelModificationType.ADD_VOLUME:
                    return Volume < TerrainInformation.Isolevel;
                case VoxelModificationType.REMOVE_VOLUME:
                    return Volume >= TerrainInformation.Isolevel;
                case VoxelModificationType.SET_TYPE_ADD_VOLUME:
                    return Type == 0 || Volume < TerrainInformation.Isolevel;
                case VoxelModificationType.SET_TYPE_REMOVE_VOLUME:
                    return Type == 0 || Volume >= TerrainInformation.Isolevel;
                case VoxelModificationType.SET_TYPE_SET_VOLUME:
                    return Type == 0 || Volume < TerrainInformation.Isolevel;
                case VoxelModificationType.SEND_EVENTS:
                    return Type == 0;
                case VoxelModificationType.NETWORK:
                    return Type == 0;
                case VoxelModificationType.REMOVE_VOLUME_IF_TYPE:
                    return Volume >= TerrainInformation.Isolevel;
                case VoxelModificationType.ADD_VOLUME_IF_TYPE:
                    return Volume < TerrainInformation.Isolevel;
                case VoxelModificationType.SET_TYPE_ADD_VOLUME_IF_EMPTY:
                    return Type == 0;
                case VoxelModificationType.SET_TYPE_ADD_VOLUME_IF_EMPTY_OR_EQUAL:
                    return Type == 0;
            }

            return true;
        }

        public void InitPosition(ITerrainBlock Block, VectorI3 Position)
        {
            BlockId = Block.BlockId;
            x = Position.x;
            y = Position.y;
            z = Position.z;
        }

        public void Init(byte Type, float Volume, VoxelModificationType ModType, bool IsNetwork = false)
        {
            this.Type = Type;
            this.Volume = Volume;
            this.ModType = ModType;
            this.IsNetwork = IsNetwork;
        }

        /// <summary>
        /// Return true if this operator need to find a block. False if operator will search block inside Execute()
        /// </summary>
        public virtual bool HasBlock()
        {
            return true;
        }

        static public void ApplyVoxelModification(VoxelModificationType ModType, ref byte CurrentType, ref float CurrentVolume, byte Type, float Volume)
        {
            switch (ModType)
            {
                case VoxelModificationType.SET_TYPE:
                    CurrentType = Type;
                    break;

                case VoxelModificationType.SET_VOLUME:
                    CurrentVolume = Volume;
                    break;

                case VoxelModificationType.ADD_VOLUME:
                    CurrentVolume = CurrentVolume + Volume;
                    break;

                case VoxelModificationType.REMOVE_VOLUME:
                    CurrentVolume = CurrentVolume - Volume;
                    break;

                case VoxelModificationType.SET_TYPE_SET_VOLUME:
                    CurrentType = Type;
                    CurrentVolume = Volume;
                    break;

                case VoxelModificationType.NETWORK:
                    CurrentType = Type;
                    CurrentVolume = Volume;
                    break;

                case VoxelModificationType.SET_TYPE_ADD_VOLUME:
                    CurrentVolume = CurrentVolume + Volume;
                    CurrentType = Type;
                    break;

                case VoxelModificationType.SET_TYPE_REMOVE_VOLUME:
                    CurrentVolume = CurrentVolume - Volume;
                    CurrentType = Type;
                    break;
                case VoxelModificationType.REMOVE_VOLUME_IF_TYPE:
                    if (CurrentType == Type)
                        CurrentVolume = CurrentVolume - Volume;
                    break;

                case VoxelModificationType.ADD_VOLUME_IF_TYPE:
                    if (CurrentType == Type)
                        CurrentVolume = CurrentVolume + Volume;
                    break;
                case VoxelModificationType.SET_TYPE_ADD_VOLUME_IF_EMPTY:
                    if (CurrentType == 0)
                    {
                        CurrentVolume = CurrentVolume + Volume;
                        CurrentType = Type;
                    }
                    break;
                case VoxelModificationType.SET_TYPE_ADD_VOLUME_IF_EMPTY_OR_EQUAL:
                    if (CurrentType == 0 || CurrentType == Type)
                    {
                        CurrentVolume = CurrentVolume + Volume;
                        CurrentType = Type;
                    }
                    break;

            }

            if (CurrentVolume < TerrainDatas.MinVolume)
            {
                CurrentVolume = TerrainDatas.MinVolume;
            }
            else if (CurrentVolume > TerrainDatas.MaxVolume)
                CurrentVolume = TerrainDatas.MaxVolume;
        }

        public void SetVoxel(ITerrainBlock Block, int vx, int vy, int vz, byte Type, float Volume, VoxelModificationType ModType, CanModifyVoxel CanModify, CanModifyVoxel OnModify)
        {
            Block = Block.GetAroundBlock(ref vx, ref vy, ref vz);

            if (Block == null || Block.HasState(TerrainBlockStates.CLOSED))
                return;

            byte CurrentType = 0;
            float CurrentVolume = 0;
            Block.Voxels.GetTypeAndVolume(vx, vy, vz, ref CurrentType, ref CurrentVolume);
            byte NewType = CurrentType;
            float NewVolume = CurrentVolume;

            ApplyVoxelModification(ModType, ref NewType, ref NewVolume, Type, Volume);

            VoxelFlush Flush = new VoxelFlush(Block, Block.GetChunkIdFromVoxel(vx, vy, vz), (ushort)vx, (ushort)vy, (ushort)vz, CurrentType, NewType, CurrentVolume, NewVolume, ModType);

            if (!CanModify(this, ref Flush))
            {
                return;
            }

            if (NewType == CurrentType && NewVolume == CurrentVolume)
            {
                return;
            }

            Block.Voxels.SetTypeAndVolume(vx, vy, vz, Flush.ChunkId, NewType, NewVolume);

            OnModify(this, ref Flush);
        }

        /// <summary>
        /// Override this function to create special voxel modification class. (Box,Line,Sphere,etc)
        /// </summary>
        /// <param name="Block"></param>
        /// <param name="CanModify">Function called before voxel data is modified. Return false will abord modification and OnModify will not be called</param>
        /// <param name="OnModify">Function called after voxel modification. Returned value is not used</param>
        public abstract void Execute(TerrainOperatorContainer OperatorsContainer, ITerrainContainer Container, ITerrainBlock Block, CanModifyVoxel CanModify, CanModifyVoxel OnModify);
    }
}
