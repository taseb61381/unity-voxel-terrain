﻿using System.Collections.Generic;

namespace TerrainEngine
{
    public class VoxelsOperator : ITerrainOperator
    {
        public List<VoxelPoint> Voxels = new List<VoxelPoint>();

        /// <summary>
        /// Return false, this operator will search Block inside Execute Function
        /// </summary>
        public override bool HasBlock()
        {
            return false;
        }

        public void Add(List<ComplexVoxelPoint> Points)
        {
            for (int i = 0; i < Points.Count; ++i)
                Voxels.Add(new VoxelPoint(Points[i]));
        }

        public override void Execute(TerrainOperatorContainer OperatorsContainer, ITerrainContainer Container, ITerrainBlock Block, ITerrainOperator.CanModifyVoxel CanModify, ITerrainOperator.CanModifyVoxel OnModify)
        {
            for (int i = 0; i < Voxels.Count; ++i)
            {
                Block = Container.GetBlock(Voxels[i].BlockId);
                if (Block == null || Block.HasState(TerrainBlockStates.CLOSED))
                {
                    continue;
                }

                SetVoxel(Block, Voxels[i].x, Voxels[i].y, Voxels[i].z, Voxels[i].Type, Voxels[i].Volume, Voxels[i].ModType, CanModify, OnModify);
            }
        }
    }
}
