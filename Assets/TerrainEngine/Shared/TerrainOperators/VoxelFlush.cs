﻿
using UnityEngine;

namespace TerrainEngine
{
    public struct VoxelFlush
    {
        public ITerrainBlock Block;
        public int ChunkId;
        public byte OldType, NewType;
        public ushort x, y, z;
        public float OldVolume, NewVolume;
        public VoxelModificationType ModType;

        public VoxelFlush(ITerrainBlock Block, int ChunkId, ushort x, ushort y, ushort z, byte OldType, byte NewType, VoxelModificationType ModType)
        {
            this.Block = Block;
            this.ChunkId = ChunkId;
            this.x = x;
            this.y = y;
            this.z = z;
            this.OldType = OldType;
            this.NewType = NewType;
            this.OldVolume = 0;
            this.NewVolume = 0;
            this.ModType = ModType;
        }

        public VoxelFlush(ITerrainBlock Block, int ChunkId, ushort x, ushort y, ushort z, byte OldType, byte NewType, float OldVolume, float NewVolume, VoxelModificationType ModType)
        {
            this.Block = Block;
            this.ChunkId = ChunkId;
            this.x = x;
            this.y = y;
            this.z = z; 
            this.OldType = OldType;
            this.NewType = NewType;
            this.OldVolume = OldVolume;
            this.NewVolume = NewVolume;
            this.ModType = ModType;
        }

        public void Init(ITerrainBlock Block, int ChunkId, ushort x, ushort y, ushort z, byte OldType, byte NewType, float OldVolume, float NewVolume, VoxelModificationType ModType)
        {
            this.Block = Block;
            this.ChunkId = ChunkId;
            this.x = x;
            this.y = y;
            this.z = z; 
            this.OldType = OldType;
            this.NewType = NewType;
            this.OldVolume = OldVolume;
            this.NewVolume = NewVolume;
            this.ModType = ModType;
        }

        // Unity World position.
        public Vector3 WorldPosition
        {
            get
            {
                return Block.GetWorldPosition(x, y, z, false);
            }
        }

        // Voxel world position.
        public VectorI3 WorldVoxel
        {
            get
            {
                return Block.Information.GetVoxelWorldPosition(Block.LocalPosition, x, y, z);
            }
        }

        public override string ToString()
        {
            return "Block:"+Block+",ChunkId:"+ChunkId+",OldType:"+OldType+",NewType:"+NewType+",OldVolume:"+OldVolume+",NewVolume:"+NewVolume;
        }
    }
}
