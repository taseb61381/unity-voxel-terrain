﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerrainEngine
{
    /// <summary>
    /// Generic Class to execute Terrain Operators
    /// </summary>
    public class TerrainOperatorContainer
    {
        public delegate void OnOperatorFlush(ITerrainOperator Operator);
        public Queue<ITerrainOperator> Operators = new Queue<ITerrainOperator>();
        public bool IsDirty = false;

        public int Count
        {
            get
            {
                lock (Operators)
                    return Operators.Count;
            }
        }

        public void AddOperator(ITerrainOperator Operator)
        {
            lock (Operators)
            {
                Operators.Enqueue(Operator);
            }
        }

        public virtual void Flush(ITerrainContainer Container, ITerrainOperator.CanModifyVoxel CanModify,ITerrainOperator.CanModifyVoxel OnModify,OnOperatorFlush OperatorFlush)
        {
            ITerrainOperator Operator;
            ITerrainBlock Block = null;

            lock (Operators)
            {
                while (Operators.Count != 0)
                {
                    Operator = Operators.Dequeue();
                    if (Operator == null)
                        continue;

                    if (Operator.HasBlock())
                    {
                        if (Block == null || Block.BlockId != Operator.BlockId)
                            Block = Container.BlocksArray[Operator.BlockId];

                        if (Block == null || Block.HasState(TerrainBlockStates.CLOSED) || Block.Voxels == null)
                        {
                            GeneralLog.LogError("Invalid Block : " + Operator.BlockId);
                            continue;
                        }
                    }

                    Operator.Execute(this, Container, Block, CanModify, OnModify);

                    if (OperatorFlush != null)
                        OperatorFlush(Operator);
                }

                IsDirty = false;
            }
        }
    }
}
