﻿using System;
using System.Reflection;

namespace TerrainEngine
{
    public class ITerrainOperatorNetworkManager
    {
        public ITerrainOperatorNetwork[] OperatorsNetwork;

        /// <summary>
        /// This function will load all operators created in project. All operators classes must inherit from ITerrainOperatorNetwork
        /// </summary>
        public void LoadOperators()
        {
            if (OperatorsNetwork == null)
            {
                OperatorsNetwork = new ITerrainOperatorNetwork[255];
            }

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    // Pick up a class
                    if (type.IsClass != true)
                        continue;

                    foreach (object at in type.GetCustomAttributes(typeof(TerrainOperatorAttribute), false))
                    {
                        TerrainOperatorAttribute Attr = at as TerrainOperatorAttribute;
                        if (type.IsSubclassOf(typeof(ITerrainOperatorNetwork)))
                        {
                            GeneralLog.Log("Registering OperatorNetwork :" + type + ", Id : " + Attr.OperatorId);
                            if (OperatorsNetwork[Attr.OperatorId] != null)
                                GeneralLog.LogError("OperatorNetwork already registered. OperatorId must be unique in : TerrainOperatorAttribute");
                            else
                            {
                                OperatorsNetwork[Attr.OperatorId] = Activator.CreateInstance(type) as ITerrainOperatorNetwork;
                                OperatorsNetwork[Attr.OperatorId].OperatorId = Attr.OperatorId;
                                OperatorsNetwork[Attr.OperatorId].OperatorType = Attr.OperatorType;
                            }
                        }

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Return new Operator object
        /// </summary>
        public ITerrainOperator GetNewOperator(byte Id)
        {
            if (Id >= OperatorsNetwork.Length)
            {
                GeneralLog.LogError("Invalid Operator Id :" + Id);
                return null;
            }

            if (OperatorsNetwork[Id] != null)
                return Activator.CreateInstance(OperatorsNetwork[Id].OperatorType) as ITerrainOperator;

            return null;
        }

        /// <summary>
        /// Write Operator to Packet Object. Search from all classes inheriting from ITerrainOperatorNetwork. 
        /// </summary>
        public void Write(ITerrainOperator Operator, object Packet)
        {
            if (Operator == null)
            {
                GeneralLog.LogError("Can not write Operator : null");
                return;
            }

            if (OperatorsNetwork == null)
            {
                GeneralLog.Log("No Operators. Please Call LoadOperators first");
                LoadOperators();
            }

            Type t = Operator.GetType();
            for (int i = 0; i < 255; ++i)
            {
                if (OperatorsNetwork[i] != null && OperatorsNetwork[i].OperatorType == t)
                {
                    OperatorsNetwork[i].Write(Operator, Packet);
                    return;
                }
            }

            GeneralLog.Log("OperatorNetwork not found :" + Operator);
        }

        /// <summary>
        /// Read Operator from Packet Object. Search from all classes inheriting from ITerrainOperatorNetwork. 
        /// </summary>
        public void Read(ITerrainOperator Operator, object Packet)
        {
            if (Operator == null)
            {
                GeneralLog.LogError("Can not read Operator : null");
                return;
            }
            Type t = Operator.GetType();
            for (int i = 0; i < 255; ++i)
            {
                if (OperatorsNetwork[i] != null && OperatorsNetwork[i].OperatorType == t)
                {
                    OperatorsNetwork[i].Read(Operator, Packet);
                    break;
                }
            }
        }
    }
}
