﻿using System;

namespace TerrainEngine
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TerrainOperatorAttribute : Attribute
    {
        /// <summary>
        /// Attribute for classes managing Operators network. Class must inherit from ITerrainOperatorNetwork
        /// </summary>
        /// <param name="OperatorId">Unique Operator Id</param>
        /// <param name="OperatorType">Operator type (BoxOperator,SphereOperator,etc..)</param>
        /// <param name="Override">True, this class will override any other class registered with this OperatorId</param>
        public TerrainOperatorAttribute(byte OperatorId, Type OperatorType, bool Override)
        {
            this.OperatorId = OperatorId;
            this.OperatorType = OperatorType;
            this.Override = Override;
        }

        public byte OperatorId;
        public Type OperatorType;
        public bool Override;
    }

    public abstract class ITerrainOperatorNetwork
    {
        /// <summary>
        /// Unique Operator Id, Used for network
        /// </summary>
        public byte OperatorId;

        /// <summary>
        /// Operator Type (BoxOperator,SphereOperator,Etc...)
        /// </summary>
        public Type OperatorType;

        /// <summary>
        /// Override this fuction to send Data to your Server/Client
        /// </summary>
        /// <param name="Operator">Operator to send</param>
        /// <param name="Packet">Object to use for send data to Server/Client</param>
        public virtual void Write(ITerrainOperator Operator, object Packet)
        {
        }

        /// <summary>
        /// Override this fuction to Read Data from your Server/Client
        /// </summary>
        /// <param name="Operator">Operator object to read</param>
        /// <param name="Packet">Object to use for read data from Server/Client</param>
        public virtual void Read(ITerrainOperator Operator, object Packet)
        {

        }
    }
}
