﻿using UnityEngine;

namespace TerrainEngine
{
    public class SphereOperator : ITerrainOperator
    {
        public int Radius;

        public override void Execute(TerrainOperatorContainer OperatorsContainer, ITerrainContainer Container, ITerrainBlock Block, ITerrainOperator.CanModifyVoxel CanModify, ITerrainOperator.CanModifyVoxel OnModify)
        {
            VectorI3 p = new VectorI3(x, y, z);
            int x1 = p.x - Radius;
            int y1 = p.y - Radius;
            int z1 = p.z - Radius;
            int x2 = p.x + Radius;
            int y2 = p.y + Radius;
            int z2 = p.z + Radius;
            Vector3 v;
            float bpos;
            int px, py, pz;


            for (px = x1; px < x2; ++px)
            {
                for (pz = z1; pz < z2; ++pz)
                {
                    for (py = y1; py < y2; ++py)
                    {
                        v.x = px - p.x;
                        v.y = py - p.y;
                        v.z = pz - p.z;
                        bpos = Radius - v.magnitude;
                        if (bpos > 0f)
                        {
                            SetVoxel(Block, px, py, pz, Type, bpos >= 1f ? Volume : ((bpos % 1f) * Volume), ModType, CanModify, OnModify);
                        }
                    }
                }
            }
        }
    }
}
