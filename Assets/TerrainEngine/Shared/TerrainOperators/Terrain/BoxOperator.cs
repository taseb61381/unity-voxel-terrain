﻿
namespace TerrainEngine
{
    public class BoxOperator : ITerrainOperator
    {
        public int SizeX, SizeY, SizeZ;

        public override void Execute(TerrainOperatorContainer OperatorsContainer, ITerrainContainer Container, ITerrainBlock Block, ITerrainOperator.CanModifyVoxel CanModify, ITerrainOperator.CanModifyVoxel OnModify)
        {
            int ax, ay, az;
                for (ax = 0; ax < SizeX; ++ax)
                {
                    for (az = 0; az < SizeZ; ++az)
                    {
                        for (ay = 0; ay <SizeY; ++ay)
                        {
                            SetVoxel(Block, ax + x, ay + y, az + z, Type, Volume, ModType, CanModify, OnModify);
                        }
                    }
                }
        }
    }
}
