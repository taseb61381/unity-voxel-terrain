﻿using UnityEngine;

namespace TerrainEngine
{
    public class CylinderOperator : ITerrainOperator
    {
        public int Height;
        public int Size;
        public Quaternion Rotation;

        public override void Execute(TerrainOperatorContainer OperatorsContainer, ITerrainContainer Container, ITerrainBlock Block, CanModifyVoxel CanModify, CanModifyVoxel OnModify)
        {
            VectorI3 CurrentVoxel = new VectorI3(this.x, this.y, this.z);
            int x, z, y;
            Vector2 v = new Vector2();
            Vector3 p = new Vector3();
            float bpos = 0;

            for (x = CurrentVoxel.x - Size; x <= CurrentVoxel.x + Size; ++x)
            {
                for (z = CurrentVoxel.z - Size; z <= CurrentVoxel.z + Size; ++z)
                {
                    for (y = CurrentVoxel.y; y < CurrentVoxel.y + Height; ++y)
                    {
                        v.x = x - CurrentVoxel.x;
                        v.y = z - CurrentVoxel.z;
                        bpos = (float)Size - v.magnitude;

                        if (bpos > 0f)
                        {
                            p.x = x - CurrentVoxel.x;
                            p.y = y - CurrentVoxel.y;
                            p.z = z - CurrentVoxel.z;
                            p = Rotation * p;
                            p.x += CurrentVoxel.x;
                            p.y += CurrentVoxel.y;
                            p.z += CurrentVoxel.z;
                            SetVoxel(Block, (int)p.x, (int)p.y, (int)p.z, (byte)Type, bpos >= 1f ? Volume : ((bpos % 1f) * Volume), ModType, CanModify, OnModify);
                        }
                    }
                }
            }
        }
    }
}
