﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{        
    [Flags]
    public enum TerrainBlockStates : byte
    {
        NONE = 0,
        GENERATED = 1,
        LOADED = 2,
        VISIBLE = 4,
        CLOSED = 8,
    }

    public abstract class ITerrainBlock : ITerrainObject
    {
        public VectorI3 LocalPosition;
        public VectorI3 WorldVoxel;
        public ITerrainBlock[] AroundBlocks;

        public ITerrainBlock this[int x, int y, int z]
        {
            get
            {
                return AroundBlocks[x * 9 + y * 3 + z];
            }
            set
            {
                AroundBlocks[x * 9 + y * 3 + z] = value; 
            }
        }

        public override void Init(TerrainInformation Information, VectorI3 Position, ushort UID)
        {
            base.Init(Information, Position, UID);

            if (AroundBlocks == null)
                AroundBlocks = new ITerrainBlock[27];

            for (int i = 0; i < 27; ++i)
                AroundBlocks[i] = null;

            InitPosition(Position);
        }

        public virtual void InitPosition(VectorI3 Position)
        {
            this.LocalPosition = Position;
            this.WorldPosition = Information.GetBlockWorldPosition(LocalPosition);
            this.WorldVoxel.x = (LocalPosition.x * Information.VoxelsPerAxis);
            this.WorldVoxel.y = (LocalPosition.y * Information.VoxelsPerAxis);
            this.WorldVoxel.z = (LocalPosition.z * Information.VoxelsPerAxis);
        }

        #region Voxels

        public override byte GetByte(int x, int y, int z, bool Safe = false)
        {
            if (Safe)
                return Voxels.GetType(x, y, z);

            ITerrainBlock Block = GetAroundBlock(ref x, ref y, ref z);
            if (Block == null || Block.Voxels == null)
                return 0;

            return Block.Voxels.GetType(x, y, z);
        }

        public override float GetFloat(int x, int y, int z, bool Safe = false)
        {
            if (Safe)
                return Voxels.GetVolume(x, y, z);

            ITerrainBlock Block = GetAroundBlock(ref x, ref y, ref z);
            if (Block == null || Block.Voxels == null)
                return 0;

            return Block.Voxels.GetVolume(x, y, z);
        }

        public override void SetByte(int x, int y, int z, byte Type, bool Safe = false)
        {
            if (Safe)
                Voxels.SetType(x, y, z, Type);
            else
            {
                ITerrainBlock Block = GetAroundBlock(ref x, ref y, ref z);
                if (Block == null || Block.Voxels == null)
                    return;

                Block.Voxels.SetType(x, y, z, Type);
            }
        }

        public override void SetFloat(int x, int y, int z, float Volume, bool Safe = false)
        {
            if (Safe)
                Voxels.SetVolume(x, y, z, Volume);
            else
            {
                ITerrainBlock Block = GetAroundBlock(ref x, ref y, ref z);
                if (Block == null || Block.Voxels == null)
                    return;

                Block.Voxels.SetVolume(x, y, z, Volume);
            }
        }

        public override void GetByteAndFloat(int x, int y, int z, ref byte Type, ref float Volume, bool Safe = false)
        {
            if (Safe)
            {
                Voxels.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
                return;
            }

            ITerrainBlock Block = GetAroundBlock(ref x, ref y, ref z);
            if (Block == null || (object)Block.Voxels == null)
                return;

            Block.Voxels.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
        }

        public override bool HasVolume(int x, int y, int z, bool Safe = false)
        {
            if (Safe)
                return Voxels.HasVolume(x, y, z, TerrainInformation.Isolevel) || HasRegisterVolume(x, y, z);

            ITerrainBlock Block = GetAroundBlock(ref x, ref y, ref z);
            if (Block == null || (object)Block.Voxels == null)
                return true;

            return Block.Voxels.HasVolume(x, y, z, TerrainInformation.Isolevel) || Block.HasRegisterVolume(x, y, z);
        }

        public override bool HasVolume(int x, int y, int z, float MinVolume, bool Safe = false)
        {
            if (Safe)
                return Voxels.HasVolume(x, y, z, MinVolume) || HasRegisterVolume(x, y, z);

            ITerrainBlock Block = GetAroundBlock(ref x, ref y, ref z);
            if (Block == null || (object)Block.Voxels == null)
                return true;

            return Block.Voxels.HasVolume(x, y, z, MinVolume) || Block.HasRegisterVolume(x, y, z);
        }

        /// <summary>
        /// Return true if the voxel has volume. Check from custom systems like Construction kit if the voxel is used
        /// </summary>
        public override bool HasVolume(int x, int y, int z, float MinVolume, VoxelTypes IgnoreType, bool Safe = false)
        {
            ITerrainBlock Block = this;
            if (!Safe)
            {
                Block = GetAroundBlock(ref x, ref y, ref z);
                if (Block == null || (object)Block.Voxels == null)
                    return true;
            }

            byte Type = 0;
            float Volume = 0;
            Block.Voxels.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
            if (Type != 0 && Volume >= MinVolume)
            {
                if (GetVoxelType(Type) == IgnoreType)
                    return false;

                return true;
            }

            return Block.HasRegisterVolume(x, y, z);
        }

        public override bool HasFluid(int x, int y, int z, float MinVolume, bool Safe = false)
        {
            byte Type = 0;
            float Volume = 0;
            if (Safe)
            {
                if (HasRegisterVolume(x, y, z))
                    return false;

                Voxels.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
            }
            else
            {
                ITerrainBlock Block = GetAroundBlock(ref x, ref y, ref z);
                if (Block == null || Block.HasRegisterVolume(x, y, z))
                    return false;
                Block.Voxels.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
            }

            if (Volume >= MinVolume && GetVoxelType(Type) == VoxelTypes.FLUID)
                return true;

            return false;
        }

        public override bool IsUnderGround(int x, int y, int z)
        {
            ITerrainBlock Around = GetAroundBlock(ref x, ref  y, ref z);
            if (Around == null)
                return true;

            if (y == 0 && Around.BottomBlock == null)
                return true;

            return Around.Voxels.IsUnderGround(x, y, z);
        }

        #endregion

        #region AroundBlocks

        public ITerrainBlock TopBlock
        {
            get
            {
                return AroundBlocks[16];
            }
        }
        public ITerrainBlock BottomBlock
        {
            get
            {
                return AroundBlocks[10];
            }
        }
        public ITerrainBlock LeftBlock
        {
            get
            {
                return AroundBlocks[4];
            }
        }
        public ITerrainBlock RightBlock
        {
            get
            {
                return AroundBlocks[22];
            }
        }
        public ITerrainBlock ForwardBlock
        {
            get
            {
                return AroundBlocks[14];
            }
        }
        public ITerrainBlock BackwardBlock
        {
            get
            {
                return AroundBlocks[12];
            }
        }

        public ITerrainBlock GetAroundBlock(ref int vx, ref int vy, ref int vz)
        {
            int Index = 13;
            if (vx >= Information.VoxelsPerAxis) { vx -= Information.VoxelsPerAxis; Index += 9; }
            if (vx < 0) { vx += Information.VoxelsPerAxis; Index -= 9; }

            if (vy >= Information.VoxelsPerAxis) { vy -= Information.VoxelsPerAxis; Index += 3; }
            if (vy < 0) { vy += Information.VoxelsPerAxis; Index -= 3; }

            if (vz >= Information.VoxelsPerAxis) { vz -= Information.VoxelsPerAxis; Index += 1; }
            if (vz < 0) { vz += Information.VoxelsPerAxis; Index -= 1; }

            if (Index == 13)
                return this;

            // Around Block is not null
            if (AroundBlocks[Index] != null)
                return AroundBlocks[Index].GetAroundBlock(ref vx, ref vy, ref vz);

            return null;
        }

        public ITerrainBlock GetAroundBlock(int vx, int vy, int vz)
        {
            int Index = 13;
            if (vx >= Information.VoxelsPerAxis) { vx -= Information.VoxelsPerAxis; Index += 9; }
            if (vx < 0) { vx += Information.VoxelsPerAxis; Index -= 9; }

            if (vy >= Information.VoxelsPerAxis) { vy -= Information.VoxelsPerAxis; Index += 3; }
            if (vy < 0) { vy += Information.VoxelsPerAxis; Index -= 3; }

            if (vz >= Information.VoxelsPerAxis) { vz -= Information.VoxelsPerAxis; Index += 1; }
            if (vz < 0) { vz += Information.VoxelsPerAxis; Index -= 1; }

            if (Index == 13)
                return this;

            // Around Block is not null
            if (AroundBlocks[Index] != null)
                return AroundBlocks[Index].GetAroundBlock(vx, vy, vz);

            return null;
        }

        public virtual void ClearAroundBlocks()
        {
            int x, y, z;

            ITerrainBlock Block = null;
            for (x = -1; x < 2; ++x)
                for (y = -1; y < 2; ++y)
                    for (z = -1; z < 2; ++z)
                    {
                        if (x != 0 || y != 0 || z != 0)
                        {
                            Block = this[x + 1, y + 1, z + 1];
                            if (Block != null)
                            {
                                Block[2 - (x + 1), 2 - (y + 1), 2 - (z + 1)] = null;
                                this[x + 1, y + 1, z + 1] = null;
                            }
                        }
                    }
        }

        #endregion

        #region Coords

        /// <summary>
        /// Return local voxel position from WorldPosition
        /// </summary>
        public virtual VectorI3 GetVoxelFromWorldPosition(Vector3 WorldPosition, bool Remove)
        {
            return (WorldPosition - this.WorldPosition) / Information.Scale;
        }

        /// <summary>
        /// World voxel position of an local voxel
        /// </summary>
        public int GetWorldVoxelX(int x)
        {
            return (LocalPosition.x * Information.VoxelsPerAxis) + x;
        }
        public int GetWorldVoxelY(int y)
        {
            return (LocalPosition.y * Information.VoxelsPerAxis) + y;
        }
        public int GetWorldVoxelZ(int z)
        {
            return (LocalPosition.z * Information.VoxelsPerAxis) + z;
        }

        public virtual Vector3 GetWorldPosition(VectorI3 VoxelPosition, bool AutoAlignToGround = true)
        {
            return GetWorldPosition(VoxelPosition.x, VoxelPosition.y, VoxelPosition.z, AutoAlignToGround);
        }

        public virtual Vector3 GetWorldPosition(int x, int y, int z, bool AutoAlignToGround = true)
        {
            return new Vector3((float)x * Information.Scale + WorldPosition.x, AutoAlignToGround ? GetLowerOrUpperHeight(x, y, z, 0).y : ((float)y * Information.Scale + WorldPosition.y), (float)z * Information.Scale + WorldPosition.z);
        }

        public virtual Vector3 GetTopWorldPosition(int x, int y, int z, bool AutoAlignToGround = true)
        {
            return new Vector3((float)x * Information.Scale + WorldPosition.x, AutoAlignToGround ? GetTopLowerOrUpperHeight(x, y, z, 0).y : ((float)y * Information.Scale + WorldPosition.y), (float)z * Information.Scale + WorldPosition.z);
        }

        public virtual VectorI3 GetWorldVoxel(VectorI3 CurrentVoxel)
        {
            CurrentVoxel.x = CurrentVoxel.x + (LocalPosition.x * Information.VoxelsPerAxis);
            CurrentVoxel.y = CurrentVoxel.y + (LocalPosition.y * Information.VoxelsPerAxis);
            CurrentVoxel.z = CurrentVoxel.z + (LocalPosition.z * Information.VoxelsPerAxis);
            return CurrentVoxel;
        }

        #endregion

        #region Normals

        /// <summary>
        ///  Return terran normal at given X,Y,Z coord (Used for Plants following terrain). Safe if coord have volume.
        /// </summary>
        public Vector3 GetNormal(VectorI3 Position, bool Safe = false)
        {
            return Safe ? GetNormalUpper(Position.x, Position.y, Position.z) : GetNormalLowerOrUpper(Position.x, Position.y, Position.z);
        }

        public Vector3 GetNormal(int x, int y,int z, bool Safe = false)
        {
            return Safe ? GetNormalUpper(x, y, z) : GetNormalLowerOrUpper(x, y, z);
        }

        /// <summary>
        ///  Return terran normal at given X,Y,Z coord (Used for Plants following terrain). Safe if coord have volume.
        /// </summary>
        public Vector3 GetNormal(int x, int y, int z, float Clamp, bool Safe = false)
        {
            Vector3 Normal = Safe ? GetNormalUpper(x, y, z) : GetNormalLowerOrUpper(x, y, z);
            if (Normal.x > Clamp) Normal.x = Clamp;
            else if (Normal.x < -Clamp) Normal.x = -Clamp;

            if (Normal.z > Clamp) Normal.z = Clamp;
            else if (Normal.z < -Clamp) Normal.z = -Clamp;

            Normal.y = 1f - (Normal.x*0.5f) + (Normal.z*0.5f);

            return Normal;
        }

        static public Vector3 GetNormal(float s0,float s1,float s2,float s3,float s4,float s5,float s6,float s7,float s8)
        {
            Vector3 n = new Vector3();
            n.x = - (s2 - s0 + 2 * (s5 - s3) + s8 - s6);
            n.z = - (s6 - s0 + 2 * (s7 - s1) + s8 - s2);
            n.y = 4f;
            return n.normalized;
        }

        public Vector3 GetNormalUpper(int x, int y, int z)
        {
            float s0 = GetUpperHeight(x - 1, y, z - 1, 0).y;
            float s1 = GetUpperHeight(x, y, z - 1, 0).y;
            float s2 = GetUpperHeight(x + 1, y, z - 1, 0).y;

            float s3 = GetUpperHeight(x - 1, y, z, 0).y;
            float s4 = GetUpperHeight(x, y, z, 0).y;
            float s5 = GetUpperHeight(x + 1, y, z, 0).y;

            float s6 = GetUpperHeight(x - 1, y, z + 1, 0).y;
            float s7 = GetUpperHeight(x, y, z + 1, 0).y;
            float s8 = GetUpperHeight(x + 1, y, z + 1, 0).y;

            Vector3 n = new Vector3();
            n.x = -(s2 - s0 + 2 * (s5 - s3) + s8 - s6);
            n.z = -(s6 - s0 + 2 * (s7 - s1) + s8 - s2);
            n.y = 4f;
            return n.normalized;
        }
        public Vector3 GetNormalLower(int x, int y, int z)
        {
            float s0 = GetLowerHeight(x - 1, y, z - 1, 0).y;
            float s1 = GetLowerHeight(x, y, z - 1, 0).y;
            float s2 = GetLowerHeight(x + 1, y, z - 1, 0).y;

            float s3 = GetLowerHeight(x - 1, y, z, 0).y;
            float s4 = GetLowerHeight(x, y, z, 0).y;
            float s5 = GetLowerHeight(x + 1, y, z, 0).y;

            float s6 = GetLowerHeight(x - 1, y, z + 1, 0).y;
            float s7 = GetLowerHeight(x, y, z + 1, 0).y;
            float s8 = GetLowerHeight(x + 1, y, z + 1, 0).y;

            Vector3 n = new Vector3();
            n.x = -(s2 - s0 + 2 * (s5 - s3) + s8 - s6);
            n.z = -(s6 - s0 + 2 * (s7 - s1) + s8 - s2);
            n.y = 4f;
            return n.normalized;
        }
        public Vector3 GetNormalLowerOrUpper(int x, int y, int z)
        {
            float s0 = GetLowerOrUpperHeight(x - 1, y, z - 1, 0).y;
            float s1 = GetLowerOrUpperHeight(x, y, z - 1, 0).y;
            float s2 = GetLowerOrUpperHeight(x + 1, y, z - 1, 0).y;

            float s3 = GetLowerOrUpperHeight(x - 1, y, z, 0).y;
            float s4 = GetLowerOrUpperHeight(x, y, z, 0).y;
            float s5 = GetLowerOrUpperHeight(x + 1, y, z, 0).y;

            float s6 = GetLowerOrUpperHeight(x - 1, y, z + 1, 0).y;
            float s7 = GetLowerOrUpperHeight(x, y, z + 1, 0).y;
            float s8 = GetLowerOrUpperHeight(x + 1, y, z + 1, 0).y;

            Vector3 n = new Vector3();
            n.x = -(s2 - s0 + 2 * (s5 - s3) + s8 - s6);
            n.z = -(s6 - s0 + 2 * (s7 - s1) + s8 - s2);
            n.y = 4f;
            return n.normalized;
        }

        public float GetNormalY(int x, int y, int z, bool Safe = false)
        {
            return Safe ? GetNormalUpperY(x, y, z) : GetNormalLowerOrUpperY(x, y, z);
        }

        public float GetNormalUpperY(int x, int y, int z)
        {
            bool Safe = x > 0 && x < Information.VoxelsPerAxis - 1 && y > 0 && y < Information.VoxelsPerAxis - 1 && z > 0 && z < Information.VoxelsPerAxis - 1;

            byte Type = 0;
            float s0 = GetUpperHeightY(x - 1, y, z - 1, Type,Safe);
            float s1 = GetUpperHeightY(x, y, z - 1, Type, Safe);
            float s2 = GetUpperHeightY(x + 1, y, z - 1, Type, Safe);

            float s3 = GetUpperHeightY(x - 1, y, z, Type, Safe);
            float s4 = GetUpperHeightY(x, y, z, Type, Safe);
            float s5 = GetUpperHeightY(x + 1, y, z, Type, Safe);

            float s6 = GetUpperHeightY(x - 1, y, z + 1, Type, Safe);
            float s7 = GetUpperHeightY(x, y, z + 1, Type, Safe);
            float s8 = GetUpperHeightY(x + 1, y, z + 1, Type, Safe);

            Vector3 n = new Vector3();
            n.x = -(s2 - s0 + 2 * (s5 - s3) + s8 - s6);
            n.z = -(s6 - s0 + 2 * (s7 - s1) + s8 - s2);
            n.y = 4f;
            return n.normalized.y;
        }

        public float GetNormalLowerY(int x, int y, int z)
        {
            bool Safe = x > 0 && x < Information.VoxelsPerAxis - 1 && y > 0 && y < Information.VoxelsPerAxis - 1 && z > 0 && z < Information.VoxelsPerAxis - 1;
            byte Type = 255;
            float s0 = GetLowerHeightY(x - 1, y, z - 1, Type, Safe);
            float s1 = GetLowerHeightY(x, y, z - 1, Type, Safe);
            float s2 = GetLowerHeightY(x + 1, y, z - 1, Type, Safe);

            float s3 = GetLowerHeightY(x - 1, y, z, Type, Safe);
            float s4 = GetLowerHeightY(x, y, z, Type, Safe);
            float s5 = GetLowerHeightY(x + 1, y, z, Type, Safe);

            float s6 = GetLowerHeightY(x - 1, y, z + 1, Type, Safe);
            float s7 = GetLowerHeightY(x, y, z + 1, Type, Safe);
            float s8 = GetLowerHeightY(x + 1, y, z + 1, Type, Safe);

            Vector3 n = new Vector3();
            n.x = -(s2 - s0 + 2 * (s5 - s3) + s8 - s6);
            n.z = -(s6 - s0 + 2 * (s7 - s1) + s8 - s2);
            n.y = 4f;
            return n.normalized.y;
        }
        public float GetNormalLowerOrUpperY(int x, int y, int z)
        {
            bool Safe = x > 0 && x < Information.VoxelsPerAxis - 1 && y > 0 && y < Information.VoxelsPerAxis - 1 && z > 0 && z < Information.VoxelsPerAxis - 1;

            byte Type = 255;
            float s0 = GetLowerOrUpperHeightY(x - 1, y, z - 1, Type, Safe);
            float s1 = GetLowerOrUpperHeightY(x, y, z - 1, Type, Safe);
            float s2 = GetLowerOrUpperHeightY(x + 1, y, z - 1, Type, Safe);

            float s3 = GetLowerOrUpperHeightY(x - 1, y, z, Type, Safe);
            float s4 = GetLowerOrUpperHeightY(x, y, z, Type, Safe);
            float s5 = GetLowerOrUpperHeightY(x + 1, y, z, Type, Safe);

            float s6 = GetLowerOrUpperHeightY(x - 1, y, z + 1, Type, Safe);
            float s7 = GetLowerOrUpperHeightY(x, y, z + 1, Type, Safe);
            float s8 = GetLowerOrUpperHeightY(x + 1, y, z + 1, Type, Safe);

            Vector3 n = new Vector3();
            n.x = -(s2 - s0 + 2 * (s5 - s3) + s8 - s6);
            n.z = -(s6 - s0 + 2 * (s7 - s1) + s8 - s2);
            n.y = 4f;
            return n.normalized.y;
        }

        #endregion


        #region Height

        public virtual float InterpolateHeight(byte CurrentType, float TopVolume, float CurrentVolume)
        {
            if (CurrentType != 0 && GetVoxelType(CurrentType) == VoxelTypes.FLUID)
            {
                CurrentVolume = TerrainInformation.Isolevel + CurrentVolume * 0.5f;
                TopVolume = 0;
            }

            return (Mathf.Lerp(1, 0, (TerrainInformation.Isolevel - TopVolume) / (CurrentVolume - TopVolume)) * Information.Scale);
        }

        /// <summary>
        /// Return Voxel Height
        /// Result.X = VoxelY
        /// Result.Y = WorldY
        /// Result.Z = TopVolume
        public Vector3 GetHeight(float x, float y, float z, byte Type)
        {
            ITerrainBlock Block = null;
            x -= WorldPosition.x;
            y -= WorldPosition.y;
            z -= WorldPosition.z;
            return GetLowerOrUpperHeight(ref Block, (int)(x / Information.Scale), (int)(y / Information.Scale), (int)(z / Information.Scale), ref Type, null);
        }

        /// <summary>
        /// Return Voxel Height
        /// Result.X = VoxelY
        /// Result.Y = WorldY
        /// Result.Z = TopVolume
        public Vector3 GetHeight(Vector3 Position, byte Type)
        {
            ITerrainBlock Block = null;
            Position -= WorldPosition;
            return GetLocalHeight(ref Block, Position, ref Type, null);
        }

        /// <summary>
        /// Return Voxel Height
        /// Result.X = VoxelY
        /// Result.Y = WorldY
        /// Result.Z = TopVolume
        public Vector3 GetHeight(ref ITerrainBlock Block, Vector3 Position, ref byte Type, object CustomData = null)
        {
            Position -= WorldPosition;
            return GetLocalHeight(ref Block, Position, ref Type, CustomData);
        }

        /// <summary>
        /// Return the WorldPosition with first height block. WorldPosition.y will be used to find valid block.
        /// </summary>
        public Vector3 GetUpperHeight(ref ITerrainBlock Block, Vector3 Position, ref byte Type, object CustomData = null)
        {
            Position -= WorldPosition;
            return GetLocalUpperHeight(ref Block, Position, ref Type, CustomData);
        }

        /// <summary>
        /// Return the WorldPosition with first height block. WorldPosition.y will be used to find valid block.
        /// </summary>
        public Vector3 GetLowerHeight(ref ITerrainBlock Block, Vector3 Position, ref byte Type, object CustomData = null)
        {
            Position -= WorldPosition;
            return GetLocalLowerHeight(ref Block, Position, ref Type, CustomData);
        }

        /// <summary>
        /// Return Voxel Height WorldPosition. From y -> Upper Block
        /// Result.X = VoxelY
        /// Result.Y = WorldY
        /// Result.Z = TopVolume
        /// </summary>
        public Vector3 GetUpperHeight(ref ITerrainBlock Block, int x, int y, int z, ref byte Type, object CustomData = null)
        {
            if (y < 0)
            {
                if (BottomBlock != null)
                    return BottomBlock.GetUpperHeight(ref Block, x, Information.VoxelsPerAxis + y, z, ref Type);

                y = 0;
            }

            byte TopType = 0;
            byte CurrentType = 0;
            float TopVolume = 0;
            float CurrentVolume = 0;
            Block = this;

            for (; y < Information.VoxelsPerAxis; ++y)
            {
                GetByteAndFloat(x, y, z, ref CurrentType, ref CurrentVolume, false);
                if (CurrentType == 0 || CurrentVolume < TerrainInformation.Isolevel)
                    continue;

                if (Type == 0)
                {
                    if (GetVoxelType(CurrentType) == VoxelTypes.FLUID)
                        continue;
                }
                else if (CurrentType != Type)
                    continue;

                GetByteAndFloat(x, y + 1, z, ref TopType, ref TopVolume, false);
                if (TopType != CurrentType)
                    TopVolume = 0;

                if (TopVolume < TerrainInformation.Isolevel)
                {
                    Vector3 Result = new Vector3(-1, -1, -1);
                    Type = CurrentType;
                    Result.x = y;
                    Result.y = InterpolateHeight(CurrentType, TopVolume, CurrentVolume) + (y * Information.Scale) + WorldPosition.y;
                    Result.z = TopVolume;
                    return Result;
                }
            }

            if (TopBlock != null)
                return TopBlock.GetUpperHeight(ref Block, x, y, z, ref Type);

            return new Vector3(-1, -1, -1);
        }

        /// <summary>
        /// Return Voxel Height WorldPosition. From y -> Lower Block
        /// Result.X = VoxelY
        /// Result.Y = WorldY
        /// Result.Z = TopVolume
        /// </summary>
        public Vector3 GetLowerHeight(ref ITerrainBlock Block, int x, int y, int z, ref byte Type, object CustomData = null)
        {
            if (y >= Information.VoxelsPerAxis)
            {
                if (TopBlock != null)
                    return TopBlock.GetLowerHeight(ref Block, x, y - Information.VoxelsPerAxis, z, ref Type);

                y = Information.VoxelsPerAxis - 1;
            }

            byte CurrentType = 0;
            float CurrentVolume = 0;

            byte TopType = 0;
            float TopVolume = 0;

            Block = this;

            for (; y >= 0; --y)
            {
                GetByteAndFloat(x, y, z, ref CurrentType, ref CurrentVolume, x >= 0 && z >= 0 && x < Information.VoxelsPerAxis && z < Information.VoxelsPerAxis);
                if (CurrentType == 0 || CurrentVolume < TerrainInformation.Isolevel)
                    continue;

                if (Type == 0)
                {
                    if (GetVoxelType(CurrentType) == VoxelTypes.FLUID)
                        continue;
                }
                else if (CurrentType != Type)
                    continue;

                GetByteAndFloat(x, y + 1, z, ref TopType, ref TopVolume, x >= 0 && z >= 0 && x < Information.VoxelsPerAxis && z < Information.VoxelsPerAxis && y < Information.VoxelsPerAxis - 1);
                if (TopType != CurrentType)
                    TopVolume = 0;

                if (TopVolume < TerrainInformation.Isolevel)
                {
                    Vector3 Result = new Vector3(-1, -1, -1);
                    Type = CurrentType;
                    Result.x = y;
                    Result.y = InterpolateHeight(CurrentType, TopVolume, CurrentVolume) + (y * Information.Scale) + WorldPosition.y;
                    Result.z = TopVolume;
                    return Result;
                }
            }

            if (BottomBlock != null)
                return BottomBlock.GetLowerHeight(ref Block, x, Information.VoxelsPerAxis - 1, z, ref Type);

            return new Vector3(-1, -1, -1);
        }

        /// <summary>
        /// Return Voxel Height WorldPosition. From y -> Upper Block
        /// </summary>
        public float GetUpperHeightY(int x, int y, int z, byte Type, bool Safe)
        {
            if (y < 0)
            {
                if (BottomBlock != null)
                    return BottomBlock.GetUpperHeightY(x, Information.VoxelsPerAxis + y, z, Type, Safe);

                y = 0;
            }

            byte TopType = 0;
            byte CurrentType = 0;
            float TopVolume = 0;
            float CurrentVolume = 0;

            for (; y < Information.VoxelsPerAxis; ++y)
            {
                GetByteAndFloat(x, y, z, ref CurrentType, ref CurrentVolume, Safe);
                if (CurrentType == 0 || CurrentVolume < TerrainInformation.Isolevel)
                    continue;

                if (Type == 0)
                {
                    if (GetVoxelType(CurrentType) == VoxelTypes.FLUID)
                        continue;
                }
                else if (Type != 255 && CurrentType != Type)
                    continue;

                GetByteAndFloat(x, y + 1, z, ref TopType, ref TopVolume, Safe && y < Information.VoxelsPerAxis - 1);
                if (TopType != CurrentType)
                    TopVolume = 0;

                if (TopVolume < TerrainInformation.Isolevel)
                {
                    return InterpolateHeight(CurrentType, TopVolume, CurrentVolume) + (y * Information.Scale) + WorldPosition.y;
                }
            }

            if (TopBlock != null)
                return TopBlock.GetUpperHeightY(x, y, z, Type, Safe);

            return -1f;
        }

        /// <summary>
        /// Return Voxel Height WorldPosition. From y -> Lower Block
        /// </summary>
        public float GetLowerHeightY(int x, int y, int z, byte Type,bool Safe)
        {
            if (y >= Information.VoxelsPerAxis)
            {
                if (TopBlock != null)
                    return TopBlock.GetLowerHeightY(x, y - Information.VoxelsPerAxis, z, Type,Safe);

                y = Information.VoxelsPerAxis - 1;
            }

            byte CurrentType = 0;
            float CurrentVolume = 0;

            byte TopType = 0;
            float TopVolume = 0;

            for (; y >= 0; --y)
            {
                GetByteAndFloat(x, y, z, ref CurrentType, ref CurrentVolume, Safe);
                if (CurrentType == 0 || CurrentVolume < TerrainInformation.Isolevel)
                    continue;

                if (Type == 0)
                {
                    if (GetVoxelType(CurrentType) == VoxelTypes.FLUID)
                        continue;
                }
                else if (Type != 255 && CurrentType != Type)
                    continue;

                GetByteAndFloat(x, y + 1, z, ref TopType, ref TopVolume, Safe && y < Information.VoxelsPerAxis - 1);
                if (TopType != CurrentType)
                    TopVolume = 0;

                if (TopVolume < TerrainInformation.Isolevel)
                {
                    return InterpolateHeight(CurrentType, TopVolume, CurrentVolume) + (y * Information.Scale) + WorldPosition.y;
                }
            }

            if (BottomBlock != null)
                return BottomBlock.GetLowerHeightY(x, Information.VoxelsPerAxis - 1, z, Type,Safe);

            return -1f;
        }

        public Vector3 GetLowerOrUpperHeight(ref ITerrainBlock Block, int x, int y, int z, ref byte Type, object CustomData = null)
        {
            Vector3 Result = new Vector3(-1, -1, -1);
            byte TestType = 0;
            float TestVolume = 0;
            GetByteAndFloat(x, y + 1, z, ref TestType, ref TestVolume);

            if (TestType != 0 && TestVolume >= TerrainInformation.Isolevel && GetVoxelType(TestType) != VoxelTypes.FLUID)
            {
                Result = GetUpperHeight(ref Block, x, y, z, ref Type, CustomData);
                if (Result.y == -1f)
                    return GetLowerHeight(ref Block, x, y, z, ref Type, CustomData);
                else
                    return Result;
            }

            Result = GetLowerHeight(ref Block, x, y, z, ref Type, CustomData);
            if (Result.y == -1f)
                return GetUpperHeight(ref Block, x, y, z, ref Type, CustomData);

            return Result;
        }

        public float GetLowerOrUpperHeightY(int x, int y, int z, byte Type,bool Safe)
        {
            float Result = -1f;
            byte TestType = 0;
            float TestVolume = 0;
            GetByteAndFloat(x, y + 1, z, ref TestType, ref TestVolume, Safe);

            if (TestType != 0 && TestVolume >= TerrainInformation.Isolevel && GetVoxelType(TestType) != VoxelTypes.FLUID)
            {
                Result = GetUpperHeightY(x, y, z, Type,Safe);
                if (Result == -1f)
                    return GetLowerHeightY(x, y, z, Type, Safe);
                else
                    return Result;
            }

            Result = GetLowerHeightY(x, y, z, Type, Safe);
            if (Result == -1f)
                return GetUpperHeightY(x, y, z, Type,Safe);

            return Result;
        }

        /// <summary>
        /// Return Height from Local WorldPosition. LocalWorld is position in the current Block.
        /// </summary>
        public virtual Vector3 GetLocalUpperHeight(ref ITerrainBlock Block, Vector3 Position, ref byte Type, object CustomData = null)
        {
            int x = (int)(Position.x / (float)Information.Scale);
            int z = (int)(Position.z / (float)Information.Scale);
            int y = (int)(Position.y / (float)Information.Scale);

            int xPlusOne = x + 1;
            int zPlusOne = z + 1;

            float sqX = (Position.x / (float)Information.Scale) - x;
            float sqZ = (Position.z / (float)Information.Scale) - z;

            Vector3 Result = GetUpperHeight(ref Block, x, y, z, ref Type, CustomData);

            float triZ0 = Result.y;
            float triZ1 = GetUpperHeight(ref Block, xPlusOne, y, z, ref Type, CustomData).y;
            float triZ2 = GetUpperHeight(ref Block, x, y, zPlusOne, ref Type, CustomData).y;
            float triZ3 = GetUpperHeight(ref Block, xPlusOne, y, zPlusOne, ref Type, CustomData).y;

            if (triZ0 == -1)
                triZ0 = Mathf.Max(triZ1, triZ2, triZ3);

            if (triZ1 == -1)
                triZ1 = Mathf.Max(triZ0, triZ2, triZ3);

            if (triZ2 == -1)
                triZ2 = Mathf.Max(triZ1, triZ0, triZ3);

            if (triZ3 == -1)
                triZ3 = Mathf.Max(triZ1, triZ0, triZ2);

            if ((sqX + sqZ) < 1)
            {
                Result.y = triZ0;
                Result.y += (triZ1 - triZ0) * sqX;
                Result.y += (triZ2 - triZ0) * sqZ;
            }
            else
            {
                Result.y = triZ3;
                Result.y += (triZ1 - triZ3) * (1.0f - sqZ);
                Result.y += (triZ2 - triZ3) * (1.0f - sqX);
            }

            return Result;
        }

        /// <summary>
        /// Return Height from Local WorldPosition. LocalWorld is position in the current Block.
        /// </summary>
        public virtual Vector3 GetLocalLowerHeight(ref ITerrainBlock Block, Vector3 Position, ref byte Type, object CustomData = null)
        {
            int x = (int)(Position.x / (float)Information.Scale);
            int z = (int)(Position.z / (float)Information.Scale);
            int y = (int)(Position.y / (float)Information.Scale);

            int xPlusOne = x + 1;
            int zPlusOne = z + 1;

            float sqX = (Position.x / (float)Information.Scale) - x;
            float sqZ = (Position.z / (float)Information.Scale) - z;

            Vector3 Result = GetLowerHeight(ref Block, x, y, z, ref Type, CustomData);

            float triZ0 = Result.y;
            float triZ1 = GetLowerHeight(ref Block, xPlusOne, y, z, ref Type, CustomData).y;
            float triZ2 = GetLowerHeight(ref Block, x, y, zPlusOne, ref Type, CustomData).y;
            float triZ3 = GetLowerHeight(ref Block, xPlusOne, y, zPlusOne, ref Type, CustomData).y;

            if (triZ0 == -1)
                triZ0 = Mathf.Max(triZ1, triZ2, triZ3);

            if (triZ1 == -1)
                triZ1 = Mathf.Max(triZ0, triZ2, triZ3);

            if (triZ2 == -1)
                triZ2 = Mathf.Max(triZ1, triZ0, triZ3);

            if (triZ3 == -1)
                triZ3 = Mathf.Max(triZ1, triZ0, triZ2);

            if ((sqX + sqZ) < 1)
            {
                Result.y = triZ0;
                Result.y += (triZ1 - triZ0) * sqX;
                Result.y += (triZ2 - triZ0) * sqZ;
            }
            else
            {
                Result.y = triZ3;
                Result.y += (triZ1 - triZ3) * (1.0f - sqZ);
                Result.y += (triZ2 - triZ3) * (1.0f - sqX);
            }

            return Result;
        }

        /// <summary>
        /// Return height. Select To Up or Down depending of current y.
        /// Using WorldPosition
        /// </summary>
        public virtual Vector3 GetLocalHeight(ref ITerrainBlock Block, Vector3 Position, ref byte Type, object CustomData = null)
        {
            int x = (int)(Position.x / (float)Information.Scale);
            int z = (int)(Position.z / (float)Information.Scale);
            int y = (int)(Position.y / (float)Information.Scale);

            int xPlusOne = x + 1;
            int zPlusOne = z + 1;

            float sqX = (Position.x / (float)Information.Scale) - x;
            float sqZ = (Position.z / (float)Information.Scale) - z;

            Vector3 Result = GetLowerOrUpperHeight(ref Block, x, y, z, ref Type, CustomData);
            if (Result.y == -1f)
                return Result;

            float triZ0 = Result.y;
            float triZ1 = Block.GetLowerOrUpperHeight(ref Block, xPlusOne, (int)Result.x, z, ref Type, CustomData).y;
            float triZ2 = Block.GetLowerOrUpperHeight(ref Block, x, (int)Result.x, zPlusOne, ref Type, CustomData).y;
            float triZ3 = Block.GetLowerOrUpperHeight(ref Block, xPlusOne, (int)Result.x, zPlusOne, ref Type, CustomData).y;

            if (triZ0 == -1)
                triZ0 = Mathf.Max(Mathf.Max(triZ1, triZ2), triZ3);

            if (triZ1 == -1)
                triZ1 = Mathf.Max(Mathf.Max(triZ0, triZ2), triZ3);

            if (triZ2 == -1)
                triZ2 = Mathf.Max(Mathf.Max(triZ1, triZ0), triZ3);

            if (triZ3 == -1)
                triZ3 = Mathf.Max(Mathf.Max(triZ1, triZ0), triZ2);

            if ((sqX + sqZ) < 1)
            {
                Result.y = triZ0;
                Result.y += (triZ1 - triZ0) * sqX;
                Result.y += (triZ2 - triZ0) * sqZ;
            }
            else
            {
                Result.y = triZ3;
                Result.y += (triZ1 - triZ3) * (1.0f - sqZ);
                Result.y += (triZ2 - triZ3) * (1.0f - sqX);
            }

            return Result;
        }

        public Vector3 GetLowerHeight(int x, int y, int z, byte Type)
        {
            ITerrainBlock Block = null;
            return GetLowerHeight(ref Block, x, y, z, ref Type);
        }

        public Vector3 GetUpperHeight(int x, int y, int z, byte Type)
        {
            ITerrainBlock Block = null;
            return GetUpperHeight(ref Block, x, y, z, ref Type);
        }

        public Vector3 GetLowerOrUpperHeight(int x, int y, int z, byte Type)
        {
            ITerrainBlock Block = null;
            return GetLowerOrUpperHeight(ref Block, x, y, z, ref Type);
        }

        public Vector3 GetLowerHeight(Vector3 Position, byte Type)
        {
            ITerrainBlock Block = null;
            return GetLowerHeight(ref Block, Position, ref Type);
        }

        public Vector3 GetUpperHeight(Vector3 Position, byte Type)
        {
            ITerrainBlock Block = null;
            return GetUpperHeight(ref Block, Position, ref Type);
        }

        #endregion

        #region TopHeight

        public Vector3 GetTopLowerOrUpperHeight(int x, int y, int z, byte Type)
        {
            ITerrainBlock Block = null;
            return GetLowerOrUpperHeight(ref Block, x, y, z, ref Type);
        }

        /// <summary>
        /// Return Voxel Height
        /// Result.X = VoxelY
        /// Result.Y = WorldY
        /// Result.Z = TopVolume
        public Vector3 GetTopHeight(ref ITerrainBlock Block, Vector3 Position, ref byte Type, object CustomData = null)
        {
            Position -= WorldPosition;
            return GetTopLocalHeight(ref Block, Position, ref Type, CustomData);
        }

        /// <summary>
        /// Return Voxel Height WorldPosition. From y -> Upper Block
        /// Result.X = VoxelY
        /// Result.Y = WorldY
        /// Result.Z = TopVolume
        /// </summary>
        public Vector3 GetTopUpperHeight(ref ITerrainBlock Block, int x, int y, int z, ref byte Type, object CustomData = null)
        {
            if (y < 0)
            {
                if (BottomBlock != null)
                    return BottomBlock.GetTopUpperHeight(ref Block, x, Information.VoxelsPerAxis + y, z, ref Type);

                y = 0;
            }

            byte TopType = 0;
            byte CurrentType = 0;
            float TopVolume = 0;
            float CurrentVolume = 0;
            Block = this;

            for (; y < Information.VoxelsPerAxis; ++y)
            {
                GetByteAndFloat(x, y, z, ref CurrentType, ref CurrentVolume, false);
                if (CurrentType != 0 && CurrentVolume >= TerrainInformation.Isolevel)
                    continue;

                GetByteAndFloat(x, y + 1, z, ref TopType, ref TopVolume, false);
                if (TopType == 0 || TopVolume < TerrainInformation.Isolevel || (Type == 0 && GetVoxelType(TopType) == VoxelTypes.FLUID))
                    continue;

                if (Type == 0)
                    Type = TopType;

                if (TopType != CurrentType)
                    TopVolume = 0;

                Vector3 Result = new Vector3(-1, -1, -1);
                Type = CurrentType;
                Result.x = y;
                Result.y = InterpolateHeight(CurrentType, TopVolume, CurrentVolume) + (y * Information.Scale) + WorldPosition.y;
                Result.z = TopVolume;
                return Result;
            }

            if (TopBlock != null)
                return TopBlock.GetTopUpperHeight(ref Block, x, y, z, ref Type);

            return new Vector3(-1, -1, -1);
        }

        /// <summary>
        /// Return Voxel Height WorldPosition. From y -> Lower Block
        /// Result.X = VoxelY
        /// Result.Y = WorldY
        /// Result.Z = TopVolume
        /// </summary>
        public Vector3 GetTopLowerHeight(ref ITerrainBlock Block, int x, int y, int z, ref byte Type, object CustomData = null)
        {
            if (y >= Information.VoxelsPerAxis)
            {
                if (TopBlock != null)
                    return TopBlock.GetTopLowerHeight(ref Block, x, y - Information.VoxelsPerAxis, z, ref Type);

                y = Information.VoxelsPerAxis - 1;
            }

            byte CurrentType = 0;
            float CurrentVolume = 0;

            byte TopType = 0;
            float TopVolume = 0;

            Block = this;

            for (; y >= 0; --y)
            {
                GetByteAndFloat(x, y, z, ref CurrentType, ref CurrentVolume, false);
                if (CurrentType != 0 && CurrentVolume >= TerrainInformation.Isolevel)
                    continue;

                GetByteAndFloat(x, y + 1, z, ref TopType, ref TopVolume, false);
                if (TopType == 0 || TopVolume < TerrainInformation.Isolevel || (Type == 0 && GetVoxelType(TopType) == VoxelTypes.FLUID))
                    continue;

                if (Type == 0)
                    Type = TopType;

                if (TopType != CurrentType)
                    TopVolume = 0;

                Vector3 Result = new Vector3(-1, -1, -1);
                Type = CurrentType;
                Result.x = y;
                Result.y = InterpolateHeight(CurrentType, TopVolume, CurrentVolume) + (y * Information.Scale) + WorldPosition.y;
                Result.z = TopVolume;
                return Result;
            }

            if (BottomBlock != null)
                return BottomBlock.GetTopLowerHeight(ref Block, x, Information.VoxelsPerAxis - 1, z, ref Type);

            return new Vector3(-1, -1, -1);
        }

        public Vector3 GetTopLowerOrUpperHeight(ref ITerrainBlock Block, int x, int y, int z, ref byte Type, object CustomData = null)
        {
            Vector3 Result = new Vector3(-1, -1, -1);
            byte TestType = 0;
            float TestVolume = 0;
            GetByteAndFloat(x, y + 1, z, ref TestType, ref TestVolume);

            if (TestType != 0 && TestVolume >= TerrainInformation.Isolevel && GetVoxelType(TestType) != VoxelTypes.FLUID)
            {
                Result = GetTopUpperHeight(ref Block, x, y + 1, z, ref Type, CustomData);
                if (Result.y == -1f)
                    return GetTopLowerHeight(ref Block, x, y, z, ref Type, CustomData);
            }

            Result = GetTopLowerHeight(ref Block, x, y, z, ref Type, CustomData);
            if (Result.y == -1f)
                return GetTopUpperHeight(ref Block, x, y + 1, z, ref Type, CustomData);

            return Result;
        }

        /// <summary>
        /// Return height. Select To Up or Down depending of current y.
        /// </summary>
        public virtual Vector3 GetTopLocalHeight(ref ITerrainBlock Block, Vector3 Position, ref byte Type, object CustomData = null)
        {
            int x = (int)(Position.x / (float)Information.Scale);
            int z = (int)(Position.z / (float)Information.Scale);
            int y = (int)(Position.y / (float)Information.Scale);

            int xPlusOne = x + 1;
            int zPlusOne = z + 1;

            float sqX = (Position.x / (float)Information.Scale) - x;
            float sqZ = (Position.z / (float)Information.Scale) - z;

            Vector3 Result = GetTopLowerOrUpperHeight(ref Block, x, y, z, ref Type, CustomData);
            if (Result.y == -1f)
                return Result;

            float triZ0 = Result.y;
            float triZ1 = Block.GetTopLowerOrUpperHeight(ref Block, xPlusOne, (int)Result.x, z, ref Type, CustomData).y;
            float triZ2 = Block.GetTopLowerOrUpperHeight(ref Block, x, (int)Result.x, zPlusOne, ref Type, CustomData).y;
            float triZ3 = Block.GetTopLowerOrUpperHeight(ref Block, xPlusOne, (int)Result.x, zPlusOne, ref Type, CustomData).y;

            if (triZ0 == -1)
                triZ0 = Mathf.Max(Mathf.Max(triZ1, triZ2), triZ3);

            if (triZ1 == -1)
                triZ1 = Mathf.Max(Mathf.Max(triZ0, triZ2), triZ3);

            if (triZ2 == -1)
                triZ2 = Mathf.Max(Mathf.Max(triZ1, triZ0), triZ3);

            if (triZ3 == -1)
                triZ3 = Mathf.Max(Mathf.Max(triZ1, triZ0), triZ2);

            if ((sqX + sqZ) < 1)
            {
                Result.y = triZ0;
                Result.y += (triZ1 - triZ0) * sqX;
                Result.y += (triZ2 - triZ0) * sqZ;
            }
            else
            {
                Result.y = triZ3;
                Result.y += (triZ1 - triZ3) * (1.0f - sqZ);
                Result.y += (triZ2 - triZ3) * (1.0f - sqX);
            }

            return Result;
        }

        #endregion

        /// <summary>
        /// Get offset position for spawn an object to the ground surface. Marching cubes use top and ground voxel volume to calculate the position of the Y. Cubic add 0.5f to Y, etc...
        /// </summary>
        public virtual void GetPositionOffset(ref float x, ref float y, ref float z)
        {

        }

        public override void Clear()
        {
            base.Clear();
        }

        public override string ToString()
        {
            return "Terrain-[" + (int)LocalPosition.x + "," + (int)LocalPosition.y + "," + (int)LocalPosition.z + "].bin";
        }
    }
}
