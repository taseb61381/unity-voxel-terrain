﻿using System;
using System.Xml.Serialization;
using UnityEngine;

namespace TerrainEngine
{
    [Serializable]
    public abstract class IWorldGenerator : IServerPlugin
    {
        public delegate bool HasTimeDelegate();
        public delegate void AddObjectDelegate(BiomeData Biome, ICustomTerrainData Data, int x, int y, int z, int StartX, int StartZ, IObjectBaseInformations Info);
        public delegate bool GenerateDelegate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ);


        [Serializable]
        public enum GeneratorTypes : byte
        {
            TERRAIN = 0, // Generators that will modify terrain height/voxels
            CUSTOM = 1, // Generators that will modify voxels terrain after height is generated
            GROUND = 2, // Generators that only need to know where ground points exist (grass,details,trees,etc)
            MAX = 3,
            NONE = 3,
        }

        /// <summary>
        /// Unique Generator ID. Used for network optimization
        /// </summary>
        [XmlIgnore]
        public byte UID;

        [XmlIgnore]
        public WorldGenerator World;

        [XmlIgnore]
        public TerrainInformation Information;

        [XmlIgnore]
        public VoxelInformation[] Palette;

        public string Name;

        public string DataName;

        public int Priority = 0;

        public string ClassName
        {
            get
            {
                return GetType().Name;
            }
        }

        public string FileName
        {
            get
            {
                return ClassName + "-" + DataName;
            }
        }

        public virtual void Init()
        {

        }

        public virtual byte GetVoxelID(string Name)
        {
            Name = Name.ToLower();
            for (int i = 0; i < Palette.Length; ++i)
            {
                if (Palette[i].Name.ToLower() == Name)
                    return (byte)i;
            }

            GeneralLog.Log("Can not find voxel in palette :" + Name);
            return 0;
        }

        //-------------------------- Handlers --------------------------//

        public virtual GeneratorTypes GetGeneratorType()
        {
            return GeneratorTypes.TERRAIN;
        }

        public virtual bool IsCallingCustomGenerators()
        {
            return false;
        }

        public virtual bool IsUsingGroundPoints()
        {
            return false;
        }

        public struct SelecterContainer
        {
            public BiomeData Biome;
            public ICustomTerrainData Data;
            public NoiseObjectsContainer Container;
            public TerrainBlockServer BlockServer;
            public TerrainDatas Block;
        }

        public void GenerateObjects(ActionThread Thread, ref SelecterContainer SC, int ChunkId, int StartX, int StartZ,
            Vector2 Density, Vector2 TopDensity, AddObjectDelegate OnAdd)
        {
            SC.Biome.GetBiomes();
            int[] NoiseGround = SC.Biome.GetGrounds(SC.Block.Information.VoxelPerChunk, SC.Block.Information.VoxelPerChunk);

            int Groundlevel = (int)(SC.Block.Information.WorldGroundLevel / SC.Block.Information.Scale);
            SC.Biome.Random.InitSeed(ChunkId+1);

            int WorldY = SC.Block.WorldY;
            int LocalWorldY;
            GroundPoint Point;

            if (ThreadManager.EnableStats)
                Thread.RecordTime();

            byte Side = 0;
            if (SC.Biome.GroundPoints.Count != 0)
            {
                for (int i = SC.Biome.GroundPoints.Count - 1; i >= 0; --i)
                {
                    Side = SC.Biome.GroundPoints.array[i].Side;

                    if (Side == 0 && !SC.Container.HasSide0) continue;
                    if (Side == 1 && !SC.Container.HasSide1) continue;
                    if (Side == 2 && !SC.Container.HasSide2) continue;

                    Point.x = SC.Biome.GroundPoints.array[i].x;
                    Point.z = SC.Biome.GroundPoints.array[i].z;

                    SC.Biome.LoadBiomeReso(Point.x, Point.z);

                    if (SC.Container.IsValid(SC.Biome, SC.Biome, (int)Density.x, (int)Density.y) != 0)
                    {
                        continue;
                    }

                    Point.y = SC.Biome.GroundPoints.array[i].y;
                    Point.Type = SC.Biome.GroundPoints.array[i].Type;
                    Point.Side = Side;

                    LocalWorldY = SC.Block.WorldY + Point.y;

                    SelectObjectToApply(Thread, ref SC, Point.Type, Side != 2 ? VoxelTypes.OPAQUE : VoxelTypes.FLUID, Point.x + StartX, Point.y, Point.z + StartZ, StartX, StartZ,
                        LocalWorldY - Groundlevel, // Height
                        LocalWorldY - NoiseGround[SC.Biome.GetBiomeIndex(Point.x, Point.z)], // Depth
                        HeightSides.BOTTOM, OnAdd);
                }
            }

            if (ThreadManager.EnableStats)
                Thread.AddStats("GenerateGroundObjects", Thread.Time());
        }

        static public bool SelectObjectToApply(ActionThread Thread, ref SelecterContainer SC, byte Type, VoxelTypes VoxelType,
            int x, int y, int z, int StartX, int StartZ, int Height, int Depth, HeightSides Side, AddObjectDelegate OnAdd)
        {

            int TotalChance = 0;
            int Chance = 0;
            int NoiseCount = SC.Container.Count;
            int i = 0, j = 0;

            SC.Biome.SelectedObjects.ClearFast();
            VectorI3 ObjInfo = new VectorI3();

            NoiseObjects Objs;
            for (i = 0; i < NoiseCount; ++i)
            {
                if (SC.Container.NoisesObjects[i].IsValid(SC.Biome) == 0)
                {
                    Objs = SC.Container.NoisesObjects[i];
                    for (j = 0; j < Objs.Objects.Count; ++j)
                    {
                        if (Objs.Objects[j].IsValid(SC.Biome, Height, Depth, SC.Block, x, y, z, Type, VoxelType, Side) == 0)
                        {
                            ObjInfo.x = i;
                            ObjInfo.y = j;
                            ObjInfo.z = Objs.Objects[j].Object.Chance;
                            SC.Biome.SelectedObjects.Add(ObjInfo);
                            TotalChance += ObjInfo.z;
                        }
                    }
                }
            }

            if (TotalChance == 0)
                return false;

            Chance = (int)(((1f+SC.Biome.Random.randomFloat())*0.5f) * (float)TotalChance);
            TotalChance = 0;

            IObjectBaseInformations Info = null;
            NoiseCount = SC.Biome.SelectedObjects.Count;
            float Slope = 0;
            for (i = 0; i < NoiseCount; ++i)
            {
                 ObjInfo.z = SC.Biome.SelectedObjects.array[i].z;

                if (Chance >= TotalChance && Chance < TotalChance + ObjInfo.z)
                {
                    ObjInfo.x = SC.Biome.SelectedObjects.array[i].x;
                    ObjInfo.y = SC.Biome.SelectedObjects.array[i].y;

                    Info = SC.Container.NoisesObjects[ObjInfo.x].Objects[ObjInfo.y].Object;

                    if (Info.SlopeLimits.x != Info.SlopeLimits.y)
                    {
                        Slope = SC.BlockServer.GetNormalLowerOrUpperY(x, y, z);
                        if (Slope < Info.SlopeLimits.x || Slope > Info.SlopeLimits.y)
                            break;
                    }
                    OnAdd(SC.Biome, SC.Data, x, y, z, StartX, StartZ, Info);
                    return true;
                }

                TotalChance += ObjInfo.z;
            }
            return false;
        }

        // Add grass without checking if grass exist at this position, it's not necessary because X,Y,Z are not iterated few times
        public void OnAdd(BiomeData Biome, ICustomTerrainData Data, int x, int y, int z, int StartX, int StartZ, IObjectBaseInformations Info)
        {
            Biome.SimpleObjects.Add(new BiomeData.SimpleTempObject() { Id = (byte)(Info.ObjectId + 1), x = (ushort)x, y = (ushort)y, z = (ushort)z });
        }
    }
}
