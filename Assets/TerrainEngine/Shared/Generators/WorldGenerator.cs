﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TerrainEngine
{
    /// <summary>
    /// This class contains all Generators that will generate the world. Used for all systems like terrain,grass,details,etc..
    /// </summary>
    [Serializable]
    public class WorldGenerator
    {
        [Serializable]
        public struct GeneratorInformation
        {
            public string FileName;
            public string ClassName;
        }

        public WorldGenerator()
        {

        }

        public TerrainInformation Information;
        public VoxelInformation[] Palette;

        public virtual VoxelTypes GetType(byte Type)
        {
            return Palette[Type].VoxelType;
        }

        public virtual byte GetVoxelID(string Name)
        {
            Name = Name.ToLower();
            for (int i = 0; i < Palette.Length; ++i)
            {
                if (Palette[i].Name.ToLower() == Name)
                    return (byte)i;
            }

            GeneralLog.Log("Can not find voxel in palette :" + Name);
            return 0;
        }

        public List<GeneratorInformation> GeneratorsFiles = new List<GeneratorInformation>();

        [XmlIgnore]
        public List<IWorldGenerator> Generators = new List<IWorldGenerator>();

        [XmlIgnore]
        public IWorldGenerator[][] WorldGenerators;

        [XmlIgnore]
        public Dictionary<string, Type> GeneratorsTypes = new Dictionary<string, Type>();

        static public int PriorityComparer(IWorldGenerator a, IWorldGenerator b)
        {
            if (a.Priority > b.Priority)
                return 1;
            if (a.Priority < b.Priority)
                return -1;
            return 0;
        }

        public int GetGeneratorsCount(IWorldGenerator.GeneratorTypes Type)
        {
            if (WorldGenerators[(int)Type] != null)
                return WorldGenerators[(int)Type].Length;

            return 0;
        }

        public void SortGenerators()
        {
            Generators.Sort(PriorityComparer);
        }

        public void AddGenerator(IWorldGenerator Gen, bool AddToFile = true)
        {
            Gen.World = this;
            Gen.Information = Information;
            Gen.Palette = Palette;
            Generators.Add(Gen);

            byte GenType = (byte)Gen.GetGeneratorType();

            GeneralLog.Log("WorldGenerator : Add " + Gen + " : " + Gen.Priority + ",Type:" + (IWorldGenerator.GeneratorTypes)GenType);

            if (GenType < (int)IWorldGenerator.GeneratorTypes.MAX)
            {
                if (WorldGenerators == null)
                    WorldGenerators = new IWorldGenerator[(int)IWorldGenerator.GeneratorTypes.MAX][];

                if (WorldGenerators[GenType] == null)
                    WorldGenerators[GenType] = new IWorldGenerator[1];
                else
                    Array.Resize(ref WorldGenerators[GenType], WorldGenerators[GenType].Length + 1);

                WorldGenerators[GenType][WorldGenerators[GenType].Length - 1] = Gen;
            }

            if (AddToFile)
            {
                GeneratorInformation Info = new GeneratorInformation();
                Info.ClassName = Gen.ClassName;
                Info.FileName = Gen.FileName;
                GeneratorsFiles.Add(Info);
            }

            Gen.Init();
            Gen.UID = (byte)Generators.Count;
        }

        public string ExportToXML()
        {
            if(GeneralLog.DrawLogs)
                GeneralLog.Log("WorldGenerator : Exporting Biomes to XML Files");

            string DirectoryPath = Directory.GetCurrentDirectory();
            string Folder = DirectoryPath + "\\" + Information.FolderName + "\\";
            Directory.Delete(Folder,true);
            Directory.CreateDirectory(Folder);

            foreach (IWorldGenerator Gen in Generators)
            {
                XmlMgr.SaveXMLFile(Folder + Gen.FileName + ".xml", Gen);

            }

            XmlMgr.SaveXMLFile(Folder +"WorldGenerator.xml", this);

            return Folder;
        }

        //-------------------------- Handlers --------------------------//

        public void OnTerrainDataCreate(TerrainDatas Data)
        {
            for (int i = 0; i < Generators.Count; ++i)
                Generators[i].OnTerrainDataCreate(Data);
        }

        public void OnTerrainDataClose(TerrainDatas Data)
        {
            for (int i = 0; i < Generators.Count; ++i)
                Generators[i].OnTerrainDataClose(Data);
        }

        public void OnServerStart(TerrainWorldServer Server)
        {
            for (int i = 0; i < Generators.Count; ++i)
                Generators[i].OnServerStart(Server);
        }

        public void OnServerStop(TerrainWorldServer Server)
        {
            for (int i = 0; i < Generators.Count; ++i)
                Generators[i].OnServerStop(Server);
        }

        public void OnServerUpdate(ActionThread Thread, TerrainWorldServer Server)
        {
            for (int i = 0; i < Generators.Count; ++i)
                Generators[i].OnServerUpdate(Thread, Server);
        }
    }
}
