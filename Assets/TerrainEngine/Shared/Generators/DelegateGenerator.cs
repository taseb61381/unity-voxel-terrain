﻿
namespace TerrainEngine
{
    /// <summary>
    /// Generator used by IBlockProcessor that doesn"t contains a specific generator. Used only client side
    /// </summary>
    public class DelegateGenerator : IWorldGenerator
    {
        public IWorldGenerator.GenerateDelegate GenerateFunction;
        public GeneratorTypes GeneratorType = GeneratorTypes.TERRAIN;

        public DelegateGenerator(IWorldGenerator.GenerateDelegate Delegate)
        {
            this.GenerateFunction = Delegate;
        }

        public override IWorldGenerator.GeneratorTypes GetGeneratorType()
        {
            return GeneratorType;
        }

        public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
        {
            return GenerateFunction(Thread, Parent, BlockServer, Block, Data, ChunkId, ChunkX, ChunkZ);
        }

        public override string ToString()
        {
            return GenerateFunction != null ? GenerateFunction.ToString() : base.ToString();
        }
    }

}
