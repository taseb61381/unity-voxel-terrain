﻿

namespace TerrainEngine
{
    static public class BlockManager
    {
        static public VoxelInformation[] Voxels;
        static public byte None = 0;
        static public byte Grass = 1;
        static public byte Dirt = 2;
        static public byte Stone = 3;
        static public byte MantleStone = 4;
        static public byte Water = 5;
        static public byte Snow = 6;
        static public byte Sand = 7;
        static public byte Ice = 8;
        static public byte Leaves = 9;
        static public byte Flore = 10;
        static public byte Wood = 11;
        static public byte Air = 0;

        static public byte GetDataType(string Name)
        {
            for (byte i = 0; i < Voxels.Length; ++i)
                if (Voxels[i].Name == Name)
                    return i;

            return 0;
        }

        static public void Init(VoxelInformation[] Voxels)
        {
            BlockManager.Voxels = Voxels;
            Grass = GetDataType("Grass");
            Dirt = GetDataType("Dirt");
            Stone = GetDataType("Stone");
            MantleStone = GetDataType("MantleStone");
            Water = GetDataType("Water");
            Snow = GetDataType("Snow");
            Sand = GetDataType("Sand");
            Ice = GetDataType("Ice");
            Leaves = GetDataType("Leaves");
            Flore = GetDataType("Flore");
            Wood = GetDataType("Wood");
            Air = GetDataType("Air");
        }

        static public byte getBlock(string BlockName)
        {
            switch (BlockName)
            {
                case "Grass":
                    return Grass;

                case "Dirt":
                    return Dirt;

                case "Stone":
                    return Stone;

                case "MantleStone":
                    return MantleStone;

                case "Water":
                    return Water;

                case "Snow":
                    return Snow;

                case "Sand":
                    return Sand;

                case "Ice":
                    return Ice;

                case "Leaves":
                    return Leaves;

                default:
                    return GetDataType(BlockName);
            }
        }
    }
}