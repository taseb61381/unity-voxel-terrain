﻿using UnityEngine;
using System.Collections.Generic;

namespace TerrainEngine
{
    public abstract class ITerrainObject : IPoolable
    {
        public ushort BlockId;
        public Vector3 WorldPosition;
        public TerrainInformation Information;
        public TerrainDatas Voxels;
        public TerrainBlockStates State;
        public Vector3 CenterPosition
        {
            get
            {
                return WorldPosition + Information.WorldBlockSize;
            }
        }

        public virtual void Init(TerrainInformation Information, VectorI3 Position, ushort UID)
        {
            this.BlockId = UID;
            this.Information = Information;
            SetState(TerrainBlockStates.GENERATED, false);
            SetState(TerrainBlockStates.CLOSED, false);
        }

        #region States

        public void SetState(TerrainBlockStates NewState, bool Value)
        {
            if (Value)
                State |= NewState;
            else
                State &= ~NewState;
        }

        public bool IsValid()
        {
            return !HasState(TerrainBlockStates.CLOSED);
        }

        public bool HasState(TerrainBlockStates NewState)
        {
            return (State & NewState) == NewState;
        }

        #endregion

        #region Voxels

        public abstract VoxelTypes GetVoxelType(byte Id);

        public virtual int GetChunkId(int ChunkX, int ChunkY, int ChunkZ)
        {
            return ChunkZ + ChunkY * Information.ChunkPerBlock + ChunkX * Information.ChunkPerBlock * Information.ChunkPerBlock;
        }

        public virtual int GetChunkIdFromVoxel(int vx, int vy, int vz)
        {
            return (vz / Information.VoxelPerChunk) + (vy / Information.VoxelPerChunk) * Information.ChunkPerBlock + (vx / Information.VoxelPerChunk) * Information.ChunkPerBlock * Information.ChunkPerBlock;
        }

        public virtual byte GetByte(int x, int y, int z, bool Safe = false)
        {
            if (Safe)
                return Voxels.GetType(x, y, z);

            return 0;
        }

        public virtual float GetFloat(int x, int y, int z, bool Safe = false)
        {
            if (Safe)
                return Voxels.GetVolume(x, y, z);

            return 0f;
        }

        public virtual void SetByte(int x, int y, int z, byte Type, bool Safe = false)
        {
            if (Safe)
                Voxels.SetType(x, y, z, Type);
        }

        public virtual void SetFloat(int x, int y, int z, float Volume, bool Safe = false)
        {
            if (Safe)
                Voxels.SetVolume(x, y, z, Volume);
        }

        public virtual void GetByteAndFloat(int x, int y, int z, ref byte Type, ref float Volume, bool Safe = false)
        {
            if (Safe)
            {
                Voxels.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
                return;
            }

            Volume = 0f;
            Type = 0;
        }

        public virtual bool HasVolume(int x, int y, int z, bool Safe = false)
        {
            if (Safe)
                return Voxels.HasVolume(x, y, z, TerrainInformation.Isolevel) || HasRegisterVolume(x, y, z);

            return false;
        }

        public virtual bool HasVolume(int x, int y, int z, float MinVolume, bool Safe = false)
        {
            if (Safe)
                return Voxels.HasVolume(x, y, z, MinVolume) || HasRegisterVolume(x, y, z);

            return false;
        }

        /// <summary>
        /// Return true if the voxel has volume. Check from custom systems like Construction kit if the voxel is used
        /// </summary>
        public virtual bool HasVolume(int x, int y, int z, float MinVolume, VoxelTypes IgnoreType, bool Safe = false)
        {
            if (!Safe)
            {
                return false;
            }

            byte Type = 0;
            float Volume = 0;
            Voxels.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
            if (Type != 0 && Volume >= MinVolume)
            {
                if (GetVoxelType(Type) == IgnoreType)
                    return false;

                return true;
            }

            return false;
        }

        public virtual bool HasFluid(int x, int y, int z, float MinVolume, bool Safe = false)
        {
            byte Type = 0;
            float Volume = 0;
            if (Safe)
            {
                if (HasRegisterVolume(x, y, z))
                    return false;

                Voxels.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
            }

            return false;
        }

        public virtual bool IsUnderGround(int x, int y, int z)
        {
            return false;
        }

        #region HasVolume Functions

        public delegate bool HasVolumeDelegate(int x, int y, int z);

        public List<HasVolumeDelegate> HasVolumeFunctions = new List<HasVolumeDelegate>();

        public virtual void RegisterHasVolume(HasVolumeDelegate Del)
        {
            HasVolumeFunctions.Add(Del);
        }

        public virtual void RemoveHasVolume(HasVolumeDelegate Del)
        {
            HasVolumeFunctions.Remove(Del);
        }

        public virtual bool HasRegisterVolume(int x, int y, int z)
        {
            for (int i = 0; i < HasVolumeFunctions.Count; ++i)
                if (HasVolumeFunctions[i](x, y, z))
                    return true;

            return false;
        }

        #endregion

        #endregion

        public override void Clear()
        {
            SetState(TerrainBlockStates.GENERATED, false);
            SetState(TerrainBlockStates.CLOSED, true);
        }
    }
}
