﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(MarchingPalette))]
public class MarchingPaletteInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Separator();
        EditorGUILayout.HelpBox("Array must contains 2 elements Minimum.\nFirst Element must always be NONE and Empty.\nNONE Name must not be used on other elements.", MessageType.Info);
    }
}