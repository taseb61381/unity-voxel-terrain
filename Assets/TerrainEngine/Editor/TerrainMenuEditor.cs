using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class TerrainMenuEditor
{

    #region TerrainObject

    [MenuItem("TerrainEngine/TerrainObject/Setup Physics Object Scripts")]
    static public void InitMainPhysicsObject()
    {
        if (Selection.activeGameObject == null)
        {
            Debug.LogError("No GameObject Selected in scene. Please select an object. Terrain will load around this object");
            return;
        }

        TerrainObjectColliderViewer ColliderViewer;

        if (Selection.activeGameObject.GetComponent<TerrainObject>() == null)
            Selection.activeGameObject.AddComponent<TerrainObject>();

        if ((ColliderViewer = Selection.activeGameObject.GetComponent<TerrainObjectColliderViewer>()) == null)
            ColliderViewer = Selection.activeGameObject.AddComponent<TerrainObjectColliderViewer>();

        if (Selection.activeGameObject.GetComponent<TerrainObjectGroundAligner>() == null)
            Selection.activeGameObject.AddComponent<TerrainObjectGroundAligner>();

        CharacterController Controller = Selection.activeGameObject.GetComponent<CharacterController>();
        Rigidbody Rigid = Selection.activeGameObject.GetComponent<Rigidbody>();

        if(Controller != null && Controller.enabled)
        {
            Controller.enabled = false;
            Debug.Log("CharacterController found. Script disabled and added to active list on ColliderViewer. This script will be enabled when TerrainCollider are generated");
            if(ColliderViewer.OnGroundColliderGenerated.ColliderToInvertState == null)
                ColliderViewer.OnGroundColliderGenerated.ColliderToInvertState = new List<Collider>();

            ColliderViewer.OnGroundColliderGenerated.ColliderToInvertState.Add(Controller);
        }

        if(Rigid != null && Rigid.useGravity)
        {
            Rigid.useGravity = false;
            Debug.Log("Rigidbody found. Script disabled and added to active list on ColliderViewer. This script will be enabled when TerrainCollider are generated");
            if(ColliderViewer.OnGroundColliderGenerated.GravityToIntertState == null)
                ColliderViewer.OnGroundColliderGenerated.GravityToIntertState = new List<Rigidbody>();

            ColliderViewer.OnGroundColliderGenerated.GravityToIntertState.Add(Rigid);
        }


         Debug.Log(Selection.activeGameObject + " Physics Inited. Added TerrainObject,TerrainObjectColliderViewe andTerrainObjectGroundAligner scripts.");

    }

    [MenuItem("TerrainEngine/TerrainObject/Load Terrain around this object")]
    static public void SetMainObject()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        if (Selection.activeGameObject == null)
        {
            Debug.LogError("No GameObject Selected in scene. Please select an object. Terrain will load around this object");
            return;
        }

        LocalTerrainObjectServerScript Script = GameObject.FindObjectOfType(typeof(LocalTerrainObjectServerScript)) as LocalTerrainObjectServerScript;
        if (Script == null)
            Script = Manager.gameObject.AddComponent<LocalTerrainObjectServerScript>();

        Script.CTransform = Selection.activeGameObject.transform;
        Selection.activeGameObject = Script.gameObject;
        Debug.Log("Terrain will be loaded around : " + Script.gameObject+", you can fixe the position by modifying 'FixedPositions' values on LocalTerrainObjectServerScript script");
    }

    [MenuItem("TerrainEngine/TerrainObject/OnLocalTerrainLoaded/Invert State Object (Active Or Disable)")]
    static public void OnLocalTerrainLoadedInvertState()
    {
        CreateFirstLoadingProcessor();

        OnFirstLoadingProcessor Processor = GameObject.FindObjectOfType(typeof(OnFirstLoadingProcessor)) as OnFirstLoadingProcessor;
        if (Processor == null)
        {
            Debug.LogError("No OnFirstLoadingProcessor in scene.");
            return;
        }

        if (Selection.activeGameObject == null)
        {
            Debug.LogError("No GameObject Selected in scene. Please select an object. Terrain will load around this object");
            return;
        }

        TerrainObject TObject = Selection.activeGameObject.GetComponent<TerrainObject>();
        if(TObject == null)
        {
            Debug.Log("No TerrainObject script on :" + TObject + ",Added");
            TObject = Selection.activeGameObject.AddComponent<TerrainObject>();
        }

        if (Processor.OnLocalTerrainLoaded == null)
            Processor.OnLocalTerrainLoaded = new List<OnFirstLoadingProcessor.LocalTerrainLoadedEvent>();

        for (int i = 0; i < Processor.OnLocalTerrainLoaded.Count; ++i)
        {
            if (Processor.OnLocalTerrainLoaded[i].Object == TObject)
            {
                Debug.LogError("Object already on activate list.");
                return;
            }
        }

        Processor.OnLocalTerrainLoaded.Add(new OnFirstLoadingProcessor.LocalTerrainLoadedEvent(TObject));
        EditorUtility.SetDirty(Processor);

        Debug.Log(Selection.activeGameObject +" state will be inverted after local terrain loading (if gameobject is active, it will be disabled)");
    }

    [MenuItem("TerrainEngine/TerrainObject/OnLocalTerrainLoaded/Destroy when any LocalTerrain is called")]
    static public void OnLocalTerrainLoadedDestroy()
    {
        CreateFirstLoadingProcessor();

        OnFirstLoadingProcessor Processor = GameObject.FindObjectOfType(typeof(OnFirstLoadingProcessor)) as OnFirstLoadingProcessor;
        if (Processor == null)
        {
            Debug.LogError("No OnFirstLoadingProcessor in scene.");
            return;
        }

        if (Selection.activeGameObject == null)
        {
            Debug.LogError("No GameObject Selected in scene. Please select an object. Terrain will load around this object");
            return;
        }

        if (Processor.OnLocalTerrainLoaded == null)
            Processor.OnLocalTerrainLoaded = new List<OnFirstLoadingProcessor.LocalTerrainLoadedEvent>();

        for (int i = 0; i < Processor.OnLocalTerrainLoaded.Count; ++i)
        {
            if (Processor.OnLocalTerrainLoaded[i].Object != null)
            {
                if (Processor.OnLocalTerrainLoaded[i].OnTerrainLoaded == null)
                    Processor.OnLocalTerrainLoaded[i].OnTerrainLoaded = new ActiveDisableContainer();

                if (Processor.OnLocalTerrainLoaded[i].OnTerrainLoaded.Objects == null)
                    Processor.OnLocalTerrainLoaded[i].OnTerrainLoaded.Objects = new List<ActiveDisableInfo>();

                ActiveDisableInfo Info = new ActiveDisableInfo();
                Info.DestroyObject = Selection.activeGameObject;
                Processor.OnLocalTerrainLoaded[i].OnTerrainLoaded.Objects.Add(Info);
                EditorUtility.SetDirty(Processor);

                Debug.Log(Selection.activeGameObject + " will be destroyed after local terrain loading is called on object:" + Processor.OnLocalTerrainLoaded[i].OnTerrainLoaded.Objects);
                return;
            }
        }

        Debug.LogError("No Object called on LocalTerrainLoaded (FirstLoadingProcessor). This object can not be destroyed");
    }

    [MenuItem("TerrainEngine/TerrainObject/OnCompleteTerrainLoaded/Invert State Object (Active Or Disable)")]
    static public void OnCompleteTerrainLoadedInvertState()
    {
        CreateFirstLoadingProcessor();

        OnFirstLoadingProcessor Processor = GameObject.FindObjectOfType(typeof(OnFirstLoadingProcessor)) as OnFirstLoadingProcessor;
        if (Processor == null)
        {
            Debug.LogError("No OnFirstLoadingProcessor in scene.");
            return;
        }

        if (Selection.activeGameObject == null)
        {
            Debug.LogError("No GameObject Selected in scene. Please select an object. Terrain will load around this object");
            return;
        }

        if (Processor.OnCompleteTerrainLoaded == null)
            Processor.OnCompleteTerrainLoaded = new ActiveDisableContainer();

        if (Processor.OnCompleteTerrainLoaded.GoToInvertState == null)
            Processor.OnCompleteTerrainLoaded.GoToInvertState = new List<GameObject>();

        for (int i = 0; i < Processor.OnCompleteTerrainLoaded.GoToInvertState.Count; ++i)
        {
            if(Processor.OnCompleteTerrainLoaded.GoToInvertState[i] == Selection.activeGameObject)
            {
                Debug.LogError(Selection.activeGameObject + " already on InvertState list");
                return;
            }
        }

        Processor.OnCompleteTerrainLoaded.GoToInvertState.Add(Selection.activeGameObject);
        Debug.Log(Selection.activeGameObject + " state will be inverted after terrain is completely loaded (if gameobject is active, it will be disabled)");
    }

    [MenuItem("TerrainEngine/TerrainObject/OnLoadingStart/Invert State Object (Active Or Disable)")]
    static public void OnLoadingStartInvertState()
    {
        CreateFirstLoadingProcessor();

        OnFirstLoadingProcessor Processor = GameObject.FindObjectOfType(typeof(OnFirstLoadingProcessor)) as OnFirstLoadingProcessor;
        if (Processor == null)
        {
            Debug.LogError("No OnFirstLoadingProcessor in scene.");
            return;
        }

        if (Selection.activeGameObject == null)
        {
            Debug.LogError("No GameObject Selected in scene. Please select an object. Terrain will load around this object");
            return;
        }

        TerrainObject TObject = Selection.activeGameObject.GetComponent<TerrainObject>();
        if (TObject == null)
        {
            Debug.Log("No TerrainObject script on :" + TObject + ",Added");
            TObject = Selection.activeGameObject.AddComponent<TerrainObject>();
        }

        if (Processor.OnLocalTerrainLoaded == null)
            Processor.OnLocalTerrainLoaded = new List<OnFirstLoadingProcessor.LocalTerrainLoadedEvent>();

        for (int i = 0; i < Processor.OnLocalTerrainLoaded.Count; ++i)
        {
            if (Processor.OnLocalTerrainLoaded[i].Object == TObject)
            {
                Debug.LogError("Object already on activate list.");
                return;
            }
        }

        Processor.OnLocalTerrainLoaded.Add(new OnFirstLoadingProcessor.LocalTerrainLoadedEvent(TObject));
        EditorUtility.SetDirty(Processor);

        Debug.Log(Selection.activeGameObject + " state will be inverted after terrain loading (if gameobject is active, it will be disabled)");
    }

    [MenuItem("TerrainEngine/Biome/Export To XML")]
    static public void ExportBiomeXML()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager != null)
        {
            WorldGenerator Gen = Manager.InitGenerators(true); // Initing Generator class before serialization

            Debug.Log("Exporting Biomes : " + Manager + ",Generators=" + Gen.Generators.Count + ",Files=" + Gen.GeneratorsFiles.Count);
            ShowExplorer(Gen.ExportToXML()); // Open folder
        }
        else
            Debug.Log("TerrainManager not found. Create or Enable a Terrain");
    }

    static public void ShowExplorer(string itemPath)
    {
        itemPath = itemPath.Replace(@"/", @"\");   // explorer doesn't like front slashes
        System.Diagnostics.Process.Start("explorer.exe", "/select," + itemPath);
    }

    static public FileMgr CheckObjects()
    {
        FileMgr File = SceneView.FindObjectOfType(typeof(FileMgr)) as FileMgr;
        ActionProcessor Processor = SceneView.FindObjectOfType(typeof(ActionProcessor)) as ActionProcessor;

        if (File == null)
        {
            GameObject Obj = new GameObject();
            File = Obj.AddComponent<FileMgr>();
        }

        if (Processor == null)
        {
            GameObject Obj = new GameObject();
            Processor = Obj.AddComponent<ActionProcessor>();
            Processor.EnablePooling = true;
            Obj.name = "2-ActionProcessor";
        }

        File.name = "1-FileMgr";
        Processor.name = "2-ActionProcessor";

        return File;
    }

    #endregion

    [MenuItem("TerrainEngine/Processors/5-FirstLoadingProcessor")]
    static public void CreateFirstLoadingProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        OnFirstLoadingProcessor Obj = GameObject.FindObjectOfType(typeof(OnFirstLoadingProcessor)) as OnFirstLoadingProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<OnFirstLoadingProcessor>();

        Obj.name = "5-FirstLoadingProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 5;
    }

    [MenuItem("TerrainEngine/Processors/9-ColliderProcessor")]
    static public void CreateColliderProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        ColliderProcessor Obj = GameObject.FindObjectOfType(typeof(ColliderProcessor)) as ColliderProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<ColliderProcessor>();

        Obj.name = "9-ColliderProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 9;
    }

    [MenuItem("TerrainEngine/Processors/11-UndoRedoProcessor")]
    static public void CreateUndoRedoProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        UndoRedoProcessor Obj = GameObject.FindObjectOfType(typeof(UndoRedoProcessor)) as UndoRedoProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<UndoRedoProcessor>();

        Obj.name = "11-UndoRedoProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 11;
    }


    #region

    [MenuItem("TerrainEngine/Processors/Debug/20-DebugGUIProcessor")]
    static public void CreateDebugGUIProcessor()
    {
        TerrainManager Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;
        if (Manager == null)
        {
            Debug.LogError("No TerrainManager in scene.");
            return;
        }

        DebugGUIProcessor Obj = GameObject.FindObjectOfType(typeof(DebugGUIProcessor)) as DebugGUIProcessor;
        if (Obj == null)
            Obj = new GameObject().AddComponent<DebugGUIProcessor>();

        Obj.name = "20-DebugGUIProcessor";
        Obj.transform.parent = Manager.transform;
        Obj.Priority = 20;
    }

    #endregion

}
