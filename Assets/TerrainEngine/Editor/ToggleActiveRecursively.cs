using UnityEditor;
using UnityEngine;

public class ToggleActiveRecursively : ScriptableObject
{
    [MenuItem("Custom/Toggle Active And Send Recursively %i")]
    static void DoToggle()
    {
        GameObject activeGO = Selection.activeGameObject;

        activeGO.SetActiveRecursively(!activeGO.active);
    }
}