using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class TessellationMaterialCreator : EditorWindow
{
    [Serializable]
    public struct TextureInfo
    {
        public Texture2D Texture;

        public bool IsReadable()
        {
            try
            {
                if (Texture != null)
                    Texture.GetPixel(0, 0);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool IsRGBA()
        {
            if (Texture == null)
                return true;

            return Texture.format == TextureFormat.RGBA32;
        }


        public bool IsValid()
        {
            return IsReadable() && IsRGBA();
        }
        

        public bool Draw()
        {
            bool IsInvalid = true;

            if (!IsReadable())
                GUI.color = Color.red;
            else if (!IsRGBA())
                GUI.color = Color.blue;

            IsInvalid = GUI.color != Color.white;
            Texture = EditorGUILayout.ObjectField(Texture, typeof(Texture2D), false, GUILayout.Height(60)) as Texture2D;
            GUI.color = Color.white;
            return IsInvalid;
        }
    }

    [Serializable]
    public class MaterialInfo
    {
        public string Name = "NewMaterial";
        public Material Mat;
        public TextureInfo[] RGBA = new TextureInfo[4]; // Diffuse
        public TextureInfo[] ORGBA = new TextureInfo[4]; // Occlusion
        public TextureInfo[] HRGBA = new TextureInfo[4]; // HeighMap;
        public TextureInfo Wall, OWall, HWall; // Wall Texture

        bool Opened = false;

        public bool IsValidMaterial()
        {
            if (Mat == null)
                return false;

            if (!Mat.HasProperty("_ColorR") || !Mat.HasProperty("_ColorG") || !Mat.HasProperty("_ColorB") || !Mat.HasProperty("_ColorA"))
                return false;

            return true;
        }

        public bool IsValidTextures()
        {
            for (int i = 0; i < 4; ++i)
                if (!RGBA[i].IsValid())
                    return false;

            for (int i = 0; i < 4; ++i)
                if (!ORGBA[i].IsValid())
                    return false;

            for (int i = 0; i < 4; ++i)
                if (!HRGBA[i].IsValid())
                    return false;

            return true;
        }
        public void Draw(List<Texture2D> InvalidTextures)
        {
            Opened = EditorGUILayout.Foldout(Opened, Name);
            if (Opened)
            {
                Name = EditorGUILayout.TextField("Name:", Name);

                if (Mat == null)
                    GUI.color = Color.red;
                Mat = EditorGUILayout.ObjectField(Mat, typeof(Material), false) as Material;
                GUI.color = Color.white;

                if (!IsValidMaterial())
                {
                    EditorGUILayout.HelpBox("No material selected. Material folder will be used to save generated textures.Shader must be Triplanar Tessellation.", MessageType.Error);
                    return;
                }

                EditorGUILayout.LabelField("Folder:" + Path.GetDirectoryName(AssetDatabase.GetAssetPath(Mat.GetInstanceID())));

                EditorGUILayout.LabelField("Diffuse : R G B A");
                EditorGUILayout.BeginHorizontal("box");
                for (int i = 0; i < 4;++i)
                    if (RGBA[i].Draw()) InvalidTextures.Add(RGBA[i].Texture);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.LabelField("Occlusion : R G B A");
                EditorGUILayout.BeginHorizontal("box");
                for (int i = 0; i < 4; ++i)
                    if (ORGBA[i].Draw()) InvalidTextures.Add(ORGBA[i].Texture);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.LabelField("HeightMap : R G B A");
                EditorGUILayout.BeginHorizontal("box");
                for (int i = 0; i < 4; ++i)
                    if (HRGBA[i].Draw()) InvalidTextures.Add(HRGBA[i].Texture);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.LabelField("Wall : Diffuse,Occlusion,HeightMap");
                EditorGUILayout.BeginHorizontal("box");
                if (Wall.Draw()) InvalidTextures.Add(Wall.Texture);
                if (OWall.Draw()) InvalidTextures.Add(OWall.Texture);
                if (HWall.Draw()) InvalidTextures.Add(HWall.Texture);
                EditorGUILayout.EndHorizontal();
            }
        }

        public Texture2D CombineOcclusion(TextureInfo A,TextureInfo Occlusion)
        {
            Texture2D Text = new Texture2D(A.Texture.width, A.Texture.height, TextureFormat.RGBA32, true);
            Color[] ACol = A.Texture.GetPixels();
            Color[] BCol = Occlusion.Texture.GetPixels();

            for(int i=0;i<ACol.Length;++i)
            {
                ACol[i].a = BCol[i].r;
            }

            Text.SetPixels(ACol);
            Text.Apply();
            return Text;
        }

        public Texture2D CombineHeightMap(TextureInfo A,TextureInfo B,TextureInfo C,TextureInfo D)
        {
            Texture2D Text = new Texture2D(A.Texture.width, A.Texture.height, TextureFormat.RGBA32, true);
            Color[] ACol = A.Texture.GetPixels();
            Color[] BCol = B.Texture.GetPixels();
            Color[] CCol = C.Texture.GetPixels();
            Color[] DCol = D.Texture.GetPixels();

            for (int i = 0; i < ACol.Length; ++i)
            {
                ACol[i].g = BCol[i].r;
                ACol[i].b = CCol[i].r;
                ACol[i].a = DCol[i].r;
            }

            Text.SetPixels(ACol);
            Text.Apply();
            return Text;
        }

        public string[] RGBALetters = new string[] { "R", "G", "B", "A" };

        public void Combine()
        {
            if (!IsValidMaterial())
            {
                Debug.LogError("Can not combine textures. Material Invalid");
                return;
            }

            if(!IsValidTextures())
            {
                Debug.LogError("Can not combine textures. Textures are not readable or format is not RGBA32");
                return;
            }

            string AssetFolder = Path.GetDirectoryName(AssetDatabase.GetAssetPath(Mat.GetInstanceID()));
            

            for(int i=0;i<4;++i)
            {
                Texture2D Diffuse = CombineOcclusion(RGBA[i], ORGBA[i]);
                File.WriteAllBytes(AssetFolder + "/Diffuse" + i + ".png", Diffuse.EncodeToPNG());

                Mat.SetTexture("_Color" + RGBALetters[i], (Texture2D)AssetDatabase.LoadAssetAtPath(AssetFolder + "/Diffuse" + i + ".png", typeof(Texture2D)));
            }

            Texture2D HeightMap = CombineHeightMap(HRGBA[0],HRGBA[1],HRGBA[2],HRGBA[3]);
            File.WriteAllBytes(AssetFolder + "/HeightMap.png", HeightMap.EncodeToPNG());
            Mat.SetTexture("_HeightMap", (Texture2D)AssetDatabase.LoadAssetAtPath(AssetFolder + "/HeightMap.png", typeof(Texture2D)));

            Texture2D W = CombineOcclusion(Wall, OWall);
            File.WriteAllBytes(AssetFolder + "/Wall.png", W.EncodeToPNG());
            Mat.SetTexture("_Wall", (Texture2D)AssetDatabase.LoadAssetAtPath(AssetFolder + "/Wall.png", typeof(Texture2D)));

            File.WriteAllBytes(AssetFolder + "/WallHeightMap.png", HWall.Texture.EncodeToPNG());
            Mat.SetTexture("_WallHeightMap", (Texture2D)AssetDatabase.LoadAssetAtPath(AssetFolder + "/WallHeightMap.png", typeof(Texture2D)));

            
            // As we are saving to the asset folder, tell Unity to scan for modified or new assets
            AssetDatabase.Refresh();
        }
    }

    public List<MaterialInfo> Materials = new List<MaterialInfo>();

    public Vector2 Scroll;

    [MenuItem("TerrainEngine/Shader/Tessellation Material Creator")]
    public static void ShowWindow()
    {
        var window = GetWindow<TessellationMaterialCreator>();
        window.Show();
    }

    List<Texture2D> InvalidTextures = new List<Texture2D>();

    public void OnGUI()
    {
        if (InvalidTextures == null)
            InvalidTextures = new List<Texture2D>();

        InvalidTextures.Clear();

        EditorGUILayout.BeginVertical("box");
            EditorGUILayout.BeginHorizontal();
            GUI.color = Color.red;
            EditorGUILayout.ObjectField(null, typeof(Texture2D), false);
            EditorGUILayout.LabelField(" = Texture not readable");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUI.color = Color.blue;
            EditorGUILayout.ObjectField(null, typeof(Texture2D), false);
            EditorGUILayout.LabelField(" = Texture format not RGBA32");
            EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        GUI.color = Color.white;

        Scroll = GUILayout.BeginScrollView(Scroll);

        for (int i = 0; i < Materials.Count; ++i)
        {
            EditorGUILayout.BeginVertical("box");
            Materials[i].Draw(InvalidTextures);
            if (GUILayout.Button("Combine"))
                Materials[i].Combine();

            if (GUILayout.Button("Delete"))
                Materials.RemoveAt(i);
            EditorGUILayout.EndVertical();
        }

        if (GUILayout.Button("Select Invalid Textures : " + InvalidTextures.Count))
            Selection.objects = InvalidTextures.ToArray();

        if (GUILayout.Button("Create"))
            Materials.Add(new MaterialInfo());

        GUILayout.EndScrollView();
    }
}
