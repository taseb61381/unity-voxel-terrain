﻿using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(TransvoxelManager))]
public class TransvoxelManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        TransvoxelManager Manager = target as TransvoxelManager;
        if (Manager.TransvoxelInfo.NodeLevels == null)
            Manager.TransvoxelInfo.NodeLevels = new TerrainEngine.NodeLevelInformation();

        int NodeSize = (int)Manager.Informations.VoxelPerChunkXYZ;
        int TreeSize = (int)Manager.Informations.VoxelPerChunkXYZ * (int)Manager.Informations.ChunkPerBlockXYZ;
        Manager.TransvoxelInfo.NodeLevels.SetMaxLevel(TreeSize, NodeSize);

        Manager.TransvoxelInfo.NodeLevels.GetDistances(TreeSize, NodeSize, (int Level, float Distance, int Power) =>
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label("Lod:" + Level,GUILayout.Width(50));
                int NewPower =  (int)GUILayout.HorizontalSlider(Power, 0, 10);

                GUILayout.Label("Meters:" + Distance * Manager.Informations.Scale +" ("+Power+")");
                EditorGUILayout.EndHorizontal();

                if (NewPower != Power)
                {
                    EditorUtility.SetDirty(target);
                    EditorApplication.MarkSceneDirty();
                }
                return NewPower;
            });
    }
}
