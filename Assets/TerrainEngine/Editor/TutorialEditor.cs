﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEditor;
using UnityEngine;

public class TutorialEditor : EditorWindow
{
    [MenuItem("TerrainEngine/Tutorial")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(TutorialEditor));
    }


    public int StepId = 0;
    public Vector2 Scroll = new Vector2();

    public void OnGUI()
    {
        Scroll = GUILayout.BeginScrollView(Scroll);

        TerrainManager Manager = FindObjectOfType<TerrainManager>();

        SetColor(Manager == null ? 0 : 1);
        GUILayout.BeginVertical("0 - Create a new Terrain", GUI.skin.box);
        {
            GUILayout.Label("");
            GUILayout.Label("Select your terrain type on toolbar : [TerrainEngine/Terrains/*Type*/New Terrain]");
            GUILayout.Label("Example : [TerrainEngine/Terrains/Transvoxel/New Terrain]");
            if (Manager != null && Manager.ChunkPrefab == null)
            {
                DisplayError("TerrainManager script do not contains a ChunkPrefab. You must link a gameObject that contains MeshFilder,MeshRenderer and MeshCollider");
            }

            if (Manager is VoxelManager)
            {
                if ((Manager as VoxelManager).WorldTextureShader == null)
                    DisplayError("TerrainManager : Select a shader to use for textures");

                if ((Manager as VoxelManager).WorldTextureShaderAlpha == null)
                    DisplayError("TerrainManager : Select a shader to use for textures using alpha");
            }
        }
        GUILayout.EndVertical();
        ResetColors();

        if (Manager == null)
        {
            GUILayout.EndScrollView();
            return;
        }

        IVoxelPalette Palette = FindObjectOfType<IVoxelPalette>();

        SetColor(Palette == null ? 0 : 1);
        GUILayout.BeginVertical("1 - Create voxels materials", GUI.skin.box);
        {
            GUILayout.Label("");
            GUILayout.Label("Add your voxels materials on Palette.");
            GUILayout.Label("Or import an existing palette prefab");
            GUILayout.Label("Example : 3-DefaultPalettePrefab on folder Extra/TerrainEngine/Assets/Terrain Textures");
            if (GUILayout.Button("Select current Palette"))
                Selection.activeGameObject = Palette.gameObject;
            if (Manager.Palette == null)
            {
                DisplayError("Your current palette is not linked on TerrainManager script, 'Palette' variable.");
            }
            if (Palette != null && Palette.GetDataCount() <= 1)
            {
                DisplayError("Your palette do not contains voxels. Please select your palette and add a new voxel type.");
            }
        }
        GUILayout.EndVertical();
        ResetColors();

        AdvancedHeightMap HeightMap = FindObjectOfType<AdvancedHeightMap>();
        UnityTerrainConverterProcessor Converter = FindObjectOfType<UnityTerrainConverterProcessor>();
        IBlockProcessor[] Processors = FindObjectsOfType<IBlockProcessor>();
        bool HasProcessor = Processors.Length != 0;

        SetColor(HasProcessor ? 1 : 0);
        GUILayout.BeginVertical("2 - Add a terrain generator", GUI.skin.box);
        {
            GUILayout.Label("");
            GUILayout.Label("Add a new processor that will generate the terrain.");

            SetColor(HeightMap != null ? 1 : 0, false);
            GUILayout.Label("Example: [TerrainManager/Processors/0-HeightMap]");
            SetColor(LastColor);
            GUILayout.Label("-- This processors will create a procedural terrain");

            SetColor(HeightMap != null ? 1 : 0, false);
            GUILayout.Label("Example: [TerrainManager/Processors/12-ImportUnityTerrain]");
            SetColor(LastColor);
            GUILayout.Label("-- This processors will convert an existing unity terrain on your scene.\nYou must select the terrain first");
        }
        GUILayout.EndVertical();
        ResetColors();

        SetColor(Camera.main != null && Camera.main.GetComponent<TerrainObject>() != null ? 1 : 0);
        GUILayout.BeginVertical("3 - Init your camera/player", GUI.skin.box);
        {
            GUILayout.Label("");
            GUILayout.Label("All cameras must have a TerrainObject.cs");
            GUILayout.Label("'TerrainObject.cs' : This script will update the position of the gameobject (Current Chunk,Block,Voxel)");
            GUILayout.Label("'TerrainObjectDetailViewer.cs' : This script will update around details (rocks,plants,etc...)(Used by DetailProcessor)");
            GUILayout.Label("'TerrainObjectGrassViewer.cs' : This script will update around grass (Used by GrassProcessor)");
            GUILayout.Label("You can use the menu : [TerrainManager/TerrainObject/Setup Main Camera]");
        }
        GUILayout.EndVertical();
        ResetColors();

        GUILayout.BeginVertical("4 - Load Terrain Around your player", GUI.skin.box);
        {
            GUILayout.Label("");
            GUILayout.Label("You must select an object that will be the main object. Terrain will load around it");
            GUILayout.Label("'LocalTerrainObjectServerScript.cs' : This script will send the position to the local server.");
            GUILayout.Label("Select your Main Object and use the menu : [TerrainManager/TerrainObject/Load Terrain Around this object]");
            GUILayout.Label("LocalTerrainObjectServer script will be added to TerrainManager gameobject. Your object can be disabled this script will still load the terrain.\n--Used when you need to load the terrain first and enable your player after");
            if (GUILayout.Button("Load Terrain around " + GetSelectedObject()))
            {
                TerrainMenuEditor.SetMainObject();
            }
        }
        GUILayout.EndVertical();
        ResetColors();

        OnFirstLoadingProcessor LoadingProcessor = FindObjectOfType<OnFirstLoadingProcessor>();

        GUILayout.BeginVertical("5 - First Loading", GUI.skin.box);
        {
            GUILayout.Label("");
            GUILayout.Label("Terrain loading can take some times after the game is started. You must enable/disable some objects after the loading (Game systems, characters,etc..)");
            GUILayout.Label("You can use the processor : 'OnFirstProcessorLoading.cs' to enabled gameobjects/components/colliders");
            GUILayout.Label("You can use the menu : [TerrainEngine/Processors/5-FirstLoadingProcessor]");
            if (GUILayout.Button("Add FirstLoadingProcessor"))
            {
                TerrainMenuEditor.CreateFirstLoadingProcessor();
            }

            GUILayout.Label("Now you can drag and drop your objects into the FirsLoadingProcessor script.");
            GUILayout.Label("OnLoadingStart : This array will contains scripts to enable/disable when terrain start to load. Example: for loading screen");
            GUILayout.Label("OnFirstLoading : This array will contains Objects to enable/disable when all terrains are loaded. Example: for IA/");
            GUILayout.Label("OnLocalTerrainLoaded : Here you can place gameobjects to enable/disable when the terrain is loaded localy. GameObject must contains TerrainObject.cs\n--Used for enable player even if all terrains are not loaded.");

            SetColor(LoadingProcessor == null ? 0 : 1);
            if (GUILayout.Button("Enable/Disable " + GetSelectedObject() + " when Terrain Loading Start"))
            {
                if (Selection.activeGameObject != null)
                {
                    Debug.Log(Selection.activeGameObject + " GameObject state will be inversed after terrain loading start :" + Selection.activeGameObject.active);
                    LoadingProcessor.OnLoadingStart.GoToInvertState.Add(Selection.activeGameObject);
                }
                else
                    Debug.LogError("No GameObject Selected");
            }
            if (GUILayout.Button("Enable/Disable " + GetSelectedObject() + " when Terrain is completely loaded"))
            {
                if (Selection.activeGameObject != null)
                {
                    Debug.Log(Selection.activeGameObject + " GameObject state will be inversed after terrain load :" + Selection.activeGameObject.active);

                    if (LoadingProcessor.OnCompleteTerrainLoaded.GoToInvertState == null)
                        LoadingProcessor.OnCompleteTerrainLoaded.GoToInvertState = new List<GameObject>();

                    LoadingProcessor.OnCompleteTerrainLoaded.GoToInvertState.Add(Selection.activeGameObject);
                }
                else
                    Debug.LogError("No GameObject Selected");
            }
            if (GUILayout.Button("Enable/Disable " + GetSelectedObject() + " when Terrain is localy loaded"))
            {
                if (Selection.activeGameObject != null)
                {
                    if (Selection.activeGameObject.GetComponent<TerrainObject>() == null)
                    {
                        Debug.Log("No TerrainObject script on " + Selection.activeGameObject + ", Adding Script");
                        Selection.activeGameObject.AddComponent<TerrainObject>();
                    }
                    Debug.Log(Selection.activeGameObject + " GameObject state will be inversed when terrain will be loaded around his position :" + Selection.activeGameObject.active);
                    LoadingProcessor.OnLocalTerrainLoaded.Add(new OnFirstLoadingProcessor.LocalTerrainLoadedEvent(Selection.activeGameObject.GetComponent<TerrainObject>()));
                }
                else
                    Debug.LogError("No GameObject Selected.");
            }
            SetColor(LastColor);
        }
        GUILayout.EndVertical();
        ResetColors();

        GUILayout.BeginVertical("6 - Colliders", GUI.skin.box);
        {
            GUILayout.Label("");

            GUILayout.Label("All objects using terrain colliders must contains a 'TerrainObjectColliderViewer.cs'.\n--This script will update colliders around the object");
            GUILayout.Label("When you use colliders with the terrain (example with your characters), you must enable gravity/movements after the terrain is loaded.");
            GUILayout.Label("'TerrainObjectColliderViewer' contains a value : OnGroundColliderGenerated");
            GUILayout.Label("Drag and drop your components/gameobjects to modify their states when colliders under this object will be generated");
            GUILayout.Label("For procedural terrain, if you don't know the terrain height at your player position, you must add a 'TerrainObjectGroundAligner.cs' to your object.\n--This script will teleport the gameobject to the ground when the terrain will be loaded.");
            
            if (GUILayout.Button("Add 'TerrainObjectColliderViewer' to " + GetSelectedObject()))
            {
                if (Selection.activeGameObject == null)
                {
                    Debug.LogError("No GameObject Selected.");
                }
                else
                {
                    if (Selection.activeGameObject.GetComponent<TerrainObjectColliderViewer>())
                    {
                        Debug.Log(Selection.activeGameObject + " Added TerrainObjectColliderViewer script");
                        Selection.activeGameObject.AddComponent<TerrainObjectColliderViewer>();
                    }
                }
            }

            if (GUILayout.Button("Add 'TerrainObjectGroundAligner' to " + GetSelectedObject()))
            {
                if (Selection.activeGameObject == null)
                {
                    Debug.LogError("No GameObject Selected.");
                }
                else
                {
                    if (Selection.activeGameObject.GetComponent<TerrainObjectGroundAligner>())
                    {
                        Debug.Log(Selection.activeGameObject + " Added TerrainObjectGroundAligner script");
                        Selection.activeGameObject.AddComponent<TerrainObjectGroundAligner>();
                    }
                }
            }
        } 
        GUILayout.EndVertical();
        ResetColors();

        GUILayout.BeginVertical("7 - Networking", GUI.skin.box);
        {
            GUILayout.Label("");

            GUILayout.Label("-- All examples are made on folder 'C:/TerrainEngine/'");
            GUILayout.Label("1) Build the server scene located : 'TerrainEngine/Example/Network'");
            GUILayout.Label("2) Open the client scene (Example CompleteWorld): 'Extra/TerrainEngine/Scenes/Biomes/CompleteWorld'");
            GUILayout.Label("3) Export Biomes Files on your server folder");
            GUILayout.Label("4) Start your server");
        }
        GUILayout.EndVertical();
        ResetColors();

        GUILayout.EndScrollView();

    }

    public string GetSelectedObject()
    {
        if (Selection.activeGameObject == null)
            return "[No Object Selected]";
        else
            return "'" + Selection.activeGameObject.name + "'";
    }

    public void ResetColors()
    {
        GUI.backgroundColor = Color.white;
        GUI.color = Color.white;
    }

    public void DisplayError(string Text)
    {
        EditorGUILayout.HelpBox(Text, MessageType.Error);
    }

    public int LastColor = 0;
    public int CurrentColor = 0;
    public void SetColor(int Value, bool Background =true)
    {
        if (Background)
        {
            if (Value == 0)
                GUI.backgroundColor = Color.red;
            else if (Value == 1)
                GUI.backgroundColor = Color.green;
            else
                GUI.backgroundColor = Color.white;
        }
        else
        {
            if (Value == 0)
                GUI.color = Color.red;
            else if (Value == 1)
                GUI.color = Color.green;
            else
                GUI.color = Color.white;
        }

        LastColor = CurrentColor;
        CurrentColor = Value;
    }
}
