﻿using System;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;

namespace UnityEditor
{
    internal class StandardTriplanarGUI : ShaderGUI
    {
        public struct PropertyInformation
        {
            public GUIContent Content;
            public string PName;
            public MaterialProperty Prop;
            public byte PType; // 0 Texture,1 Float, 2 Vector
            public string GroupName;
            public int MaxCount;

            public void Draw(MaterialEditor materialEditor, Material mat)
            {
                if (PType == 0)
                    materialEditor.TexturePropertySingleLine(Content, Prop);
                else
                    materialEditor.DefaultShaderProperty(Prop, Content.text + Content.tooltip);
            }
        }

        public List<PropertyInformation> Infos;

        public void AddProperty(string Name, string Tooltip, string PName, bool IsTexture, bool IsVector, MaterialProperty[] props,string GroupName,int MaxCount = -1)
        {
            byte PType = 0;

            if (IsTexture) PType = 0;
            else if (IsVector) PType = 2;
            else PType = 1;

            Infos.Add(new PropertyInformation()
            {
                Content = new UnityEngine.GUIContent(Name, Tooltip),
                PName = PName,
                PType = PType,
                Prop = FindProperty(PName, props),
                GroupName = GroupName,
                MaxCount = MaxCount
            });
        }

        public void InitProperties(MaterialProperty[] props)
        {
            if (Infos != null && Infos.Count != 0)
                return;

            if (Infos == null)
                Infos = new List<PropertyInformation>();

            AddProperty("FloorR", "(Occlusion A)", "_FloorR", true, false, props, "Textures", 1);
            AddProperty("BumpR", "BumpR Texture", "_BumpR", true, false, props, "Textures", 0);

            AddProperty("FloorG", "(Occlusion A)", "_FloorG", true, false, props, "Textures", 1);
            AddProperty("BumpG", "BumpG Texture", "_BumpG", true, false, props, "Textures", 0);

            AddProperty("FloorB", "(Occlusion A)", "_FloorB", true, false, props, "Textures", 1);
            AddProperty("BumpB", "BumpB Texture", "_BumpB", true, false, props, "Textures", 0);

            AddProperty("FloorA", "(Occlusion A)", "_FloorA", true, false, props, "Textures", 1);
            AddProperty("BumpA", "BumpA Texture", "_BumpA", true, false, props, "Textures", 0);

            AddProperty("Wall", "", "_Wall", true, false, props, "Textures", 1);
            AddProperty("WallBump", "", "_WallBump", true, false, props, "Textures", 0);

            AddProperty("Texture Power", "", "_TexPower", false, false, props, "Base");
            AddProperty("Floor Power", "", "_FloorPower", false, false, props, "Base");

            AddProperty("DataR", "(Scale/Smoothness/Metalic/Bump)", "_DataR", false, true, props, "Base");
            AddProperty("DataG", "(Scale/Smoothness/Metalic/Bump)", "_DataG", false, true, props, "Base");
            AddProperty("DataB", "(Scale/Smoothness/Metalic/Bump)", "_DataB", false, true, props, "Base");
            AddProperty("DataA", "(Scale/Smoothness/Metalic/Bump)", "_DataA", false, true, props, "Base");
            AddProperty("DataWall", "(Scale/Smoothness/Metalic/Bump)", "_DataWall", false, true, props, "Base");


            AddProperty("FloorScale", "", "_FloorScaleD", false, false, props, "Distant");
            AddProperty("WallScale", "", "_WallScaleD", false, false, props, "Distant");
            AddProperty("Bump Powers", "(Floor,FloorD,Wall,WallD)", "_DataBumpD", false, false, props, "Distant");
            AddProperty("TransitionStart", "", "_TransitionStartD", false, false, props, "Distant");
            AddProperty("TransitionScale", "", "_TransitionScaleD", false, false, props, "Distant");

            AddProperty("Floor Noise Power", "", "_TexPowerN", false, false, props, "Noise");
            AddProperty("DataNoise", "(Scale/Offset/Pow)", "_DataN", false, true, props, "Noise");

            AddProperty("FloorR Noise", "(Occlusion A)", "_FloorRN", true, false, props, "Noise",1);
            AddProperty("BumpR Noise", "(Occlusion A)", "_BumpRN", true, false, props, "Noise", 0);
            AddProperty("DataRN", "(Scale/Smoothness/Metalic/Bump)", "_DataRN", false, true, props, "Noise");
            AddProperty("WallN", "(Occlusion A)", "_WallN", true, false, props, "Noise");
            AddProperty("WallBumpN", "(Occlusion A)", "_WallBumpN", true, false, props, "Noise");

            AddProperty("Vertical Texture", "", "_TextureV", true, false, props, "Vertical", 1);
            AddProperty("Vertical Bump", "", "_BumpV", true, false, props, "Vertical", 0);
            AddProperty("Vertical Power", "", "_PowerV", false, false, props, "Vertical");
            AddProperty("DataV", "(Scale/Smoothness/Metalic/Bump)", "_DataV", false, true, props, "Vertical");

            AddProperty("Pertubations", "(Scale1/Power1/Scale2/Power2)", "_PerturbationsV", false, true, props, "Vertical");
            AddProperty("DataNV", "(VertPower/VertOffset/NoisePower/NoiseOffset)", "_DataNV", false, true, props, "Vertical");

            AddProperty("FloorColorR", "", "_FloorColorR", false, false, props, "Color");
            AddProperty("FloorColorG", "", "_FloorColorG", false, false, props, "Color");
            AddProperty("FloorColorB", "", "_FloorColorB", false, false, props, "Color");
            AddProperty("FloorColorA", "", "_FloorColorA", false, false, props, "Color");
            AddProperty("WallColor", "", "_WallColor", false, false, props, "Color");
            AddProperty("FloorColorN", "", "_FloorColorN", false, false, props, "Color");

            AddProperty("_ColorPowerR", "", "_ColorPowerR", false, false, props, "ColorPower");
            AddProperty("_ColorPowerG", "", "_ColorPowerG", false, false, props, "ColorPower");
            AddProperty("_ColorPowerB", "", "_ColorPowerB", false, false, props, "ColorPower");
            AddProperty("_ColorPowerA", "", "_ColorPowerA", false, false, props, "ColorPower");
            AddProperty("_ColorPowerWall", "", "_ColorPowerWall", false, false, props, "ColorPower");
            AddProperty("_ColorPowerN", "", "_ColorPowerN", false, false, props, "ColorPower");
            AddProperty("_ColorPowerRN", "", "_ColorPowerRN", false, false, props, "ColorPower");
            AddProperty("_ColorPowerV", "", "_ColorPowerV", false, false, props, "ColorPower");
        }

        public bool DrawKeyword(Material material, string Name)
        {
            bool Current = material.IsKeywordEnabled(Name);
            bool NewValue = EditorGUILayout.Toggle(Name, Current);
            if(Current != NewValue)
                SetKeyword(material, Name, NewValue);
            return NewValue;
        }

        public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] props)
        {
            InitProperties(props);

            Material material = materialEditor.target as Material;

            // Use default labelWidth
            EditorGUIUtility.labelWidth = 0f;

            // Detect any changes to the material
            EditorGUI.BeginChangeCheck();
            {
                EditorGUILayout.BeginVertical("box");
                bool IsLocal = DrawKeyword(material, "_TE_LOCAL");
                bool IsSplatMap = DrawKeyword(material, "_TE_SPLAT");
                bool UseDisant = DrawKeyword(material, "_TE_DISTANT");
                bool UseVertical = DrawKeyword(material, "_TE_VERTICAL");
                bool UseNoise = DrawKeyword(material, "_TE_NOISE");
                EditorGUILayout.EndVertical();

                string LastGroup = "";
                bool CanDrawInfo = true;
       
                EditorGUILayout.BeginVertical("box");

                foreach(var pinfo in Infos)
                {
                    if (pinfo.Prop == null)
                        continue;

                    CanDrawInfo = true;

                    for (int i = pinfo.PName.Length - 1; i >= 0 && CanDrawInfo; --i)
                    {
                        Char c = pinfo.PName[i];

                        if (Char.IsUpper(c))
                        {
                            if (c == 'N')
                            {
                                if (!UseNoise)
                                    CanDrawInfo = false;
                            }
                            else if (c == 'D' && !UseDisant)
                                CanDrawInfo = false;
                            else if (c == 'V' && !UseVertical)
                                CanDrawInfo = false;
                            else if (c == 'R' || c == 'G' || c == 'B' || c == 'A')
                            {
                                if (c != 'R' && !IsSplatMap)
                                    CanDrawInfo = false;
                            }
                        }
                        else
                            break;
                    }

                    if (!CanDrawInfo)
                        continue;

                    if (LastGroup == "")
                        LastGroup = pinfo.GroupName;

                    if(pinfo.GroupName != LastGroup)
                    {
                        EditorGUILayout.EndVertical();
                        EditorGUILayout.BeginVertical("box");
                        LastGroup = pinfo.GroupName;
                    }

                    if(pinfo.MaxCount == 1)
                    {
                        EditorGUILayout.BeginHorizontal();
                    }

                    pinfo.Draw(materialEditor, material);

                    if (pinfo.MaxCount == 0)
                    {
                        EditorGUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.EndVertical();
            }

            if (EditorGUI.EndChangeCheck())
            {

            }
        }

        static void SetKeyword(Material m, string keyword, bool state)
        {
            if (state)
                m.EnableKeyword(keyword);
            else
                m.DisableKeyword(keyword);
        }
    }

}
