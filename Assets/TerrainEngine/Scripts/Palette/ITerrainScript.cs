﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ITerrainScript : MonoBehaviour
{
    public virtual void Close()
    {
        CloseSubScripts();
    }

    public virtual bool CanClose()
    {
        if (Scripts.array != null)
        {
            for (int i = Scripts.Count - 1; i >= 0; --i)
                if (Scripts.array[i] != null && !Scripts.array[i].CanCloseChunk())
                    return false;
        }

        return true;
    }

    #region SubScripts

    [NonSerialized]
    public SList<TerrainSubScript> Scripts;

    public T GetSubScript<T>(int ScriptHashCode,int SubScriptHashCode) where T : TerrainSubScript
    {
        if (Scripts.array == null)
            return null;

        for (int i = Scripts.Count - 1; i >= 0; --i)
        {
            if ((object)Scripts.array[i] != null && Scripts.array[i].SubScriptHashCode == SubScriptHashCode && Scripts.array[i].ScriptHashCode == ScriptHashCode)
                return Scripts.array[i] as T;
        }

        return null;
    }

    public void AddSubScript(TerrainSubScript Obj)
    {
        if (Scripts.array == null)
        {
            Scripts = new SList<TerrainSubScript>(1);
        }

        for (int i = Scripts.Count - 1; i >= 0; --i)
        {
            if (Scripts.array[i] == null)
            {
                Scripts.array[i] = Obj;
                return;
            }
        }

        Scripts.Add(Obj);
    }


    public void RemoveSubScript(int ScriptHashCode,int SubScriptHashCode, bool Remove)
    {
        if (Remove)
        {
            if (Scripts.array != null)
            {
                for (int i = Scripts.Count - 1; i >= 0; --i)
                {
                    if (Scripts.array[i] != null && Scripts.array[i].SubScriptHashCode == SubScriptHashCode && Scripts.array[i].ScriptHashCode == ScriptHashCode)
                    {
                        Scripts.array[i] = null;
                        break;
                    }
                }
            }
        }
    }

    public void CloseSubScripts()
    {
        if (Scripts.array == null)
            return;

        for (int i = Scripts.Count - 1; i >= 0; --i)
            if ((object)Scripts.array[i] != null)
                Scripts.array[i].Close(false);

        Scripts.Clear();
    }

    #endregion
}