﻿using System;

using UnityEngine;

namespace TerrainEngine
{
    public abstract class IVoxelPalette : MonoBehaviour
    {
        public string Name;
        [NonSerialized]
        public VoxelInformation[] GenericPalette;

        private int dataCount = -1;
        public int DataCount
        {
            get
            {
                if(dataCount == -1)
                    dataCount = GetDataCount();

                return dataCount;
            }
        }

        public abstract T GetVoxelData<T>(byte Type) where T : VoxelInformation;

        public virtual T GetVoxelData<T>(string Name) where T : VoxelInformation
        {
            for (byte i = 0; i < GetDataCount(); ++i)
                if (GetVoxelData<VoxelInformation>(i).Name == Name)
                    return GetVoxelData<VoxelInformation>(i) as T;

            return null;
        }

        public virtual byte GetVoxelId(string Name)
        {
            for (byte i = 0; i < GetDataCount(); ++i)
                if (GetVoxelData<VoxelInformation>(i).Name == Name)
                    return i;

            return 0;
        }

        public virtual byte GetVoxelType(VoxelTypes Type)
        {
            for (int i = 0; i < DataCount; ++i)
                if (GetType((byte)i) == Type)
                    return (byte)i;

            return 0;
        }

        public virtual VoxelTypes GetType(byte Type)
        {
            return GenericPalette[Type].VoxelType;
        }


        public virtual VoxelInformation[] GetVoxels()
        {
            if (GenericPalette == null || GenericPalette.Length != GetDataCount())
            {
                GenericPalette = new VoxelInformation[GetDataCount()];
                VoxelInformation V;
                VoxelInformation N;
                for (int i = 0; i < GenericPalette.Length; ++i)
                {
                    V = GetVoxelData<VoxelInformation>((byte)i);
                    N = new VoxelInformation();
                    N.Name = V.Name;
                    N.VoxelType = V.VoxelType;
                    GenericPalette[i] = N;
                }
            }

            return GenericPalette;
        }

        public abstract int GetDataCount();

        public virtual bool IsBlock(byte Type, VoxelTypes VoxelType)
        {
            return GetVoxelData<VoxelInformation>(Type).VoxelType == VoxelType;
        }

        public virtual bool IsBlock(byte Type, VoxelTypes[] VoxelType)
        {
            foreach(VoxelTypes T in VoxelType)
                if(IsBlock(Type,T))
                    return true;

            return false;
        }

        public virtual Vector2 GetUv(int Type, int Face, int Vertex)
        {
            return new Vector2();
        }
    }
}
