using UnityEngine;

public abstract class IColorGenerator : MonoBehaviour 
{
    public delegate Color GenerateColorDelegate(float vx, float vy, float vz, Vector3 Normal);

    public string Name;
    public BlockHelper.BlockFace[] AllowedFaces;

    public virtual bool IsAllowedFace(BlockHelper.BlockFace Face)
    {
        foreach (BlockHelper.BlockFace Allowed in AllowedFaces)
            if (Allowed == Face)
                return true;

        return false;
    }

    public virtual Color GenerateColor(float vx, float vy, float vz, int Face, Color AmbientColor)
    {

        return AmbientColor;
    }

    public virtual Color GenerateColor(float vx, float vy, float vz, Vector3 Normal)
    {
        return Color.white;
    }
}
