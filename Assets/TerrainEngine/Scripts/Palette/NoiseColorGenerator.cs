using TerrainEngine;
using UnityEngine;

public class NoiseColorGenerator : IColorGenerator
{
    public Color NoiseMin;
    public Color NormalColor;
    public Color NoiseMax;
    public float NoiseDetail = 800f;
    public bool UseY = false;

    public override Color GenerateColor(float vx, float vy, float vz, int Face, Color AmbientColor)
    {
        if (NoiseDetail != 0)
        {
            AmbientColor *= GenerateColor(vx, vy, vz, Vector3.zero);
        }
        else
            AmbientColor *= NormalColor;

        return AmbientColor;
    }

    public override Color GenerateColor(float vx, float vy, float vz, Vector3 Normal)
    {
        float Noise = 0.0f;
        float fineDetail = NoiseDetail != 1 ? PerlinSimplexNoise.noise(vx / (NoiseDetail / 10f), UseY ? vy : 30, vz / (NoiseDetail / 10f)) : 1;
        float bigDetails = PerlinSimplexNoise.noise(vx / NoiseDetail, UseY ? vy : 0, vz / NoiseDetail);
        Noise = (fineDetail * bigDetails);

        if (Noise >= 0)
            return  Color.Lerp(NormalColor, NoiseMax, Noise);
        else
            return Color.Lerp(NormalColor, NoiseMin, -Noise);

    }
}
