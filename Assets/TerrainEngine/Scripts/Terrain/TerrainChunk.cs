﻿using System;

using UnityEngine;

namespace TerrainEngine
{
    [Flags]
    public enum ChunkStates : byte
    {
        None = 0,
        HasScript = 1,
        Dirty = 2,
        Selected = 4,
        Combined = 8,
        Border = 16,
        Visible = 32,
        Occluded = 64,
        Flush = 128,
    };

    #region ChunkRef
    /// <summary>
    /// Struct Reference of chunk. Avoid keeping TerrainChunk ref directly (Garbage collector optimization)
    /// BlockId = Index of TerrainBlock in TerrainBlock array
    /// ChunkId = Index of TerrainChunk in TerrainChunk array in TerrainBlock
    /// </summary>
    public struct TerrainChunkRef
    {
        public ushort BlockId;
        public int ChunkId;
        public TerrainInformation Informations
        {
            get
            {
                return TerrainManager.Instance.Informations;
            }
        }

        public Vector3 WorldPosition
        {
            get
            {
                TerrainBlock Block = null;
                if (IsValid(ref Block))
                    return Block.Chunks[ChunkId].GetWorldPosition(Block);
                return Vector3.zero;
            }
        }

        public TerrainChunkRef(ushort BlockId, int ChunkId)
        {
            this.BlockId = BlockId;
            this.ChunkId = ChunkId;
        }

        public TerrainChunkRef(ITerrainBlock Block, int ChunkId)
        {
            if (Block != null)
            {
                this.BlockId = Block.BlockId;
                this.ChunkId = ChunkId;
            }
            else
            {
                this.BlockId = ushort.MaxValue;
                this.ChunkId = int.MaxValue;
            }
        }

        public void Init(ushort BlockId, int ChunkId)
        {
            this.BlockId = BlockId;
            this.ChunkId = ChunkId;
        }

        public void Init(ITerrainBlock Block, int ChunkId)
        {
            if (Block != null)
            {
                this.BlockId = Block.BlockId;
                this.ChunkId = ChunkId;
            }
            else
            {
                this.BlockId = ushort.MaxValue;
                this.ChunkId = int.MaxValue;
            }
        }

        public bool IsValidID()
        {
            if (ChunkId == int.MaxValue || BlockId == ushort.MaxValue)
                return false;

            return true;
        }

        public bool IsValid()
        {
            if (ChunkId == int.MaxValue || BlockId == ushort.MaxValue || TerrainManager.Instance.Container.BlocksArray[BlockId] == null)
                return false;

            return true;
        }

        public bool IsValid(ref TerrainBlock BlockRef)
        {
            if (ChunkId == int.MaxValue || BlockId == ushort.MaxValue)
                return false;

            BlockRef = TerrainManager.Instance.Container.BlocksArray[BlockId] as TerrainBlock;
            if (BlockRef == null || !BlockRef.IsValid())
                return false;

            return true;
        }

        public bool IsValid(ref TerrainChunk ChunkRef)
        {
            if (ChunkId == int.MaxValue || BlockId == ushort.MaxValue)
                return false;

            TerrainBlock BlockRef = TerrainManager.Instance.Container.BlocksArray[BlockId] as TerrainBlock;
            if (BlockRef == null || !BlockRef.IsValid())
                return false;

            ChunkRef = BlockRef.Chunks[ChunkId];
            return true;
        }

        public bool IsValid(ref TerrainBlock BlockRef, ref TerrainChunk ChunkRef)
        {
            if (ChunkId == int.MaxValue || BlockId == ushort.MaxValue)
                return false;

            if (BlockRef == null || BlockRef.BlockId != BlockId)
            {
                BlockRef = TerrainManager.Instance.Container.BlocksArray[BlockId] as TerrainBlock;
                if (BlockRef == null || !BlockRef.IsValid())
                    return false;
            }

            ChunkRef = BlockRef.Chunks[ChunkId];
            return true;
        }

        public bool IsEqual(ushort BlockId, int ChunkId)
        {
            return this.BlockId == BlockId && this.ChunkId == ChunkId;
        }

        public TerrainChunk Chunk
        {
            get
            {
                TerrainChunk ChunkRef = new TerrainChunk();
                IsValid(ref ChunkRef);
                return ChunkRef;
            }
        }

        public TerrainBlock Terrain
        {
            get
            {
                TerrainBlock T = null;
                if (IsValid(ref T))
                    return T;
                return null;
            }
        }

        public void SetState(ChunkStates State, bool Value)
        {
            TerrainBlock Block = null;
            if (IsValid(ref Block))
                Block.SetChunkState(ChunkId, State, Value);
        }

        public override int GetHashCode()
        {
            return BlockId ^ ChunkId;
        }

        public static bool operator !=(TerrainChunkRef lhs, TerrainChunkRef rhs)
        {
            return lhs.BlockId != rhs.BlockId && lhs.ChunkId != rhs.ChunkId;
        }

        public static bool operator ==(TerrainChunkRef lhs, TerrainChunkRef rhs)
        {
            return lhs.BlockId == rhs.BlockId && lhs.ChunkId == rhs.ChunkId;
        }

        public override bool Equals(object obj)
        {
            if (obj is TerrainChunkRef)
            {
                TerrainChunkRef v = (TerrainChunkRef)obj;
                return this.BlockId == v.BlockId && this.ChunkId == v.ChunkId;
            }

            return false;
        }

        public void GetLocalPosition(ref int x,ref int y,ref int z)
        {
            if (ChunkId >= TerrainManager.Instance.Informations.TotalChunksPerBlock)
                return;

            int Id = ChunkId;
            x = Id / (TerrainManager.Instance.Informations.ChunkPerBlock * TerrainManager.Instance.Informations.ChunkPerBlock);
            Id = Id % (TerrainManager.Instance.Informations.ChunkPerBlock * TerrainManager.Instance.Informations.ChunkPerBlock);

            y = Id / TerrainManager.Instance.Informations.ChunkPerBlock;
            Id = Id % TerrainManager.Instance.Informations.ChunkPerBlock;

            z = Id;
        }

        static public TerrainChunkRef Null = new TerrainChunkRef(ushort.MaxValue, int.MaxValue);

        public override string ToString()
        {
            return "(BlockId:" + BlockId + ",ChunkId:" + ChunkId + ")";
        }
    }

    #endregion

    public struct TerrainChunk
    {
        /// <summary>
        /// Chunk local position in Current Block. 0 -> ChunkPerBlock
        /// </summary>
        public byte LocalX;
        public byte LocalY;
        public byte LocalZ;
        public VectorI3 LocalPosition
        {
            get
            {
                return new VectorI3(LocalX, LocalY, LocalZ);
            }
        }

        /// <summary>
        /// Starting X Voxel in CurrentBlock. Depend of LocalPosition.
        /// Used with for-loop of all chunk voxels. for(int x=Chunk.StartX; x < Chunk.EndX ;++x) Block.Voxels.GetByte(x,y,z)
        /// </summary>
        public int StartX
        {
            get
            { 
                return (LocalX * TerrainManager.Instance.Informations.VoxelPerChunk);
            }
        }
        public int StartY
        {
            get
            {
                return (LocalY * TerrainManager.Instance.Informations.VoxelPerChunk);
            }
        }
        public int StartZ
        {
            get
            {
                return (LocalZ * TerrainManager.Instance.Informations.VoxelPerChunk);
            }
        }

        /// <summary>
        /// Return Around Chunks. 0 = Left, 1 Midle, 2 Right.
        /// [2,0,1] = Right,Bottom,Midle.
        /// [0,1,2] = Left,Midle,Forward.
        /// [1,1,1] = this.
        /// |__ | 2 |__|
        /// | 0 | 1 | 2|
        /// |__ | 0 |__|
        public bool GetAroundChunk(TerrainBlock Block, int x, int y, int z, ref TerrainChunkRef Ref)
        {
            x += (int)LocalX - 1;
            y += (int)LocalY - 1;
            z += (int)LocalZ - 1;

            int ax, ay, az;
            if (x >= Block.Information.ChunkPerBlock) { x -= Block.Information.ChunkPerBlock; ax = 2; }
            else if (x < 0) { x += Block.Information.ChunkPerBlock; ax = 0; }
            else ax = 1;

            if (y >= Block.Information.ChunkPerBlock) { y -= Block.Information.ChunkPerBlock; ay = 2; }
            else if (y < 0) { y += Block.Information.ChunkPerBlock; ay = 0; }
            else ay = 1;

            if (z >= Block.Information.ChunkPerBlock) { z -= Block.Information.ChunkPerBlock; az = 2; }
            else if (z < 0) { z += Block.Information.ChunkPerBlock; az = 0; }
            else az = 1;

            Block = Block.AroundBlocks[ax * 9 + ay * 3 + az] as TerrainBlock;
            if (Block != null)
            {
                Ref.BlockId = Block.BlockId;
                Ref.ChunkId = Block.GetChunkId(x, y, z);
                return true;
            }

            Ref.BlockId = ushort.MaxValue;
            Ref.ChunkId = int.MaxValue;
            return false;
        }
        public bool GetAroundChunk(ref TerrainBlock Block, int x, int y, int z, ref TerrainChunkRef Ref)
        {
            x += LocalX - 1;
            y += LocalY - 1;
            z += LocalZ - 1;

            int ax, ay, az;
            if (x >= Block.Information.ChunkPerBlock) { x -= Block.Information.ChunkPerBlock; ax = 2; }
            else if (x < 0) { x += Block.Information.ChunkPerBlock; ax = 0; }
            else ax = 1;

            if (y >= Block.Information.ChunkPerBlock) { y -= Block.Information.ChunkPerBlock; ay = 2; }
            else if (y < 0) { y += Block.Information.ChunkPerBlock; ay = 0; }
            else ay = 1;

            if (z >= Block.Information.ChunkPerBlock) { z -= Block.Information.ChunkPerBlock; az = 2; }
            else if (z < 0) { z += Block.Information.ChunkPerBlock; az = 0; }
            else az = 1;

            Block = Block.AroundBlocks[ax * 9 + ay * 3 + az] as TerrainBlock;
            if (Block != null)
            {
                Ref.BlockId = Block.BlockId;
                Ref.ChunkId = Block.GetChunkId(x, y, z);
                return true;
            }

            Ref.BlockId = ushort.MaxValue;
            Ref.ChunkId = int.MaxValue;
            return false;
        }

        public void Update(TerrainBlock Block,int ChunkId, ref int CombineCount, ref int OccludedCount)
        {
            bool IsOccluded = (Block.States[ChunkId] & ChunkStates.Occluded) == ChunkStates.Occluded;

            if (!Block.HasState(TerrainBlockStates.VISIBLE) || Block.HasState(TerrainBlockStates.CLOSED))
                SetVisibility(Block,ChunkId, false);

            if (Block.HasChunkState(ChunkId, ChunkStates.Combined))
            {
                ++CombineCount;
                if (IsOccluded && (Block.States[ChunkId] & ChunkStates.Visible) == ChunkStates.Visible)
                    ++OccludedCount;
            }
            else
            {
                //UpdateVisibility(Block);

                if ((Block.States[ChunkId] & ChunkStates.HasScript) == ChunkStates.HasScript && ActionProcessor.UnityFrameCount % 5 == LocalX % 5)
                {
                    TerrainChunkScript Script = Block.GetChunkScript(ChunkId);

                    if (Script.CRenderer.enabled)
                    {
                        if (IsOccluded)
                        {
                            Script.CRenderer.enabled = false;
                        }
                    }
                    else
                    {
                        if (!IsOccluded)
                        {
                            Script.CRenderer.enabled = true;
                        }
                    }
                }
            }
        }

        public void SetVisibility(TerrainBlock Block,int ChunkId, bool Value)
        {
            if (((Block.States[ChunkId] & ChunkStates.Visible) == ChunkStates.Visible) != Value)
            {
                Block.SetChunkState(ChunkId, ChunkStates.Visible, Value);
                if (Value)
                    Block.SetChunkState(ChunkId, ChunkStates.Occluded, false);
            }
        }

        public Vector3 GetWorldPosition(TerrainBlock Block)
        {
            return new Vector3(Block.WorldPosition.x + LocalX * Block.Information.ChunkSizeX, Block.WorldPosition.y + LocalY * Block.Information.ChunkSizeY, Block.WorldPosition.z + LocalZ * Block.Information.ChunkSizeZ);
        }

        public Vector3 GetLocalWorldPosition(TerrainBlock Block)
        {
            return new Vector3(LocalX * Block.Information.ChunkSizeX, LocalY * Block.Information.ChunkSizeY, LocalZ * Block.Information.ChunkSizeZ);
        }

        #region Around Chunks

        public delegate void ChunkIterationCallBack(TerrainBlock Block, int ChunkId);
        static public void GetChunksIteration(TerrainBlock Block, int ChunkX,int ChunkY,int ChunkZ, Vector3 Position, int ChunkDistance,ChunkIterationCallBack callback, bool MustHaveScript = true,int Resolution = 1)
        {
            if (Block == null)
                return;

            TerrainBlock ABlock = null;
            int ChunkId = 0;
            int ax, ay, az;
            for (ax = -ChunkDistance; ax <= ChunkDistance; ax += Resolution)
            {
                for (ay = -ChunkDistance; ay <= ChunkDistance; ay += Resolution)
                {
                    for (az = -ChunkDistance; az <= ChunkDistance; az += Resolution)
                    {
                        if (!Block.GetChunkId(ax + ChunkX, ay + ChunkY, az + ChunkZ, ref ABlock, ref ChunkId))
                            continue;

                        if (!MustHaveScript || ABlock.HasChunkState(ChunkId, ChunkStates.HasScript))
                            callback(ABlock, ChunkId);
                    }
                }
            }
        }

        #endregion

        public override string ToString()
        {
            return "TerrainChunk : " + LocalX + "," + LocalY + "," + LocalZ;
        }
    }
}
