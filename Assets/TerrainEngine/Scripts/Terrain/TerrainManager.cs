﻿using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

/// <summary>
/// Main class managing Blocks / Server / Loading / Processors
/// </summary>
public abstract class TerrainManager : MonoBehaviour
{
    [Serializable]
    public enum LoadingStates : byte
    {
        INITIALIZING = 0, // Initing all classes and references
        INIT_MANAGER = 1, // Calling virtual functions of TerrainManager
        INIT_PROCESSORS = 2, // Calling OnInitManager and OnInit() of all Processors
        STARTING_PROCESSORS = 3, // Calling OnStart() of all processors and Starting server if exist
        STARTING = 4, // Starting server is possible, Calling OnStart on all Processors and OnManagerInited() to the palette
        STARTED = 5,
        LOADED = 6, // World Building is done, terrain are completely loaded and generated
    };

    static public TerrainManager Instance = null;

    [NonSerialized]
    public Transform BlocksParent;

    public TerrainInformation Informations;

    public TerrainWorldServer Server;

    [HideInInspector]
    public TerrainContainer Container;

    [HideInInspector]
    public TerrainBuilder Builder;

    [NonSerialized]
    public List<IBlockProcessor> Processors = new List<IBlockProcessor>();

    public ITerrainNetworkInterface ModificationInterface;

    public GameObject ChunkPrefab;
    public IVoxelPalette Palette;

    #region LoadingState

    public bool isStarted
    {
        get
        {
            return State > LoadingStates.INIT_MANAGER;
        }
    }
    public bool isInited
    {
        get
        {
            return State > LoadingStates.INIT_PROCESSORS;
        }
    }
    public bool isLoaded
    {
        get
        {
            return State >= LoadingStates.STARTED;
        }
    }

    public LoadingStates State = LoadingStates.INITIALIZING;

    [HideInInspector]
    public float LoadingProgress = 100.0f;

    private float LastLoadingProgress = 100.0f;
    private float LoadingTimeStart = 0f;
    public float LoadingTime = 0f;

    #endregion

    public bool DrawLogs = false;

    [HideInInspector]
    public ITerrainCameraHandler CameraHandler;

    [NonSerialized]
    public WorldGenerator Generator;

    public ProgressSystem SubProgress = new ProgressSystem();

    public bool OptimizeBlockBorders = true;
    public bool CombineBlockChunks = true;
    public bool RegenerateCornersChunks = true;

    public SList<IBlockProcessor> ProcessorsById;

    public bool SaveAllTerrains = false;

    void Start()
    {
        if (Instance != this)
            Debug.LogError("[TerrainManager] already started. Only one TerrainManager by scene is allowed");

        if (State != LoadingStates.INITIALIZING)
        {
            Debug.LogWarning("[TerrainManager] State != INITIALIZING. Enum Value of 'State' must be 'INITIALIZING' and must not be changed in Play Mode");
            State = LoadingStates.INITIALIZING;
        }

        Instance = this;

        BlocksParent = new GameObject().transform;
        BlocksParent.name = "Terrain";
        BlocksParent.parent = transform;
    }

    void Update()
    {
        if (!ActionProcessor.Started)
            return;

        if (SaveAllTerrains && Server != null)
        {
            SaveAllTerrains = false;
            Server.SaveAll();
        }

        UpdateState();

        GeneralLog.DrawLogs = DrawLogs;
        ThreadManager.UnityTime = Time.realtimeSinceStartup;
        CameraHandler.DoUpdate(this);
    }

    void OnDestroy()
    {
        Instance = null;
        MemoryAllocator.CleanMemory();
    }

    void UpdateState()
    {
        if (State < LoadingStates.STARTED)
            Debug.Log("[TerrainManager] : " + State + ",Processors:" + Processors.Count + ",HasServer:" + (Server != null ? "Yes" : "No"));

        switch (State)
        {
            case LoadingStates.INITIALIZING:
                {
                    Informations.Init();

                    // Manage Terrain Modification. Overridable to use with server
                    if (ModificationInterface == null)
                    {
                        ModificationInterface = GetComponent<ITerrainNetworkInterface>();
                        if (ModificationInterface == null)
                            ModificationInterface = gameObject.AddComponent<ITerrainNetworkInterface>();
                    }
                    ModificationInterface.Init(this);

                    // Manage cameras position
                    CameraHandler = GetComponent<ITerrainCameraHandler>();
                    if (CameraHandler == null)
                    {
                        CameraHandler = gameObject.AddComponent<ITerrainCameraHandler>();
                    }
                    CameraHandler.Init(this);

                    OnState(State);
                    State = LoadingStates.INIT_MANAGER;
                } break;
            case LoadingStates.INIT_MANAGER:
                {
                    Container = new TerrainContainer(this,Informations);
                    Builder = new TerrainBuilder(this);

                    Informations.Init();
                    Informations.OnDataCreate += OnDataCreate;

                    TerrainRaycaster.VoxelCount = Informations.VoxelsPerAxis;
                    TerrainRaycaster.VoxelPerChunk = Informations.VoxelPerChunk;
                    Palette.GetVoxels();

                    if (!InitCustomData())
                    {
                        Debug.LogError("Can not start TerrainManager : InitCustomData() error");
                        return;
                    }

                    if (!InitOffsets())
                    {
                        Debug.LogError("Can not start TerrainManager : InitOffsets() error");
                        return;
                    }

                    if (!InitTextures())
                    {
                        Debug.LogError("Can not start TerrainManager : InitTextures() error");
                        return;
                    }

                    if (!InitActionClass())
                    {
                        Debug.LogError("Can not start TerrainManager : InitActionClass() error");
                        return;
                    }

                    Informations.Init();

                    OnState(State);
                    State = LoadingStates.INIT_PROCESSORS;
                } break;
            case LoadingStates.INIT_PROCESSORS:
                {
                    int i = 0;
                    for (i = 0; i < Processors.Count; ++i)
                        Processors[i].OnInitManager(this);

                    for (i = 0; i < Processors.Count; ++i)
                        Processors[i].OnInitSeed(Informations.IntSeed);

                    OnState(State);
                    State = LoadingStates.STARTING_PROCESSORS;
                } break;
            case LoadingStates.STARTING_PROCESSORS:
                {
                    bool CanStart = true;
                    int i = 0;

                    // Checking all processors
                    for (i = 0; i < Processors.Count; ++i)
                    {
                        if (!Processors[i].CanStart())
                        {
                            CanStart = false;
                        }
                    }

                    if (!CanStart)
                        return;

                    Informations.Init();
                    Server = ModificationInterface.GetServer();
                    if (Generator == null)
                        BlockManager.Init(Palette.GetVoxels());

                    if (Server != null)
                    {
                        for (i = Processors.Count - 1; i >= 0; --i)
                        {
                            Server.OnAddObject += Processors[i].OnServerAddObject;
                            Server.OnRemoveObject += Processors[i].OnServerRemoveObject;
                            Server.OnServerSendCreateBlock += Processors[i].OnServerSendCreateBlock;
                            Server.OnServerSendAllowedBlocks += Processors[i].OnServerSendAllowedBlocks;
                        }
                    }

                    OnState(State);
                    State = LoadingStates.STARTING;
                } break;
            case LoadingStates.STARTING:
                {
                    for (int i = 0; i < Processors.Count; ++i)
                        Processors[i].OnStart();

                    ModificationInterface.OnManagerInited(this);

                    if (!Application.isEditor)
                        Resources.UnloadUnusedAssets();

                    OnState(State);
                    State = LoadingStates.STARTED;
                }break;
            case LoadingStates.STARTED:
                {
                    if (Server != null && Server.Objects.Count == 0)
                    {
                        Debug.LogError("No TerrainObject on server. Please add a new LocalTerrainObjectServer.cs and link your main object transform on 'CTransform' value. Terrain will load around this object");
                    }
                    State = LoadingStates.LOADED;
                }break;
            case LoadingStates.LOADED:
                {
                    LoadingProgress = ProgressSystem.Instance.GetProgress(ThreadManager.UnityTime);

                    if (LoadingProgress != 100f)
                    {
                        if (LastLoadingProgress != LoadingProgress)
                        {
                            if (LastLoadingProgress == 100f)
                            {
                                LoadingTimeStart = Time.realtimeSinceStartup;
                            }

                            LastLoadingProgress = LoadingProgress;
                        }

                        LoadingTime = Time.realtimeSinceStartup - LoadingTimeStart;
                    }
                } break;
        }
    }

    public WorldGenerator InitGenerators(bool ExportingXML=false)
    {
        if (Generator == null)
        {
            Generator = new WorldGenerator();
            Generator.Information = Informations;
            Generator.Palette = Palette.GetVoxels();
            BlockManager.Init(Generator.Palette);
        }

        if (!Application.isPlaying || Processors == null || Processors.Count == 0)
        {
            Processors = new List<IBlockProcessor>(GetComponentsInChildren<IBlockProcessor>());
            Debug.Log("Searching Processors :" + Processors.Count);
        }

        foreach (IBlockProcessor Processor in Processors)
        {
            GetProcessorGenerator(Processor, ExportingXML);
        }

        Debug.Log("TerrainManager : Generators Inited");
        Generator.SortGenerators();
        return Generator;
    }

    public void GetProcessorGenerator(IBlockProcessor Processor, bool ExportingXML)
    {
        if (Processor == null)
            return;

        if (!Processor.enabled || !Processor.gameObject.activeInHierarchy)
        {
            Debug.LogError("Processor disabled : " + Processor);
            return;
        }

        if (!Application.isPlaying)
        {
            try
            {
                Processor.OnInitManager(this);
            }
            catch(Exception e)
            {
                GeneralLog.LogError(e.ToString());
            }
        }

        IWorldGenerator Gen = Processor.GetGenerator();

        if (Gen != null)
        {
            Gen.Priority = Processor.Priority;
            Processor.HasGenerator = true;
            Generator.AddGenerator(Gen);
        }
        else
        {
            // Delegates can't be exported
            if (!ExportingXML)
            {
                System.Reflection.MethodInfo[] Methods = Processor.GetType().GetMethods();
                for (int i = 0; i < Methods.Length; ++i)
                {
                    if (Methods[i].Name == "Generate" && Methods[i].DeclaringType == Processor.GetType())
                    {
                        Debug.Log("TerrainManager : Creating Generate Delegate Generator for " + Processor);
                        Processor.HasGenerator = true;
                        DelegateGenerator DelGen = new DelegateGenerator(Processor.Generate);
                        DelGen.GeneratorType = Processor.GetGeneratorType();
                        DelGen.Priority = Processor.Priority;
                        DelGen.DataName = Processor.DataName;
                        Generator.AddGenerator(DelGen);
                    }
                }
            }

            if (!Processor.HasGenerator)
                Debug.Log("TerrainManager : No Generator " + Processor);
        }
    }

    public virtual void OnState(LoadingStates State)
    {

    }

    #region Init

    public virtual bool InitTextures()
    {
        if (Palette == null)
        {
            enabled = false;
            return false;
        }

        for (int i = 0; i < Palette.GetDataCount(); ++i)
        {
            Palette.GetVoxelData<VoxelInformation>((byte)i).Type = i;
        }
        return true;
    }

    public virtual bool InitOffsets()
    {
        return true;
    }

    /// <summary>
    /// For each thread add a new Action that will load/generate TerrainBlock
    /// </summary>
    public virtual bool InitActionClass()
    {
        for (int i = 0; i < ActionProcessor.TManager.Threads.Length; ++i)
        {
            ExecuteAction Action = new ExecuteAction("TerrainBuilder", Builder.UpdateThread);
            ActionProcessor.TManager.Threads[i].AddAction(Action);
        }

        ExecuteAction UnityAction = new ExecuteAction("TerrainBuilder", Builder.UpdateUnity);
        ActionProcessor.Unity.AddAction(UnityAction);
        return true;
    }

    public virtual bool InitCustomData()
    {
        return true;
    }

    #endregion

    #region Virtuals

    public virtual TerrainBlock CreateTerrain()
    {
        return null;
    }

    public virtual GameObject GetChunkPrefab()
    {
        return ChunkPrefab;
    }

    static public TerrainChunkScript GetGameObjectChunk(GameObject Obj)
    {
        return Obj.GetComponent<TerrainChunkScript>();
    }

    #endregion

    #region Processors

    public void AddProcessor(IBlockProcessor Processor)
    {
        Debug.Log(name + " : Adding Processor '" + Processor + "'");

        lock (Processors)
        {
            if(ProcessorsById.array == null)
                ProcessorsById = new SList<IBlockProcessor>(10);

            if (Processor.UniqueId == 255)
            {
                ProcessorsById.Add(Processor);
                Processor.UniqueId = (byte)(ProcessorsById.Count-1);
            }
            else
            {
                ProcessorsById.array[Processor.UniqueId] = Processor;
            }

            Processors.Add(Processor);
            Processors.Sort(ProcessorsComparer);

            if (isInited)
            {
                Processor.OnInitManager(this);
                Processor.OnInitSeed(Informations.IntSeed);
            }
        }
    }

    public void RemoveProcessor(IBlockProcessor Processor)
    {
        Debug.Log(name + " : Removing Processor '" + Processor + "'");

        lock (Processors)
        {
            if (ProcessorsById.array == null)
                ProcessorsById = new SList<IBlockProcessor>(10);

            if (Processor.UniqueId < ProcessorsById.Count)
                ProcessorsById.array[Processor.UniqueId] = null;

            if (Processors.Remove(Processor))
            {
                Processors.Remove(Processor);
            }
        }

        Processor.OnStop();
    }

    public T[] GetProcessors<T>() where T : IBlockProcessor
    {
        int Count = 0;

        for (int i = 0; i < Processors.Count; ++i)
            if (Processors[i].GetType() == typeof(T))
                ++Count;

        T[] Result = new T[Count];
        Count = 0;
        foreach (IBlockProcessor Processor in Processors)
            if (Processor.GetType() == typeof(T))
            {
                Result[Count] = Processor as T;
                ++Count;
            }

        return Result;
    }

    public T GetProcessor<T>() where T : IBlockProcessor
    {
        for (int i = 0; i < Processors.Count; ++i)
            if (Processors[i].GetType() == typeof(T))
                return Processors[i] as T;

        return null;
    }

    public IBlockProcessor GetProcessor(string Name)
    {
        for (int i = 0; i < Processors.Count; ++i)
            if (Processors[i].name.ToLower() == Name.ToLower())
                return Processors[i];

        return null;
    }

    static public int ProcessorsComparer(IBlockProcessor a, IBlockProcessor b)
    {
        if (a.Priority > b.Priority)
            return 1;
        if (a.Priority < b.Priority)
            return -1;
        return 0;
    }

    public void OnDataCreate(TerrainDatas Data)
    {
        int i;
        for (i = 0; i < Processors.Count; ++i)
            Processors[i].OnBlockDataCreate(Data);
    }

    #endregion

    #region Statics

    public int GetChunkDistance(Vector3 Origin, Vector3 Position)
    {
        VectorI3 OriginChunk = Informations.GetChunkGlobalPosition(Origin);
        VectorI3 PositionChunk = Informations.GetChunkGlobalPosition(Position);

        return Max(Distance(OriginChunk.x, PositionChunk.x), Distance(OriginChunk.y, PositionChunk.y), Distance(OriginChunk.z, PositionChunk.z));
    }

    static public int Max(int a, int b, int c)
    {
        if (b > a)
            a = b;

        if (c > a)
            a = c;

        return a;
    }

    static public int Distance(int a, int b)
    {
        if (a > b)
            return a - b;

        return b - a;
    }

    public virtual TerrainBlock GetBlockFromWorldPosition(Vector3 WorldPosition)
    {
        return Container.GetBlock(Informations.GetBlockFromWorldPosition(WorldPosition)) as TerrainBlock;
    }

    public virtual TerrainBlock GetBlockXYFromWorldPosition(Vector3 WorldPosition)
    {
        ITerrainBlock Block = null;
        VectorI3 LocalPosition = Informations.GetBlockFromWorldPosition(WorldPosition);
        Container.GetBlockXY(LocalPosition.x, LocalPosition.y, out Block);
        return Block as TerrainBlock;
    }

    #endregion
}