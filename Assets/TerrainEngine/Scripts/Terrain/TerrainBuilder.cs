﻿using FrameWork;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace TerrainEngine
{
    public struct ChunkGenerateInfo
    {
        public ChunkGenerateInfo(TerrainChunkRef ChunkRef, IMeshData Data)
        {
            this.ChunkRef = ChunkRef;
            this.Data = Data;
        }
        public TerrainChunkRef ChunkRef;
        public IMeshData Data;
    }

    public class TerrainBuilder
    {
        public struct BlockAction
        {
            public TerrainDatas Data;
            public VectorI3 LocalPosition;
            public bool Remove; // True, this block must be removed
        }

        public enum GenerationStep : byte
        {
            CREATE_TERRAIN = 0,
            GENERATE_CHUNKS = 1,
            CALL_EVENTS = 2,
            TERRAIN_LOADED = 3,

            CLOSE_TERRAIN_THREAD = 0,
            CLOSE_TERRRAIN_UNITY = 1,
        }

        public class TerrainBlockBuilder
        {
            public TerrainManager Manager;
            public TerrainContainer Container;
            public TerrainInformation Information;
            public bool CombineBlockChunks;
            public bool IsGenerating = false;
            public BlockAction CurrentAction; // Current Action, Remove or Create Block
            public TerrainBlock Block; // Current Generating Block
            public byte Step = 0; // Step in current generation

            public GenerateAndBuildGroupEvent Event;
            public TerrainChunksCombiner Combiner;
            public int GeneratingChunkId = 0;
            public int ChunksToGenerate = 0;
            private int[] GeneratedCount;
            public int TotalGeneratedCount
            {
                get
                {
                    int Count = 0;
                    for (int i = GeneratedCount.Length - 1; i >= 0; --i)
                        Count += GeneratedCount[i];
                    return Count;
                }
                set
                {
                    for (int i = GeneratedCount.Length - 1; i >= 0; --i)
                        GeneratedCount[i] = value;
                }
            }
            public FList<TerrainChunkRef> AroundChunksToGenerate = new FList<TerrainChunkRef>(500); // Chunks to generate from blocks around
            public int BuildPosition = 0;
            public FList<ChunkGenerateInfo> GeneratedChunks = new FList<ChunkGenerateInfo>(500);    // Generated Chunks Mesh

            public TerrainBlockBuilder(TerrainManager Manager)
            {
                this.Manager = Manager;
                this.Information = Manager.Informations;
                this.Container = Manager.Container;
                this.CombineBlockChunks = Manager.CombineBlockChunks;
            }

            public TerrainBlockBuilder(TerrainContainer Container,TerrainInformation Information)
            {
                this.Container = Container;
                this.Information = Information;
                this.CombineBlockChunks = false;
            }

            public void UpdateThread(ActionThread Thread)
            {
                if (IsGenerating)
                {
                    if (CurrentAction.Remove)
                        CloseBlock(Thread);
                    else
                        CreateBlock(Thread);
                }
            }

            public void UpdateUnity(ActionThread Thread)
            {
                if (IsGenerating)
                {
                    if (CurrentAction.Remove)
                        CloseBlock(Thread);
                    else
                    {
                        if (Step > (int)GenerationStep.CREATE_TERRAIN)
                        {
                            //Debug.Log(Block.LocalPosition + ",Generated:" + GeneratedCount + ",ToGenerate:" + ChunksToGenerate + ",Step:" + Step + ",ChunksDirty:" + GeneratedChunksDirty);

                            if (Step == (int)GenerationStep.TERRAIN_LOADED)
                            {
                                int i = 0;
                                int Count = TerrainManager.Instance.Processors.Count;

                                if (!HasCalledUpdaters)
                                {
                                    HasCalledUpdaters = true;
                                    if (Combiner != null && CombineBlockChunks)
                                        Combiner.Apply(Block);

                                    for (i = 0; i < Count; ++i)
                                    {
                                        TerrainManager.Instance.Processors[i].ActiveUpdaters(Block);
                                        TerrainManager.Instance.Processors[i].OnChunksStartBuilding(Block);
                                    }
                                }

                                bool CanDraw = true;
                                for (i = 0; i < Count; ++i)
                                {
                                    if (!TerrainManager.Instance.Processors[i].OnTerrainIsReadyToDraw(Block))
                                        CanDraw = false;
                                }

                                if (!CanDraw)
                                {
                                    return;
                                }

                                ChunkGenerateInfo Info = new ChunkGenerateInfo();
                                while (GetChunkToApply(ref Info))
                                {
                                    if (ThreadManager.EnableStats)
                                        Thread.RecordTime();

                                    BuilChunk(Thread, this, ref Info);

                                    if (ThreadManager.EnableStats)
                                        Thread.AddStats("Client:BuildChunk", Thread.Time());

                                    if (!Thread.HasTime())
                                        break;
                                }

                                if (BuildPosition < GeneratedChunks.Count)
                                    return;


                                if (Event != null)
                                {
                                    if (!Event.OnTerrainBuilded(Thread, Block))
                                        return;
                                }


                                if (ThreadManager.EnableStats)
                                    Thread.RecordTime();

                                for (i = 0; i < Count; ++i)
                                {
                                    TerrainManager.Instance.Processors[i].OnTerrainLoadingDone(Block);
                                }

                                ProgressSystem.Instance.SetProgress("Build Terrain : " + CurrentAction.LocalPosition.ToString(), 100f, ThreadManager.UnityTime);
                                Block.LoadingDone();

                                if (ThreadManager.EnableStats)
                                    Thread.AddStats("Client:TerrainLoaded", Thread.Time());

                                Done();
                            }
                        }
                    }
                }

                return;
            }

            public bool GetNextChunk(ref ushort BlockId, ref int ChunkId)
            {
                ChunkId = System.Threading.Interlocked.Increment(ref GeneratingChunkId) - 1;

                // ChunkId > totalchunk per block, now generate AroundChunk, ChunkId is now used as Index on AroundChunksToGenerate
                if (ChunkId >= Information.TotalChunksPerBlock)
                {
                    ChunkId -= Information.TotalChunksPerBlock;
                    if (ChunkId >= AroundChunksToGenerate.Count)
                        return false;

                    BlockId = AroundChunksToGenerate.array[ChunkId].BlockId;
                    ChunkId = AroundChunksToGenerate.array[ChunkId].ChunkId;
                    return true;
                }

                BlockId = Block.BlockId;
                return true;
            }

            public bool GetChunkToApply(ref ChunkGenerateInfo Info)
            {
                if (GeneratedChunks.Count != 0 && BuildPosition < GeneratedChunks.Count)
                {
                    Info = GeneratedChunks.array[BuildPosition];
                    ++BuildPosition;
                    return true;
                }

                ++BuildPosition;
                return false;
            }

            public void UnCompressData(ITerrainDataSave TData)
            {
                if (TData.CompressedData != null)
                {
                    using (MemoryStream TempStream = new MemoryStream())
                    {
                        using (ZOutputStream ZStream = new ZOutputStream(TempStream, true))
                        {
                            ZStream.Write(TData.CompressedData, 0, TData.CompressedData.Length);
                            TempStream.Position = 0;

                            using (BinaryReader Reader = new BinaryReader(TempStream))
                            {
                                TData.Read(Reader);
                            }
                        }
                    }

                    TData.CompressedData = null;
                }
            }

            /*public void GenerateChunk(ActionThread Thread, TerrainBlock GeneratingBlock, int GeneratingChunkId)
            {
                int GeneratingParentId = GeneratingBlock.GetChunkParent(GeneratingChunkId);
                if (GeneratingParentId != GeneratingChunkId)
                    return;

                if (ThreadManager.EnableStats)
                    Thread.RecordTime();

                IMeshData GeneratingData = null;
                try
                {
                    GeneratingData = GeneratingBlock.GenerateChunk(Thread, GeneratingChunkId);

                    if (Event != null)
                        Event.OnGenerateChunk(Thread, GeneratingBlock, GeneratingChunkId);
                }
                catch (Exception e)
                {
                    if (GeneratingData != null)
                        GeneratingData.Close();

                    GeneratingData = null;
                    GeneralLog.LogError(e.ToString());
                }

                if (GeneratingData != null)
                {
                    ChunkGenerateInfo GeneratingInfo = new ChunkGenerateInfo();
                    GeneratingInfo.ChunkRef = new TerrainChunkRef(GeneratingBlock.BlockId, GeneratingChunkId);
                    GeneratingInfo.Data = GeneratingData;

                    lock (GeneratedChunks.array)
                    {
                        GeneratedChunks.Add(GeneratingInfo);
                    }
                }

                if (ThreadManager.EnableStats)
                    Thread.AddStats("GenerateChunk", Thread.Time());
            }*/

            static public void BuilChunk(ActionThread Thread, TerrainBlockBuilder Builder, ref ChunkGenerateInfo Info)
            {
                TerrainBlock Block = null;
                if (Builder.Block.BlockId == Info.ChunkRef.BlockId)
                    Block = Builder.Block;
                else
                {
                    if (!Info.ChunkRef.IsValid(ref Block))
                    {
                        if (Info.Data != null)
                            Info.Data.Close();
                        return;
                    }
                }

                Block.Build(Info.Data, Info.ChunkRef.ChunkId, Info.ChunkRef.BlockId == Builder.Block.BlockId, Info.Data == null ? false : Info.Data.Combined, false);

                if (Builder.Block == Block)
                {
                    if (Builder.Manager != null)
                    {
                        int Count = Builder.Manager.Processors.Count;
                        for (int i = 0; i < Count; ++i)
                            Builder.Manager.Processors[i].OnFirstChunkBuild(Block, Info.ChunkRef.ChunkId, Info.Data);

                    }
                }

                if (Info.Data != null)
                    Info.Data.Close();
            }

            public void CreateBlock(ActionThread Thread)
            {
                if (Thread.ThreadId == 0)
                {
                    // Create Terrain
                    if (Step == (int)GenerationStep.CREATE_TERRAIN)
                    {
                        if (GeneratedCount == null)
                            GeneratedCount = new int[Thread.Manager.Threads.Length];

                        if (ThreadManager.EnableStats)
                            Thread.RecordTime();

                        Block = Container.CreateTerrain(CurrentAction.LocalPosition, CurrentAction.Data);

                        if (Block == null)
                        {
                            Cancel();
                        }
                        else
                        {
                            for (int i = 0; i <= CurrentAction.Data.Customs.Count; ++i)
                            {
                                if (i == 0)
                                    UnCompressData(CurrentAction.Data);
                                else
                                    UnCompressData(CurrentAction.Data.Customs.array[i - 1]);
                            }

                            Block.InitChunks(AroundChunksToGenerate);

                            ChunksToGenerate = Information.TotalChunksPerBlock + AroundChunksToGenerate.Count;
                            Step = (int)GenerationStep.GENERATE_CHUNKS;
                        }

                        if (ThreadManager.EnableStats)
                            Thread.AddStats("Client:CreateTerrain", Thread.Time());
                    }

                    if (Step == (int)GenerationStep.CALL_EVENTS)
                    {
                        if (ThreadManager.EnableStats)
                            Thread.RecordTime();

                        if (Manager != null)
                            for (int i = 0; i < Manager.Processors.Count; ++i)
                                Manager.Processors[i].OnChunksGenerationDone(Block);

                        Step = (int)GenerationStep.TERRAIN_LOADED;

                        if (ThreadManager.EnableStats)
                            Thread.AddStats("Client:OnChunksGenerationDone", Thread.Time());
                    }
                }

                // Generate Local Chunks
                if (Step == (int)GenerationStep.GENERATE_CHUNKS)
                {
                    ushort BlockId = 0;
                    int ChunkId = 0;

                    int GeneratingParentId, GeneratingChunkId;
                    TerrainBlock GeneratingBlock;
                    IMeshData GeneratingData = null;
                    ChunkGenerateInfo GeneratingInfo = new ChunkGenerateInfo();

                    while (Thread.HasTime() && GetNextChunk(ref BlockId, ref ChunkId))
                    {
                        try
                        {
                            if (BlockId != Block.BlockId)
                            {
                                GeneratingBlock = Container.BlocksArray[BlockId] as TerrainBlock;
                                GeneratingChunkId = ChunkId;
                            }
                            else
                            {
                                GeneratingBlock = Block;
                                GeneratingChunkId = ChunkId;
                            }
                            //GenerateChunk(Thread, GeneratingBlock, GeneratingChunkId);

                            GeneratingParentId = GeneratingBlock.GetChunkParent(GeneratingChunkId);
                            if (GeneratingParentId == GeneratingChunkId)
                            {
                                if (ThreadManager.EnableStats)
                                    Thread.RecordTime();

                                GeneratingData = null;
                                try
                                {
                                    GeneratingData = GeneratingBlock.GenerateChunk(Thread, GeneratingChunkId);

                                    if (Event != null)
                                        Event.OnGenerateChunk(Thread, GeneratingBlock, GeneratingChunkId);
                                }
                                catch (Exception e)
                                {
                                    if (GeneratingData != null)
                                        GeneratingData.Close();

                                    GeneratingData = null;
                                    GeneralLog.LogError(e.ToString());
                                }

                                if (GeneratingData != null || (BlockId != Block.BlockId && GeneratingBlock.HasChunkState(GeneratingChunkId, ChunkStates.HasScript)))
                                {
                                    GeneratingInfo.ChunkRef.BlockId = GeneratingBlock.BlockId;
                                    GeneratingInfo.ChunkRef.ChunkId = GeneratingChunkId;
                                    GeneratingInfo.Data = GeneratingData;

                                    lock (GeneratedChunks.array)
                                    {
                                        GeneratedChunks.Add(GeneratingInfo);
                                    }
                                }

                                if (ThreadManager.EnableStats)
                                    Thread.AddStats("Builder:GenerateChunk", Thread.Time());
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e.ToString());
                        }

                        ++GeneratedCount[Thread.ThreadId];
                    }

                    if (Thread.ThreadId == 0 && ChunksToGenerate <= TotalGeneratedCount)
                    {
                        if (CombineBlockChunks)
                        {
                            if (Combiner == null)
                                Combiner = new TerrainChunksCombiner();
                            else
                                Combiner.Clear();

                            Combiner.AddChunks(Block, GeneratedChunks);
                            Combiner.Combine(Block);
                        }
                        Step = (int)GenerationStep.CALL_EVENTS;
                    }
                }
            }

            public void CloseBlock(ActionThread Thread)
            {
                if (Step == (byte)GenerationStep.CLOSE_TERRAIN_THREAD) // Close Thread
                {
                    if (Thread.ThreadId == 0)
                    {
                        if (Block == null)
                        {
                            if (!Container.CloseThreadTerrain(CurrentAction.LocalPosition, ref Block))
                            {
                                //Debug.LogError("Can not remove Block:" + CurrentAction.LocalPosition);
                                Cancel();
                                return;
                            }
                        }

                        if (Block.ProcessorUpdate != null && !Block.ProcessorUpdate.IsDone())
                        {
                            Block.ProcessorUpdate.IsStopped = true;
                            return;
                        }

                        Block.SetState(TerrainBlockStates.CLOSED, true);

                        if (Manager != null)
                        {
                            int Count = Manager.Processors.Count;
                            for (int i = 0; i < Count; ++i)
                                Manager.Processors[i].OnTerrainThreadClose(Block);
                        }

                        if (Block.ProcessorUpdate != null)
                            Block.ProcessorUpdate.Clear();

                        Block.CloseThread();
                        Step = (byte)GenerationStep.CLOSE_TERRRAIN_UNITY;
                    }
                }
                else if (Step == (byte)GenerationStep.CLOSE_TERRRAIN_UNITY) // Close Unity
                {
                    if (Thread.IsUnityThread())
                    {
                        if (Manager != null)
                        {
                            int Count = Manager.Processors.Count;
                            for (int i = 0; i < Count; ++i)
                                Manager.Processors[i].OnTerrainUnityClose(Block);
                        }

                        Container.CloseUnityTerrain(Block);
                        CurrentAction.Data = Block.Voxels;
                        Cancel();
                    }
                }
            }

            public void Cancel()
            {
                if (CurrentAction.Data != null)
                    CurrentAction.Data.IsUsedByClient = false;

                Done();
            }

            public bool HasCalledUpdaters = false;

            public void Done()
            {
                IsGenerating = false;
                HasCalledUpdaters = false;
                Step = 0;
                Block = null;
                CurrentAction.Data = null;
                GeneratingChunkId = 0;
                TotalGeneratedCount = 0;
                AroundChunksToGenerate.Clear();
                BuildPosition = 0;
                GeneratedChunks.Clear();
                CurrentAction = new BlockAction();
            }
        }

        public TerrainManager Manager;      // TerrainManager, contains TerrainContainer
        public List<VectorI3> Blocks;       // Blocks that must exist in world
        public bool BlocksDirty = false;    // True, blocks list have changed
        public Queue<BlockAction> Actions = new Queue<BlockAction>(); // Action list to execute
        public TerrainBlockBuilder BlockBuilder;

        public bool WasDone = true;
        public bool IsDone = true;

        public TerrainBuilder(TerrainManager Manager)
        {
            this.Manager = Manager;
            this.Blocks = new List<VectorI3>();
            this.BlockBuilder = new TerrainBlockBuilder(Manager);
        }

        public void LoadBlock(TerrainDatas Voxels)
        {
            Voxels.IsUsedByClient = true;

            BlockAction Action = new BlockAction();
            Action.Data = Voxels;
            Action.LocalPosition = Voxels.Position;
            Action.Remove = false;
            Actions.Enqueue(Action);
            BlocksDirty = true;

            if(GeneralLog.DrawLogs)
                GeneralLog.Log("TerrainBuilder: Load Block : " + Voxels.Position);

            ProgressSystem.Instance.SetProgress("Build Terrain : " + Voxels.Position.ToString(), 0, ThreadManager.UnityTime);
        }

        public void LoadBlocks(List<VectorI3> Blocks)
        {
            if (GeneralLog.DrawLogs)
            {
                String str = "";
                foreach (var v in Blocks)
                    str += v.ToString();
                GeneralLog.Log("TerrainBuilder: LoadBlocks : " + str);

            }

            this.Blocks.Clear();
            this.Blocks.AddRange(Blocks);
            BlocksDirty = true;
            //Debug.Log("TerrainBuilder: Blocks Dirty, count:" + Blocks.Count);
        }

        public void RemoveBlocks(ActionThread Thread)
        {
            BlockAction Action = new BlockAction();
            Action.Remove = true;
            for(int i=0;i<Manager.Container.Blocks.Count;++i)
            {
                if (!Blocks.Contains(Manager.Container.Blocks[i].LocalPosition))
                {
                    Action.LocalPosition = Manager.Container.Blocks[i].LocalPosition;
                    Actions.Enqueue(Action);
                }
            }
        }

        public bool UpdateUnity(ActionThread Thread)
        {
            if (!BlockBuilder.IsGenerating && ThreadManager.IsClientWorking)
                return false;

            if (IsDone)
            {
                if (!WasDone)
                {
                    TerrainManager.Instance.State = TerrainManager.LoadingStates.LOADED;
                    int Count = TerrainManager.Instance.Processors.Count;
                    for (int i = 0; i < Count; ++i)
                        TerrainManager.Instance.Processors[i].OnAllTerrainLoaded();
                    WasDone = true;
                }
            }
            else
                WasDone = false;

            BlockBuilder.UpdateUnity(Thread);
            return false;
        }

        public bool UpdateThread(ActionThread Thread)
        {
            if (!BlockBuilder.IsGenerating && ThreadManager.IsClientWorking)
                return false;

            if (Thread.ThreadId == 0)
            {
                float LoadingProgress = ProgressSystem.Instance.GetProgress(ThreadManager.UnityTime);
                IsDone = LoadingProgress >= 99.9f;

                if (!BlockBuilder.IsGenerating)
                {
                    if (BlocksDirty)
                    {
                        BlocksDirty = false;
                        RemoveBlocks(Thread);
                    }

                    if (Actions.Count != 0 && Blocks.Count != 0)
                    {
                        BlockBuilder.CurrentAction = Actions.Dequeue();
                        if (!BlockBuilder.CurrentAction.Remove)
                        {
                            if (!Blocks.Contains(BlockBuilder.CurrentAction.LocalPosition))
                            {
                                BlockBuilder.CurrentAction.Data.IsUsedByClient = false;
                                BlockBuilder.CurrentAction.Data = null;
                                return false;
                            }
                        }

                        Debug.Log("TerrainBuilder: Start generating block:" + BlockBuilder.CurrentAction.LocalPosition);
                        BlockBuilder.Step = 0;
                        BlockBuilder.IsGenerating = true;
                    }
                }
            }

            BlockBuilder.UpdateThread(Thread);

            return false;
        }
    }
}
