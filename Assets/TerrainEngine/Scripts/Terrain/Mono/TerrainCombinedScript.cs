﻿
using TerrainEngine;
using UnityEngine;

public class TerrainCombinedScript : TerrainSubScript
{
    public Transform CTransform;

    [HideInInspector]
    public Material[] Materials;

    void Awake()
    {
        CTransform = transform;
    }

    public override void Init(int ScriptHashCode,int SubScriptHashCode, IBlockProcessor Processor, ITerrainScript Script)
    {
        base.Init(ScriptHashCode,SubScriptHashCode, Processor, Script);
    }

    /*public void GenerateMesh(IMeshData Data)
    {
        CMesh.Clear(false);

        if (Data == null)
            return;

        CMesh.vertices = Data.Vertices;
        CMesh.subMeshCount = Data.Materials.Length;
        for (int i = 0; i < Data.Indices.Length; ++i)
            CMesh.SetTriangles(Data.Indices[i], i);
        CMesh.normals = Data.Normals;
        CMesh.colors = Data.Colors;
        CMesh.tangents = Data.Tangents;
        CMesh.uv = Data.UVs;
        if(Data.Normals == null)
            CMesh.RecalculateNormals();

        CRenderer.sharedMaterials = Data.Materials;
        Materials = Data.Materials;

        if (CRenderer.enabled == false)
            CRenderer.enabled = true;

        //PoolManager.UPool.CloseIndicesArray(Data.Indices);
    }*/

    public override bool CanCloseChunk()
    {
        return true;
    }

    public override void Close(bool Remove)
    {
        CRenderer.enabled = false;
        CTransform.parent = null;
        GameObjectPool.CloseGameObject(gameObject);
        base.Close(Remove);
    }
}
