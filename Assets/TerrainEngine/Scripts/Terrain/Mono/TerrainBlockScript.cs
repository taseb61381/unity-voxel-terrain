﻿using System;
using TerrainEngine;
using UnityEngine;

public class TerrainBlockScript : ITerrainScript
{
    [HideInInspector]
    public TerrainBlock CurrentBlock;
    public int BlockId;

    [NonSerialized]
    public Transform _transform;

    public void Relocate(TerrainBlock Block)
    {
        if (CurrentBlock != Block)
        {
            CurrentBlock = Block;
            BlockId = Block.BlockId;

            if (_transform == null)
                _transform = transform;

            if (ThreadManager.IsEditor)
                name = "Block:" + Block.BlockId + Block.LocalPosition;

            _transform.position = Block.WorldPosition;
            enabled = true;
        }
    }

    void Update()
    {
        if (CurrentBlock != null)
        {
            if (!CurrentBlock.HasState(TerrainBlockStates.CLOSED))
                CurrentBlock.Update();
        }
    }

    void LateUpdate()
    {
        if (CurrentBlock != null)
        {
            if (!CurrentBlock.HasState(TerrainBlockStates.CLOSED))
            {
                CurrentBlock.LODContainer.Draw(CurrentBlock.WorldPosition);
            }
        }
    }

    public override void Close()
    {
        base.Close();
        CurrentBlock = null;
        enabled = false;
    }

}