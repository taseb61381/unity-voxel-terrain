﻿using UnityEngine;
using TerrainEngine;

public class TerrainSubScript : MonoBehaviour
{
    public ITerrainScript Script;
    public MeshFilter CFilter;
    public MeshRenderer CRenderer;

    public int ScriptHashCode;
    public int SubScriptHashCode;

    public Mesh GetMesh(bool Create = true)
    {
        Mesh M = CFilter.sharedMesh;
        if (Create && M == null)
            CFilter.sharedMesh = M = CFilter.mesh;
        return M;
    }

    public virtual void Init(int ScriptHashCode,int SubScriptHashCode, IBlockProcessor Processor, ITerrainScript Script)
    {
        this.Script = Script;
        this.ScriptHashCode = ScriptHashCode;
        this.SubScriptHashCode = SubScriptHashCode;
        transform.parent = Script.transform;
        Script.AddSubScript(this);

        if (CFilter == null)
        {
            GameObject Obj = gameObject;
            CRenderer = Obj.GetComponent<MeshRenderer>();
            CFilter = Obj.GetComponent<MeshFilter>();

            if (CRenderer == null)
                CRenderer = Obj.AddComponent<MeshRenderer>();

            if (CFilter == null)
                CFilter = Obj.AddComponent<MeshFilter>();

        }
    }

    public virtual void Apply(IMeshData Data)
    {
        Mesh CMesh = null;

        if (Data == null || !Data.IsValid())
        {
            CMesh = GetMesh(false);
            if (CMesh != null)
                CMesh.Clear(false);

            if (CRenderer.enabled == true)
                CRenderer.enabled = false;

            return;
        }

        Data.Apply(GetMesh(true));
        CRenderer.sharedMaterials = Data.Materials;

        if (CRenderer.enabled == false)
            CRenderer.enabled = true;
    }

    public virtual bool CanCloseChunk()
    {
        return true;
    }

    public virtual void Close(bool Remove)
    {
        Mesh CMesh = GetMesh(false);
        if(CMesh != null)
            CMesh.Clear(false);

        CRenderer.enabled = false;
        transform.parent = null;
        GameObjectPool.CloseGameObject(gameObject);
        Script.RemoveSubScript(ScriptHashCode,SubScriptHashCode,Remove);
    }
}
