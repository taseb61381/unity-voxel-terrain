﻿using System;
using TerrainEngine;
using UnityEngine;

public class TerrainChunkScript : MonoBehaviour
{
    public int ChunkId;
    public TerrainChunkRef ChunkRef;

    [NonSerialized]
    public Transform CTransform;

    public MeshCollider CCollider;
    public MeshRenderer CRenderer;
    public MeshFilter CFilter;

    public Mesh GetMesh(bool Create=true)
    {
        Mesh M = CFilter.sharedMesh;
        if (Create && M == null)
            CFilter.sharedMesh = M = CFilter.mesh;
        return M;
    }

    public virtual bool Relocate(TerrainBlock Block, Vector3 WorldPosition, int ChunkId)
    {
        //if (ChunkRef.BlockId != Block.BlockId || ChunkRef.ChunkId != ChunkId)
        if ((object)CTransform == null)
            CTransform = transform;

        if (Block.Information.ForceHierarchyUpdate || ThreadManager.IsEditor)
        {
            name = ChunkId + ":" + WorldPosition.ToString();

            if (ChunkRef.BlockId != Block.BlockId)
            {
                CTransform.parent = Block.Script._transform;
            }
        }

        CTransform.position = WorldPosition;

        ChunkRef.Init(Block, ChunkId);

        return true;
    }

    public virtual void Apply(IMeshData Data)
    {
        Mesh CMesh = null;

        if (Data == null || !Data.IsValid())
        {
            CMesh = GetMesh(false);
            if (CMesh != null)
                CMesh.Clear(false);
            return;
        }

        Data.Apply(GetMesh(true));
        CRenderer.sharedMaterials = Data.Materials;
    }

    public void Close()
    {
        Mesh CMesh = GetMesh(false);
        if (CMesh != null)
            CMesh.Clear(false);
    }
}