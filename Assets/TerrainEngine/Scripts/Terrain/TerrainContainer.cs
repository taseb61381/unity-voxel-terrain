﻿using System.Collections.Generic;
using UnityEngine;


namespace TerrainEngine
{
    public class TerrainContainer : ITerrainContainer
    {
        public TerrainManager Manager;
        public TerrainInformation Information;
        public PoolInfoRef TerrainPool;
        public int ChunkPool;
        public GameObject DefaultChunkPrefab;

        public TerrainContainer(TerrainManager Manager,TerrainInformation Information)
        {
            this.Manager = Manager;
            this.Information = Information;
            ChunkPool = GameObjectPool.GetPoolId("Chunk");
            DefaultChunkPrefab = Manager.GetChunkPrefab();

            TerrainPool = PoolManager.Pool.GetPool("TerrainBlock");
            PoolManager.Pools.array[TerrainPool.Index].CanFreeMemory = false;
        }

        #region Blocks

        public void GetAllBlocks(ref HashSet<VectorI3> L)
        {
            L.Clear();

            lock (Blocks)
            {
                for (int i = 0; i < Blocks.Count; ++i)
                    L.Add(Blocks[i].LocalPosition);
            }
        }

        public TerrainBlock GetVerticalTerrain(VectorI3 Position)
        {
            TerrainBlock Block;
            for (int i = 0; i <= MaxId; ++i)
            {
                if (BlocksArray[i] != null)
                {
                    Block = BlocksArray[i] as TerrainBlock;
                    if (Block.LocalPosition.x == Position.x && Block.LocalPosition.z == Position.z)
                        return Block;
                }
            }

            return null;
        }

        public TerrainBlock CreateTerrain(VectorI3 Position, TerrainDatas Data)
        {
            if (Data == null)
                return null;

            int i;
            TerrainBlock Block = null;
            ITerrainBlock Block2 = null;
            VectorI3 AroundPos = new VectorI3();
            VectorI3 direction = new VectorI3();

            lock (Blocks)
            {
                if (GetBlock(Position, out Block2) && Block2.HasState( TerrainBlockStates.GENERATED ))
                {
                    if (GeneralLog.DrawLogs)
                        Debug.LogError("TerrainBlock Already exist : Original" + Position + ",Existing Local:" + Block2.LocalPosition + ",World:" + Block2.WorldPosition + ",Generated:" + Block2.HasState(TerrainBlockStates.GENERATED));
                    return null;
                }

                Block = PoolManager.Pools.array[TerrainPool.Index].GetData() as TerrainBlock;
                if (Block == null)
                {
                    Block = Manager.CreateTerrain();
                    PoolManager.Pools.array[TerrainPool.Index].SetData(Block);
                }

                Block.Init(Information, Position, Data.BlockId);
                Block.Voxels = Data;

                if (Manager != null)
                {
                    for (i = 0; i < Manager.Processors.Count; ++i)
                        Manager.Processors[i].OnCreateTerrain(Block);
                }

                for (i = BlockHelper.direction.Length - 1; i >= 0; --i)
                {
                    direction = BlockHelper.direction[i];
                    AroundPos.x = Position.x + direction.x;
                    AroundPos.y = Position.y + direction.y;
                    AroundPos.z = Position.z + direction.z;

                    if (GetBlock(AroundPos, out Block2))
                    {
                        Block[direction.x + 1, direction.y + 1, direction.z + 1] = Block2;
                        Block2[2 - (direction.x + 1), 2 - (direction.y + 1), 2 - (direction.z + 1)] = Block;
                    }
                }

                Block[1, 1, 1] = Block;
                Block.RelocateChunks();

                AddBlock(Block, Data.BlockId);

                if (Manager != null)
                {
                    for (i = 0; i < Manager.Processors.Count; ++i)
                        Manager.Processors[i].OnTerrainLoadingStart(Block);
                }
            }

            return Block;
        }

        public bool CloseThreadTerrain(VectorI3 Position, ref TerrainBlock Terrain)
        {
            lock (Blocks)
            {
                ITerrainBlock Block = null;
                if (!GetBlock(Position, out Block))
                    return false;

                Blocks.Remove(Block);
                Terrain = Block as TerrainBlock;
            }

            return true;
        }

        public void CloseUnityTerrain(TerrainBlock Terrain)
        {
            if (Terrain == null)
                return;

            Terrain.CloseUnity();
            RemoveBlock(Terrain, Terrain.BlockId);
            BlocksArray[Terrain.BlockId] = null;
            Terrain.Close();
        }

        #endregion

        #region Chunks

        public Queue<TerrainChunkScript> ChunkScripts = new Queue<TerrainChunkScript>();

        public TerrainChunkScript GetChunk(bool ForceNew = false)
        {
            TerrainChunkScript Script;

            if (ChunkScripts.Count == 0)
            {
                GameObject ChunkObj = GameObject.Instantiate(DefaultChunkPrefab) as GameObject;
                Script = ChunkObj.GetComponent<TerrainChunkScript>();
                if (Script == null)
                    ChunkObj.AddComponent<TerrainChunkScript>();

                ++GameObjectPool.Pools.array[ChunkPool].CreatedObject;
                return Script;
            }

            ++GameObjectPool.Pools.array[ChunkPool].DequeueObject;
            Script = ChunkScripts.Dequeue();
            Script.gameObject.SetActive(true);
            return Script;
        }

        public void CloseChunk(TerrainBlock Block, int ChunkId, TerrainChunkScript Script)
        {
            if ((object)Script == null)
                return;

            Script.Close();
            Script.ChunkRef = TerrainChunkRef.Null;

            Script.gameObject.SetActive(false);

            if (ThreadManager.IsEditor || Information.ForceHierarchyUpdate)
            {
                if (GameObjectPool.MainPoolObject == null)
                {
                    GameObjectPool.MainPoolObject = new GameObject().transform;
                    GameObjectPool.MainPoolObject.name = "(Pool)";
                }

                if (Script.CTransform == null)
                    Script.CTransform = Script.transform;
                Script.CTransform.parent = GameObjectPool.MainPoolObject;
            }

            ++GameObjectPool.Pools.array[ChunkPool].EnqueueObject;
            ChunkScripts.Enqueue(Script);
        }

        #endregion
    }
}
