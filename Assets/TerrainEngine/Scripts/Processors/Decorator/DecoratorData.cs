﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TerrainEngine
{
    public class DecoratorData : ICustomTerrainData
    {
        public OctreeChildsArray<int> NodesArray;

        public override void Init(TerrainDatas Terrain)
        {
            base.Init(Terrain);
        }

        public override void Clear()
        {
            for (int i = NodesArray.ClosedNodes.Length - 1; i >= 0; --i)
            {
                NodesArray.ClosedNodes[i] = false;
            }
        }

        public void InitOctree(DecoratorProcessor Processor)
        {
            NodesArray = new OctreeChildsArray<int>(Processor.NodesArray);
        }
    }
}
