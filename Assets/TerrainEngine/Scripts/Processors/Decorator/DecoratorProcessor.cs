﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace TerrainEngine
{
    public class DecoratorProcessor : IBlockProcessor
    {
        public Transform Viewer;
        public DecoratorContainer Container;
        public ExecuteAction UpdateLODAction;

        public override void OnInitManager(TerrainManager Manager)
        {
            InitOctree(Manager.Informations);
        }

        public override void OnBlockDataCreate(TerrainDatas Data)
        {
            DecoratorData CData = Data.GetCustomData<DecoratorData>(InternalDataName);
            if (CData == null)
            {
                CData = new DecoratorData();
                Data.RegisterCustomData(InternalDataName, CData);
                CData.InitOctree(this);

                if(UpdateLODAction == null)
                {
                    UpdateLODAction = new ExecuteAction("UpdateDecoratorLOD", UpdateLOD);
                    ActionProcessor.Add(UpdateLODAction, 0);
                }
            }
        }

        public void Update()
        {
            if (Viewer == null)
                return;

            CurrentPosition = Viewer.transform.position;
        }

        #region Octree

        public NodeLevelInformation NodeLevels;
        public OctreeChildsArray<int> NodesArray;

        public void InitOctree(TerrainInformation Informations)
        {
            if (NodeLevels == null)
                NodeLevels = new NodeLevelInformation();

            int MaxLevel = NodeLevels.SetMaxLevel(Informations.VoxelsPerAxis, Informations.VoxelPerChunk);
            NodeLevels.GenerateDistancePowers(Informations.VoxelsPerAxis, Informations.VoxelPerChunk);

            NodesArray = new OctreeChildsArray<int>(new OctreeNode<int>(0, 0, 0, -1), MaxLevel);
            NodesArray.Array[0].SubDivise(0, NodesArray, 0, 0, 0, 0, Informations.ChunkPerBlock, OnNodeSubDivise);
        }

        public void OnNodeSubDivise(int NodeIndex, int x, int y, int z, int Size)
        {

        }

        #endregion

        #region Update

        public float UpdateDistance = 8f;
        public Vector3 CurrentPosition;
        private Vector3 LastPosition;
        private List<ITerrainBlock> Blocks = new List<ITerrainBlock>();
        private ActionProgressCounter Progress = null;

        public bool UpdateLOD(ActionThread Thread)
        {
            if(Vector3.Distance(LastPosition,CurrentPosition) >= UpdateDistance)
            {
                LastPosition = CurrentPosition;
                UpdateNodes(Thread);
            }

            return false;
        }

        public void UpdateNodes(ActionThread Thread)
        {
            Blocks.Clear();
            TerrainManager.Instance.Container.GetBlocksByDistance(Blocks, CurrentPosition);
            ITerrainBlock Block = null;

            for(int i=0;i<Blocks.Count;++i)
            {
                Block = Blocks[i];

                if (Block == null || Block.HasState(TerrainBlockStates.CLOSED) || !Block.HasState(TerrainBlockStates.LOADED))
                    return;

                DecoratorUpdateAction Action = new DecoratorUpdateAction();
                Action.Processor = this;
                Action.Block = Block;

                ThreadManager.Instance.AddAction(Action);
            }
        }


        #endregion
    }

    public class DecoratorUpdateAction : IAction
    {
        public DecoratorProcessor Processor;
        public ITerrainBlock Block;
        public List<KeyValuePair<int, bool>> L;

        public override bool OnThreadUpdate(ActionThread Thread)
        {
            DecoratorData Data = Block.Voxels.GetCustomData<DecoratorData>();
            Debug.Log("Update Decorator : " + Data);
            return base.OnThreadUpdate(Thread);
        }
    }

    public enum DecoratorSide : byte
    {
        BOTTOM = 0,
        TOP = 1,
        SIZE = 2,
    }

    [Serializable]
    public class DecoratorObject
    {
        public int ObjectId;
        public int MaterialId;
        public Texture2D Texture;
        public MeshFilter Filter;

        public bool IsTexture = false;
    }

    [Serializable]
    public class DecoratorPreset
    {
        public int ObjectId;
        public string VoxelName;
        public DecoratorSide Side;
        public int MaxOctreeLevel = 0;
        public float Density = 100f;

        [HideInInspector]
        public int VoxelId;
    }

    [Serializable]
    public class DecoratorContainer
    {
        public float Density = 0f;
        public VoxelSizes ChunkSize = VoxelSizes.SIZE_16;
        public List<DecoratorObject> Objects;
        public List<DecoratorPreset> Presets;
    }
}
