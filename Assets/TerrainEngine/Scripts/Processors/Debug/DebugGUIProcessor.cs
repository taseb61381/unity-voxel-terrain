﻿using System;
using System.Text;
using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Processors/Debug GUI")]
public class DebugGUIProcessor : IBlockProcessor
{
    static public bool DrawGUI = true;
    static public DebugGUIProcessor Instance;

    public bool DrawDebugLog = true;
    public bool DrawDebugBar = true;
    public bool DrawDebugConsole = false;
    public bool IsDemo = true;
    public bool AutoFogDistanceCalculation = true;
    public long LastGCTime = 0;

    private StringBuilder Builder = new StringBuilder();

    private int StartCollection = 0;

    void Start()
    {
        Menus = new string[8] { "Logs", "Pool", "Stats", "TimeLine", "Profiler", "Refresh", "LOD", "Start Record" };

        Instance = this;
    }

    public override void OnInitManager(TerrainManager Manager)
    {
        base.OnInitManager(Manager);

        if (AutoFogDistanceCalculation)
        {
            RenderSettings.fogEndDistance = Mathf.Max(Manager.Informations.BlockSizeX, Manager.Informations.BlockSizeZ) * (Manager.Informations.MaxRange.x+1);
            RenderSettings.fogEndDistance -= RenderSettings.fogEndDistance * 0.01f;
            RenderSettings.fogStartDistance = RenderSettings.fogEndDistance - (RenderSettings.fogEndDistance / 10f);
        }
    }

    public override void OnStart()
    {
        StartCollection = GC.CollectionCount(0);

        if (Record)
            Init();
    }

    public override void OnAllTerrainLoaded()
    {
        if (StopWhenTerrainloaded)
            Record = false;
    }

    void DrawProgress()
    {
        if (TerrainManager.Instance.LoadingProgress > 0 && TerrainManager.Instance.LoadingProgress < 100f)
        {
            GUI.color = Color.blue;
            GUI.Button(new Rect(0, Screen.height - 22, (int)((float)Screen.width * TerrainManager.Instance.LoadingProgress / 100f), 22), "");
            GUI.color = Color.white;

            Rect Pos = new Rect(0, Screen.height - 22, (float)Screen.width, 22);
            GUI.Box(Pos, "[Loading : " + TerrainManager.Instance.LoadingProgress.ToString("000") + "%]");
            if (Pos.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)) && Event.current.type == EventType.repaint)
            {
                Pos.y = 30;
                Pos.width = 200;
                Pos.height = Screen.height - 22 - 30;
                ProgressSystem.Instance.DrawList(Pos);
            }

            float SubProgress = TerrainManager.Instance.SubProgress.GetProgress(ThreadManager.UnityTime);
            if (SubProgress != 100f)
            {
                GUI.color = Color.green;
                GUI.Button(new Rect(0, Screen.height - 44, Screen.width * SubProgress * 0.01f, 22), "");
                GUI.color = Color.white;

                GUI.Box(new Rect(0, Screen.height - 44, Screen.width, 22), "[" + TerrainManager.Instance.SubProgress.Name + " : " + SubProgress.ToString("000") + "%]");
            }
        }
    }

    void OnGUI()
    {
        DrawProgress();

        if (IsDemo)
            GUI.Box(new Rect(Screen.width / 2 - 150, Screen.height - 120, 300, 30), "TerrainEngine : DEMO");

        if (!DebugGUIProcessor.DrawGUI)
            return;

        if (!DrawDebugLog || TerrainManager.Instance == null || !TerrainManager.Instance.isInited)
            return;

        GUI.depth = -1;

        if (Event.current.type == EventType.repaint)
        {
            if (DrawDebugBar)
            {
                GUI.Box(new Rect(0, 0, Screen.width, 22), "");

                Builder.Length = 0;
                Builder.Append("[TerrainEngine] ");
                Builder.Append("[Blocks: ");
                Builder.Append(TerrainManager.Instance.Container.TerrainPool.CreatedObject);
                Builder.Append("][TData: ");
                Builder.Append(TerrainManager.Instance.Informations.TerrainDatasPool.CreatedObject);
                Builder.Append("][Voxels: ");
                Builder.Append(((long)TerrainManager.Instance.Container.TerrainPool.CreatedObject * (long)TerrainManager.Instance.Informations.VoxelsPerAxis * (long)TerrainManager.Instance.Informations.VoxelsPerAxis * (long)TerrainManager.Instance.Informations.VoxelsPerAxis));
                Builder.Append("][Chunks: ");
                Builder.Append(GameObjectPool.Pools.array[TerrainManager.Instance.Container.ChunkPool].CreatedObject);
                Builder.Append("][Gc.Col: ");
                Builder.Append(GC.CollectionCount(0) - StartCollection);
                Builder.Append("][GC.Time:");
                Builder.Append(LastGCTime);
                Builder.Append("][Render:");
                Builder.Append(ActionProcessor.Instance.RenderTime.ToString("00"));
                Builder.Append("][Update:");
                Builder.Append(ActionProcessor.Instance.UpdateTime.ToString("00"));
                Builder.Append("][LoadT:");
                Builder.Append(TerrainManager.Instance.LoadingTime.ToString("00"));
                Builder.Append("][LOD:");
                Builder.Append(GameObjectProcessor.GetStats());
                Builder.Append("]");
                

                if (ITerrainCameraHandler.Instance.HasCamera)
                    Builder.Append(ITerrainCameraHandler.Instance.GetMainCameraTObject().Position.ToString());

                GUI.Label(new Rect(0, 0, Screen.width, 22), Builder.ToString());
            }
        }

        bool NewValue = GUI.Toggle(new Rect(Screen.width - 150, 0, 150, 20),DrawDebugConsole, "Debug Console");

        if(NewValue != DrawDebugConsole)
        {
            DrawDebugConsole = NewValue;

            if (DrawDebugConsole)
            {
                if (DisableDebugOpen != null)
                {
                    for (int i = 0; i < DisableDebugOpen.Length; ++i)
                        if (DisableDebugOpen[i] != null)
                            DisableDebugOpen[i].enabled = false;
                }
            }
            else
            {
                if (DisableDebugOpen != null)
                {
                    for (int i = 0; i < DisableDebugOpen.Length; ++i)
                        if (DisableDebugOpen[i] != null)
                            DisableDebugOpen[i].enabled = true;
                }
            }
        }

        if (DrawDebugConsole)
            WindowPosition = GUI.Window(-51874053, WindowPosition, DrawDebugWindow, "Debug Console");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
            DrawGUI = !DrawGUI;

        if (Profiler != null)
            Profiler.Update();

        if (Record)
            ThreadManager.EnableStats = true;

        if(Input.GetKeyDown(KeyCode.G))
        {
            System.Diagnostics.Stopwatch Watch = new System.Diagnostics.Stopwatch();
            Watch.Start();
            GC.Collect();
            Watch.Stop();
            LastGCTime = Watch.ElapsedMilliseconds;
        }
    }

    #region Debug

    public bool Record = false;
    public bool StopWhenTerrainloaded = false;
    public Rect WindowPosition = new Rect(200,100,1000,800);
    public MonoBehaviour[] DisableDebugOpen;
    public int MenuId = 0;
    public string[] Menus;

    public int ThreadId = 0;
    public DataTableGUI[] Stats;
    public DataTableGUI[] Pools;
    public DataTableGUI LOD;
    public ProfilerGUI Profiler;
    public TimeLineGUI TimeLine;

    public void Init()
    {
        if (Stats != null)
            return;

        Stats = new DataTableGUI[ThreadManager.Instance.Threads.Length + 2];
        Pools = new DataTableGUI[ThreadManager.Instance.Threads.Length + 2];
        Profiler = new ProfilerGUI();
        TimeLine = new TimeLineGUI();

        for (int i = 0; i < Stats.Length; ++i)
        {
            Stats[i] = new DataTableGUI();
            Stats[i].AddColumn<string>("Name", 4f);
            Stats[i].AddColumn<long>("Time", 1f);
            Stats[i].AddColumn<long>("Count", 1f);
            Stats[i].AddColumn<long>("MaxTime", 1f);
            Stats[i].AddColumn<long>("LastTime", 1f);

            Pools[i] = new DataTableGUI();
            Pools[i].AddColumn<string>("Name", 4f);
            Pools[i].AddColumn<int>("Created", 1f);
            Pools[i].AddColumn<int>("Enqueue", 1f);
            Pools[i].AddColumn<int>("Dequeue", 1f);
            Pools[i].AddColumn<int>("Deleted", 1f);

            if (i == 0)
                TimeLine.AddLine("FastThread");
            else if (i == 1)
                TimeLine.AddLine("UnityThread");
            else
                TimeLine.AddLine("WorkerThread" + (i - 2));
        }

        LOD = new DataTableGUI();
        LOD.AddColumn<long>("X", 20);
        LOD.AddColumn<long>("Y", 20);
        LOD.AddColumn<float>("WaitTime", 30);
        LOD.AddColumn<int>("Objects", 25);

        ThreadManager.Instance.OnStat += OnStat;
    }

    /// <summary>
    /// Function called when a new action is executed. Duration(ms)
    /// </summary>
    public void OnStat(int ThreadId, string Name,long Duration,bool IsMain)
    {
        if (!Record)
            return;

        float ElapsedTime = (float)Duration / 1000f;
        float StartTime = (float)((DateTime.UtcNow - ThreadManager.Instance.StartTime).TotalMilliseconds * 0.001f) - ElapsedTime;

        Profiler.AddAction(ThreadId, Name);
        TimeLine.AddObject(ThreadId + 2, Name, StartTime, ElapsedTime, (byte)(IsMain ? 0 : 1));
    }

    public void RefreshStat(ActionThread Thread, DataTableGUI Table)
    {
        Thread.GetStats((ActionStats Action) =>
            {
                int ObjectId = Table.GetNewRawId();
                Table.AddValue<string>(ObjectId, "Name", Action.ActionName);
                Table.AddValue<long>(ObjectId, "Time", Action.TotalMSTime);
                Table.AddValue<long>(ObjectId, "Count", Action.ExecutionCount);
                Table.AddValue<long>(ObjectId, "MaxTime", Action.MaxTime);
                Table.AddValue<long>(ObjectId, "LastTime", Action.LastMSTime);
            });
        Table.Order();
    }

    public void RefreshStat(PoolManager Pool, DataTableGUI Table)
    {
        Pool.GetStats((string Name,int Created, int Enqueue,int Dequeue,int Deleted) =>
        {
            if (Created == 0)
                return;

            int ObjectId = Table.GetNewRawId();
            Table.AddValue<string>(ObjectId, "Name", Name);
            Table.AddValue<int>(ObjectId, "Created", Created);
            Table.AddValue<int>(ObjectId, "Enqueue", Enqueue);
            Table.AddValue<int>(ObjectId, "Dequeue", Dequeue);
            Table.AddValue<int>(ObjectId, "Deleted", Deleted);
        });
        Table.Order();
    }

    public void DrawDebugWindow(int Id)
    {
        int NewMenuId = GUI.SelectionGrid(new Rect(0, 20, WindowPosition.width, 20), MenuId, Menus, Menus.Length);
        if (Record)
            Menus[7] = "Stop Record";
        else
            Menus[7] = "Start Record";

        Rect Position = new Rect(2, 42, WindowPosition.width - 4, WindowPosition.height - 24);

        Init();

        if (NewMenuId == 5 || Stats[0].ObjectCount == 0)
        {
            for (int i = 0; i < Stats.Length; ++i)
            {
                Stats[i].Clear(false);
                Pools[i].Clear(false);

                if (i == 0)
                {
                    RefreshStat(ThreadManager.Instance.FastThread, Stats[i]);
                    RefreshStat(ThreadManager.Instance.FastThread.Pool, Pools[i]);
                }
                else if (i == 1)
                {
                    RefreshStat(ActionProcessor.Instance.UnityThread, Stats[i]);
                    RefreshStat(ActionProcessor.Instance.UnityThread.Pool, Pools[i]);
                    RefreshStat(PoolManager.Pool, Pools[i]);
                    GameObjectPool.GetStats((string Name, int Created, int Enqueue, int Dequeue, int Deleted) =>
                    {
                        int ObjectId = Pools[i].GetNewRawId();
                        Pools[i].AddValue<string>(ObjectId, "Name", Name);
                        Pools[i].AddValue<int>(ObjectId, "Created", Created);
                        Pools[i].AddValue<int>(ObjectId, "Enqueue", Enqueue);
                        Pools[i].AddValue<int>(ObjectId, "Dequeue", Dequeue);
                        Pools[i].AddValue<int>(ObjectId, "Deleted", Deleted);
                    });
                    Pools[i].Order();
                }
                else
                {
                    RefreshStat(ThreadManager.Instance.Threads[i - 2], Stats[i]);
                    RefreshStat(ThreadManager.Instance.Threads[i - 2].Pool, Pools[i]);
                }
            }
        }
        else if (NewMenuId == 7)
        {
            Record = !Record;
            ThreadManager.EnableStats = Record;
        }
        else
            MenuId = NewMenuId;

        if(MenuId == 0)
        {
            Position.width /= 2;

            GUI.Box(new Rect(Position.x, Position.y, Position.width, Position.height - Position.y),"Client");
            GUI.Box(new Rect(Position.x + Position.width, Position.y, Position.width, Position.height - Position.y), "Server");

            Position.height = 20;
            GUI.Label(Position, "Actions=" + TerrainManager.Instance.Builder.Actions.Count);
            Position.y += 20;

            GUI.Label(Position, "IsGenerating=" + TerrainManager.Instance.Builder.BlockBuilder.IsGenerating);
            Position.y += 20;

            if (TerrainManager.Instance.Builder.BlockBuilder.IsGenerating)
            {
                GUI.Label(Position, "Step=" + TerrainManager.Instance.Builder.BlockBuilder.Step);
                Position.y += 20;
                GUI.Label(Position, "Position=" + TerrainManager.Instance.Builder.BlockBuilder.CurrentAction.LocalPosition);
                Position.y += 20;
                GUI.Label(Position, "Removing=" + TerrainManager.Instance.Builder.BlockBuilder.CurrentAction.Remove);
                Position.y += 20;
                GUI.Label(Position, "ChunksToGenerate=" + TerrainManager.Instance.Builder.BlockBuilder.ChunksToGenerate);
                Position.y += 20;
                GUI.Label(Position, "GeneratedCount=" + TerrainManager.Instance.Builder.BlockBuilder.TotalGeneratedCount);
                Position.y += 20;
                GUI.Label(Position, "GeneratedChunks=" + TerrainManager.Instance.Builder.BlockBuilder.GeneratedChunks.Count);
                Position.y += 20;
                GUI.Label(Position, "GeneratingChunkId=" + TerrainManager.Instance.Builder.BlockBuilder.GeneratingChunkId);
                Position.y += 20;
                GUI.Label(Position, "AroundChunks=" + TerrainManager.Instance.Builder.BlockBuilder.AroundChunksToGenerate.Count);
                Position.y += 20;
                GUI.Label(Position, "BuildPosition=" + TerrainManager.Instance.Builder.BlockBuilder.BuildPosition);
                Position.y += 20;
            }

            Position.y = 42;
            Position.x += Position.width;
            if (TerrainManager.Instance.Server != null)
            {
                GUI.Label(Position, "IsGenerating=" + TerrainManager.Instance.Server.WorldGenerator.IsGenerating);
                Position.y += 20;

                if (TerrainManager.Instance.Server.WorldGenerator.IsGenerating)
                {
                    GUI.Label(Position, "Generating Blocks:" + TerrainManager.Instance.Server.WorldGenerator.GetGeneratingBlocksCount());
                    Position.y += 20;
                    GUI.Label(Position, "Position:" + TerrainManager.Instance.Server.WorldGenerator.GeneratingBlock.Block.LocalPosition);
                    Position.y += 20;
                    GUI.Label(Position, "Generated Chunks:" + TerrainManager.Instance.Server.WorldGenerator.GetGeneratedChunks());
                    Position.y += 20;
                    GUI.Label(Position, "Total Chunks To Generate:" + TerrainManager.Instance.Server.WorldGenerator.GetTotalChunksToGenerate());
                    Position.y += 20;

                    string ThreadsStates = TerrainManager.Instance.Server.WorldGenerator.ToString();
                    Position.height = GUI.skin.label.CalcHeight(new GUIContent(ThreadsStates), Position.width);
                    GUI.Label(Position, ThreadsStates);
                    Position.y += Position.height;
                }
            }
        }
        else if (MenuId == 1)
        {
            if (ThreadId > 0 && GUI.Button(new Rect(Position.x, Position.y, 50, 20), "<-"))
                --ThreadId;

            if (ThreadId == 0)
                GUI.Label(new Rect(Position.x + 50, Position.y, 200, 20), "FastThread:0/" + Stats.Length);
            else if (ThreadId == 1)
                GUI.Label(new Rect(Position.x + 50, Position.y, 200, 20), "UnityThread:1/" + Stats.Length);
            else
                GUI.Label(new Rect(Position.x + 50, Position.y, 200, 20), "WorkerThread:" + (ThreadId - 2) + "/" + Stats.Length);

            if (ThreadId < Stats.Length - 1 && GUI.Button(new Rect(Position.x + 200, Position.y, 50, 20), "->"))
                ++ThreadId;

            Position.y += 20;
            Position.height -= 20;
            Pools[ThreadId].OnGUI(WindowPosition, Position);
        }
        else if (MenuId == 2)
        {
            if (ThreadId > 0 && GUI.Button(new Rect(Position.x, Position.y, 50, 20), "<-"))
                --ThreadId;

            if (ThreadId == 0)
                GUI.Label(new Rect(Position.x + 50, Position.y, 200, 20), "FastThread:0/" + Stats.Length);
            else if (ThreadId == 1)
                GUI.Label(new Rect(Position.x + 50, Position.y, 200, 20), "UnityThread:1/" + Stats.Length);
            else
                GUI.Label(new Rect(Position.x + 50, Position.y, 200, 20), "WorkerThread:" + (ThreadId - 2) + "/" + Stats.Length);

            if (ThreadId < Stats.Length - 1 && GUI.Button(new Rect(Position.x + 200, Position.y, 50, 20), "->"))
                ++ThreadId;

            Position.y += 20;
            Position.height -= 20;
            Stats[ThreadId].OnGUI(WindowPosition, Position);
        }
        else if (MenuId == 3)
        {
            TimeLine.OnGUI(WindowPosition, Position, true, true);
        }
        else if (MenuId == 4)
        {
            Profiler.OnGUI(WindowPosition, Position);
        }
        else if(MenuId == 6)
        {
            if(Event.current.type == EventType.Repaint)
            {
                LOD.Clear(false);

                if (GameObjectProcessor.UseCellSystem)
                {
                    for (int i = 0; i < GameObjectProcessor.Cells.Count; ++i)
                    {
                        GameObjectClientDatas Data = GameObjectProcessor.Cells.array[i];
                        int Size = Data.CellSize;

                        for (int j = 0; j < Data.Cells.Count; ++j)
                        {
                            int RawId = LOD.GetNewRawId();

                            int WorldX = Data.Terrain.Position.x * Size + Data.Cells.array[j].x;
                            int WorldZ = Data.Terrain.Position.z * Size + Data.Cells.array[j].z;
                            LOD.AddValue<long>(RawId, 0, WorldX);
                            LOD.AddValue<long>(RawId, 1, WorldZ);
                            LOD.AddValue<float>(RawId, 2, Data.Cells.array[j].LastWaitTime);
                            LOD.AddValue<int>(RawId, 3, Data.Cells.array[j].Objects.Count);
                        }
                    }

                    LOD.Order();
                }
            }

            Position.y += 20;
            Position.height -= 20;
            LOD.OnGUI(WindowPosition, Position);
        }

        GUI.DragWindow();
    }

    #endregion
}
