﻿using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    /// <summary>
    /// This class manage actions executed on all threads for each frames
    /// </summary>
    public class ProfilerGUI
    {
        /// <summary>
        /// Action Information for profiler
        /// </summary>
        public struct ProfilerAction
        {
            public int ThreadId;
            public string Name;
            public int Count;

            public ProfilerAction(int ThreadId, string Name)
            {
                this.ThreadId = ThreadId;
                this.Name = Name;
                Count = 1;
            }

            public override string ToString()
            {
                return ThreadId + ":" + Name + " x " + Count;
            }
        }

        /// <summary>
        /// Contains list of actions executed on frame XXX
        /// </summary>
        public class ProfilerFrame
        {
            public int Frame;
            public float FPS;
            public List<ProfilerAction> Actions;

            public ProfilerFrame()
            {
                Actions = new List<ProfilerAction>(10);
            }

            public void Add(int ThreadId, string Name)
            {
                ProfilerAction Info;

                lock (Actions)
                {
                    for (int i = Actions.Count - 1; i >= 0; --i)
                    {
                        Info = Actions[i];
                        if (/*Info.ThreadId == ThreadId &&*/ Info.Name == Name)
                        {
                            Info.Count++;
                            Actions[i] = Info;
                            return;
                        }
                    }

                    Info = new ProfilerAction(ThreadId, Name);
                    Actions.Add(Info);
                }
            }
        }

        public ProfilerFrame[] Frames;

        public bool Pause = false;
        public int MaxFrames = 60; // Max recorded frames.
        public float MaxFPS; // Max FPS for all recoreded frames
        public float UpdateInterval = 0.1f; // Every 0.1 second record stats
        public Vector2 ScrollPosition;
        public int DrawFrameID = -1;

        private Texture2D GreenTexture;
        private Texture2D RedTexture;
        private Texture2D WhiteTexture;

        public ProfilerGUI()
        {
            SetTexture(ref GreenTexture, Color.green, false);
            SetTexture(ref RedTexture, Color.red, false);
            SetTexture(ref WhiteTexture, Color.white, false);

            if (Frames == null)
            {
                Frames = new ProfilerFrame[MaxFrames];
                for (int i = 0; i < Frames.Length; ++i)
                    Frames[i] = new ProfilerFrame();
            }
        }

        /// <summary>
        /// Create a texture used to draw a timeobject
        /// </summary>
        public void SetTexture(ref Texture2D Text, Color Col, bool border)
        {
            if (!border)
            {
                Text = new Texture2D(2, 2);
                Text.SetPixel(0, 0, Col);
                Text.SetPixel(0, 1, Col);
                Text.SetPixel(1, 0, Col);
                Text.SetPixel(1, 1, Col);
            }
            else
            {
                Text = new Texture2D(200, 2);

                for (int x = 0; x < 200; ++x)
                {
                    Text.SetPixel(x, 0, Col);
                    Text.SetPixel(x, 1, Col);
                }

                Text.SetPixel(0, 0, Color.black);
                Text.SetPixel(0, 1, Color.black);
                Text.SetPixel(1, 0, Color.black);
                Text.SetPixel(1, 1, Color.black);

                Text.SetPixel(198, 0, Color.black);
                Text.SetPixel(198, 1, Color.black);
                Text.SetPixel(199, 0, Color.black);
                Text.SetPixel(199, 1, Color.black);
            }
            Text.Apply();
        }

        /// <summary>
        /// Add a new action to the current frame
        /// </summary>
        public void AddAction(int ThreadId, string Name)
        {
            if (Pause)
                return;

            Frames[0].Add(ThreadId, Name);
        }

        /// <summary>
        /// Move to next frame. Update all actions
        /// </summary>
        public void MoveToNextFrame()
        {
            if (Frames == null)
                return;

            if (Pause)
                return;

            Frames[0].FPS = fps;
            Frames[0].Frame = Time.frameCount;

            MaxFPS = float.MinValue;

            for (int i = MaxFrames - 1; i >= 1; --i)
            {
                lock (Frames[i].Actions)
                {
                    Frames[i].Actions.Clear();
                    Frames[i].Actions.AddRange(Frames[i - 1].Actions);
                    Frames[i].FPS = Frames[i - 1].FPS;
                    Frames[i].Frame = Frames[i - 1].Frame;

                    if (Frames[i].FPS > MaxFPS)
                        MaxFPS = Frames[i].FPS;
                }
            }

            MaxFPS += 10f;

            lock (Frames[0].Actions)
            {
                Frames[0].Actions.Clear();
                Frames[0].Frame = 0;
                Frames[0].FPS = 0;
            }
        }

        /// <summary>
        /// Draw Profiler frames
        /// </summary>
        public void OnGUI(Rect WindowPosition, Rect Position)
        {
            if (Frames == null)
                return;

            if (Pause && GUI.Button(new Rect(Position.x, Position.y, 50, 20), "Play"))
                Pause = false;

            Rect P = Position;
            P.x += 5;
            P.y += 5;
            P.width -= 10;
            P.height -= 10;

            if (DrawFrameID != -1 && Pause)
                P.width -= (Position.width / 3);

            Rect Pos = new Rect();

            Pos.width = P.width / (float)MaxFrames;

            for (int i = 0; i < MaxFrames; ++i)
            {
                if (Frames[i].Frame != 0)
                {
                    Pos.height = P.height * (Frames[i].FPS / MaxFPS);

                    Pos.x = P.x + i * Pos.width;
                    Pos.y = P.y + (P.height - Pos.height);

                    GUI.DrawTexture(Pos, Frames[i].FPS < 30 ? RedTexture : GreenTexture);
                    if (GUI.RepeatButton(Pos, "", GUIStyle.none))
                    {
                        DrawFrameID = i;
                        Pause = true;
                    }
                }
            }

            DrawFPS(Position, P, 30);
            DrawFPS(Position, P, 60);
            DrawFPS(Position, P, 90);

            if (DrawFrameID != -1 && Pause)
            {
                Rect ListPos = new Rect();
                ListPos.x = P.x + P.width;
                ListPos.y = Position.y;
                ListPos.width = Position.width/3;
                ListPos.height = P.height;
                    int Actions = Frames[DrawFrameID].Actions.Count;

                GUI.Box(ListPos, "Actions:" + Actions);

                ListPos.y += 20;
                ScrollPosition = GUI.BeginScrollView(ListPos, ScrollPosition, new Rect(ListPos.x, ListPos.y, ListPos.width, Frames[DrawFrameID].Actions.Count * 20));


                for (int i = 0; i < Actions; ++i)
                {
                    GUI.Label(new Rect(ListPos.x+5, ListPos.y, ListPos.width-10, 20), Frames[DrawFrameID].Actions[i].ToString());
                    ListPos.y += 20;
                }

                GUI.EndScrollView();
            }
        }

        /// <summary>
        /// Draw FPS bar
        /// </summary>
        /// <param name="P"></param>
        /// <param name="Value"></param>
        public void DrawFPS(Rect Position, Rect P, float Value)
        {
            float SizeY = P.height * (Value / MaxFPS);
            GUI.contentColor = Color.blue;
            if ((Value / MaxFPS) < 1)
            {
                GUI.Label(new Rect(Position.x - 3, P.y + (P.height - SizeY) - 10, 20, 20), ((int)(Value)).ToString());
                GUI.DrawTexture(new Rect(Position.x + 15, P.y + (P.height - SizeY), Position.width - 20, 2), WhiteTexture);
            }
            GUI.contentColor = Color.white;
        }

        #region FPS

        private float lastInterval = 0; // Last interval end time
        private int frames = 0; // Frames over current interval
        public float fps; // Current FPS

        public void Update()
        {
            ++frames;
            float timeNow = Time.realtimeSinceStartup;
            if (timeNow > lastInterval + UpdateInterval)
            {
                fps = frames / (timeNow - lastInterval);
                frames = 0;
                lastInterval = timeNow;
                MoveToNextFrame();
            }
        }

        #endregion
    }
}
