﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TerrainEngine
{
    public class DataTableGUI
    {
        public abstract class DefaultColumn
        {
            public delegate void ColumnObjectDelegate(int Id, object Value);

            public string Name;
            public float WidthPower;

            public abstract void Sort(bool Ascending);

            public virtual void AddValue(int ObjectId, object Value)
            {

            }

            public abstract object GetValue(int Id);

            public abstract void GetAllObjects(ColumnObjectDelegate Call);

            public abstract void Clear();
        }

        public class Column<T> : DefaultColumn
        {
            public struct ColumnValue<T>
            {
                public int Id;
                public T Value;
            }

            public List<ColumnValue<T>> Values = new List<ColumnValue<T>>();

            public Column(string Name, float WidthPower)
            {
                this.Name = Name;
                this.WidthPower = WidthPower;
            }

            public override void AddValue(int ObjectId, object Value)
            {
                ColumnValue<T> Val = new ColumnValue<T>();
                Val.Id = ObjectId;
                Val.Value = (T)Value;
                Values.Add(Val);
            }

            public override object GetValue(int Id)
            {
                ColumnValue<T> Val;
                for (int i = Values.Count - 1; i >= 0; --i)
                {
                    Val = Values[i];
                    if (Val.Id == Id)
                        return Val.Value;
                }

                return "";
            }

            public override void GetAllObjects(ColumnObjectDelegate Call)
            {
                ColumnValue<T> Val;
                for (int i = 0; i < Values.Count; ++i)
                {
                    Val = Values[i];
                    Call(Val.Id, Val.Value);
                }
            }

            public override void Sort(bool Ascending)
            {
                Values = Values.OrderBy(Val => Val.Value).ToList();
                if (!Ascending)
                    Values.Reverse();
            }

            public override void Clear()
            {
                Values.Clear();
            }
        }

        public int ObjectCount = 0;
        public List<DefaultColumn> Columns = new List<DefaultColumn>(1);
        public DefaultColumn OrderColumn;
        public bool Ascending = false;
        public float TotalPower;

        public Vector2 ScrollPosition;
        public float TotalHeight;

        public void AddColumn<T>(string Name, float WidthPower)
        {
            Columns.Add(new Column<T>(Name, WidthPower));
            TotalPower += WidthPower;
        }

        public DefaultColumn GetColumn(string Name)
        {
            for (int i = Columns.Count - 1; i >= 0; --i)
            {
                if (Columns[i].Name == Name)
                    return Columns[i];
            }

            return null;
        }

        public int GetNewRawId()
        {
            return ObjectCount++;
        }

        public void AddValue<T>(int RawId, string ColumnName, T Value)
        {
            DefaultColumn Col = GetColumn(ColumnName);
            if (Col == null)
            {
                Debug.LogError("Invalid Column Name :" + ColumnName);
                return;
            }

            Col.AddValue(RawId, Value);
        }

        public void AddValue<T>(int RawId, int ColumnId, T Value)
        {
            if (ColumnId >= Columns.Count || ColumnId < 0)
            {
                Debug.LogError("Invalid Column Id : " + ColumnId);
                return;
            }

            Columns[ColumnId].AddValue(RawId, Value);
        }

        public void Clear(bool ClearColumns = false)
        {
            if(ClearColumns)
            {
                Columns.Clear();
                TotalPower = 0;
            }
            else
            {
                for(int i=Columns.Count-1;i>=0;--i)
                {
                    Columns[i].Clear();
                }
            }

            ObjectCount = 0;
            TotalHeight = 0;

            ScrollPosition.x = ScrollPosition.y = 0;
        }

        public void SetOrderColum(string Name)
        {
            SetOrderColumn(GetColumn(Name));
        }

        public void SetOrderColumn(DefaultColumn Col)
        {
            if (OrderColumn == Col)
                Ascending = !Ascending;

            OrderColumn = Col;
            Order();
        }

        public void Order()
        {
            if (OrderColumn != null)
                OrderColumn.Sort(Ascending);
        }

        public void OnGUI(Rect WindowPosition, Rect Position)
        {
            if (TotalPower == 0)
                return;

            Rect Pos = new Rect();
            Pos.x = Position.x;
            Pos.y = Position.y;

            GUIContent Content = new GUIContent();
            DefaultColumn Col;

            for (int i = 0; i < Columns.Count; ++i)
            {
                Col = Columns[i];

                Pos.width = (Col.WidthPower / TotalPower) * Position.width;
                Pos.height = Position.height;
                GUI.Box(Pos, "");

                Content.text = Col.Name;
                Pos.height = 20;
                if (GUI.Button(Pos, Content))
                {
                    SetOrderColumn(Col);
                }

                Pos.x += Pos.width;
            }

            Pos.x = Position.x;
            Pos.y = Position.y + 20;
            Pos.height = 20;

            ScrollPosition = GUI.BeginScrollView(new Rect(Position.x, Position.y + 20, Position.width, Position.height - 20), ScrollPosition, 
                new Rect(Position.x, Position.y + 20, Position.width, TotalHeight));

            (OrderColumn != null ? OrderColumn : Columns[0]).GetAllObjects((int Id, object Value) =>
            {
                Pos.x = Position.x;
                for (int i = 0; i < Columns.Count; ++i)
                {
                    Col = Columns[i];
                    Pos.width = (Col.WidthPower / TotalPower) * Position.width;

                    if (Col == OrderColumn)
                    {
                        Content.text = Value.ToString();
                    }
                    else
                    {
                        Content.text = Col.GetValue(Id).ToString();
                    }

                    GUI.Label(Pos, Content);
                    Pos.x += Pos.width;
                }

                Pos.y += 20;
            });
            TotalHeight = Pos.y - 20;

            GUI.EndScrollView();
        }
    }
}
