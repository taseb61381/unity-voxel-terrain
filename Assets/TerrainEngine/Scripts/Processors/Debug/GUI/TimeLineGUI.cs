﻿using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    /// <summary>
    /// This class manage events and return box containing information.
    /// </summary>
    public class TimeLineGUI
    {
        /// <summary>
        /// Object to draw on timeline. Contains Name/Time
        /// </summary>
        public class TimeLineObject
        {
            public string Name;
            public float StartTime;
            public float Duration;
            public SList<TimeLineObject> SubObjects;

            public bool ContainFilter(string Filter)
            {
                if (Name.Contains(Filter))
                    return true;

                for (int i = 0; i < SubObjects.Count; ++i)
                {
                    if (SubObjects.array[i].ContainFilter(Filter))
                        return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Contains all objects to draw one line.
        /// </summary>
        public class LineObjects
        {
            public string Name;
            public int LineId;
            public SList<TimeLineObject> Objects;
            public SList<TimeLineObject> TempObjects;

            public LineObjects()
            {
                Objects = new SList<TimeLineObject>(50);
                TempObjects = new SList<TimeLineObject>(50);
            }
        }

        /// <summary>
        /// Scale of the timeline. (MS, 2000 = 2seconds draw on the timeline)
        /// </summary>
        public float Scale = 2000f;
        
        /// <summary>
        /// Time of first object added
        /// </summary>
        private float StartTime = float.MaxValue;

        /// <summary>
        /// Time of last object added
        /// </summary>
        private float EndTime = float.MinValue;

        /// <summary>
        /// Filter Name. Remove all objects from lines that do not contain string
        /// </summary>
        public string Filter = "";

        /// <summary>
        /// TimeLine lines. Each line contains objects
        /// </summary>
        public List<LineObjects> Lines = new List<LineObjects>(1);

        public Texture2D RedTexture;
        public Texture2D GreenTexture;
        public GUIStyle Label;
        public Vector2 ScrollPosition;
        private float TotalWidth
        {
            get
            {
                return (EndTime - StartTime) * Scale;
            }
        }

        public TimeLineGUI()
        {
            SetTexture(ref RedTexture, Color.red, false);
            SetTexture(ref GreenTexture, Color.green, true);
        }

        /// <summary>
        /// Create a texture used to draw a timeobject
        /// </summary>
        public void SetTexture(ref Texture2D Text, Color Col, bool border)
        {
            if (!border)
            {
                Text = new Texture2D(2, 2);
                Text.SetPixel(0, 0, Col);
                Text.SetPixel(0, 1, Col);
                Text.SetPixel(1, 0, Col);
                Text.SetPixel(1, 1, Col);
            }
            else
            {
                Text = new Texture2D(200, 2);

                for (int x = 0; x < 200; ++x)
                {
                    Text.SetPixel(x, 0, Col);
                    Text.SetPixel(x, 1, Col);
                }

                Text.SetPixel(0, 0, Color.black);
                Text.SetPixel(0, 1, Color.black);
                Text.SetPixel(1, 0, Color.black);
                Text.SetPixel(1, 1, Color.black);

                Text.SetPixel(198, 0, Color.black);
                Text.SetPixel(198, 1, Color.black);
                Text.SetPixel(199, 0, Color.black);
                Text.SetPixel(199, 1, Color.black);
            }
            Text.Apply();
        }

        /// <summary>
        /// Create a new line in the timeline. Return class that contains objects on this line
        /// </summary>
        /// <returns></returns>
        public LineObjects AddLine(string Name)
        {
            LineObjects Objects = new LineObjects();
            Objects.Name = Name;
            Objects.LineId = Lines.Count;
            Lines.Add(Objects);
            return Objects;
        }

        /// <summary>
        /// Add a new object to the timeline. Must Provide a TimeLineId, (Id get from AddLine())
        /// </summary>
        public void AddObject(int LineId, string Name, float Start, float Duration, byte Level)
        {
            if (Duration == 0)
                return;

            LineObjects Line = Lines[LineId];

            TimeLineObject Obj = new TimeLineObject();
            Obj.Name = Name;
            Obj.StartTime = Start;
            Obj.Duration = Duration;

            if (Level != 0)
            {
                for (int i = Line.TempObjects.Count - 1; i >= 0; --i)
                {
                    Start = Line.TempObjects.array[i].StartTime;
                    if (Start >= Obj.StartTime && Start + Line.TempObjects.array[i].Duration < Obj.StartTime + Obj.Duration)
                    {
                        if (Obj.SubObjects.array == null)
                            Obj.SubObjects = new SList<TimeLineObject>(2);

                        Obj.SubObjects.Add(Line.TempObjects.array[i]);
                        Line.TempObjects.RemoveAt(i);
                    }
                }

                Line.TempObjects.Add(Obj);
            }
            else
            {
                if (Line.TempObjects.Count != 0)
                {
                    if (Obj.SubObjects.array == null)
                        Obj.SubObjects = new SList<TimeLineObject>(Line.TempObjects.Count);

                    for (int i = Line.TempObjects.Count - 1; i >= 0; --i)
                    {
                        Obj.SubObjects.Add(Line.TempObjects.array[i]);
                    }

                    Line.TempObjects.Clear();
                }

                Line.Objects.Add(Obj);
            }

            if (StartTime > Obj.StartTime)
                StartTime = Obj.StartTime;

            if (EndTime < 0.01f || EndTime < Obj.StartTime + Obj.Duration)
                EndTime = Obj.StartTime + Obj.Duration;
        }

        /// <summary>
        /// Add a new object to the timeline
        /// </summary>
        public void AddObject(LineObjects Line, string Name, float Start, float Duration, byte Level)
        {
            if (Duration == 0)
                return;

            TimeLineObject Obj = new TimeLineObject();
            Obj.Name = Name;
            Obj.StartTime = Start;
            Obj.Duration = Duration;

            if (Level != 0)
            {
                for (int i = Line.TempObjects.Count - 1; i >= 0; --i)
                {
                    Start = Line.TempObjects.array[i].StartTime;
                    if (Start >= Obj.StartTime-0.001f && Start + Line.TempObjects.array[i].Duration < (Obj.StartTime + Obj.Duration)+0.001f)
                    {
                        if (Obj.SubObjects.array == null)
                            Obj.SubObjects = new SList<TimeLineObject>(2);

                        Obj.SubObjects.Add(Line.TempObjects.array[i]);
                        Line.TempObjects.RemoveAt(i);
                    }
                }

                Line.TempObjects.Add(Obj);
            }
            else
            {
                if (Line.TempObjects.Count != 0)
                {
                    if (Obj.SubObjects.array == null)
                        Obj.SubObjects = new SList<TimeLineObject>(Line.TempObjects.Count);

                    for (int i = Line.TempObjects.Count - 1; i >= 0; --i)
                    {
                        Obj.SubObjects.Add(Line.TempObjects.array[i]);
                    }

                    Line.TempObjects.Clear();
                }

                Line.Objects.Add(Obj);
            }

            if (StartTime > Obj.StartTime)
                StartTime = Obj.StartTime;

            if (EndTime < 0.01f || EndTime < Obj.StartTime + Obj.Duration)
                EndTime = Obj.StartTime + Obj.Duration;
        }

        /// <summary>
        /// Draw TimeLine at desired Position. WindowPosition used to check mouse position inside window
        /// </summary>
        /// <param name="WindowPosition"></param>
        /// <param name="Position"></param>
        public void OnGUI(Rect WindowPosition, Rect Position, bool AllowMouseWheelScale = true, bool AllowMouseDrag = true)
        {
            if (Label == null)
            {
                Label = new GUIStyle(GUI.skin.label);
                Label.alignment = TextAnchor.MiddleCenter;
            }

            ///////////////////////// TIME BAR //////////////////////////////
            float Size = Position.width / 10f;
            float BarTimeStart = StartTime + ScrollPosition.x;
            Rect BarPosition = Position;
            for (int i = 0; i < 10; ++i) // Draw 10 vertical lines
            {
                if (i == 0 || i == 9)
                {
                    BarPosition.x = Position.x + Size * i + 5;
                    BarPosition.width = Size / 3;
                    BarPosition.height = 20;
                    GUI.Label(BarPosition, (BarTimeStart / Scale).ToString("#.###s"));
                }
                else
                {
                    BarPosition.x = Position.x + Size * i + 5;
                    BarPosition.width = Size;
                    BarPosition.height = 20;
                    GUI.Label(BarPosition, (BarTimeStart / Scale).ToString(".###s"));
                }

                BarPosition.width = 2;
                BarPosition.height = Position.height;
                GUI.DrawTexture(BarPosition, RedTexture);
                BarTimeStart += Size;
            }
            ///////////////////////// GUI EVENTS //////////////////////////////
            // If mouse wheel, change TimeLine scale
            if (Event.current.type == EventType.scrollWheel
                && Event.current.delta.y != 0)
            {
                Vector2 MousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
                float x = MousePos.x - WindowPosition.x;

                if (WindowPosition.Contains(MousePos))
                {
                    float MousePosPCT = ((ScrollPosition.x + x) / TotalWidth);

                    if (Event.current.delta.y > 0)
                        Scale /= 1.3f;
                    else
                        Scale *= 1.3f;
                    ScrollPosition.x = (MousePosPCT * TotalWidth) - x;
                }
            }
            else if (Event.current.type == EventType.mouseDrag)
            {
                Vector2 MousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

                if (new Rect(WindowPosition.x + Position.x, WindowPosition.y + Position.y, Position.width, Position.height).Contains(MousePos))
                {
                    ScrollPosition.x -= Event.current.delta.x;
                    Event.current.Use();
                }
            }
            

            ///////////////////////// DRAW LINES //////////////////////////////

            Position.y += 30;
            GUI.Label(new Rect(Position.x+10,Position.y,100,30),"Filter:");
            Filter = GUI.TextField(new Rect(Position.x + 120, Position.y, Position.width - Position.x - 120, 20), Filter);
            
            for (int i = 0; i < Lines.Count; ++i)
            {
                GUI.Box(new Rect(Position.x, Position.y + (20 + i * 34), Position.width - 10, 34), Lines[i].Name);
            }

            ScrollPosition = GUI.BeginScrollView(new Rect(Position.x, Position.y + 20, Position.width, Position.height - 20), ScrollPosition, new Rect(Position.x, Position.y + 20, Mathf.Max(Position.width, TotalWidth), Position.height - 40));

            for (int i = 0; i < Lines.Count; ++i)
            {
                DrawLine(Lines[i], WindowPosition, new Rect(Position.x, Position.y + (20 + i * 34 + 2), Position.width - 10, 30));
            }

            GUI.EndScrollView();
        }

        public void DrawLine(LineObjects Line, Rect WindowPosition, Rect LinePosition)
        {
            GUI.contentColor = Color.black;
            DrawChildObjects(WindowPosition, LinePosition, ref Line.Objects, 0);
            GUI.contentColor = Color.white;
        }

        public int DrawChildObjects(Rect WindowPosition,Rect LinePosition, ref SList<TimeLineObject> Objects, int Level)
        {
            if (Objects.Count == 0)
                return 0;

            Vector2 MousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

            bool IsOnMouse = false;
            Rect P = new Rect();
            P.y = LinePosition.y;
            P.height = LinePosition.height;


            int Count = 0; // Number of objects draw
            int SubCount = 0; // Number of child objects draw
            float TextWidth = 0;
            GUIContent Content = new GUIContent();
            TimeLineObject Obj = null;

            for (int i = Objects.Count - 1; i >= 0; --i)
            {
                Obj = Objects.array[i];

                P.x = LinePosition.x + (Obj.StartTime - StartTime) * Scale;
                P.width = Obj.Duration * Scale;
                if(P.width < 2)
                {
                    if((Filter == null || Filter.Length == 0))
                        P.width = 2;
                    else
                        continue;
                }

                if (Level == 0)
                {
                    if (P.x > (LinePosition.width + ScrollPosition.x) || (P.x + P.width - ScrollPosition.x) < 0)
                    {
                        continue;
                    }
                }

                if (Filter != null && Filter.Length > 0)
                {
                    if (!Obj.ContainFilter(Filter))
                        continue;
                }

                Content.text = Obj.Name;
                TextWidth = Label.CalcSize(Content).x;

                IsOnMouse = (MousePos.x > P.x + WindowPosition.x - ScrollPosition.x
                    && MousePos.y > P.y + WindowPosition.y - ScrollPosition.y
                    && MousePos.x < P.x + WindowPosition.x + P.width - ScrollPosition.x
                    && MousePos.y < P.y + WindowPosition.y + 30 - ScrollPosition.y);

                if (IsOnMouse|| P.width >= 300)
                {
                    SubCount = DrawChildObjects(WindowPosition, LinePosition, ref Obj.SubObjects, Level+1);
                    if (SubCount == 0)
                    {
                        GUI.DrawTexture(P, GreenTexture);
                        GUI.Label(P, Obj.Name + "|" + (Obj.Duration * 1000f) + "ms", Label);
                        ++SubCount;
                    }

                    Count += SubCount;
                }
                else
                {
                    GUI.DrawTexture(P, GreenTexture);

                    if (P.width >= 250)
                        GUI.Label(P, Obj.Name + "|" + (Obj.Duration * 1000f) + "ms", Label);
                    else
                        GUI.Label(P, Obj.Name, Label);
                    ++Count;
                }
            }

            return Count;
        }
    }
}
