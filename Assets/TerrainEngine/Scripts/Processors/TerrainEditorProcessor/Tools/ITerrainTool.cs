﻿using UnityEngine;
using System.Collections;

namespace TerrainEngine
{
    public abstract class ITerrainTool : MonoBehaviour
    {
        public int DownMouse = -1;
        public KeyCode[] DownKeys;

        public int PressedMouse = -1;
        public KeyCode[] PressedKeys;

        /// <summary>
        /// Return true if pressed keys are valid to use this tool
        /// </summary>
        /// <returns></returns>
        public virtual bool IsValidKeys(ITerrainToolContainer Container)
        {
            if ((DownKeys == null || DownKeys.Length == 0) 
                && (PressedKeys == null || PressedKeys.Length == 0)
                && DownMouse == -1 
                && PressedMouse == -1)
                return false;

            if(DownMouse != -1)
            {
                if (!Input.GetMouseButtonDown(DownMouse))
                    return false;
            }

            if (PressedMouse != -1)
            {
                if (!Input.GetMouseButton(PressedMouse))
                    return false;
            }

            if (DownKeys != null)
            {
                for (int i = DownKeys.Length - 1; i >= 0; --i)
                {
                    if (!Input.GetKeyDown(DownKeys[i]))
                        return false;
                }
            }

            if (PressedKeys != null)
            {
                for (int i = PressedKeys.Length - 1; i >= 0; --i)
                {
                    if (!Input.GetKey(PressedKeys[i]))
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Called by tool when IsValidKeys return true.
        /// Return false to abord modification.
        /// </summary>
        public abstract bool OnModificationStart(ITerrainToolContainer Container);

        /// <summary>
        /// Called by tool when IsValidKeys return true.
        /// Return false to abord modification.
        /// </summary>
        public abstract bool OnModificationUpdate(ITerrainToolContainer Container);

        /// <summary>
        /// Called by tool when IsValidKeys return true.
        /// Return false to abord modification.
        /// </summary>
        public abstract bool OnModificationEnd(ITerrainToolContainer Container);
    }
}
