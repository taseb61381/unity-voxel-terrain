﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


namespace TerrainEngine
{
    /// <summary>
    /// Contains tools for a specified processor type
    /// </summary>
    public class ITerrainToolContainer : MonoBehaviour
    {
        [Serializable]
        public class ToolInfo
        {
            public KeyCode Key = KeyCode.Alpha1;
            public int SizeX = 1, SizeY = 1, SizeZ = 1;
            public int TypeId = 1, TextureId = 1, Rotation = 0;
            public float Volume = 1f;
            public VoxelModificationType ModType = VoxelModificationType.SET_TYPE_SET_VOLUME;
        }

        /// <summary>
        /// ToolProcessor that contains this Container
        /// </summary>
        [HideInInspector]
        public ToolProcessor Processor;


        /// <summary>
        /// First Key to press to select this Container
        /// </summary>
        public KeyCode DownKey0 = KeyCode.LeftControl;

        /// <summary>
        /// Second Key to press to select this Container
        /// </summary>
        public KeyCode DownKey1 = KeyCode.Alpha1;

        public ToolInfo[] Values;
        public ITerrainTool[] Tools;
        public ITerrainTool CurrentTool;

        public void OnInit()
        {

        }

        public virtual void OnSelected()
        {

        }

        public virtual void OnUpdate()
        {
            if (CurrentTool == null)
            {
                CurrentTool = GetValidTool();
                if (CurrentTool != null)
                {
                    if (CurrentTool.OnModificationStart(this))
                    {
                        Processor.GetRaycast(true);
                    }
                    else
                        CurrentTool = null;
                }
            }

            if (CurrentTool != null)
            {
                Processor.GetRaycast(false);
                CurrentTool.OnModificationUpdate(this);
            }

            if (!CurrentTool.IsValidKeys(this))
            {
                Processor.GetRaycast(false);
                CurrentTool.OnModificationEnd(this);
                CurrentTool = null;
            }
        }

        public virtual void OnUnselected()
        {

        }

        public bool IsKeyPressed()
        {
            if (DownKey0 != KeyCode.None)
                if (!Input.GetKeyDown(DownKey0))
                    return false;

            if (DownKey1 != KeyCode.None)
                if (!Input.GetKeyDown(DownKey1))
                    return false;

            return true;
        }

        public ITerrainTool GetValidTool()
        {
            for (int i = 0; i < Tools.Length; ++i)
                if (Tools[i].IsValidKeys(this))
                    return Tools[i];
            return null;
        }
    }
}