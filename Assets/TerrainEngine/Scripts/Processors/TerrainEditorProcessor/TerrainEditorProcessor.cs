﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace TerrainEngine
{
    public class ToolProcessor : IBlockProcessor 
    {
        [Serializable]
        public class RaycastPoint
        {
            public TerrainBlock Block = null;
            public Vector3 Position = new Vector3();
            public Vector3 Normal = new Vector3();
        }

        /// <summary>
        /// Camera to use for raycast. If none, Camera.main is used
        /// </summary>
        public Camera MainCamera;

        /// <summary>
        /// List of tools container. Each container contains a tool list for a specified processor.
        /// </summary>
        public ITerrainToolContainer[] Containers;
        public ITerrainToolContainer CurrentContainer;
        public bool CenterRaycast = false;


        public void Start()
        {
            if (Containers == null || Containers.Length == 0)
                Debug.LogError("TerrainEditorProcessor: No Tool Container");

            if (Containers != null)
            for (int i = 0; i < Containers.Length; ++i)
            {
                Containers[i].Processor = this;
                Containers[i].OnInit();
            }
        }

        public void Update()
        {
            MainCamera = Camera.main;

            if (MainCamera == null)
                return;


            if (Input.anyKeyDown)
            {
                ITerrainToolContainer NewContainer = GetContainer();
                if (NewContainer != null)
                {
                    if (CurrentContainer != null && CurrentContainer != NewContainer)
                        CurrentContainer.OnUnselected();

                    CurrentContainer = NewContainer;
                    CurrentContainer.OnSelected();
                }
            }

            if (CurrentContainer != null)
                CurrentContainer.OnUpdate();
        }

        public ITerrainToolContainer GetContainer()
        {
            if (Containers != null)
            for (int i = 0; i < Containers.Length; ++i)
                if (Containers[i].IsKeyPressed())
                    return Containers[i];

            return null;
        }

        #region Raycasting

        [HideInInspector]
        public RaycastPoint StartPoint = new RaycastPoint(), CurrentPoint = new RaycastPoint();

        public bool GetRaycast(bool IsStart)
        {
            if (MainCamera == null)
                return false;

            Ray r = GetComponent<Camera>().ScreenPointToRay(CenterRaycast ? new Vector3(Screen.width / 2, Screen.height / 2, 0) : Input.mousePosition);
            RaycastHit Hit;
            if (Physics.Raycast(r, out Hit, 100))
            {
                TerrainChunkScript Script = TerrainManager.GetGameObjectChunk(Hit.collider.gameObject);
                if (Script == null)
                    return false;

                if (IsStart)
                {
                    StartPoint.Position = Hit.point;
                    StartPoint.Normal = Hit.normal;
                    return Script.ChunkRef.IsValid(ref StartPoint.Block);
                }
                else
                {
                    CurrentPoint.Position = Hit.point;
                    CurrentPoint.Normal = Hit.normal;
                    return Script.ChunkRef.IsValid(ref CurrentPoint.Block);
                }
            }

            return false;
        }

        #endregion
    }
}