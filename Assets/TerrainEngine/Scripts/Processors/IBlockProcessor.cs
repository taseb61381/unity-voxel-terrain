﻿using System;
using System.Collections.Generic;

using TerrainEngine;

using UnityEngine;

[Serializable]
public abstract class IBlockProcessor : MonoBehaviour
{
    [NonSerialized]
    public string InternalName; // Name of current GameObject

    /// <summary>
    /// Data Storage name.  Used for save terrain.
    /// </summary>
    public string DataName = "";

    [HideInInspector]
    public string InternalDataName = "";

    [HideInInspector]
    public byte UniqueId = 255; // Unique Processor Id. Set by TerrainManager

    /// <summary>
    /// Priority in the list of processors. 0->1->2->etc.. Priority is get from the gameobject name number-name : 0-SmoothBiome, 1-TreeProcessor,etc...
    /// </summary>
    public int Priority = 0;

    [HideInInspector]
    public bool EnableUnityUpdateFunction = false; // Call OnTerrainBlockUnityUpdate
    [HideInInspector]
    public bool EnableThreadUpdateFunction = false; // Call OnTerrainBlockThreadUpdate
    [HideInInspector]
    public bool InterlacedUpdate = false;           // UnityUpdate and ThreadUpdate must be alternating
    [HideInInspector]
    public bool EnableGenerationFunction = true;    // True is this processor need to use OnBlockGenerate function
    [HideInInspector]
    internal bool HasGenerator = false;             // Has Generator on client server. If true, OnGenerateBlock will not be called on client side. Function is already called on TerrainWorldServer

    [HideInInspector]
    public int NextThreadId = 0; // Next Thread Id to use for Updaters. Dispatch all updaters between all threads

    [NonSerialized]
    public Queue<ProcessorVoxelPoint> CustomPoints = new Queue<ProcessorVoxelPoint>();  // Custom voxel data, used for modify objects on custom systems like GameObjects,CosntructionKit,etc..

    [NonSerialized]
    public List<TerrainObject> TerrainObjects = new List<TerrainObject>();  // Current TerrainObject using this Processor. Used by IViewableProcessor for check hum much objects are in range of each chunk and draw them of not

    // Do not override
    void OnEnable()
    {
        if (transform.parent == null)
            return;

        if (!Application.isPlaying)
        {
            if (Priority == 0)
            {
                string[] names = name.Split('-');
                if (names.Length <= 1 || !int.TryParse(names[0], out Priority))
                    Debug.LogWarning("Invalid  processor name. Name must be : Priority-name. Example : 0-SmoothBiome,1-TreeProcessor,etc...");
            }
        }

        if (Application.isPlaying)
        {
            TerrainManager.Instance = transform.parent.GetComponent<TerrainManager>();
            if (TerrainManager.Instance != null)
                TerrainManager.Instance.AddProcessor(this);
            else
            {
                if (transform.parent.parent != null)
                    TerrainManager.Instance = transform.parent.parent.GetComponent<TerrainManager>();

                if (TerrainManager.Instance != null)
                    TerrainManager.Instance.AddProcessor(this);
                else
                    Debug.LogError("Processor : Manager not found");
            }

            InternalName = name;
        }
    }

    // Do not override
    void OnDisable()
    {
        if (TerrainManager.Instance != null)
            TerrainManager.Instance.RemoveProcessor(this);
    }

    void Awake()
    {
        if (Application.isPlaying)
            return;

        if (Priority == 0)
        {
            string[] names = name.Split('-');
            if (names.Length <= 1 || !int.TryParse(names[0], out Priority))
                Debug.LogWarning("Invalid  processor name. Name must be : Priority-name. Example : 0-SmoothBiome,1-TreeProcessor,etc...");
            DataName = names[1];

            gameObject.name = ToString();
            gameObject.SetActive(false);
        }
    }

    public void ActiveUpdaters(TerrainBlock Block)
    {
        if (InterlacedUpdate || EnableThreadUpdateFunction || EnableUnityUpdateFunction)
        {
            if (Block.ProcessorUpdate == null || Block.ProcessorUpdate.IsStopped)
            {
                if (Block.ProcessorUpdate == null)
                    Block.ProcessorUpdate = new TerrainBlockProcessorUpdateAction();

                Block.ProcessorUpdate.Init(Block);

                for (int i = 0; i < ThreadManager.Instance.Threads.Length; ++i)
                {
                    ThreadManager.Instance.Threads[i].AddAction(Block.ProcessorUpdate);
                }

                ActionProcessor.AddToUnity(Block.ProcessorUpdate);
            }

            Block.ProcessorUpdate.AddProcessor(this);
        }
    }

    #region Init

    /// <summary>
    /// [UnityThread] Called when TerrainManager Start()
    /// </summary>
    /// <param name="Manager">Current Manager</param>
    public virtual void OnInitManager(TerrainManager Manager)
    {

    }

    /// <summary>
    /// [UnityThread] Called when TerrainManager Start()
    /// </summary>
    /// <param name="Seed">Current seed of the world</param>
    public virtual void OnInitSeed(int Seed)
    {

    }

    /// <summary>
    /// [UnityThread] Called before starting the TerrainManager.
    /// </summary>
    /// <returns>True if manager can start</returns>
    public virtual bool CanStart()
    {
        return true;
    }

    /// <summary>
    /// [UnityThread] Called when all Processors return CanStart() true and TerrainManager is Loaded
    /// </summary>
    public virtual void OnStart()
    {

    }

    /// <summary>
    /// [UnityThread] Called when processor is disable or TerrainManager disable
    /// </summary>
    public virtual void OnStop()
    {

    }

    #endregion

    #region Generation

    /// <summary>
    /// [OtherThread] Called when BlockContainer is loading/generating
    /// </summary>
    /// <param name="Data"></param>
    public virtual void OnBlockDataCreate(TerrainDatas Data)
    {

    }

    /// <summary>
    /// [UnityThread] Called When BlockContainer is closing
    /// </summary>
    /// <param name="Data"></param>
    public virtual void OnBlockDataClose(TerrainDatas Data)
    {

    }

    public virtual IWorldGenerator GetGenerator()
    {
        return null;
    }

    /// <summary>
    /// [OtherThread] Called when block is generating. Used for terrain biome generation.
    /// </summary>
    public virtual bool Generate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        return true;
    }

    /// <summary>
    /// [OtherThread] Return the type of generator. Override this function to change the call order. Terrain to be called first, custom called after heightmap generated, ground, called last when ground points are generated
    /// </summary>
    public virtual IWorldGenerator.GeneratorTypes GetGeneratorType()
    {
        return IWorldGenerator.GeneratorTypes.TERRAIN;
    }

    #endregion

    #region Terrain

    /// <summary>
    /// [OtherThread] Called When a new Terrain class is created.
    /// </summary>
    /// <param name="Blocks"></param>
    public virtual void OnCreateTerrain(TerrainBlock Terrain)
    {

    }

    /// <summary>
    /// [OtherThread] Called When Terrain loading start.
    /// </summary>
    /// <param name="Blocks"></param>
    public virtual void OnTerrainLoadingStart(TerrainBlock Terrain)
    {

    }

    public virtual bool OnTerrainIsReadyToDraw(TerrainBlock Terrain)
    {
        return true;
    }

    /// <summary>
    /// [UnityThread] Called When Terrain loading done. All chunks are builded.
    /// </summary>
    public virtual void OnTerrainLoadingDone(TerrainBlock Terrain)
    {

    }

    /// <summary>
    /// [OtherThread] Called  When terrain is closing. Used for non-Unity object suppression.
    /// </summary>
    /// <param name="Terrain"></param>
    public virtual void OnTerrainThreadClose(TerrainBlock Terrain)
    {

    }

    /// <summary>
    /// [UnityThread] Called  When terrain is closing. Last call, after the terrain is removed.
    /// </summary>
    /// <param name="Terrain"></param>
    public virtual void OnTerrainUnityClose(TerrainBlock Terrain)
    {

    }

    /// <summary>
    /// [UnityThread] Called When there are no more terrain to load. Can be called several times.
    /// </summary>
    public virtual void OnAllTerrainLoaded()
    {

    }

    /// <summary>
    /// [OtherThread] Called after voxels have been modifed (Block.Flush())
    /// </summary>
    /// <param name="Obj">Object who is modifying the terrain</param>
    /// <param name="Voxels">List of all voxels modified</param>
    public virtual void OnFlushVoxels(ActionThread Thread, TerrainObject Obj, ref  VoxelFlush Voxel, TerrainChunkGenerateGroupAction Action)
    {

    }

    /// <summary>
    /// [OtherThread] Called when chunk is modified by a flush and need to be rebuild
    /// </summary>
    public virtual void OnFlushChunk(ActionThread Thread, TerrainChunkRef ChunkRef)
    {

    }

    /// <summary>
    /// [OtherThread] Called before modifying voxels. Return True if voxel can be changed.
    /// Used for make voxels non-removable. For example, if there is tree on this voxel, if it's the minimal position for dig or if player doesn't use the correct item for remove it.
    /// </summary>
    /// <param name="Obj">Object who is modifying the terrain</param>
    /// <param name="Type">Current Type</param>
    /// <param name="Volume">Current Volume</param>
    /// <param name="NewType">New Type</param>
    /// <param name="NewVolume">New Volume</param>
    /// <returns>True if change is accepted. False the voxels will not be changed</returns>
    public virtual bool CanModifyVoxel(TerrainObject Obj, ref VoxelFlush Flush)
    {
        return true;
    }

    /// <summary>
    /// Update called by ActionManager. Return true if this action can be passed to ThreadUpdate .(Used by Interlaced system)
    /// </summary>
    public virtual bool OnTerrainUnityUpdate(ActionThread Thread, TerrainBlock Terrain)
    {
        return true;
    }

    /// <summary>
    /// Update called by ActionManager. Return true if this action can be passed to ThreadUpdate .(Used by Interlaced system)
    /// </summary>
    public virtual bool OnTerrainThreadUpdate(ActionThread Thread, TerrainBlock Terrain)
    {
        return true;
    }

    #endregion

    #region Chunks

    /// <summary>
    /// [OtherThread] Called after all chunks begins to be builded
    /// </summary>
    /// <param name="Terrain"></param>
    public virtual void OnChunksGenerationDone(TerrainBlock Terrain)
    {

    }
    
    public virtual void OnChunksStartBuilding(TerrainBlock Terrain)
    {

    }

    /// <summary>
    /// [UnityThread] Called after a chunk mesh have been builded
    /// </summary>
    /// <param name="Chunk"></param>
    public virtual void OnChunkBuild(TerrainBlock Block, int ChunkId, IMeshData Data)
    {

    }

    /// <summary>
    /// [UnityThread] Called after a chunk mesh have been builded for the FIRST TIME
    /// </summary>
    /// <param name="Chunk"></param>
    public virtual void OnFirstChunkBuild(TerrainBlock Block, int ChunkId, IMeshData Data)
    {

    }

    /// <summary>
    /// [OtherThread] Called after a chunk mesh have been generated for the FIRST TIME
    /// </summary>
    /// <param name="Chunk"></param>
    public virtual void OnFirstChunkGenerate(ActionThread Thread,TerrainBlock RebuildingBlock, TerrainBlock Block, int ChunkId, IMeshData Data)
    {

    }

    /// <summary>
    /// Function to call when chunk is changed. Used only by ColliderProcessor to update instantly chunk Collider. Called By PhysicsProcessor
    /// </summary>
    public virtual void OnChunkModification(TerrainBlock Block, int ChunkId)
    {

    }

    /// <summary>
    /// Function called by TerrainManager when terrain LOD has changed and need a rebuild
    /// </summary>
    public virtual void OnTerrainLODUpdate(TerrainChunkGenerateAndBuildGroup Group,int Level)
    {

    }

    #endregion

    #region Objects

    /// <summary>
    /// [UnityThread] Called when new object is added on the Terrain
    /// </summary>
    /// <param name="Obj"></param>
    public virtual void OnObjectAdd(TerrainObject Obj)
    {

    }

    /// <summary>
    /// [UnityThread] Called when object is removed from the terrain
    /// </summary>
    /// <param name="Obj"></param>
    public virtual void OnObjectRemove(TerrainObject Obj)
    {

    }

    /// <summary>
    /// [UnityThread] Called when object has changed Terrain
    /// </summary>
    /// <param name="Obj"></param>
    /// <param name="OldBlock"></param>
    public virtual void OnObjectMoveTerrain(TerrainObject Obj)
    {

    }

    /// <summary>
    /// [UnityThread] Called when objct has changed Chunk
    /// </summary>
    /// <param name="Obj"></param>
    /// <param name="OldChunk"></param>
    public virtual void OnObjectMoveChunk(TerrainObject Obj)
    {

    }

    #endregion

    #region Pool

    public PoolManager Pool
    {
        get
        {
            return PoolManager.Pool;
        }
    }

    #endregion

    #region CustomProcessor Data

    public delegate bool CanModifyPoint(ref TerrainBlock Block, ref int x, ref int y, ref int z);

    public enum ProcessorVoxelChangeType : byte
    {
        SET = 0,
        ROTATE = 1,
        SCALE = 2,
    };

    public struct ProcessorVoxelPoint
    {
        public bool UseRandom;
        public TerrainBlock Block;
        public int ChunkId;
        public int x;
        public int y;
        public int z;
        public int Id;
        public float Rotation;
        public float Scale;
        public ProcessorVoxelChangeType ModifyType;
        public bool IsNetwork;
        public CanModifyPoint Callback;

        public ProcessorVoxelPoint(TerrainBlock Block, int x, int y, int z, int Id, float Rotation, float Scale, bool UseRandom, ProcessorVoxelChangeType ModifyType, bool IsNetwork, CanModifyPoint Callback = null)
        {
            this.Block = Block;
            this.ChunkId = 0;
            this.x = x;
            this.y = y;
            this.z = z;
            this.Id = Id;
            this.Rotation = Rotation;
            this.Scale = Scale;
            this.UseRandom = UseRandom;
            this.ModifyType = ModifyType;
            this.Callback = Callback;
            this.IsNetwork = IsNetwork;
        }
    }

    public virtual GameObject GetCursor(int Id)
    {
        return null;
    }

    public virtual void ModifyPoint(TerrainBlock Block, int x, int y, int z, int Id, float Rotation, float Scale, bool UseRandom, ProcessorVoxelChangeType ModifyType, bool IsNetwork, CanModifyPoint Callback = null)
    {
        lock (CustomPoints)
            CustomPoints.Enqueue(new ProcessorVoxelPoint(Block, x, y, z, Id, Rotation, Scale, UseRandom, ModifyType, IsNetwork, Callback));
    }

    public virtual void ModifyCube(TerrainBlock Block, int x, int y, int z, int SizeX, int SizeY, int SizeZ, int Id, float Rotation, float Scale, bool UseRandom, ProcessorVoxelChangeType ModifyType, CanModifyPoint Callback = null)
    {
        int px, py, pz;
        for (px = -SizeX; px < SizeX + 1; ++px)
            for (py = -SizeY; py < SizeY + 1; ++py)
                for (pz = -SizeZ; pz < SizeZ + 1; ++pz)
                {
                    ModifyPoint(Block, x + px, y + py, z + pz, Id, Rotation, Scale, UseRandom, ModifyType, false, Callback);
                }
    }

    public virtual void ModifySphere(TerrainBlock Block, int x, int y, int z, int Radius, int Id, float Rotation, float Scale, bool UseRandom, ProcessorVoxelChangeType ModifyType, CanModifyPoint Callback = null)
    {
        VectorI3 p = new VectorI3(x, y, z);
        int x1 = p.x - Radius;
        int y1 = p.y - Radius;
        int z1 = p.z - Radius;
        int x2 = p.x + Radius;
        int y2 = p.y + Radius;
        int z2 = p.z + Radius;

        int px, py, pz;
        for (px = x1; px < x2; ++px)
        {
            for (py = y1; py < y2; ++py)
            {
                for (pz = z1; pz < z2; ++pz)
                {
                    Vector3 v = new Vector3(px - p.x, py - p.y, pz - p.z);
                    float bpos = Radius - v.magnitude;
                    if (bpos > 0f)
                    {
                        ModifyPoint(Block, px, py, pz, Id, Rotation, Scale, UseRandom, ModifyType, false, Callback);
                    }
                }
            }
        }
    }

    public virtual IAction Flush(ActionThread Thread)
    {
        lock (CustomPoints)
        {
            if (CustomPoints.Count == 0)
                return null;

            TerrainBlock Block = null;
            TerrainChunk Chunk;

            while (CustomPoints.Count > 0)
            {
                ProcessorVoxelPoint Point = CustomPoints.Dequeue();
                Block = Point.Block.GetAroundBlock(ref Point.x, ref Point.y, ref Point.z) as TerrainBlock;
                if (Block == null)
                    continue;

                Chunk = Block.GetSafeChunkFromVoxel(Point.x, Point.y, Point.z);

                if (Point.Callback != null && !Point.Callback(ref Block, ref Point.x, ref Point.y, ref Point.z))
                    continue;

                Point.ChunkId = Block.GetChunkId(Chunk.LocalX, Chunk.LocalY, Chunk.LocalZ);
                ApplyPoint(Point);
            }
        }

        return OnVoxelFlushDone(Thread, null);
    }

    public virtual void ApplyPoint(ProcessorVoxelPoint Point)
    {

    }

    public virtual IAction OnVoxelFlushDone(ActionThread Thread, TerrainChunkGenerateGroupAction Action)
    {
        return null;
    }

    public virtual int GetDataIdByName(string Name)
    {
        return 0;
    }

    public virtual KeyValuePair<string, int>[] GetDatas()
    {
        return null;
    }

    public virtual void OnFlushEvent(ITerrainBlock Block, ICustomTerrainData Datas, IObjectBaseInformations CurrentInfo, int Current, int New, int x, int y, int z)
    {
        int i = ProcessorScripts.Count - 1;
        for (; i >= 0; --i)
            ProcessorScripts[i].OnFlushEvent(Block, Datas, CurrentInfo, Current, New, x, y, z);
    }

    #endregion

    #region TerrainObject

    public bool AddObject(TerrainObject Obj)
    {
        lock (TerrainObjects)
        {
            if (!TerrainObjects.Contains(Obj))
            {
                TerrainObjects.Add(Obj);
                OnTerrainObjectAdd(Obj);
                return true;
            }
        }

        return false;
    }

    public bool RemoveObject(TerrainObject Obj)
    {
        lock (TerrainObjects)
        {
            if (TerrainObjects.Remove(Obj))
            {
                OnTerrainObjectRemove(Obj);
                return true;
            }
        }

        return false;
    }

    public bool HasObject(TerrainObject Obj)
    {
        return TerrainObjects.Contains(Obj);
    }

    public virtual void OnTerrainObjectAdd(TerrainObject Obj)
    {

    }

    public virtual void OnTerrainObjectRemove(TerrainObject Obj)
    {

    }

    public virtual bool GetNearObject(ref TerrainObject TObject, Vector3 Position)
    {
        float MinDistance = float.MaxValue;
        float Distance = 0;
        TObject = null;

        lock (TerrainObjects)
        {
            for (int i = TerrainObjects.Count-1; i >= 0; --i)
            {
                Distance = Vector3.Distance(TerrainObjects[i].Position, Position);

                if (Distance < MinDistance)
                {
                    MinDistance = Distance;
                    TObject = TerrainObjects[i];
                }
            }
        }

        return Distance != 0;
    }

    #endregion

    #region Network

    public virtual void OnServerAddObject(TerrainObjectServer Object)
    {

    }

    public virtual void OnServerRemoveObject(TerrainObjectServer Object)
    {

    }

    public virtual void OnServerSendCreateBlock(TerrainObjectServer Object,TerrainBlockServer Block)
    {

    }

    public virtual void OnServerSendAllowedBlocks(TerrainObjectServer Object,List<VectorI3> Positions)
    {

    }


    #endregion

    #region Scripting

    private List<IProcessorScript> ProcessorScripts = new List<IProcessorScript>();

    public void AddScript(IProcessorScript Script)
    {
        ProcessorScripts.Add(Script);
    }

    public void RemoveScript(IProcessorScript Script)
    {
        ProcessorScripts.Remove(Script);
    }

    #endregion
}
