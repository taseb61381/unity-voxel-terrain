using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Processors/First Loading Actions")]
public class OnFirstLoadingProcessor : IBlockProcessor 
{
    [Serializable]
    public class LocalTerrainLoadedEvent
    {
        public LocalTerrainLoadedEvent()
        {
        }
        public LocalTerrainLoadedEvent(TerrainObject Object)
        {
            this.Object = Object;
            OnTerrainLoaded = new ActiveDisableContainer();
            OnTerrainLoaded.GoToInvertState = new List<GameObject>();
            OnTerrainLoaded.GoToInvertState.Add(Object.gameObject);
        }

        public TerrainObject Object;
        public ActiveDisableContainer OnTerrainLoaded;
        public bool Called = false;
    }

    public ActiveDisableContainer OnLoadingStart; // When TerrainManager is started.
    public ActiveDisableContainer OnCompleteTerrainLoaded; // When all Terrain Are loaded
    public List<LocalTerrainLoadedEvent> OnLocalTerrainLoaded; // When Terrain Around an object is loaded
    public bool FirstLoading = true;

    public override void OnStart()
    {
        OnLoadingStart.Call();
    }

    public override void OnAllTerrainLoaded()
    {
        if (FirstLoading)
        {
            FirstLoading = false;
            OnCompleteTerrainLoaded.Call();

            foreach (LocalTerrainLoadedEvent Event in OnLocalTerrainLoaded)
            {
                if (Event.Called)
                    continue;

                Event.OnTerrainLoaded.Call();
                Event.Called = true;
            }
        }
    }

    public override void OnTerrainLoadingDone(TerrainBlock Terrain)
    {
        if (OnLocalTerrainLoaded == null)
            return;

        foreach (LocalTerrainLoadedEvent Event in OnLocalTerrainLoaded)
        {
            if (Event.Called)
                continue;

            if (Event.Object == null)
            {
                Debug.LogError("NULL Object OnFirstLoadingProcessor");
                continue;
            }

            if (TerrainManager.Instance.Informations.GetBlockFromWorldPosition(Event.Object.transform.position) == Terrain.LocalPosition)
            {
                Event.OnTerrainLoaded.Call();
                Event.Called = true;
            }
        }

    }

    void Update()
    {
        if (FirstLoading == false)
        {
            OnCompleteTerrainLoaded.Update();
        }
    }
}
