﻿
namespace TerrainEngine
{
    public abstract class IUndoAction
    {
        public float Date;

        public IUndoAction()
        {
            Date =ThreadManager.UnityTime;
        }

        public abstract string GetName();

        public abstract void Undo();

        public abstract void Redo();
    }
}
