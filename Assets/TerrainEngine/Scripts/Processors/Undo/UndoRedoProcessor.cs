﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class UndoRedoProcessor : IBlockProcessor
{
    static public UndoRedoProcessor Instance;

    public int CurrentId = 0;
    public int MaxActions = 100;
    public int ActionCount = 0;
    public List<IUndoAction> Actions = new List<IUndoAction>();
    public KeyCode UndoKey = KeyCode.B;
    public KeyCode RedoKey = KeyCode.Y;
    public URFlushAction FlushAction;

    void Start()
    {
        Instance = this;
    }

    void Update()
    {
	    if (!Input.anyKey) return;

        if (Input.GetKeyDown(UndoKey))
            Undo();

        if (Input.GetKeyDown(RedoKey))
            Redo();
    }

    public void Undo()
    {
        if (CurrentId == 0)
            return;

        Actions[CurrentId-1].Undo();

        --CurrentId;
    }

    public void Redo()
    {
        if (CurrentId >= Actions.Count)
            return;

        Actions[CurrentId].Redo();

        ++CurrentId;
    }

    /// <summary>
    /// Add a new action to the list.
    /// </summary>
    public void AddAction(IUndoAction Action)
    {
        Actions.Add(Action);
        if (Actions.Count >= MaxActions)
            Actions.RemoveAt(0);
        CurrentId = Actions.Count;
        ActionCount = Actions.Count;
    }

    public override void OnFlushVoxels(ActionThread Thread, TerrainObject Obj, ref VoxelFlush Voxel, TerrainChunkGenerateGroupAction Action)
    {
        if (FlushAction == null)
            FlushAction = new URFlushAction();

        FlushAction.Add(Voxel);
    }

    public override IAction OnVoxelFlushDone(ActionThread Thread, TerrainChunkGenerateGroupAction Action)
    {
        if (FlushAction != null)
            AddAction(FlushAction);
        FlushAction = null;
        return null;
    }
}
