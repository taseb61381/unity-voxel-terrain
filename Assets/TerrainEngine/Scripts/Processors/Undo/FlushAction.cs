﻿using System.Collections.Generic;

namespace TerrainEngine
{
    public class URFlushAction : IUndoAction
    {
        public List<VoxelFlush> Voxels = new List<VoxelFlush>();

        public override string GetName()
        {
            return "VoxelFlush :" + Voxels.Count;
        }

        public void Add(VoxelFlush Flush)
        {
            Voxels.Add(Flush);
        }

        public override void Undo()
        {
            /*foreach (VoxelFlush Flush in Voxels)
            {
                TerrainManager.Instance.ModificationInterface.ModifyVoxel(Flush.Block, Flush.x, Flush.y, Flush.z, Flush.OldType, Flush.OldVolume, VoxelModificationType.SET_TYPE_SET_VOLUME);
            }

            TerrainManager.Instance.ModificationInterface.Flush();*/
        }

        public override void Redo()
        {
            /*foreach (VoxelFlush Flush in Voxels)
            {
                TerrainManager.Instance.ModificationInterface.ModifyVoxel(Flush.Block, Flush.x, Flush.y, Flush.z, Flush.NewType, Flush.NewVolume, VoxelModificationType.SET_TYPE_SET_VOLUME);
            }

            TerrainManager.Instance.ModificationInterface.Flush();*/
        }
    }
}
