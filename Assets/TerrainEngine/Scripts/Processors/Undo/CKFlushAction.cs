﻿using System.Collections.Generic;

namespace TerrainEngine.ConstructionKit
{
    public struct CKFlushVoxel
    {
        public CKFlushVoxel(CKVoxelModification Mod, byte Type, byte Texture, byte Rotation)
        {
            this.Mod = Mod;
            this.Type = Type;
            this.Texture = Texture;
            this.Rotation = Rotation;
        }

        public CKVoxelModification Mod;
        public byte Type, Texture, Rotation;
    };

    public class CKFlushAction : IUndoAction
    {
        static public Queue<CKVoxelModification> Modifications;
        public List<CKFlushVoxel> Voxels;
        public CKChunkProcessor Manager;

        public CKFlushAction(CKChunkProcessor Manager) : base()
        {
            Voxels = new List<CKFlushVoxel>();
            this.Manager = Manager;
        }

        public override string GetName()
        {
            return "CKFlush :" + Voxels.Count;
        }

        public void Add(CKVoxelModification Mod, byte Type, byte Texture, byte Rotation)
        {
            Voxels.Add(new CKFlushVoxel(Mod, Type, Texture, Rotation));
        }

        public override void Undo()
        {
            if (Modifications == null)
                Modifications = new Queue<CKVoxelModification>();
            else
                Modifications.Clear();

            foreach (CKFlushVoxel Flush in Voxels)
            {
                CKVoxelModification Mod = Flush.Mod;
                Mod.Type = Flush.Type;
                Mod.Texture = Flush.Texture;
                Mod.Rotation = Flush.Rotation;
                Modifications.Enqueue(Mod);
            }

            Manager.Flush(Modifications, false,true);
        }

        public override void Redo()
        {
            if (Modifications == null)
                Modifications = new Queue<CKVoxelModification>();
            else
                Modifications.Clear();

            foreach (CKFlushVoxel Flush in Voxels)
                Modifications.Enqueue(Flush.Mod);

            Manager.Flush(Modifications, false, true);
        }
    }
}
