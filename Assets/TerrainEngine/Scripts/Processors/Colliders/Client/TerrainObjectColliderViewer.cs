using TerrainEngine;
using UnityEngine;

/// <summary>
/// Register current TerrainObject to ColliderProcessor (Will generate Chunks colliders around it)
/// Used for Physic (Players, Creatures, etc)
/// </summary>
[AddComponentMenu("TerrainEngine/TerrainObject/Collider Viewer (Generate Colliders around)")]
public class TerrainObjectColliderViewer : MonoBehaviour 
{
    private bool Inited = false;
    private bool Generated = false;
    public TerrainObject TObject;
    public ActiveDisableContainer OnGroundColliderGenerated;
    public float WaitTime = 2f;
    public Vector3 TeleportOffset = new Vector3();
    public bool DrawLog = false;
    public string[] LayersNames = new string[1] { "Chunk" };


    private float ColliderStarted = 0;
    private Vector3 WorldPosition;

    void Awake()
    {
        if (TObject == null)
            TObject = GetComponent<TerrainObject>();
    }

    void OnEnable()
    {
        Inited = false;
    }

    void OnDisable()
    {
        if (TObject == null)
            return;

        TObject.Processors.RemoveAll(proc => (proc is ColliderProcessor && proc.RemoveObject(TObject)));
    }

    void Update()
    {
        if (!Inited)
        {
            if (TObject == null)
                TObject = GetComponent<TerrainObject>();

            if (TObject == null)
            {
                Debug.LogError("No TerrainObject on ColliderTerrainObject gameObject " + name);
                return;
            }

            if (TerrainManager.Instance == null)
            {
                Debug.LogError("TerrainManager not found " + name);
                return;
            }

            for (int i = 0; i < TerrainManager.Instance.Processors.Count; ++i)
            {
                if (TerrainManager.Instance.Processors[i] is ColliderProcessor && TObject.AddProcessor(TerrainManager.Instance.Processors[i]))
                {
                    if (DrawLog)
                        Debug.Log(name + " Added Processor : " + TerrainManager.Instance.Processors[i]);
                }
            }

            Inited = true;
        }
        else if (!Generated)
        {
            if (OnGroundColliderGenerated == null || OnGroundColliderGenerated.Count == 0)
            {
                Generated = true;
                return;
            }

            if (TObject == null)
            {
                if (DrawLog)
                    Debug.LogError("No TObject");
                return;
            }

            if (!TObject.HasBlock)
            {
                if (DrawLog)
                    Debug.LogError("!TObject.HasBlock ");
                return;
            }

            if (!TObject.HasChunk)
            {
                if (DrawLog)
                    Debug.LogError("!TObject.HasChunk");
                return;
            }



            if (ColliderStarted == 0)
            {
                RaycastHit Info;
                Ray R = new Ray(transform.position, -Vector3.up);

                if (Physics.Raycast(R, out Info, 100f, LayerMask.GetMask(LayersNames)))
                {
                    if (DrawLog)
                        Debug.Log("Collider :" + Info.collider + ",Layer:" + Info.collider.gameObject.layer);

                    ColliderStarted = Time.time;
                    WorldPosition = transform.position + TeleportOffset;
                }
                else
                {
                    for (int i = 0; i < TObject.Processors.Count; ++i)
                    {
                        if (TObject.Processors[i] is ColliderProcessor)
                            (TObject.Processors[i] as ColliderProcessor).OnObjectMoveChunk(TObject);
                    }

                    if (DrawLog)
                        Debug.LogError("Raycast Failled");
                }
            }

            if (ColliderStarted != 0 && ColliderStarted < Time.time + WaitTime)
            {
                if(DrawLog)
                    Debug.Log(name + " Collider Found : Teleporting " + WorldPosition);

                transform.position = WorldPosition; 
                Generated = true;
                OnGroundColliderGenerated.Call();
            }
        }
    }

    static public TerrainObjectColliderViewer Add(TerrainObject Obj)
    {
        TerrainObjectColliderViewer Viewer = Obj.GetComponent<TerrainObjectColliderViewer>();
        if (Viewer == null)
            Viewer = Obj.gameObject.AddComponent<TerrainObjectColliderViewer>();

        return Viewer;
    }
}
