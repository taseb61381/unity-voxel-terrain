﻿using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Processors/Colliders")]
public class ColliderProcessor : IBlockProcessor
{
    public struct ColliderTemp
    {
        public ColliderTemp(TerrainChunkRef Ref, float NextUpdate)
        {
            this.ChunkRef = Ref;
            this.NextUpdate = NextUpdate;
        }

        public TerrainChunkRef ChunkRef;
        public float NextUpdate;
    };

    public bool GenerateAllColliders = false;
    public int DrawChunkDistance = 1;
    public int RemoveChunkDistance = 2;
    public float ColliderUpdateInterval = 0.2f;

    public int CollidersToUpdateCount;
    public int CollidersToCheckCount;
    public int CollidersGeneratedCount;

    internal List<ColliderTemp> CollidersToUpdate;
    internal Queue<TerrainChunkRef> CollidersToCheck;
    internal FList<TerrainChunkRef> CollidersGenerated;
    internal float NextColliderCheck = 0;
    internal System.Diagnostics.Stopwatch Watch = new System.Diagnostics.Stopwatch();

    void Start()
    {
        CollidersToUpdate = new List<ColliderTemp>();
        CollidersToCheck = new Queue<TerrainChunkRef>();
        CollidersGenerated = new FList<TerrainChunkRef>(20);
    }

    public override void OnObjectMoveChunk(TerrainObject Obj)
    {
        CheckAroundObject(Obj);
    }

    public override void OnTerrainObjectAdd(TerrainObject Obj)
    {
        if (TerrainManager.Instance != null && TerrainManager.Instance.isInited)
            CheckAroundObject(Obj);
    }

    public void CheckAroundObject(TerrainObject Obj)
    {
        if (GenerateAllColliders || !Obj.ChunkRef.IsValid(ref Obj.BlockObject))
            return;

        CheckExistingColliders();

        //CheckCollider(Obj.BlockObject, Obj.ChunkRef.ChunkId, true);
        CollidersToCheck.Enqueue(Obj.ChunkRef);

        TerrainChunk.GetChunksIteration(Obj.BlockObject, (int)Obj.CurrentLocalChunk.x, (int)Obj.CurrentLocalChunk.y, (int)Obj.CurrentLocalChunk.z, Obj.Position, DrawChunkDistance, CallBack, true);
    }

    void CallBack(TerrainBlock Block, int ChunkId)
    {
        CollidersToCheck.Enqueue(new TerrainChunkRef(Block, ChunkId));
        //CheckCollider(Block, ChunkId, true);
    }

    public void CheckExistingColliders()
    {
        if (GenerateAllColliders)
            return;

        TerrainBlock Block = null;
        NextColliderCheck = Time.realtimeSinceStartup + 2f;

        for (int i = 0; i < CollidersGenerated.Count; )
        {
            if (CollidersGenerated.array[i].IsValid(ref Block))
            {
                if (!CheckCollider(Block, CollidersGenerated.array[i].ChunkId, false))
                {
                    RemoveCollider(Block, CollidersGenerated.array[i].ChunkId);
                    continue;
                }
            }

            ++i;
        }
    }

    void Update()
    {
        CollidersGeneratedCount = CollidersGenerated.Count;
        CollidersToCheckCount = CollidersToCheck.Count;
        CollidersToUpdateCount = CollidersToUpdate.Count;

        TerrainChunkRef Chunk;
        TerrainBlock Block = null;

        Watch.Start();
        if (NextColliderCheck < Time.realtimeSinceStartup)
            CheckExistingColliders();

        while (CollidersToCheck.Count != 0)
        {
            Chunk = CollidersToCheck.Dequeue();
            if (Chunk.IsValid(ref Block))
            {
                CheckCollider(Block, Chunk.ChunkId, true);
            }

            if (Watch.ElapsedMilliseconds >= 2)
                break;
        }

        Watch.Stop();
        Watch.Reset();

        ColliderTemp Temp;
        for (int i = 0; i < CollidersToUpdate.Count; )
        {
            Temp = CollidersToUpdate[i];

            if (!Temp.ChunkRef.IsValid(ref Block))
            {
                CollidersToUpdate.RemoveAt(i);
                continue;
            }

            if (CollidersToUpdate[i].NextUpdate <= Time.realtimeSinceStartup)
            {
                GenerateCollider(Block, Temp.ChunkRef.ChunkId, null);
                CollidersToUpdate.RemoveAt(i);
                continue;

            }

            if (Watch.ElapsedMilliseconds >= 3)
                return;

            ++i;
        }
    }

    public override void OnTerrainUnityClose(TerrainBlock Terrain)
    {
        for (int i = CollidersGenerated.Count - 1; i >= 0; --i)
        {
            if (CollidersGenerated.array[i].BlockId == Terrain.BlockId)
            {
                CollidersGenerated.RemoveAt(i);
                RemoveChunkCollider(Terrain, CollidersGenerated.array[i].ChunkId);
            }
        }
    }

    public override void OnChunkBuild(TerrainBlock Block,int ChunkId, IMeshData Data)
    {
        TerrainChunkScript Script = Block.HasChunkState(ChunkId, ChunkStates.HasScript) ? Block.GetChunkScript(ChunkId) : null;

        if (!GenerateAllColliders)
        {
            if (Script == null)
                return;

            Vector3 WorldPosition = Block.GetChunkWorldPosition(ChunkId);

            TerrainObject Near = null;
            if (!GetNearObject(ref Near, WorldPosition) || TerrainManager.Instance.GetChunkDistance(WorldPosition, Near.Position) > RemoveChunkDistance)
                return;

            if (ColliderUpdateInterval <= 0)
            {
                GenerateCollider(Block, ChunkId, Script);
                return;
            }

            CollidersToUpdate.Add(new ColliderTemp(new TerrainChunkRef(Block,ChunkId), Time.realtimeSinceStartup + ColliderUpdateInterval));
        }
        else
            GenerateCollider(Block, ChunkId, Script);
    }

    public override void OnFirstChunkBuild(TerrainBlock Block, int ChunkId, IMeshData Data)
    {
        OnChunkBuild(Block, ChunkId, Data);
    }

    public override void OnChunkModification(TerrainBlock Block, int ChunkId)
    {
        GenerateCollider(Block, ChunkId, null);
        Debug.Log("Force Generate Collider :" + Block.WorldPosition + "," + Block.GetChunkWorldPosition(ChunkId));
    }

    public bool CheckCollider(TerrainBlock Block, int ChunkId, bool Generate=true)
    {
        if (!Block.HasChunkState(ChunkId, ChunkStates.HasScript))
            return false;

        TerrainObject Near = null;

        if (!GenerateAllColliders)
        {
            Vector3 WorldPosition = Block.GetChunkWorldPosition(ChunkId);
            if (!GetNearObject(ref Near, WorldPosition) || TerrainManager.Instance.GetChunkDistance(WorldPosition, Near.Position) > RemoveChunkDistance)
                return false;
        }

        if(Generate)
            GenerateCollider(Block, ChunkId, null);

        return true;
    }

    public bool Contains(ushort BlockId, int ChunkId)
    {
        for (int i = CollidersGenerated.Count - 1; i >= 0; --i)
        {
            if (CollidersGenerated.array[i].ChunkId == ChunkId && CollidersGenerated.array[i].BlockId == BlockId)
                return true;
        }

        return false;
    }

    public bool GenerateCollider(TerrainBlock Block, int ChunkId, TerrainChunkScript Script)
    {
        bool Result = GenerateChunkCollider(Block, ChunkId, null, Script);

        if (Result && !Contains(Block.BlockId, ChunkId))
            CollidersGenerated.Add(new TerrainChunkRef(Block, ChunkId));

        return Result;
    }

    public void RemoveCollider(TerrainBlock Block, int ChunkId)
    {
        RemoveChunkCollider(Block, ChunkId);

        for (int i = 0; i < CollidersGenerated.Count; )
        {
            if (CollidersGenerated.array[i].ChunkId == ChunkId && CollidersGenerated.array[i].BlockId == Block.BlockId)
            {
                CollidersGenerated.RemoveAt(i);
                continue;
            }

            ++i;
        }
    }

    /// <summary>
    /// Remove chunk collider if chunk has mesh
    /// </summary>
    public bool RemoveChunkCollider(TerrainBlock Block, int ChunkId)
    {
        if (Block.HasChunkState(ChunkId, ChunkStates.HasScript))
        {
            TerrainChunkScript Script = Block.GetChunkScript(ChunkId);
            if (Script.CCollider != null)
            {
                Script.CCollider.enabled = false;
                Script.CCollider.sharedMesh = null;
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Generate chunk collider if chunk mesh exist and if chunk mesh is dirty
    /// </summary>
    public bool GenerateChunkCollider(TerrainBlock Block, int ChunkId, IMeshData Data, TerrainChunkScript Script)
    {
        if (Block.HasChunkState(ChunkId, ChunkStates.HasScript))
        {
            if (Script == null)
                Script = Block.GetChunkScript(ChunkId);

            if (Data == null || Data.IsValid())
            {
                if (Block.HasChunkState(ChunkId, ChunkStates.Dirty))
                {
                    Block.SetChunkState(ChunkId, ChunkStates.Dirty, false);

                    if (Script.CCollider == null)
                        Script.CCollider = Script.gameObject.AddComponent<MeshCollider>();

                    Script.CCollider.sharedMesh = null;
                    Script.CCollider.sharedMesh = Script.GetMesh(false);

                    Script.CCollider.enabled = true;

                    return true;
                }

                if (Script.CCollider == null)
                    Script.CCollider = Script.gameObject.AddComponent<MeshCollider>();
                
                if(Script.CCollider.enabled != true)
                {
                    Mesh CMesh = Script.GetMesh(false);
                    if (CMesh != null && Script.CCollider.sharedMesh != CMesh)
                        Script.CCollider.sharedMesh = CMesh;
                    Script.CCollider.enabled = true;
                    return true;
                }
            }
        }

        return false;
    }

}