﻿using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

/// <summary>
/// This class provide code for spawn an GameObject when a Processor Data Type is removed (Like when plant is removed on DetailProcessor)
/// </summary>
public class SpawnOnRemoveProcessorScript : IProcessorScript
{
    [Serializable]
    public class SpawnOnRemoveInfo
    {
        public bool Enable;             // True if this informations can be used
        public string Name;             // Name of the Data to handle (Bush4 for Spawn the GameObject Bush4 when bush4 is removed from DetailProcessor)
        public GameObject Obj;          // Object to spawn when Data is removed.
        public Vector3 PositionOffset;  // Position offset of the new object spawned
        public float DespawnTime;       // DespawnTime after Object Created

        [HideInInspector]
        public int ObjectId;              // Let this to 0, the name will be used for find the DataId

        [HideInInspector]
        public bool HasObject;          // MultiThreading value for check is the current Info have object
    };

    public List<SpawnOnRemoveInfo> Objects;

    void Start()
    {
        foreach (SpawnOnRemoveInfo Info in Objects)
        {
            if (Info.ObjectId <= 0)
                Info.ObjectId = Processor.GetDataIdByName(Info.Name);

            Info.HasObject = Info.Obj != null;
        }
    }

    public override void OnFlushEvent(ITerrainBlock Block, ICustomTerrainData Datas, IObjectBaseInformations CurrentInfo, int Current, int New, int x, int y, int z)
    {
        if (Current != 0 && New == 0)
        {
            SFastRandom FRandom = new SFastRandom(0);
            foreach (SpawnOnRemoveInfo Info in Objects)
            {
                if (!Info.Enable)
                    continue;

                if (Info.ObjectId == Current-1 && Info.HasObject)
                {
                    Vector3 S = CurrentInfo.GetScale(ref FRandom, x, y, z);
                    float StoredScale = Datas.GetScale(x, y, z);
                    if (StoredScale != 0)
                        S.x = S.y = S.z = StoredScale;

                    Vector3 Position = Block.GetWorldPosition(x, y+1, z, true);
                    if (Position.y == -1)
                    {
                        Debug.LogError("Can not find Height :" + Position + ",P=" + x + "," + y + "," + z);
                        Position = Block.GetWorldPosition(x, y, z, false);
                    }

                    CreateGameObjectAction Action = PoolManager.Pool.GetData<CreateGameObjectAction>("CreateGameObjectAction");
                    Action.Init(Info.Obj,
                        Position + Info.PositionOffset,
                        Quaternion.Euler(0, Datas.GetOrientation(x,y,z), 0), 
                        new Vector3(S.x, S.x, S.x), 
                        Info.DespawnTime);
                    ActionProcessor.AddToUnity(Action);
                }
            }
        }
    }
}