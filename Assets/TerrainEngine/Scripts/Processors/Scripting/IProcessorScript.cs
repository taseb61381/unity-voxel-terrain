﻿using TerrainEngine;
using UnityEngine;

public class IProcessorScript : MonoBehaviour
{
    public IBlockProcessor Processor;

    void OnEnable()
    {
        if (transform.parent != null)
            Processor = transform.parent.GetComponent<IBlockProcessor>();

        if (Processor != null)
            Processor.AddScript(this);
        else
        {
            Debug.LogError("Can not add script " + name + ", no Processor parent");
            enabled = false;
        }
    }

    void OnDisable()
    {
        if (Processor != null)
            Processor.RemoveScript(this);
    }

    /// <summary>
    /// Ovveride this Event for create your own scripts.
    /// </summary>
    public virtual void OnFlushEvent(ITerrainBlock Block, ICustomTerrainData Datas, IObjectBaseInformations CurrentInfo, int Current, int New, int x, int y, int z)
    {

    }
}