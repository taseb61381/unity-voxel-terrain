﻿using UnityEngine;
using System.Collections;

using TerrainEngine;

public class MineralProcessor : IBlockProcessor 
{
    public int Seed = 0;
    public float PerlinScale = 0.01f;
    public float MinValue = 0.5f;
    public float MaxMineralHeight = 5;
    public int VoxelType = 1;

    public override IWorldGenerator GetGenerator()
    {
        MineralGenerator Gen = new MineralGenerator();
        Gen.Seed = Seed;
        Gen.PerlinScale = PerlinScale;
        Gen.MinValue = MinValue;
        Gen.MaxMineralHeight = MaxMineralHeight;
        Gen.VoxelType = VoxelType;
        return Gen;
    }

    public override bool CanModifyVoxel(TerrainObject Obj, ref VoxelFlush Flush)
    {
        if (Flush.OldType == (byte)VoxelType)
            return false;

        return true;
    }
}
