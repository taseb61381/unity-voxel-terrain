﻿using UnityEngine;
using System;
using System.Collections;

using TerrainEngine;

[Serializable]
public class MineralGenerator : IWorldGenerator
{
    public int Seed = 0;
    public float PerlinScale = 0.01f;
    public float MinValue = 0.5f;
    public float MaxMineralHeight = 5;
    public int VoxelType = 1;

    public override IWorldGenerator.GeneratorTypes GetGeneratorType()
    {
        return GeneratorTypes.CUSTOM;
    }

    public override bool OnGenerate(ActionThread Thread, IWorldGenerator Parent, TerrainBlockServer BlockServer, TerrainDatas Block, BiomeData Data, int ChunkId, int ChunkX, int ChunkZ)
    {
        int VPC = Information.VoxelPerChunk;
        int StartX = ChunkX * VPC;
        int StartZ = ChunkZ * VPC;
        int WorldX, WorldZ = 0,WorldY=0;
        int BlockY = Block.WorldY, GroundY = 0,BlockEndY = Information.VoxelsPerAxis;
        int x, z, f=0;
        int[] NoiseGround = Data.GetGrounds(VPC, VPC);
        SimplexNoise.Initialize();

        for (x = VPC - 1; x >= 0; --x)
        {
            WorldX = Block.WorldX + StartX + x;
            for (z = VPC - 1; z >= 0; --z, ++f)
            {
                WorldZ = Block.WorldZ + StartZ + z;

                if (SimplexNoise.Noise2(WorldX * PerlinScale, WorldZ * PerlinScale) < MinValue)
                    continue;

                GroundY = NoiseGround[f] - BlockY;

                //Size = (SimplexNoise.Noise2(WorldX * 0.001f, WorldZ * 0.001f) + 1f) * MaxMineralHeight;


                WorldY = GroundY;

                // Ground on other TerrainBlock (upper or lower)
                if (WorldY < 0 || WorldY >= BlockEndY)
                    continue;

                Block.SetTypeAndVolume(x, WorldY, z, (byte)VoxelType, 1f);
            }
        }

        return true;
    }

    public override bool OnVoxelStartModification(ITerrainBlock Block, uint TObject, ref VoxelFlush Flush)
    {
        if (Flush.OldType == (byte)VoxelType)
            return false;

        return true;
    }
}
