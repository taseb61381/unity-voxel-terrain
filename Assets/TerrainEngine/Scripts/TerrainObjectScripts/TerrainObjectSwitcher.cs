using System;
using TerrainEngine;
using UnityEngine;

/// <summary>
/// Enable or Disable components depend on current voxel type
/// Used for Underwater effect , when current object voxel is water , enable all effects
/// Example : Can be used for deal damages when object is on lava
/// </summary>
[AddComponentMenu("TerrainEngine/TerrainObject/Switcher (Handle Current Voxel Events)")]
public class TerrainObjectSwitcher : MonoBehaviour 
{
    public enum VoxelTriggers
    {
        ON_ENTER = 1,
        ON_LEAVE = 2,
        ON_MOVE = 4,
    };

    public enum SwitcherEventTypes
    {
        ACTIVE = 1,
        DISABLE = 2,
        ACTIVE_DISABLE = 4,
        INSTANCIATE = 8,
    };

    public enum TypeComparatorTypes
    {
        EQUAL = 0,
        DIFFFERENT = 1,
        DIFFERENT_OR_VOLUME = 2,
        EMPTY_OR_VOLUME = 3,
    };

    [Serializable]
    public class SwitchObject
    {
        public SwitchObject(float RemoveTime, GameObject Obj)
        {
            this.RemoveTime = RemoveTime;
            this.Obj = Obj;
        }

        public float RemoveTime;
        public GameObject Obj;

        public bool CanRemove()
        {
            return Time.realtimeSinceStartup >= RemoveTime;
        }
    }

    [Serializable]
    public class SwitchEvent
    {
        public string Name;
        public VoxelTriggers TriggerTypes;
        public SwitcherEventTypes SwitchType;
        public GameObject Obj;
        public float DisactiveTime;
        public MonoBehaviour Mono;
        public ActiveDisableContainer OnEvent;
        public TypeComparatorTypes TypeComparator;
        public int Type = 1;
        public float MinVolume=0;
        public float MaxVolume=1;
        public int PreviousType=-1;
        public bool CheckActive = true;

        public float MinDistanceForActive;
        public float UpdateActiveInterval;

        public float MinDistanceForDisable;
        public float UpdateDisableInterval;

        public bool FollowParent;
        public bool FollowTerriainHeight;
        public bool IsActive;
        public bool OnlyOneObject;
        public float DestroyTimeAfterDisable = 0;

        [HideInInspector]
        public GameObject CreatedObject;

        [HideInInspector]
        public Vector3 LastActiveCheck = new Vector3();

        [HideInInspector]
        public Vector3 LastDisableCheck = new Vector3();

        [HideInInspector]
        public float NextActiveUpdate;

        [HideInInspector]
        public float NextDisableUpdate;

        [HideInInspector]
        public bool IsEnabled = false;
    };

    public string Name;
    public SwitchEvent[] Events;

    private TerrainObject TObject;

    void Start()
    {
        if (TObject == null)
            TObject = GetComponent<TerrainObject>();

        if (TObject == null)
        {
            TObject = gameObject.AddComponent<TerrainObject>();
            TObject.UpdateCurrentVoxel = true;
        }
    }

    public void OnCurrentVoxelChange(TerrainObject Object)
    {
        bool Valid = false;

        foreach (SwitchEvent Event in Events)
        {
            Valid = false;
            if(Event.TypeComparator == TypeComparatorTypes.EQUAL && Event.Type == Object.CurrentVoxelType) 
            {
                if (Event.MinVolume <= Object.CurrentVoxelVolume)
                    Valid = true;
            }
            else if(Event.TypeComparator == TypeComparatorTypes.DIFFFERENT && Event.Type != Object.CurrentVoxelType)
            {
                if(Event.MinVolume <= Object.CurrentVoxelVolume)
                    Valid = true;
            }
            else if(Event.TypeComparator == TypeComparatorTypes.DIFFERENT_OR_VOLUME)
            {
                if(Event.Type != Object.CurrentVoxelType || (Event.Type == Object.CurrentVoxelType && Object.CurrentVoxelVolume <= Event.MinVolume))
                     Valid = true;
            }
            else if (Event.TypeComparator == TypeComparatorTypes.EMPTY_OR_VOLUME)
            {
                if (Object.CurrentVoxelType == 0 || (Event.Type == Object.CurrentVoxelType && Object.CurrentVoxelVolume <= Event.MinVolume))
                    Valid = true;
            }

            if (Valid)
            {
                if (Event.PreviousType != -1 && Event.PreviousType != Object.LastVoxelType)
                    continue;

                if (!Event.CheckActive || (Event.CheckActive && !Event.IsActive))
                {
                    Event.IsActive = true;
                    SetEvent(Event, true);
                }
            }
            else
            {
                if (!Event.CheckActive || (Event.CheckActive && Event.IsActive))
                {
                    Event.IsActive = false;
                    SetEvent(Event, false);
                }
            }
        }
    }

    void Update()
    {
        float Distance = 0;
        foreach (SwitchEvent Event in Events)
        {
            if (!Event.IsActive)
                continue;

            if (Event.TriggerTypes == VoxelTriggers.ON_MOVE)
            {
                if (Event.UpdateActiveInterval >= 0 && Time.realtimeSinceStartup >= Event.NextActiveUpdate)
                {
                    Event.NextActiveUpdate = Time.realtimeSinceStartup + Event.UpdateActiveInterval;
                    Distance = Vector3.Distance(Event.LastActiveCheck, transform.position);

                    if (Event.MinDistanceForActive == 0 || Distance >= Event.MinDistanceForActive)
                    {
                        Event.IsEnabled = true;
                        Event.LastActiveCheck = transform.position;
                        CallEvent(Event);
                    }
                }

                if (Event.UpdateDisableInterval >= 0 && Time.realtimeSinceStartup >= Event.NextDisableUpdate)
                {
                    if (!Event.IsEnabled)
                        continue;

                    Event.NextDisableUpdate = Time.realtimeSinceStartup + Event.UpdateDisableInterval;
                    Distance = Vector3.Distance(Event.LastDisableCheck, transform.position);
                    Event.LastDisableCheck = transform.position;

                    if (Event.MinDistanceForDisable != 0 && Distance < Event.MinDistanceForDisable)
                    {
                        Event.IsEnabled = false;
                        Event.LastActiveCheck = transform.position;
                        if (Event.CreatedObject != null)
                            ActionProcessor.SetGameObjectState(Event.CreatedObject, false, true);
                    }
                }
            }
        }
    }

    public void SetEvent(SwitchEvent Event, bool Active)
    {
        Event.LastActiveCheck = transform.position;
        Event.LastDisableCheck = transform.position;

        if ((!Active && Event.TriggerTypes == VoxelTriggers.ON_LEAVE) || (Active && Event.TriggerTypes == VoxelTriggers.ON_ENTER))
        {
            //Debug.Log("Active : " + Event.Name);
            CallEvent(Event);
        }
        else
        {
            if (Event.CreatedObject == null)
                return;

            TimedEffectContainer Container = Event.CreatedObject.GetComponent<TimedEffectContainer>();
            if (Container != null && Container.Effects.Count > 0)
            {
                foreach (TimedEffect Effect in Container.Effects)
                    Effect.Disable();
            }

            if (Event.DestroyTimeAfterDisable >= 0)
                GameObject.Destroy(Event.CreatedObject, Event.DestroyTimeAfterDisable);

        }
    }

    public void CallEvent(SwitchEvent Event)
    {
        if (Event.OnEvent != null)
            Event.OnEvent.Call();

        if (Event.SwitchType == SwitcherEventTypes.ACTIVE)
        {
            if (Event.Mono != null)
                Event.Mono.enabled = true;

            if (Event.Obj != null)
                Event.Obj.SetActiveRecursively(true);
        }
        else if (Event.SwitchType == SwitcherEventTypes.DISABLE)
        {
            if (Event.Mono != null)
                Event.Mono.enabled = false;

            if (Event.Obj != null)
                Event.Obj.SetActiveRecursively(false);
        }
        else if (Event.SwitchType == SwitcherEventTypes.ACTIVE_DISABLE)
        {
            if (Event.Mono != null)
                Event.Mono.enabled = !Event.Mono.enabled;

            if (Event.Obj != null)
                Event.Obj.SetActiveRecursively(!Event.Obj.activeInHierarchy);
        }
        else if (Event.SwitchType == SwitcherEventTypes.INSTANCIATE)
        {
            if (Event.Obj != null)
            {
                if(!Event.OnlyOneObject || Event.CreatedObject == null)
                    Event.CreatedObject = GameObject.Instantiate(Event.Obj) as GameObject;

                Event.CreatedObject.transform.position = transform.position;
                Event.CreatedObject.transform.localScale = transform.localScale;
                Event.CreatedObject.transform.parent = Event.FollowParent ? transform.parent : null;
                if (Event.FollowTerriainHeight)
                {
                    Vector3 Position = transform.position;
                    Position.y = int.MaxValue;
                    float Height = TObject.BlockObject.GetLowerHeight(Position, 0).y;
                    Event.CreatedObject.transform.position = new Vector3(Event.CreatedObject.transform.position.x, Height, Event.CreatedObject.transform.position.z);
                }

                ActionProcessor.SetGameObjectState(Event.CreatedObject, true, true);
                if (Event.DisactiveTime != 0)
                {
                    GameObject.Destroy(Event.CreatedObject, Event.DisactiveTime);
                }
            }
        }
    }
}
