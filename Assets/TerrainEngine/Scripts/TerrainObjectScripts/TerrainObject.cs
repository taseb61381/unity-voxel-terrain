﻿using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

[AddComponentMenu("TerrainEngine/Terrain Object")]
public class TerrainObject : MonoBehaviour
{
    public List<IBlockProcessor> Processors = new List<IBlockProcessor>();

    [NonSerialized]
    public Transform CTransform;

    [NonSerialized]
    public Vector3 Position;

    [NonSerialized]
    public Vector3 Rotation;

    [NonSerialized]
    public Vector2 Position2D;

    public VectorI3 CurrentBlock;
    public VectorI3 CurrentChunk;
    public VectorI3 CurrentLocalChunk;
    public bool DestroyIfNoTerrain = false;
    public TerrainBlockScript CurrentBlockScript;
    public TerrainChunkScript CurrentChunkScript;

    public bool UpdateCurrentVoxel = true;
    public Vector3 CurrentVoxel;
    public Vector3 CurrentVoxelNormal;
    public VectorI3 IntCurrentVoxel;
    public int CurrentVoxelType = 0;
    public float CurrentVoxelVolume = 0;

    public bool HasBlock = false;
    public bool HasChunk = false;

    public int ChunkId;
    public int FlattenId;

    [NonSerialized]
    public Vector3 WorldVoxel;

    [NonSerialized]
    public bool HasPositionChanged = false;

    [NonSerialized]
    public bool HasBillBoardChanged = false;

    [NonSerialized]
    public TerrainBlock BlockObject;

    [NonSerialized]
    public TerrainChunkRef ChunkRef;

    public TerrainBlock VerticalBlockObject; // Even if object is not on the terrain, under or upper , this block is the block is the X/Z corresponding block to this position
    public TerrainBlockScript VerticalBlockScript;

    [NonSerialized]
    public bool WasValid = false;

    public VoxelRaycastHit VoxelHit = new VoxelRaycastHit();

    [NonSerialized]
    public VectorI3 NewChunk;

    [NonSerialized]
    public VectorI3 NewBlock;

    private Vector3 LastPosition;
    private Vector3 LastRotation;
    public int LastVoxelType = -1;
    public float LastVoxelVolume = -1;
    private bool PositionInited = false;
    private float LastVerticalCheck = 0;

    void Awake()
    {
        CTransform = transform;
        HasBlock = HasChunk = PositionInited = false;
    }

    void Start()
    {
        CurrentBlock = CurrentChunk = CurrentLocalChunk = new VectorI3();
        CurrentBlockScript = null;
        CurrentChunkScript = null;
        IntCurrentVoxel = new VectorI3();
        HasBlock = HasChunk = PositionInited = false;
        CheckPosition();
    }

    void OnDisable()
    {

    }

    void OnEnable()
    {

    }

    /// <summary>
    /// Force and update current position
    /// </summary>
    public void UpdatePosition()
    {
        CheckPosition();
        HasPositionChanged = true;
        HasBillBoardChanged = true;
        Update();
    }

    void Update()
    {
        if (TerrainManager.Instance == null || TerrainManager.Instance.Container == null)
            return;

        CheckBillboard();

        if (TerrainManager.Instance == null || !TerrainManager.Instance.enabled || !TerrainManager.Instance.isStarted)
        {
            HasBlock = HasChunk = false;
            return;
        }

        if (HasPositionChanged || !HasBlock || CurrentBlockScript == null || CurrentChunkScript == null)
        {
            CheckCurrentBlock();

            if (BlockObject != null)
            {
                IntCurrentVoxel = BlockObject.GetVoxelFromWorldPosition(Position, false);
                CurrentVoxel = IntCurrentVoxel;
                WorldVoxel = IntCurrentVoxel + BlockObject.WorldVoxel;
            }

            if (UpdateCurrentVoxel && PositionInited)
                CheckCurrentVoxel();
        }

        if (BlockObject == null || BlockObject.HasState(TerrainBlockStates.CLOSED))
            HasBlock = false;

        if (!HasBlock || !ChunkRef.IsValidID())
            HasChunk = false;

        if (HasBlock || TerrainManager.Instance.isLoaded)
            WasValid = true;

        if (WasValid && !HasBlock && DestroyIfNoTerrain)
            GameObject.Destroy(gameObject);

        if (HasBlock && HasChunk)
        {
            ChunkId = ChunkRef.ChunkId;
            FlattenId = TerrainManager.Instance.Informations.GetChunkIdFromVoxels(IntCurrentVoxel.x, IntCurrentVoxel.y, IntCurrentVoxel.z);
            CurrentVoxelNormal = BlockObject.GetNormal(IntCurrentVoxel);

            /*for (int x = -10; x < 11; ++x)
            {
                for (int z = -10; z < 11; ++z)
                {
                    CurrentVoxelNormal = BlockObject.GetNormal(IntCurrentVoxel + new VectorI3(x,0,z));
                    Vector3 P = BlockObject.GetWorldPosition(IntCurrentVoxel + new VectorI3(x, 0, z));
                    Debug.DrawLine(P, P + CurrentVoxelNormal);
                }
            }*/
        }
    }

    void LateUpdate()
    {
        CheckPosition();
    }

    public void CheckCurrentBlock()
    {
        NewChunk = TerrainManager.Instance.Informations.GetChunkGlobalPosition(Position);
        NewBlock = TerrainManager.Instance.Informations.GetBlockFromWorldPosition(Position);

        if (NewChunk != CurrentChunk || CurrentBlockScript == null)
        {
            if (NewBlock != CurrentBlock || CurrentBlockScript == null)
            {
                BlockObject = TerrainManager.Instance.Container.GetBlock(NewBlock) as TerrainBlock;

                if (BlockObject != null && BlockObject.HasState(TerrainBlockStates.CLOSED))
                    BlockObject = null;

                if (BlockObject != null)
                {
                    CurrentBlock = NewBlock;
                    CurrentBlockScript = BlockObject.Script;
                    ChunkRef.BlockId = BlockObject.BlockId;
                    HasBlock = true;
                }
                else
                {
                    BlockObject = null;
                    CurrentBlockScript = null;
                    ChunkRef.BlockId = ushort.MaxValue;
                    HasBlock = false;
                }
            }

            if (HasBlock && BlockObject != null)
            {
                CurrentLocalChunk = BlockObject.GetChunkFromGlobalPosition(NewChunk, ref ChunkRef);
                if (ChunkRef.IsValidID())
                {
                    CurrentChunkScript = BlockObject.GetChunkScript(ChunkRef.ChunkId);
                    HasChunk = true;
                }
                else
                {
                    CurrentChunkScript = null;
                    HasChunk = false;
                }
            }

            if (BlockObject == null)
            {
                if (LastVerticalCheck < ThreadManager.UnityTime)
                {
                    LastVerticalCheck = ThreadManager.UnityTime + 0.5f;
                    VerticalBlockObject = TerrainManager.Instance.Container.GetVerticalTerrain(NewBlock);
                }
            }
            else
                VerticalBlockObject = BlockObject;

            if (VerticalBlockScript == null)
            {
                if (VerticalBlockObject == null)
                    VerticalBlockScript = null;
                else
                    VerticalBlockScript = VerticalBlockObject.Script;
            }
        }

        if (CurrentChunkScript == null)
        {
            if (ChunkRef.IsValidID() && BlockObject != null)
                CurrentChunkScript = BlockObject.GetChunkScript(ChunkRef.ChunkId);
            else
                HasChunk =false;
        }

        if (NewChunk != CurrentChunk)
        {
            if (BlockObject != null)
                CurrentChunk = NewChunk;

            SendMessage("OnObjectMoveChunk", this, SendMessageOptions.DontRequireReceiver);
            for (int i = 0; i < Processors.Count; ++i)
            {
                if (Processors[i].enabled)
                    Processors[i].OnObjectMoveChunk(this);
            }
        }

        if (NewBlock != CurrentBlock)
        {
            SendMessage("OnObjectMoveTerrain", this, SendMessageOptions.DontRequireReceiver);
            for (int i = 0; i < Processors.Count; ++i)
            {
                if (Processors[i].enabled)
                    Processors[i].OnObjectMoveTerrain(this);
            }
        }
    }

    public void CheckCurrentVoxel()
    {
        if (BlockObject == null)
        {
            CurrentVoxelType = 0;
            CurrentVoxelVolume = 0;
            return;
        }

        if (HasBlock && HasChunk)
        {
            byte Type = 0;
            BlockObject.GetByteAndFloat(IntCurrentVoxel.x, IntCurrentVoxel.y, IntCurrentVoxel.z, ref Type, ref CurrentVoxelVolume, false);
            CurrentVoxelType = Type;
        }
        else
        {
            CurrentVoxel.x = CurrentVoxel.y = CurrentVoxel.z = 0;
            IntCurrentVoxel.x = IntCurrentVoxel.y = IntCurrentVoxel.z;
            CurrentVoxelType = 0;
            CurrentVoxelVolume = 0;
        }

        if (CurrentVoxelType != LastVoxelType || CurrentVoxelVolume != LastVoxelVolume)
        {
            SendMessage("OnCurrentVoxelChange", this, SendMessageOptions.DontRequireReceiver);
            LastVoxelType = CurrentVoxelType;
            LastVoxelVolume = CurrentVoxelVolume;
        }
    }

    public void CheckBillboard()
    {
        HasBillBoardChanged = false;
        if (LastPosition.x != Position.x || LastPosition.z != Position.z)
        {
            HasBillBoardChanged = true;
            LastPosition.x = CTransform.position.x;
            LastPosition.z = CTransform.position.z;
        }

        if (LastRotation != Rotation)
        {
            HasBillBoardChanged = true;
            LastRotation = Rotation;
        }
    }

    public void CheckPosition()
    {
        this.Position = CTransform.position;
        this.Rotation = CTransform.rotation.eulerAngles;
        this.Position2D.x = Position.x;
        this.Position2D.y = Position.z;
        this.PositionInited = true;

        HasPositionChanged = this.Position != LastPosition;

        if (HasPositionChanged)
        {
            LastPosition.y = Position.y;
        }
    }

    public bool AddProcessor(IBlockProcessor Processor)
    {
        if (!Processors.Contains(Processor))
        {
            Processors.Add(Processor);
            Processor.AddObject(this);
            return true;
        }

        return false;
    }

    public bool HasProcessor(IBlockProcessor Processor)
    {
        for (int i = 0; i < Processors.Count; ++i)
            if (Processors[i].InternalName == Processor.InternalName)
                return true;

        return false;
    }
}
