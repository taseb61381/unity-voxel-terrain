﻿using System;

using UnityEngine;

/// <summary>
/// Teleport object on ground if object is underground (falling or bad starting position)
/// </summary>
[AddComponentMenu("TerrainEngine/TerrainObject/Ground Aligner (Teleport if underground)")]
public class TerrainObjectGroundAligner : MonoBehaviour
{
    public TerrainObject TObject;
    public float NextUpdate = 0;
    public float Offset = -3;
    public Vector3 TeleportOffset = new Vector3(0, 2, 0);
    public bool UseRaycast = false;
    public bool MustCheck = false;
    public float RaycastDistance = 500;
    public bool DrawLog = false;

    public void OnCurrentVoxelChange(TerrainObject Object)
    {
        if (TObject == null)
        {
            TObject = GetComponent<TerrainObject>();

            if (TObject != null)
                TObject.UpdateCurrentVoxel = true;
        }

        if (Object.CurrentVoxelType != 0 && TObject.CurrentVoxelVolume > 0.5f && TerrainManager.Instance.Palette.GetType((byte)TObject.CurrentVoxelType) != VoxelTypes.FLUID)
        {
            VectorI3 Offset = Object.IntCurrentVoxel;
            if (Object.BlockObject.HasVolume(Offset.x, Offset.y + 1, Offset.z, 0.5f, VoxelTypes.FLUID, false))
            {
                if (Object.BlockObject.HasVolume(Offset.x, Offset.y - 1, Offset.z, 0.5f, VoxelTypes.FLUID, false))
                {
                    if (NextUpdate == 0)
                        NextUpdate = Time.realtimeSinceStartup + 2f;
                }
                else
                    NextUpdate = 0;
            }
            else
                NextUpdate = 0;
        }
        else
            NextUpdate = 0;
    }

    Vector3 LastOkPosition = Vector3.zero;
    Vector3 TargetPosition = Vector3.zero;
    bool FirstCheck = true;

    public float FreezeDuration = 5f;
    private Vector3 PositionToApply;
    private float ApplyEnd;

    void Update()
    {
        if (ApplyEnd > Time.realtimeSinceStartup)
        {
            TObject.CTransform.position = PositionToApply;
            return;
        }

        if (TObject == null)
        {
            TObject = GetComponent<TerrainObject>();
            if (TObject == null)
            {
                TObject = gameObject.AddComponent<TerrainObject>();
                TObject.UpdatePosition();
            }
        }

        if (TObject.VerticalBlockObject == null)
            return;

        if (FirstCheck)
        {
            if (!TObject.HasBlock || !TObject.HasChunk)
                MustCheck = true;
        }

        if (MustCheck || (NextUpdate != 0 && NextUpdate < Time.realtimeSinceStartup))
        {
            NextUpdate = 0;
            Vector3 Position = TObject.VerticalBlockObject.GetHeight(TObject.Position.x, int.MaxValue, TObject.Position.z, 0);

            if (Position.x == -1f || Position.y == -1f || Position.z == -1f)
            {
                NextUpdate = Time.realtimeSinceStartup + 0.5f;
                return;
            }

            FirstCheck = false;
            MustCheck = false;

            PositionToApply = new Vector3(TObject.Position.x, Position.y + TerrainManager.Instance.Informations.Scale, TObject.Position.z) + TeleportOffset;
            ApplyEnd = Time.realtimeSinceStartup + FreezeDuration;

            if (DrawLog)
                Debug.Log("GroundAligner : Object Underground, Teleporting " + name + " : " + Position);
        }
    }
}
