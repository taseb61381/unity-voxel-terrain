﻿using TerrainEngine;
using UnityEngine;

public class TerrainObjectCaveDetecter : MonoBehaviour 
{
    public TerrainObject ToFollow;
    public int RaysCount = 2;
    public int RaySpace = 1;
    public int MinRayToEnable = 3;
    public float RayDistance = 30;
    public bool IsInCave = false;
    public ActiveDisableContainer OnEnterCave;
    public ActiveDisableContainer OnLeaveCave;

    public void Awake()
    {
        if (ToFollow == null)
            ToFollow = GetComponent<TerrainObject>();
        IsInCave = false;
    }

    void RayUpdate()
    {
        int x, z;
        int WorldX, WorldY = 0, WorldZ;
        int Count = 0;
        VoxelRaycastHit Hit = new VoxelRaycastHit();
        TerrainBlock Block;
        for (x = -RaysCount; x <= RaysCount; ++x)
        {
            for (z = -RaysCount; z <= RaysCount; ++z)
            {
                WorldX = ToFollow.IntCurrentVoxel.x + (x * RaySpace);
                WorldY = ToFollow.IntCurrentVoxel.y;
                WorldZ = ToFollow.IntCurrentVoxel.z + (z * RaySpace);
                Block = ToFollow.BlockObject.GetAroundBlock(ref WorldX, ref WorldY, ref WorldZ) as TerrainBlock;

                if (Block != null && TerrainRaycaster.Raycast(Block, WorldX, WorldY, WorldZ, Vector3.up, (int)RayDistance, ref Hit))
                {
                    ++Count;
                }

                if (Count >= MinRayToEnable)
                {
                    IsInCave = true;
                    return;
                }
            }
        }

        IsInCave = false;
    }

    private byte LastState = 2;
    void Update()
    {
        if (ToFollow == null || !ToFollow.HasBlock)
            return;

        if (ToFollow.HasPositionChanged)
            RayUpdate();

        if ((LastState == 2 || LastState == 0) && IsInCave)
        {
            LastState = 1;
            if (OnEnterCave != null)
                OnEnterCave.Call();
        }
        else if((LastState == 2 || LastState == 1) && !IsInCave)
        {
            LastState = 0;
            if (OnLeaveCave != null)
                OnLeaveCave.Call();
        }
    }
}
