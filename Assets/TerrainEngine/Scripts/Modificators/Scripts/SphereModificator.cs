using TerrainEngine;

public class SphereModificator : IModificator
{
    public override void UpdateModification()
    {
        SphereOperator Operator = new SphereOperator();
        Operator.InitPosition(CurrentBlock, CurrentVoxel);
        Operator.Init((byte)Type, Volume, ModType);
        Operator.Radius = Size;
        TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
    }
}
