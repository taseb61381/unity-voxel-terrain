﻿using TerrainEngine;

public class CylinderModificator : IModificator
{
    public override void UpdateModification()
    {
        CylinderOperator Operator = new CylinderOperator();
        Operator.InitPosition(CurrentBlock, CurrentVoxel);
        Operator.Init((byte)Type, Volume, ModType);
        Operator.Size = Size;
        Operator.Height = Height;
        Operator.Rotation = TObject.CTransform.rotation;
        TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
    }
}
