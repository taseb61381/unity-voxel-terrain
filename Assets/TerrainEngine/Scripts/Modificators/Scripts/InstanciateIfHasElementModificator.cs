﻿using TerrainEngine;
using UnityEngine;

/// <summary>
/// This script instanciate an gameobject if there is a data. ( Voxels have volume , or any processor have Element)
/// </summary>
public class InstanciateIfHasElementModificator : IModificator 
{
    public GameObject PrefabToInstanciate;
    public string DataName = "Grass";

    public override void UpdateModification()
    {
        if (Width == 0 && Depth == 0 && Height == 0)
            Width = Depth = Height = Size;

        int x, y, z;
        int px,py,pz;
        ITerrainBlock Block = null;
        bool MustInstanciate = false;
        for (x = -Width; x < Width + 1; ++x)
        {
            for (y = -Height; y < Height + 1; ++y)
            {
                for (z = -Depth; z < Depth + 1; ++z)
                {
                    px = x + CurrentVoxel.x;
                    py = y + CurrentVoxel.y;
                    pz = z + CurrentVoxel.z;
                    Block = TObject.BlockObject.GetAroundBlock(ref px, ref py, ref pz);
                    if (Block != null)
                    {
                        MustInstanciate = false;
                        if (DataName == null || DataName.Length == 0)
                        {
                            if (Block.Voxels.HasVolume(px, py, pz, 0.5f) )
                                MustInstanciate = true;

                        }
                        else
                        {
                            for (int i = Block.Voxels.Customs.Count - 1; i >= 0;--i)
                            {
                                if (Block.Voxels.Customs.array[i].GetType().Name.Contains(DataName))
                                {
                                    if (Block.Voxels.Customs.array[i].GetByte(px, py, pz) != 0)
                                        MustInstanciate = true;
                                }
                            }
                        }

                        if (MustInstanciate)
                        {
                            CreateGameObjectAction Action = PoolManager.UPool.GameObjectCreateActionPool.GetData<CreateGameObjectAction>();
                            Action.Init(PrefabToInstanciate, Block.GetWorldPosition(px, py, pz, false) + TerrainManager.Instance.Informations.DemiScale, Quaternion.identity, Vector3.one);
                            ActionProcessor.AddToUnity(Action);
                        }
                    }
                }
            }
        }
        base.UpdateModification();
    }
}
