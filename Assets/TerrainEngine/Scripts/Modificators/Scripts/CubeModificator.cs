
using TerrainEngine;

public class CubeModificator : IModificator
{
    public override void UpdateModification()
    {
        if (Width == 0 && Depth == 0 && Height == 0)
            Width = Depth = Height = Size;

        BoxOperator Operator = new BoxOperator();
        Operator.InitPosition(CurrentBlock, CurrentVoxel);
        Operator.Init((byte)Type, Volume, ModType);
        Operator.SizeX = Width;
        Operator.SizeY = Height;
        Operator.SizeZ = Depth;
        TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
    }
}
