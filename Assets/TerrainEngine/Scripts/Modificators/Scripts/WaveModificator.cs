﻿using TerrainEngine;
using UnityEngine;

public class WaveModificator : IModificator
{
    public bool BlockOnly = false;
    public float ElapsedOffset = 0;
    public float Speed = 1f;
    public float WaveTime = 5f;
    public float Amplitude = 1f;
    public float Attenuation = 1;
    public bool ForceReduceOverTime = true;
    public GameObject Particles;
    public int PoolContainer;
    public float ParticlesTime = 1f;
    public bool Inverse = false;

    private float px;
    private float pz;
    private float R;
    private float Sin;
    private float MaxY;

    private int[,] Heights;
    private float[,] LastSins;


    public override void UpdateModification()
    {
        if (PoolContainer == 0 && Particles != null)
            PoolContainer = GameObjectPool.GetPoolId(Particles.name);


        VoxelsOperator Operator = new VoxelsOperator();

        if (Heights == null)
        {
            Heights = new int[Size * 2 + 1, Size * 2 + 1];
            LastSins = new float[Size * 2 + 1, Size * 2 + 1];

            for (int x = -Size; x < Size + 1; ++x)
            {
                for (int z = -Size; z < Size + 1; ++z)
                {
                    LastSins[x + Size, z + Size] = float.MinValue;
                    Heights[x + Size, z + Size] = int.MinValue;

                    for (int y = -2; y < 10; ++y)
                    {
                        if (CurrentBlock.GetFloat(CurrentVoxel.x + x, CurrentVoxel.y + y, CurrentVoxel.z + z) < 0.5f)
                        {
                            Heights[x + Size, z + Size] = y;
                            break;
                        }
                    }
                }
            }
        }

        float Value = 0;
        Vector3 v = new Vector3();
        float bpos = 0;
        float CurrentTime = EaseOutExpo(ElapsedTime, 0f, WaveTime, WaveTime);
        if (CurrentTime >= WaveTime)
            CurrentTime = WaveTime;

        CurrentTime += ElapsedOffset;

        for (int x = -Size; x < Size + 1; ++x)
        {
            for (int z = -Size; z < Size + 1; ++z)
            {
                if (Heights[x + Size, z + Size] == int.MinValue)
                    continue;

                v.x = x;
                v.y = 0;
                v.z = z;

                bpos = Size - v.magnitude;
                if (bpos <= 0f) continue;
                else if (bpos > 1f) bpos = 1f;

                R = Mathf.Sqrt(x * x + z * z) - ((float)CurrentTime * Speed);
                Sin = Amplitude * Mathf.Sin(R) * bpos;
                if (ForceReduceOverTime && R != (float)0)
                    Sin /= R * Attenuation;

                if (Sin > MaxY)
                    MaxY = Sin;

                byte CurrentType = (byte)Type;

                for (int y = 0; y <= MaxY; ++y)
                {
                    Value = (Sin - (float)y);

                    if (Value > 1f)
                        Value = 1f;
                    else if (Value < 0f)
                        Value = 0f;

                    if (BlockOnly && Value <= 0.5)
                        CurrentType = 0;

                    if (Inverse)
                    {
                        if (Value >= 0.5f)
                            CurrentType = 0;
                        else
                            CurrentType = (byte)Type;
                    }

                    Operator.Voxels.Add(new VoxelPoint(CurrentBlock.BlockId, CurrentVoxel.x + x, CurrentVoxel.y + (Inverse ? -y : y) + Heights[x + Size, z + Size], CurrentVoxel.z + z, CurrentType, Value, ModType));
                }

                if (LastSins[x + Size, z + Size] != -1)
                {
                    if (LastSins[x + Size, z + Size] > Sin)
                    {
                        if (Particles != null)
                        {
                            CreateGameObjectAction Action = PoolManager.Pool.GetData<CreateGameObjectAction>("CreateGameObjectAction");
                            Action.Init(Particles,
                                CurrentBlock.GetWorldPosition(CurrentVoxel.x + x, CurrentVoxel.y + (int)Sin + Heights[x + Size, z + Size], CurrentVoxel.z + z)
                                , Quaternion.identity, new Vector3(1, 1, 1), ParticlesTime, true, PoolContainer);
                            Action.Execute(ActionProcessor.Unity);
                        }
                        LastSins[x + Size, z + Size] = -1;
                    }
                    else
                        LastSins[x + Size, z + Size] = Sin;
                }
            }
        }

        TerrainManager.Instance.ModificationInterface.AddOperator(Operator);

        if (CurrentTime >= WaveTime)
            StopModification();
    }

    public static float EaseInOutSine(float t, float b, float c, float d)
    {
        return -c / 2 * (Mathf.Cos(Mathf.PI * t / d) - 1) + b;
    }

    static public float EaseInExpo(float start, float distance, float elapsedTime, float duration)
    {
        if (elapsedTime > duration) { elapsedTime = duration; }
        return distance * Mathf.Pow(2, 10 * (elapsedTime / duration - 1)) + start;
    }

    static public float EaseOutExpo(float start, float distance, float elapsedTime, float duration)
    {
        // clamp elapsedTime to be <= duration
        if (elapsedTime > duration) { elapsedTime = duration; }
        return distance * (-Mathf.Pow(2, -10 * elapsedTime / duration) + 1) + start;
    }
    static public float EaseInOutExpo(float start, float distance, float elapsedTime, float duration)
    {
        // clamp elapsedTime so that it cannot be greater than duration
        elapsedTime = (elapsedTime > duration) ? 2.0f : elapsedTime / (duration / 2);
        if (elapsedTime < 1) return distance / 2 * Mathf.Pow(2, 10 * (elapsedTime - 1)) + start;
        elapsedTime--;
        return distance / 2 * (-Mathf.Pow(2, -10 * elapsedTime) + 2) + start;
    }
}
