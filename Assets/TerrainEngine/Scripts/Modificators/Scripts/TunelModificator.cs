﻿using TerrainEngine;

public class TunelModificator : IModificator
{
    public int EmptySize = 1;
    public float EmptyVolume = 0.4f;

    public override void UpdateModification()
    {
        TunelOperator Operator = new TunelOperator();
        Operator.InitPosition(CurrentBlock, CurrentVoxel);
        Operator.Init((byte)Type, Volume, ModType);

        Operator.Size = Size;
        Operator.Height = Height;
        Operator.Rotation = TObject.CTransform.rotation;
        Operator.EmptySize = EmptySize;
        Operator.EmptyVolume = EmptyVolume;
        TerrainManager.Instance.ModificationInterface.AddOperator(Operator);
    }
}
