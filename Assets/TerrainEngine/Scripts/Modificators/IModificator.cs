using TerrainEngine;
using UnityEngine;

public abstract class IModificator : MonoBehaviour 
{
    public TerrainObject TObject;
    public TerrainBlock CurrentBlock
    {
        get
        {
            if (TObject != null)
                return TObject.BlockObject;

            return null;
        }
    }

    public IAction CurrentAction
    {
        get
        {
            return TerrainManager.Instance.ModificationInterface.CurrentAction;
        }
    }

    public bool IsStarted = false;
    public bool IsDone = false;
    public bool RemoveWhenDone = false;
    public bool RemoveScriptWhenDone = false;
    public bool RealTimeModification = false;

    public int Size = 2;
    public int Width = 0;
    public int Height = 0;
    public int Depth = 0;

    public int OffsetX = 0, OffsetY = 0, OffsetZ = 0;

    public int Type = 0;
    public float Volume = 1f;
    public VoxelModificationType ModType = VoxelModificationType.REMOVE_VOLUME;
    public int Count = 0;
    public VectorI3 CurrentVoxel;
    public float StartDelay = 0;
    public float CheckTime = 0f;
    public float ElapsedTime = 0f;

    private float StartTime = 0;
    private float StartDelayed = 0;
    private VectorI3 BlockPosition;
    private float NextCheck = 0;
    internal int CurrentCount = 0;

    void Start()
    {
        if (TObject == null)
        {
            TObject = gameObject.GetComponent<TerrainObject>();

            if(TObject == null)
                TObject = gameObject.AddComponent<TerrainObject>();

            TObject.UpdateCurrentVoxel = false;
        }

        if (StartDelay != 0)
            DelayedStart(null, StartDelay);

        StartModification();
    }

    void Update()
    {
        if (!IsStarted || IsDone)
            return;

        if (TerrainManager.Instance == null)
            return;

        if (StartDelayed != 0 && Time.realtimeSinceStartup < StartDelayed)
            return;

        if (!TerrainManager.Instance.isStarted)
            return;

        if (CurrentBlock == null )
            return;

        if (CanModify())
        {
            if (NextCheck >= Time.realtimeSinceStartup)
                return;

            NextCheck = Time.realtimeSinceStartup + CheckTime;
            if (CurrentCount == 0)
            {
                StartTime = Time.realtimeSinceStartup;
                OnStart();
            }

            ElapsedTime = Time.realtimeSinceStartup - StartTime;

            if (Count == 0 || CurrentCount < Count)
            {
                CurrentVoxel = TObject.CurrentVoxel;
                CurrentVoxel.x += OffsetX;
                CurrentVoxel.y += OffsetY;
                CurrentVoxel.z += OffsetZ;

                UpdateModification();
                TerrainManager.Instance.ModificationInterface.SetDirty();
                ++CurrentCount;
            }
            else
                StopModification();
        }
    }

    void OnEnable()
    {
        IsStarted = false;
        StartModification();
    }

    public void DelayedStart(TerrainManager Manager, float DelayedTime)
    {
        StartDelayed = Time.realtimeSinceStartup + DelayedTime;
    }

    public void StartModification()
    {
        if (IsStarted)
            return;

        IsStarted = true;
        CurrentCount = 0;
        StartTime = 0f;
    }

    public void StopModification()
    {
        if (!IsStarted)
            return;

        IsStarted = false;
        IsDone = true;
        CurrentCount = 0;
        OnStop();

        if (RemoveWhenDone)
        {
            enabled = false;
            ActionProcessor.Add(new DeleteGameObjectAction(gameObject), -1, true);
        }

        if (RemoveScriptWhenDone)
        {
            GameObject.Destroy(this);
        }
    }

    public bool CanModify()
    {
        return CurrentAction == null || CurrentAction.IsDone();
    }

    public virtual void UpdateModification()
    {

    }

    public virtual void OnStart()
    {

    }

    public virtual void OnStop()
    {

    }
}
