﻿using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    public class TerrainChunkGenerateGroupAction : TerrainChunkBuildAction
    {
        public List<ChunkGenerateInfo> Chunks = new List<ChunkGenerateInfo>(2);
        public PhysicsFlushUpdater PhysicsFlush;
        private bool ForceGroup = false;

        public TerrainChunkGenerateGroupAction() : base()
        {

        }

        public TerrainChunkGenerateGroupAction(List<ChunkGenerateInfo> Chunks, bool ForceGroup = false) : base()
        {
            this.Chunks = new List<ChunkGenerateInfo>(Chunks);
            this.ForceGroup = ForceGroup;
            this.OnActionEvent = null;
            this.PhysicsFlush = null;
        }

        public override void Clear()
        {
            this.OnActionEvent = null;
            this.PhysicsFlush = null;
        }

        public void Init(List<ChunkGenerateInfo> Chunks, bool ForceGroup = false)
        {
            this.Chunks = new List<ChunkGenerateInfo>(Chunks);
            this.ForceGroup = ForceGroup;
        }

        public override bool OnExecute(ActionThread Thread)
        {
            ExecuteAction(Thread);

            return true;
        }

        public void AddChunk(ChunkGenerateInfo Info)
        {
            Chunks.Add(Info);
        }

        public IAction ExecuteAction(ActionThread Thread, bool InstantBuild = false)
        {
            TerrainChunkRef ChunkRef;
            int z = 0;
            IMeshData Data;
            TerrainBlock Block = null;
            TerrainChunkScript Script = null;

            if (OnActionEvent != null)
                OnActionEvent(Thread, this);

            for (int i = 0; i < Chunks.Count; ++i)
            {
                ChunkRef = Chunks[i].ChunkRef;
                if (!ChunkRef.IsValid(ref Block))
                {
                    Debug.LogError("Can not generate Chunk. Block not found :" + ChunkRef.BlockId);
                    continue;
                }

                Block.SetChunkState(ChunkRef.ChunkId, ChunkStates.Flush, false);

                Data = Block.GenerateChunk(Thread, ChunkRef.ChunkId);
                if (InstantBuild)
                {
                    Block.CheckChunkGameObject(ChunkRef.ChunkId, ref Script, Data != null ? Data.IsValid() : false);
                    Block.Build(Data, ChunkRef.ChunkId);

                    int Count = TerrainManager.Instance.Processors.Count;
                    for (z = 0; z < Count; ++z)
                        TerrainManager.Instance.Processors[z].OnChunkBuild(Block, ChunkRef.ChunkId, null);
                }
                else
                {
                    ChunkGenerateInfo Info = Chunks[i];
                    Info.Data = Data;
                    Chunks[i] = Info;
                }
            }

            if (InstantBuild)
                return null;

            if (ForceGroup || Chunks.Count > 1)
            {
                TerrainChunkBuildGroupAction BuildGroup = new TerrainChunkBuildGroupAction(Chunks.ToArray(), false);
                BuildGroup.FlushId = FlushId;
                BuildGroup.OnActionEvent = OnActionEvent;
                OnActionEvent = null;
                ActionProcessor.AddToUnity(BuildGroup);
                return BuildGroup;
            }
            else
            {
                for (int i = 0; i < Chunks.Count; ++i)
                {
                    TerrainChunkBuildMeshAction Build = new TerrainChunkBuildMeshAction(Chunks[i]);
                    Build.FlushId = FlushId;
                    Build.OnActionEvent = OnActionEvent;
                    ActionProcessor.AddToUnity(Build);
                }
                OnActionEvent = null;
            }

            return null;
        }
    }
}
