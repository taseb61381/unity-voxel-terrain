﻿using System;


namespace TerrainEngine
{
    public class GenerateAndBuildGroupEvent : IPoolable
    {
        public virtual void OnCreateGenerateAction(TerrainChunkGenerateAndBuildGroup Current, TerrainChunkGenerateAction Action)
        {

        }

        public virtual void OnCreateBuildGroupAction(TerrainChunkGenerateAndBuildGroup Current,TerrainChunkBuildGroupAction Action)
        {

        }

        public virtual void OnCloseActions(TerrainChunkGenerateAndBuildGroup Current, TerrainChunkGenerateAndBuildGroup Action)
        {

        }

        public virtual void OnGenerateChunk(ActionThread Thread,TerrainBlock Block, int ChunkId)
        {

        }

        public virtual bool OnTerrainBuilded(ActionThread Thread,TerrainBlock Block)
        {
            return true;
        }

        public virtual void OnBuildCheckGameObject(ActionThread Thread, TerrainBlock Block, int ChunkId)
        {

        }

        public virtual void OnBuildChunk(ActionThread Thread, TerrainBlock Block, int ChunkId)
        {

        }

        public virtual bool CanStartToBuild()
        {
            return true;
        }

        public virtual bool CanFinish()
        {
            return true;
        }

        public override void Clear()
        {
            
        }
    }
    public class TerrainChunkGenerateAndBuildGroup : IAction
    {
        public PoolInfoRef Pool;

        public ChunkGenerateInfo[] Chunks;
        public ActionProgressCounter Counter;
        public TerrainChunkBuildGroupAction BuildGroup;
        public GenerateAndBuildGroupEvent Event;
        public bool CanBuild = true;
        public byte Step = 0;

        public TerrainChunkGenerateAndBuildGroup()
        {

        }

        public void Init(ChunkGenerateInfo[] Chunks)
        {
            if (Pool.Index == 0)
                Pool = PoolManager.Pool.GetPool("TerrainChunkGenerateAction");

            this.Chunks = Chunks;
            this.Step = 0;

            if (this.Counter == null)
                this.Counter = new ActionProgressCounter();
            else
                this.Counter.Clear();

            this.CanBuild = true;
            this.BuildGroup = null;
        }

        public void Start()
        {
            for (int i = 0; i < Chunks.Length; ++i)
            {
                TerrainChunkGenerateAction Action = Pool.GetData<TerrainChunkGenerateAction>();
                Action.Init(Chunks[i].ChunkRef, false);
                Counter.Add(Action);
                ActionProcessor.AddToThreadFast(Action);

                if (Event != null)
                    Event.OnCreateGenerateAction(this, Action);
            }
        }

        public override bool OnExecute(ActionThread Thread)
        {
            if (Step == 0) // Wait all threads finishing to Generate Chunks
            {
                if (Chunks.Length == 0)
                    return true;

                if (!Counter.IsDone())
                    return false;

                if (Event != null && !Event.CanStartToBuild())
                    return false;

                int Len = Chunks.Length - 1;
                for (int i = Len; i >= 0; --i)
                {
                    Chunks[i].Data = (Counter.Actions[i] as TerrainChunkGenerateAction).Data;
                }

                BuildGroup = new TerrainChunkBuildGroupAction(Chunks, false);
                BuildGroup.CanBuild = CanBuild;

                if (Event != null)
                    Event.OnCreateBuildGroupAction(this, BuildGroup);

                ActionProcessor.AddToUnity(BuildGroup);
                Step = 1;
                return false;
            }
            else if (Step == 1) // Wait Unity Thread finishing to apply meshes
            {
                if (Event != null && !Event.CanFinish())
                    return false;

                if (BuildGroup == null || BuildGroup.IsDone())
                {
                    if (Counter != null)
                    {
                        for (int i = 0; i < Counter.Actions.Count; ++i)
                        {
                            (Counter.Actions[i] as TerrainChunkGenerateAction).ForceClose();
                        }
                    }


                    Counter = null;
                    Chunks = null;
                    BuildGroup = null;
                    if (Event != null)
                        Event.Close();
                    Event = null;

                    Close();
                    return true;
                }
                return false;
            }

            return false;
        }
    }
}
