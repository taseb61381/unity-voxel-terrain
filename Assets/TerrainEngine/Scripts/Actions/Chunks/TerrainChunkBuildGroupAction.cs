﻿

namespace TerrainEngine
{
    public class TerrainChunkBuildGroupAction : TerrainChunkBuildAction
    {
        public ChunkGenerateInfo[] Builds;
        public bool CanBuild = true;
        private bool Force = false;
        private int CallEventPosition;
        public GenerateAndBuildGroupEvent Event;

        public byte Step = 0;


        public TerrainChunkBuildGroupAction(ChunkGenerateInfo[] Builds, bool Force)
        {
            this.Builds = Builds;
            this.Force = Force;
            this.CallEventPosition = 0;
            this.Event = null;
            this.OnActionEvent = null;
        }

        public override void Clear()
        {
            Step = 0;

            this.CanBuild = true;
            this.Builds = null;
            this.Force = false;
            this.CallEventPosition = 0;
            this.OnActionEvent = null;
            this.Event = null;
        }

        public void Build(ActionThread Thread)
        {
            HasCalledEvents = true;
            Generate(Thread);

            if (OnActionEvent != null)
                OnActionEvent(Thread, this);
            Step = 2;

        }

        public override bool OnExecute(ActionThread Thread)
        {
            if(Step == 0) // Check GameObjects
            {
                if (!CheckGameObjects(Thread))
                {
                    if (!Force)
                        return false;
                }

                Step = 1;

                if(!Thread.HasTime())
                    return false;
            }

            if(Step == 1) // Build Mesh
            {
                if (CanBuild)
                {
                    Build(Thread);
                }

                if (!Thread.HasTime())
                    return false;
            }

            if(Step == 2) // Call Processors
            {
                CallEvents(Thread);

                if (CallEventPosition >= Builds.Length)
                    return true;
                else
                    return false;
            }

            return false;
        }

        public bool CheckGameObjects(ActionThread Thread)
        {
            TerrainChunkRef ChunkRef;
            TerrainBlock Block = null;
            TerrainChunkScript Script = null;

            bool Result = true;
            for (int i = 0; i < Builds.Length; ++i)
            {
                ChunkRef = Builds[i].ChunkRef;
                if (!ChunkRef.IsValid(ref Block))
                {
                    continue;
                }

                if (!Block.CheckChunkGameObject(ChunkRef.ChunkId, ref Script, Builds[i].Data != null ? Builds[i].Data.IsValid() : false))
                    Result = false;

                if (Event != null)
                    Event.OnBuildCheckGameObject(Thread, Block, ChunkRef.ChunkId);
            }

            return Result;
        }

        private void Generate(ActionThread Thread)
        {
            TerrainChunkRef ChunkRef;
            TerrainBlock Block = null;
            for (int i = 0; i < Builds.Length; ++i)
            {
                ChunkRef = Builds[i].ChunkRef;
                if (!ChunkRef.IsValid(ref Block))
                    continue;

                Block.Build(Builds[i].Data, ChunkRef.ChunkId);
            }
        }

        public void CallEvents(ActionThread Thread)
        {
            TerrainChunkRef ChunkRef;
            TerrainBlock Block = null;
            int i;
            int Count = TerrainManager.Instance.Processors.Count;

            while (CallEventPosition < Builds.Length)
            {
                ChunkRef = Builds[CallEventPosition].ChunkRef;
                ++CallEventPosition;

                if (!ChunkRef.IsValid(ref Block))
                    continue;

                if (Event != null)
                    Event.OnBuildChunk(Thread, Block, ChunkRef.ChunkId);

                for (i = 0; i < Count; ++i)
                    TerrainManager.Instance.Processors[i].OnChunkBuild(Block, ChunkRef.ChunkId, null);

                if (!Thread.HasTime())
                    break;
            }
        }
    }
}
