﻿
namespace TerrainEngine
{
    public class TerrainChunkBuildAction : IAction
    {
        static public int MaxFlushId = int.MinValue;
        public int GetFlushId()
        {
            if (MaxFlushId >= int.MaxValue)
                MaxFlushId = int.MinValue + 1;

            return ++MaxFlushId;
        }

        public int FlushId = int.MinValue;
        public ActionDoneCallback OnActionEvent;
        public bool HasCalledEvents;

        public TerrainChunkBuildAction()
        {
            FlushId = GetFlushId();
        }

        public override void Clear()
        {
            HasCalledEvents = false;
            OnActionEvent = null;
            FlushId = int.MinValue;
        }
    }

    public class TerrainChunkBuildMeshAction : TerrainChunkBuildAction
    {
        private ChunkGenerateInfo Build;

        public TerrainChunkBuildMeshAction(ChunkGenerateInfo Build)
        {
            this.Build = Build;
            this.HasCalledEvents = false;
            this.OnActionEvent = null;
        }

        public override bool OnUpdate(ActionThread Thread)
        {
            TerrainBlock Block = null;
            if (!HasCalledEvents)
            {
                HasCalledEvents = true;

                if (Build.ChunkRef.IsValid(ref Block))
                {
                    Block.Build(Build.Data, Build.ChunkRef.ChunkId);
                }

                if (OnActionEvent != null)
                    OnActionEvent(Thread, this);

                return false;
            }
            else
            {
                if (Build.ChunkRef.IsValid(ref Block))
                {
                    int Count = TerrainManager.Instance.Processors.Count;
                    for (int i = 0; i < Count; ++i)
                        TerrainManager.Instance.Processors[i].OnChunkBuild(Block, Build.ChunkRef.ChunkId, null);
                }

                return true;
            }
        }
    }
}
