﻿

namespace TerrainEngine
{
    public class TerrainChunkGenerateAction : IAction
    {
        public TerrainChunkRef ChunkRef;
        public IMeshData Data;
        public bool BuildMesh = true;
        public GenerateAndBuildGroupEvent Event;

        public TerrainChunkGenerateAction()
        {

        }

        public TerrainChunkGenerateAction(TerrainChunkRef ChunkRef, bool BuildMesh =true)
        {
            this.ChunkRef = ChunkRef;
            this.BuildMesh = BuildMesh;
            this.Data = null;
            this.Event = null;
        }

        public void Init(TerrainChunkRef ChunkRef, bool BuildMesh = true)
        {
            this.ChunkRef = ChunkRef;
            this.BuildMesh = BuildMesh;
            this.Data = null;
            this.Event = null;
        }

        public override void Clear()
        {
            base.Clear();
            this.Event = null;
        }

        public override bool OnThreadUpdate(ActionThread Thread)
        {
            TerrainBlock Block = null;
            if (ChunkRef.IsValid(ref Block))
            {
                Data = Block.GenerateChunk(Thread, ChunkRef.ChunkId);

                if (Event != null)
                    Event.OnGenerateChunk(Thread, Block, ChunkRef.ChunkId);

                if (BuildMesh)
                {
                    ActionProcessor.AddToUnity(this);
                    return true;
                }
            }
            return true;
        }

        public override bool OnUpdate(ActionThread Thread)
        {
            TerrainBlock Block = null;
            if (ChunkRef.IsValid(ref Block))
            {
                Block.Build(Data, ChunkRef.ChunkId);

                if (Event != null)
                    Event.OnBuildChunk(Thread, Block, ChunkRef.ChunkId);
            }
            return true;
        }
    }
}
