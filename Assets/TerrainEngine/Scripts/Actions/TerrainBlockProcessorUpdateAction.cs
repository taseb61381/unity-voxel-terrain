﻿using System;
using System.Collections.Generic;

namespace TerrainEngine
{
    public class TerrainBlockProcessorUpdateAction : IAction
    {
        public void Init(TerrainBlock Block)
        {
            this.Block = Block;
            this.IsStopped = false;
        }

        public override void Clear()
        {
            UnityProcessors.Clear();
            ThreadProcessors.Clear();
        }

        public bool IsStopped = false;
        public TerrainBlock Block;
        public Queue<IBlockProcessor> UnityProcessors = new Queue<IBlockProcessor>();
        public Queue<IBlockProcessor> ThreadProcessors = new Queue<IBlockProcessor>();

        public void AddProcessor(IBlockProcessor Processor)
        {
            if (Processor.InterlacedUpdate)
            {
                AddProcessor(ThreadProcessors, Processor);
            }
            else
            {
                if (Processor.EnableUnityUpdateFunction)
                    AddProcessor(UnityProcessors, Processor);

                if (Processor.EnableThreadUpdateFunction)
                    AddProcessor(ThreadProcessors, Processor);
            }
        }

        public void AddProcessor(Queue<IBlockProcessor> Queue, IBlockProcessor Processor)
        {
            lock (Queue)
                Queue.Enqueue(Processor);
        }

        public IBlockProcessor GetProcessor(Queue<IBlockProcessor> Queue)
        {
            lock (Queue)
            {
                if (Queue.Count != 0)
                    return Queue.Dequeue();
            }

            return null;
        }

        public override bool OnThreadUpdate(ActionThread Thread)
        {
            if (IsStopped)
            {
                return true;
            }

            bool Result = false;

            IBlockProcessor Processor = GetProcessor(ThreadProcessors);
            if ((object)Processor != null)
            {
                try
                {

                    Result = Processor.OnTerrainThreadUpdate(Thread, Block);
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogError(e.ToString());
                }

                if (Result)
                {
                    if (Processor.InterlacedUpdate)
                        AddProcessor(UnityProcessors, Processor);
                    else
                        AddProcessor(ThreadProcessors, Processor);
                }
                else
                    AddProcessor(ThreadProcessors, Processor);
            }

            return false;
        }

        public override bool OnUpdate(ActionThread Thread)
        {
            if (IsStopped)
            {
                return true;
            }

            bool Result = false;

            IBlockProcessor Processor = GetProcessor(UnityProcessors);
            if ((object)Processor != null)
            {
                try
                {
                    Result = Processor.OnTerrainUnityUpdate(Thread, Block);
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogError(e.ToString());
                }
                if (Result)
                {
                    if (Processor.InterlacedUpdate)
                        AddProcessor(ThreadProcessors, Processor);
                    else
                        AddProcessor(UnityProcessors, Processor);
                }
                else
                    AddProcessor(UnityProcessors, Processor);
            }

            return false;
        }
    }
}
