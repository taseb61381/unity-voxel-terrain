﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TerrainEngine
{
    public class CombinedChunks
    {
        public List<int> Chunks = new List<int>();
        public TerrainCombinedScript Script;
        public Material[] Materials;
    }

    public class TerrainChunksCombiner
    {
        public class CombinerData
        {
            public Material[] Materials;
            public int Vertices;
            public List<ChunkGenerateInfo> Chunks;
            public List<int> IndicesCount;
            public IMeshData CombinedMesh;

            public void Clear()
            {
                Vertices = 0;
                Chunks.Clear();
                IndicesCount.Clear();

                if (CombinedMesh != null)
                    CombinedMesh.Clear();
            }
        }

        public SList<CombinerData> ChunksData = new SList<CombinerData>(2);
        public Stack<CombinerData> DataPool = new Stack<CombinerData>();

        public bool UseUV = false;
        public bool UseNormal = false;
        public bool UseTangents = false;
        public bool UseColors = false;

        public void Clear()
        {
            for (int i = 0; i < ChunksData.Count; ++i)
            {
                DataPool.Push(ChunksData.array[i]);
                ChunksData.array[i].Clear();
            }
            ChunksData.Clear();
            UseUV = UseNormal = UseTangents = UseColors = false;
        }

        public CombinerData GetCombinedData(IMeshData Data,int Count)
        {
            CombinerData CData = null;
            for(int i=ChunksData.Count-1;i>=0;--i)
            {
                if (ChunksData.array[i].Materials == Data.Materials)
                {
                    CData = ChunksData.array[i];
                    break;
                }
            }

            if (CData == null)
            {
                if (DataPool.Count == 0)
                {
                    CData = new CombinerData();
                    CData.Chunks = new List<ChunkGenerateInfo>(Count);
                    CData.IndicesCount = new List<int>();
                }
                else
                    CData =  DataPool.Pop();

                CData.Materials = Data.Materials;

                if (Data.UVs != null && Data.UVs.Length > 0)
                    UseUV = true;
                if (Data.Normals != null && Data.Normals.Length > 0)
                    UseNormal = true;
                if (Data.Tangents != null && Data.Tangents.Length > 0)
                    UseTangents = true;
                if (Data.Colors != null && Data.Colors.Length > 0)
                    UseColors = true;


                ChunksData.Add(CData);
            }

            return CData;
        }

        /// <summary>
        /// [Thread]Check all chunks if they are valid and can be combined.
        /// </summary>
        public void AddChunks(TerrainBlock Terrain, FList<ChunkGenerateInfo> Chunks)
        {
            Clear();

            ChunkGenerateInfo Info;
            CombinerData CData;
            TerrainChunk Chunk;
            int j, m;

            for (j = 0; j < Chunks.Count; ++j)
            {
                Info = Chunks.array[j];
                if (Info.ChunkRef.BlockId != Terrain.BlockId)
                    continue;

                if (!IsValidMesh(Info.Data))
                    continue;

                Chunk = Info.ChunkRef.Chunk;
                if (!IsValidChunk(Chunk))
                    continue;

                CData = GetCombinedData(Info.Data, Chunks.Count);
                if (CData.Vertices > 60000)
                    continue;

                for (m = 0; m < Info.Data.Materials.Length; ++m)
                {
                    if (CData.IndicesCount.Count <= m)
                        CData.IndicesCount.Add(0);

                    CData.IndicesCount[m] += Info.Data.Indices[m].Length;
                }

                CData.Vertices += Info.Data.Vertices.Length;
                CData.Chunks.Add(Info);
            }
        }

        /// <summary>
        /// [Thread]Combine all chunks into few MeshData, grouped by an unique Material[]
        /// </summary>
        public void Combine(TerrainBlock Terrain)
        {
            int i, j,d;
            Vector3 WorldPosition;
            Vector3 ChunkWorldPosition;
            ChunkGenerateInfo Info;
            int CurrentVertices = 0;
            int CurrentIndices = 0;

            for (int v = 0; v < ChunksData.Count; ++v)
            {
                CombinerData CData = ChunksData.array[v];
                if (CData.Vertices >= 65000)
                    continue;

                if (CData.CombinedMesh == null)
                    CData.CombinedMesh = IMeshData.Create(UseUV, UseTangents, UseColors);

                CurrentVertices = 0;

                Vector3[] Vertices = null;
                Vector2[] Uvs = null;
                Vector3[] Normals = null;
                Vector4[] Tangents = null;
                Color[] Colors = null;
                int[][] Indices = null;
                Material[] Materials = null;

                Vertices = new Vector3[CData.Vertices];
                if (UseUV) Uvs = new Vector2[CData.Vertices];
                if (UseNormal) Normals = new Vector3[CData.Vertices];
                if (UseTangents) Tangents = new Vector4[CData.Vertices];
                if (UseColors) Colors = new Color[CData.Vertices];

                Indices = new int[CData.Materials.Length][];
                Materials = CData.Materials;

                for (d = 0; d < CData.Materials.Length; ++d)
                {
                    Indices[d] = new int[CData.IndicesCount[d]];
                    CData.IndicesCount[d] = 0;
                }

                CData.CombinedMesh.SetVertices(Vertices);
                CData.CombinedMesh.SetUVs(Uvs);
                CData.CombinedMesh.SetColors(Colors);
                CData.CombinedMesh.SetNormals(Normals);
                CData.CombinedMesh.SetTangents(Tangents);
                CData.CombinedMesh.SetMaterials(Materials);
                CData.CombinedMesh.SetIndices(Indices);

                for (j = 0; j < CData.Chunks.Count; ++j)
                {
                    Vector3[] DataVertices = null;
                    int[][] DataIndices = null;
                    Vector3[] DataNormals = null;
                    Vector2[] DataUvs = null;
                    Vector4[] DataTangents = null;
                    Color[] DataColors = null;
                    Material[] DataMaterials = null;


                    Info = CData.Chunks[j];
                    ChunkWorldPosition = Terrain.GetChunkWorldPosition(Info.ChunkRef.ChunkId);

                    Info.Data.Combined = true;
                    Info.Data.GetVerticesCount(ref DataVertices);
                    Info.Data.GetIndicesCount(ref DataIndices);
                    Info.Data.GetNormalsCount(ref DataNormals);
                    Info.Data.GetUVCount(ref DataUvs);
                    Info.Data.GetTangentsCount(ref DataTangents);
                    Info.Data.GetMaterialsCount(ref DataMaterials);

                    Info.Data.GetColorsCount(ref DataColors);
                    WorldPosition = ChunkWorldPosition - Terrain.WorldPosition;
                    for (i = 0; i < DataVertices.Length; ++i)
                    {
                        Vertices[i + CurrentVertices] = DataVertices[i] + WorldPosition;
                    }

                    if (UseUV)
                    {
                        Array.Copy(DataUvs, 0, Uvs, CurrentVertices, DataUvs.Length);
                    }

                    if (UseNormal)
                    {
                        Array.Copy(DataNormals, 0, Normals, CurrentVertices, DataNormals.Length);
                    }

                    if (UseTangents)
                    {
                        Array.Copy(DataTangents, 0, Tangents, CurrentVertices, DataTangents.Length);
                    }

                    if (UseColors)
                    {
                        Array.Copy(DataColors, 0, Colors, CurrentVertices, DataColors.Length);
                    }

                    for (d = 0; d < CData.Materials.Length; ++d)
                    {
                        CurrentIndices = CData.IndicesCount[d];
                        for (i = 0; i < DataIndices[d].Length; ++i)
                        {
                            Indices[d][i + CurrentIndices] = DataIndices[d][i] + CurrentVertices;
                        }

                        CData.IndicesCount[d] += DataIndices[d].Length;
                    }

                    CurrentVertices += DataVertices.Length;
                }
            }
        }

        /// <summary>
        /// [Unity]Apply combined mesh to GameObject.
        /// </summary>
        static public string ObjectName = "TCombined";
        static public GameObject DefaultGameObject;
        public bool Apply(TerrainBlock Terrain)
        {
            if (ChunksData.Count == 0)
                return false;

            if (DefaultGameObject == null)
            {
                GameObject DefaultPoolObject = new GameObject();
                DefaultPoolObject.name = ObjectName;
                DefaultPoolObject.AddComponent<TerrainCombinedScript>();
                DefaultPoolObject.SetActive(false);
                DefaultPoolObject.transform.parent = TerrainManager.Instance.transform;
                GameObjectPool.SetDefaultObject(ObjectName, DefaultPoolObject);
            }

            Terrain.CheckGameObject();
            ITerrainScript IScript = Terrain.Script as ITerrainScript;
            int Count = 0;
            TerrainCombinedScript Script;
            string Name;

            CombinerData Data;
            for (int v = 0; v < ChunksData.Count; ++v)
            {
                Data = ChunksData.array[v];
                if (Data != null && Data.CombinedMesh != null && !Data.CombinedMesh.IsValid())
                {
                    if (Data != null)
                        Data.Clear();

                    continue;
                }

                Name = ObjectName;
                Script = IScript.GetSubScript<TerrainCombinedScript>(ObjectName.GetHashCode(),Count);
                if (Script == null)
                {
                    Script = GameObjectPool.GetGameObject(ObjectName).GetComponent<TerrainCombinedScript>();
                    GameObject Obj = Script.gameObject;
                    Obj.layer = LayerMask.NameToLayer("Chunk");
                    Obj.name = ObjectName;

                    Script.Init(Name.GetHashCode(),Count, null, IScript);
                    Script.CTransform.parent = IScript.transform;
                    Script.CTransform.localPosition = new Vector3();
                }

                Script.Apply(Data.CombinedMesh);

                CombinedChunks L = new CombinedChunks();
                L.Script = Script;
                for (int i = 0; i < Data.Chunks.Count; ++i)
                {
                    L.Chunks.Add(Data.Chunks[i].ChunkRef.ChunkId);
                }

                if (Terrain.Combined == null)
                    Terrain.Combined = new List<CombinedChunks>(10);
                Terrain.Combined.Add(L);

                ++Count;
                Data.Clear();
            }
            ChunksData.Clear();
            return true;
        }

        static public bool IsValidMesh(IMeshData Data)
        {
            if (Data == null || !Data.IsValid() || Data.GetVerticesCount() > 3000)
                return false;

            return true;
        }

        static public bool IsValidChunk(TerrainChunk Chunk)
        {
            if (TerrainManager.Instance.RegenerateCornersChunks)
            {
                if (Chunk.LocalX == TerrainManager.Instance.Informations.ChunkPerBlock - 1
                    || Chunk.LocalY == TerrainManager.Instance.Informations.ChunkPerBlock - 1
                    || Chunk.LocalZ == TerrainManager.Instance.Informations.ChunkPerBlock - 1)
                    return false;
            }

            return true;
        }
    }
}