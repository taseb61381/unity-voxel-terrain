﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace TerrainEngine
{
    public class GraphicMeshContainer : IPoolable
    {
        static public PoolInfoRef GraphicPool;
        public int ChunkId;
        public bool HasMesh = true;
        public Vector3 Position;

        public GraphicMeshContainer()
        {
            if (GraphicPool.Index == 0)
                GraphicPool = PoolManager.Pool.GetPool("GraphicMesh");
        }

        [NonSerialized]
        internal FList<GraphicMesh> GraphicsMesh = new FList<GraphicMesh>(1);

        public GraphicMesh GetMesh(string Name, bool Remove = false)
        {
            for (int GraphId = GraphicsMesh.Count - 1; GraphId >= 0; --GraphId)
            {
                if (GraphicsMesh.array[GraphId].Name == Name)
                {
                    if (Remove)
                    {
                        GraphicMesh Mesh = GraphicsMesh.array[GraphId];
                        GraphicsMesh.RemoveAt(GraphId);
                        return Mesh;
                    }

                    return GraphicsMesh.array[GraphId];
                }
            }

            return null;
        }

        public GraphicMesh AddMesh(string Name, bool Search=true)
        {
            GraphicMesh Mesh;
            HasMesh = true;
            if (Search)
            {
                Mesh = GetMesh(Name);
                if (Mesh != null)
                    return Mesh;
            }

            Mesh = GetNewGraphic();
            Mesh.Name = Name;

            GraphicsMesh.Add(Mesh);
            return Mesh;
        }

        public void UpdateMesh(GraphicMesh Mesh, IMeshData Data)
        {
            Mesh.UpdateMesh(Data);
            HasMesh = true;
        }

        public void SetMeshActive(GraphicMesh Mesh, bool Enable)
        {
            Mesh.Enabled = Enable;
            if (Enable)
                HasMesh = true;
            else if (GraphicsMesh.Count == 0)
                HasMesh = false;
        }

        public void RemoveMesh(string Name)
        {
            GraphicMesh Mesh = GetMesh(Name, true);
            if (Mesh != null)
            {
                Mesh.Close();
            }

            HasMesh = GraphicsMesh.Count != 0;
        }

        public bool HasTag(string Tag)
        {
            for (int GraphId = GraphicsMesh.Count - 1; GraphId >= 0; --GraphId)
                if (GraphicsMesh.array[GraphId].Tag == Tag)
                    return true;

            return false;
        }

        public void RemoveMeshByTag(string Tag)
        {
            if (GraphicsMesh.Count != 0)
            {
                for (int i = 0; i < GraphicsMesh.Count; )
                {
                    if (GraphicsMesh.array[i].Tag != null && GraphicsMesh.array[i].Tag == Tag)
                    {
                        GraphicsMesh.array[i].Close();
                        GraphicsMesh.RemoveAt(i);
                        continue;
                    }

                    ++i;
                }
            }

            HasMesh = GraphicsMesh.Count != 0;
        }

        public void Draw(Vector3 Position)
        {
            if (GraphicsMesh.Count != 0)
            {
                HasMesh = false;

                for (int GraphId = GraphicsMesh.Count - 1; GraphId >= 0; --GraphId)
                {
                    if (GraphicsMesh.array[GraphId].Draw(Position))
                    {
                        HasMesh = true;
                    }
                }
            }
        }

        public virtual GraphicMesh GetNewGraphic()
        {
            return GraphicPool.GetData<GraphicMesh>();
        }

        public override void Clear()
        {
            if (GraphicsMesh.array != null)
            {
                for (int i = 0; i < GraphicsMesh.Count; ++i)
                {
                    GraphicsMesh.array[i].Close();
                }

                GraphicsMesh.Clear();
            }

            HasMesh = true;
        }
    };

    [Serializable]
    public class LODMeshContainer : GraphicMeshContainer
    {
        static public PoolInfoRef GraphicLODPool;

        public LODMeshContainer()
        {
            if (GraphicLODPool.Index == 0)
                GraphicLODPool = PoolManager.Pool.GetPool("GraphicLODMesh");
        }

        public override GraphicMesh GetNewGraphic()
        {
            return GraphicLODPool.GetData<GraphicLODMesh>();
        }

        public bool HasDirtyMesh()
        {
            for (int i = 0; i < GraphicsMesh.Count; ++i)
            {
                if ((GraphicsMesh.array[i] as GraphicLODMesh).Dirty)
                    return true;
            }

            return false;
        }
    }

    [Flags]
    public enum GraphicMeshStates : byte
    {
        None = 0,
        Enabled = 1,
        CastShadow = 2,
        ReceiveShadow = 4,
        UseTangents = 8,
        PoolMaterials = 16,
    };

    public class GraphicMesh : IPoolable
    {
        static public Quaternion Identity = Quaternion.identity;
        static public MaterialPropertyBlock Property;

        static public int _CutoffId = 0;
        static public int _ColorId = 0;
        static public int _FadeColorId = 0;

        public string Name;
        public string Tag = null;
        public Mesh CMesh;
        public Material[] Materials;
        public byte[] HasProperties;
        public byte Resolution=1;

        public GraphicMeshStates State;
        public MeshTopology Topology = MeshTopology.Triangles;
        public string FadeName = "";
        public float FadeTime = 0;
        public float FadeStart = 0;

        public bool Enabled
        {
            get
            {
                return (State & GraphicMeshStates.Enabled) == GraphicMeshStates.Enabled;
            }
            set
            {
                if (value)
                {
                    State |= GraphicMeshStates.Enabled;
                }
                else
                {
                    State &= ~GraphicMeshStates.Enabled;
                }
            }
        }

        public bool CastShadow
        {
            get
            {
                return (State & GraphicMeshStates.CastShadow) == GraphicMeshStates.CastShadow;
            }
            set
            {
                if (value)
                    State |= GraphicMeshStates.CastShadow;
                else
                    State &= ~GraphicMeshStates.CastShadow;
            }
        }

        public bool ReceiveShadow
        {
            get
            {
                return (State & GraphicMeshStates.ReceiveShadow) == GraphicMeshStates.ReceiveShadow;
            }
            set
            {
                if (value)
                    State |= GraphicMeshStates.ReceiveShadow;
                else
                    State &= ~GraphicMeshStates.ReceiveShadow;
            }
        }

        public bool UseTangents
        {
            get
            {
                return (State & GraphicMeshStates.UseTangents) == GraphicMeshStates.UseTangents;
            }
            set
            {
                if (value)
                    State |= GraphicMeshStates.UseTangents;
                else
                    State &= ~GraphicMeshStates.UseTangents;
            }
        }

        public bool PoolMaterials
        {
            get
            {
                return (State & GraphicMeshStates.PoolMaterials) == GraphicMeshStates.PoolMaterials;
            }
            set
            {
                if (value)
                    State |= GraphicMeshStates.PoolMaterials;
                else
                    State &= ~GraphicMeshStates.PoolMaterials;
            }
        }

        public GraphicMesh()
        {
            CMesh = new Mesh();
            if (Property == null)
            {
                Property = new MaterialPropertyBlock();
                _ColorId = Shader.PropertyToID("_Color");
                _CutoffId = Shader.PropertyToID("_Cutoff");
                _FadeColorId = Shader.PropertyToID("_FadeColor");
            }
            Clear();
        }

        public override void Delete()
        {
            HasProperties = null;
            Materials = null;
            CMesh = null;
        }

        public void UpdateMesh(IMeshData Data)
        {
            if(PoolMaterials && Materials != null && Materials != Data.Materials)
                PoolManager.UPool.CloseMaterialArray(Materials);

            Enabled = true;
            Materials = (Data as PropertiesMeshDatas).CMaterials;
            HasProperties = (Data as PropertiesMeshDatas).Properties;

            CMesh.Clear(false);
            CMesh.vertices = Data.Vertices;
            CMesh.subMeshCount = Data.Indices.Length;
            for (int i = 0; i < CMesh.subMeshCount; ++i)
            {
                if (Topology == MeshTopology.Triangles)
                    CMesh.SetTriangles(Data.Indices[i], i);
                else
                    CMesh.SetIndices(Data.Indices[i], Topology, i);
            }

            CMesh.uv = Data.UVs;
            CMesh.normals = Data.Normals;
            CMesh.colors = Data.Colors;

            if (UseTangents)
                CMesh.tangents = Data.Tangents;

        }

        public virtual bool Draw(Vector3 Position)
        {
            if (!Enabled)
                return false;

            if (FadeStart > 0)
            {
                float Alpha = (ThreadManager.UnityTime - FadeStart) / FadeTime;
                if (Alpha < 0 || Alpha > 1) FadeStart = 0;


                Alpha = Mathf.Clamp01(Alpha);
                Material Mat;
                Color Col;
                byte Properties = 0;

                for (int i = Materials.Length-1; i >= 0; --i)
                {
                    Mat = Materials[i];
                    Properties = HasProperties[i];
                    Property.Clear();

                    if (Properties == 1)
                    {
                        Property.SetFloat(FadeName, Alpha);
                    }
                    else if (Properties == 2)
                    {
                        Property.SetFloat(_CutoffId, Mathf.Clamp(Alpha, Mat.GetFloat(_CutoffId), 1f));
                    }
                    else if (Properties == 3)
                    {
                        Col = Mat.GetColor(_FadeColorId);
                        Col.a = 1f-Alpha;
                        Property.SetColor(_FadeColorId, Col);
                    }
                    else if (Properties == 4)
                    {
                        Col = Mat.GetColor(_ColorId);
                        Col.a = 1f-Alpha;
                        Property.SetColor(_ColorId, Col);
                    }

                    Graphics.DrawMesh(CMesh, Position, Identity, Mat, 0, null, i, Property, CastShadow, ReceiveShadow);
                }
            }
            else
            {
                for (int i = Materials.Length-1; i >= 0; --i)
                    Graphics.DrawMesh(CMesh, Position, Identity, Materials[i], 0, null, i, null, CastShadow, ReceiveShadow);
            }
            return true;
        }

        public override void Clear()
        {
            if (PoolMaterials && Materials != null)
                PoolManager.UPool.CloseMaterialArray(Materials);

            State = GraphicMeshStates.None;
            Materials = null;
            Topology = MeshTopology.Triangles;
            Tag = null;
        }
    };

    public class GraphicLODMesh : GraphicMesh
    {
        [Serializable]
        public struct LODPosition
        {
            public int Id;
            public Vector3 Position;
            public Quaternion Rotation;
            public float Scale;
        };

        public struct MeshArray
        {
            public MeshArray(int Size)
            {
                Vertices = new Vector3[Size];
                Uvs = null;
                Normals = null;
                Tangents = null;
                Colors = null;
                Indices = null;
            }

            public Vector3[] Vertices;
            public Vector2[] Uvs;
            public Vector3[] Normals;
            public Vector4[] Tangents;
            public Color[] Colors;
            public int[][] Indices;
        }

        public Mesh LODMesh;
        public FList<LODPosition> Positions = new FList<LODPosition>(1);
        public bool Dirty = false;

        public int OriginalVerticesCount;
        public int MaterialCount;
        public bool UseNormal = false;
        public bool UseUvs = false;
        public bool UseColors = false;
        public bool InstantUpdate = false;

        private Vector3[] OriginalVertices;
        private Vector2[] OriginalUvs;
        private Vector3[] OriginalNormals;
        private Vector4[] OriginalTangents;
        private Color[] OriginalColors;
        private int[][] OriginalIndices;
        private int[] OriginalIndicesCount;

        private Queue<MeshArray> TempMesh = new Queue<MeshArray>();

        private int[] CurrentIndice;
        private int[] CurrentVertices;
        private float NextUpdate = 0;
        public int Revision = 0;

        public int UniqueId = 0;

        public int AddPosition(LODPosition P)
        {
            if (UniqueId > int.MaxValue - 10)
                UniqueId = 0;

            NextUpdate = ThreadManager.UnityTime + 2f;

            P.Id = ++UniqueId;
            Positions.Add(P);
            Enabled = true;
            Dirty = true;
            return P.Id;
        }

        public void RemovePosition(int Id)
        {
            if (Id < 0)
                return;

            for (int i = Positions.Count - 1; i >= 0; --i)
            {
                if (Positions.array[i].Id == Id)
                {
                    NextUpdate = ThreadManager.UnityTime + 2f;
                    Dirty = true;
                    Positions.RemoveAt(i);
                    if (Positions.Count == 0) Enabled = false;
                    return;
                }
            }
        }

        public override void Clear()
        {
            TempMesh.Clear();
            Enabled = false;
            Dirty = false;
            Positions.Clear();
            base.Clear();
        }

        public void SetMesh(Mesh NewMesh, Material[] NewMaterials)
        {
            if (NewMesh == null)
                Enabled = false;

            this.Materials = NewMaterials;

            if (NewMesh == LODMesh)
                return;

            CMesh.Clear();
            this.LODMesh = NewMesh;
            this.OriginalVertices = NewMesh.vertices;
            this.OriginalUvs = NewMesh.uv;
            this.OriginalNormals = NewMesh.normals;
            this.OriginalTangents = NewMesh.tangents;
            this.OriginalColors = NewMesh.colors;
            this.OriginalIndices = new int[NewMesh.subMeshCount][];
            this.OriginalIndicesCount = new int[NewMesh.subMeshCount];
            this.OriginalVerticesCount = OriginalVertices.Length;
            this.UseColors = OriginalColors != null && OriginalColors.Length != 0;

            for (int i = 0; i < NewMesh.subMeshCount; ++i)
            {
                OriginalIndices[i] = NewMesh.GetIndices(i);
                OriginalIndicesCount[i] = OriginalIndices[i].Length;
            }

            UseNormal = OriginalNormals != null && OriginalNormals.Length == OriginalVertices.Length;
            UseTangents = OriginalTangents != null && OriginalTangents.Length == OriginalVertices.Length;
            UseUvs = OriginalUvs != null && OriginalUvs.Length == OriginalVertices.Length;
            MaterialCount = Materials.Length;
        }

        public override bool Draw(Vector3 Position)
        {
            if (!Enabled || Materials == null || CMesh == null)
                return false;

            if (TempMesh.Count != 0)
            {
                MeshArray MArray = TempMesh.Dequeue();
                CMesh.Clear(false);

                if (MArray.Vertices != null)
                {
                    Enabled = true;
                    CMesh.vertices = MArray.Vertices;
                    CMesh.subMeshCount = Materials.Length;
                    for (int i = 0; i < Materials.Length; ++i)
                        CMesh.SetIndices(MArray.Indices[i], LODMesh.GetTopology(i), i);

                    if (UseUvs) CMesh.uv = MArray.Uvs;
                    if (UseTangents) CMesh.tangents = MArray.Tangents;
                    if (UseNormal) CMesh.normals = MArray.Normals;
                    if (UseColors) CMesh.colors = MArray.Colors;
                }
                else
                    Enabled = false;
            }

            return base.Draw(Position);
        }

        public bool UpdateProceduralMesh()
        {
            if ((!InstantUpdate && NextUpdate > ThreadManager.UnityTime) || OriginalVertices == null)
                return false;

            NextUpdate = ThreadManager.UnityTime + 4f;
            lock (Positions.array)
            {
                if (!Dirty)
                    return true;

                Dirty = false;
                ++Revision;

                int CurrentVertice = 0;
                int i = 0;
                int VerticeCount = OriginalVertices.Length * Positions.Count;
                //Debug.Log("Update :" + Name + ",Vertices=" + VerticeCount + ",Count="+Positions.Count);

                if (VerticeCount == 0)
                {
                    TempMesh.Enqueue(new MeshArray());
                    return false;
                }

                MeshArray MArray = new MeshArray(VerticeCount);

                if (UseUvs)
                {
                    if (MArray.Uvs == null || MArray.Uvs.Length != VerticeCount)
                    MArray.Uvs = new Vector2[VerticeCount];
                }

                if (UseNormal)
                {
                    if (MArray.Normals == null || MArray.Normals.Length != VerticeCount)
                    MArray.Normals = new Vector3[VerticeCount];
                }

                if (UseTangents)
                {
                    if (MArray.Tangents == null || MArray.Tangents.Length != VerticeCount)
                    MArray.Tangents = new Vector4[VerticeCount];
                }

                if (UseColors)
                {
                    if (MArray.Colors == null || MArray.Colors.Length != VerticeCount)
                        MArray.Colors = new Color[VerticeCount];
                }

                MArray.Indices = new int[MaterialCount][];

                if (CurrentIndice == null || CurrentIndice.Length != MaterialCount)
                {
                    CurrentIndice = new int[MaterialCount];
                    CurrentVertices = new int[MaterialCount];
                }
                else
                {
                    for (i = 0; i < MaterialCount; ++i)
                    {
                        CurrentIndice[i] = CurrentVertices[i] = 0;
                        MArray.Indices[i] = null;
                    }
                }

                int j;

                for (i = 0; i < MaterialCount; ++i)
                    MArray.Indices[i] = new int[OriginalIndicesCount[i] * Positions.Count];

                LODPosition Position;
                for (int b = 0; b < Positions.Count; ++b)
                {
                    Position = Positions.array[b];

                    if(UseNormal && UseTangents)
                    {
                        for (i = OriginalVerticesCount-1; i >= 0; --i)
                        {
                            MArray.Vertices[CurrentVertice + i] = Position.Position + (Position.Rotation * OriginalVertices[i]) * Position.Scale;
                            MArray.Normals[CurrentVertice + i] = (Position.Rotation * OriginalNormals[i]);
                            MArray.Tangents[CurrentVertice + i] = (Position.Rotation * OriginalTangents[i]);
                        }
                    }
                    else if (UseNormal)
                    {
                        for (i = OriginalVerticesCount - 1; i >= 0; --i)
                        {
                            MArray.Vertices[CurrentVertice + i] = Position.Position + (Position.Rotation * OriginalVertices[i]) * Position.Scale;
                            MArray.Normals[CurrentVertice + i] = (Position.Rotation * OriginalNormals[i]);
                        }
                    }
                    else
                    {
                        for (i = OriginalVerticesCount - 1; i >= 0; --i)
                        {
                            MArray.Vertices[CurrentVertice + i] = Position.Position + (Position.Rotation * OriginalVertices[i]) * Position.Scale;
                        }
                    }

                    if (UseUvs)
                        Array.Copy(OriginalUvs, 0, MArray.Uvs, CurrentVertice, OriginalVerticesCount);

                    if (UseColors)
                        Array.Copy(OriginalColors, 0, MArray.Colors, CurrentVertice, OriginalVerticesCount);

                    for (i = 0; i < MaterialCount; ++i)
                    {
                        for (j = 0; j < OriginalIndicesCount[i]; ++j)
                            MArray.Indices[i][j + CurrentIndice[i]] = OriginalIndices[i][j] + (CurrentVertices[i] * OriginalVerticesCount);

                        CurrentIndice[i] += OriginalIndicesCount[i];
                        CurrentVertices[i]++;
                    }

                    CurrentVertice += OriginalVerticesCount;
                }

                TempMesh.Enqueue(MArray);
            }

            return true;
        }
    };
}
