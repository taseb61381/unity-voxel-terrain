﻿using System;
using System.Collections.Generic;
using TerrainEngine;
using UnityEngine;

public class ITerrainNetworkInterface : MonoBehaviour
{
    public List<ChunkGenerateInfo> FlushChunks = new List<ChunkGenerateInfo>();
    public IAction CurrentAction;

    public virtual void Init(TerrainManager Manager)
    {

    }

    public virtual void OnManagerInited(TerrainManager Manager)
    {

    }

    public void Update()
    {
        if (OperatorsContainer.IsDirty && (CurrentAction == null || CurrentAction.IsDone()))
        {
            CurrentAction = Flush(ActionProcessor.Unity, null, false, false);
        }
    }

    public void SetDirty()
    {
        OperatorsContainer.IsDirty = true;  
    }

    public virtual TerrainWorldServer GetServer()
    {
        TerrainManager.Instance.InitGenerators();

        TerrainWorldServer Server = new TerrainWorldServer();
        Server.Start(ActionProcessor.Instance.Manager, TerrainManager.Instance.Generator);

        return Server;
    }

    /// <summary>
    /// Thread Function that handle ActionProcessor System for update Terrain Server
    /// </summary>
    public virtual bool UpdateServerThread(ActionThread Thread)
    {
        TerrainManager.Instance.Server.UpdateThread(Thread);
        return false;
    }

    #region VoxelModification

    public TerrainOperatorContainer OperatorsContainer = new TerrainOperatorContainer();

    public virtual void AddOperator(ITerrainOperator Operator)
    {
        OperatorsContainer.AddOperator(Operator);
    }

    public virtual bool Flush(ActionThread Thread, TerrainObject Obj, List<ChunkGenerateInfo> ModifiedChunks, out TerrainChunkGenerateGroupAction Action)
    {
        Action = null;
        if (OperatorsContainer.Count == 0)
        {
            return false;
        }

        //Debug.Log("Flush:" + OperatorsContainer.Count);

        int ProcessorCount = TerrainManager.Instance.Processors.Count;
        TerrainChunkGenerateGroupAction GenAction = null;

        OperatorsContainer.Flush(TerrainManager.Instance.Container, (ITerrainOperator Operator, ref VoxelFlush Flush) => // Can Modify Voxel. Return false if it's not possible (doesn"t have item, voxel type is undestructible, etc...)
                    {
                        bool CanModify = true;

                        for (int i = 0; i < ProcessorCount; ++i)
                        {
                            if (!TerrainManager.Instance.Processors[i].CanModifyVoxel(Obj, ref Flush))
                            {
                                CanModify = false;
                                break;
                            }
                        }

                        if (Operator.ModType == VoxelModificationType.SEND_EVENTS)
                        {
                            CanModify = false;
                            for (int i = 0; i < ProcessorCount; ++i)
                                TerrainManager.Instance.Processors[i].OnFlushVoxels(Thread, Obj, ref Flush, null);
                        }

                        return CanModify;
                    },
                    (ITerrainOperator Operator, ref VoxelFlush Flush) => // Voxel has been modified. Now send events to all Processors and add Chunks to rebuild list
                    {
                        if (GenAction == null)
                            GenAction = new TerrainChunkGenerateGroupAction();

                        for (int i = 0; i < ProcessorCount; ++i)
                            TerrainManager.Instance.Processors[i].OnFlushVoxels(Thread, Obj, ref Flush, GenAction);

                        AddChunkVoxelToGenerate(Thread, Flush.Block as TerrainBlock, Flush.ChunkId, Flush.x, Flush.y, Flush.z, ModifiedChunks);
                        OnModifyVoxel(Operator, ref Flush);
                        return true;
                    },
                    OnOperatorFlush
                    );

        Action = GenAction;

        for (int i = 0; i < ProcessorCount; ++i)
            TerrainManager.Instance.Processors[i].OnVoxelFlushDone(Thread, Action);

        for (int i = 0; i < ModifiedChunks.Count; ++i)
            ModifiedChunks[i].ChunkRef.SetState(ChunkStates.Flush, false);

        OnFlushDone();

        return ModifiedChunks.Count > 0;
    }

    public virtual bool OnModifyVoxel(ITerrainOperator Operator, ref VoxelFlush Flush)
    {
        return true;
    }

    #endregion

    public virtual void Flush()
    {
        Flush(ActionProcessor.Unity, null, true, false);
    }

    public virtual IAction Flush(ActionThread Thread, TerrainObject Obj, bool InstantGeneration = true, bool InstantBuild = false)
    {
        lock (FlushChunks)
        {
            FlushChunks.Clear();
            TerrainChunkGenerateGroupAction Action = null;

            if (!Flush(Thread, Obj, FlushChunks, out Action))
            {
                return null;
            }

            Action.Init(FlushChunks, true);

            if (InstantGeneration)
            {
                return Action.ExecuteAction(ActionProcessor.Instance.UnityThread, InstantBuild);
            }
            else
            {
                ActionProcessor.AddFast(Action);
                return Action;
            }
        }
    }

    static public void AddChunkVoxelToGenerate(ActionThread Thread, TerrainBlock Block, int ChunkId, int x, int y, int z, List<ChunkGenerateInfo> ModifiedChunks)
    {
        int CPB = Block.Information.ChunkPerBlock;
        int VPC = Block.Information.VoxelPerChunk;

        int LocalZ = ChunkId % CPB;
        int LocalY = (ChunkId / CPB) % CPB;
        int LocalX = ChunkId / (CPB * CPB);

        int C = x - LocalX * VPC;
        int LowerX = 0, LowerY = 0, LowerZ = 0;

        if (C <= 1)
            LowerX = -1;
        else if (C >= TerrainManager.Instance.Informations.VoxelPerChunk - 2)
            LowerX = 1;

        C = y - LocalY * VPC;
        if (C <= 1)
            LowerY = -1;
        else if (C >= TerrainManager.Instance.Informations.VoxelPerChunk - 2)
            LowerY = 1;

        C = z - LocalZ * VPC;
        if (C <= 1)
            LowerZ = -1;
        else if (C >= TerrainManager.Instance.Informations.VoxelPerChunk - 2)
            LowerZ = 1;

        TerrainChunkRef ChunkRef = new TerrainChunkRef(Block.BlockId, ChunkId);
        TerrainBlock AroundBlock = null;

        CheckAndAddToList(Thread, Block, ModifiedChunks, Block, ref ChunkRef);

        if (Block.GetAroundChunk(ref AroundBlock, LocalX + 1 + LowerX, LocalY + 1, LocalZ + 1, ref ChunkRef))
            CheckAndAddToList(Thread, Block, ModifiedChunks, AroundBlock, ref ChunkRef);

        if (Block.GetAroundChunk(ref AroundBlock, LocalX + 1, LocalY + 1 + LowerY, LocalZ + 1, ref ChunkRef))
            CheckAndAddToList(Thread, Block, ModifiedChunks, AroundBlock, ref ChunkRef);

        if (Block.GetAroundChunk(ref AroundBlock, LocalX + 1, LocalY + 1, LocalZ + 1 + LowerZ, ref ChunkRef))
            CheckAndAddToList(Thread, Block, ModifiedChunks, AroundBlock, ref ChunkRef);

        if (Block.GetAroundChunk(ref AroundBlock, LocalX + 1, LocalY + 1 + LowerY, LocalZ + 1 + LowerZ, ref ChunkRef))
            CheckAndAddToList(Thread, Block, ModifiedChunks, AroundBlock, ref ChunkRef);

        if (Block.GetAroundChunk(ref AroundBlock, LocalX + 1 + LowerX, LocalY + 1 + LowerY, LocalZ + 1 + LowerZ, ref ChunkRef))
            CheckAndAddToList(Thread, Block, ModifiedChunks, AroundBlock, ref ChunkRef);
    }

    static public void CheckAndAddToList(ActionThread Thread, TerrainBlock Block, List<ChunkGenerateInfo> ModifiedChunks, TerrainBlock AroundBlock, ref TerrainChunkRef Around)
    {
        Around.ChunkId = AroundBlock.GetChunkParent(Around.ChunkId);
        if (!AroundBlock.HasChunkStateOrSet(Around.ChunkId, ChunkStates.Flush))
            return;

        for (int i = 0; i < TerrainManager.Instance.Processors.Count; ++i)
            TerrainManager.Instance.Processors[i].OnFlushChunk(Thread, Around);

        ModifiedChunks.Add(new ChunkGenerateInfo(Around, null));
    }

    public virtual void OnOperatorFlush(ITerrainOperator Operator)
    {

    }

    // Flush complete , send network data to server.
    public virtual void OnFlushDone()
    {

    }

    /// <summary>
    /// Function to override to share scripts state over clients. Example 
    /// </summary>
    /// <param name="Script"></param>
    public virtual void OnCustomScriptActive(IGameObjectSpawnScript Script, bool Active)
    {

    }
}