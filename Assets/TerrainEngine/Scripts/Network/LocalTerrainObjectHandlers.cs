﻿using System.Collections.Generic;

namespace TerrainEngine
{
    public class LocalTerrainObjectHandlers : ITerrainObjectHandlers
    {
        public override void Init(TerrainObjectServer Owner)
        {
        }

        public override void SendGeneratingBlock(VectorI3 Position, float Pct)
        {
            ProgressSystem.Instance.SetProgress("Build Terrain : " + Position.ToString(), Pct, ThreadManager.UnityTime);
        }

        public override void SendGeneratingAbord(VectorI3 Position)
        {
            ProgressSystem.Instance.SetProgress("Build Terrain : " + Position.ToString(), 100f, ThreadManager.UnityTime);
        }

        public override void CreateBlock(TerrainBlockServer Block)
        {
            TerrainManager.Instance.Builder.LoadBlock(Block.Voxels);

            //TerrainBuilderAction.CreateTerrain(Block.LocalPosition, Block.Voxels);
        }

        public override void SendAllowedBlocks(List<VectorI3> Positions)
        {
            TerrainManager.Instance.Builder.LoadBlocks(Positions);

            //TerrainBuilderAction.SetAllowedTerrains(Positions);
        }

        #region Distant Objects

        public override void SendMeTo(TerrainObjectServer Object)
        {
        }

        public override void OnAddObjectInRange(TerrainObjectServer Object)
        {
        }

        public override void OnRemoveObjectInRange(TerrainObjectServer Object)
        {
        }

        #endregion
    }
}