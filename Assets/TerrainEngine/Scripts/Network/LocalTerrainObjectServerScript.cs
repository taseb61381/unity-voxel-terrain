﻿using TerrainEngine;
using UnityEngine;


/// <summary>
/// This class will update positions to server and handle blocks around update
/// </summary>
[AddComponentMenu("TerrainEngine/TerrainObject/Local Object (Player)")]
[ExecuteInEditMode]
public class LocalTerrainObjectServerScript : MonoBehaviour
{
    public TerrainPlayerServer ServerObject;
    private bool IsInited = false;
    public Transform CTransform;
    public Vector3 FixedPositions;
    public bool Enabled = true;
    public KeyCode DisableGenerationKey = KeyCode.P;

    public float DistanceFromGround = 0;
    public int BlockPositionY;

    private float UpdateTime = 0;

    void Start()
    {
        if (CTransform == null)
        {
            Debug.LogError(name + " : No Tranform on 'CTransform' variable. No object to load terrain around. Please link and object (player)");
        }
    }

    public void Update()
    {
        if (Application.isPlaying)
        {
            if (TerrainManager.Instance == null || TerrainManager.Instance.State < TerrainManager.LoadingStates.STARTING)
                return;

            if (TerrainManager.Instance.Server == null)
            {
                Debug.LogError("No server found. Remove this script if you're not using an TerrainEngine Server.");
                enabled = false;
                return;
            }

            if (!IsInited)
            {
                IsInited = true;

                if (CTransform == null)
                    CTransform = transform;

                ServerObject = new TerrainPlayerServer();
                TerrainManager.Instance.Server.AddObject(ServerObject);
                ServerObject.SetHandler(new LocalTerrainObjectHandlers());
            }

            if (Input.GetKeyDown(DisableGenerationKey))
                Enabled = !Enabled;

            if (UpdateTime < ThreadManager.UnityTime && Enabled)
            {
                UpdateTime = ThreadManager.UnityTime + 1f;

                // Send the real position of this object to the server
                ServerObject.SetPosition(CTransform.position.x,CTransform.position.y,CTransform.position.z);

                // Send the position where to load blocks around this object
                ServerObject.SetLoadPosition(FixedPositions.x == 0 ? CTransform.position.x : FixedPositions.x,
                    FixedPositions.y == 0 ? CTransform.position.y : FixedPositions.y,
                    FixedPositions.z == 0 ? CTransform.position.z : FixedPositions.z);
            }
        }
        else
        {
            TerrainManager Manager = FindObjectOfType<TerrainManager>();
            if(Manager != null)
            {
                if (Manager.Informations == null)
                    Manager.Informations = new TerrainInformation();

                Manager.Informations.Init();
                BlockPositionY = (int)(FixedPositions.y / Manager.Informations.BlockSizeY);
                DistanceFromGround = Manager.Informations.WorldGroundLevel - (BlockPositionY * Manager.Informations.BlockSizeY);
            }
        }
    }

    void OnDisable()
    {
        if (ServerObject == null || TerrainManager.Instance == null || TerrainManager.Instance.Server == null)
            return;

        ServerObject.RemoveFromWorld();
        ServerObject = null;
    }
}