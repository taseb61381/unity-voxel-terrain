﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TerrainEngine;
using UnityEngine;


/// <summary>
/// This class Manage all camera positions. Used by all systems needing a position to update LOD or other events.
/// Override this class and place the component on your TerrainManager GameObject to use custom positions. (Usefull when used on a Dedicated server that don't use any camera)
/// </summary>
public class ITerrainCameraHandler : MonoBehaviour
{
    public static ITerrainCameraHandler Instance;

    public void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Manage position movement and distance check.
    /// </summary>
    public class PositionMovementManager
    {
        public struct PositionDistanceCheck
        {
            public int Index;
            public Vector3 LastPosition;

            public bool IsValid(Vector3 Position, float MaxDistance)
            {
                if (Vector3.Distance(LastPosition, Position) > MaxDistance)
                {
                    LastPosition = Position;
                    return true;
                }

                return false;
            }
        }

        public SList<PositionDistanceCheck> Positions;

        /// <summary>
        /// Return true if position has changed.
        /// </summary>
        /// <returns></returns>
        public bool HasPositionChanged(int Index, Vector3 Position, float MaxDistance)
        {
            for (int i = Positions.Count-1; i >= 0; --i)
            {
                if (Positions.array[i].Index == Index)
                {
                    if (Vector3.Distance(Positions.array[i].LastPosition, Position) > MaxDistance)
                    {
                        Positions.array[i].LastPosition = Position;
                        return true;
                    }
                    else
                        return false;
                }
            }

            if (Positions.Count == 0)
            {
                Positions = new SList<PositionDistanceCheck>(1);
            }
            PositionDistanceCheck Check = new PositionDistanceCheck();
            Check.Index = Index;
            Check.LastPosition = Position;
            Positions.Add(Check);
            return true;
        }
    }

    /// <summary>
    /// Called by TerrainManager when all systems are inited
    /// </summary>
    public virtual void DoUpdate(TerrainManager Manager)
    {
        UpdateCurrentCamera(Manager);

        if (MainCamera != null)
        {
            MainCameraPosition = MainCamera.transform.position;
            MainCamera2DPosition.x = MainCameraPosition.x;
            MainCamera2DPosition.y = MainCameraPosition.z;

            if (CurrentUID == -1)
                CurrentUID = Container.AddTransform(MainCameraPosition, Quaternion.identity);

            Container.UpdateTransform(CurrentUID, MainCameraPosition, Quaternion.identity);
        }
        else
        {
            Container.RemoveTransform(CurrentUID);
            CurrentUID = -1;
        }
    }

    /// <summary>
    /// Return the current Camera Transform
    /// </summary>
    public virtual Camera GetMainCamera()
    {
        return MainCamera;
    }

    /// <summary>
    /// Return the current Camera TerrainObject script
    /// </summary>
    public virtual TerrainObject GetMainCameraTObject()
    {
        return CamObject;
    }

    /// <summary>
    /// Override and return false if build not use a camera (Server side ?)
    /// This will disable meshes and avoid LOD check on systems (not terrain)
    /// </summary>
    /// <returns></returns>
    public virtual bool IsUsingCamera()
    {
        return true;
    }

    #region V11

    private int CurrentUID = -1;
    public TransformsContainer Container;

    [HideInInspector]
    public bool HasCamera = false;

    public Camera MainCamera;
    public Camera MainForcedCamera; // If != null this camera will be set as main camera and TerrainProcessor needing camera position will use this Camera (Grass, Details , GameObjects,etc...)

    [HideInInspector]
    public Vector3 MainCameraPosition;

    [HideInInspector]
    public Vector2 MainCamera2DPosition;

    [HideInInspector]
    public TerrainObject CamObject;
    
    public virtual void Init(TerrainManager Manager)
    {
        Container = new TransformsContainer();
    }

    public virtual void UpdateCurrentCamera(TerrainManager Manager)
    {
        if (MainForcedCamera == null)
        {
            if (MainCamera == null || !MainCamera.isActiveAndEnabled)
            {
                MainCamera = null;
                Camera New = null;
                Camera Existing = null;
                TerrainObject NewObject = null;

                for (int i = 0; i < Camera.allCameras.Length; ++i)
                {
                    Existing = Camera.allCameras[i];
                    if (Existing.enabled && Existing.gameObject.activeInHierarchy && (NewObject = Existing.GetComponent<TerrainObject>()) != null)
                    {
                        New = Existing;
                        break;
                    }
                }

                if (New == null)
                {
                    HasCamera = false;
                    CamObject = null;
                    MainCamera = null;
                }
                else if (MainCamera != New && NewObject != null)
                {
                    MainCamera = New;
                    CamObject = NewObject;
                    HasCamera = true;
                }

                if (Manager.isInited && Manager.isLoaded)
                {
                    if (New == null)
                    {
                        Debug.LogError("No main camera. Please set a main camera to TerrainManager");
                    }

                    if (NewObject == null)
                    {
                        Debug.LogError("No main camera with TerrainObject. Please set a main camera to TerrainManager and add TerrainObject.cs script to it.");
                    }
                }
            }
        }
        else
        {
            if (MainCamera != MainForcedCamera)
            {
                MainCamera = MainForcedCamera;
                CamObject = MainForcedCamera.GetComponent<TerrainObject>();
                HasCamera = true;
            }
            if (CamObject == null)
                CamObject = MainForcedCamera.GetComponent<TerrainObject>();
            HasCamera = true;
        }
    }

    #endregion
}