﻿using System;

using UnityEngine;

/// <summary>
/// Place this script on your TerrainManager gameobject when no camera are used on build (Dedicated server)
/// </summary>
public class TerrainNoCameraHandler : ITerrainCameraHandler
{
    public override void Init(TerrainManager Manager)
    {
        base.Init(Manager);
    }

    public override bool IsUsingCamera()
    {
        return false;
    }

    public override void DoUpdate(TerrainManager Manager)
    {

    }
}
