﻿using System;
using System.IO;
using TerrainEngine;
using UnityEngine;

public class FileMgr : MonoBehaviour
{
    static public FileMgr Instance;

    public int CaptureFrameRate = 0;
    public string[] ToCreateFolders;
    public ActiveDisableContainer OnStart;
    private bool IsStarted = false;

    void Start()
    {
        Debug.Log("FileMgr : Starting...");
    }

    void Update()
    {
        if (!IsStarted)
        {
            IsStarted = true;
            GC.Collect();

            if (CaptureFrameRate != 0)
                Time.captureFramerate = CaptureFrameRate;

            //System.Runtime.GCSettings.LatencyMode = System.Runtime.GCLatencyMode.LowLatency;

            foreach (string Folder in ToCreateFolders)
                CheckFolders(Directory.GetCurrentDirectory() + "\\" + Folder);

            OnStart.Call();
        }
    }

    void OnEnable()
    {
        Instance = this;
    }

    void OnDisable()
    {
        Instance = null;
    }

    #region XML

    static public void CheckFolders(string Path)
    {
        if (ThreadManager.IsWebPlayer)
            return;

        if (!Directory.Exists(Path))
        {
            Directory.CreateDirectory(Path);
        }
    }

    static public bool ExistFile(string Path)
    {
        if (ThreadManager.IsWebPlayer)
            return false;

        return File.Exists(Path);
    }

    #endregion

    #region Prefabs

    public GameObject[] Prefabs;

    static public GameObject GetPrefab(string Name)
    {
        GameObject Obj = Array.Find(Instance.Prefabs, info => info.name == Name);
        return Obj;
    }

    #endregion
}
