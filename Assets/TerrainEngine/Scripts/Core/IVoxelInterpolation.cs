﻿
namespace TerrainEngine
{
    public class IVoxelArray : IPoolable
    {
        public int SizeX, SizeY, SizeZ,BorderSize,PaletteCount;
        public bool[] ActiveSubMesh;
        public int MaterialCount;
        public virtual void Init(int SizeX, int SizeY, int SizeZ, int BorderSize, int PaletteCount)
        {
            this.SizeX = SizeX;
            this.SizeY = SizeY;
            this.SizeZ = SizeZ;
            this.BorderSize = BorderSize;
            this.MaterialCount = 0;
            if (ActiveSubMesh == null || PaletteCount != this.PaletteCount)
            {
                this.PaletteCount = PaletteCount;
                ActiveSubMesh = new bool[PaletteCount];
            }
        }

        public void ActiveMaterial(int Type)
        {
            if (ActiveSubMesh[Type] == false)
            {
                ActiveSubMesh[Type] = true;
                ++MaterialCount;
            }
        }


        public override void Clear()
        {
            if (ActiveSubMesh != null)
            {
                MaterialCount = 0;
                for (int i = 0; i < ActiveSubMesh.Length; ++i)
                {
                    ActiveSubMesh[i] = false;
                }
            }
        }
    }
}
