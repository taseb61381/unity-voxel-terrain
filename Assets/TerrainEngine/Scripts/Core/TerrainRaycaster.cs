﻿using System;

using UnityEngine;

namespace TerrainEngine
{
    public struct VoxelRaycastHit
    {
        public TerrainBlock Block;
        public int x;
        public int y;
        public int z;
        public byte Type;
        public float Volume;
        public VoxelTypes VoxelType;

                public override string ToString()
        {
            return (Block != null ? Block.WorldPosition.ToString() : "") + "," + x + "," + y + "," + z;
        }
    };

    static public class TerrainRaycaster
    {
        // Static Values to speed up the raycast
        static public int VoxelCount;
        static public int VoxelPerChunk;

        static public float OcclusionVolume = 0.9f;

        static public bool Raycast(TerrainBlock Block, int x, int y, int z, Vector3 Direction, int MaxDepth, ref VoxelRaycastHit Hit, float MinVolume = 0.5f)
        {
            int stepX = Direction.x < 0 ? -1 : Direction.x > 0 ? 1 : 0;
            int stepY = Direction.y < 0 ? -1 : Direction.y > 0 ? 1 : 0;
            int stepZ = Direction.z < 0 ? -1 : Direction.z > 0 ? 1 : 0;

            float tMaxx = ((x + (stepX > 0 ? 1 : 0)) - x) / Direction.x;
            float tMaxy = ((y + (stepY > 0 ? 1 : 0)) - y) / Direction.y;
            float tMaxz = ((z + (stepZ > 0 ? 1 : 0)) - z) / Direction.z;

            if (Single.IsNaN(tMaxx)) tMaxx = Single.PositiveInfinity;
            if (Single.IsNaN(tMaxy)) tMaxy = Single.PositiveInfinity;
            if (Single.IsNaN(tMaxz)) tMaxz = Single.PositiveInfinity;

            float tDeltax = stepX / Direction.x;
            float tDeltay = stepY / Direction.y;
            float tDeltaz = stepZ / Direction.z;

            if (Single.IsNaN(tDeltax)) tDeltax = Single.PositiveInfinity;
            if (Single.IsNaN(tDeltay)) tDeltay = Single.PositiveInfinity;
            if (Single.IsNaN(tDeltaz)) tDeltaz = Single.PositiveInfinity;

            int ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

            for (int i = 0; i < MaxDepth; i++)
            {
                if (tMaxx < tMaxy && tMaxx < tMaxz)
                {
                    x += stepX;
                    tMaxx += tDeltax;

                    if (x < 0)
                    {
                        if ((Block = Block[0, 1, 1] as TerrainBlock) == null)
                            return false;
                        x = VoxelCount - 1;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
                    }
                    else if (x >= VoxelCount)
                    {
                        if ((Block = Block[2, 1, 1] as TerrainBlock) == null)
                            return false;
                        x = 0;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
                    }
                    else if (x % VoxelPerChunk == 0)
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
                }
                else if (tMaxy < tMaxz)
                {
                    y += stepY;
                    tMaxy += tDeltay;

                    if (y < 0)
                    {
                        if ((Block = Block[1, 0, 1] as TerrainBlock) == null)
                            return false;
                        y = VoxelCount - 1;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
                    }
                    else if (y >= VoxelCount)
                    {
                        if ((Block = Block[1, 2, 1] as TerrainBlock) == null)
                            return false;
                        y = 0;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
                    }
                    else if (y % VoxelPerChunk == 0)
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
                }
                else
                {
                    z += stepZ;
                    tMaxz += tDeltaz;

                    if (z < 0)
                    {
                        if ((Block = Block[1, 1, 0] as TerrainBlock) == null)
                            return false;
                        z = VoxelCount - 1;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
                    }
                    else if (z >= VoxelCount)
                    {
                        if ((Block = Block[1, 1, 2] as TerrainBlock) == null)
                            return false;
                        z = 0;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
                    }
                    else if (z % VoxelPerChunk == 0)
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
                }


                Block.Voxels.GetTypeAndVolume(x, y, z, ChunkId, ref Hit.Type, ref Hit.Volume);

                if (Hit.Type != 0 && Hit.Volume >= MinVolume)
                {
                    if ((Hit.VoxelType = TerrainManager.Instance.Palette.GetType(Hit.Type)) != VoxelTypes.FLUID)
                    {
                        Hit.Block = Block;
                        Hit.x = x;
                        Hit.y = y;
                        Hit.z = z;
                        return true;
                    }
                }
            }

            return false;
        }

        static public bool RaycastOcclusion(TerrainBlock Block, int x, int y, int z, Vector3 Direction, int MaxDepth)
        {
            Block = Block.GetAroundBlock(ref x, ref y, ref z) as TerrainBlock;
            if (Block == null)
                return true;

            int stepX = Direction.x < 0 ? -1 : Direction.x > 0 ? 1 : 0;
            int stepY = Direction.y < 0 ? -1 : Direction.y > 0 ? 1 : 0;
            int stepZ = Direction.z < 0 ? -1 : Direction.z > 0 ? 1 : 0;

            float tMaxx = ((x + (stepX > 0 ? 1 : 0)) - x) / Direction.x;
            float tMaxy = ((y + (stepY > 0 ? 1 : 0)) - y) / Direction.y;
            float tMaxz = ((z + (stepZ > 0 ? 1 : 0)) - z) / Direction.z;

            if (Single.IsNaN(tMaxx)) tMaxx = Single.PositiveInfinity;
            if (Single.IsNaN(tMaxy)) tMaxy = Single.PositiveInfinity;
            if (Single.IsNaN(tMaxz)) tMaxz = Single.PositiveInfinity;

            float tDeltax = stepX / Direction.x;
            float tDeltay = stepY / Direction.y;
            float tDeltaz = stepZ / Direction.z;

            if (Single.IsNaN(tDeltax)) tDeltax = Single.PositiveInfinity;
            if (Single.IsNaN(tDeltay)) tDeltay = Single.PositiveInfinity;
            if (Single.IsNaN(tDeltaz)) tDeltaz = Single.PositiveInfinity;

            int ChunkId = Block.GetChunkIdFromVoxel(x, y, z);
            if (Block.HasChunkState(ChunkId, ChunkStates.Occluded))
                return true;

            byte Type = 0;

            bool DoubleCheck = false;
            float Volume = 0;

            for (int i = 0; i < MaxDepth; i++)
            {
                if (tMaxx < tMaxy && tMaxx < tMaxz)
                {
                    x += stepX;
                    tMaxx += tDeltax;

                    if (x < 0)
                    {
                        if ((Block = Block.LeftBlock as TerrainBlock) == null)
                            return false;
                        x = VoxelCount - 1;

                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

                        if (Block.HasChunkState(ChunkId,ChunkStates.Occluded))
                            return true;
                    }
                    else if (x >= VoxelCount)
                    {
                        if ((Block = Block.RightBlock as TerrainBlock) == null)
                            return false;
                        x = 0;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

                        if (Block.HasChunkState(ChunkId, ChunkStates.Occluded))
                            return true;
                    }
                    else if (x % VoxelPerChunk == 0)
                    {
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

                        if (Block.HasChunkState(ChunkId, ChunkStates.Occluded))
                            return true;
                    }
                }
                else if (tMaxy < tMaxz)
                {
                    y += stepY;
                    tMaxy += tDeltay;

                    if (y < 0)
                    {
                        if ((Block = Block.BottomBlock as TerrainBlock) == null)
                            return false;
                        y = VoxelCount - 1;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

                        if (Block.HasChunkState(ChunkId, ChunkStates.Occluded))
                            return true;
                    }
                    else if (y >= VoxelCount)
                    {
                        if ((Block = Block.TopBlock as TerrainBlock) == null)
                            return false;
                        y = 0;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

                        if (Block.HasChunkState(ChunkId, ChunkStates.Occluded))
                            return true;
                    }
                    else if (y % VoxelPerChunk == 0)
                    {
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

                        if (Block.HasChunkState(ChunkId, ChunkStates.Occluded))
                            return true;
                    }
                }
                else
                {
                    z += stepZ;
                    tMaxz += tDeltaz;

                    if (z < 0)
                    {
                        if ((Block = Block.BackwardBlock as TerrainBlock) == null)
                            return false;
                        z = VoxelCount - 1;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

                        if (Block.HasChunkState(ChunkId, ChunkStates.Occluded))
                            return true;
                    }
                    else if (z >= VoxelCount)
                    {
                        if ((Block = Block.ForwardBlock as TerrainBlock) == null)
                            return false;
                        z = 0;
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

                        if (Block.HasChunkState(ChunkId, ChunkStates.Occluded))
                            return true;
                    }
                    else if (z % VoxelPerChunk == 0)
                    {
                        ChunkId = Block.GetChunkIdFromVoxel(x, y, z);

                        if (Block.HasChunkState(ChunkId, ChunkStates.Occluded))
                            return true;
                    }
                }

                if (!Block.HasState(TerrainBlockStates.CLOSED))
                {
                    Block.Voxels.GetTypeAndVolume(x, y, z, ref Type, ref Volume);
                    if (Type == 0 || Volume <= OcclusionVolume || TerrainManager.Instance.Palette.GenericPalette[Type].VoxelType != VoxelTypes.OPAQUE)
                        continue;

                    if (DoubleCheck)
                        return true;

                    DoubleCheck = true;
                }
            }

            return false;
        }
    }
}
