﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "TerrainEngine/NoiseDNSSLocal" {

    Properties {

		_Color("Main Color", Color) = (1,1,1,1)
		_ColorPower ("Color Power", Float) = 1.0
		_LightPower ("Light Power", Float) = 1.0
		_NormalPower ("Normal Power", Float) = 1.0
		_SpecularPower ("Specular Power", Float) = 1
		_CeilingPower ("Ceiling Power", Range (-2,2)) = 0
		_WallPower ("Wall Power", Range (0,2)) = 1
		_TransitionStartDistance("Transition Start Distance", Float) = 10
		_TransitionScaleLength("Transition Scale Length", Float) = 10
		_AOIntensity ("AO Intensity", Range(0, 2)) = 1.5		// intensity and power of the ambient occlusions
		_AOPower ("AO Power", Range(1, 10)) = 1.0

    	_FloorTexture ("Floor", 2D) = "white" {}
		_FloorNoiseTexture ("Floor Noise", 2D) = "white" {}
		_FloorNormal ("Floor Normal", 2D) = "white" {}
		_FloorScale("Floor Scale", Float) = .2
		_FloorNoiseScale("Floor Noise Scale", Float) = .2

    	_WallTexture ("Wall", 2D) = "white" {}
		_WallNoiseTexture ("Wall Noise", 2D) = "white" {}
		_WallNormal ("Wall Normal", 2D) = "white" {}
		_WallScale ("Wall Scale", Float) = .2
		_WallFarScale ("Wall Far Scale", Float) = .2
		_WallNoiseScale("Wall Noise Scale", Float) = .2

		_WallDetailScale("Wall Detail Scale", Float) = .2
		_WallDetailStartDistance("Wall Detail Start Distance", Float) = 100
		_WallDetailScaleLength("Wall Detail Scale Length", Float) = 10

		_NoiseScale ("Noise Scale", Float) = 1.0
    	_NoiseVal1 ("Noise Value 1", Float) = 125777.0
		_NoiseVal2 ("Noise Value 2", Float) = 233.0
		_ColorPowers("Color Powers", Vector) = (1,1,1,1)
		_NormalPowers("Normal Powers", Vector) = (1,1,1,1)

    }

    SubShader {
    
		Tags { "RenderType" = "Opaque" }

		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf SimpleLambert vertex:vert
		#include "ShadersCommon.cginc"

		struct Input 
		{
			float3 worldNormal;
			float3 localPos;
			float3 viewDir;
			float dist;
			float distdetail;
		};

		struct CustomSurfaceOutput
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Alpha;
			half3 BumpNormal;
			half Specular;
		};
		
		sampler2D _MainTex;

		sampler2D _FloorTexture;
		sampler2D _FloorNormal;
		sampler2D _FloorNoiseTexture;
		float _FloorScale;
		float _FloorNoiseScale;

		float _AOIntensity;
		float _AOPower;

		sampler2D _WallTexture;
		sampler2D _WallNormal;
		sampler2D _WallNoiseTexture;
		float _WallScale;

		float _WallNoiseScale;
		float _WallFarScale;

		float4 _Color;
		float _ColorPower;
		float _LightPower;
		float _NormalPower;
		float _CeilingPower;
		float _WallPower;
		float _SpecularPower;

		float _TransitionStartDistance;
		float _TransitionScaleLength;

		float _WallDetailStartDistance;
		float _WallDetailScaleLength;
		float _WallDetailScale;
		float4 _ColorPowers;
		float4 _NormalPowers;

		half4 LightingSimpleLambert (CustomSurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		{
			half NdotL = dot(normalize(s.BumpNormal), normalize(lightDir));
			if(NdotL < 0.1)
				NdotL = 0.1;

			half3 h = normalize (lightDir + normalize(viewDir));
			
			half nh = max (0, dot (s.BumpNormal, h));
			half spec = smoothstep(0, 1.0, pow(nh, 32.0 * s.Specular)) * _SpecularPower;
			
			half4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * NdotL + _LightColor0.rgb * spec) * atten * _LightPower * 2;
			c.a = s.Alpha;

			return c;
		}

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input,o);
			float3 worldPos=mul (unity_ObjectToWorld, v.vertex).xyz;
			float dist=distance(worldPos, _WorldSpaceCameraPos);
			o.dist=saturate((dist - _TransitionStartDistance) / _TransitionScaleLength);
			o.distdetail=saturate((dist - _WallDetailStartDistance) / _WallDetailScaleLength);
			o.localPos = v.vertex.xyz;
			o.worldNormal = v.normal.xyz;
		}

		void surf (Input IN, inout CustomSurfaceOutput o)
		{
			float3 p = IN.localPos;
			float3 wn = IN.worldNormal;
			float3 n = normalize(abs(wn));
			float noise = 0;
			
			n = (n - 0.2) * 7.0;
			n.y = n.y *_WallPower;
			n = max(n, 0.0);
			noise = n.x + n.y + n.z;
			n /= float3(noise,noise,noise);
			noise = fractalNoise(p * _NoiseScale);

			// Texture
			float4 cy = (wn.y < _CeilingPower) ?  
			tex2D(_WallTexture, frac(p.zx * _WallScale)) * (n.y) :  lerp(tex2D(_FloorTexture, frac(p.zx * _FloorScale)), 
			tex2D(_FloorNoiseTexture, frac(p.zx * _FloorNoiseScale)), noise) * n.y  * _ColorPowers.y;

			
			float det_lerp=saturate(1-IN.dist);

			float3 cz = lerp(tex2D(_WallTexture, frac(p.xy * _WallScale)), tex2D(_WallNoiseTexture, frac(p.xy * _WallNoiseScale)), noise) * n.z * _ColorPowers.z;
			float3 cx = lerp(tex2D(_WallTexture, frac(p.yz * _WallScale)), tex2D(_WallNoiseTexture, frac(p.yz * _WallNoiseScale)), noise) * n.x * _ColorPowers.x;

			float3 lcz = lerp(tex2D(_WallTexture, frac(p.xy * _WallFarScale)), tex2D(_WallNoiseTexture, frac(p.xy * _WallFarScale)), noise) * n.z * _ColorPowers.z;
			float3 lcx = lerp(tex2D(_WallTexture, frac(p.yz * _WallFarScale)), tex2D(_WallNoiseTexture, frac(p.yz * _WallFarScale)), noise) * n.x * _ColorPowers.x;

			cz = lerp(lcz,cz,det_lerp);
			cx = lerp(lcx,cx,det_lerp);
			_WallScale = lerp(_WallFarScale,_WallScale,det_lerp);

			det_lerp=saturate(1-IN.distdetail);

			float3 a = tex2D(_WallTexture,frac(p.xy * _WallDetailScale)) * n.z;
			float3 b = tex2D(_WallTexture,frac(p.yz * _WallDetailScale))* n.x;

			cz = lerp(a, lerp(a , cz, det_lerp), det_lerp);
			cx = lerp(b, lerp(b , cx, det_lerp), det_lerp);

			// Normal
			float3 blendingWeights = n;
			blendingWeights = (blendingWeights - 0.2) * 7;
			blendingWeights.y = blendingWeights.y *_WallPower; 
			blendingWeights = max(blendingWeights, 0); 
			blendingWeights /= (blendingWeights.x + blendingWeights.y + blendingWeights.z ).xxx; 

			float2 coord1 = p.zy * -_WallScale;
			float2 coord2 = p.zx * _FloorScale;
			float2 coord3 = float2(p.x, -p.y) * -_WallScale;

			float2 bumpFetch1 = (tex2D(_WallNormal, coord1).xy - 0.5) * _NormalPowers.z; 
			float2 bumpFetch2a = (tex2D(_WallNormal, coord2).xy - 0.5) *  _NormalPowers.x;
			float2 bumpFetch2b = (tex2D(_FloorNormal, coord2).xy - 0.5) * _NormalPowers.y;
			float2 bumpFetch2 = (wn.y < _CeilingPower) ? bumpFetch2a : bumpFetch2b;
			float2 bumpFetch3 = (tex2D(_WallNormal, coord3).xy - 0.5) * _NormalPowers.x; 

			float3 bump1 = float3(0, -bumpFetch1.y, -bumpFetch1.x); 
			float3 bump2 = float3(bumpFetch2.y, 0, bumpFetch2.x); 
			float3 bump3 = float3(bumpFetch3.x, bumpFetch3.y, 0);

			float3 blendedNormal = bump1.xyz * blendingWeights.xxx + 
			bump2.xyz * blendingWeights.yyy + 
			bump3.xyz * blendingWeights.zzz;
			
			o.BumpNormal = normalize(wn + (normalize(float4(blendedNormal.x, blendedNormal.y, -blendedNormal.z, 1))) * -_NormalPower);

			// Blend Mode: Multiply
			o.Albedo = (cy + cz + cx) * _Color * _ColorPower;
			o.Specular = _SpecularPower;
			o.Alpha = 1.0;

		}

      ENDCG

    }

    Fallback "Diffuse"
		
}
