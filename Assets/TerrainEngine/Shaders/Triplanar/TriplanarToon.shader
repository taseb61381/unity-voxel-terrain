Shader "TerrainEngine/Toon/Triplanar" {
	Properties {
		_Color ("Main Color", Color) = (.5,.5,.5,1)
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width", Range (.002, 0.03)) = .005
		_CeilingTexture ("Ceiling", 2D) = "white" {}
		_FloorTexture ("Floor", 2D) = "white" {}
		_WallTexture ("Wall", 2D) = "white" {}
		_TriplanarFrequency ("Triplanar Frequency", Float) = .2
	}
	
	CGINCLUDE
	#include "UnityCG.cginc"
	
	struct appdata {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
	};

	struct v2f {
		float4 pos : POSITION;
		float4 color : COLOR;
	};
	
	uniform float _Outline;
	uniform float4 _OutlineColor;
	
	v2f vert(appdata v) 
	{
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
		float2 offset = TransformViewToProjection(norm.xy);

		o.pos.xy += offset * o.pos.z * _Outline;
		o.color = _OutlineColor;
		return o;
	}
	ENDCG	

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		UsePass "Toon/Basic/BASE"
		Pass {
			Name "OUTLINE"
			Tags { "LightMode" = "Always" }
			Cull Front
			ZWrite On
			ColorMask RGB
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			half4 frag(v2f i) :COLOR { return i.color; }

			ENDCG
		}
		
		Tags { "RenderType" = "Opaque" }

		// Prevents chunk seam shimmer
		// Leaving this off by default.  If you see seams, then turn it back on.
		Cull Off


		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Lambert
		
		struct Input {
			float2 uv_Texture;
			float3 worldPos;
			float3 worldNormal;
			float4 color: COLOR;
		};
		
		sampler2D _CeilingTexture;
		sampler2D _FloorTexture;
		sampler2D _WallTexture;
				
		float _TriplanarFrequency;
		
		void surf (Input IN, inout SurfaceOutput o)
		{
			
			float3 p = IN.worldPos * _TriplanarFrequency;
			float3 wn = IN.worldNormal;
			float3 n = wn;
			n = normalize(abs(n));
			
			n = (n - 0.275) * 7.0;
			n = max(n, 0.0);
			n /= float3(n.x + n.y + n.z,n.x + n.y + n.z,n.x + n.y + n.z);
						
			float3 cy = (wn.y < 0.0)
				? tex2D(_CeilingTexture, p.zx) * (n.y)
				: tex2D(_FloorTexture, p.zx) * (n.y);
			
			float3 cz = tex2D(_WallTexture, p.xy) * n.z;
			float3 cx = tex2D(_WallTexture, p.yz) * n.x;
			
			o.Albedo = (cy + cz + cx) * .33;
			
		}

      ENDCG

    }
	
	Fallback "Toon/Basic"
}
