﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "TerrainEngine/Standard/NoiseLocal" {

   Properties {
		// Triplanar space, for UI
		[HideInInspector] _TriplanarSpace("Triplanar Space", Float) = 0.0

		_TexPower("Texture Power", Range(0, 20)) = 10.0
		_TopMultiplier ("Top Multiplier", Range(0,8)) = 1.0
		_BottomMultiplier ("Bottom Multiplier", Range(0,8)) = 0.0
		_VertexColorStrength("Vertex Color Strength", Range(0,1)) = 1.0
		_NoiseScale ("Noise Scale", Float) = 1.0
    	_NoiseVal1 ("Noise Value 1", Float) = 125777.0
		_NoiseVal2 ("Noise Value 2", Float) = 233.0
		_TransitionStartDistance("Transition Start Distance", Float) = 10
		_TransitionScaleLength("Transition Scale Length", Float) = 10
		
		// TOP
		
		_TopColor ("Color", Color) = (1,1,1,1)
		_TopMainTex ("Floor", 2D) = "white" {}
		_TopMainTexNoise ("FloorNoise", 2D) = "white" {}
		
		_TopGlossiness ("Floor Smoothness", Range(0,1)) = 0.0
		[Gamma] _TopMetallic ("Floor Metallic", Range(0,1)) = 0.0
		_TopMetallicGlossMap("Floor Metallic", 2D) = "black" {}
		_TopUsingMetallicGlossMap("Floor Using Metallic Gloss Map", float) = 0.0
		
		_TopBumpScale("Floor Bump Scale", Float) = 1.0
		_TopBumpMap("Floor Normal Map", 2D) = "bump" {}

		_TopEmissionColor("Floor Emission Color", Color) = (0,0,0)
		_TopEmissionMap("Floor Emission", 2D) = "white" {}

		[HideInInspector] _TopEmissionScaleUI("Floor Scale", Float) = 0.0
		[HideInInspector] _TopEmissionColorUI("Floor Color", Color) = (1,1,1)

		// Wall
		
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Wall", 2D) = "white" {}
		_MainTexNoise ("WallNoise", 2D) = "white" {}
		
		_Glossiness ("Wall Smoothness", Range(0,1)) = 0.0
		[Gamma] _Metallic ("Wall Metallic", Range(0,1)) = 0.0
		_MetallicGlossMap("Wall Metallic", 2D) = "black" {}
		_UsingMetallicGlossMap("Wall Using Metallic Gloss Map", float) = 0.0
		
		_BumpScale("Wall Bump Scale", Float) = 1.0
		_BumpMap("Wall Normal Map", 2D) = "bump" {}

		_EmissionColor("Wall Emission Color", Color) = (0,0,0)
		_EmissionMap("Wall Emission", 2D) = "white" {}

		[HideInInspector] _EmissionScaleUI("Wall Scale", Float) = 0.0
		[HideInInspector] _EmissionColorUI("Wall Color", Color) = (1,1,1)

		_WallFarScale ("Wall Far Scale", Float) = .2
		_WallDetailScale("Wall Detail Scale", Float) = .2
		_WallDetailStartDistance("Wall Detail Start Distance", Float) = 100
		_WallDetailScaleLength("Wall Detail Scale Length", Float) = 10
	}
	SubShader {
		Tags { "RenderType"="Opaque"  "PerformanceChecks"="False"}
		LOD 300

		CGPROGRAM
		#pragma target 3.0
		
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard vertex:vert fullforwardshadows
		#include "../ShadersCommon.cginc"

		#pragma shader_feature _EMISSION
		#pragma shader_feature _METALLICGLOSSMAP

		// Uncomment to enable experimental feature which flips
		// backward textures. Note: Causes some normals to be flipped.
		// #define _UVFREE_FLIP_BACKWARD_TEXTURES
		
		half _TexPower;
		half _TopMultiplier;
		
		// FRONT
		
		fixed4		_Color;

		sampler2D	_MainTex;
		sampler2D	_MainTexNoise;
		float4		_MainTex_ST;

		sampler2D	_BumpMap;
		half		_BumpScale;

		sampler2D	_MetallicGlossMap;
		fixed		_Metallic;
		fixed		_Glossiness;
		fixed		_UsingMetallicGlossMap;
		
		half4 		_EmissionColor;
		sampler2D	_EmissionMap;
				
		// TOP
		
		fixed4		_TopColor;

		sampler2D	_TopMainTex;
		sampler2D	_TopMainTexNoise;
		float4		_TopMainTex_ST;

		sampler2D	_TopBumpMap;
		half		_TopBumpScale;

		sampler2D	_TopMetallicGlossMap;
		half		_TopMetallic;
		half		_TopGlossiness;
		fixed		_TopUsingMetallicGlossMap;

		half4 		_TopEmissionColor;
		sampler2D	_TopEmissionMap;
				
		#ifdef		_UVFREE_VERTEX_COLOR
		fixed		_VertexColorStrength;
		#endif

		float _TransitionStartDistance;
		float _TransitionScaleLength;
		float _WallFarScale;
		float _WallDetailStartDistance;
		float _WallDetailScaleLength;
		float _WallDetailScale;
						
		struct Input {
			fixed3 powerNormal;
			fixed3 normal;
			float3 worldPos;
			float2 dist;	
		};

		void vert (inout appdata_full v, out Input o) {
		
			UNITY_INITIALIZE_OUTPUT(Input,o);
			float3 worldPos=mul (unity_ObjectToWorld, v.vertex).xyz;
			float dist=distance(worldPos, _WorldSpaceCameraPos);

			o.dist.x=saturate((dist - _TransitionStartDistance) / _TransitionScaleLength);
			o.dist.y=saturate((dist - _WallDetailStartDistance) / _WallDetailScaleLength);	


			fixed3 worldNormal = normalize(mul(unity_ObjectToWorld, fixed4(v.normal, 0)).xyz);					
			o.normal = v.normal;
			fixed3 powerNormal = v.normal;
			fixed3 weightedPowerNormal = v.normal;

			// texpower sets the sharpness
			powerNormal = pow(abs(powerNormal), _TexPower);
			powerNormal = max(powerNormal, 0.0001);
			powerNormal /= dot(powerNormal, 1.0);
			o.powerNormal = powerNormal;
			

			weightedPowerNormal.y = max(0.0, weightedPowerNormal.y)*_TopMultiplier + min(0.0, weightedPowerNormal.y);
			
			weightedPowerNormal = pow(abs(weightedPowerNormal), _TexPower);
			weightedPowerNormal = max(weightedPowerNormal, 0.0001);
			weightedPowerNormal /= dot(weightedPowerNormal, 1.0);
						
			fixed3 lerpedPowerNormal = lerp(powerNormal, weightedPowerNormal, weightedPowerNormal.y);

			v.tangent.xyz = 
					cross(v.normal, fixed3(0.0,sign(v.normal.x),0.0)) * (lerpedPowerNormal.x)
				  + cross(v.normal, fixed3(0.0,0.0,sign(v.normal.y))) * (lerpedPowerNormal.y)
				  + cross(v.normal, fixed3(0.0,sign(v.normal.z),0.0)) * (lerpedPowerNormal.z)
				;
				
				v.tangent.w = dot(-v.normal, lerpedPowerNormal);
		}
		
		void surf (Input IN, inout SurfaceOutputStandard o) {
			
			fixed topLerp = smoothstep(0.0, 1.0, _TopMultiplier);
			
			fixed3 weightedPowerNormal = IN.normal;
			weightedPowerNormal.y = max(0.0, weightedPowerNormal.y)*_TopMultiplier + min(0.0, weightedPowerNormal.y);
			weightedPowerNormal = pow(abs(weightedPowerNormal), _TexPower);
			weightedPowerNormal = max(weightedPowerNormal, 0.0001);
			weightedPowerNormal /= dot(weightedPowerNormal, 1.0);

			fixed topBottomLerp = sign(IN.normal.y)*0.5 + 0.5;
			fixed yLerp = weightedPowerNormal.y;
			
			// TRIPLANAR UVs BASED ON WORLD OR LOCAL POSITION
			//
			
			float3 pos = mul(unity_WorldToObject, float4(IN.worldPos, 1.0)).xyz;
			float2 posX = pos.zy;
			float2 posY = pos.xz;
			float2 posZ = float2(-pos.x, pos.y);

			float noise = fractalNoise(pos * _NoiseScale);

			float2 xUV = posX * _MainTex_ST.xy + _MainTex_ST.zw;
			float2 yUVTop = posY * _TopMainTex_ST.xy + _TopMainTex_ST.zw;
			float2 yUV = posY * _MainTex_ST.xy + _MainTex_ST.zw;
			float2 zUV = posZ * _MainTex_ST.xy + _MainTex_ST.zw;
			
			#ifdef _UVFREE_FLIP_BACKWARD_TEXTURES
				fixed3 powerSign = sign(IN.normal);
				xUV.x *= powerSign.x;
				zUV.x *= powerSign.z;
				yUV.y *= powerSign.y;
				yUVTop.y *= powerSign.y;
				yUVBottom.y *= powerSign.y;
			#endif
						
			// DIFFUSE
			//

			float det_lerp=saturate(1-IN.dist.x);

			fixed4 texX = lerp(tex2D(_MainTex, xUV*_WallFarScale) * _Color,tex2D(_MainTex, xUV) * _Color,det_lerp);
			fixed4 texY = lerp(tex2D(_MainTex, yUV*_WallFarScale) * _Color,tex2D(_MainTex, yUV) * _Color,det_lerp);
			fixed4 texZ = lerp(tex2D(_MainTex, zUV*_WallFarScale) * _Color,tex2D(_MainTex, zUV) * _Color,det_lerp);


			fixed4 texXNoise = lerp(tex2D(_MainTexNoise, xUV*_WallFarScale) * _Color,tex2D(_MainTexNoise, xUV) * _Color,det_lerp);
			fixed4 texYNoise = lerp(tex2D(_MainTexNoise, yUV*_WallFarScale) * _Color,tex2D(_MainTexNoise, yUV) * _Color,det_lerp);
			fixed4 texZNoise = lerp(tex2D(_MainTexNoise, zUV*_WallFarScale) * _Color,tex2D(_MainTexNoise, zUV) * _Color,det_lerp);

			texX = lerp(texX,texXNoise,noise);
			texY = lerp(texY,texYNoise,noise);
			texZ = lerp(texZ,texZNoise,noise);
			
			fixed4 texTop = tex2D(_TopMainTex, yUVTop) * _TopColor;
			fixed4 texTopNoise = tex2D(_TopMainTexNoise, yUVTop) * _TopColor;

			texTop = lerp(texTop,texTopNoise,noise);

			texTop = lerp(texY, texTop, topLerp);
			texTop = lerp(texY, texTop, topBottomLerp);
			
			fixed4 tex = 
			    lerp(texX * IN.powerNormal.x, texTop * weightedPowerNormal.x, yLerp)
			  + lerp(texY * IN.powerNormal.y, texTop * weightedPowerNormal.y, yLerp)
			  + lerp(texZ * IN.powerNormal.z, texTop * weightedPowerNormal.z, yLerp)
			;
			
			#ifdef _UVFREE_VERTEX_COLOR			 
				tex *= lerp(fixed4(1.0,1.0,1.0,1.0), IN.color, _VertexColorStrength);
			#endif
				
			o.Albedo = max(fixed3(0.001, 0.001, 0.001), tex.rgb);
			o.Alpha = tex.a;
			
			// NORMAL
			//

			fixed3 bumpX = UnpackScaleNormal(tex2D(_BumpMap, xUV), _BumpScale);
			fixed3 bumpY = UnpackScaleNormal(tex2D(_BumpMap, yUV), _BumpScale);
			fixed3 bumpZ = UnpackScaleNormal(tex2D(_BumpMap, zUV), _BumpScale);
			
			fixed3 bumpTop = UnpackScaleNormal(tex2D(_TopBumpMap, yUVTop), _TopBumpScale);
			bumpTop = lerp(bumpY, bumpTop, topLerp);


				bumpTop = lerp(bumpY, bumpTop, topBottomLerp);

			o.Normal = normalize( 
			    lerp(bumpX * IN.powerNormal.x, bumpTop * weightedPowerNormal.x, yLerp)
			  + lerp(bumpY * IN.powerNormal.y, bumpTop * weightedPowerNormal.y, yLerp)
			  + lerp(bumpZ * IN.powerNormal.z, bumpTop * weightedPowerNormal.z, yLerp)
			  );
						
			// METALLIC/GLOSS
			//
				
			fixed2 mgX = fixed2(_Metallic, _Glossiness);
			fixed2 mgY = mgX;
			fixed2 mgZ = mgX;
			fixed2 mgTop = fixed2(_TopMetallic, _TopGlossiness);
			
			#ifdef _METALLICGLOSSMAP
				
				mgX = lerp(mgX, tex2D(_MetallicGlossMap, xUV).ra, _UsingMetallicGlossMap);
				mgY = lerp(mgY, tex2D(_MetallicGlossMap, yUV).ra, _UsingMetallicGlossMap);
				mgZ = lerp(mgZ, tex2D(_MetallicGlossMap, zUV).ra, _UsingMetallicGlossMap);
				
				mgTop = lerp(mgTop, tex2D(_TopMetallicGlossMap, yUVTop).ra, _TopUsingMetallicGlossMap);
				#ifdef _UVFREE_BOTTOM
					mgBottom = lerp(mgBottom, tex2D(_BottomMetallicGlossMap, yUVBottom).ra, _BottomUsingMetallicGlossMap);
				#endif

			#endif
			
			mgTop = lerp(mgY, mgTop, topLerp);

				mgTop = lerp(mgY, mgTop, topBottomLerp);
				
			fixed2 mg = 
			    lerp(mgX * IN.powerNormal.x, mgTop * weightedPowerNormal.x, yLerp)
			  + lerp(mgY * IN.powerNormal.y, mgTop * weightedPowerNormal.y, yLerp)
			  + lerp(mgZ * IN.powerNormal.z, mgTop * weightedPowerNormal.z, yLerp)
			;

			o.Metallic = mg.x;
			o.Smoothness = mg.y;
			
			// EMISSION
			//
				
			#ifdef _EMISSION
				half3 emissionX = tex2D(_EmissionMap, xUV).rgb * _EmissionColor.rgb;
				half3 emissionY = tex2D(_EmissionMap, yUV).rgb * _EmissionColor.rgb;
				
				half3 emissionYTop = tex2D(_TopEmissionMap, yUVTop).rgb * _TopEmissionColor.rgb;
				emissionYTop = lerp(emissionY, emissionYTop, topLerp);


				emissionYTop = lerp(emissionY, emissionYTop, topBottomLerp);				
				
				half3 emissionZ = tex2D(_EmissionMap, zUV).rgb * _EmissionColor.rgb;
				
				o.Emission +=  
				    lerp(emissionX * IN.powerNormal.x, emissionYTop * weightedPowerNormal.x, yLerp)
				  + lerp(emissionY * IN.powerNormal.y, emissionYTop * weightedPowerNormal.y, yLerp)
				  + lerp(emissionZ * IN.powerNormal.z, emissionYTop * weightedPowerNormal.z, yLerp)
				;
			#endif
		
		}
		ENDCG
	} 
	FallBack "Diffuse"
}

