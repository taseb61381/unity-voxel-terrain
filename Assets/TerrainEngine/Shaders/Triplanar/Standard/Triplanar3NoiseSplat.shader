// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "TerrainEngine/Standard/NoiseSplat" {

    Properties {
		_TexPower("Texture Power", Range(0, 20)) = 10.0
		_TopMultiplier ("Top Multiplier", Range(0,8)) = 1.0
		_BottomMultiplier ("Bottom Multiplier", Range(0,8)) = 0.0
		_NoiseScale ("Noise Scale", Float) = 1.0
    	_NoiseVal1 ("Noise Value 1", Float) = 125777.0
		_NoiseVal2 ("Noise Value 2", Float) = 233.0
		_TransitionStartDistance("Transition Start Distance", Float) = 10
		_TransitionScaleLength("Transition Scale Length", Float) = 10
		_SplatScales("Splat Scales", Vector) = (1,1,1,1)
		_SplatPowers("Splat Powers", Vector) = (1,1,1,1)
		_NormalPowers("Normal Powers", Vector) = (1,1,1,1)

		// TOP
		
		_TopColor ("Color", Color) = (1,1,1,1)
		_FloorTextureR ("FloorR", 2D) = "white" {}
		_FloorNormalR ("Floor NormalR", 2D) = "white" {}
		_TopGlossinessR ("FloorR Smoothness", Range(0,1)) = 0.0
		[Gamma] _TopMetallicR ("FloorR Metallic", Range(0,1)) = 0.0

		_FloorTextureG ("FloorG", 2D) = "white" {}
		_FloorNormalG ("Floor NormalG", 2D) = "white" {}
		_TopGlossinessG ("FloorG Smoothness", Range(0,1)) = 0.0
		[Gamma] _TopMetallicG ("FloorG Metallic", Range(0,1)) = 0.0

		_FloorTextureB ("FloorB", 2D) = "white" {}
		_FloorNormalB ("Floor NormalB", 2D) = "white" {}
		_TopGlossinessB ("FloorB Smoothness", Range(0,1)) = 0.0
		[Gamma] _TopMetallicB ("FloorB Metallic", Range(0,1)) = 0.0

		_FloorTextureA ("FloorA", 2D) = "white" {}
		_FloorNormalA ("Floor NormalA", 2D) = "white" {}
		_TopGlossinessA ("FloorA Smoothness", Range(0,1)) = 0.0
		[Gamma] _TopMetallicA ("FloorA Metallic", Range(0,1)) = 0.0
		
		_TopBumpScale("Floor Bump Scale", Float) = 1.0

		_TopEmissionColor("Floor Emission Color", Color) = (0,0,0)

		[HideInInspector] _TopEmissionScaleUI("Floor Scale", Float) = 0.0
		[HideInInspector] _TopEmissionColorUI("Floor Color", Color) = (1,1,1)

		// Wall
		
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Wall", 2D) = "white" {}
		_MainTexNoise ("WallNoise", 2D) = "white" {}
		_BumpMap("Wall Normal Map", 2D) = "bump" {}

		_Glossiness ("Wall Smoothness", Range(0,1)) = 0.0
		[Gamma] _Metallic ("Wall Metallic", Range(0,1)) = 0.0
		_UsingMetallicGlossMap("Wall Using Metallic Gloss Map", float) = 0.0
		_BumpScale("Wall Bump Scale", Float) = 1.0
		_EmissionColor("Wall Emission Color", Color) = (0,0,0)

		_WallFarScale ("Wall Far Scale", Float) = .2
		_WallDetailScale("Wall Detail Scale", Float) = .2
		_WallDetailStartDistance("Wall Detail Start Distance", Float) = 100
		_WallDetailScaleLength("Wall Detail Scale Length", Float) = 10
	}
	SubShader {
		Tags { "RenderType"="Opaque"}
		LOD 300

		CGPROGRAM
		#pragma target 3.0
		
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard vertex:vert nolightmap
		#include "../ShadersCommon.cginc"

		
		half _TexPower;
		half _TopMultiplier;
		
		// FRONT
		
		fixed4		_Color;

		sampler2D	_MainTex;
		sampler2D	_MainTexNoise;
		float4		_MainTex_ST;

		sampler2D	_BumpMap;
		half		_BumpScale;

		fixed		_Metallic;
		fixed		_Glossiness;
		fixed		_UsingMetallicGlossMap;
		
		half4 		_EmissionColor;
		sampler2D	_EmissionMap;
				
		// TOP
		
		fixed4		_TopColor;

		sampler2D _FloorTextureR;
		sampler2D _FloorTextureG;
		sampler2D _FloorTextureB;
		sampler2D _FloorTextureA;
		sampler2D _FloorNormalR;
		sampler2D _FloorNormalG;
		sampler2D _FloorNormalB;
		sampler2D _FloorNormalA;
		float4 _SplatPowers;
		float4 _NormalPowers;
		float4 _SplatScales;

		sampler2D	_TopBumpMap;
		half		_TopBumpScale;

		half		_TopMetallicR;
		half		_TopGlossinessR;

		half		_TopMetallicG;
		half		_TopGlossinessG;

		half		_TopMetallicB;
		half		_TopGlossinessB;

		half		_TopMetallicA;
		half		_TopGlossinessA;

		half4 		_TopEmissionColor;
		sampler2D	_TopEmissionMap;

		float _TransitionStartDistance;
		float _TransitionScaleLength;
		float _WallFarScale;
		float _WallDetailStartDistance;
		float _WallDetailScaleLength;
		float _WallDetailScale;
						
		struct Input {
			fixed3 powerNormal;
			fixed3 normal;
			float3 worldPos;
			float2 dist;
			float4 color : COLOR;	
		};

		void vert (inout appdata_full v, out Input o) {
		
			UNITY_INITIALIZE_OUTPUT(Input,o);
			float3 worldPos=mul (unity_ObjectToWorld, v.vertex).xyz;
			float dist=distance(worldPos, _WorldSpaceCameraPos);

			o.dist.x=saturate((dist - _TransitionStartDistance) / _TransitionScaleLength);
			o.dist.y=saturate((dist - _WallDetailStartDistance) / _WallDetailScaleLength);	


			fixed3 worldNormal = normalize(mul(unity_ObjectToWorld, fixed4(v.normal, 0)).xyz);					
			o.normal = worldNormal;
			fixed3 powerNormal = worldNormal;
			fixed3 weightedPowerNormal = worldNormal;

			// texpower sets the sharpness
			powerNormal = pow(abs(powerNormal), _TexPower);
			powerNormal = max(powerNormal, 0.0001);
			powerNormal /= dot(powerNormal, 1.0);
			o.powerNormal = powerNormal;
			

			weightedPowerNormal.y = max(0.0, weightedPowerNormal.y)*_TopMultiplier + min(0.0, weightedPowerNormal.y);
			
			weightedPowerNormal = pow(abs(weightedPowerNormal), _TexPower);
			weightedPowerNormal = max(weightedPowerNormal, 0.0001);
			weightedPowerNormal /= dot(weightedPowerNormal, 1.0);
						
			fixed3 lerpedPowerNormal = lerp(powerNormal, weightedPowerNormal, weightedPowerNormal.y);

			v.tangent.xyz = 
				cross(v.normal, mul(unity_WorldToObject,fixed4(0.0,sign(worldNormal.x),0.0,0.0)).xyz * (lerpedPowerNormal.x))
				+ cross(v.normal, mul(unity_WorldToObject,fixed4(0.0,0.0,sign(worldNormal.y),0.0)).xyz * (lerpedPowerNormal.y))
				+ cross(v.normal, mul(unity_WorldToObject,fixed4(0.0,sign(worldNormal.z),0.0,0.0)).xyz * (lerpedPowerNormal.z))
			;
				
			v.tangent.w = dot(-worldNormal, lerpedPowerNormal);		
		}
		
		void surf (Input IN, inout SurfaceOutputStandard o) {
			
			fixed topLerp = smoothstep(0.0, 1.0, _TopMultiplier);
			
			fixed3 weightedPowerNormal = IN.normal;
			weightedPowerNormal.y = max(0.0, weightedPowerNormal.y)*_TopMultiplier + min(0.0, weightedPowerNormal.y);
			weightedPowerNormal = pow(abs(weightedPowerNormal), _TexPower);
			weightedPowerNormal = max(weightedPowerNormal, 0.0001);
			weightedPowerNormal /= dot(weightedPowerNormal, 1.0);

			fixed topBottomLerp = sign(IN.normal.y)*0.5 + 0.5;
			fixed yLerp = weightedPowerNormal.y;
			
			// TRIPLANAR UVs BASED ON WORLD OR LOCAL POSITION
			//
			
			float3 pos = IN.worldPos;
			float2 posX = IN.worldPos.zy;
			float2 posY = IN.worldPos.xz;
			float2 posZ = float2(-IN.worldPos.x, IN.worldPos.y);				

			float noise = fractalNoise(pos * _NoiseScale);

			float2 xUV = posX * _MainTex_ST.xy + _MainTex_ST.zw;
			float2 yUVTop = posY;
			float2 yUV = posY * _MainTex_ST.xy + _MainTex_ST.zw;
			float2 zUV = posZ * _MainTex_ST.xy + _MainTex_ST.zw;
						
			// DIFFUSE
			//

			float det_lerp=saturate(1-IN.dist.x);

			fixed4 texX = lerp(tex2D(_MainTex, xUV*_WallFarScale) * _Color,tex2D(_MainTex, xUV) * _Color,det_lerp);
			fixed4 texY = lerp(tex2D(_MainTex, yUV*_WallFarScale) * _Color,tex2D(_MainTex, yUV) * _Color,det_lerp);
			fixed4 texZ = lerp(tex2D(_MainTex, zUV*_WallFarScale) * _Color,tex2D(_MainTex, zUV) * _Color,det_lerp);


			fixed4 texXNoise = lerp(tex2D(_MainTexNoise, xUV*_WallFarScale) * _Color,tex2D(_MainTexNoise, xUV) * _Color,det_lerp);
			fixed4 texYNoise = lerp(tex2D(_MainTexNoise, yUV*_WallFarScale) * _Color,tex2D(_MainTexNoise, yUV) * _Color,det_lerp);
			fixed4 texZNoise = lerp(tex2D(_MainTexNoise, zUV*_WallFarScale) * _Color,tex2D(_MainTexNoise, zUV) * _Color,det_lerp);

			texX = lerp(texX,texXNoise,noise);
			texY = lerp(texY,texYNoise,noise);
			texZ = lerp(texZ,texZNoise,noise);

			fixed4 texTop;
			texTop  = IN.color.r * tex2D(_FloorTextureR, yUVTop*_SplatScales.x) * _TopColor * _SplatPowers.x;
			texTop += IN.color.g * tex2D(_FloorTextureG, yUVTop*_SplatScales.y) * _TopColor * _SplatPowers.y;
			texTop += IN.color.b * tex2D(_FloorTextureB, yUVTop*_SplatScales.z) * _TopColor * _SplatPowers.z;
			texTop += IN.color.a * tex2D(_FloorTextureA, yUVTop*_SplatScales.w) * _TopColor * _SplatPowers.w;

			texTop = lerp(texY, texTop, topLerp);
			texTop = lerp(texY, texTop, topBottomLerp);
			
			fixed4 tex = 
			    lerp(texX * IN.powerNormal.x, texTop * weightedPowerNormal.x, yLerp)
			  + lerp(texY * IN.powerNormal.y, texTop * weightedPowerNormal.y, yLerp)
			  + lerp(texZ * IN.powerNormal.z, texTop * weightedPowerNormal.z, yLerp)
			;
				
			o.Albedo = max(fixed3(0.001, 0.001, 0.001), tex.rgb);
			o.Alpha = 1.0;
			
			// NORMAL
			//

			fixed3 bumpX = UnpackScaleNormal(tex2D(_BumpMap, xUV), _BumpScale);
			fixed3 bumpY = UnpackScaleNormal(tex2D(_BumpMap, yUV), _BumpScale);
			fixed3 bumpZ = UnpackScaleNormal(tex2D(_BumpMap, zUV), _BumpScale);
			
			fixed3 bumpTop;
			bumpTop =	IN.color.r * UnpackScaleNormal(tex2D(_FloorNormalR, yUVTop*_SplatScales.x), _TopBumpScale*_NormalPowers.x );
			bumpTop +=  IN.color.g * UnpackScaleNormal(tex2D(_FloorNormalG, yUVTop*_SplatScales.y), _TopBumpScale*_NormalPowers.y );
			bumpTop +=	IN.color.b * UnpackScaleNormal(tex2D(_FloorNormalB, yUVTop*_SplatScales.z), _TopBumpScale*_NormalPowers.z );
			bumpTop +=  IN.color.a * UnpackScaleNormal(tex2D(_FloorNormalA, yUVTop*_SplatScales.w), _TopBumpScale*_NormalPowers.w );

			bumpTop = lerp(bumpY, bumpTop, topLerp);
			bumpTop = lerp(bumpY, bumpTop, topBottomLerp);

			o.Normal = normalize( 
			    lerp(bumpX * IN.powerNormal.x, bumpTop * weightedPowerNormal.x, yLerp)
			  + lerp(bumpY * IN.powerNormal.y, bumpTop * weightedPowerNormal.y, yLerp)
			  + lerp(bumpZ * IN.powerNormal.z, bumpTop * weightedPowerNormal.z, yLerp)
			  );
						
			// METALLIC/GLOSS
			//
				
			fixed2 mgX = fixed2(_Metallic, _Glossiness);
			fixed2 mgY = mgX;
			fixed2 mgZ = mgX;
			fixed2 mgTop = fixed2(_TopMetallicR, _TopGlossinessR) * IN.color.r +
			fixed2(_TopMetallicG, _TopGlossinessG) * IN.color.g +
			fixed2(_TopMetallicB, _TopGlossinessB) * IN.color.b +
			fixed2(_TopMetallicA, _TopGlossinessA) * IN.color.a;
			
			mgTop = lerp(mgY, mgTop, topLerp);
			mgTop = lerp(mgY, mgTop, topBottomLerp);
				
			fixed2 mg = 
			    lerp(mgX * IN.powerNormal.x, mgTop * weightedPowerNormal.x, yLerp)
			  + lerp(mgY * IN.powerNormal.y, mgTop * weightedPowerNormal.y, yLerp)
			  + lerp(mgZ * IN.powerNormal.z, mgTop * weightedPowerNormal.z, yLerp)
			;

			o.Metallic = mg.x;
			o.Smoothness = mg.y;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}

