﻿Shader "TerrainEngine/Standard/Triplanar" {
	Properties {
		_TexPower("Texture Power", Range(0, 20)) = 10.0
		_FloorPower ("Floor Power", Range(0,10)) = 1.0
		_TexPowerN("Texture Power", float) = 0.0

		_FloorColorR ("Floor ColorR", Color) = (1,1,1,1)
		_FloorColorG ("Floor ColorG", Color) = (1,1,1,1)
		_FloorColorB ("Floor ColorB", Color) = (1,1,1,1)
		_FloorColorA ("Floor ColorA", Color) = (1,1,1,1)
		_WallColor ("Wall Color", Color) = (1,1,1,1)
		_FloorColorN ("Floor ColorN", Color) = (1,1,1,1)

		_FloorR ("FloorR (Occlusion A)", 2D) = "white" {}
		_BumpR ("BumpR", 2D) = "white" {}

		_FloorG ("FloorG (Occlusion A)", 2D) = "white" {}
		_BumpG ("BumpG", 2D) = "white" {}

		_FloorB ("FloorB (Occlusion A)", 2D) = "white" {}
		_BumpB ("BumpB", 2D) = "white" {}

		_FloorA ("FloorA (Occlusion A)", 2D) = "white" {}
		_BumpA ("BumpA", 2D) = "white" {}

		_Wall ("Wall", 2D) = "white" {}
		_WallBump ("Wall Bump", 2D) = "white" {}

		_DataR ("Scale/Smoothness/Metalic/Bump", Vector) = (1,0,0,1)
		_DataG ("Scale/Smoothness/Metalic/Bump", Vector) = (1,0,0,1)
		_DataB ("Scale/Smoothness/Metalic/Bump", Vector) = (1,0,0,1)
		_DataA ("Scale/Smoothness/Metalic/Bump", Vector) = (1,0,0,1)
		_DataWall ("Scale/Smoothness/Metalic/Bump", Vector) = (1,0,0,1)

		_ColorPowerR ("Color Power R", float) = 1
		_ColorPowerG ("Color Power G", float) = 1
		_ColorPowerB ("Color Power B", float) = 1
		_ColorPowerA ("Color Power A", float) = 1
		_ColorPowerWall ("Color Power Wall", float) = 1

		// DISTANT
		_FloorScaleD ("Distant Floor Scale", float) = 1
		_WallScaleD ("Distant Wall Scale", float) = 1
		_DataBumpD ("Floor,FloorD,Wall,WallD", Vector) = (1.33,0.66,1.33,0.66)
		_TransitionStartD("Transition Start Distance", Float) = 100
		_TransitionScaleD("Transition Scale Length", Float) = 10

		// NOISE
		_FloorRN ("FloorR Noise", 2D) = "white" {}
		_BumpRN ("BumpR Noise", 2D) = "white" {}
		_ColorPowerN ("Color Power N", float) = 1

		_WallN ("WallN (Occlusion A)", 2D) = "white" {}
		_WallBumpN ("WallBumpN (Occlusion A)", 2D) = "white" {}

		_DataRN ("Scale/Smoothness/Metalic/Bump", Vector) = (1,0,0,1)
		_DataWallN ("Scale/Smoothness/Metalic/Bump", Vector) = (1,0,0,1)

		_DataN ("Scale/Offset/Pow", Vector) = (0.04,0,1,0)
		_ColorPowerRN ("Color Power RN", float) = 1

		// VERTICAL
		_TextureV ("Vertical texture", 2D) = "white" {}
		_BumpV ("Vertical Bump Texture", 2D) = "white" {}
		_PowerV ("Vertical texture power", float) = 0.5
		_DataV ("Scale/Smoothness/Metalic/Bump", Vector) = (1,0,0,1)
		_PerturbationsV ("Scale1/Power1/Scale2/Power2", Vector) = (1,1,1,1)
		_DataNV ("VertPower/VertOffset/NoisePower/NoiseOffset", Vector) = (1,0,1,0)
		_ColorPowerV ("Color Power RN", float) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard addshadow vertex:vert nolightmap

		#pragma shader_feature _TE_LOCAL
		#pragma shader_feature _TE_SPLAT
		#pragma shader_feature _TE_VERTICAL 
		#pragma shader_feature _TE_NOISE 
		#pragma shader_feature _TE_DISTANT 

		// TRIPLANAR && SPLATMAP
		sampler2D _FloorR,_FloorG,_FloorB,_FloorA,_Wall;
		sampler2D _BumpR,_BumpG,_BumpB,_BumpA,_WallBump;
		fixed4 _DataR,_DataG,_DataB,_DataA,_DataWall;
		float4 _FloorColorR,_FloorColorG,_FloorColorB,_FloorColorA,_WallColor,_FloorColorN;
		float _FloorPower, _TexPower;
		float _TransitionStartD,_TransitionScaleD;

		float _ColorPowerR,_ColorPowerG,_ColorPowerB,_ColorPowerA,_ColorPowerWall,_ColorPowerRN,_ColorPowerN;

		// DISTANT
		float _FloorScaleD,_WallScaleD;

		// NOISE
		sampler2D _FloorRN,_BumpRN, _WallN,_WallBumpN;
		fixed4 _DataN,_DataRN,_DataWallN,_DataBumpD;
		float _TexPowerN;

		// VERTICAL
		sampler2D _TextureV,_BumpV;
		fixed4 _DataV,_PerturbationsV,_DataNV;
		float _PowerV;

		struct appdata {
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float4 tangent : TANGENT;

			#if _TE_SPLAT
			float4 color : COLOR;
			float2 texcoord : TEXCOORD0;
			#endif
		};

		struct Input
		{
			fixed3 powerNormal;
			fixed3 normal;
			float3 worldPos;
			fixed3 weightedPowerNormal;

			#if _TE_SPLAT
				float4 color : COLOR;
			#endif

			#if _TE_NOISE
				float noise;
			#endif

			
			#if _TE_DISTANT
				float dist;
			#endif
		};

		float hash( float n )
		{
			return frac(sin(n)*43758.5453);
		}
 
		float FastPerlin( float3 x )
		{
			// The noise function returns a value in the range -1.0f -> 1.0f
 
			float3 p = floor(x);
			float3 f = frac(x);
 
			f       = f*f*(3.0-2.0*f);
			float n = p.x + p.y*57.0 + 113.0*p.z;
 
			return lerp(lerp(lerp( hash(n+0.0), hash(n+1.0),f.x),
						   lerp( hash(n+57.0), hash(n+58.0),f.x),f.y),
					   lerp(lerp( hash(n+113.0), hash(n+114.0),f.x),
						   lerp( hash(n+170.0), hash(n+171.0),f.x),f.y),f.z);
		}

		void vert (inout appdata v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input,o);

			float noise = 0;
			float3 worldPos,worldNormal,powerNormal,weightedPowerNormal,lerpedPowerNormal;
			worldPos=mul (unity_ObjectToWorld, v.vertex).xyz;
			#if _TE_LOCAL
				worldNormal =v.normal;	
			#else
				worldNormal = normalize(mul(unity_ObjectToWorld, fixed4(v.normal, 0)).xyz);
			#endif

			#if _TE_NOISE
				noise = clamp((FastPerlin(worldPos*_DataN.x)+_DataN.y + ((FastPerlin(worldPos*_DataN.x*0.1)+1)*0.001)) * (_DataN.z+1),0,1);
				o.noise = noise;
				noise = (((noise+1)*0.5)+1)*_TexPowerN;
			#endif

			#if _TE_DISTANT
				o.dist=saturate((distance(worldPos, _WorldSpaceCameraPos) - _TransitionStartD) / _TransitionScaleD);
			#endif
			
			powerNormal = worldNormal;
			weightedPowerNormal = worldNormal;

			// texpower sets the sharpness
			powerNormal = max(pow(abs(powerNormal), _TexPower),0.0001);
			powerNormal /= dot(powerNormal, 1.0);
			

			weightedPowerNormal.y = max(0.0, weightedPowerNormal.y)*(_FloorPower+noise) + min(0.0, weightedPowerNormal.y);
			
			weightedPowerNormal = pow(abs(weightedPowerNormal), _TexPower);
			weightedPowerNormal = max(weightedPowerNormal, 0.0001);
			weightedPowerNormal /= dot(weightedPowerNormal, 1.0);
						
			lerpedPowerNormal = lerp(powerNormal, weightedPowerNormal, weightedPowerNormal.y);

			v.tangent.xyz = 
					cross(worldNormal, fixed3(0.0,sign(worldNormal.x),0.0)) * (lerpedPowerNormal.x)
				  + cross(worldNormal, fixed3(0.0,0.0,sign(worldNormal.y))) * (lerpedPowerNormal.y)
				  + cross(worldNormal, fixed3(0.0,sign(worldNormal.z),0.0)) * (lerpedPowerNormal.z)
				;
				
				v.tangent.w = dot(-worldNormal, lerpedPowerNormal);

			o.normal = worldNormal;		
			o.powerNormal = powerNormal;
			o.weightedPowerNormal = weightedPowerNormal;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			fixed topLerp = smoothstep(0.0, 1.0, _FloorPower);
			
			fixed3 weightedPowerNormal = IN.weightedPowerNormal;

			fixed topBottomLerp = sign(IN.normal.y)*0.5 + 0.5;

			float3 pos;
			float2 posX,posY,posZ;
			float noisevalue = 1;

			#if _TE_NOISE
				noisevalue = IN.noise;
			#endif
			
			#if _TE_LOCAL
				pos = mul(unity_WorldToObject, float4(IN.worldPos, 1.0)).xyz;
				posX = pos.zy;
				posY = pos.xz;
				posZ = float2(-pos.x, pos.y);
			#else
				pos = IN.worldPos;
				posX = pos.zy;
				posY  = pos.xz;
				posZ = float2(-pos.x, pos.y);	
			#endif

			float2 posDataWallx = posX * _DataWall.x;
			float2 posDataWally = posY * _DataWall.x;
			float2 posDataWallz = posZ * _DataWall.x;
			float2 posWallScaleDx = posX * _WallScaleD;
			float2 posWallScaleDy = posY * _WallScaleD;
			float2 posWallScaleDz = posZ * _WallScaleD;

			// NORMAL
			//
			fixed3 bumpX,bumpY,bumpZ,bumpTop;

			#if _TE_DISTANT
				//bumpX = UnpackScaleNormal((tex2D(_WallBump, posDataWallx)*_DataBumpD.z  + tex2D(_WallBump, posWallScaleDx)*_DataBumpD.w )*0.5, _DataWall.w);
				//bumpY = UnpackScaleNormal((tex2D(_WallBump, posDataWally)*_DataBumpD.z  + tex2D(_WallBump, posWallScaleDy)*_DataBumpD.w )*0.5, _DataWall.w);
				//bumpZ = UnpackScaleNormal((tex2D(_WallBump, posDataWallz)*_DataBumpD.z  + tex2D(_WallBump, posWallScaleDz)*_DataBumpD.w )*0.5, _DataWall.w);

				float det_lerp=saturate(1-IN.dist);

				bumpX = lerp( UnpackScaleNormal(  tex2D(_WallBump, posWallScaleDx)  *_DataBumpD.z, _DataWall.w), UnpackScaleNormal(  tex2D(_WallBump, posDataWallx)  *_DataBumpD.w, _DataWall.w), det_lerp);
				bumpY = lerp( UnpackScaleNormal(  tex2D(_WallBump, posWallScaleDy)  *_DataBumpD.z, _DataWall.w), UnpackScaleNormal(  tex2D(_WallBump, posDataWally)  *_DataBumpD.w, _DataWall.w), det_lerp);
				bumpZ = lerp( UnpackScaleNormal(  tex2D(_WallBump, posWallScaleDz)  *_DataBumpD.z, _DataWall.w), UnpackScaleNormal(  tex2D(_WallBump, posDataWallz)  *_DataBumpD.w, _DataWall.w), det_lerp);

				#if _TE_NOISE
					float3 bumpXN = lerp( UnpackScaleNormal(tex2D(_WallBumpN, posWallScaleDx)  *_DataBumpD.z, _DataWall.w), UnpackScaleNormal(tex2D(_WallBumpN, posDataWallx)  *_DataBumpD.w, _DataWall.w), det_lerp);
					float3 bumpYN = lerp( UnpackScaleNormal(tex2D(_WallBumpN, posWallScaleDy)  *_DataBumpD.z, _DataWall.w), UnpackScaleNormal(tex2D(_WallBumpN, posDataWally)  *_DataBumpD.w, _DataWall.w), det_lerp);
					float3 bumpZN = lerp( UnpackScaleNormal(tex2D(_WallBumpN, posWallScaleDz)  *_DataBumpD.z, _DataWall.w), UnpackScaleNormal(tex2D(_WallBumpN, posDataWallz)  *_DataBumpD.w, _DataWall.w), det_lerp);

					bumpX = lerp(bumpX,bumpXN, noisevalue);
					bumpY = lerp(bumpY,bumpYN, noisevalue);
					bumpZ = lerp(bumpZ,bumpZN, noisevalue);
				#endif
			#else
				bumpX = UnpackScaleNormal(tex2D(_WallBump, posDataWallx),_DataWall.w);
				bumpY = UnpackScaleNormal(tex2D(_WallBump, posDataWally),_DataWall.w);
				bumpZ = UnpackScaleNormal(tex2D(_WallBump, posDataWallz),_DataWall.w);

				#if _TE_NOISE
					float3 bumpXN = UnpackScaleNormal(tex2D(_WallBumpN, posDataWallx),_DataWall.w);
					float3 bumpYN = UnpackScaleNormal(tex2D(_WallBumpN, posDataWally),_DataWall.w);
					float3 bumpZN =  UnpackScaleNormal(tex2D(_WallBumpN, posDataWallz),_DataWall.w);

					bumpX = lerp(bumpX,bumpXN, noisevalue);
					bumpY = lerp(bumpY,bumpYN, noisevalue);
					bumpZ = lerp(bumpZ,bumpZN, noisevalue);
				#endif
			#endif
			
			#if _TE_SPLAT
				bumpTop = IN.color.r * UnpackScaleNormal(tex2D(_BumpR, posY*_DataR.x), _DataR.w)
				+ IN.color.g * UnpackScaleNormal(tex2D(_BumpG, posY*_DataG.x), _DataG.w)
				+ IN.color.b * UnpackScaleNormal(tex2D(_BumpB, posY*_DataB.x), _DataB.w)
				+ IN.color.a * UnpackScaleNormal(tex2D(_BumpA, posY*_DataA.x), _DataA.w);
			#else

				#if _TE_DISTANT
					bumpTop = UnpackScaleNormal((tex2D(_BumpR, posY*_DataR.x)*_DataBumpD.x +tex2D(_BumpR, posY*_FloorScaleD)*_DataBumpD.y)*0.5, _DataR.w);

					#if _TE_NOISE
						fixed3 bumpTopNoise = UnpackScaleNormal((tex2D(_BumpRN, posY*_DataRN.x)*_DataBumpD.x +tex2D(_BumpRN, posY*_FloorScaleD)*_DataBumpD.y)*0.5, _DataRN.w);
						bumpTop = lerp(bumpTop,bumpTopNoise,noisevalue);
					#endif
				#else
					bumpTop = UnpackScaleNormal(tex2D(_BumpR, posY*_DataR.x), _DataR.w);

					#if _TE_NOISE
						fixed3 bumpTopNoise = UnpackScaleNormal(tex2D(_BumpRN, posY*_DataRN.x), _DataRN.w);
						bumpTop = lerp(bumpTop,bumpTopNoise,noisevalue);
					#endif
				#endif
			#endif


			weightedPowerNormal.y+=_DataN.w*(abs(o.Normal.y)-(bumpX * IN.powerNormal.x)-(bumpZ * IN.powerNormal.z))*0.005;
			topLerp+=_DataN.w*(abs(o.Normal.y)-(bumpX * IN.powerNormal.x)-(bumpZ * IN.powerNormal.z));
			topLerp = min(1,topLerp);
			topLerp = max(0,topLerp);

			bumpTop = lerp(bumpY, bumpTop, topLerp);
			bumpTop = lerp(bumpY, bumpTop, topBottomLerp);

			o.Normal = normalize( 
			    lerp(bumpX * IN.powerNormal.x, bumpTop * weightedPowerNormal.x, weightedPowerNormal.y)
			  + lerp(bumpY * IN.powerNormal.y, bumpTop * weightedPowerNormal.y, weightedPowerNormal.y)
			  + lerp(bumpZ * IN.powerNormal.z, bumpTop * weightedPowerNormal.z, weightedPowerNormal.y)
			  );
						
			// DIFFUSE
			float3 texX,texY,texZ,texTop,tex;

			#if _TE_DISTANT
				det_lerp=saturate(1-IN.dist);

				texX = lerp( tex2D(_Wall, posWallScaleDx), tex2D(_Wall, posDataWallx), det_lerp) * _ColorPowerWall * _WallColor.r;
				texY = lerp( tex2D(_Wall, posWallScaleDy), tex2D(_Wall, posDataWally), det_lerp) * _ColorPowerWall * _WallColor.g;
				texZ = lerp( tex2D(_Wall, posWallScaleDz), tex2D(_Wall, posDataWallz), det_lerp) * _ColorPowerWall * _WallColor.b;

				#if _TE_NOISE
					float3 texXN = lerp( tex2D(_WallN, posWallScaleDx), tex2D(_WallN, posDataWallx), det_lerp) * _ColorPowerWall * _WallColor.r;
					float3 texYN = lerp( tex2D(_WallN, posWallScaleDy), tex2D(_WallN, posDataWally), det_lerp) * _ColorPowerWall * _WallColor.g;
					float3 texZN = lerp( tex2D(_WallN, posWallScaleDz), tex2D(_WallN, posDataWallz), det_lerp) * _ColorPowerWall * _WallColor.b;

					texX = lerp(texX,texXN, noisevalue);
					texY = lerp(texY,texYN, noisevalue);
					texZ = lerp(texZ,texZN, noisevalue);
				#endif
		
			#else
				texX = tex2D(_Wall, posDataWallx) * _WallColor* _ColorPowerWall;
				texY = tex2D(_Wall, posDataWally) * _WallColor* _ColorPowerWall;
				texZ = tex2D(_Wall, posDataWallz) * _WallColor* _ColorPowerWall;
			#endif

			#if _TE_SPLAT
				texTop = IN.color.r * tex2D(_FloorR, posY*_DataR.x) * _FloorColorR * _ColorPowerR
				+ IN.color.g * tex2D(_FloorG, posY*_DataG.x) * _FloorColorG * _ColorPowerG
				+ IN.color.b * tex2D(_FloorB, posY*_DataB.x) * _FloorColorB * _ColorPowerB
				+ IN.color.a * tex2D(_FloorA, posY*_DataA.x) * _FloorColorA * _ColorPowerA;
			#else
				#if _TE_DISTANT
					texTop = (tex2D(_FloorR, posY*_DataR.x) * tex2D(_FloorR, posY * _FloorScaleD)) * _FloorColorR*4 * _ColorPowerR;

					#if _TE_NOISE
						fixed4 texNoiseTop = (tex2D(_FloorRN, posY*_DataRN.x) * tex2D(_FloorRN, posY * _FloorScaleD)) * _ColorPowerN;
						texNoiseTop*=_FloorColorN*4;
						texTop = lerp(texTop,texNoiseTop,noisevalue);
					#endif
				#else
					texTop = tex2D(_FloorR, posY*_DataR.x)  * _FloorColorR * 4 * _ColorPowerR;

					#if _TE_NOISE
						fixed4 texNoiseTop = tex2D(_FloorRN, posY*_DataRN.x) * _FloorColorN * _ColorPowerRN;
						texTop = lerp(texTop,texNoiseTop,noisevalue);
					#endif
				#endif
			#endif


			texTop = lerp(texY, texTop, topLerp);
			texTop = lerp(texY, texTop, topBottomLerp);
			
			tex = 
			    lerp(texX * IN.powerNormal.x, texTop * weightedPowerNormal.x, weightedPowerNormal.y)
			  + lerp(texY * IN.powerNormal.y, texTop * weightedPowerNormal.y, weightedPowerNormal.y)
			  + lerp(texZ * IN.powerNormal.z, texTop * weightedPowerNormal.z, weightedPowerNormal.y)
			;

			#if _TE_VERTICAL
					float2 vert_tex_uv=float2(0, (pos.y)*_DataV.x);
					float4 vert_tex= (tex2D(_TextureV, vert_tex_uv) + tex2D(_TextureV, float2(0, (pos.y)*_DataV.y))) *0.5;

				#if _TE_NOISE
					tex = lerp(tex, (tex*vert_tex)*2, 
					( 
						((_PowerV * _DataNV.x) + _DataNV.y) + 
						((noisevalue*_DataNV.z) + _DataNV.w)
					)
					 * 1/(_DataNV.x+_DataNV.z))
					 ;
				#else
					tex = lerp(tex, (tex+vert_tex)*0.5,_PowerV);
				#endif
			#endif
				
			o.Albedo = max(fixed3(0.001, 0.001, 0.001), tex.rgb);
			o.Alpha = 0;

			// METALLIC/GLOSS
			//
				
			fixed2 mgX = fixed2(_DataWall.z, _DataWall.y),mgTop;

			#if _TE_SPLAT
				mgTop = fixed2(_DataR.z, _DataR.y) * IN.color.r +
				fixed2(_DataG.z, _DataG.y) * IN.color.g +
				fixed2(_DataB.z, _DataB.y) * IN.color.b +
				fixed2(_DataA.z, _DataA.y) * IN.color.a;
				//o.Albedo = IN.color;
			#else
				mgTop.x = _DataR.z;
				mgTop.y = _DataR.y;

				#if _TE_NOISE
					mgTop = lerp(mgTop,fixed2(_DataRN.z, _DataRN.y),noisevalue);
				#endif
			#endif

			mgTop = lerp(mgX, mgTop, topLerp);
			mgTop = lerp(mgX, mgTop, topBottomLerp);
				
			/*mgTop = 
			    lerp(mgX * IN.powerNormal.x, mgTop * weightedPowerNormal.x, weightedPowerNormal.y)
			  + lerp(mgX * IN.powerNormal.y, mgTop * weightedPowerNormal.y, weightedPowerNormal.y)
			  + lerp(mgX * IN.powerNormal.z, mgTop * weightedPowerNormal.z, weightedPowerNormal.y)
			;*/

			o.Metallic = mgTop.x;
			o.Smoothness = mgTop.y;
		}

		ENDCG
	}
	FallBack "Diffuse"
	CustomEditor "StandardTriplanarGUI"
}
