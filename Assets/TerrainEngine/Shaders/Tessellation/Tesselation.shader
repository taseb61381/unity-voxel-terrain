﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Standard Triplanar (Tesselation)" {
        Properties {
                _Color ("Color", Color) = (1,1,1,1)
                _MainTex ("Albedo (RGB)", 2D) = "white" {}
                _BumpMap ("Normal Map (RGB)", 2D) = "bump" {}
				_Occlusion ("Occlusion Map (R)" , 2D) = "white" {}
                _ParallaxMap ("Heightmap (A)", 2D) = "black" {}
				_NormalPower ("Normal Power", Float) = 1
				_Tess ("Tessellation", Range(1,32)) = 4

				_Height ("Height", Float) = 0.2
				_Scale ("Scale", Float) = 0.2
 
        }
        SubShader {
                Tags { "RenderType"="Opaque" }
                LOD 200
               
                CGPROGRAM
                // Physically based Standard lighting model, and enable shadows on all light types
               
                #pragma surface surf Standard addshadow vertex:disp tessellate:tessEdge nolightmap
                #include "Tessellation.cginc"

				fixed4 _Color;
                sampler2D _ParallaxMap;
                sampler2D _MainTex;
				sampler2D _Occlusion;
                sampler2D _BumpMap;
				float _Scale;
				float _Height;
				float _NormalPower;
				float _Tess;

                struct appdata {
                        float4 vertex : POSITION;
						float4 tangent : TANGENT;
                        float3 normal : NORMAL;

                };
               
                float4 tessEdge (appdata v0, appdata v1,appdata v2)
                {
					return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, 0, 40, _Tess);
                    //return UnityEdgeLengthBasedTessCull (v0.vertex, v1.vertex, v2.vertex, _EdgeLength, _Height * 1.5f);
                }      
       
                void disp (inout appdata v)
                {
					float3 worldNormal = mul( unity_ObjectToWorld, float4( v.normal, 0.0 ) ).xyz;
					float3 worldPos = mul (unity_ObjectToWorld, v.vertex);

					float3 blendingWeights = abs(worldNormal);
					blendingWeights = (blendingWeights - 0.2) * 7; 
					blendingWeights = max(blendingWeights, 0);
					blendingWeights /= (blendingWeights.x + blendingWeights.y + blendingWeights.z ).xxx; 

					float4 col1 = tex2Dlod(_ParallaxMap, float4(-worldPos.zy,0,0) * _Scale);
					float4 col2 = tex2Dlod(_ParallaxMap, float4(worldPos.zx,0,0) * _Scale);
					float4 col3 = tex2Dlod(_ParallaxMap, float4(-worldPos.x, worldPos.y,0,0) * _Scale);

					float4 blendedColor = col1.xyzw * blendingWeights.xxxx + 
					col2.xyzw * blendingWeights.yyyy + 
					col3.xyzw * blendingWeights.zzzz;
					v.vertex.xyz += v.normal * blendedColor.x * _Height;
                }

				struct Input
				{
					float3 uv_Texture;
					float3 worldPos;
					float3 worldNormal;
					INTERNAL_DATA
				};
 
                void surf (Input IN, inout SurfaceOutputStandard o) 
				{
					float3 blendingWeights = abs( normalize(IN.worldNormal));
					blendingWeights = (blendingWeights - 0.2) * 7; 
					blendingWeights = max(blendingWeights, 0);
					blendingWeights /= (blendingWeights.x + blendingWeights.y + blendingWeights.z ).xxx; 

					float4 blendedColor;
					float3 blendedNormal;
					float blendedSpecular;

					float2 coord1 = IN.worldPos.zy * -_Scale;
					float2 coord2 = IN.worldPos.zx * _Scale;
					float2 coord3 = float2(IN.worldPos.x, -IN.worldPos.y) * -_Scale;
			
					float4 col1 = tex2D(_MainTex, coord1);
					float4 col2 = tex2D(_MainTex, coord2);
					float4 col3 = tex2D(_MainTex, coord3);
			
			
					blendedColor = col1.xyzw * blendingWeights.xxxx + 
					col2.xyzw * blendingWeights.yyyy + 
					col3.xyzw * blendingWeights.zzzz;

					// TEXTURE
					// Albedo comes from a texture tinted by color
					fixed4 c = blendedColor * _Color;
					o.Albedo = c.rgb;

					// Occlusion
					//
					col1 = tex2D(_Occlusion, coord1);
					col2 = tex2D(_Occlusion, coord2);
					col3 = tex2D(_Occlusion, coord3);

					blendedColor = col1.xyzw * blendingWeights.xxxx + 
					col2.xyzw * blendingWeights.yyyy + 
					col3.xyzw * blendingWeights.zzzz;
					o.Occlusion = blendedColor*blendedColor*blendedColor*blendedColor;

					// Normal
					//
					//col1 = tex2D(_BumpMap, coord1);
					//col2 = tex2D(_BumpMap, coord2);
					//col3 = tex2D(_BumpMap, coord3);

					//blendedColor = col1.xyzw * blendingWeights.xxxx + 
					//col2.xyzw * blendingWeights.yyyy + 
					//col3.xyzw * blendingWeights.zzzz;
					//o.Normal = blendedColor;
                }
                ENDCG
        }
        FallBack "Diffuse"
}