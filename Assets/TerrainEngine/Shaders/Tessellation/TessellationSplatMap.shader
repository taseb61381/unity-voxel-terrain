﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Standard (Tesselation Triplanar SplatMap)" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Tess ("Tessellation", Range(1,32)) = 8

		_ColorR ("ColorR (RGB)(A)Occlusion", 2D) = "white" {}
		_ColorG ("ColorG (RGB)(A)Occlusion", 2D) = "white" {}
		_ColorB ("ColorB (RGB)(A)Occlusion", 2D) = "white" {}
		_ColorA ("ColorA (RGB)(A)Occlusion", 2D) = "white" {}
		_Wall ("Wall (RGB)(A)Occlusion", 2D) = "white" {}

		_HeightMap ("HeightMap (RGBA)", 2D) = "white" {}
		_WallHeightMap ("Wall HeightMap (A)", 2D) = "white" {}

		_Height ("Height", Float) = 0.2
		_Scale ("Scale", Float) = 0.2

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		
        #pragma surface surf Standard addshadow vertex:disp tessellate:tessDist nolightmap
        #include "Tessellation.cginc"

		sampler2D _ColorR,_ColorG,_ColorB,_ColorA,_Wall;
		sampler2D _HeightMap,_WallHeightMap;
		float _Scale,_Height,_Tess;
		
		struct appdata {
			float4 vertex : POSITION;
			float4 tangent : TANGENT;
			float3 normal : NORMAL;
			float4 color : COLOR;
			float2 texcoord : TEXCOORD0;
		};
		
		float4 tessDist (appdata v0, appdata v1,appdata v2)
		{
			return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, 0, 25, _Tess);
		}	
	
		void disp (inout appdata v)
		{
			float3 worldNormal = mul( unity_ObjectToWorld, float4( v.normal, 0.0 ) ).xyz;
			float3 worldPos = mul (unity_ObjectToWorld, v.vertex);

			float3 blendingWeights = abs(worldNormal);
			blendingWeights = (blendingWeights - 0.2) * 7; 
			blendingWeights = max(blendingWeights, 0);
			blendingWeights /= (blendingWeights.x + blendingWeights.y + blendingWeights.z ).xxx; 

			float4 col1 = tex2Dlod(_WallHeightMap, float4(-worldPos.zy,0,0) * _Scale);
			float4 col2 = tex2Dlod(_HeightMap, float4(worldPos.zx,0,0) * _Scale);
			float4 col3 = tex2Dlod(_WallHeightMap, float4(-worldPos.x, worldPos.y,0,0) * _Scale);

			col2.r = (col2.r * v.color.r) + (col2.g* v.color.g) + (col2.b* v.color.b) + (col2.a* v.color.a);


			float4 blendedColor = col1.xyzw * blendingWeights.xxxx + 
			col2.xyzw * blendingWeights.yyyy + 
			col3.xyzw * blendingWeights.zzzz;
			v.vertex.xyz += v.normal * blendedColor.x * _Height;
		}

		struct Input
		{
			float3 uv_Texture;
			float3 worldPos;
			float3 worldNormal;
			float4 color : COLOR;
		};

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			// DIFFUSE
			float3 blendingWeights = abs( normalize(IN.worldNormal));
			blendingWeights = (blendingWeights - 0.2) * 7; 
			blendingWeights = max(blendingWeights, 0);
			blendingWeights /= (blendingWeights.x + blendingWeights.y + blendingWeights.z ).xxx; 

			float2 coord2 = IN.worldPos.zx * _Scale;
			
			float4 col1 = tex2D(_Wall,IN.worldPos.zy * -_Scale);
			float4 col2 = tex2D(_ColorR, coord2) * IN.color.r 
			+ tex2D(_ColorG, coord2) * IN.color.g 
			+ tex2D(_ColorB, coord2) * IN.color.b 
			+ tex2D(_ColorA, coord2) * IN.color.a;


			float4 col3 = tex2D(_Wall,float2(IN.worldPos.x, -IN.worldPos.y) * -_Scale);
			
			
			float4 blendedColor = col1.xyzw * blendingWeights.xxxx + 
			col2.xyzw * blendingWeights.yyyy + 
			col3.xyzw * blendingWeights.zzzz;
			o.Albedo = blendedColor.rgb;
			o.Occlusion = blendedColor.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}