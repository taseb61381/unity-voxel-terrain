Shader "Transparent/VertexLit with Z" {

Properties {

    _Color ("Main Color", Color) = (1,1,1,1)

    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}

}

 

SubShader {

    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}

    // Render into depth buffer only

    Pass {

        Blend SrcAlpha OneMinusSrcAlpha

        ColorMask RGB
        Cull Off

        Material {

            Diffuse [_Color]

            Ambient [_Color]

        }

        Lighting On

        SetTexture [_MainTex] {

            Combine texture * primary DOUBLE, texture * primary

        } 

    }

 

}

}