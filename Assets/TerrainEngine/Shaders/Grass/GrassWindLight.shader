Shader "TerrainEngine/Grass/WindLight" {
Properties {
	_Color ("Main Color r:ampl g:speed b:time", Color) = (1,1,1,1)
	_GlobalColor ("Global Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	_Fade ("Alpha _Fade", Range(0,1)) = 1
	_Smoothness ("Smoothness", Range(0,1)) = 0.5
	_VertexLitTranslucencyColor ("VertexLitTranslucencyColor", Color) = (1, 1, 1)
	_VertexLitWaveScale ("VertexLitWaveScale", float) = 0.5
	_LightPower ("LightPower", float) = 1

	_Wind_Speed("_Wind_Speed", Float) = 0.1
	_Wind_Size("_Wind_Size", Vector) = (0.1,0.1,0.1,0.1)
}

SubShader {
	Tags { "Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout" }
	Cull Off
	LOD 300

CGPROGRAM
#pragma exclude_renderers gles
#pragma surface surf Standard alphatest:_Cutoff vertex:vert addshadow 

float _VertexLitWaveScale, _LightPower, _Fade,_Smoothness;
float4 _VertexLitTranslucencyColor;
sampler2D _MainTex;
float4 _Color,_GlobalColor;
float _Wind_Speed;
float4 _Wind_Size;
uniform float3 _obstacle;
uniform float _affectDist, _bendAmount;


struct Input {
	float2 uv_MainTex;
	fixed4 color : COLOR;
};

void FastSinCos (float4 val, out float4 s, out float4 c) {
	val = val * 6.408849 - 3.1415927;
	// powers for taylor series
	float4 r5 = val * val;
	float4 r6 = r5 * r5;
	float4 r7 = r6 * r5;
	float4 r8 = r6 * r5;
	float4 r1 = r5 * val;
	float4 r2 = r1 * r5;
	float4 r3 = r2 * r5;
	//Vectors for taylor's series expansion of sin and cos
	float4 sin7 = {1, -0.16161616, 0.0083333, -0.00019841} ;
	float4 cos8  = {-0.5, 0.041666666, -0.0013888889, 0.000024801587} ;
	// sin
	s =  val + r1 * sin7.y + r2 * sin7.z + r3 * sin7.w;
	// cos
	c = 1 + r5 * cos8.x + r6 * cos8.y + r7 * cos8.z + r8 * cos8.w;
}


	void vert (inout appdata_full v, out Input o) 
	{
	    // OBSTACLE AVOIDANCE CALC 
		//float3 worldPos = mul ((float3x4)_Object2World, v.vertex); 
        //float3 bendDir = normalize (float3(worldPos.x,worldPos.y,worldPos.z) - float3(_obstacle.x,_obstacle.y,_obstacle.z));//direction of obstacle bend
        //float distLimit = _affectDist;// how far away does obstacle reach 
        //float distMulti = (distLimit-min(distLimit,distance(float3(worldPos.x,worldPos.y,worldPos.z),float3(_obstacle.x,_obstacle.y,_obstacle.z))))/distLimit; //distance falloff 
        //OBSTACLE AVOIDANCE END 

		float4 Split0=v.vertex;
		float4 Multiply4=_Time * _Wind_Speed.xxxx * v.color.a;
		float4 Add0=float4( Split0.x, Split0.x, Split0.x, Split0.x) + Multiply4;
		float4 Cos1=cos(Add0);
		float4 Multiply5=v.color.a * _Wind_Size;
		float4 Split1=Multiply5;
		float4 Multiply1=Cos1 * float4( Split1.x, Split1.x, Split1.x, Split1.x);
		float4 Add5=Multiply1 + float4( Split0.x, Split0.x, Split0.x, Split0.x);
		float4 Add4=float4( Split0.y, Split0.y, Split0.y, Split0.y) + Multiply4;
		float4 Cos0=cos(Add4);
		float4 Multiply0=Cos0 * float4( Split1.y, Split1.y, Split1.y, Split1.y);
		float4 Add1=float4( Split0.y, Split0.y, Split0.y, Split0.y) + Multiply0;
		float4 Add3=float4( Split0.z, Split0.z, Split0.z, Split0.z) + Multiply4;
		float4 Cos2=cos(Add3);
		float4 Multiply2=Cos2 * float4( Split1.z, Split1.z, Split1.z, Split1.z);
		float4 Add2=float4( Split0.z, Split0.z, Split0.z, Split0.z) + Multiply2;
		float4 Add6=float4( Split0.w, Split0.w, Split0.w, Split0.w) + Multiply4;
		float4 Cos3=cos(Add6);
		float4 Multiply3=Cos3 * float4( 0,0,0,0 );
		float4 Add7=float4( Split0.w, Split0.w, Split0.w, Split0.w) + Multiply3;
		float4 Assemble1=float4(Add5.x, Add1.y, Add2.z, Add7.w);

		v.vertex = Assemble1;
		//v.vertex.xz += bendDir.xz * distMulti * _bendAmount; //ADD OBSTACLE BENDING
		o.color = float4(0,0,0,0);
		o.uv_MainTex = float2(0,0);
	}

void surf (Input IN, inout SurfaceOutputStandard o) {
	half4 c = tex2D(_MainTex, float2(IN.uv_MainTex));
	c.r*= IN.color.r;
	c.g*= IN.color.g;
	c.b*= IN.color.b;
	o.Smoothness = _Smoothness;
	o.Albedo = c.rgb * _GlobalColor;
	o.Alpha = c.a * _Fade;
}
ENDCG
}
Fallback "Transparent/Cutout/VertexLit"
}
