// Upgrade NOTE: replaced 'SeperateSpecular' with 'SeparateSpecular'

Shader "TerrainEngine/Transparent/VertexColor" 
{
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _SpecColor ("Spec Color", Color) = (0,0,0,0)
    _Emission ("Emmisive Color", Color) = (0,0,0,0)
    _Shininess ("Shininess", Range (0.01, 1)) = 1.0
    _MainTex ("Base (RGB)", 2D) = "white" {}
}

SubShader 
{
    Pass 
	{
        Material 
		{
            Shininess [_Shininess]
            Specular [_SpecColor]
            Emission [_Emission]    
        }

		Alphatest Greater 1
        ColorMaterial AmbientAndDiffuse
		Blend SrcAlpha OneMinusSrcAlpha
        Lighting On
        SeparateSpecular On

        SetTexture [_MainTex] 
		{
            Combine texture * primary, texture * primary
        }

        SetTexture [_MainTex] 
		{
            constantColor [_Color]
            Combine previous * constant DOUBLE, previous * constant
        } 
    }
}

Fallback " VertexLit", 1
}