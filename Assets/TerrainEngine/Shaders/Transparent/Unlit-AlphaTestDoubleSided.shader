// Unlit alpha-cutout shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "TerrainEngine/Unlit/Transparent Cutout Double Sided" 
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_Emission ("Emissive Color", Color) = (0,0,0,0)
	}

	SubShader 
	{
		Tags {"Queue"="Transparent-99" "IgnoreProjector"="True" "RenderType"="Transparent"}
		LOD 200


		Cull Off
		ZWrite On
		Lighting On
		Blend SrcAlpha OneMinusSrcAlpha

		Pass 
		{
			Material 
			{
				Diffuse [_Color]
				Ambient [_Color]
				Emission [_Emission]
			}

			Alphatest Greater [_Cutoff]
			SetTexture [_MainTex] 
			{ 
				 Combine texture * primary DOUBLE, texture * primary
			}
		}
	}

Fallback "Transparent/Diffuse"
}