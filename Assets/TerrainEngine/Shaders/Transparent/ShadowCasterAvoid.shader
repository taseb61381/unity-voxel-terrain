﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "TerrainEngine/Transparent/TransparentShadowCasterAvoid" 
{ 

Properties
    {
            _Color ("Main Color", Color) = (1,1,1,1)
            _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
    }
 
    SubShader
    {
            Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="Opaque"}
            blend SrcAlpha OneMinusSrcAlpha
           
            LOD 200
			CGPROGRAM
			#pragma exclude_renderers gles
            #pragma surface surf Lambert vertex:vert exclude_path:prepass
           
            sampler2D _MainTex;
            fixed4 _Color;
			uniform float3 _obstacle;
			uniform float _affectDist, _bendAmount;

            struct Input
            {
                    float2 uv_MainTex;
            };
           
		   	void vert (inout appdata_full v, out Input o) 
			{
				UNITY_INITIALIZE_OUTPUT(Input,o);

				// OBSTACLE AVOIDANCE CALC 
				float3 worldPos = mul ((float3x4)unity_ObjectToWorld, v.vertex); 
				float3 bendDir = normalize (float3(worldPos.x,worldPos.y,worldPos.z) - float3(_obstacle.x,_obstacle.y-2,_obstacle.z));//direction of obstacle bend
				float distLimit = _affectDist;// how far away does obstacle reach 
				float distMulti = (distLimit-min(distLimit,distance(float3(worldPos.x,worldPos.y,worldPos.z),float3(_obstacle.x,_obstacle.y,_obstacle.z))))/distLimit; //distance falloff 
				//OBSTACLE AVOIDANCE END

				v.vertex.xz += bendDir.xz * distMulti * _bendAmount * float3(1,1.5,1); //ADD OBSTACLE BENDING 
			}

            void surf (Input IN, inout SurfaceOutput o)
            {
                fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
                o.Albedo = c.rgb;
                o.Alpha = c.a;
            }
            ENDCG
    }
Fallback "VertexLit"
}