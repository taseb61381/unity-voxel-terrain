Shader "TerrainEngine/Transparent/LOD" {

Properties {
   _Color ("Main Color", Color) = (1,1,1,1)
   _Emissive ("Emissive", Color) = (0.5,0.5,0.5,0)
   _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
   _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5

	_Power ("Subsurface Power", Float) = 1.0
	_Distortion ("Subsurface Distortion", Float) = 0.0
	_Scale ("Subsurface Scale", Float) = 0.5
	_SubColor ("Subsurface Color", Color) = (1.0, 1.0, 1.0, 1.0)
}

SubShader {

     Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="AlphaTest"}

	 ZWrite On
CGPROGRAM

#pragma surface surf Translucent alphatest:_Cutoff 
sampler2D _MainTex;
fixed4 _Color,_Emissive;

float _Scale, _Power, _Distortion;
fixed4 _SubColor;

struct Input {
   float2 uv_MainTex;
};

	void surf (Input IN, inout SurfaceOutput o) 
	{
	   fixed4 c = (tex2D(_MainTex, IN.uv_MainTex) * _Color) + _Emissive;
	   o.Albedo = c.rgb;
	   o.Alpha = c.a;
	}

	inline fixed4 LightingTranslucent (SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
		{		
		fixed diff = max (0, dot (s.Normal, lightDir)) * 4;
	
		fixed4 c;
		c.rgb = s.Albedo * _LightColor0.rgb * diff * atten;
		c.a = saturate(s.Alpha);
		return c;
		}

 ENDCG
 }
 
 Fallback "Transparent/Cutout/VertexLit"
 }
