General :
The world is managed by an : TerrainManager.
An TerrainBlock is a block of terrain that contains X*Y*Z Chunks.
A Chunk is a part of terrain that contain X*Y*Z voxels.
A TerrainBlock contains all voxels of chunks that it manage.
TerrainManager contains all TerrainBlock.

For change the size of terrain you must go on the TerrainManager GameObject in Informations Variable.
-MinRange / MaxRange , are the number of TerrainBlock loaded around the player (distance of view)
-VoxelPerChunk : Number of voxel for one chunks
-ChunkCount : Number of chunk per block.
So , for 8*8*8 chunk of 16*16*16 voxel. One block will contain 8*8*8*16*16*16 voxels.
-ChunkPrefab : The prefab object that will contain the mesh. It need a meshcollider,meshfilter and MeshRenderer.
-OptimiseBlockBorder : True, remove block invisible borders at extremity of terrain.

TerrainEngine is made : Client/Server
Folder : TerrainEngine/Shared/ contains all classes that can be exported on net project and compiled without unity.
TerrainWorldServer.cs containt systems to generate , send, save and load blocks.

FileMgr/ActionProcessor are first Objects that must be enabled
When FileMgr is Started, it enable the terrain for the loading.

TerrainManager contains all subsystems in childs gameobjects :
-Terrain generation algo
-Grass
-Details
-Trees
-Etc..

OnFirstLoadingProcessor is used for enable/disable/instanciate/delete any object when the terrain is loaded.
Example :
-Remove loading screen
-Spawn Player
-Active Camera
-Etc...

LocalTerrainObjectServerScript is the class that load terrain around an object (here, the player).
You can fixe positions for the loading.
FixedPosition = (0,100,0). Player height will be overrited by 100 and terrain will be only loaded around Y = 100.
That will not load underground caves or upper terrain when player will go on top of mountains of fall underground.
Set FixedPosition.y to 0 if you want load in all axis around the player (for infinite caves/mountains)

ColliderProcessor is the class that manage all colliders of the terrain. Colliders are not all generated at start but only when needed.
If there is a TerrainObject around a chunk, the collider will be generated.
-GenerateAllColliders : True if you want all colliders at game start (usefull for terrain edition)
-DrawChunkDistance : Distance around an object that need to have a collider ( default 1. Set more if you have big objects like big monster that collider touch more than one chunk when grounded)
-Collider Update Interval : Time for update a collider that is modified by an Modificator*. 

Palette : A palette is a class that contain all voxel materials/name/type.
Marching Cubes :
4-ForestPalette is the defaut palette.
A voxel type contain 3 variables :
-Name
-VoxelType : Opaque/Translucent/Fluid
-Material : The material for the current type.

Cubic : 
-Size/Top/Bottom : Textures to use

Any object that use the terrain must have a TerrainObject class.(Players,Monsters,Camera,Any physic object)
TerrainObject is the class that contain all systems for send events to Processors.
A TerrainObject must have special script to work with certain processors :
-TerrainObjectColliderViewer : This script will update terrain chunk colliders around.
-TerrainDetailViewer : This script will draw details mesh around this object ( Generaly on Camera )
-TerrainGrassViewer : This script will draw grass mesh around this object ( Generaly on Camera )
-TerrainGroundAligner : Teleport this object on ground. (usefull for teleport player at first loading when the ground height is not know)
A camera need to have a TerrainObject class for be able to update grass/plants/trees around.
But a camera don"t need to generate colliders around , so you don"t need to enable "UseTerrainColliders". Except if you use tools , that need raycast.

The terrain works with Plugins called "Processors". You can manage all the terrain with only one class : IBlockProcessor.
Processors must be as child gameobject of TerrainManager.
For manage an event , example , set a block indestructible, create a new class like that :
public class IndestructibleBlocks : IBlockProcessor
{
	 // And override an function
	public override CanModifyVoxel(byte Type,etc...)
	{
		if(Type == UndestructibleType)
			return false;
	return true;
	}
}

GameObject/Grass/Details Processors :
They all need a container that will contains gameobjects/textures/MEsh :
-GameObjectContainer.cs : For GameObjects (like trees)
-DetailsContainer.cs : Contains only MeshFilter
-GrassContainer.cs : Contains only Grass Texture.

AdvancedDetails/AdvancedGrass Processor :
-DrawDistance : Witch Chunk distance the Mesh will be draw. Be carefull , it's chunk distance and not metter distance. If set to 2, it will draw mesh 2 chunks around (x/y/z) the camera.
-Container : Link to the gameobject that contain all Objects.

AdvancedGameObjectProcessor : (TreesProcessor)
-DataName : Name of the storage, do not change it if you're using a saved terrain.
-Any Objects can have an LOD. The LOD is multithreaded and use distance from camera. Add an new object , click on Active LOD.
If LOD is active , after "Lod Start Distance" value , the object will be remove if there are no next LOD or changed by the next.
Click on New LOD if you want add a new LOD. There are 3 different types :
-GameObject : The original object will be changed by the new.
-Plane/Cross Plane : A Plane is generated and use the material passed. If you enable Procedural Texture variable , a screenshot of the original gameobject will be take at start and used as texture for the plane. It's the default use for the LOD , and very usefull for trees.

When you've created a new GameObject, add AdvancedGameObjectProcessor.cs for example. Now you must link script that contains all Objects
Add GameObjectsContainer.cs to a new gameObject and drag the script into Container value on AdvancedGameObjectProcessor script.
Now you can Click on NEW button and start to add your objects.
Chance : Chance to be spawned, default 1
Ground Side : Bottom is a ground object, TOP is object spawning under ground. For example under flying islands or on caves for climbing plants
Scale Min / Max :  Random scale, check enable box to enable it
Orientation Min / Max : Random Orientation



IModificator : "TerrainEngine/Terrain/Modificator"
This class is used for modify the terrain in realtime and easily. Usefull for create spells, skills,bombs, etc..
Drag and drop to the scene an prefab like : CubeModificator.
It will modify the terrain. Variables are :
Size, Width,Height,Depth : Size of the modification
Type : Type of voxel. 
ModType : If the modificator will add/remove volume , change the type of voxel,etc...
Count : Number of execution. If you remove 0.2 and count = 5. It will remove a complete voxel volume in 5 times.

ActionProcessor : Multi-Threading.
This class manage all Threads, the FPS and the Pooling
-Max Thread Count : Set to 0 for use all availables core of the machine. Set number for use the number set. (if you set 16, it will create 16 thread even if you have only 2 CPU, be carefull)
-Thread Sleep MSTime : Threads Sleep time. Modify the % of use of the processor. Bigger value will let the cpu do other things and use less %
MaxThreaded Action Time : Max MS time for each action. Same as previous.

How to use :
ActionProcessor.AddToUnity(IAction); // Add an action to unity, and it will delaye it for keep a good FPS. Usefull for instanciate gameobjects
ActionProcessor.AddToThread(IAction); // Add an action to another thread. Used for generating chunks or distance for LOD
ActionProcessor.AddToFastThread(IAction); // Fast Thread is a thread used for fastest action that take small time. Like , generate chunks , check occlusion or any distance function. Be carefull , don't pass too big action to this thread , it can make lag
ActionProcessor.Add(IAction,ThreadID); // Select the thread for an action. -1 = Unity. Int.max will select the better thread.


PathFinding :  Terrain/Core/PathFinder/TerrainPathFinder.cs
The pathfinding class is very simple to use.It provide you a list of position to use for go from point A to B. Must be used like that :
PathInformations Info = new PathInformations();
Info.Init(TerrainBlock, WorldVoxel A, VoxelVoxel B); // You can get a WorldVoxel position to any TerrainObject.WorldVoxel. And Block TerrainObject.BlockObject
Info.Find();

You can take a look at PathFinderExample.cs for more details.



Create a new biome :

Create a new gameobject : 0-Ground. Add AdvancedHeightMap.cs script to it.
Create a new gameobject : BiomeNoises. Add script : BiomeNoisesContainer.cs

On AdvancedHeightMap script : link Container value to BiomeNoisesContainer script from BiomeNoises GameObject.

On BiomeNoisesContainer script, Create your noises. 
Example for a normal world we need noises :
Ground
Valley
Mountains

Ground noise
-Type : Turbulence
-Octave :  2
-Frequency : 0.005
-Lacuracity : 2
-Persistence : 1

Valley noise
-Type : Perlin
-Octave : 2
-Frequency : 0.002
-Lacuracity : 2
-Persistence : 1

Mountains
-Type : Ridged Multifractal
-Octaves : 4
-Frequency : 0.002
-Lacuracity : 2
-Persisence : 1.5

A noise return a float value between -1 and 1.

This value is used as height. So we need to scale them to fit to a correct world height.

For example, Max valley scale must be 8.  So we need to convert -1 to 1 range to 0 - 8.
Click on "Add Calculation" Button. This will add a new line calculation what is used to modify the result of the noise
So we need :
(result + 1f) * 4. This will return a value between 0 to 8

So we need 2 calculation : First
BiasOutput, that will add a value to the result.
Add a new PreCalculations :
Name : BiasOutput
Bias : 1

And now we need to scale the result, add another calculations
Name : ScaleOutput
Scale : 4


For mountains, we want for example, 0 to 50. Go to Mountains noise and add 2 new PreCalculations
Bias 1 + ScaleOutput 50.


So now , we need to combine this noises to create a world. 
There is a GUI for it, Click on TerrainEngine->Biome->Advanced Height Editor
A window will open, drag your 0-Ground gameobject on it.

On left you have 3 boxes : 
Noises
Presets
Preview

Preset are calculation between different noises. A preset can be a noise or a type of blending between 2 noises.
if your presets list is empty, Click on your noises on Noises Box (ground, valley and mountains) that will create presets from this noises.

The world is started with the Main Preset. You must have a Main Window with the type : Noise.

Now we need to add Ground to Valley and Result to Mountains.

So replace the Main preset noise type by : ADD.
You now have 2 new elements : PresetA and PresetB.
Select PresetA (it become blue when you click) and click on your Ground Preset (on Presets box)
Select PresetB and click on your Valley Preset (on Presets box)

Now Ground will be ADD to Valley.
But we need Valley + Mountains. 

So on Valley box, rename Valley by ValleyMountains, and change type to : ADD
So now on PresetA we need ValleyNoise, but we have just changed it by ValleyMountains, so for recreate a preset from noise , Click on PresetA, and After on Valley button on Noises box
This will create a new preset called : NoiseValley on Presets box.
Now Click again on PresetA and after on NoiseValley button on Presets box
Click on PresetB and click on Mountains Button on Presets box.