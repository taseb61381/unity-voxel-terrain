using UnityEngine;

namespace TerrainEngine
{
    public class InputManager : MonoBehaviour
    {
        public KeyCode KeyLockCursor = KeyCode.Escape;
        public KeyCode SlowTimeKey = KeyCode.LeftControl;
        public float SlowTimeSpeed = 0.4f;
        static public bool LockCursor;

        public float StartTime = 0;
        public float DesiredTime = 1f;

        void Update()
        {
            if (Input.GetKeyDown(KeyLockCursor))
                LockCursor = !LockCursor;

            if (Input.GetKey(SlowTimeKey))
            {
                if (DesiredTime == 1f)
                    StartTime = Time.realtimeSinceStartup;

                DesiredTime = SlowTimeSpeed;
            }
            else
            {
                if (DesiredTime != 1f)
                    StartTime = Time.realtimeSinceStartup;

                DesiredTime = 1f;
            }

            if (Time.timeScale != 1f)
                Time.timeScale = Mathf.Lerp(1f, DesiredTime, Mathf.Clamp(Time.realtimeSinceStartup - StartTime / 1f, 0f, 1f));

        }

        static public float GetAxis(string axisName)
        {
            if (LockCursor)
                return 0;

            return Input.GetAxis(axisName);
        }

        static public bool GetMouseButtonDown(int button)
        {
            if (LockCursor)
                return false;

            return Input.GetMouseButtonDown(button);
        }

        static public bool GetMouseButton(int button)
        {
            if (LockCursor)
                return false;

            return Input.GetMouseButton(button);
        }
    }

}