using System;
using TerrainEngine;
using UnityEngine;

public class ModifyTool : ITerrainModifier
{
    void OnGUI()
    {
        if (!DebugGUIProcessor.DrawGUI)
            return;

        GUI.Toggle(new Rect(Screen.width / 2 - 5, Screen.height / 2 - 5, 10, 10), true, "");

        if (Manager == null)
            return;

        if (DrawBlockTypes)
            ToolType = GUI.SelectionGrid(new Rect(0, 30, Screen.width, 60), ToolType, BlockNames, BlockNames.Length / 2);

        if (!DrawToolConfig)
            return;

        GUI.Label(new Rect(Screen.width - 200, 90, 200, 20), "Tool Size : " + ToolSize);
        ToolSize = (int)GUI.HorizontalSlider(new Rect(Screen.width - 200, 110, 200, 20), ToolSize, 0, 10);

        GUI.Label(new Rect(Screen.width - 200, 130, 200, 20), "Volume : " + ToolVolume);
        ToolVolume = GUI.HorizontalSlider(new Rect(Screen.width - 200, 150, 200, 20), ToolVolume, 0, 1f);

        Sphere = GUI.Toggle(new Rect(Screen.width - 200, 160, 200, 20), Sphere, "Sphere");
        InstantChunksGeneration = GUI.Toggle(new Rect(Screen.width - 200, 200, 200, 20), InstantChunksGeneration, "InstantChunksGeneration");
        InstantChunksBuild = GUI.Toggle(new Rect(Screen.width - 200, 180, 200, 20), InstantChunksBuild, "InstantChunksBuild");
        OnKeyDown = GUI.Toggle(new Rect(Screen.width - 200, 220, 200, 20), OnKeyDown, "OnKeyDown");

        Selection = GUI.SelectionGrid(new Rect(Screen.width - 200, 240, 200, 200), Selection, ModificationNames, 1);
        ModType = (VoxelModificationType)(ModificationValues[Selection]);
    
        if(GUI.Button(new Rect(Screen.width-200,460,200,20),"Make Cube"))
        {
            ToolVolume = 0.51f;
            ToolType = BlockManager.Stone;
            ModType = VoxelModificationType.SET_TYPE_SET_VOLUME;
            Selection = Array.Find(ModificationValues, (id => id == (int)ModType));
        }
    }

    void Update()
    {
        UpdateCursor();

        if (Manager == null || !Manager.enabled)
            return;

        if (ToEnableDisable != null && Input.GetKeyDown(KeyCode.Escape))
            ToEnableDisable.enabled = !ToEnableDisable.enabled;

        float Wheel = Input.GetAxis("Mouse ScrollWheel");

        if(Wheel != 0)
            CursorDistance += Wheel > 0 ? 1 : -1;

        if (Input.GetKeyDown(KeyCode.KeypadPlus)) ToolSize += 1;
        else if (Input.GetKeyDown(KeyCode.KeypadMinus)) ToolSize -= 1;

        if (OnKeyDown && !Input.anyKeyDown)
            return;

        if (new Rect(Screen.width-200, 0, 200, 500).Contains(new Vector2(Input.mousePosition.x,Screen.height-Input.mousePosition.y)))
            return;

        if (new Rect(0, 0, Screen.width, 60).Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
            return;

        bool Left = InputManager.GetMouseButton(0);
        bool Right = InputManager.GetMouseButton(1);

        if (Left || Right)
        {
            if (IsFlushing())
                return;

            Vector3 Position = new Vector3();
            TerrainChunkScript Script = Raycast(true, ref Position);
            TerrainBlock Block = null;
            if (Script != null && Script.ChunkRef.IsValid(ref Block))
                Modify(Block, Hit.point, Hit.normal, ToolSize, ToolSize, ToolSize, Right);
        }
	}

    public override Vector3 GetCursorScale()
    {
        CursorWidth = ToolSize;
        CursorHeight = ToolSize;
        CursorDepth = ToolSize;
        return base.GetCursorScale();
    }

    void LateUpdate()
    {
        UpdateCursor();
    }
}
