private var revertFogState = false;
public var HasFog = false;

function OnPreRender () {
    revertFogState = RenderSettings.fog;
    RenderSettings.fog = HasFog;
}
 
function OnPostRender () {
    RenderSettings.fog = revertFogState;
}
 
@script RequireComponent (Camera)