﻿using System;
using System.Collections.Generic;

using TerrainEngine;
using UnityEngine;

public class ModifyToolV2 : ITerrainModifier
{
    public enum GroundLayers
    {
        VOXEL = 0,
        PROCESSOR = 1,
    };

    public enum BrushTypes
    {
        CUSTOM_OBJECT = 0,
        PROCESSOR_OBJECT = 1,
        PLANE_BRUSH = 2,
        OBJECT_BRUSH = 3,
    };

    [Serializable]
    public class ModifyToolPreset
    {
        public string Name;
        public bool Enabled;
        public KeyCode Key;
        public VoxelModificationType ModType;
        public string ObjectName;
        public float Volume;
        public bool Sphere;
        public bool OnKeyDown;

        public GroundLayers Layer = GroundLayers.VOXEL;
        public BrushTypes BrushType;
        public GameObject Obj;
        [HideInInspector]
        public GameObject CursorObject;

        public bool StrictVoxelPosition = false;
        public bool AutoAlignToGround = true;

        [NonSerialized]
        public int TypeId;
        public IBlockProcessor.ProcessorVoxelChangeType BlockModType;
        public IBlockProcessor ProcessorLayer;
        public string ProcessorName;
        public Vector2 RandomChance = new Vector2(10, 1000);
        public Material BrushMaterial;

        public KeyValuePair<string, int>[] Objects;
        private string[] _ObjectsName;
        public string[] ObjectsName
        {
            get
            {
                if (_ObjectsName == null || Objects.Length != _ObjectsName.Length)
                {
                    _ObjectsName = new string[Objects.Length];
                    for (int i = 0; i < Objects.Length; ++i)
                    {
                        _ObjectsName[i] = Objects[i].Key;
                    }
                }

                return _ObjectsName;
            }
        }
        public int Selected = 0;
        public int GetTypeId()
        {
            return Objects[Selected].Value;
        }

    };

    public LayerMask ChunkLayer;
	[HideInInspector]
    public ClickMouseLook Look;
    public ModifyToolPreset[] Presets;
    private ModifyToolPreset CurrentPreset;

    private Rect ObjectsRect = new Rect();
    private VectorI3 LastPoint;

    private int Radius
    {
        get
        {
            return Mathf.Max(CursorWidth + CursorHeight + CursorDepth);
        }
    }
    private float Scale
    {
        get
        {
            return Mathf.Max(CursorWidth, CursorHeight, CursorDepth, 1) + ScaleOffset;
        }
    }
    private float ScaleOffset;
    private float Rotation;
    private bool IsAdding = true;

    public override void OnInit()
    {
        UpdatePresets(Presets);

        if (Presets.Length > 0)
            ActivePreset(Presets[0]);

        SetCursorScale(1, 1, 1);
    }

    void Update()
    {
        UpdateCursor();

		if (ToEnableDisable != null && Input.GetKeyDown(KeyCode.Escape))
            ToEnableDisable.enabled = !ToEnableDisable.enabled;

        float Wheel = Input.GetAxis("Mouse ScrollWheel");
        if (Input.GetKeyDown(KeyCode.F))
            IsAdding = !IsAdding;

        if (Look != null)
        {
            if (CurrentPreset != null && CurrentPreset.BrushType != BrushTypes.CUSTOM_OBJECT)
            {
                if (Look != null)
                {
                    if (InputManager.GetMouseButton(1))
                    {
                        Look.enabled = true;
                    }
                    else
                        Look.enabled = false;
                }
            }
            else
                Look.enabled = true;
        }

        if (Wheel != 0)
        {
            if (Input.GetKey(KeyCode.LeftShift) && CurrentPreset != null)
            {
                if (CurrentPreset.BrushType == BrushTypes.PROCESSOR_OBJECT)
                    ScaleOffset += Wheel;
                else
                    SetCursorScale(CursorWidth + (Wheel > 0 ? 1 : -1), CursorHeight, CursorDepth);

                ActivePreset(CurrentPreset);
            }
            else if (Input.GetKey(KeyCode.LeftAlt) && CurrentPreset != null)
            {
                SetCursorScale(CursorWidth, CursorHeight + (Wheel > 0 ? 1 : -1), CursorDepth);
                ActivePreset(CurrentPreset);
            }
            else if (Input.GetKey(KeyCode.LeftControl) && CurrentPreset != null)
            {
                Rotation += (Wheel * 20);
                if (Rotation > 360) Rotation = 0;
                else if (Rotation < 0) Rotation = 360;
                ActivePreset(CurrentPreset);
            }
            else
            {
                CursorDistance += Wheel > 0 ? 1 : -1;
            }
        }

        if (Input.GetKeyDown(KeyCode.KeypadPlus)) ScaleUp();
        else if (Input.GetKeyDown(KeyCode.KeypadMinus)) ScaleDown();

        if (CurrentPreset.BrushType == BrushTypes.PROCESSOR_OBJECT && CurrentPreset.Obj != null && CurrentPreset.CursorObject != null && CurrentPreset.CursorObject.name != CurrentPreset.Obj.name)
        {
            GameObject.Destroy(CurrentPreset.CursorObject);
            CurrentPreset.CursorObject = null;
            ActivePreset(CurrentPreset);
        }

        if (new Rect(0, 0, Screen.width, ObjectsRect.height + 20).Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
            return;

        if (CurrentPreset != null && CurrentPreset.Enabled == false)
            return;

        if (InputManager.GetMouseButton(0))
        {
            if (CurrentPreset != null && (!CurrentPreset.OnKeyDown || (CurrentPreset.OnKeyDown && InputManager.GetMouseButtonDown(0))))
            {
                ModifyPreset(CurrentPreset);
            }
        }
        /*else if (InputManager.GetMouseButton(1))
        {
            if (CurrentPreset != null && (CurrentPreset.BrushType != BrushTypes.CUSTOM_OBJECT))
                return;

            if (CurrentPreset != null && (!CurrentPreset.OnKeyDown || (CurrentPreset.OnKeyDown && InputManager.GetMouseButtonDown(1))))
                ModifyPreset(CurrentPreset);
        }*/
        else if (InputManager.GetMouseButtonDown(2)) // Rotate
        {
            RotatePreset(CurrentPreset);
        }
    }

    float TotalHeight = 0;
    void OnGUI()
    {
        if (TerrainManager.Instance == null || (int)TerrainManager.Instance.State <= 2)
            return;

		if (!DrawToolConfig)
            return;

        GUI.Box(new Rect(25,25,210,TotalHeight),"");
        DrawPreset(ref TotalHeight, 30, 30, Presets);
    }

    public void UpdatePresets(ModifyToolPreset[] Presets)
    {
        foreach (ModifyToolPreset Preset in Presets)
        {
            if (Preset.Obj != null && Preset.CursorObject == null)
            {
                Preset.CursorObject = GameObject.Instantiate(Preset.Obj) as GameObject;
                Preset.CursorObject.name = Preset.Obj.name;
                Preset.CursorObject.SetActiveRecursively(false);
            }
            else if (Preset.Obj == null && Preset.BrushType == BrushTypes.PLANE_BRUSH)
            {
                Preset.CursorObject = new GameObject();
                Preset.CursorObject.name = "Brush";
                MeshFilter CFilter = Preset.CursorObject.AddComponent<MeshFilter>();
                MeshRenderer CRenderer = Preset.CursorObject.AddComponent<MeshRenderer>();

                Mesh CMesh = new Mesh();
                CFilter.sharedMesh = CMesh;
                CRenderer.sharedMaterial = Preset.BrushMaterial;
            }

            if (Preset.Layer != GroundLayers.VOXEL)
            {
                if (Preset.ProcessorLayer == null)
                {
                    Debug.LogWarning("No Processor Object in ProcessorLayer : " + Preset.ObjectName + ". Searching By ProcessorName...");
                    Preset.ProcessorLayer = Manager.GetProcessor(Preset.ProcessorName);
                    if (Preset.ProcessorLayer == null)
                    {
                        Debug.LogError("Processor not found for Preset : " + Preset.ObjectName + ", Processor : " + Preset.ProcessorName);
                        continue;
                    }
                }

                if (Preset.ProcessorLayer.enabled)
                    Preset.Objects = Preset.ProcessorLayer.GetDatas();
                else
                    Preset.Objects = new KeyValuePair<string, int>[0];
            }

            if (Preset.Layer == GroundLayers.VOXEL)
            {
                for (int i = 0; i < BlocksId.Length; ++i)
                {
                    if (BlockNames[i].text == Preset.ObjectName)
                    {
                        Preset.TypeId = (byte)i;
                        break;
                    }
                }
            }
            else if(Preset.ObjectName != null && Preset.ObjectName.Length != 0)
                Preset.TypeId = Preset.ProcessorLayer.GetDataIdByName(Preset.ObjectName);
        }
    }

    public void DrawPreset(ref float TotalHeight, float x, float y, ModifyToolPreset[] Presets)
    {
        for (int i = 0; i < Presets.Length; ++i)
        {
            if (!Presets[i].Enabled)
                continue;

            if (CurrentPreset == Presets[i])
                GUI.color = Color.blue;

            GUI.Label(new Rect(x, y, 100, 20), Presets[i].Key.ToString());
            if (GUI.Button(new Rect(x+100, y, 100, 20), Presets[i].Name) || Event.current.keyCode == Presets[i].Key)
            {
                ActivePreset(Presets[i]);
            }

            GUI.color = Color.white;

            y += 20;
        }

        GUI.Label(new Rect(x, y, 200, 20), "+ : Tool Size + 1");
        y += 20;
        GUI.Label(new Rect(x, y, 200, 20), "- : Tool Size - 1");
        y += 20;
        GUI.Label(new Rect(x, y, 200, 20), "Alt+Wheel : Tool Height");
        y += 20;
        TotalHeight = y;
    }

    public void DrawObjects(ref Rect Position)
    {
        if (Position.width == 0)
            Position = new Rect(100, 30, Screen.width - 100, 80);

        if (CurrentPreset != null && CurrentPreset.Objects != null && CurrentPreset.Objects.Length > 0)
        {
            Position.height = CurrentPreset.Objects.Length / 5 * 20;

            if (Event.current.type == EventType.mouseDown && Event.current.button == 2) // Next Object
            {
                CurrentPreset.Selected++;
                if (CurrentPreset.Selected >= CurrentPreset.Objects.Length)
                    CurrentPreset.Selected = 0;
            }

            CurrentPreset.Selected = GUI.SelectionGrid(Position, CurrentPreset.Selected, CurrentPreset.ObjectsName, 5);
            int Id = CurrentPreset.GetTypeId();

            if (Id != CurrentPreset.TypeId)
            {
                CurrentPreset.TypeId = Id;
                ActivePreset(CurrentPreset);
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }

    public void ModifyPreset(ModifyToolPreset Preset)
    {
        if (Preset == null)
            return;

        ActivePreset(Preset);

        if (IsFlushing())
        {
            //Debug.LogError("IsFlushing ");
            return;
        }

        if (CurrentPreset.Layer == GroundLayers.VOXEL)
            ModifyVoxel();
        else
            ModifyProcessor();
    }

    public void ModifyVoxel()
    {
        Vector3 Position = new Vector3();
        TerrainBlock Block = null;
        if (CursorTObject != null)
        {
            if (CursorTObject.BlockObject == null)
                return;

            Block = CursorTObject.BlockObject;
            if (Block == null)
                return;

            Position = GetCursorVoxel();
        }
        else
        {
            TerrainChunkScript Script = Raycast(CenterRaycast, ref Position);
            if (Script == null || !Script.ChunkRef.IsValid(ref Block))
            {
                //Debug.LogError("Raycast failed");
                return;
            }
        }

        ToolSize = Radius;
        if (!IsAdding)
        {
            if (ModType == VoxelModificationType.ADD_VOLUME)
                ModType = VoxelModificationType.REMOVE_VOLUME;
            else if (ModType == VoxelModificationType.REMOVE_VOLUME)
                ModType = VoxelModificationType.ADD_VOLUME;
            else if (ModType == VoxelModificationType.SET_TYPE_ADD_VOLUME)
                ModType = VoxelModificationType.REMOVE_VOLUME;
            else if (ModType == VoxelModificationType.SET_TYPE_REMOVE_VOLUME)
                ModType = VoxelModificationType.SET_TYPE_ADD_VOLUME;
            else if(ModType == VoxelModificationType.SET_TYPE)
                ToolType = 0;
        }

        Modify(Block, Position, new Vector3(), CursorWidth, CursorHeight, CursorDepth, false);
        ToolType = CurrentPreset.TypeId;
        ModType = CurrentPreset.ModType;
    }

    public void ModifyProcessor()
    {
        if (IsFlushing())
            return;

        TerrainBlock Block = CursorTObject.BlockObject;
        if (Block == null)
            return;

        VectorI3 Voxel = CursorTObject.CurrentVoxel;
        if (Voxel.y == -1 || LastPoint == Voxel)
            return;

        LastPoint = Voxel;

        if (!IsAdding)
            ToolType = 0;

        if (CurrentPreset.BrushType == BrushTypes.PROCESSOR_OBJECT)
            CurrentPreset.ProcessorLayer.ModifyPoint(Block, Voxel.x, Voxel.y, Voxel.z, ToolType, Rotation, Scale, false, CurrentPreset.BlockModType,false, CanModifyVoxel);
        else
        {
            if (Sphere)
                CurrentPreset.ProcessorLayer.ModifySphere(Block, Voxel.x, Voxel.y, Voxel.z, CursorWidth, ToolType, Rotation, Scale, true, CurrentPreset.BlockModType, CanModifyVoxel);
            else
                CurrentPreset.ProcessorLayer.ModifyCube(Block, Voxel.x, Voxel.y, Voxel.z, CursorWidth, CurrentPreset.AutoAlignToGround ? 0 : CursorHeight, CursorDepth, ToolType, Rotation, Scale, true, CurrentPreset.BlockModType, CanModifyVoxel);
        }
        ToolType = CurrentPreset.TypeId;
        FlushAction = CurrentPreset.ProcessorLayer.Flush(ActionProcessor.Instance.UnityThread);
    }

    public bool CanModifyVoxel(ref TerrainBlock Block, ref int x, ref int y, ref int z)
    {
        if (CurrentPreset.AutoAlignToGround)
        {
            byte Type = 0;
            ITerrainBlock IBlock = null;
            y = (int)Block.GetLowerOrUpperHeight(ref IBlock, x, y, z, ref Type).x;
            Block = IBlock as TerrainBlock;

            if (y == -1 || Block == null)
                return false;
        }

        if (CurrentPreset.BrushType == BrushTypes.PROCESSOR_OBJECT || UnityEngine.Random.Range(0, CurrentPreset.RandomChance.y) <= CurrentPreset.RandomChance.x)
            return true;

        return false;
    }

    void LateUpdate()
    {
        UpdateCursor();
    }

    public void RotatePreset(ModifyToolPreset Preset)
    {
        if (Preset == null)
            return;

        SetCursorScale(CursorDepth, CursorHeight, CursorWidth);
        ActivePreset(Preset);
    }

    public void ActivePreset(ModifyToolPreset Preset)
    {
        if (CurrentPreset != Preset)
            DisablePreset(CurrentPreset);

        if (Preset == null)
            return;

        CurrentPreset = Preset;

        this.ToolVolume = Preset.Volume;
        this.ToolType = Preset.TypeId;
        this.Sphere = Preset.Sphere;
        this.OnKeyDown = Preset.OnKeyDown;
        this.ModType = Preset.ModType;

        if (CurrentPreset.BrushType == BrushTypes.PROCESSOR_OBJECT && CurrentPreset.Layer == GroundLayers.PROCESSOR)
        {
            GameObject Cursor = CurrentPreset.ProcessorLayer.GetCursor(CurrentPreset.TypeId);

            if (Cursor != null && (CurrentPreset.CursorObject == null || CurrentPreset.CursorObject.name != Cursor.name))
            {
                if(CurrentPreset.CursorObject != null)
                    GameObject.Destroy(CurrentPreset.CursorObject);

                CurrentPreset.CursorObject = GameObject.Instantiate(Cursor) as GameObject;
                CurrentPreset.CursorObject.name = Cursor.name;
                CursorObject = CurrentPreset.CursorObject;
            }
        }

        this.CursorObject = Preset.CursorObject;
        this.CusorStrictVoxelPosition = CurrentPreset.StrictVoxelPosition;

        if(CursorObject != null)
            CursorObject.SetActiveRecursively(true);
        UpdateCursor();
    }

    public void DisablePreset(ModifyToolPreset Preset)
    {
        if (Preset == null)
            return;

        CursorObject = null;
        CursorTObject = null;
        if (Preset.CursorObject != null)
        {
            Preset.CursorObject.SetActiveRecursively(false);
        }
    }

    public override void UpdateCursor()
    {
        if (CurrentPreset == null || TObject == null)
            return;

        base.UpdateCursor();
    }


    #region Cursor

    public void ScaleUp()
    {
        SetCursorScale(CursorWidth + 1, CursorHeight + 1, CursorDepth + 1);
    }

    public void ScaleDown()
    {
        SetCursorScale(CursorWidth - 1, CursorHeight - 1, CursorDepth - 1);
    }

    public override void SetCursorScale(int Width, int Height, int Depth)
    {
        base.SetCursorScale(Width, Height, Depth);

        foreach (ModifyToolPreset Preset in Presets)
        {
            if (Preset.BrushType == BrushTypes.PLANE_BRUSH)
            {
                Mesh CMesh = Preset.CursorObject.GetComponent<MeshFilter>().sharedMesh;
                MeshUtils.CreatePlane(CMesh, Mathf.Max(CursorWidth, 1) * 2, Mathf.Max(CursorDepth, 1) * 2);
            }
        }
    }

    public bool CenterRaycast = true;
    public override Vector3 GetCursorPosition()
    {
        Vector3 Position = new Vector3();

        if (CurrentPreset.BrushType != BrushTypes.CUSTOM_OBJECT)
        {
            if (TObject.BlockObject == null || CursorTObject == null)
                return Position;

            if (CursorTObject.BlockObject == null)
            {
                return transform.position;
            }

            Ray r = GetComponent<Camera>().ScreenPointToRay(CenterRaycast ? new Vector3(Screen.width/2,Screen.height/2,0) : Input.mousePosition);

            /*
            VoxelRaycastHit Hit = new VoxelRaycastHit();
            Vector3 Voxel = TObject.BlockObject.GetVoxelFromWorldPosition(r.origin + r.direction * 1000, new Vector3(), false);
            if (TerrainRaycaster.Raycast(TObject.BlockObject, TObject.IntCurrentVoxel.x, TObject.IntCurrentVoxel.y, TObject.IntCurrentVoxel.z, (Voxel - TObject.CurrentVoxel).normalized, 100, ref Hit))
                Position = Hit.Block.GetWorldPosition(Hit.x, Hit.y, Hit.z, false);
             * */

            RaycastHit RHit = new RaycastHit();
            if (Physics.Raycast(r, out RHit, 200, ChunkLayer.value))
            {
                Position = RHit.point;
            }
            else
            {
                return CursorObject.transform.position;
            }

            if (CurrentPreset.BrushType == BrushTypes.PLANE_BRUSH)
            {
                Position.y = CursorTObject.BlockObject.GetHeight(Position, 0).y;
                Mesh CMesh = CursorObject.GetComponent<MeshFilter>().sharedMesh;
                Vector3[] Vertices = CMesh.vertices;
                float Height = 0;
                for (int i = 0; i < Vertices.Length; ++i)
                {
                    Height = CursorTObject.BlockObject.GetHeight(Position.x + Vertices[i].x, Position.y, Position.z + Vertices[i].z, 0).y;
                    if (Height == -1) continue;
                    Vertices[i].y = (Height - Position.y) + 0.5f;
                }
                CMesh.vertices = Vertices;
            }
        }
        else
            Position = base.GetCursorPosition();

        return Position;
    }


    public override Vector3 GetCursorScale()
    {
        if (CurrentPreset.BrushType == BrushTypes.PLANE_BRUSH)
            return new Vector3(1, 1, 1);
        else if (CurrentPreset.BrushType == BrushTypes.PROCESSOR_OBJECT)
        {
            if (CursorObject != null)
                CursorObject.transform.rotation = Quaternion.Euler(0, Rotation, 0);
            return new Vector3(Scale, Scale, Scale);
        }
        else
            return base.GetCursorScale();
    }

    #endregion
}