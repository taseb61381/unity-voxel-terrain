using System;
using System.Collections.Generic;
using UnityEngine;


public class UpdateAroundBlocks : MonoBehaviour 
{
    public Transform Target;

    public bool EnableBlockUpdate = true;
    public float BlockUpdateRange = 5.0f;
    public Vector3 CurrentPosition = new Vector3();
    public VectorI3 CurrentBlock = new VectorI3();
    public bool WaitLoadingForUpdate = true;

    [NonSerialized]
    public Vector3 LastPositionCheck = new Vector3();

    [NonSerialized]
    public ExecuteAction UpdateBlockAction;

    [NonSerialized]
    public ActionProgressCounter LoadingCounter = new ActionProgressCounter();

    public KeyCode EnableKey = KeyCode.P;

    public float FixedYUpdate = -1;
    public float FixedZUpdate = -1;

	void Start() 
    {
        Debug.LogError("UpdateAroundBlocks.cs is deprecated. Please use LocalTerrainObjectScript: " + gameObject);

        enabled = false;
	}

    void UpdatePosition()
    {
        if (Target == null)
            return;

        CurrentPosition.x = Target.position.x;

        if (FixedZUpdate != -1)
            CurrentPosition.z = FixedZUpdate;
        else
            CurrentPosition.z = Target.position.z;

        if (FixedYUpdate == -1)
            CurrentPosition.y = Target.position.y;
        else
            CurrentPosition.y = FixedYUpdate;
    }

    void Update()
    {
        /*if (Manager == null)
            Manager = FindObjectOfType(typeof(TerrainManager)) as TerrainManager;

        if (Manager == null)
            return;

        if (Input.GetKeyDown(EnableKey))
            EnableBlockUpdate = !EnableBlockUpdate;

        if (UpdateBlockAction == null)
        {
            if (Manager != null && Manager.isStarted)
            {
                UpdateBlockAction = new ExecuteAction(null, UpdateAround);
                ActionProcessor.Add(UpdateBlockAction, 0, false);
            }
            else
                return;
        }

        UpdatePosition();*/
    }

    void OnDisable()
    {
        EnableBlockUpdate = false;
    }

    public HashSet<VectorI3> TerrainList = new HashSet<VectorI3>();
    public List<VectorI3> RemoveList = new List<VectorI3>();
    public List<VectorI3> CreateList = new List<VectorI3>();
    public byte Step = 0;

    /*public bool UpdateAround(ActionThread Thread)
    {
        if (Step == 0) // Check if player has moved
        {
            if (!EnableBlockUpdate)
                return false;

            if (WaitLoadingForUpdate && Manager.LoadingProgress != 100.0f)
                return false;

            float Distance = Vector3.Distance(LastPositionCheck, CurrentPosition);
            if (Distance < BlockUpdateRange)
                return false;

            LastPositionCheck = CurrentPosition;

            VectorI3 BlockPosition = TerrainManager.Instance.Informations.GetBlockFromWorldPosition(CurrentPosition);
            if (BlockPosition == CurrentBlock)
                return false;

            Thread.RecordTime();

            VectorI3 Position = new VectorI3();
            CurrentBlock = BlockPosition;

            CreateList.Clear();
            RemoveList.Clear();
            TerrainList.Clear();

            Manager.Container.GetTerrains(ref TerrainList, true);

            foreach (VectorI3 Existing in TerrainList)
            {
                if (Existing.x < TerrainManager.Instance.Informations.MinRange.x + CurrentBlock.x || Existing.x > TerrainManager.Instance.Informations.MaxRange.x + CurrentBlock.x)
                    RemoveList.Add(Existing);
                else if (Existing.y < TerrainManager.Instance.Informations.MinRange.y + CurrentBlock.y || Existing.y > TerrainManager.Instance.Informations.MaxRange.y + CurrentBlock.y)
                    RemoveList.Add(Existing);
                else if (Existing.z < TerrainManager.Instance.Informations.MinRange.z + CurrentBlock.z || Existing.z > TerrainManager.Instance.Informations.MaxRange.z + CurrentBlock.z)
                    RemoveList.Add(Existing);
            }

            for (int x = (int)TerrainManager.Instance.Informations.MinRange.x; x <= (int)TerrainManager.Instance.Informations.MaxRange.x; ++x)
                for (int y = (int)TerrainManager.Instance.Informations.MinRange.y; y <= (int)TerrainManager.Instance.Informations.MaxRange.y; ++y)
                    for (int z = (int)TerrainManager.Instance.Informations.MinRange.z; z <= (int)TerrainManager.Instance.Informations.MaxRange.z; ++z)
                    {
                        Position.x = CurrentBlock.x + x;
                        Position.y = CurrentBlock.y + y;
                        Position.z = CurrentBlock.z + z;

                        if (!TerrainList.Contains(Position))
                            CreateList.Add(Position);
                    }

            CreateList.Sort((VectorI3 a, VectorI3 b) =>
                {
                    if (Vector2.Distance(a, CurrentBlock) < Vector2.Distance(b, CurrentBlock))
                        return -1;
                    else
                        return 1;
                });
            Step = 1;
            Thread.AddStats("Update.Generate", Thread.Time());
        }
        else if (Step == 1) // Add new Terrains to process
        {
            Thread.RecordTime();

            foreach (VectorI3 Position in RemoveList)
                TerrainBuilderAction.RemoveTerrain(Position);

            foreach (VectorI3 Position in CreateList)
                TerrainBuilderAction.CreateTerrain(Position, null);

            CreateList.Clear();
            RemoveList.Clear();
            Thread.AddStats("Update.Add", Thread.Time());

            Step = 0;
        }

        return false;
    }*/
}
