using UnityEngine;

public class TouchControl : ITerrainModifier 
{
    public MPJoystick LeftJoystik;
    public MPJoystick RightJoystik;

    public Transform cameraPivot;
    public Transform cameraTransform;

    public float Speed = 5f;
    public Vector2 rotationSpeed = new Vector2(50, 25);

    public Rigidbody targetRigid;

    public GUIText TextMaterial;
    public MPJoystick ApplyButton;
    public MPJoystick RemoveButton;
    public MPJoystick NextButton;
    public MPJoystick PreviousButton;
    public MPJoystick RangeIncButton;
    public MPJoystick RangeDecButton;
    private bool NextDown = false;

    void Update()
    {
        if (Manager == null)
            return;

        UpdateDistance();
        UpdateCursor();
        UpdateSelection();

        if (ApplyButton.IsFingerDown())
        {
            OnLeftDown();
        }
        else if (RemoveButton.IsFingerDown())
        {
            OnRightDown();
        }
    }

    public virtual void OnLeftDown()
    {
        ModType = VoxelModificationType.SET_TYPE_REMOVE_VOLUME;
        Vector3 Position = GetCursorVoxel();
        Modify(Manager.GetBlockFromWorldPosition(Position), Position, new Vector3(), ToolSize, ToolSize, ToolSize, false);
    }

    public virtual void OnRightDown()
    {
        ModType = VoxelModificationType.SET_TYPE_ADD_VOLUME;
        Vector3 Position = GetCursorVoxel();
        Modify(Manager.GetBlockFromWorldPosition(Position), Position, new Vector3(), ToolSize, ToolSize, ToolSize, false);
    }

    public virtual void UpdateDistance()
    {
        if (RangeIncButton.IsFingerDown())
            CursorDistance += 0.5f;
        else if (RangeDecButton.IsFingerDown())
            CursorDistance -= 0.5f;
    }

    public virtual void UpdateSelection()
    {
        TextMaterial.text = BlockNames[ToolType].text;

        if (NextButton.IsFingerDown())
        {
            if (!NextDown)
            {
                NextDown = true;
                ++ToolType;
                if (ToolType >= BlockNames.Length)
                    ToolType = 0;
            }
        }
        else if (PreviousButton.IsFingerDown())
        {
            if (!NextDown)
            {
                NextDown = true;
                --ToolType;
                if (ToolType < 0)
                    ToolType = BlockNames.Length - 1;
            }
        }
        else
            NextDown = false;

    }

    public virtual void UpdateCamera()
    {
        Vector3 v = Vector3.zero;
        v += targetRigid.transform.right * LeftJoystik.position.x;
        v += targetRigid.transform.forward * LeftJoystik.position.y;
        v *= Speed;
        targetRigid.velocity = v;

        // Scale joystick input with rotation speed
        Vector2 camRotation = RightJoystik.position;
        camRotation.x *= rotationSpeed.x;
        camRotation.y *= rotationSpeed.y;
        camRotation *= Time.deltaTime;

        // Rotate around the character horizontally in world, but use local space
        // for vertical rotation
        cameraPivot.Rotate(0, camRotation.x, 0, Space.World);
        cameraPivot.Rotate(-camRotation.y, 0, 0);
    }

    void FixedUpdate()
    {
        UpdateCamera();
    }
}
