﻿
using UnityEngine;

namespace TerrainEngine
{
    [RequireComponent(typeof(Rigidbody))]
    public class MoveControl : MonoBehaviour
    {
        private Vector3 _velocity = Vector3.zero;
        public float Power = 5;
        public float ShiftPower = 2f;
        public bool DisableCursor = true;
        public Camera CurrentCam;
        public Transform CTransform;
        public Rigidbody CRigidbody;

        void Start()
        {
            if (DisableCursor)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }

            CurrentCam = GetComponent<Camera>();
            CTransform = transform;
            CRigidbody = CTransform.GetComponent<Rigidbody>();
        }

        void Update()
        {
            if (DisableCursor)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }

            Vector3 v = Vector3.zero;

            v.x = InputManager.GetAxis("Horizontal");
            v.z = InputManager.GetAxis("Vertical");
            v.y = Input.GetKey(KeyCode.Q) ? 1f : Input.GetKey(KeyCode.Z) ? -1f : 0;
            if (Input.GetKey(KeyCode.Space))
                v.y += 1;

            if (gameObject.GetComponent<Rigidbody>().useGravity)
                v.y += Physics.gravity.y * 0.05f;

            if (CurrentCam != null && CurrentCam.orthographic && v.z != 0)
            {
                CurrentCam.orthographicSize -= v.z * 0.1f;
                v.z = 0;
            }

            float power = Power;

            if (Input.GetKey(KeyCode.LeftShift))
            {
                power *= ShiftPower;
            }

            if (v.x != 0f) v.x = Mathf.Sign(v.x) * power;
            if (v.z != 0f) v.z = Mathf.Sign(v.z) * power;

            v.y *= power;

            float ry = CTransform.localRotation.eulerAngles.y;

            ry = ry * Mathf.Deg2Rad + Mathf.PI / 2f;

            Vector3 dv = Vector3.zero;

            dv.x = -Mathf.Cos(ry) * v.z + Mathf.Sin(ry) * v.x;
            dv.y = v.y;
            dv.z = Mathf.Sin(ry) * v.z + Mathf.Cos(ry) * v.x;

            _velocity += dv * power;
            _velocity *= 0.5f;

            CRigidbody.velocity = _velocity;
        }
    }
}