﻿using System;
using TerrainEngine;
using UnityEngine;


public class ITerrainModifier : MonoBehaviour
{
	[HideInInspector]
    public TerrainManager Manager = null;
	[HideInInspector]
    public TerrainObject TObject = null;
    //not appliable in V2
	[HideInInspector]
    public bool DrawBlockTypes = true;
    public bool DrawToolConfig = true;

	[HideInInspector]
    public int ToolSize = 1;
	[HideInInspector]
    public int ToolType = 0;
	[HideInInspector]
    public float ToolVolume = 1f;
	[HideInInspector]
    public VoxelModificationType ModType;
	[HideInInspector]
    public bool Sphere = false;

    public bool InstantChunksGeneration = true;
    public bool InstantChunksBuild = false;

	[HideInInspector]
    public bool OnKeyDown = false;

    public GameObject CursorObject = null;
    public float CursorDistance = 25f;
    public bool CusorStrictVoxelPosition = false;
    public Vector3 CursorOffset = new Vector3();

    public IAction FlushAction = null;

    internal GUIContent[] BlockNames;
    internal int[] BlocksId;

    internal int Selection = 0;
    internal string[] ModificationNames;
    internal byte[] ModificationValues;

    internal Ray Ray;
    internal RaycastHit Hit;

    public MonoBehaviour ToEnableDisable = null;
    public GameObject ToTeleportOnDisable;

	[HideInInspector]
    public TerrainObject CursorTObject;

    public int CursorWidth;
    public int CursorHeight;
    public int CursorDepth;

    void Start()
    {
        if (Manager == null)
            Manager = GameObject.FindObjectOfType(typeof(TerrainManager)) as TerrainManager;

        if (Manager != null)
        {
            BlocksId = new int[Manager.Palette.GetDataCount()];
            BlockNames = new GUIContent[Manager.Palette.GetDataCount()];
            for (int i = 0; i < BlockNames.Length; ++i)
                BlockNames[i] = new GUIContent(Manager.Palette.GetVoxelData<VoxelInformation>((byte)i).Name, "T");
        }

        ModificationNames = Enum.GetNames(typeof(VoxelModificationType));
        ModificationValues = (byte[])Enum.GetValues(typeof(VoxelModificationType));
        for (int i = 0; i < ModificationValues.Length; ++i)
        {
            if (ModificationValues[i] == (int)ModType)
            {
                Selection = i;
                break;
            }
        }

        if(TObject == null)
            TObject = GetComponent<TerrainObject>();

        FlushAction = null;

        OnInit();
    }

    public virtual void OnInit()
    {

    }

    void OnEnable()
    {
        if (CursorObject != null)
            CursorObject.SetActive(true);
    }

    void OnDisable()
    {
        if (CursorObject != null)
            CursorObject.SetActive(false);

        if (ToTeleportOnDisable != null)
        {
            ToTeleportOnDisable.transform.position = transform.position;
            ToTeleportOnDisable.transform.rotation = Quaternion.Euler(new Vector3(0,transform.rotation.eulerAngles.y,0));
        }
    }

    public virtual bool IsFlushing()
    {
        return FlushAction != null && !FlushAction.IsDone();
    }

    public virtual void UpdateCursor()
    {
        if (Manager == null || !Manager.enabled)
            return;

        if (CursorObject == null)
            return;

        if (CursorTObject == null || CursorTObject.gameObject != CursorObject)
        {
            CursorTObject = CursorObject.GetComponent<TerrainObject>();
            if (CursorTObject == null)
                CursorTObject = CursorObject.AddComponent<TerrainObject>();

            CursorTObject.UpdateCurrentVoxel = false;
        }

        if (CursorTObject != null && CursorTObject.enabled == false)
            CursorTObject.enabled = true;

        CursorObject.transform.position = GetCursorPosition();

        Vector3 Scale = GetCursorScale();
        if (CursorObject.transform.localScale != Scale)
            CursorObject.transform.localScale = Scale;

        UpdateStrictVoxel();
    }

    public void UpdateStrictVoxel()
    {
        if (CusorStrictVoxelPosition)
        {
            CursorObject.transform.position = TerrainManager.Instance.Informations.GetStrictVoxelPosition(CursorObject.transform.position);
        }
    }

    public virtual TerrainChunkScript Raycast(bool CenterRaycast,ref Vector3 Position)
    {
        Ray = GetComponent<Camera>().ScreenPointToRay(CenterRaycast ? new Vector3(Screen.width/2,Screen.height/2,0) : Input.mousePosition);
        if (Physics.Raycast(Ray, out Hit, 100))
        {
            //Debug.LogError(Hit.collider);
            Position = Hit.point;
            TerrainChunkScript Script = TerrainManager.GetGameObjectChunk(Hit.collider.gameObject);
            if (Script == null)
                return null;

            return Script;
        }

        return null;
    }

    public virtual Vector3 GetCursorVoxel()
    {
        if (CursorObject != null)
            return CursorObject.transform.position;
        else
            return gameObject.transform.position;
    }

    public virtual void Modify(TerrainBlock Block, Vector3 Position, Vector3 Normal, int Tx, int Ty, int Tz, bool Remove)
    {
        if (Block == null)
        {
            Debug.LogError("Modify Block == null " + Position);
            return;
        }

        VectorI3 Voxel = Block.GetVoxelFromWorldPosition(Position, Remove);

        if (Sphere)
        {
            SphereOperator Operator = new SphereOperator();
            Operator.InitPosition(Block, Voxel);
            Operator.Init((byte)ToolType, ToolVolume, ModType);
            Operator.Radius = ToolSize;
            Manager.ModificationInterface.AddOperator(Operator);
        }
        else
        {
            BoxOperator Operator = new BoxOperator();
            Operator.InitPosition(Block, Voxel);
            Operator.Init((byte)ToolType, ToolVolume, ModType);
            Operator.SizeX = Tx;
            Operator.SizeY = Ty;
            Operator.SizeZ = Tz;
            Manager.ModificationInterface.AddOperator(Operator);
        }

        FlushAction = Manager.ModificationInterface.Flush(ActionProcessor.Instance.UnityThread, TObject,InstantChunksGeneration, InstantChunksBuild);
    }

    public virtual Vector3 GetCursorScale()
    {
        float x = (Mathf.Max(1, CursorWidth * 2) + (ToolVolume - TerrainInformation.Isolevel + 0.1f)) * TerrainManager.Instance.Informations.Scale;
        float y = (Mathf.Max(1, CursorHeight * 2) + (ToolVolume - TerrainInformation.Isolevel + 0.1f)) * TerrainManager.Instance.Informations.Scale;
        float z = (Mathf.Max(1, CursorDepth * 2) + (ToolVolume - TerrainInformation.Isolevel + 0.1f)) * TerrainManager.Instance.Informations.Scale;
        return new Vector3(x, y, z);
    }

    public virtual Vector3 GetCursorPosition()
    {
        Vector3 Position = gameObject.transform.position + gameObject.transform.forward * (CursorDistance + ToolSize * 2) + CursorOffset;
        return new VectorI3(Position);
    }

    public virtual void SetCursorScale(int Width, int Height, int Depth)
    {
        CursorWidth = Width;
        CursorHeight = Height;
        CursorDepth = Depth;

        if (CursorWidth < 0) CursorWidth = 0;
        if (CursorHeight < 0) CursorHeight = 0;
        if (CursorDepth < 0) CursorDepth = 0;
    }
}
