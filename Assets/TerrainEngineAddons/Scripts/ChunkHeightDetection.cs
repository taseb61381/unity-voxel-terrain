using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TerrainEngine;

public class ChunkHeightDetection : MonoBehaviour 
{
    public TerrainChunkScript Script;

    public Queue<ChunkSphere> Spheres = new Queue<ChunkSphere>();
    public List<ChunkSphere> Objs = new List<ChunkSphere>();

    public ChunkSphere GetObject()
    {
        if (Spheres.Count == 0)
        {
            GameObject Obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            Obj.transform.parent = gameObject.transform;
            Obj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            ChunkSphere Sphere = Obj.AddComponent<ChunkSphere>();
            Objs.Add(Sphere);
            return Sphere;
        }
        else
            return Spheres.Dequeue();
    }

	void OnEnable () 
    {
        Script = TerrainManager.GetGameObjectChunk(gameObject);
        TerrainChunk Chunk = Script.ChunkRef.Chunk;
        ITerrainBlock Block = null;
        TerrainBlock BlockRef = null;
        byte Type = 0;
        if (!Script.ChunkRef.IsValid(ref BlockRef))
            return;

        for (int x = 0; x < Script.ChunkRef.Informations.VoxelPerChunk; ++x)
        {
            for (int z = 0; z < Script.ChunkRef.Informations.VoxelPerChunk; ++z)
            {
                float Height = BlockRef.GetLowerOrUpperHeight(ref Block, Chunk.StartX + x, int.MaxValue, Chunk.StartZ + z, ref Type).y;
                if (Height == -1)
                    Height = Script.ChunkRef.WorldPosition.y;

                ChunkSphere Obj = GetObject();
                Obj.name = x + "," + Height + "," + z;
                Obj.Init(Script, x, Height, z);
            }
        }
	}

    void OnDisable()
    {
        foreach (ChunkSphere Obj in Objs)
            Spheres.Enqueue(Obj);

        Objs.Clear();
    }
}
