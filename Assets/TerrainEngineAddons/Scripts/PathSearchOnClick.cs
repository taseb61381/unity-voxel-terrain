﻿using UnityEngine;
using System.Collections;
using TerrainEngine;

public class PathSearchOnClick : MonoBehaviour 
{
    public Camera Cam;
    public PathTypes PathType = PathTypes.GROUND_PATH;
    public TerrainObject ObjectA;
    private TerrainObject ObjectB;
    private PathFinderExample Example;

    void Start()
    {
        Example = gameObject.AddComponent<PathFinderExample>();
        Example.DrawDebugSpheres = true;
        Example.PathType = PathType;

        ObjectB = GameObject.CreatePrimitive(PrimitiveType.Cylinder).AddComponent<TerrainObject>();
        ObjectB.name = "ObjectB";
        ObjectB.transform.parent = transform;
    }

	// Update is called once per frame
    void Update()
    {
        if (Cam == null)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit Hit;
            Vector3 v = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
            Ray ray = Cam.ScreenPointToRay(v);
            if (Physics.Raycast(ray, out Hit))
            {
                ObjectB.CTransform.position = Hit.point; // We set GameObject position to mouse click position
                ObjectB.UpdatePosition(); // We update TerranObject Position, object will update current block and current chunk

                Example.StartObject = ObjectA;
                Example.EndObject = ObjectB;

                Debug.Log("Path search " + ObjectA.Position + " To " + ObjectB.Position);
                Example.Info.Init(ObjectA.BlockObject, ObjectA.WorldVoxel, ObjectB.WorldVoxel, PathType, 1); // We set start and end  points to search
                Example.Info.Find(); // Start the research, the end will call PathFinderExample.OnPathEnd

                // Select all PathFollowers and move their targets
                PathFollower[] Followers = FindObjectsOfType<PathFollower>();
                if (Followers != null)
                {
                    foreach (PathFollower Follower in Followers)
                    {
                        Follower.SetTarget(ObjectB, 4, 0);
                    }
                }
            }
            else
                Debug.LogError("Raycast failed : No terrain");
        }
    }
}
