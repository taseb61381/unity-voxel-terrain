﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class MeshEditorModifier : MonoBehaviour 
{
    public List<GameObject> VerticesObj = new List<GameObject>();

	void Start () 
    {
	
	}

	void Update () 
    {
        MeshFilter Filter = GetComponent<MeshFilter>();
        if (Filter == null || Filter.sharedMesh == null)
            return;

        if (VerticesObj.Count != Filter.sharedMesh.vertices.Length)
        {
            Clean();

            for (int i = 0; i < Filter.sharedMesh.vertices.Length; ++i)
            {
                GameObject Obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                Obj.name = i.ToString();
                Obj.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                Obj.transform.position = Filter.transform.position + Filter.sharedMesh.vertices[i];
                Obj.transform.parent = transform;
                VerticesObj.Add(Obj);
            }
        }
	}

    public void Clean()
    {
        VerticesObj.ForEach(info => { if (info != null) GameObject.DestroyImmediate(info); });
        VerticesObj.Clear();
    }

    void OnDelete()
    {
        Clean();
    }
}
