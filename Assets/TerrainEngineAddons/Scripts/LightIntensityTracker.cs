﻿using UnityEngine;
using System.Collections;
using TerrainEngine;

public class LightIntensityTracker : MonoBehaviour {

    public Light Directional;
    public float MinLightIntensity;
    public ActiveDisableContainer OnOff;
    public ActiveDisableContainer OnOn;
    public bool Enabled;

	void Update () 
    {
        if (Enabled && Directional.intensity < MinLightIntensity)
        {
            Enabled = false;
            if (OnOff != null)
                OnOff.Call();
        }
        else if (!Enabled && Directional.intensity > MinLightIntensity)
        {
            Enabled = true;
            if (OnOn != null)
                OnOn.Call();
        }
	}
}
