﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DisableIfActiveMono : MonoBehaviour 
{
    public List<MonoBehaviour> ToDisable;
    public List<GameObject> ToDisableGo;
    public byte LastState = 2;

    public void SetState(bool Value)
    {
        if (LastState == 2 || (Value && LastState == 0) || (!Value && LastState == 1))
        {
            LastState = (byte)(Value ? 1 : 0);

            foreach (MonoBehaviour Mono in ToDisable)
                Mono.enabled = !Value;

            foreach (GameObject Obj in ToDisableGo)
                Obj.SetActive(!Value);
        }
    }
}
