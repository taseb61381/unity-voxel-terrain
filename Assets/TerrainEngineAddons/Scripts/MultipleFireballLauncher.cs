﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultipleFireballLauncher : MonoBehaviour 
{
    public Transform Player;
    public GameObject Fireball;
    public GameObject Explosion;
    public KeyCode LaunchKey = KeyCode.U;
    public int LaunchCount = 20;
    public float RandomSize = 50;
    public float LaunchDistance = 50;
    public float Speed = 2f;

    public List<TerrainObject> Fireballs = new List<TerrainObject>();

	void Update () 
    {
        TerrainObject TObj;
        SphereModificator Mod;
        GameObject Obj;

        for (int i = 0; i < Fireballs.Count; )
        {
            TObj = Fireballs[i];

            if (TObj.HasBlock && TObj.CurrentVoxelType != 0 && TObj.CurrentVoxelVolume > 0.5f) // Explode
            {
                Obj = GameObject.Instantiate(Explosion) as GameObject;
                Obj.transform.position = TObj.Position;

                Mod = TObj.gameObject.AddComponent<SphereModificator>();
                Mod.TObject = TObj;
                Mod.IsStarted = true;
                Mod.ModType = VoxelModificationType.REMOVE_VOLUME;
                Mod.Volume = 0.7f;
                Mod.Size = 3;
                Mod.Count = 1;

                Fireballs.RemoveAt(i);
                GameObject.Destroy(Obj, 5f);
                GameObject.Destroy(TObj.gameObject, 0.5f);
                continue;
            }

            if (TObj == null)
            {
                Fireballs.RemoveAt(i);
                GameObject.Destroy(TObj.gameObject, 0.5f);
                continue;
            }

            TObj.CTransform.position += -Vector3.up * Time.deltaTime * Speed;
            ++i;
        }

        if (Input.GetKeyDown(LaunchKey))
        {
            for (int i = 0; i < LaunchCount; ++i)
            {
                Obj = GameObject.Instantiate(Fireball) as GameObject;
                Obj.name += i;
                Transform TR = Obj.transform;
                TR.position = Player.position + Player.forward * LaunchDistance + new Vector3(Random.Range(-RandomSize, RandomSize), 50 + Random.Range(-RandomSize, RandomSize), Random.Range(-RandomSize, RandomSize));
                TObj = Obj.AddComponent<TerrainObject>();
                TObj.UpdateCurrentVoxel = true;
                TObj.DestroyIfNoTerrain = true;
                Fireballs.Add(TObj);
            }
        }
	}
}
