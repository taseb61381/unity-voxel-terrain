﻿using UnityEngine;
using System.Collections;

public class CameraGroundFollower : MonoBehaviour 
{
    private Transform CTransform;
    public float ForwardSpeed = 10;
    public float UpSpeed = 10;
    public float OffsetFromGround = 3;
    public float DesiredY;
    public LayerMask Layer;
    public bool IsRunning = false;
    public KeyCode Key = KeyCode.J;
    public KeyCode SaveOrientationKey = KeyCode.K;
    public KeyCode TeleportKey = KeyCode.L;
    public GameObject ToMoveWith;

    void Start()
    {
        CTransform = transform;
    }

    public void FixedUpdate()
    {
        if (!IsRunning)
            return;

        RaycastHit Hit;
        if (Physics.Raycast(CTransform.position + new Vector3(0, 100, 0), Vector3.down, out Hit, 999f, Layer))
            DesiredY = Hit.point.y + OffsetFromGround;
    }
	
	void Update () 
    {
        if (Input.GetKeyDown(SaveOrientationKey))
        {
            PlayerPrefs.SetFloat("RX",CTransform.rotation.eulerAngles.x);
            PlayerPrefs.SetFloat("RY", CTransform.rotation.eulerAngles.y);
            PlayerPrefs.SetFloat("RZ", CTransform.rotation.eulerAngles.z);

            PlayerPrefs.SetFloat("PX", CTransform.position.x);
            PlayerPrefs.SetFloat("PY", CTransform.position.y);
            PlayerPrefs.SetFloat("PZ", CTransform.position.z);
        }
        if (Input.GetKeyDown(Key))
        {
            IsRunning = !IsRunning;
            CTransform.eulerAngles = new Vector3(0, PlayerPrefs.GetFloat("RY", CTransform.eulerAngles.y), 0);
        }
        if (Input.GetKeyDown(TeleportKey))
        {
            CTransform.eulerAngles = new Vector3(PlayerPrefs.GetFloat("RX"), PlayerPrefs.GetFloat("RY"), PlayerPrefs.GetFloat("RZ"));
            CTransform.position = new Vector3(PlayerPrefs.GetFloat("PX"), PlayerPrefs.GetFloat("PY"), PlayerPrefs.GetFloat("PZ"));
        }

        if (!IsRunning)
            return;

        CTransform.eulerAngles = new Vector3(0, CTransform.rotation.eulerAngles.y, 0);
        Vector3 NewPosition = CTransform.position;
        NewPosition += new Vector3(CTransform.forward.x,0,CTransform.forward.z) * Time.deltaTime * ForwardSpeed;
        if(DesiredY != 0)
            NewPosition.y += (DesiredY - NewPosition.y) * Time.deltaTime * UpSpeed;
        CTransform.position = NewPosition;

        if(ToMoveWith != null)
            ToMoveWith.transform.position = NewPosition;
	}
}
