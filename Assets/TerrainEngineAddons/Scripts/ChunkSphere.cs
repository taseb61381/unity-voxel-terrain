using UnityEngine;
using System.Collections;

using TerrainEngine;

public class ChunkSphere : MonoBehaviour 
{
    public TerrainChunkScript Script;
    public Vector3 OriginalPosition;
    public int x;
    public int z;
    public Vector3 LastPosition;
    public int VoxelType = 0;
    public bool DrawNormal = false;
    public Vector3 Offset;
    public Vector3 Normal;

    public void Init(TerrainChunkScript Script, int x,float Height, int z)
    {
        this.Script = Script;
        this.x = x;
        this.z = z;

        this.OriginalPosition = new Vector3(x * TerrainManager.Instance.Informations.Scale, Height - Script.ChunkRef.WorldPosition.y, z * TerrainManager.Instance.Informations.Scale);
        this.transform.localPosition = OriginalPosition;
        this.LastPosition = transform.position;
    }

    void Update()
    {
        if (LastPosition != transform.position)
        {
            LastPosition = transform.position;

            Vector3 Position = transform.position;
            byte Type = (byte)VoxelType;
            ITerrainBlock Block = null;
            TerrainBlock BlockRef = null;
            float Height = Position.y;
            if (Script.ChunkRef.IsValid(ref BlockRef))
                Height = BlockRef.GetHeight(ref Block, Position, ref Type).y;

            if (Height == -1)
                return;

            transform.localPosition = Offset + new Vector3(transform.localPosition.x, (Height - Script.ChunkRef.WorldPosition.y), transform.localPosition.z);
        }

        if (DrawNormal)
        {
            TerrainBlock BlockRef = null;
            if (Script.ChunkRef.IsValid(ref BlockRef))
            {
                Normal = BlockRef.GetNormal(BlockRef.GetVoxelFromWorldPosition(LastPosition,false));
                Debug.DrawRay(LastPosition, Normal, Color.red, 1);
            }
        }
    }
}
